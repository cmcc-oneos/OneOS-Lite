/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task_switch.S
 *
 * @brief       This file provides context switch functions related to the RISC-V architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-18   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include "oneos_config.h"
#include "riscv_bits.h"

.equ    OS_TASK_STATE_RUNNING,    0x0004              /* Task Status Flag (RUNNING) */
.equ    PFIC_IENR1,               0xE000E100

.extern g_os_current_task
.extern g_os_next_task
.extern os_task_switch_interrupt_flag
.extern os_task_switch_notify
.extern os_test_print


/**
 ***********************************************************************************************************************
 * @brief           This function is pending SW_handler.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
.macro PEND_SW
    li  t0,0x4000
    li  t1,0xE000E200
    sw  t0, 0 * REGBYTES(t1)
.endm


.macro EN_YSYIRQ
    li  t0,0x1000
    li  t1,0xE000E100
    sw  t0, 0 * REGBYTES(t1)
.endm



/**
 ***********************************************************************************************************************
 * @brief           This function is called when the scheduler starts the first task, Only used once.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
.macro SWITCH_TO_TASK_PROCESS

    /* get switch to task struct */    
    la    t1, g_os_next_task                    /* t1 = &g_os_next_task */
    LOAD  t1, 0(t1)                             /* t1 = g_os_next_task */
    
    /* g_os_current_task = g_os_next_task */
    la    t0, g_os_current_task                 /* t0 = &g_os_current_task */
    STORE t1, 0(t0)                             /* (*t0) = t1 = g_os_next_task */

    /* begin: Restore next task context */
    LOAD  sp, 0(t1)                             /* sp = g_os_next_task->stack_top; Pickup task stack pointer */

    /* Set switch to task running state */
    lh    t0, 3 * REGBYTES(t1)                  /* t0 = g_os_current_task->state */
    ori   t0, t0, OS_TASK_STATE_RUNNING         /* t0 |= (OS_TASK_STATE_RUNNING) */
    sh    t0, 3 * REGBYTES(t1)                  /* g_os_current_task->state = t0 */

    /* set mpp, restore mstatus */
    LOAD  t0,  0 * REGBYTES(sp)
    li    t2,  0x00001800                      /* set the privilege as machine mode. */
    or    t0,  t0, t2
    csrw  mstatus, t0

    /* load mepc */
    LOAD  t0,  1 * REGBYTES(sp)
    csrw  mepc, t0
.endm


.globl os_first_task_start
os_first_task_start:
    mv   t0,      sp
    csrw mscratch,t0

    SWITCH_TO_TASK_PROCESS
    LOAD  x1,   2 * REGBYTES(sp)                /* load ra */
    LOAD  x10, 10 * REGBYTES(sp)                /* load a0 */
            
#ifdef ARCH_RISCV_FPU
    addi sp,  sp, 64 * REGBYTES
#else
    addi sp,  sp, 32 * REGBYTES
#endif
    /* EN_YSYIRQ */

    mret


 /**
 ***********************************************************************************************************************
 * @brief           Start the task swtich process.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
.globl os_task_switch
os_task_switch:
    PEND_SW
    ret


