/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exception.c
 *
 * @brief       This file provides exception handler functions related to the Loongarch64 architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-09-21   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <os_types.h>
#include <os_util.h>
#include <arch_task.h>
#include <arch_interrupt.h>
#include <os_task.h>

static char *arch_exception_names[] = {
    "[0]INT",
    "[1]PIL",
    "[2]PIS",
    "[3]PIF",
    "[4]PME",
    "[5]PNR",
    "[6]PNX",
    "[7]PPI",
    "[8]ADEF/ADEM",
    "[9]ALE",
    "[Ah]BCE",
    "[Bh]SYS",
    "[Ch]BRK",
    "[Dh]INE",
    "[Eh]IPE",
    "[Fh]FPD",
    "[10h]SXD",
    "[11h]ASXD",
    "[12h]FPE/VFPE",
    "[13h]WPEF/WPEM",
    "[14h]BTD",
    "[15h]BTE",
    "[16h]GSPR",
    "[17h]HVC",
    "[18h]GCSC/GCHC",
};

/**
 ***********************************************************************************************************************
 * @brief           This function handles hard fault exception.
 *
 * @param[in]       vector          exception number.
 * @param[in]       stack_frame     The start address of the stack frame when the exception occurs.
 *
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
void os_arch_fault_exception(os_uint32_t vector, void *stack_frame)
{
#ifdef  STACK_TRACE_EN
        //_arch_exception_stack_show(stack_frame, msp , psp);

        while(1);
#else

    os_uint32_t ecode, esubcode;
    struct os_hw_stack_frame   *stack_common;

    stack_common = (struct os_hw_stack_frame *)stack_frame;

    /* Stack frame with floating point storage */
        os_kprintf("epc : 0x%Lx\r\n", stack_common->epc);
        os_kprintf("prmd: 0x%Lx\r\n", stack_common->prmd);
        os_kprintf("r0  : 0x%Lx\r\n", stack_common->r0);
        os_kprintf("r1  : 0x%Lx\r\n", stack_common->r1);
        os_kprintf("r2  : 0x%Lx\r\n", stack_common->r2);
        os_kprintf("r4  : 0x%Lx\r\n", stack_common->r4);
        os_kprintf("r5  : 0x%Lx\r\n", stack_common->r5);
        os_kprintf("r6  : 0x%Lx\r\n", stack_common->r6);
        os_kprintf("r7  : 0x%Lx\r\n", stack_common->r7);
        os_kprintf("r8  : 0x%Lx\r\n", stack_common->r8);
        os_kprintf("r9  : 0x%Lx\r\n", stack_common->r9);
        os_kprintf("r10 : 0x%Lx\r\n", stack_common->r10);
        os_kprintf("r11 : 0x%Lx\r\n", stack_common->r11);
        os_kprintf("r12 : 0x%Lx\r\n", stack_common->r12);
        os_kprintf("r13 : 0x%Lx\r\n", stack_common->r13);
        os_kprintf("r14 : 0x%Lx\r\n", stack_common->r14);
        os_kprintf("r15 : 0x%Lx\r\n", stack_common->r15);
        os_kprintf("r16 : 0x%Lx\r\n", stack_common->r16);
        os_kprintf("r17 : 0x%Lx\r\n", stack_common->r17);
        os_kprintf("r18 : 0x%Lx\r\n", stack_common->r18);
        os_kprintf("r19 : 0x%Lx\r\n", stack_common->r19);
        os_kprintf("r20 : 0x%Lx\r\n", stack_common->r20);
        os_kprintf("r21 : 0x%Lx\r\n", stack_common->r21);
        os_kprintf("r22 : 0x%Lx\r\n", stack_common->r22);
        os_kprintf("r23 : 0x%Lx\r\n", stack_common->r23);
        os_kprintf("r24 : 0x%Lx\r\n", stack_common->r24);
        os_kprintf("r25 : 0x%Lx\r\n", stack_common->r25);
        os_kprintf("r26 : 0x%Lx\r\n", stack_common->r26);
        os_kprintf("r27 : 0x%Lx\r\n", stack_common->r27);
        os_kprintf("r28 : 0x%Lx\r\n", stack_common->r28);
        os_kprintf("r29 : 0x%Lx\r\n", stack_common->r29);
        os_kprintf("r30 : 0x%Lx\r\n", stack_common->r30);
        os_kprintf("r31 : 0x%Lx\r\n", stack_common->r31);

    /* Exception is generated in task context. */
    if (os_task_interrupt_nesting_flag == 0)
    {
        os_kprintf("exception in task: %s\r\n", os_task_self()->name);
    }
    /* Exception is generated in interrupt context. */
    else
    {
        os_kprintf("exception in interrupt, irq number: 0x%X\r\n", os_irq_num());
    }

    ecode = vector & 0x3f;
    esubcode = (vector & 0x7FC0) >> 6;
    os_kprintf("Exception Ecode is: %s\r\n", arch_exception_names[ecode]);
    if((ecode == 0x8) || (ecode == 0x12) || (ecode == 0x13) || (ecode == 0x18))
    {
        os_kprintf("Esubcode is: %d\r\n", esubcode);
    }

    while (1);
#endif  /* STACK_TRACE_EN */

}

