/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_interrupt.c
 *
 * @brief       This file provides interrupt related functions under the Loongarch64 architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-09-21   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include <os_types.h>
#include <arch_task.h>

/**
 ***********************************************************************************************************************
 * @brief           Disable interrupt.
 *
 * @param           None.
 *
 * @return          The state before disable interrupt.
 ***********************************************************************************************************************
 */
os_ubase_t os_irq_lock(void)
{
    __volatile__ os_ubase_t crmd_ie = 0;
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__(
        "move    %0, $r0\n"
        "addi.d  %1, $r0, 0x4\n"
        "csrxchg %0, %1, 0x0\n"
        "and     %0, %0, %1\n"
        : "+r"(crmd_ie),"+r"(tmpval)
        : 
        : "memory");

    return crmd_ie;
}

/**
 ***********************************************************************************************************************
 * @brief           Restore interrupt status.
 *
 * @param[in]       The status need be restore.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_irq_unlock(os_ubase_t irq_save)
{
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__(
        "addi.d  %1, $r0, 0x4\n"
        "csrxchg %0, %1, 0x0\n"
        : 
        : "r"(irq_save),"r"(tmpval)
        : "memory");

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Disable interrupt.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_irq_disable(void)
{
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__(
        "addi.d  %0, $r0, 0x4\n"
        "csrxchg $r0, %0, 0x0\n"
        : 
        : "r"(tmpval)
        : "memory");

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Enable interrupt.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_irq_enable(void)
{
    __volatile__ os_ubase_t tmpval1 = 0;
    __volatile__ os_ubase_t tmpval2 = 0;

    __asm__ __volatile__(
        "addi.d  %1, $r0, 0x4\n"
        "move    %0, %1\n"
        "csrxchg %0, %1, 0x0\n"
        : 
        : "r"(tmpval1),"r"(tmpval2)
        : "memory");

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Get the current context state, 1: irq context  0: task context.
 *
 * @param           None
 *
 * @return          0:        task context.
 *                  1:        irq context.
 ***********************************************************************************************************************
 */
os_bool_t os_is_irq_active(void)
{
    if(os_task_interrupt_nesting_flag==0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/**
 ***********************************************************************************************************************
 * @brief           Get irq status.
 *
 * @param           None
 *
 * @return          0:    irq enable
 *                  1:    irq disable
 ***********************************************************************************************************************
 */
os_bool_t os_is_irq_disabled(void)
{
    __volatile__ os_bool_t ret = 0;
    __volatile__ os_ubase_t crmd_ie = 0;
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__(
        "move    %0, $r0\n"        
        "csrrd   %1, 0x0\n"
        "addi.d  %2, $r0, 0x4\n"
        "and     %1, %1, %2\n"
        "bnez    %1, in_irq_enable\n"
        "addi.d  %0, $r0, 0x1\n"
        "in_irq_enable:"
        :"=r"(ret),"+r"(crmd_ie),"+r"(tmpval)
        : 
        :"cc", "memory");

    return ret;    
}
/**
 ***********************************************************************************************************************
 * @brief           Get irq num.
 *
 * @param           None
 *
 * @return          "CSR_MCAUSE" num
 ***********************************************************************************************************************
 */
os_uint32_t os_irq_num(void)
{
    __volatile__ os_uint32_t ret = 0;
    __volatile__ os_uint32_t ecfg = 0;
    __volatile__ os_uint32_t tmpval = 0;

    __asm__ __volatile__(
        "lu12i.w %2, 0x1\n"
        "addi.w  %2, %2, 0x7ff\n"
        "addi.w  %2, %2, 0x1\n"
        "addi.w  %2, %2, 0x7ff\n"        // $%2 = 0x1fff
        "csrrd   %0, 0x5\n"
        "and     %0, %0, %2\n"
        "csrrd   %1, 0x4\n"
        "and     %1, %1, %2\n"
        "and     %0, %0, %1\n"           // ret = ESTAT & ECFG

        :"=r"(ret), "+r"(ecfg),"+r"(tmpval)
        : 
        :"memory");

    return ret;
}
/**
 ***********************************************************************************************************************
 * @brief           Determine whether the current context is an exception context.
 *
 * @detail          If return 0, context may running into one status of these: "task", "interrupt" and "NMI". 
 *
 * @param           None.
 *
 * @return          Return 1 in exception context, otherwise return 0.
 * @retval          1               In exception context.
 * @retval          0               In other context.
 ***********************************************************************************************************************
 */
os_bool_t os_is_fault_active(void)
{
    if(os_task_exception_nesting_flag==0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

