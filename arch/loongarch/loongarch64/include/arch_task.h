/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task.h
 *
 * @brief       This file provides external declarations of architecture-related functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-09-21   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __ARCH_TASK_H__
#define __ARCH_TASK_H__

#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_ARCH_STACK_ALIGN_SIZE 8

extern volatile os_uint32_t os_task_interrupt_nesting_flag;
extern volatile os_uint32_t os_task_exception_nesting_flag;

struct os_hw_stack_frame
{
    os_ubase_t epc;  /* EPC          - exception program counter           */
    os_ubase_t prmd; /* CSR_PRMD     - exception processor status register */
    os_ubase_t r0;   /* zero         - for FPU emulator and gdb debug stub */
    os_ubase_t r1;   /* ra           - return address for jumps            */
    os_ubase_t r2;   /* tp           - task pointer                        */
    /* r3==sp, needn`t save it                                                     */
    os_ubase_t r4;  /* a0           - function argument 0                 */
    os_ubase_t r5;  /* a1           - function argument 1                 */
    os_ubase_t r6;  /* a2           - function argument 2                 */
    os_ubase_t r7;  /* a3           - function argument 3                 */
    os_ubase_t r8;  /* a4           - function argument 4                 */
    os_ubase_t r9;  /* a5           - function argument 5                 */
    os_ubase_t r10; /* a6           - function argument 6                 */
    os_ubase_t r11; /* a7           - function argument 7                 */
    os_ubase_t r12; /* t0           - temporary register 0                */
    os_ubase_t r13; /* t1           - temporary register 1                */
    os_ubase_t r14; /* t2           - temporary register 2                */
    os_ubase_t r15; /* t3           - temporary register 3                */
    os_ubase_t r16; /* t4           - temporary register 4                */
    os_ubase_t r17; /* t5           - temporary register 5                */
    os_ubase_t r18; /* t6           - temporary register 6                */
    os_ubase_t r19; /* t7           - temporary register 7                */
    os_ubase_t r20; /* t8           - temporary register 8                */
    os_ubase_t r21; /* t9           - temporary register 9                */
    os_ubase_t r22; /* fp           - frame pointer                       */
    os_ubase_t r23; /* s0           - save register 0                     */
    os_ubase_t r24; /* s1           - save register 1                     */
    os_ubase_t r25; /* s2           - save register 2                     */
    os_ubase_t r26; /* s3           - save register 3                     */
    os_ubase_t r27; /* s4           - save register 4                     */
    os_ubase_t r28; /* s5           - save register 5                     */
    os_ubase_t r29; /* s6           - save register 6                     */
    os_ubase_t r30; /* s7           - save register 7                     */
    os_ubase_t r31; /* s8           - save register 8                     */
};

extern void *os_hw_stack_init(void *tentry, void *parameter, void *stack_begin, os_uint32_t stack_size, void *texit);

extern os_uint32_t os_hw_stack_max_used(void *stack_begin, os_uint32_t stack_size);

extern void os_first_task_start(void);
extern void os_task_switch(void);

#ifdef OS_USING_OVERFLOW_CHECK
extern os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_TASK_H__ */
