/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exception.c
 *
 * @brief       exception information function
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_types.h>
#include <os_util.h>
#include <arch_task.h>
#include <arch_interrupt.h>
#include <arch_misc.h>
#include <os_task.h>

#if (CFG_SOC_NAME == SOC_BK7231N)
#define START_TYPE_ADDR (0x00800000 + 84 * 4)
#else
#define START_TYPE_ADDR (0x0080a080)
#endif

#define CRASH_XAT0_VALUE           0xbedead00
#define CRASH_UNDEFINED_VALUE      0xbedead01
#define CRASH_PREFETCH_ABORT_VALUE 0xbedead02
#define CRASH_DATA_ABORT_VALUE     0xbedead03
#define CRASH_UNUSED_VALUE         0xbedead04

struct os_exception_registers
{
    os_uint32_t r0;
    os_uint32_t r1;
    os_uint32_t r2;
    os_uint32_t r3;
    os_uint32_t r4;
    os_uint32_t r5;
    os_uint32_t r6;
    os_uint32_t r7;
    os_uint32_t r8;
    os_uint32_t r9;
    os_uint32_t r10;
    os_uint32_t fp;
    os_uint32_t ip;
    os_uint32_t sp;
    os_uint32_t lr;
    os_uint32_t pc;
    os_uint32_t spsr;
    os_uint32_t cpsr;
};

/**
 ***********************************************************************************************************************
 * @brief           this function will show registers of CPU.
 *
 * @param[in]       regs        system registers frame pointer
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_show_register(struct os_exception_registers *regs)
{
    os_kprintf("Current regs:\r\n");
    os_kprintf("r00:0x%08x r01:0x%08x r02:0x%08x r03:0x%08x\r\n", regs->r0, regs->r1, regs->r2, regs->r3);
    os_kprintf("r04:0x%08x r05:0x%08x r06:0x%08x r07:0x%08x\r\n", regs->r4, regs->r5, regs->r6, regs->r7);
    os_kprintf("r08:0x%08x r09:0x%08x r10:0x%08x\r\n", regs->r8, regs->r9, regs->r10);
    os_kprintf("fp :0x%08x ip :0x%08x\r\n", regs->fp, regs->ip);
    os_kprintf("sp :0x%08x lr :0x%08x pc :0x%08x\r\n", regs->sp, regs->lr, regs->pc);
    os_kprintf("SPSR:0x%08x\r\n", regs->spsr);
    os_kprintf("CPSR:0x%08x\r\n", regs->cpsr);

    int                 i;
    const unsigned int *reg1;

    os_kprintf("\r\nseparate regs:\r\n");

    reg1 = (const unsigned int *)0x400024;
    os_kprintf("SYS:cpsr r8-r14\r\n");
    for (i = 0; i < 0x20 >> 2; i++)
    {
        os_kprintf("0x%08x\r\n", *(reg1 + i));
    }

    os_kprintf("IRQ:cpsr spsr r8-r14\r\n");
    reg1 = (const unsigned int *)0x400044;
    for (i = 0; i < 0x24 >> 2; i++)
    {
        os_kprintf("0x%08x\r\n", *(reg1 + i));
    }

    os_kprintf("FIR:cpsr spsr r8-r14\r\n");
    reg1 = (const unsigned int *)0x400068;
    for (i = 0; i < 0x24 >> 2; i++)
    {
        os_kprintf("0x%08x\r\n", *(reg1 + i));
    }

    os_kprintf("ABT:cpsr spsr r8-r14\r\n");
    reg1 = (const unsigned int *)0x40008c;
    for (i = 0; i < 0x24 >> 2; i++)
    {
        os_kprintf("0x%08x\r\n", *(reg1 + i));
    }

    os_kprintf("UND:cpsr spsr r8-r14\r\n");
    reg1 = (const unsigned int *)0x4000b0;
    for (i = 0; i < 0x24 >> 2; i++)
    {
        os_kprintf("0x%08x\r\n", *(reg1 + i));
    }

    os_kprintf("SVC:cpsr spsr r8-r14\r\n");
    reg1 = (const unsigned int *)0x4000d4;
    for (i = 0; i < 0x24 >> 2; i++)
    {
        os_kprintf("0x%08x\r\n", *(reg1 + i));
    }

    os_kprintf("\r\n");
}

/**
 ***********************************************************************************************************************
 * @brief           When ARM9E comes across an instruction which it cannot handle,
 *                  it takes the undefined instruction trap.
 *
 * @param[in]       frame        system registers frame pointer
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_trap_udef(void *frame)
{
    struct os_exception_registers *regs = (struct os_exception_registers *)frame;

#if (CFG_SOC_NAME == SOC_BK7231N)
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)(CRASH_UNDEFINED_VALUE & 0xffff);
#else
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)CRASH_UNDEFINED_VALUE;
#endif

    os_kprintf("\r\nException has occurred! exception name: undefined instruction\r\n");
    os_hw_show_register(regs);
    if ((regs->spsr & 0x1F) == 0x13)
    {
        os_task_t *task = os_task_self();
        os_kprintf("exception from task, task name: %.*s \r\n", OS_NAME_MAX, task->name);
    }
    else
    {
        os_kprintf("exception from interrupt or other exception, machine mode: 0x%X\r\n", (regs->spsr & 0x1F));
    }
    os_hw_cpu_shutdown();
}

/**
 ***********************************************************************************************************************
 * @brief           The software interrupt instruction (SWI) is used for entering Supervisor mode,
 *                  usually to request a particular supervisor function.
 *
 * @param[in]       frame        system registers frame pointer
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_trap_swi(void *frame)
{
    struct os_exception_registers *regs = (struct os_exception_registers *)frame;

    os_kprintf("\r\nException has occurred! exception name: software interrupt\r\n");
    os_hw_show_register(regs);
    if ((regs->spsr & 0x1F) == 0x13)
    {
        os_task_t *task = os_task_self();
        os_kprintf("exception from task, task name: %.*s \r\n", OS_NAME_MAX, task->name);
    }
    else
    {
        os_kprintf("exception from interrupt or other exception, machine mode: 0x%X\r\n", (regs->spsr & 0x1F));
    }
    os_hw_cpu_shutdown();
}

/**
 ***********************************************************************************************************************
 * @brief           An abort indicates that the current memory access cannot be completed,
 *                  which occurs during an instruction prefetch.
 *
 * @param[in]       frame        system registers frame pointer
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_trap_pabt(void *frame)
{
    struct os_exception_registers *regs = (struct os_exception_registers *)frame;

#if (CFG_SOC_NAME == SOC_BK7231N)
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)(CRASH_PREFETCH_ABORT_VALUE & 0xffff);
#else
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)CRASH_PREFETCH_ABORT_VALUE;
#endif

    os_kprintf("\r\nException has occurred! exception name: prefetch abort\r\n");
    os_hw_show_register(regs);
    if ((regs->spsr & 0x1F) == 0x13)
    {
        os_task_t *task = os_task_self();
        os_kprintf("exception from task, task name: %.*s \r\n", OS_NAME_MAX, task->name);
    }
    else
    {
        os_kprintf("exception from interrupt or other exception, machine mode: 0x%X\r\n", (regs->spsr & 0x1F));
    }
    os_hw_cpu_shutdown();
}

/**
 ***********************************************************************************************************************
 * @brief           An abort indicates that the current memory access cannot be completed,
 *                  which occurs during a data access.
 *
 * @param[in]       frame        system registers frame pointer
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_trap_dabt(void *frame)
{
    struct os_exception_registers *regs = (struct os_exception_registers *)frame;

#if (CFG_SOC_NAME == SOC_BK7231N)
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)(CRASH_DATA_ABORT_VALUE & 0xffff);
#else
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)CRASH_DATA_ABORT_VALUE;
#endif

    os_kprintf("\r\nException has occurred! exception name: data abort\r\n");
    os_hw_show_register(regs);
    if ((regs->spsr & 0x1F) == 0x13)
    {
        os_task_t *task = os_task_self();
        os_kprintf("exception from task, task name: %.*s \r\n", OS_NAME_MAX, task->name);
    }
    else
    {
        os_kprintf("exception from interrupt or other exception, machine mode: 0x%X\r\n", (regs->spsr & 0x1F));
    }
    os_hw_cpu_shutdown();
}

/**
 ***********************************************************************************************************************
 * @brief           Normally, system will never reach here.
 *
 * @param[in]       frame        system registers frame pointer
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_trap_resv(void *frame)
{
    struct os_exception_registers *regs = (struct os_exception_registers *)frame;

#if (CFG_SOC_NAME == SOC_BK7231N)
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)(CRASH_UNUSED_VALUE & 0xffff);
#else
    *((volatile os_uint32_t *)START_TYPE_ADDR) = (os_uint32_t)CRASH_UNUSED_VALUE;
#endif

    os_kprintf("\r\nException has occurred! exception name: not used\r\n");
    os_hw_show_register(regs);
    if ((regs->spsr & 0x1F) == 0x13)
    {
        os_task_t *task = os_task_self();
        os_kprintf("exception from task, task name: %.*s \r\n", OS_NAME_MAX, task->name);
    }
    else
    {
        os_kprintf("exception from interrupt or other exception, machine mode: 0x%X\r\n", (regs->spsr & 0x1F));
    }
    os_hw_cpu_shutdown();
}
