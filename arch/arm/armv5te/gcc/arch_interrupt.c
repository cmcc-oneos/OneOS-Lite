/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_interrupt.c
 *
 * @brief       This file provides functions related to the ARM9E architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <os_types.h>

/**
 ***********************************************************************************************************************
 * @brief           Get irq status and disable irq.
 *
 * @param           None
 *
 * @return          0:    irq enable
 *                  1:    irq disable
 ***********************************************************************************************************************
 */
os_ubase_t os_irq_lock(void)
{
    __volatile__ os_ubase_t cpsr_if = 0;
    __volatile__ os_ubase_t tmpval  = 0;

    __asm__ __volatile__("MRS     %1, CPSR\n"
                         "AND     %0, %1, #0xc0\n"
                         "ORR     %1, %1, #0xc0\n"
                         "MSR     CPSR_c, %1\n"
                         : "=r"(cpsr_if), "+r"(tmpval)
                         :
                         : "memory");

    return cpsr_if;
}

/**
 ***********************************************************************************************************************
 * @brief           Restore irq enable state.
 *
 * @param[in]       irq_save          irq status
 *
 * @return          None
 ***********************************************************************************************************************
 */
void os_irq_unlock(os_ubase_t irq_save)
{
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__("MRS     %1, CPSR\n"
                         "BIC     %1, %1, #0xc0\n"
                         "ORR     %1, %1, %0\n"
                         "MSR     CPSR_c, %1\n"
                         :
                         : "r"(irq_save), "r"(tmpval)
                         : "memory");

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           disable irq.
 *
 * @param           None
 *
 * @return          None
 ***********************************************************************************************************************
 */
void os_irq_disable(void)
{
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__("MRS     %0, CPSR\n"
                         "ORR     %0, %0, #0xc0\n"
                         "MSR     CPSR_c, %0\n"
                         : "+r"(tmpval)
                         :
                         : "memory");

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           enable irq.
 *
 * @param           None
 *
 * @return          None
 ***********************************************************************************************************************
 */
void os_irq_enable(void)
{
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__("MRS     %0, CPSR\n"
                         "BIC     %0, %0, #0xc0\n"
                         "MSR     CPSR_c, %0\n"
                         :
                         : "r"(tmpval)
                         : "memory");

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Get the current context state, 1: irq context  0: task context.
 *
 * @param           None
 *
 * @return          0:        task context.
 *                  1:        irq context.
 ***********************************************************************************************************************
 */
os_bool_t os_is_irq_active(void)
{
    __volatile__ os_bool_t  active = 1;
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__("   MRS     %1, CPSR\n"
                         "   AND     %1, %1, #0x1f\n"
                         "   CMP     %1, #0x11\n" /* into the FIQ mode */
                         "   BEQ     in_irq_context\n"
                         "   CMP     %1, #0x12\n" /* into the IRQ mode */
                         "   BEQ     in_irq_context\n"
                         "   MOV     %0, #0\n"
                         "in_irq_context:"
                         : "+r"(active), "+r"(tmpval)
                         :
                         : "memory");

    return active;
}

/**
 ***********************************************************************************************************************
 * @brief           Get irq status.
 *
 * @param           None
 *
 * @return          0:    irq enable
 *                  1:    irq disable
 ***********************************************************************************************************************
 */
os_bool_t os_is_irq_disabled(void)
{
    __volatile__ os_bool_t  disabled = 1;
    __volatile__ os_ubase_t tmpval   = 0;

    __asm__ __volatile__("MRS     %1, CPSR\n"
                         "AND     %1, %1, #0xc0\n"
                         "CMP     %1, #0xc0\n" /* CPSR_I=1 && CPSR_F=1 */
                         "BEQ     in_irq_disabled\n"
                         "MOV     %0, #0\n"
                         "in_irq_disabled:"
                         : "+r"(disabled), "+r"(tmpval)
                         :
                         : "memory");

    return disabled;
}

/**
 ***********************************************************************************************************************
 * @brief           Get irq num.
 *
 * @param           None
 *
 * @return          "CSR_MCAUSE" num
 ***********************************************************************************************************************
 */
os_uint32_t os_irq_num(void)
{
    __volatile__ os_uint32_t irq_num = 0;

    __asm__ __volatile__("MOV     %0, #0\n" : "=r"(irq_num) : :);

    return irq_num;
}

/**
 ***********************************************************************************************************************
 * @brief           Determine whether the current context is an exception context.
 *
 *
 * @param           None.
 *
 * @return          Return 1 in exception context, otherwise return 0.
 * @retval          1               In exception context.
 * @retval          0               In other context.
 ***********************************************************************************************************************
 */
os_bool_t os_is_fault_active(void)
{
    __volatile__ os_bool_t  active = 1;
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__("   MRS     %1, CPSR\n"
                         "   AND     %1, %1, #0x1f\n"
                         "   CMP     %1, #0x17\n" /* into the Abort mode */
                         "   BEQ     in_fault_context\n"
                         "   CMP     %1, #0x1B\n" /* into the Undefined mode */
                         "   BEQ     in_fault_context\n"
                         "   MOV     %0, #0\n"
                         "in_fault_context:"
                         : "+r"(active), "+r"(tmpval)
                         :
                         : "memory");

    return active;
}

/**
 ***********************************************************************************************************************
 * @brief           Get CPSR.
 *
 * @param           None
 *
 * @return          CPSR value.
 ***********************************************************************************************************************
 */
os_ubase_t os_get_cpsr(void)
{
    __volatile__ os_ubase_t cpsr = 0;

    __asm__ __volatile__("MRS     %0, CPSR\n" : "=r"(cpsr) : : "memory");

    return cpsr;
}
