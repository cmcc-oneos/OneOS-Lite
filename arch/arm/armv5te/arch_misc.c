/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_misc.c
 *
 * @brief       This file provides functions related to the ARM9E architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <os_types.h>
#include <arch_interrupt.h>
#include <os_assert.h>
#include <os_util.h>

/**
 ***********************************************************************************************************************
 * @brief           Find the first bit set in value.
 *
 * @param[in]       value
 *
 * @return          Returns lowest bit equal to 1 in value, or returns zero if value is zero.
 ***********************************************************************************************************************
 */
os_int32_t os_ffs(os_uint32_t value)
{
    return __builtin_ffs(value);
}

/**
 ***********************************************************************************************************************
 * @brief           Find the last bit set in value.
 *
 * @param[in]       value
 *
 * @return          Returns highest bit equal to 1 in value, or returns zero if value is zero.
 ***********************************************************************************************************************
 */
os_int32_t os_fls(os_uint32_t value)
{
    os_int32_t pos;

    pos = 32;

    if (!value)
    {
        pos = 0;
    }
    else
    {
        if (!(value & 0xFFFF0000U))
        {
            value <<= 16;
            pos -= 16;
        }

        if (!(value & 0xFF000000U))
        {
            value <<= 8;
            pos -= 8;
        }

        if (!(value & 0xF0000000U))
        {
            value <<= 4;
            pos -= 4;
        }

        if (!(value & 0xC0000000U))
        {
            value <<= 2;
            pos -= 2;
        }

        if (!(value & 0x80000000U))
        {
            value <<= 1;
            pos -= 1;
        }
    }

    return pos;
}

/**
 ***********************************************************************************************************************
 * @brief           Get the sp of the current task.
 *
 * @param           None
 *
 * @return          sp pointer
 ***********************************************************************************************************************
 */
void *os_get_current_task_sp(void)
{
    register void *result;

    __asm__ __volatile__("   mov %0, sp\n" : "=r"(result));

    return result;
}

/**
 ***********************************************************************************************************************
 * @brief           shutdown CPU.
 *
 * @param           None
 *
 * @return          None
 ***********************************************************************************************************************
 */
void os_hw_cpu_shutdown(void)
{
    os_uint32_t level;
    os_kprintf("shutdown...\r\n");

    level = os_irq_lock();
    while (level)
    {
        OS_ASSERT(0);
    }
}