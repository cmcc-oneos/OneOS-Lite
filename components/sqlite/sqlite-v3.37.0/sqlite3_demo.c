#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"

#define DB_NANE "test.db"

sqlite3 *db = NULL;
char *sql = NULL;
char *zErrMsg = NULL;
const char *data = "Callback function called";
int ret = 0;

typedef enum
{
    false,
    true
} bool;

/*
typedef int (*sqlite3_callback)(
void*,    Data provided in the 4th argument of sqlite3_exec()
int,      The number of columns in row
char**,   An array of strings representing fields in the row
char**    An array of strings representing column names
);
*/
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i = 0;
    for (i = 0; i < argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");

    return 0;
}

bool connectDB()
{
    sqlite3_initialize();

    ret = sqlite3_open(DB_NANE, &db);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "Error open database: %s\n", sqlite3_errmsg(db));
        sqlite3_free(zErrMsg);

        return false;
    }

    fprintf(stdout, "Successfully opened database\n");
    return true;
}

bool createTable()
{
    /* Create SQL statement */
    sql = "CREATE TABLE COMPANY("
          "ID INT PRIMARY KEY     NOT NULL,"
          "NAME           TEXT    NOT NULL,"
          "AGE            INT     NOT NULL,"
          "ADDRESS        CHAR(50),"
          "SALARY         REAL );";

    /* Execute SQL statement */
    ret = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "Error SQL: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);

        return false;
    }

    fprintf(stdout, "Successfully table created\n");

    return true;
}

bool insertRecords()
{
    /* Create SQL statement */
    sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
          "VALUES (1, 'Paul', 32, 'California', 20000.00 ); "
          "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
          "VALUES (2, 'Allen', 25, 'Texas', 15000.00 ); "
          "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)"
          "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );"
          "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)"
          "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";

    /* Execute SQL statement */
    ret = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }

    fprintf(stdout, "successfully records created\n");

    return true;
}

bool selectFromTable()
{
    /* Create SQL statement */
    sql = "SELECT * from COMPANY";

    /* Execute SQL statement */
    ret = sqlite3_exec(db, sql, callback, (void *)data, &zErrMsg);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "Error SQL: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }

    fprintf(stdout, "successfully operation done\n");

    return true;
}
bool updateTable()
{
    /* Create merged SQL statement */
    sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1; "
          "SELECT * from COMPANY";

    /* Execute SQL statement */
    ret = sqlite3_exec(db, sql, callback, (void *)data, &zErrMsg);
    if (ret != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);

        return false;
    }

    fprintf(stdout, "Successfully operation done \n");

    return true;
}

bool closeDB()
{
    int ret = 0;
    ret = sqlite3_close(db);
    if (ret == SQLITE_BUSY)
    {
        return false;
    }

    return true;
}

int sqlites_demo(int argc, char *argv[])
{
    connectDB();
    createTable();
    insertRecords();
    selectFromTable();
    updateTable();
    selectFromTable();
    closeDB();

    return 0;
}

#include <shell.h>
SH_CMD_EXPORT(sqlites_demo, sqlites_demo, "start sqlite db demo");
