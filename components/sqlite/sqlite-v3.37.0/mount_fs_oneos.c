#include <vfs_fs.h>
#include <dlog.h>
#include <shell.h>
#include <vfs_posix.h>
#include <os_clock.h>
#include <fal.h>
#include <os_task.h>
#include <fcntl.h>
#include <os_memory.h>
#include <unistd.h>

#define FILE_SYS_TAG    "FILE_SYS"


#ifdef OS_USING_VFS_FATFS

#define OS_FS_PART_NAME	"filesystem"

static void mount_fatfs(void)
{
		int ret;
	
    if (fal_blk_device_create(OS_FS_PART_NAME))
    {
        LOG_W(FILE_SYS_TAG, "Create a block device on the %s partition of flash successful.", OS_FS_PART_NAME);
    }
    else
    {
        LOG_E(FILE_SYS_TAG, "Can't create a block device on '%s' partition.", OS_FS_PART_NAME);
    }

		ret=vfs_mkfs("fat","filesystem");
		
		if(ret!=0)
		{
			LOG_E(FILE_SYS_TAG,"mkfs fat failed,ret=%d \r\n",ret);
		}
		else
		{
			LOG_W(FILE_SYS_TAG,"mkfs fat successful,ret=%d",ret);
	  }
		
		os_task_tsleep(300);
	
    if (vfs_mount(OS_FS_PART_NAME, "/", "fat", 0, 0) == 0)
    {
        LOG_W(FILE_SYS_TAG,"FAT filesystem mount successful.");
    }
    else
    {
        LOG_E(FILE_SYS_TAG,"FAT filesystem mount fail.\r\n");
        LOG_E(FILE_SYS_TAG,"You should mkfs first, then reset board ! cmd: mkfs -t fat %s \r\n", OS_FS_PART_NAME);
    }
}
SH_CMD_EXPORT(sh_mount_fat,mount_fatfs,"mount_fatfs");

static void unmount_fatfs(void)
{
		int ret=0;
	
		ret=vfs_unmount("/");
	
		if(ret==0)
		{
			LOG_I(FILE_SYS_TAG, "unmount fat success");
		}
		else if(ret==-1)
		{
			LOG_E(FILE_SYS_TAG, "unmount fat failed");
		}
}
SH_CMD_EXPORT(sh_unmount_fat,unmount_fatfs,"unmount fatfs");

#endif

#ifdef OS_USING_VFS_JFFS2

static void mount_jffs2(void)
{
	
		os_kprintf("**********************************************\n\r");
		os_kprintf("***********jffs2 test start*******************\n\r");
		os_kprintf("**********************************************\n\r");
    const char * part_name = "backup";//OS_BK_PART_NAME;
		int ret;

	
    if (fal_blk_device_create(part_name))
    {
        LOG_I(FILE_SYS_TAG,"Create a block device on the %s partition of flash successful.\r\n", part_name);
    }
    else
    {
        LOG_E(FILE_SYS_TAG,"Can't create a block device on '%s' partition.\r\n", part_name);
    }

		ret=vfs_mkfs("jffs2",part_name);
		
		if(ret!=0)
		{
			LOG_E(FILE_SYS_TAG,"mkfs jffs2 failed,ret=%d",ret);
		}
		else
		{
			LOG_W(FILE_SYS_TAG,"mkfs jffs2 successful,ret=%d",ret);
	  }
		os_task_tsleep(300);
		
    if (vfs_mount(part_name, "/", "jffs2", 0, 0) == 0)
    {
        LOG_I(FILE_SYS_TAG, "JFFS2 filesystem mount successful.");
    }
    else
    {
				LOG_E(FILE_SYS_TAG, "jffs2 filesystem mount fail.");
        LOG_E(FILE_SYS_TAG, "You could try mkfs first, which means erasing the whole partition: %s, then reset board !", part_name);
    }
}

SH_CMD_EXPORT(sh_mount_jffs2,mount_jffs2,"mount jffs2");

static void unmount_jffs2(void)
{
	int ret=0;
	ret=vfs_unmount("/");
	if(ret==0)
	{
		LOG_I(FILE_SYS_TAG, "unmount jffs2 success");
	}
	else if(ret==-1)
	{
		LOG_E(FILE_SYS_TAG, "unmount jffs2 failed");
	}
}
SH_CMD_EXPORT(sh_unmount_jffs2,unmount_jffs2,"unmount jffs2");

#endif


#if defined(OS_USING_VFS_NFS) 
static void mount_nfs()
{//  "192.168.1.93"
		os_kprintf("**********************************************\n");
		os_kprintf("***********nfs test start*********************\n");
		os_kprintf("**********************************************\n");
		
	if (vfs_mount(OS_NULL, "/", "nfs", 0,OS_NFS_HOST_EXPORT) == 0)
	{
       LOG_I(FILE_SYS_TAG, "NFSv3 File System mount success!\n");
	}
  else
	{
       LOG_E(FILE_SYS_TAG, "NFSv3 File System mount failed!\n");
	}
}

SH_CMD_EXPORT(sh_mount_nfs,mount_nfs,"mount nfs");

static void unmount_nfs()
{
	if(vfs_unmount("/")==0)
	{
       LOG_I(FILE_SYS_TAG, "NFSv3 File System unmount success!\n");
	}
   else
	{
       LOG_E(FILE_SYS_TAG, "NFSv3 File System unmount failed!\n");
	}
}
SH_CMD_EXPORT(sh_unmount_nfs,unmount_nfs,"unmount nfs");

#endif


#if defined(OS_USING_VFS_CUTEFS) && defined(OS_USING_RAMDISK)

#include <ramdisk.h>

#define RAMDISK_NAME        "ramdisk0"
#define RAMDISK_SIZE        (25*1024)
#define RAMDISK_BLK_SIZE    512

ramdisk_dev_t *ram_dev;

static void mount_cutefs(void)
{
		os_kprintf("**********************************************");
		os_kprintf("***********ram test start*******************");
		os_kprintf("**********************************************");
	
    ram_dev = ramdisk_dev_create(RAMDISK_NAME, RAMDISK_SIZE, RAMDISK_BLK_SIZE);
    if (!ram_dev)
    {
        LOG_E(FILE_SYS_TAG, "create ramdisk failed.");
        return;
    }
    
    LOG_W(FILE_SYS_TAG, "create ramdisk successful.");

    if (0 != vfs_mount(RAMDISK_NAME, "/", "cute", 0, 0))
    {
        LOG_E(FILE_SYS_TAG, "mount cutefs failed.");
        return;
    }

    LOG_W(FILE_SYS_TAG, "mount cutefs successful.");
}
SH_CMD_EXPORT(sh_mount_cutefs,mount_cutefs,"mount_cutefs ");

static void unmount_ram_fs(void)
{
	int ret;
	ret=vfs_unmount("/");
	if(ret==0)
	{
		LOG_I(FILE_SYS_TAG,"unmount ramfs success,ret=%d \n",ret);
	}
	else 
	{
		LOG_E(FILE_SYS_TAG,"unmount ramfs success,ret=%d \n",ret);
	}
	ramdisk_dev_destroy(ram_dev);
	
}
SH_CMD_EXPORT(sh_unmount_cutefs,unmount_ram_fs,"unmount ramfs");
#endif


os_err_t init_func(void)
{
	/*jffs2*/
	#ifdef OS_USING_VFS_JFFS2
	mount_jffs2();
	return 0;
	#endif
	
		/*cutefs*/
	#if defined(OS_USING_VFS_CUTEFS) && defined(OS_USING_RAMDISK)
	mount_cutefs();
	return 0;
	#endif
	
	/*fatfs*/	
	#ifdef OS_USING_VFS_FATFS
	mount_fatfs();
	return 0;
	#endif
	
	/*nfs*/	
	#ifdef OS_USING_VFS_NFS
	mount_nfs();
  return 0;
	#endif
	
}

os_err_t clean_func(void)
{
	int ret;
	#ifdef OS_USING_VFS_JFFS2
	unmount_jffs2();
	ret=vfs_mkfs("jffs2","backup");
	if(ret!=0)
	{
		LOG_E(FILE_SYS_TAG,"mkfs jffs2 failed,ret=%d \n",ret);
	}
	#endif
	
	#if defined(OS_USING_VFS_CUTEFS) && defined(OS_USING_RAMDISK)
	unmount_ram_fs();
	return 0;
	#endif
	
	#ifdef OS_USING_VFS_FATFS
	unmount_fatfs();
	ret=vfs_mkfs("fat","filesystem");
	if(ret!=0)
	{
		LOG_E(FILE_SYS_TAG,"mkfs fat failed,ret=%d \n",ret);
	}
	return ret;
	#endif
	
	#ifdef OS_USING_VFS_NFS
	unmount_nfs();
	return 0;
	#endif
}
