/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mb_os.c
 *
 * @brief       MODBUS OneOS LAYER INTERFACE
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-05   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#define MB_OS_MODULE
#include "../../Source/mb.h"
#include <device.h>
#include <os_assert.h>
#include <os_errno.h>
#include <os_mq.h>
#include <os_sem.h>
#include <os_task.h>

#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)
static os_sem_t gs_mb_rx_sem[MODBUS_CFG_MAX_CH];
static void MB_OS_InitMaster(void);
static void MB_OS_ExitMaster(void);
#endif

static os_task_t gs_mb_rx_task;
static os_uint8_t gs_mb_rx_task_stack[MB_OS_CFG_RX_TASK_STK_SIZE];
static os_mq_t *gs_mb_rx_mq_handle;

static void MB_OS_InitRxTask(void);
static void MB_OS_ExitRxTask(void);
static void MB_OS_RxTask(void *p_arg);

os_err_t MB_RxDoneSignal(MODBUS_CH *pch)
{
    return os_mq_send(gs_mb_rx_mq_handle, &pch, sizeof(MODBUS_CH *), OS_NO_WAIT);
}

/*
*********************************************************************************************************
*                                              MB_OS_Init()
*
* Description : This function initializes the RTOS interface.  This function creates the following:
*
*               (1) A message queue to signal the reception of a packet.
*
*               (2) A task that waits for packets to be received.
*
* Argument(s) : none
*
* Return(s)   : none.
*
* Caller(s)   : MB_Init().
*
* Note(s)     : none.
*********************************************************************************************************
*/

void MB_OS_Init(void)
{
#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)
    MB_OS_InitMaster();
#endif
    MB_OS_InitRxTask();
}

/*
*********************************************************************************************************
*                                          MB_OS_InitMaster()
*
* Description : This function initializes and creates the kernel objectes needed for Modbus Master
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : MB_OS_Init().
*
* Note(s)     : none.
*********************************************************************************************************
*/

#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)
static void MB_OS_InitMaster(void)
{
    os_uint8_t i;
    os_err_t err;

    /* Create a semaphore for each channel */
    for (i = 0; i < MODBUS_CFG_MAX_CH; i++)
    {
        err = os_sem_init(&gs_mb_rx_sem[i], "uC/Modbus Rx", 0, 100);
        OS_ASSERT(err == OS_EOK);
    }
}
#endif

/*
*********************************************************************************************************
*                                          MB_OS_InitRxTask()
*
* Description : This function initializes and creates the kernel objectes needed for Modbus Rx
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : MB_OS_Init().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void MB_OS_InitRxTask(void)
{
    os_err_t err;
    const char *const name = "uC/Modbus Rx";
    err = os_task_init(&gs_mb_rx_task,
                       name,
                       MB_OS_RxTask,
                       OS_NULL,
                       &gs_mb_rx_task_stack[0],
                       sizeof(gs_mb_rx_task_stack),
                       MB_OS_CFG_RX_TASK_PRIO);
    OS_ASSERT(err == OS_EOK);
    gs_mb_rx_mq_handle = os_mq_create(name, sizeof(MODBUS_CH *), MODBUS_CFG_MAX_CH);
    OS_ASSERT(gs_mb_rx_mq_handle);
    err = os_task_startup(&gs_mb_rx_task);
    OS_ASSERT(err == OS_EOK);
}

/*
*********************************************************************************************************
*                                             MB_OS_Exit()
*
* Description : This function is called to terminate the RTOS interface for Modbus channels.  We delete
*               the following uC/OS-II objects:
*
*               (1) An event flag group to signal the reception of a packet.
*               (2) A task that waits for packets to be received.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : MB_Exit().
*
* Note(s)     : none.
*********************************************************************************************************
*/

void MB_OS_Exit(void)
{
#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)
    MB_OS_ExitMaster();
#endif
    MB_OS_ExitRxTask();
}

/*
*********************************************************************************************************
*                                          MB_OS_ExitMaster()
*
* Description : This function is called to terminate the RTOS interface for Modbus Master channels.  The
*               following objects are deleted.
*
*               (1) An event flag group to signal the reception of a packet.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : MB_OS_Exit().
*
* Note(s)     : none.
*********************************************************************************************************
*/

#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)
static void MB_OS_ExitMaster(void)
{
    os_uint8_t i;
    os_err_t err;

    /* Delete semaphore for each channel */
    for (i = 0; i < MODBUS_CFG_MAX_CH; i++)
    {
        err = os_sem_deinit(&gs_mb_rx_sem[i]);
        OS_ASSERT(err == OS_EOK);
    }
}
#endif

/*
*********************************************************************************************************
*                                          MB_OS_ExitRxTask()
*
* Description : This function is called to terminate the RTOS interface for Modbus Rx channels.
*               The following objects are deleted.
*
*               (1) A task that waits for packets to be received.
*               (2) A message queue to signal the reception of a packet.
*
* Argument(s) : none
*
* Return(s)   : none.
*
* Caller(s)   : MB_OS_Exit().
*
* Note(s)     : none.
*********************************************************************************************************
*/

void MB_OS_ExitRxTask(void)
{
    os_err_t err;
    err = os_task_deinit(&gs_mb_rx_task);
    OS_ASSERT(err == OS_EOK);
    err = os_mq_destroy(gs_mb_rx_mq_handle);
    OS_ASSERT(err == OS_EOK);
    gs_mb_rx_mq_handle = OS_NULL;
}

/*
*********************************************************************************************************
*                                              MB_OS_RxSignal()
*
* Description : This function signals the reception of a packet either from the Rx ISR(s) or the RTU timeout
*               timer(s) to indicate that a received packet needs to be processed.
*
* Argument(s) : pch     specifies the Modbus channel data structure in which a packet was received.
*
* Return(s)   : none.
*
* Caller(s)   : MB_ASCII_RxByte(),
*               MB_RTU_TmrUpdate().
*
* Note(s)     : none.
*********************************************************************************************************
*/

void MB_OS_RxSignal(MODBUS_CH *pch)
{
    os_err_t err;

    if (pch != (MODBUS_CH *)0)
    {
        switch (pch->MasterSlave)
        {
#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)

        case MODBUS_MASTER:
            err = os_sem_post(&gs_mb_rx_sem[pch->Ch]);
            OS_ASSERT(err == OS_EOK);
            break;
#endif
#if (MODBUS_CFG_SLAVE_EN == DEF_ENABLED)

        case MODBUS_SLAVE:
        default:
            MB_RxTask(pch);
            break;
#endif
        }
    }
}

/*
*********************************************************************************************************
*                                              MB_OS_RxWait()
*
* Description : This function waits for a response from a slave.
*
* Argument(s) : pch     specifies the Modbus channel data structure to wait on.
*
*               perr    is a pointer to a variable that will receive an error code.  Possible errors are:
*
*                       MODBUS_ERR_NONE        the call was successful and a packet was received
*                       MODBUS_ERR_TIMED_OUT   a packet was not received within the specified timeout
*                       MODBUS_ERR_NOT_MASTER  the channel is not a Master
*                       MODBUS_ERR_INVALID     an invalid error was detected
*
* Return(s)   : none.
*
* Caller(s)   : MBM_FCxx()  Modbus Master Functions
*
* Return(s)   : none.
*********************************************************************************************************
*/

void MB_OS_RxWait(MODBUS_CH *pch, CPU_INT16U *perr)
{
#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)
    os_err_t err;

    if (pch != (MODBUS_CH *)0)
    {
        if (pch->MasterSlave == MODBUS_MASTER)
        {
            err = os_sem_wait(&gs_mb_rx_sem[pch->Ch], pch->RxTimeout);

            switch (err)
            {
            case OS_ETIMEOUT:
                *perr = MODBUS_ERR_TIMED_OUT;
                break;
            case OS_EOK:
                *perr = MODBUS_ERR_NONE;
                break;
            default:
                *perr = MODBUS_ERR_INVALID;
                break;
            }
        }
        else
        {
            *perr = MODBUS_ERR_NOT_MASTER;
        }
    }
    else
    {
        *perr = MODBUS_ERR_NULLPTR;
    }

#else
    *perr = MODBUS_ERR_INVALID;
#endif
}

/*
*********************************************************************************************************
*                                            MB_OS_RxTask()
*
* Description : This task is created by MB_OS_Init() and waits for signals from either the Rx ISR(s) or
*               the RTU timeout timer(s) to indicate that a packet needs to be processed.
*
* Argument(s) : p_arg       This argument is not used.
*
* Return(s)   : none.
*
* Caller(s)   : This is a Task.
*
* Return(s)   : none.
*********************************************************************************************************
*/

static void MB_OS_RxTask(void *p_arg)
{
    os_err_t err;
    os_uint8_t byte;
    os_size_t msg_size;
    MODBUS_CH *pch;
    for (;;)
    {
        err = os_mq_recv(gs_mb_rx_mq_handle, &pch, sizeof(MODBUS_CH *), OS_WAIT_FOREVER, &msg_size);
        if (err != OS_EOK)
        {
            continue;
        }

        while (os_device_read_nonblock(pch->Handle, 0, &byte, 1) == 1)
        {
            MB_RxByte(pch, (CPU_INT08U)byte); /* invoke MB_RxByte() */
        }
    }
}
