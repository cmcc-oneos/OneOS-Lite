/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mb_os.h
 *
 * @brief       MODBUS OneOS LAYER INTERFACE
 *
 * @revision    V1.0.00
 * Date         Author          Notes
 * 2022-01-05   OneOS Team      First Version
 ***********************************************************************************************************************
 */
 
/*
*********************************************************************************************************
*                                              uC/Modbus
*                                       The Embedded Modbus Stack
*
*                    Copyright 2003-2020 Silicon Laboratories Inc. www.silabs.com
*
*                                 SPDX-License-Identifier: APACHE-2.0
*
*               This software is subject to an open source license and is distributed by
*                Silicon Laboratories Inc. pursuant to the terms of the Apache License,
*                    Version 2.0 available at www.apache.org/licenses/LICENSE-2.0.
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                               MODULE
*********************************************************************************************************
*/

#ifndef  MB_OS_MODULE_PRESENT
#define  MB_OS_MODULE_PRESENT


/*
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*/

#if 1
    #include <oneos_config.h>
    #include <os_types.h>
    #include <os_clock.h>
    #include <arch_interrupt.h>

    typedef            void        CPU_VOID;
//    typedef            char        CPU_CHAR;                        /*  8-bit character                                     */
    typedef       os_bool_t        CPU_BOOLEAN;                     /*  8-bit boolean or logical                            */
    typedef      os_uint8_t        CPU_INT08U;                      /*  8-bit unsigned integer                              */
//    typedef       os_int8_t        CPU_INT08S;                      /*  8-bit   signed integer                              */
    typedef     os_uint16_t        CPU_INT16U;                      /* 16-bit unsigned integer                              */
//    typedef      os_int16_t        CPU_INT16S;                      /* 16-bit   signed integer                              */
    typedef     os_uint32_t        CPU_INT32U;                      /* 32-bit unsigned integer                              */
//    typedef      os_int32_t        CPU_INT32S;                      /* 32-bit   signed integer                              */
//    typedef     os_uint64_t        CPU_INT64U;                      /* 64-bit unsigned integer                              */
//    typedef      os_int64_t        CPU_INT64S;                      /* 64-bit   signed integer                              */

    typedef            float       CPU_FP32;                        /* 32-bit floating point                                */
//    typedef            double      CPU_FP64;                        /* 64-bit floating point                                */

//    typedef  volatile  CPU_INT08U  CPU_REG08;                       /*  8-bit register                                      */
//    typedef  volatile  CPU_INT16U  CPU_REG16;                       /* 16-bit register                                      */
//    typedef  volatile  CPU_INT32U  CPU_REG32;                       /* 32-bit register                                      */
//    typedef  volatile  CPU_INT64U  CPU_REG64;                       /* 64-bit register                                      */

    #define  DEF_FALSE                                         0u
    #define  DEF_TRUE                                          1u

#if 0
    typedef  os_ubase_t                 CPU_SR;                     /* Defines   CPU status register size (see Note #3b).   */

    #define  CPU_CRITICAL_ENTER()  do { cpu_sr = os_irq_lock(); } while (0)          /* Disable   interrupts.                        */
    #define  CPU_CRITICAL_EXIT()   do { os_irq_unlock(cpu_sr);} while (0)          /* Re-enable interrupts.                        */

    #define OSTime          os_tick_get()
    #define OSCPUUsage      (OSStatTaskCPUUsage/100)
#endif

    #define MB_OS_CFG_RX_TASK_STK_SIZE OS_UCMODBUS_RX_TASK_STK
    #define MB_OS_CFG_RX_TASK_PRIO     OS_UCMODBUS_RX_TASK_PRIO
#else
/* TODO support uc/os api*/
#include  <cpu.h>
#include  <lib_def.h>
#endif


/*
*********************************************************************************************************
*                                               EXTERNS
*********************************************************************************************************
*/

#ifdef   MB_OS_MODULE
#define  MB_OS_EXT
#else
#define  MB_OS_EXT  extern
#endif


/*
*********************************************************************************************************
*                                               DEFINES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                             DATA TYPES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                          GLOBAL VARIABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                              MACRO'S
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                        CONFIGURATION ERRORS
*********************************************************************************************************
*/
#ifdef OS_CFG_Q_EN
#if      (OS_CFG_Q_EN == 0)
#error  "MODBUS Slave requires uC/OS-III Message Queue Services."
#endif
#endif

#ifndef  MB_OS_CFG_RX_TASK_PRIO
#error  "MODBUS Missing Rx Task's MB_OS_CFG_RX_TASK_PRIO."
#endif


#ifndef  MB_OS_CFG_RX_TASK_STK_SIZE
#error  "MODBUS Missing Rx Task's MB_OS_CFG_RX_TASK_STK_SIZE."
#endif

#ifdef OS_CFG_SEM_EN
#if      (MODBUS_CFG_MASTER_EN == DEF_ENABLED)
#if      (OS_CFG_SEM_EN        == 0          )
#error  "MODBUS Master requires uC/OS-III Semaphore Services."
#error  "... It needs at least MODBUS_CFG_MAX_CH semaphores."
#endif
#endif
#endif
/*
*********************************************************************************************************
*                                             MODULE END
*********************************************************************************************************
*/

#endif                                                          /* End of MB_OS module                                */

