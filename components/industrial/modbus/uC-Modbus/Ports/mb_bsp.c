/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mb_bsp.c
 *
 * @brief       MODBUS BOARD SUPPORT PACKAGE For OneOS Platform
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-05   OneOS Team      First Version
 ***********************************************************************************************************************
 */

/*
*********************************************************************************************************
*                                              uC/Modbus
*                                       The Embedded Modbus Stack
*
*                    Copyright 2003-2020 Silicon Laboratories Inc. www.silabs.com
*
*                                 SPDX-License-Identifier: APACHE-2.0
*
*               This software is subject to an open source license and is distributed by
*                Silicon Laboratories Inc. pursuant to the terms of the Apache License,
*                    Version 2.0 available at www.apache.org/licenses/LICENSE-2.0.
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*/
#include "../Source/mb.h"
#include <device.h>
#include <drv_cfg.h>
#include <os_types.h>

/*
*********************************************************************************************************
*                                               GLOBALS
*********************************************************************************************************
*/
#if (MODBUS_CFG_RTU_EN == DEF_ENABLED)
static os_hrtimer_t gs_rtu_timer;
#endif

extern os_err_t MB_RxDoneSignal(MODBUS_CH *pch);

static os_err_t mb_rx_cb(os_device_t *dev, struct os_device_cb_info *info)
{
    return MB_RxDoneSignal((MODBUS_CH *) dev->user_data);
}

/*
*********************************************************************************************************
*                                             MB_CommExit()
*
* Description : This function is called to terminate Modbus communications.  All Modbus channels are close.
*
* Argument(s) : none
*
* Return(s)   : none.
*
* Caller(s)   : MB_Exit()
*
* Note(s)     : none.
*********************************************************************************************************
*/

CPU_VOID MB_CommExit(CPU_VOID)
{
    os_uint8_t ch;
    os_device_t *device;
    MODBUS_CH *pch = &MB_ChTbl[0];

    for (ch = 0; ch < MODBUS_CFG_MAX_CH; ch++, pch++)
    {
        device = (os_device_t *)pch->Handle;

        if ((pch == OS_NULL) || (device == OS_NULL))
        {
            continue;
        }

        if (os_device_close(device) != OS_EOK)
        {
            os_kprintf("close COM%d error.\r\n", pch->PortNbr);
        }
    }
}

/*
*********************************************************************************************************
*                                           MB_CommPortCfg()
*
* Description : This function initializes the serial port to the desired baud rate and the UART will be
*               configured for N, 8, 1 (No parity, 8 bits, 1 stop).
*
* Argument(s) : pch        is a pointer to the Modbus channel
*               port_nbr   is the desired serial port number.  This argument allows you to assign a
*                          specific serial port to a sepcific Modbus channel.
*               baud       is the desired baud rate for the serial port.
*               parity     is the desired parity and can be either:
*
*                          MODBUS_PARITY_NONE
*                          MODBUS_PARITY_ODD
*                          MODBUS_PARITY_EVEN
*
*               bits       specifies the number of bit and can be either 7 or 8.
*               stops      specifies the number of stop bits and can either be 1 or 2
*
* Return(s)   : none.
*
* Caller(s)   : MB_CfgCh()
*
* Note(s)     : none.
*********************************************************************************************************
*/

CPU_VOID MB_CommPortCfg(MODBUS_CH *pch,
                        CPU_INT08U port_nbr,
                        CPU_INT32U baud,
                        CPU_INT08U bits,
                        CPU_INT08U parity,
                        CPU_INT08U stops)
{
    char name[OS_NAME_MAX];
    os_device_t *device;
    struct os_serial_device *serial_dev;
    struct os_device_cb_info cb_info = {
        .type = OS_DEVICE_CB_TYPE_RX,
        .data = OS_NULL,
        .cb = mb_rx_cb,
    };

    if (pch == (MODBUS_CH *)0)
    {
        return;
    }

    pch->PortNbr = port_nbr;
    pch->BaudRate = baud;
    pch->Parity = parity;
    pch->Bits = bits;
    pch->Stops = stops;
    pch->Handle = OS_NULL;
    os_snprintf(name, sizeof(name) - 1, "lpuart%d", port_nbr);
    device = os_device_find(name);

    if (device == OS_NULL)
    {
        os_snprintf(name, sizeof(name) - 1, "uart%d", port_nbr);
        device = os_device_find(name);
    }

    if (device == OS_NULL)
    {
        os_kprintf("not found COM%d.\r\n", port_nbr);
        return;
    }

    serial_dev = (struct os_serial_device *)device;
    serial_dev->config.baud_rate = baud;
    serial_dev->config.data_bits = (bits == 7) ? DATA_BITS_7 : DATA_BITS_8;
    serial_dev->config.stop_bits = (stops == 2) ? STOP_BITS_2 : STOP_BITS_1;

    switch (parity)
    {
    case MODBUS_PARITY_ODD:
        serial_dev->config.parity = PARITY_ODD;
        break;

    case MODBUS_PARITY_EVEN:
        serial_dev->config.parity = PARITY_EVEN;
        break;

    case MODBUS_PARITY_NONE:
    default:
        serial_dev->config.parity = PARITY_NONE;
        break;
    }

    serial_dev->config.rx_bufsz = 128;
    serial_dev->config.tx_bufsz = 128;

    if (os_device_open(device) != OS_EOK)
    {
        os_kprintf("COM%d init failed.\r\n", port_nbr);
        return;
    }

    os_device_control(device, OS_DEVICE_CTRL_SET_CB, &cb_info);
    pch->Handle = device;
    device->user_data = pch;
}

/*
*********************************************************************************************************
*                                                MB_Tx()
*
* Description : This function is called to start transmitting a packet to a modbus channel.
*
* Argument(s) : pch      Is a pointer to the Modbus channel's data structure.
*
* Return(s)   : none.
*
* Caller(s)   : MB_ASCII_Tx(),
*               MB_RTU_Tx().
*
* Note(s)     : none.
*********************************************************************************************************
*/
/* combine MB_Tx() and MB_TxByte() */
void MB_Tx(MODBUS_CH *pch)
{
    os_device_t *device;
    device = pch->Handle;
    pch->TxBufPtr = &pch->TxBuf[0];

    if (pch->TxBufByteCtr > 0)
    {
#if (MODBUS_CFG_MASTER_EN == DEF_ENABLED)

        if (pch->MasterSlave == MODBUS_MASTER)
        {
#if (MODBUS_CFG_RTU_EN == DEF_ENABLED)
            pch->RTU_TimeoutEn = MODBUS_FALSE; /* Disable RTU timeout timer until we start receiving */
#endif
            pch->RxBufByteCtr = 0; /* Flush Rx buffer                                    */
        }

#endif
        os_device_write_nonblock(device, 0, pch->TxBufPtr, pch->TxBufByteCtr); /* send a message */
        /* end of transmission */
        pch->TxCtr = pch->TxBufByteCtr;
        pch->TxBufByteCtr = 0;
    }
}

/*
*********************************************************************************************************
*                                       MB_RTU_Tmr_Timeout()
*
* Description : This function handles the case when the RTU timeout timer expires.
*
* Arguments   : none.
*
* Returns     : none.
*
* Caller(s)   : This is a Timer ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/

#if (MODBUS_CFG_RTU_EN == DEF_ENABLED)
OS_USED static void MB_RTU_Tmr_Timeout(void *param)
{
    MB_RTU_TmrCtr++;    /* Indicate that we had activities on this interrupt. */
    MB_RTU_TmrUpdate(); /* Check for RTU timers that have expired             */
}
#endif

/*
*********************************************************************************************************
*                                           MB_RTU_TmrInit()
*
* Description : This function is called to initialize the RTU timeout timer.
*
* Argument(s) : freq          Is the frequency of the modbus RTU timer interrupt.
*
* Return(s)   : none.
*
* Caller(s)   : MB_Init().
*
* Note(s)     : none.
*********************************************************************************************************
*/

#if (MODBUS_CFG_RTU_EN == DEF_ENABLED)
CPU_VOID MB_RTU_TmrInit(void)
{
    os_uint16_t rtu_timer_freq;
    os_hrtimer_t *const hrtimer = &gs_rtu_timer;

    if (!os_hrtimer_stoped(hrtimer))
    {
        os_hrtimer_stop(hrtimer);
    }

    rtu_timer_freq = MB_RTU_Freq;

    if (rtu_timer_freq > 1000)
    {
        os_kprintf("MB RTU Timer Freq out of range.\r\n");
        rtu_timer_freq = 1000;
    }

    hrtimer->timeout_func = MB_RTU_Tmr_Timeout;
    hrtimer->period_nsec = (NSEC_PER_SEC / rtu_timer_freq);
    os_hrtimer_start(hrtimer);
    MB_RTU_TmrResetAll(); /* Reset all the RTU timers, we changed freq. */
}
#endif

/*
*********************************************************************************************************
*                                           MB_RTU_TmrExit()
*
* Description : This function is called to disable the RTU timeout timer.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : MB_Exit()
*
* Note(s)     : none.
*********************************************************************************************************
*/

#if (MODBUS_CFG_RTU_EN == DEF_ENABLED)
CPU_VOID MB_RTU_TmrExit(CPU_VOID)
{
    os_hrtimer_t *const hrtimer = &gs_rtu_timer;

    if (!os_hrtimer_stoped(hrtimer))
    {
        os_hrtimer_stop(hrtimer);
    }
}
#endif
