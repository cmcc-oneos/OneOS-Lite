/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        lvgl.c
 *
 * @brief       This file implements device and task init.
 *
 * @revision
 * Date         Author          Notes
 * 2022-2-22   OneOS team      First Version
 ***********************************************************************************************************************
 */

#include "lv_port_init.h"

void gui_ex(void)
{
#ifdef OS_USING_GUI_EX_SCROLL
    void lv_example_scroll_1(void);
    lv_example_scroll_1();
#elif defined (OS_USING_GUI_EX_ANIM)
    void lv_example_anim_1(void);
    lv_example_anim_1();
#elif defined (OS_USING_GUI_EX_EVENT)
    void lv_example_event_1(void);
    lv_example_event_1();
#elif defined (OS_USING_GUI_EX_START)
    void lv_example_get_started_1(void);
    lv_example_get_started_1();
#elif defined (OS_USING_GUI_EX_FLEX)
    void lv_example_flex_1(void);
    lv_example_flex_1();
#elif defined (OS_USING_GUI_EX_GRID)
    void lv_example_grid_1(void);
    lv_example_grid_1();
#elif defined (OS_USING_GUI_EX_QRCODE)  
    void lv_example_qrcode_1(void);
    lv_example_qrcode_1();
#endif
#if 0
    void lv_example_bmp_1(void);
    lv_example_bmp_1();

    void lv_example_ffmpeg_1(void);
    lv_example_ffmpeg_1();

    void lv_example_freetype_1(void);
    lv_example_freetype_1();

    void lv_example_gif_1(void);
    lv_example_gif_1();

    void lv_example_png_1(void);
    lv_example_png_1();

    void lv_example_rlottie_1(void);
    lv_example_rlottie_1();

    void lv_example_sjpg_1(void);
    lv_example_sjpg_1();

    void lv_example_gridnav_1(void);
    lv_example_gridnav_1();

    void lv_example_monkey_1(void);
    lv_example_monkey_1();
#endif
}
