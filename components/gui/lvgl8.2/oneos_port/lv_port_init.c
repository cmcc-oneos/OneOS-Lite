/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        lvgl.c
 *
 * @brief       This file implements device and task init.
 *
 * @revision
 * Date         Author          Notes
 * 2022-2-22   OneOS team      First Version
 ***********************************************************************************************************************
 */

#include "lv_port_init.h"

static void gui_thread(void *parameter)
{

#ifdef OS_USING_GUI_LVGL_DEMO
    gui_demo();
#elif defined (OS_USING_GUI_LVGL_EXAMPLES)
    gui_ex();
#endif
    os_kprintf("This is gui task!\r\n");

    while (1)
    {

        lv_task_handler();
        os_task_msleep(5);
    }
}

static int os_gui_init(void)
{
    /* gui lib */
    lv_init();

    /* gui hardware */
    struct lvgl_device *lvgl_dev = os_calloc(1, sizeof(struct lvgl_device));
    OS_ASSERT(lvgl_dev);

    disp_init(lvgl_dev);
    input_init(lvgl_dev);

    /* gui thread */
#if defined(OS_USING_GUI_LVGL_EXAMPLES) || defined(OS_USING_GUI_LVGL_DEMO)
    os_task_t *task = os_task_create("gui", gui_thread, NULL, 4096, OS_TASK_PRIORITY_MAX/3);
    OS_ASSERT(task);
    os_task_startup(task);
#endif

    return 0;
}
OS_ENV_INIT(os_gui_init, OS_INIT_SUBLEVEL_HIGH);
