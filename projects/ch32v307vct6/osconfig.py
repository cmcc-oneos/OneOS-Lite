import os
import platform


# toolchains options
ARCH        ='RISCV'
CPU         ='ch32v307vct6'
CROSS_TOOL  ='gcc'

# bsp lib config
BSP_LIBRARY_TYPE = None

def cdk_project_find():
    cwd  = os.getcwd()
    dirs = os.listdir(os.getcwd())
    path = ''
    for i in dirs:
        if os.path.splitext(i)[1] == ".cdkproj":
            path = i;
    return os.path.join(cwd, path)

if platform.system() == "Windows":
    def cdk_path_find():
        cdkPath = ''
        aReg = ConnectRegistry(None, HKEY_CLASSES_ROOT)     
        aKey = OpenKey(aReg, r"cdkproj_auto_file\\shell\\open\\command")     
        value = QueryValueEx(aKey, "")     
        substring = value[0]     
        pos = substring.find("cdk.exe")     
        cdkPath = substring[:pos]     
        if cdkPath[0]=="\"":         
            cdkPath = cdkPath[1:pos] 
        return cdkPath


if os.getenv('OS_CC'):
    CROSS_TOOL = os.getenv('OS_CC')
if os.getenv('OS_ROOT'):
    OS_ROOT = os.getenv('OS_ROOT')

# cross_tool provides the cross compiler
# COMPILER_PATH is the compiler execute path, for example, CodeSourcery, Keil MDK, IAR
if  CROSS_TOOL == 'gcc':
    COMPILER    = 'gcc'
    CDK_PATH    = r'D:\C-Sky\CDK\\'
    CDK_TOOLCHAIN_PATH = CDK_PATH + 'CSKY\\MinGW\\csky-abiv2-elf-toolchain\\bin'
    CDK_MINGW_PATH     = CDK_PATH + 'CSKY\\MinGW\\csky-abiv2-elf-toolchain\\bin\\..\\..\\bin'
    CDK_MSYS_PATH      = CDK_PATH + 'CSKY\\MinGW\\csky-abiv2-elf-toolchain\\bin\\..\\..\\msys\\1.0\\bin'
    CDK_MSYS_CMD_PATH  = CDK_PATH + 'CSKY\\MinGW\\csky-abiv2-elf-toolchain\\bin\\..\\..\\msys\\1.0\\local\\share\\bashdb\\command'
    COMPILER_PATH      = CDK_TOOLCHAIN_PATH
    CDK_PROJECT= cdk_project_find();
    CDK_ACTION = CDK_PATH + 'cdk-make.exe -p ' + CDK_PROJECT +' -d build -c BuildSet > ' + os.getcwd() + '\\build.log'
    CDK_ENV = CDK_TOOLCHAIN_PATH + ';' + CDK_MINGW_PATH + ';' + CDK_MSYS_PATH + ';' + CDK_MSYS_CMD_PATH + ';'
    os.environ["PATH"] += CDK_ENV
    os.environ["OS_EXEC_PATH"] =  COMPILER_PATH
    os.environ["OS_CDK_PROJECT"] = CDK_PROJECT
else:
    print('Please make sure your toolchains is GNU GCC!')
    exit(0)

if CROSS_TOOL == 'gcc' and os.getenv('OS_EXEC_PATH'):
    COMPILER_PATH = os.getenv('OS_EXEC_PATH')


if COMPILER == 'gcc':
    # "BUILD" can be: 'debug_O0', 'release_O2' or 'release_Os'
    BUILD = 'debug_O0'
else:
    BUILD = 'debug'

if COMPILER == 'gcc':
    if platform.system() == "Windows":
        PREFIX  = 'csky-elfabiv2-'
    else:
        PREFIX  = 'csky-abiv2-elf-'

    # toolchains
    CC   = PREFIX + 'gcc'
    AS   = PREFIX + 'gcc'
    AR   = PREFIX + 'ar'
    CXX  = PREFIX + 'g++'
    LINK = PREFIX + 'g++'
    RESULT_SUFFIX = 'elf'
    SIZE    = PREFIX + 'size'
    OBJDUMP = PREFIX + 'objdump'
    OBJCPY  = PREFIX + 'objcopy'
    STRIP   = PREFIX + 'strip'

    if CPU == 'ck803':
        DEVICE = ' -mcpu=ck803'

    CFLAGS  = DEVICE + '  -DCONFIG_CPU_CK803  -c -fdata-sections -ffunction-sections  -Wall -mistack -mlittle-endian -mno-required-printf'
    AFLAGS  = ' -c' + DEVICE + '  -mlittle-endian -save-temps=obj -I. ' #-x assembler-with-cpp
    LFLAGS  = DEVICE + ' -mlittle-endian -Wl,--gc-sections,-Map=oneos.map -nostartfiles -static-libgcc -T board/linker_scripts/fw.ld' #-cref,-u,reset_handler -z,max-page-size=0x4
    CPATH   = ''
    LPATH   = ''

    if BUILD == 'debug_O0':
        CFLAGS += ' -O0 -gdwarf-2 -g'
        AFLAGS += ' -gdwarf-2'
    elif BUILD == 'release_O2':
        CFLAGS += ' -O2'
    else:
        CFLAGS += ' -Os'

        
    CXXFLAGS = CFLAGS 

    #module build params
    M_CFLAGS = DEVICE + ' -EL -G0 -O2 -mno-abicalls -fno-common -fno-exceptions -fno-omit-frame-pointer -mlong-calls -fno-pic '
    M_CXXFLAGS = M_CFLAGS
    M_LFLAGS = DEVICE + ' -EL -r -Wl,--gc-sections,-z,max-page-size=0x4' +\
                                    ' -nostartfiles -static-libgcc'
    M_POST_ACTION = STRIP + ' -R .hash $TARGET\n' + SIZE + ' $TARGET \n'

    DUMP_ACTION = OBJDUMP + ' -S $TARGET > oneos.asm\n' #-S -D --> non-use
    POST_ACTION = OBJCPY + ' -O binary $TARGET oneos.bin\n' + SIZE + ' $TARGET \n'

