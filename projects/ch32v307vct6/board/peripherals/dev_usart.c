#include "oneos_config.h"
#include <driver.h>
#include <bus/bus.h>

#include "ch32v30x.h"
#include "ch32v30x_usart.h"
#ifdef BSP_USING_USART
#include "drv_usart.h"
#ifdef BSP_USING_USART1
/* USART1 TX-->A.9  RX-->A.10 */
const struct ch32_usart_info usart1_info = {
    .tx_pin_port = GPIOA,
    .tx_pin_info =
        {
            .GPIO_Pin   = GPIO_Pin_9,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_Mode  = GPIO_Mode_AF_PP,
        },

    .rx_pin_port = GPIOA,
    .rx_pin_info =
        {
            .GPIO_Pin   = GPIO_Pin_10,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_Mode  = GPIO_Mode_IN_FLOATING,
        },

    .gpio_PeriphClock  = RCC_APB2PeriphClockCmd,
    .gpio_Periph       = RCC_APB2Periph_GPIOA,
    .usart_PeriphClock = RCC_APB2PeriphClockCmd,
    .usart_Periph      = RCC_APB2Periph_USART1,

    .husart   = USART1,
    .usart_it = USART_IT_RXNE,

    .usart_def_cfg =
        {
            .USART_BaudRate            = 115200,
            .USART_WordLength          = USART_WordLength_8b,
            .USART_StopBits            = USART_StopBits_1,
            .USART_Parity              = USART_Parity_No,
            .USART_HardwareFlowControl = USART_HardwareFlowControl_None,
            .USART_Mode                = USART_Mode_Tx | USART_Mode_Rx,
        },

    .usart_nvic_cfg =
        {
            .NVIC_IRQChannel                   = USART1_IRQn,
            .NVIC_IRQChannelPreemptionPriority = 0,
            .NVIC_IRQChannelSubPriority        = 1,
            .NVIC_IRQChannelCmd                = ENABLE,
        },
};

OS_HAL_DEVICE_DEFINE("USART_TypeDef", "usart1", usart1_info);
#endif

#ifdef BSP_USING_USART2
/* USART2 TX-->A.2   RX-->A.3 */
const struct ch32_usart_info usart2_info = {
    .tx_pin_port = GPIOA,
    .tx_pin_info =
        {
            .GPIO_Pin   = GPIO_Pin_2,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_Mode  = GPIO_Mode_AF_PP,
        },

    .rx_pin_port = GPIOA,
    .rx_pin_info =
        {
            .GPIO_Pin   = GPIO_Pin_3,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_Mode  = GPIO_Mode_IN_FLOATING,
        },

    .gpio_PeriphClock  = RCC_APB2PeriphClockCmd,
    .gpio_Periph       = RCC_APB2Periph_GPIOA,
    .usart_PeriphClock = RCC_APB1PeriphClockCmd,
    .usart_Periph      = RCC_APB1Periph_USART2,

    .husart   = USART2,
    .usart_it = USART_IT_RXNE,
    .usart_def_cfg =
        {
            .USART_BaudRate            = 115200,
            .USART_WordLength          = USART_WordLength_8b,
            .USART_StopBits            = USART_StopBits_1,
            .USART_Parity              = USART_Parity_No,
            .USART_HardwareFlowControl = USART_HardwareFlowControl_None,
            .USART_Mode                = USART_Mode_Tx | USART_Mode_Rx,
        },

    .usart_nvic_cfg =
        {
            .NVIC_IRQChannel                   = USART2_IRQn,
            .NVIC_IRQChannelPreemptionPriority = 0,
            .NVIC_IRQChannelSubPriority        = 2,
            .NVIC_IRQChannelCmd                = ENABLE,
        },
};

OS_HAL_DEVICE_DEFINE("USART_TypeDef", "usart2", usart2_info);
#endif

#ifdef BSP_USING_USART3
/* USART3 TX-->B.10  RX-->B.11 */
const struct ch32_usart_info usart2_info = {
    .tx_pin_port = GPIOB,
    .tx_pin_info =
        {
            .GPIO_Pin   = GPIO_Pin_10,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_Mode  = GPIO_Mode_AF_PP,
        },

    .rx_pin_port = GPIOB,
    .rx_pin_info =
        {
            .GPIO_Pin   = GPIO_Pin_11,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_Mode  = GPIO_Mode_IN_FLOATING,
        },

    .gpio_PeriphClock  = RCC_APB2PeriphClockCmd,
    .gpio_Periph       = RCC_APB2Periph_GPIOB,
    .usart_PeriphClock = RCC_APB1PeriphClockCmd,
    .usart_Periph      = RCC_APB1Periph_USART3,

    .husart   = USART3,
    .usart_it = USART_IT_RXNE,

    .usart_def_cfg =
        {
            .USART_BaudRate            = 115200,
            .USART_WordLength          = USART_WordLength_8b,
            .USART_StopBits            = USART_StopBits_1,
            .USART_Parity              = USART_Parity_No,
            .USART_HardwareFlowControl = USART_HardwareFlowControl_None,
            .USART_Mode                = USART_Mode_Tx | USART_Mode_Rx,
        },

    .usart_nvic_cfg =
        {
            .NVIC_IRQChannel                   = USART3_IRQn,
            .NVIC_IRQChannelPreemptionPriority = 0,
            .NVIC_IRQChannelSubPriority        = 3,
            .NVIC_IRQChannelCmd                = ENABLE,
        },
};

OS_HAL_DEVICE_DEFINE("USART_TypeDef", "usart3", usart3_info);
#endif

#endif
