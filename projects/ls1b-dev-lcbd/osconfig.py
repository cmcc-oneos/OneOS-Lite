import os

# toolchains options
ARCH       = 'mips'
CPU        = 'gs232'
CROSS_TOOL = 'gcc'

# bsp lib config
BSP_LIBRARY_TYPE = None

if os.getenv('OS_CC'):
    CROSS_TOOL = os.getenv('OS_CC')
if os.getenv('OS_ROOT'):
    OS_ROOT = os.getenv('OS_ROOT')

# cross_tool provides the cross compiler
if  CROSS_TOOL == 'gcc':
    COMPILER    = 'gcc'
    COMPILER_PATH   = r'D:\Program Files (x86)\mgc\bin'

#if CROSS_TOOL == 'gcc' and os.getenv('OS_EXEC_PATH'):
#    COMPILER_PATH = os.getenv('OS_EXEC_PATH')


if COMPILER == 'gcc':
    # "BUILD" can be: 'debug_O0', 'release_O2' or 'release_Os'
    BUILD = 'debug_O0'
else:
    BUILD = 'debug'

if COMPILER == 'gcc':
    # toolchains
    PREFIX  = 'mips-sde-elf-'
    CC      = PREFIX + 'gcc'
    AS      = PREFIX + 'gcc'
    AR      = PREFIX + 'ar'
    LINK    = PREFIX + 'gcc'
    RESULT_SUFFIX = 'elf'
    SIZE    = PREFIX + 'size'
    OBJDUMP = PREFIX + 'objdump'
    OBJCPY  = PREFIX + 'objcopy'
    STRIP   = PREFIX + 'strip'

DEVICE = ' -mips32 -msoft-float -mfp32'
CFLAGS = DEVICE + ' -EL -G0 -mno-abicalls -fno-pic -fno-builtin -fno-exceptions -ffunction-sections -fomit-frame-pointer -std=c11'
AFLAGS = ' -c' + DEVICE + ' -EL -fno-pic -fno-builtin -mno-abicalls -x assembler-with-cpp'
LFLAGS = DEVICE + ' -nostartfiles -EL -Wl,--gc-sections,-Map=oneos.map,-cref,-u,Reset_Handler -T board\linker_scripts\ls1b_ram.lds -L $OS_ROOT/drivers/link/'

CPATH = ''
LPATH = ''

    if BUILD == 'debug_O0':
        CFLAGS += ' -O0 -gdwarf-2 -g'
        AFLAGS += ' -gdwarf-2'
    elif BUILD == 'release_O2':
        CFLAGS += ' -O2'
    else:
        CFLAGS += ' -Os'


DUMP_ACTION = OBJDUMP + ' -D -S $TARGET > oneos.asm\n'
POST_ACTION = OBJCPY + ' -R .reserved_ram -O binary $TARGET oneos.bin\n' + SIZE + ' $TARGET \n'
