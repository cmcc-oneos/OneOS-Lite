/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        fal_cfg.c
 *
 * @brief       Flash abstract layer partition definition
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board_memory.h"

static const fal_part_info_t fal_part_info[] = 
{
    /*       part,          flash,       addr,       size,                       lock */
    { IMXRT_FLASH_APP_NAME, "onchip_flash", IMXRT_FLASH_APP_START,  IMXRT_FLASH_APP_SIZE,  FAL_PART_INFO_FLAGS_LOCKED},
    {IMXRT_FLASH_USER_NAME, "onchip_flash", IMXRT_FLASH_USER_START, IMXRT_FLASH_USER_SIZE, FAL_PART_INFO_FLAGS_UNLOCKED},
#ifdef BSP_USING_I2C_AT24CXX
    {             "at24", "at24cxx_eeprom", 0, BSP_AT24CXX_SIZE, FAL_PART_INFO_FLAGS_UNLOCKED},
#endif
};
