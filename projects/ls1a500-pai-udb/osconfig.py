import os

# toolchains options
ARCH       = 'loongarch64'
CPU        = 'gs264l'
CROSS_TOOL = 'gcc'

# bsp lib config
BSP_LIBRARY_TYPE = None

if os.getenv('OS_CC'):
    CROSS_TOOL = os.getenv('OS_CC')
if os.getenv('OS_ROOT'):
    OS_ROOT = os.getenv('OS_ROOT')

# cross_tool provides the cross compiler
if  CROSS_TOOL == 'gcc':
    COMPILER    = 'gcc'
    COMPILER_PATH   = '/work/ls2k500/loongarch64-linux-gnu-2021-06-19/bin/'

if CROSS_TOOL == 'gcc' and os.getenv('OS_EXEC_PATH'):
    COMPILER_PATH = os.getenv('OS_EXEC_PATH')


if COMPILER == 'gcc':
    # "BUILD" can be: 'debug_O0', 'release_O2' or 'release_Os'
    BUILD = 'debug_O0'
else:
    BUILD = 'debug'

if COMPILER == 'gcc':
    # toolchains
    PREFIX  = 'loongarch64-linux-gnu-'
    CC      = PREFIX + 'gcc'
    AS      = PREFIX + 'gcc'
    AR      = PREFIX + 'ar'
    LINK    = PREFIX + 'gcc'
    RESULT_SUFFIX = 'elf'
    SIZE    = PREFIX + 'size'
    OBJDUMP = PREFIX + 'objdump'
    OBJCPY  = PREFIX + 'objcopy'
    STRIP   = PREFIX + 'strip'

DEVICE = ' -Wall -static -msoft-float -march=loongarch64'
CFLAGS = DEVICE + ' -EL -G0 -mno-gpopt -mabi=lp64 -fno-pic -fno-builtin -fno-exceptions -ffunction-sections -fomit-frame-pointer'
AFLAGS = ' -c' + DEVICE + '  -EL -G0 -mno-gpopt -mabi=lp64 -fno-pic -fno-builtin -fno-exceptions -ffunction-sections -fomit-frame-pointer'
LFLAGS = DEVICE + ' -nostartfiles -EL -Wl,--wrap=rand,--wrap=srand,--wrap=strncpy,--wrap=strncmp,--wrap=strcpy,--wrap=memset,--wrap=memcpy,--gc-sections,-Map=oneos.map,-cref,-u,_start -T board/linker_scripts/link.lds -L $OS_ROOT/drivers/link/'

CPATH = ''
LPATH = ''

    if BUILD == 'debug_O0':
        CFLAGS += ' -O0 -gdwarf-2 -g'
        AFLAGS += ' -gdwarf-2'
    elif BUILD == 'release_O2':
        CFLAGS += ' -O2'
    else:
        CFLAGS += ' -Os'


DUMP_ACTION = OBJDUMP + ' -D -S $TARGET > oneos.asm\n'
POST_ACTION = OBJCPY + ' -O binary $TARGET oneos.bin\n' + SIZE + ' $TARGET \n'
