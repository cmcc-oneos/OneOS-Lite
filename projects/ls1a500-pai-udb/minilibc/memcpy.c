/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        memcpy.c
 *
 * @brief       This file provides C library function memcpy.
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-20   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <string.h>

/**
 ***********************************************************************************************************************
 * @brief           This function will copy memory content from source address to destination address.
 *
 * @param[out]      dst             The address of destination memory.
 * @param[in]       src             The address of source memory.
 * @param[in]       count           The copied length.
 *
 * @return          The address of destination memory.
 ***********************************************************************************************************************
 */
void *__wrap_memcpy(void *dst, const void *src, unsigned long count)
{
    char *dst_tmp;
    const char *src_tmp;

    dst_tmp = (char *)dst;
    src_tmp = (const char *)src;

    while (count--)
    {
        *dst_tmp = *src_tmp;
        dst_tmp++;
        src_tmp++;
    }

    return dst;
}

