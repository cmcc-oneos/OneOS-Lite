/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        memcmp.c
 *
  * @brief       This file provides C library function memcmp.
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-20   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <string.h>

/**
 ***********************************************************************************************************************
 * @brief           This function will compare two areas of memory.
 *
 * @param[in]       buf1           One area of memory.
 * @param[in]       buf1           Another area of memory.
 * @param[in]       count          The size of the area.
 *
 * @return          The compared result.
 * @retval          0               Two areas are equal.
 * @retval          else            Two areas are not equal.
 ***********************************************************************************************************************
 */
int memcmp(const void *buf1, const void *buf2, unsigned long count)
{
    const char *buf1_tmp;
    const char *buf2_tmp;
    int ret;

    buf1_tmp = (const char *)buf1;
    buf2_tmp = (const char *)buf2;
    ret = 0;

    while ((count--) && (!ret))
    {
        ret = *buf1_tmp - *buf2_tmp;
        buf1_tmp++;
        buf2_tmp++;
    }

    return ret;
}

