#!armclang --target=arm-arm-none-eabi -mcpu=cortex-m7 -E -x c
/*
** ###################################################################
**     Processors:          MIMXRT1052CVJ5B
**                          MIMXRT1052CVL5B
**                          MIMXRT1052DVJ6B
**                          MIMXRT1052DVL6B
**
**     Compiler:            Keil ARM C/C++ Compiler
**     Reference manual:    IMXRT1050RM Rev.2.1, 12/2018 | IMXRT1050SRM Rev.2
**     Version:             rev. 1.0, 2018-09-21
**     Build:               b191015
**
**     Abstract:
**         Linker file for the Keil ARM C/C++ Compiler
**
**     Copyright 2016 Freescale Semiconductor, Inc.
**     Copyright 2016-2019 NXP
**     All rights reserved.
**
**     SPDX-License-Identifier: BSD-3-Clause
**
**     http:                 www.nxp.com
**     mail:                 support@nxp.com
**NDEBUG, CPU_MIMXRT1052DVL6B, XIP_BOOT_HEADER_ENABLE=1, XIP_EXTERNAL_FLASH=1,XIP_BOOT_HEADER_DCD_ENABLE=1,SKIP_SYSCLK_INIT, DATA_SECTION_IS_CACHEABLE=1,SERIAL_PORT_TYPE_UART=1
** ###################################################################
*/

#if (defined(__ram_vector_table__))
  #define __ram_vector_table_size__    0x00000400
#else
  #define __ram_vector_table_size__    0x00000000
#endif

#define m_flash_config_start           0x60000000
#define m_flash_config_size            0x00001000

#define m_ivt_start                    0x60001000
#define m_ivt_size                     0x00001000

#define m_interrupts_start             0x60002000
#define m_interrupts_size              0x00000400

#define m_text_start                   0x60002400
#define m_text_size                    0x03FFDC00

#define m_interrupts_ram_start         0x20000000
#define m_interrupts_ram_size          __ram_vector_table_size__

#define  m_data_start                  (m_interrupts_ram_start + m_interrupts_ram_size)
#define  m_data_size                   (0x00020000 - m_interrupts_ram_size)

#define m_data2_start                  0x20200000
#define m_data2_size                   0x00040000

/* Sizes */
#if (defined(__stack_size__))
  #define Stack_Size                   __stack_size__
#else
  #define Stack_Size                   0x0400
#endif

#if (defined(__heap_size__))
  #define Heap_Size                    __heap_size__
#else
  #define Heap_Size                    0x0400
#endif



;ITCM中的主体代码
#define m_ram_text_start               0x00000400
#define m_ram_text_size                0x0001FC00

#define m_ram_interrupts_start         0x00000000
#define m_ram_interrupts_size          0x00000400

;SDRAM中的主体代码
#define m_sdram_text_start             0x80000400
#define m_sdram_text_size              0x003FFC00

#define m_sdram_interrupts_start       0x80000000
#define m_sdram_interrupts_size        0x00000400


#define OS_HEAP_SIZE (m_data_size-ImageLength(RW_m_data)-Heap_Size-Stack_Size)


#if defined(XIP_BOOT_HEADER_ENABLE) && (XIP_BOOT_HEADER_ENABLE == 1)
LR_m_text m_flash_config_start m_text_start+m_text_size-m_flash_config_start {   ; load region size_region
  RW_m_config_text m_flash_config_start FIXED m_flash_config_size { ; load address = execution address
    * (.boot_hdr.conf, +FIRST)
  }

  RW_m_ivt_text m_ivt_start FIXED m_ivt_size { ; load address = execution address
    * (.boot_hdr.ivt, +FIRST)
    * (.boot_hdr.boot_data)
    * (.boot_hdr.dcd_data)
  }
#else
LR_m_text m_interrupts_start m_text_start+m_text_size-m_interrupts_start {   ; load region size_region
#endif
  VECTOR_ROM m_interrupts_start FIXED m_interrupts_size { ; load address = execution address
    * (.isr_vector,+FIRST)
  }
  
;    ER_m_text m_text_start m_text_size { ; load address = execution address
;    * (InRoot$$Sections)
;    * (FSymTab)
;    * (AtestTcTab)
;    * (.init_call*)
;    * (at_cmd_tab)
;    * (driver_table)
;    * (device_table)
;    * (MSymTab)
;    * (fal_flash_table)
;    .ANY (+RO)
;  }
  
  
  ; * (InRoot$$Sections)是__main中从加载域复制代码到执行域的程序scatter_copy，
  ; 启动时运行的startup_mimxrt1052t和system_mimxrt1052中的代码要存放在FLASH中
  ER_m_text m_text_start m_text_size { ; load address = execution address
    * (InRoot$$Sections)
    startup_MIMXRT1052_arm.o(+RO)
    system_MIMXRT1052.o(+RO)
  } 

;ITCM
  ;	EMPTY表示这段空间留空，防止其它应用占用或编译提示warning
  VECTOR_RAM m_ram_interrupts_start EMPTY m_ram_interrupts_size { ;execution address
   
   ;这部分内容由board.c文件中的CopyAndUseRAMVectorTable函数从VECTOR_ROM中复制得到
   ;得到RAM版本的中断向量表
  }
  
  ;存放主体程序的ITCM空间，由__main函数从FLASH中加载
  ER_m_ram_text m_ram_text_start m_ram_text_size { ;execution address
    * (FSymTab)
    * (AtestTcTab)
    * (.init_call*)
    * (at_cmd_tab)
    * (driver_table)
    * (device_table)
    * (MSymTab)
    * (fal_flash_table)
    .ANY (+RO)
  }  
  
;SDRAM
;  ;	EMPTY表示这段空间留空，防止其它应用占用或编译提示warning
;  VECTOR_RAM m_sdram_interrupts_start EMPTY m_sdram_interrupts_size { ;execution address
;   
;   ;这部分内容由board.c文件中的CopyAndUseRAMVectorTable函数从VECTOR_ROM中复制得到
;   ;得到SDRAM版本的中断向量表
;  }
;  
;  ;存放主体程序的SDRAM空间，由__main函数从FLASH中加载
;  ER_m_ram_text m_sdram_text_start m_sdram_text_size { ;execution address
;    * (FSymTab)
;    * (AtestTcTab)
;    * (.init_call*)
;    * (at_cmd_tab)
;    * (driver_table)
;    * (device_table)
;    * (MSymTab)
;    * (fal_flash_table)
;    .ANY (+RO)
;  }
  
;#if (defined(__ram_vector_table__))
;  VECTOR_RAM m_interrupts_ram_start EMPTY m_interrupts_ram_size {
;  }
;#else
;  VECTOR_RAM m_interrupts_start EMPTY 0 {
;  }
;#endif
  
  RW_m_data m_data_start m_data_size-Stack_Size-Heap_Size { ; RW data
    .ANY (+RW +ZI)
    * (RamFunction)
    * (NonCacheable.init)
    * (*NonCacheable)
  }
  ARM_LIB_HEAP +0 EMPTY Heap_Size {    ; Heap region growing up
  }
  OS_HEAP +0 EMPTY OS_HEAP_SIZE{
  }
  ARM_LIB_STACK m_data_start+m_data_size EMPTY -Stack_Size { ; Stack region growing down
  }
  RW_m_ncache m_data2_start EMPTY 0 {
  }
  RW_m_ncache_unused +0 EMPTY m_data2_size-ImageLength(RW_m_ncache) { ; Empty region added for MPU configuration
  }
}