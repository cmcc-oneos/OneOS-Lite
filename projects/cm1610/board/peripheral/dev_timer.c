/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_timer.c
 *
 * @brief       This file implements hardware timer driver configuration for fm33
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifdef BSP_USING_TIM

#include <drv_hwtimer.h>

#ifdef BSP_USING_TIM0
static const struct cm32_timer_info tim0_info = {
    .htimer  = TIM0,
    .irqno   = timer0_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer0", tim0_info);
#endif

#ifdef BSP_USING_TIM1
static const struct cm32_timer_info tim1_info = {
    .htimer  = TIM1,
    .irqno   = timer1_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer1", tim1_info);
#endif

#ifdef BSP_USING_TIM2
static const struct cm32_timer_info tim2_info = {
    .htimer  = TIM2,
    .irqno   = timer2_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer2", tim2_info);
#endif

#ifdef BSP_USING_TIM3
static const struct cm32_timer_info tim3_info = {
    .htimer  = TIM3,
    .irqno   = timer3_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer3", tim3_info);
#endif

#ifdef BSP_USING_TIM4
static const struct cm32_timer_info tim4_info = {
    .htimer  = TIM4,
    .irqno   = timer4_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer4", tim4_info);
#endif

#ifdef BSP_USING_TIM5
static const struct cm32_timer_info tim5_info = {
    .htimer  = TIM5,
    .irqno   = timer5_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer5", tim5_info);
#endif

#ifdef BSP_USING_TIM6
static const struct cm32_timer_info tim6_info = {
    .htimer  = TIM6,
    .irqno   = timer6_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer6", tim6_info);
#endif

#ifdef BSP_USING_TIM7
static const struct cm32_timer_info tim7_info = {
    .htimer  = TIM7,
    .irqno   = timer7_handler_IRQn,
    .clk_div = FREQUENCY_DIVISION_7,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer7", tim7_info);
#endif

#endif /*BSP_USING_TIM*/
