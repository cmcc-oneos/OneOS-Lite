/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.h
 *
 * @brief       Board resource definition
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#include <drv_cfg.h>
#include "nuclei_sdk_soc.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CM32_FLASH_START_ADRESS    ((uint32_t)0x08000000)
#define CM32_FLASH_SIZE            (512 * 1024)
#define CM32_FLASH_BLOCK_SIZE      (2048)
#define CM32_FLASH_BYTE_ALIGN_SIZE (4)
#define CM32_FLASH_END_ADDRESS     ((uint32_t)(CM32_FLASH_START_ADRESS + CM32_FLASH_SIZE))
#define CM32_SECTOR_SIZE           (2048)

#define CM32_SRAM_SIZE (128)
#define CM32_SRAM_END  (0x20000000 + CM32_SRAM_SIZE * 1024)

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$RW_IRAM1$$ZI$$Limit;
#define HEAP_BEGIN (&Image$$RW_IRAM1$$ZI$$Limit)
#elif __ICCARM__
#pragma section = "HEAP"
#define HEAP_BEGIN (__segment_end("HEAP"))
#else
extern int _end;
#define HEAP_BEGIN (&_end)
#endif

#define HEAP_END CM32_SRAM_END

#ifdef OS_USING_PUSH_BUTTON
extern const struct push_button key_table[];
extern const int                key_table_size;
#endif

#ifdef OS_USING_LED
extern const led_t led_table[];
extern const int   led_table_size;
#endif

#ifdef __cplusplus
}
#endif

#endif
