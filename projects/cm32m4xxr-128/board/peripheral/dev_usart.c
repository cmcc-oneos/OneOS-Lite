/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file implements usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_usart.h"

#define GPIO_UART_NO_RMP 0

#ifdef BSP_USING_UART1
struct cm32_usart_info uart1_info = {
    .uart_device = 1,
    .irqn        = USART1_IRQn,
    .idx         = USART1,
    .uart_clk    = RCC_APB2_PERIPH_USART1,
    .uart_tx_clk = RCC_APB2_PERIPH_GPIOA,
    .uart_rx_clk = RCC_APB2_PERIPH_GPIOA,
    .gpio_remap  = GPIO_UART_NO_RMP,
    .tx_port     = GPIOA,
    .tx_pin      = GPIO_PIN_9,
    .rx_port     = GPIOA,
    .rx_pin      = GPIO_PIN_10,

    .dma_channel = DMA1_CH5,
    .periph_addr = USART1_BASE,
    .dma_clk     = RCC_AHB_PERIPH_DMA1,
    .dma_irqn    = DMA1_Channel5_IRQn,
#ifdef UART1_USING_DMA
    .dma_support = 1,
#else
    .dma_support = 0,
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart1", uart1_info);
#endif

#ifdef BSP_USING_UART2
struct cm32_usart_info uart2_info = {
    .uart_device = 2,
    .irqn        = USART2_IRQn,
    .idx         = USART2,
    .uart_clk    = RCC_APB1_PERIPH_USART2,
    .uart_tx_clk = RCC_APB2_PERIPH_GPIOA,
    .uart_rx_clk = RCC_APB2_PERIPH_GPIOA,
    .gpio_remap  = GPIO_UART_NO_RMP,
    .tx_port     = GPIOA,
    .tx_pin      = GPIO_PIN_2,
    .rx_port     = GPIOA,
    .rx_pin      = GPIO_PIN_3,

    .dma_channel = DMA1_CH6,
    .periph_addr = USART2_BASE,
    .dma_clk     = RCC_AHB_PERIPH_DMA1,
    .dma_irqn    = DMA1_Channel6_IRQn,
#ifdef UART2_USING_DMA
    .dma_support = 1,
#else
    .dma_support = 0,
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart2", uart2_info);
#endif

#ifdef BSP_USING_UART3
struct cm32_usart_info uart3_info = {
    .uart_device = 3,
    .irqn        = USART3_IRQn,
    .idx         = USART3,
    .uart_clk    = RCC_APB1_PERIPH_USART3,
    .uart_tx_clk = RCC_APB2_PERIPH_GPIOB,
    .uart_rx_clk = RCC_APB2_PERIPH_GPIOB,
    .gpio_remap  = GPIO_UART_NO_RMP,
    .tx_port     = GPIOB,
    .tx_pin      = GPIO_PIN_10,
    .rx_port     = GPIOB,
    .rx_pin      = GPIO_PIN_11,

    .dma_channel = DMA1_CH3,
    .periph_addr = USART3_BASE,
    .dma_clk     = RCC_AHB_PERIPH_DMA1,
    .dma_irqn    = DMA1_Channel3_IRQn,
#ifdef UART3_USING_DMA
    .dma_support = 1,
#else
    .dma_support = 0,
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart3", uart3_info);
#endif

#ifdef BSP_USING_UART4
struct cm32_usart_info uart4_info = {
    .uart_device = 4,
    .irqn        = UART4_IRQn,
    .idx         = UART4,
    .uart_clk    = RCC_APB1_PERIPH_UART4,
    .uart_tx_clk = RCC_APB2_PERIPH_GPIOC,
    .uart_rx_clk = RCC_APB2_PERIPH_GPIOC,
    .gpio_remap  = GPIO_UART_NO_RMP,
    .tx_port     = GPIOC,
    .tx_pin      = GPIO_PIN_10,
    .rx_port     = GPIOC,
    .rx_pin      = GPIO_PIN_11,

    .dma_channel = DMA2_CH3,
    .periph_addr = UART4_BASE,
    .dma_clk     = RCC_AHB_PERIPH_DMA2,
    .dma_irqn    = DMA2_Channel3_IRQn,
#ifdef UART4_USING_DMA
    .dma_support = 1,
#else
    .dma_support = 0,
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart4", uart4_info);
#endif

#ifdef BSP_USING_UART5
struct cm32_usart_info uart5_info = {
    .uart_device = 5,
    .irqn        = UART5_IRQn,
    .idx         = UART5,
    .uart_clk    = RCC_APB1_PERIPH_UART5,
    .uart_tx_clk = RCC_APB2_PERIPH_GPIOE,
    .uart_rx_clk = RCC_APB2_PERIPH_GPIOE,
    .gpio_remap  = GPIO_RMP2_UART5, /* remapping */
    .tx_port     = GPIOE,
    .tx_pin      = GPIO_PIN_8,
    .rx_port     = GPIOE,
    .rx_pin      = GPIO_PIN_9,

    .dma_channel = DMA1_CH8,
    .periph_addr = UART5_BASE,
    .dma_clk     = RCC_AHB_PERIPH_DMA1,
    .dma_irqn    = DMA1_Channel8_IRQn,
#ifdef UART5_USING_DMA
    .dma_support = 1,
#else
    .dma_support = 0,
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart5", uart5_info);
#endif

#ifdef BSP_USING_UART6
struct cm32_usart_info uart6_info = {
    .uart_device = 6,
    .irqn        = UART6_IRQn,
    .idx         = UART6,
    .uart_clk    = RCC_APB2_PERIPH_UART6,
    .uart_tx_clk = RCC_APB2_PERIPH_GPIOE,
    .uart_rx_clk = RCC_APB2_PERIPH_GPIOE,
    .gpio_remap  = GPIO_UART_NO_RMP,
    .tx_port     = GPIOE,
    .tx_pin      = GPIO_PIN_2,
    .rx_port     = GPIOE,
    .rx_pin      = GPIO_PIN_3,

    .dma_channel = DMA2_CH1,
    .periph_addr = UART6_BASE,
    .dma_clk     = RCC_AHB_PERIPH_DMA2,
    .dma_irqn    = DMA2_Channel1_IRQn,
#ifdef UART6_USING_DMA
    .dma_support = 1,
#else
    .dma_support = 0,
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart6", uart6_info);
#endif

#ifdef BSP_USING_UART7
struct cm32_usart_info uart7_info = {
    .uart_device = 7,
    .irqn        = UART7_IRQn,
    .idx         = UART7,
    .uart_clk    = RCC_APB2_PERIPH_UART7,
    .uart_tx_clk = RCC_APB2_PERIPH_GPIOC,
    .uart_rx_clk = RCC_APB2_PERIPH_GPIOC,
    .gpio_remap  = GPIO_UART_NO_RMP,
    .tx_port     = GPIOC,
    .tx_pin      = GPIO_PIN_4,
    .rx_port     = GPIOC,
    .rx_pin      = GPIO_PIN_5,

    .dma_channel = DMA2_CH6,
    .periph_addr = UART7_BASE,
    .dma_clk     = RCC_AHB_PERIPH_DMA2,
    .dma_irqn    = DMA2_Channel6_IRQn,
#ifdef UART7_USING_DMA
    .dma_support = 1,
#else
    .dma_support = 0,
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart7", uart7_info);
#endif
