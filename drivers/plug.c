/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        plug.c
 *
 * @brief       This file implements the plug functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-25   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_memory.h>
#include <os_errno.h>
#include <arch_interrupt.h>
#include <os_task.h>
#include <os_util.h>
#include <os_assert.h>
#include <string.h>
#include <driver.h>

#include "plug.h"

static os_list_node_t os_plug_list = OS_LIST_INIT(os_plug_list);

os_err_t os_plug_in(os_plug_t *plug, const char *tag, const char *name)
{
    os_base_t  level;
    os_plug_t *plug_tmp;

    OS_ASSERT(name != OS_NULL);
    OS_ASSERT(plug != OS_NULL);
    OS_ASSERT(plug->ref_count == 0);

    memset(plug->tag, 0, sizeof(plug->tag));
    strncpy(plug->tag, tag, OS_NAME_MAX);

    memset(plug->name, 0, sizeof(plug->name));
    strncpy(plug->name, name, OS_NAME_MAX);

    os_sem_init(&plug->sem, OS_NULL, 0, 1);

    level = os_irq_lock();

    os_list_for_each_entry(plug_tmp, &os_plug_list, os_plug_t, list)
    {
        if (0 == strncmp(plug_tmp->tag, tag, OS_NAME_MAX) && 0 == strncmp(plug_tmp->name, name, OS_NAME_MAX))
        {
            os_irq_unlock(level);
            return OS_EINVAL;
        }
    }

    os_list_add(&os_plug_list, &plug->list);

    os_irq_unlock(level);

    return OS_EOK;
}

static void os_plug_halt_chain(os_plug_t *plug)
{
    if (plug->halt != OS_NULL)
        plug->halt(plug);

    if (plug->upper != OS_NULL)
        os_plug_halt_chain(plug->upper);
}

void os_plug_out(os_plug_t *plug)
{
    os_base_t level;

    OS_ASSERT(plug != OS_NULL);

    level = os_irq_lock();

    os_list_del(&plug->list);

    os_irq_unlock(level);

    while (plug->ref_count > 0)
    {
        os_plug_halt_chain(plug);
        os_sem_wait(&plug->sem, OS_WAIT_FOREVER);
    }

    if (plug->upper != OS_NULL)
    {
        os_plug_out(plug->upper);
    }

    if (plug->release != OS_NULL)
        plug->release(plug);

    os_sem_deinit(&plug->sem);
}

os_plug_t *os_plug_get(const char *tag, const char *name)
{
    os_base_t  level;
    os_plug_t *plug;

    level = os_irq_lock();

    os_list_for_each_entry(plug, &os_plug_list, os_plug_t, list)
    {
        if (0 == strncmp(plug->tag, tag, OS_NAME_MAX) && 0 == strncmp(plug->name, name, OS_NAME_MAX))
        {
            plug->ref_count++;
            os_irq_unlock(level);
            return plug;
        }
    }

    os_irq_unlock(level);
    return OS_NULL;
}

void os_plug_put(os_plug_t *plug)
{
    os_base_t level;

    OS_ASSERT(plug != OS_NULL);

    level = os_irq_lock();

    plug->ref_count--;

    os_sem_post(&plug->sem);

    os_irq_unlock(level);
}
