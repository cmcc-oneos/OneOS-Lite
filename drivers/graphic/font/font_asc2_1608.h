/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        font_asc2_1608.h
 *
 * @brief       This file provides st7789vw font data
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __FONT_ASC2_1608_H__
#define __FONT_ASC2_1608_H__

#include "oneos_config.h"
#include "graphic/graphic.h"

#ifdef OS_GRAPHIC_DRAW_ENABLE
extern const os_graphic_font_t font_asc2_1608;
#endif

#endif
