/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        graphic_conf.h
 *
 * @brief       This file includes lcd configuration for graphic device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef _GRAPHIC_CONF_H_
#define _GRAPHIC_CONF_H_

#include "oneos_config.h"

#ifndef OS_GRAPHIC_LCD_WIDTH
#error Missing defination OS_GRAPHIC_LCD_WIDTH
#endif

#ifndef OS_GRAPHIC_LCD_HEIGHT
#error Missing defination OS_GRAPHIC_LCD_HEIGHT
#endif

#if (defined OS_GRAPHIC_LCD_FORMAT_RGB565)
#define OS_GRAPHIC_LCD_FORMAT OS_GRAPHIC_PIXEL_FORMAT_RGB565
#define OS_GRAPHIC_LCD_DEPTH  16
#elif (defined OS_GRAPHIC_LCD_FORMAT_RGB888)
#define OS_GRAPHIC_LCD_FORMAT OS_GRAPHIC_PIXEL_FORMAT_RGB888
#define OS_GRAPHIC_LCD_DEPTH  24
#elif (defined OS_GRAPHIC_LCD_FORMAT_ARGB8888)
#define OS_GRAPHIC_LCD_FORMAT OS_GRAPHIC_PIXEL_FORMAT_ARGB8888
#define OS_GRAPHIC_LCD_DEPTH  32
#else
#error Missing defination OS_GRAPHIC_LCD_FORMAT
#endif

enum
{
    OS_GRAPHIC_PIXEL_FORMAT_INVALID,
    OS_GRAPHIC_PIXEL_FORMAT_RGB565,
    OS_GRAPHIC_PIXEL_FORMAT_RGB888,
    OS_GRAPHIC_PIXEL_FORMAT_ARGB8888,
};

#endif /* _GRAPHIC_CONF_H_ */
