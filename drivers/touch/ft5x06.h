/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ft5406.h
 *
 * @brief       ft5406
 *
 * @details     ft5406
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef _FT5406_H_
#define _FT5406_H_

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#include <touch.h>

/*! @brief FT5X06 maximum number of simultaneously detected touches. */
#define FT5X06_MAX_TOUCHES (5U)

/*! @brief FT5X06 register address where touch data begin. */
#define FT5X06_TOUCH_DATA_SUBADDR (1)

/*! @brief FT5X06 raw touch data length. */
#define FT5X06_TOUCH_DATA_LEN (0x20)

#define FT5X06_ID_G_CIPHER 0xA3

typedef enum _touch_event
{
    kTouch_Down     = 0, /*!< The state changed to touched. */
    kTouch_Up       = 1, /*!< The state changed to not touched. */
    kTouch_Contact  = 2, /*!< There is a continuous touch being detected. */
    kTouch_Reserved = 3  /*!< No touch information available. */
} touch_event_t;

typedef struct _touch_point
{
    touch_event_t TOUCH_EVENT; /*!< Indicates the state or event of the touch point. */
    os_uint8_t    TOUCH_ID; /*!< Id of the touch point. This numeric value stays constant between down and up event. */
    os_uint16_t   TOUCH_X;  /*!< X coordinate of the touch point */
    os_uint16_t   TOUCH_Y;  /*!< Y coordinate of the touch point */
} touch_point_t;

#endif
