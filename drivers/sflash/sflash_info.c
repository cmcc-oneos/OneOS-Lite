/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sflash.c
 *
 * @brief       This file provides functions for sflash.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "sflash.h"

#ifdef OS_SFLASH_SUPPORTED_CHIP_W25Q

static const struct os_xspi_message_cfg w25q_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0x3b,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .dummy_cycles      = 8,
        .data_lines        = 2,
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0xbb,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 2,
        .dummy_cycles      = 4,
        .data_lines        = 2,
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0x6b,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .dummy_cycles      = 8,
        .data_lines        = 4,
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0xeb,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 4,
        .dummy_cycles      = 6,
        .data_lines        = 4,
    },
};

OS_SFLASH_INFO sflash_W25Q40BV_info = {
    .mf                = 0xef,
    .id                = 0x13,
    .name              = "W25Q40BV",
    .capacity          = 512 * 1024,
    .supported_cmds    = w25q_supported_cmds,
    .supported_cmds_nr = 1, /* only support fast read mode 112 */
};

OS_SFLASH_INFO sflash_W25Q16BV_info = {
    .mf                = 0xef,
    .id                = 0x15,
    .name              = "W25Q16BV",
    .capacity          = 2 * 1024 * 1024,
    .supported_cmds    = w25q_supported_cmds,
    .supported_cmds_nr = 1, /* only support fast read mode 112 */
};

OS_SFLASH_INFO sflash_W25Q64DW_info = {
    .mf                = 0xef,
    .id                = 0x17,
    .name              = "W25Q64DW",
    .capacity          = 8 * 1024 * 1024,
    .supported_cmds    = w25q_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(w25q_supported_cmds),
};

OS_SFLASH_INFO sflash_W25Q128BV_info = {
    .mf                = 0xef,
    .id                = 0x18,
    .name              = "W25Q128BV",
    .capacity          = 16 * 1024 * 1024,
    .supported_cmds    = w25q_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(w25q_supported_cmds),
};

#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_NM25Q

OS_SFLASH_INFO sflash_NM25Q128_info = {
    .mf       = 0x52,
    .id       = 0x18,
    .name     = "NM25Q128",
    .capacity = 16 * 1024 * 1024,
};

#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_AT45DB161E

static const struct os_xspi_message_cfg AT45DB161E_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
        .instruction       = 0x81,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .priv              = 512, /* erase size */
    },
};

OS_SFLASH_INFO sflash_AT45DB161E_info = {
    .mf                = 0x1f,
    .id                = 0x00,
    .name              = "AT45DB161E",
    .page_size         = 1,
    .capacity          = 2 * 1024 * 1024,
    .supported_cmds    = AT45DB161E_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(AT45DB161E_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_AT45DB041E

static const struct os_xspi_message_cfg AT45DB041E_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
        .instruction       = 0x81,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .priv              = 512, /* erase size */
    },
};

OS_SFLASH_INFO sflash_AT45DB041E_info = {
    .mf                = 0x1f,
    .id                = 0x00,
    .name              = "AT45DB041E",
    .page_size         = 1,
    .capacity          = 512 * 1024,
    .supported_cmds    = AT45DB041E_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(AT45DB041E_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_SST25VF016B

OS_SFLASH_INFO sflash_SST25VF016B_info = {
    .mf        = 0xbf,
    .id        = 0x41,
    .name      = "SST25VF016B",
    .page_size = 1,
    .capacity  = 2 * 1024 * 1024,
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_M25P32

static const struct os_xspi_message_cfg M25P32_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
        .instruction       = 0xd8,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .priv              = 65536, /* erase size */
    },
};

OS_SFLASH_INFO sflash_M25P32_info = {
    .mf                = 0x20,
    .id                = 0x16,
    .name              = "M25P32",
    .capacity          = 4 * 1024 * 1024,
    .supported_cmds    = M25P32_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(M25P32_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_M25P80

static const struct os_xspi_message_cfg M25P80_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
        .instruction       = 0xd8,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .priv              = 65536, /* erase size */
    },
};

OS_SFLASH_INFO sflash_M25P80_info = {
    .mf                = 0x20,
    .id                = 0x14,
    .name              = "M25P80",
    .capacity          = 1 * 1024 * 1024,
    .supported_cmds    = M25P80_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(M25P80_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_M25P40

static const struct os_xspi_message_cfg M25P40_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
        .instruction       = 0xd8,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .priv              = 65536, /* erase size */
    },
};

OS_SFLASH_INFO sflash_M25P40_info = {
    .mf                = 0x20,
    .id                = 0x13,
    .name              = "M25P40",
    .capacity          = 512 * 1024,
    .supported_cmds    = M25P40_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(M25P40_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_EN25Q32B

static const struct os_xspi_message_cfg EN25Q32B_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0x3b,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .dummy_cycles      = 8,
        .data_lines        = 2,
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0xeb,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 4,
        .dummy_cycles      = 6,
        .data_lines        = 4,
    },
};

OS_SFLASH_INFO sflash_EN25Q32B_info = {
    .mf                = 0x1c,
    .id                = 0x16,
    .name              = "EN25Q32B",
    .capacity          = 4 * 1024 * 1024,
    .supported_cmds    = EN25Q32B_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(EN25Q32B_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_GD25Q64B

static const struct os_xspi_message_cfg GD25Q64B_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0x3b,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .dummy_cycles      = 8,
        .data_lines        = 2,
    },
};

OS_SFLASH_INFO sflash_GD25Q64B_info = {
    .mf                = 0xc8,
    .id                = 0x17,
    .name              = "GD25Q64B",
    .capacity          = 8 * 1024 * 1024,
    .supported_cmds    = GD25Q64B_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(GD25Q64B_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_GD25Q127CSIG

OS_SFLASH_INFO sflash_GD25Q127CSIG_info = {
    .mf       = 0xc8,
    .id       = 0x18,
    .name     = "GD25Q127CSIG",
    .capacity = 16 * 1024 * 1024,
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_GD25Q16B

OS_SFLASH_INFO sflash_GD25Q16B_info = {
    .mf       = 0xc8,
    .id       = 0x15,
    .name     = "GD25Q16B",
    .capacity = 2 * 1024 * 1024,
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_S25FL216K

static const struct os_xspi_message_cfg S25FL216K_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0x3b,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .dummy_cycles      = 8,
        .data_lines        = 2,
    },
};

OS_SFLASH_INFO sflash_S25FL216K_info = {
    .mf                = 0x01,
    .id                = 0x15,
    .name              = "S25FL216K",
    .capacity          = 2 * 1024 * 1024,
    .supported_cmds    = S25FL216K_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(S25FL216K_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_S25FL032P

OS_SFLASH_INFO sflash_S25FL032P_info = {
    .mf       = 0x01,
    .id       = 0x15,
    .name     = "S25FL032P",
    .capacity = 4 * 1024 * 1024,
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_A25L080

static const struct os_xspi_message_cfg A25L080_supported_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0x3b,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .dummy_cycles      = 8,
        .data_lines        = 2,
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0xbb,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 2,
        .dummy_cycles      = 4,
        .data_lines        = 2,
    },
};

OS_SFLASH_INFO sflash_A25L080_info = {
    .mf                = 0x37,
    .id                = 0x14,
    .name              = "A25L080",
    .capacity          = 1 * 1024 * 1024,
    .supported_cmds    = A25L080_supported_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(A25L080_supported_cmds),
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_F25L004

OS_SFLASH_INFO sflash_F25L004_info = {
    .mf        = 0x8c,
    .id        = 0x13,
    .name      = "F25L004",
    .page_size = 1,
    .capacity  = 512 * 1024,
};
#endif

#ifdef OS_SFLASH_SUPPORTED_CHIP_PCT25VF016B

OS_SFLASH_INFO sflash_PCT25VF016B_info = {
    .mf        = 0xbf,
    .id        = 0x41,
    .name      = "PCT25VF016B",
    .page_size = 1,
    .capacity  = 2 * 1024 * 1024,
};
#endif
