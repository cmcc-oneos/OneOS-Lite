/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sflash.c
 *
 * @brief       This file provides functions for sflash.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_memory.h>
#include <os_errno.h>
#include <arch_interrupt.h>
#include <os_task.h>
#include <os_util.h>
#include <os_assert.h>
#include <string.h>
#include <driver.h>

#include "sflash.h"
#include "sfdp.h"

struct sflash_manufacturer_info
{
    char      *name;
    os_uint8_t mf;
};

/* clang-format off */
static struct sflash_manufacturer_info gs_manufacturer_info[] = {
    {"Cypress",    0x01},
    {"Fujitsu",    0x04},
    {"EON",        0x1C},
    {"Atmel",      0x1F},
    {"Micron",     0x20},
    {"AMIC",       0x37},
    {"Sanyo",      0x62},
    {"Intel",      0x89},
    {"ESMT",       0x8C},
    {"Fudan",      0xA1},
    {"Hyundai",    0xAD},
    {"SST",        0xBF},
    {"Micronix",   0xC2},
    {"GigaDevice", 0xC8},
    {"ISSI",       0xD5},
    {"Winbond",    0xEF},
    {"nor-mem",    0x52},

    {OS_NULL,      0xff},
};
/* clang-format on */

static const struct os_xspi_message_cfg default_cmds[] = {
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = 0x03,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .data_lines        = 1,
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_WRITE,
        .instruction       = 0x02,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .data_lines        = 1,
        .priv              = 256, /* page size */
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
        .instruction       = 0x20,
        .instruction_lines = 1,
        .address_size      = 3,
        .address_lines     = 1,
        .priv              = 4096, /* erase size */
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_STATUS,
        .instruction       = 0x05,
        .instruction_lines = 1,
        .data_lines        = 1,
        .priv              = (0 << 4) | 1, /* busy bit, busy status */
    },
    {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_UNLOCK,
        .instruction       = 0x06,
        .instruction_lines = 1,
    },
};

OS_SFLASH_INFO sflash_unknown_info = {
    .mf                = 0xff,
    .id                = 0xff,
    .name              = OS_NULL,
    .capacity          = 16 * 1024 * 1024,
    .supported_cmds    = default_cmds,
    .supported_cmds_nr = OS_ARRAY_SIZE(default_cmds),
};

static void os_sflash_reset(struct os_sflash *sflash)
{
    int                    i;
    struct os_xspi_message xmsg, *xmessage = &xmsg;

    memset(xmessage, 0, sizeof(struct os_xspi_message));

    int support_line[][2] = {
        {sflash->sfbus->support_8_line, 8},
        {sflash->sfbus->support_4_line, 4},
        {sflash->sfbus->support_2_line, 2},
        {sflash->sfbus->support_1_line, 1},
    };

    for (i = 0; i < OS_ARRAY_SIZE(support_line); i++)
    {
        if (support_line[i][0] == 0)
            continue;

        xmessage_instruction(xmessage)       = 0x66;
        xmessage_instruction_lines(xmessage) = support_line[i][1];
        os_sfbus_transfer(sflash, xmessage);

        xmessage_instruction(xmessage)       = 0x99;
        xmessage_instruction_lines(xmessage) = support_line[i][1];
        os_sfbus_transfer(sflash, xmessage);

        os_task_msleep(10);
    }
}

static void os_sflash_dummy(struct os_sflash *sflash)
{
    os_uint16_t dummy[2] = {0xff, 0xff};

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage_data_lines(xmessage) = 1;
    xmessage_data_size(xmessage)  = 2;
    xmessage_data_dir(xmessage)   = OS_XSPI_DATA_DIR_HOST_TO_DEVICE;
    xmessage_data(xmessage)       = (os_uint8_t *)dummy;

    os_sfbus_transfer(sflash, xmessage);
}

static os_uint16_t os_sflash_read_id(struct os_sflash *sflash)
{
    os_uint32_t id;

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage_instruction(xmessage)       = 0x9f;
    xmessage_instruction_lines(xmessage) = 1;

    xmessage_data_lines(xmessage) = 1;
    xmessage_data_size(xmessage)  = 3;
    xmessage_data_dir(xmessage)   = OS_XSPI_DATA_DIR_DEVICE_TO_HOST;
    xmessage_data(xmessage)       = (os_uint8_t *)&id;

    os_sfbus_transfer(sflash, xmessage);

    id = ((id >> 8) & 0xff00) | (id & 0xff);

    return id;
}

static int os_sflash_read_status(struct os_sflash *sflash, int reg_index, os_uint8_t *status)
{
    OS_ASSERT(sflash != OS_NULL);

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage->xcfg = sflash->cmds.status;

    xmessage_data_size(xmessage) = 1;
    xmessage_data_dir(xmessage)  = OS_XSPI_DATA_DIR_DEVICE_TO_HOST;
    xmessage_data(xmessage)      = status;

    return os_sfbus_transfer(sflash, xmessage);
}

static void os_sflash_wait_busy(struct os_sflash *sflash)
{
    int        ret;
    os_uint8_t status;

    os_uint8_t busy_bit = (sflash->cmds.status.priv >> 4) & 0xf;
    os_uint8_t busy_sta = sflash->cmds.status.priv & 1;

    do
    {
        ret = os_sflash_read_status(sflash, 1, &status);
    } while (ret != 0 || ((status >> busy_bit) & 1) == busy_sta);
}

static void os_sflash_unlock_blocks(struct os_sflash *sflash)
{
    os_uint8_t status = 0;

    OS_ASSERT(sflash != OS_NULL);

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    /* status write enable */
    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage_instruction(xmessage)       = 0x50;
    xmessage_instruction_lines(xmessage) = 1;

    os_sfbus_transfer(sflash, xmessage);

    /* write status to unprotect blocks */
    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage_instruction(xmessage)       = 0x01;
    xmessage_instruction_lines(xmessage) = 1;

    xmessage_data_lines(xmessage) = 1;
    xmessage_data_size(xmessage)  = 1;
    xmessage_data_dir(xmessage)   = OS_XSPI_DATA_DIR_HOST_TO_DEVICE;
    xmessage_data(xmessage)       = &status;

    os_sfbus_transfer(sflash, xmessage);
}

#ifdef OS_SFLASH_SUPPORTED_CHIP
static const struct os_sflash_info *os_sflash_find_flash_info_by_id(os_uint16_t flash_id)
{
#if defined(__CC_ARM) || defined(__CLANG_ARM) /* ARM MDK Compiler */
    extern const int os_sflash_info$$Base;
    extern const int os_sflash_info$$Limit;

    const struct os_sflash_info *table = (struct os_sflash_info *)&os_sflash_info$$Base;
    int                          num   = (struct os_sflash_info *)&os_sflash_info$$Limit - table;

#elif defined(__ICCARM__) || defined(__ICCRX__) /* for IAR Compiler */
    const struct os_sflash_info *table = (struct os_sflash_info *)__section_begin("os_sflash_info");
    int                          num   = (struct os_sflash_info *)__section_end("os_sflash_info") - table;
#elif defined(__GNUC__)                         /* for GCC Compiler */
    extern const int             __os_sflash_info_start;
    extern const int             __os_sflash_info_end;
    const struct os_sflash_info *table = (struct os_sflash_info *)&__os_sflash_info_start;
    int                          num   = (struct os_sflash_info *)&__os_sflash_info_end - table;
#endif

    int i;

    for (i = 0; i < num; i++)
    {
        if (((table[i].id << 8) | table[i].mf) == flash_id)
        {
            return &table[i];
        }
    }

    return OS_NULL;
}
#endif

#ifdef OS_SFLASH_SUPPORT_SFDP

static os_uint8_t *os_sflash_read_sfdp(struct os_sflash *sflash)
{
    os_uint8_t *sfdp_buff = os_calloc(1, 256);

    OS_ASSERT(sfdp_buff != OS_NULL);

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage_instruction(xmessage)       = 0x5a;
    xmessage_instruction_lines(xmessage) = 1;

    xmessage_address_lines(xmessage) = 1;
    xmessage_address_size(xmessage)  = 3;
    xmessage_address(xmessage)       = 0;

    xmessage_dummy_cycles(xmessage) = 8;

    xmessage_data_lines(xmessage) = 1;

    xmessage_data_size(xmessage) = 256;
    xmessage_data_dir(xmessage)  = OS_XSPI_DATA_DIR_DEVICE_TO_HOST;
    xmessage_data(xmessage)      = sfdp_buff;

    os_sfbus_transfer(sflash, xmessage);

    return sfdp_buff;
}

static struct os_sfdp *os_sflash_find_flash_info_by_sfdp(struct os_sflash *sflash)
{
    os_uint8_t *sfdp_buff = os_sflash_read_sfdp(sflash);

    if (sfdp_buff == OS_NULL)
        return OS_NULL;

    struct os_sfdp *sfdp = os_calloc(1, sizeof(struct os_sfdp));

    OS_ASSERT(sfdp != OS_NULL);

    os_sfdp_decode(sfdp_buff, sfdp);

    os_free(sfdp_buff);

    return sfdp;
}
#endif

#define OS_SFLASH_BUS_CHECK_SUPPORT_N_LINES(sfbus, cfg, n)                                                             \
    do                                                                                                                 \
    {                                                                                                                  \
        if (cfg->instruction_lines == n && sfbus->support_##n##_line == 0)                                             \
            return OS_FALSE;                                                                                           \
                                                                                                                       \
        if (cfg->address_lines == n && sfbus->support_##n##_line == 0)                                                 \
            return OS_FALSE;                                                                                           \
                                                                                                                       \
        if (cfg->alternate_lines == n && sfbus->support_##n##_line == 0)                                               \
            return OS_FALSE;                                                                                           \
                                                                                                                       \
        if (cfg->data_lines == n && sfbus->support_##n##_line == 0)                                                    \
            return OS_FALSE;                                                                                           \
    } while (0)

static os_bool_t os_sflash_bus_support_lines(struct os_sfbus *sfbus, const struct os_xspi_message_cfg *cfg)
{
    OS_SFLASH_BUS_CHECK_SUPPORT_N_LINES(sfbus, cfg, 8);
    OS_SFLASH_BUS_CHECK_SUPPORT_N_LINES(sfbus, cfg, 4);
    OS_SFLASH_BUS_CHECK_SUPPORT_N_LINES(sfbus, cfg, 2);
    OS_SFLASH_BUS_CHECK_SUPPORT_N_LINES(sfbus, cfg, 1);

    return OS_TRUE;
}

static void os_sflash_cmd_set(struct os_xspi_message_cfg *cmd, const struct os_xspi_message_cfg *cfg)
{
    if (cfg->instruction_lines != 1)
    {
        return;
    }

    if (cmd->type == OS_XSPI_MESSAGE_CFG_TYPE_NONE)
    {
    }
    else if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_READ)
    {
        if (!((cmd->data_lines == 1 && cmd->address_lines == 1 && cmd->instruction_lines == 1) ||
              cmd->data_lines < cfg->data_lines ||
              (cmd->data_lines == cfg->data_lines && cmd->address_lines < cfg->address_lines) ||
              (cmd->data_lines == cfg->data_lines && cmd->address_lines == cfg->address_lines &&
               cmd->instruction_lines < cfg->instruction_lines)))
        {
            return;
        }
    }
    else if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_WRITE)
    {
        /* priv is page size */
        if (cmd->priv < cfg->priv)
        {
            return;
        }
    }
    else if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_ERASE)
    {
        /* priv is erase size */
        if (cmd->priv < cfg->priv)
        {
            return;
        }
    }

    *cmd = *cfg;
}

static void os_sflash_prepare_cmds(struct os_sflash *sflash, const struct os_xspi_message_cfg *cmds, int cmds_nr)
{
    int                               i;
    struct os_sfbus                  *sfbus = sflash->sfbus;
    const struct os_xspi_message_cfg *cfg   = cmds;

    for (i = 0; i < cmds_nr; i++, cfg++)
    {
        OS_ASSERT(cfg->type != OS_XSPI_MESSAGE_CFG_TYPE_NONE);

        if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_READ && os_sflash_bus_support_lines(sfbus, cfg))
        {
            os_sflash_cmd_set(&sflash->cmds.read, cfg);
        }

        if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_WRITE && os_sflash_bus_support_lines(sfbus, cfg))
        {
            os_sflash_cmd_set(&sflash->cmds.write, cfg);
            if (sflash->info->addr_bytes)
            {
                sflash->cmds.write.address_size = sflash->info->addr_bytes;
            }
        }

        if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_ERASE && os_sflash_bus_support_lines(sfbus, cfg))
        {
            os_sflash_cmd_set(&sflash->cmds.erase, cfg);
        }

        if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_UNLOCK && os_sflash_bus_support_lines(sfbus, cfg))
        {
            os_sflash_cmd_set(&sflash->cmds.unlock, cfg);
        }

        if (cfg->type == OS_XSPI_MESSAGE_CFG_TYPE_STATUS && os_sflash_bus_support_lines(sfbus, cfg))
        {
            os_sflash_cmd_set(&sflash->cmds.status, cfg);
        }
    }
}

struct os_sflash *os_sflash_init(const char *bus_name, int cs)
{
    struct os_sflash *sflash = os_calloc(1, sizeof(struct os_sflash));

    sflash->cs = cs;

    sflash->sfbus = os_sfbus_attach(bus_name, cs);

    struct os_spi_configuration cfg;
    cfg.data_width = 8;
    cfg.mode       = OS_SPI_MODE_0 | OS_SPI_MSB;
    cfg.max_hz     = OS_SFLASH_FREQ;

    os_sflash_configure(sflash, &cfg);

    os_sflash_reset(sflash);
    os_sflash_dummy(sflash);

    os_uint16_t flash_id = os_sflash_read_id(sflash);

#ifdef OS_SFLASH_SUPPORTED_CHIP
    sflash->info = os_sflash_find_flash_info_by_id(flash_id);
#endif

#ifdef OS_SFLASH_SUPPORT_SFDP

    struct os_sfdp *sfdp = OS_NULL;

    if (sflash->info == OS_NULL)
    {
        sfdp = os_sflash_find_flash_info_by_sfdp(sflash);

        if (sfdp != OS_NULL)
        {
            sflash->info = &sfdp->info;
        }
    }

#endif

    if (sflash->info == OS_NULL)
    {
        sflash->info = &sflash_unknown_info;
    }

    os_sflash_prepare_cmds(sflash, default_cmds, OS_ARRAY_SIZE(default_cmds));
    os_sflash_prepare_cmds(sflash, sflash->info->supported_cmds, sflash->info->supported_cmds_nr);

    if (sflash->info->page_size != 0 && sflash->info->page_size < sflash->cmds.write.priv)
    {
        sflash->cmds.write.priv = sflash->info->page_size;
    }

#ifdef OS_SFLASH_SUPPORT_SFDP
    if (sfdp != OS_NULL)
    {
        os_sfdp_init_sflash(sflash, sfdp);
    }
#endif

    OS_ASSERT(sflash->cmds.read.type != OS_XSPI_MESSAGE_CFG_TYPE_NONE);
    OS_ASSERT(sflash->cmds.write.type != OS_XSPI_MESSAGE_CFG_TYPE_NONE);
    OS_ASSERT(sflash->cmds.write.priv != 0);
    OS_ASSERT(sflash->cmds.erase.type != OS_XSPI_MESSAGE_CFG_TYPE_NONE);
    OS_ASSERT(sflash->cmds.erase.priv != 0);

    /* sflash manufacturer */
    int                              i;
    struct sflash_manufacturer_info *minfo = &gs_manufacturer_info[0];

    for (i = 0; i < OS_ARRAY_SIZE(gs_manufacturer_info) - 1; i++, minfo++)
    {
        if (minfo->mf == (flash_id & 0xff))
        {
            break;
        }
    }

    os_kprintf("sflash: %s, %s, id: 0x%x\r\n",
               minfo->name ? minfo->name : "unknow",
               sflash->info->name ? sflash->info->name : "unknow",
               flash_id);

    /* unprotected blocks */
    os_sflash_unlock_blocks(sflash);

    return sflash;
}

int os_sflash_read_page(struct os_sflash *sflash, os_uint32_t offset, os_uint8_t *buf, os_size_t size)
{
    OS_ASSERT(sflash != OS_NULL);

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage->xcfg = sflash->cmds.read;

    xmessage_address(xmessage)   = offset;
    xmessage_data_size(xmessage) = size;
    xmessage_data_dir(xmessage)  = OS_XSPI_DATA_DIR_DEVICE_TO_HOST;
    xmessage_data(xmessage)      = buf;

    return os_sfbus_transfer(sflash, xmessage);
}

void sflash_write_unlock(struct os_sflash *sflash)
{
    struct os_xspi_message xmsg, *xmessage = &xmsg;

    /* unlock */
    if (sflash->cmds.unlock.type != OS_XSPI_MESSAGE_CFG_TYPE_NONE)
    {
        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage->xcfg = sflash->cmds.unlock;

        os_sflash_wait_busy(sflash);
        os_sfbus_transfer(sflash, xmessage);
    }
}

int os_sflash_write_page(struct os_sflash *sflash, os_uint32_t offset, const os_uint8_t *buf, os_size_t size)
{
    OS_ASSERT(sflash != OS_NULL);
    int ret;

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    /* unlock */
    if (sflash->cmds.unlock.type != OS_XSPI_MESSAGE_CFG_TYPE_NONE)
    {
        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage->xcfg = sflash->cmds.unlock;

        os_sflash_wait_busy(sflash);
        os_sfbus_transfer(sflash, xmessage);
    }

    /* write */
    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage->xcfg = sflash->cmds.write;

    xmessage_address(xmessage)   = offset;
    xmessage_data_size(xmessage) = size;
    xmessage_data_dir(xmessage)  = OS_XSPI_DATA_DIR_HOST_TO_DEVICE;
    xmessage_data(xmessage)      = (os_uint8_t *)buf;

    os_sflash_wait_busy(sflash);
    ret = os_sfbus_transfer(sflash, xmessage);
    os_sflash_wait_busy(sflash);

    return ret;
}

int os_sflash_erase_block(struct os_sflash *sflash, os_uint32_t offset)
{
    OS_ASSERT(sflash != OS_NULL);
    int ret;

    struct os_xspi_message xmsg, *xmessage = &xmsg;

    /* unlock */
    if (sflash->cmds.unlock.type != OS_XSPI_MESSAGE_CFG_TYPE_NONE)
    {
        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage->xcfg = sflash->cmds.unlock;

        os_sflash_wait_busy(sflash);
        os_sfbus_transfer(sflash, xmessage);
    }

    /* erase */
    memset(xmessage, 0, sizeof(struct os_xspi_message));

    xmessage->xcfg = sflash->cmds.erase;

    xmessage_address(xmessage) = offset;

    os_sflash_wait_busy(sflash);
    ret = os_sfbus_transfer(sflash, xmessage);
    os_sflash_wait_busy(sflash);

    return ret;
}

#ifdef OS_USING_FAL

#include <fal/fal.h>

static int os_sflash_fal_read_page(fal_flash_t *flash, os_uint32_t page_addr, os_uint8_t *buff, os_uint32_t page_nr)
{
    os_uint32_t addr = page_addr * flash->page_size;

    while (page_nr > 0)
    {
        if (os_sflash_read_page(flash->priv, addr, buff, flash->page_size) != 0)
            return -1;

        addr += flash->page_size;
        buff += flash->page_size;
        page_nr--;
    }

    return 0;
}

/* clang-format off */
static int os_sflash_fal_write_page(fal_flash_t *flash, os_uint32_t page_addr, const os_uint8_t *buff, os_uint32_t page_nr)
{
    os_uint32_t addr = page_addr * flash->page_size;

    while (page_nr > 0)
    {
        if (os_sflash_write_page(flash->priv, addr, buff, flash->page_size) != 0)
            return -1;

        addr += flash->page_size;
        buff += flash->page_size;
        page_nr--;
    }

    return 0;
}
/* clang-format on */

static int os_sflash_fal_erase_block(fal_flash_t *flash, os_uint32_t page_addr, os_uint32_t page_nr)
{
    os_uint32_t block_nr = flash->page_size * page_nr / flash->block_size;
    os_uint32_t addr     = page_addr * flash->page_size;

    while (block_nr > 0)
    {
        if (os_sflash_erase_block(flash->priv, addr) != 0)
            return -1;

        addr += flash->block_size;
        block_nr--;
    }

    return 0;
}

static int sflash_init(void)
{
    struct os_sflash *sflash = os_sflash_init(OS_SFLASH_BUS_NAME, OS_SFLASH_CS_PIN);

    if (sflash == OS_NULL)
    {
        os_kprintf("sflash init failed\r\n");
        return -1;
    }

    fal_flash_t *flash = os_calloc(1, sizeof(fal_flash_t));

    if (flash == OS_NULL)
    {
        os_kprintf("fal flash mem leak %s.\r\n", OS_EXTERN_FLASH_NAME);
        return -1;
    }

    memcpy(flash->name, OS_EXTERN_FLASH_NAME, min(FAL_DEV_NAME_MAX - 1, strlen(OS_EXTERN_FLASH_NAME)));

    flash->name[min(FAL_DEV_NAME_MAX - 1, strlen(OS_EXTERN_FLASH_NAME))] = 0;

    flash->capacity   = sflash->info->capacity;
    flash->block_size = sflash->cmds.erase.priv;
    flash->page_size  = sflash->cmds.write.priv;

    flash->ops.read_page   = os_sflash_fal_read_page;
    flash->ops.write_page  = os_sflash_fal_write_page;
    flash->ops.erase_block = os_sflash_fal_erase_block;

    flash->priv = sflash;

    return fal_flash_register(flash);
}

OS_DEVICE_INIT(sflash_init, OS_INIT_SUBLEVEL_MIDDLE);

#endif
