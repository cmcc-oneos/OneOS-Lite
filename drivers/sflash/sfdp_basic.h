/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sfdp_basic.h
 *
 * @brief       This file provides struct definition and sfdp basic functions declaration.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __SFDP_BASIC_H__
#define __SFDP_BASIC_H__

#include <spi.h>
#include "sfdp.h"

int  os_sfdp_decode_basic(struct os_sfdp *sfdp, struct os_sfdp_param *param);
void os_sfdp_basic_init(struct os_sflash *sflash, struct os_sfdp *sfdp, struct os_sfdp_param *param);

#endif
