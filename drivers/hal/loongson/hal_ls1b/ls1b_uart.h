#ifndef __LOONGSON_UART_H
#define __LOONGSON_UART_H

#include "ls1b_public.h"
#include "ls1b_regs.h"
#include "ls1b.h"

#define LS1B_UART_LSR_TE                (1 << 6)
#define LS1B_UART_LSR_TFE               (1 << 5)
#define LS1B_UART_PRINT_BUF_SIZE        (256)

#define LS1B_UART_DAT_OFFSET            (0)
#define LS1B_UART_IER_OFFSET            (1)
#define LS1B_UART_IIR_OFFSET            (2)
#define LS1B_UART_FCR_OFFSET            (2)
#define LS1B_UART_LCR_OFFSET            (3)
#define LS1B_UART_MCR_OFFSET            (4)
#define LS1B_UART_LSR_OFFSET            (5)
#define LS1B_UART_MSR_OFFSET            (6)

#define LS1B_UART_LSB_OFFSET            (0)
#define LS1B_UART_MSB_OFFSET            (1)

#define	IER_IRxE	0x1	
#define	IER_ITxE	0x2	
#define	IER_ILE	    0x4	
#define	IER_IME	    0x8	

#define	IIR_IMASK	0xf	
#define	IIR_RXTOUT	0xc	
#define	IIR_RLS		0x6	
#define	IIR_RXRDY	0x4	
#define	IIR_TXRDY	0x2	
#define	IIR_NOPEND	0x1	
#define	IIR_MLSC	0x0	
#define	IIR_FIFO_MASK	0xc0

#define	FIFO_ENABLE		0x01
#define	FIFO_RCV_RST	0x02
#define	FIFO_XMT_RST	0x04
#define	FIFO_DMA_MODE	0x08
#define	FIFO_TRIGGER_1	0x00
#define	FIFO_TRIGGER_4	0x40
#define	FIFO_TRIGGER_8	0x80
#define	FIFO_TRIGGER_14	0xc0

#define	CFCR_DLAB	0x80
#define	CFCR_SBREAK	0x40
#define	CFCR_PZERO	0x30
#define	CFCR_PONE	0x20
#define	CFCR_PEVEN	0x10
#define	CFCR_PODD	0x00
#define	CFCR_PENAB	0x08
#define	CFCR_STOPB	0x04
#define	CFCR_8BITS	0x03
#define	CFCR_7BITS	0x02
#define	CFCR_6BITS	0x01
#define	CFCR_5BITS	0x00

#define	MCR_LOOPBACK	0x10
#define	MCR_IENABLE	0x08
#define	MCR_DRS		0x04
#define	MCR_RTS		0x02
#define	MCR_DTR		0x01

#define	LSR_RCV_FIFO	0x80
#define	LSR_TSRE	0x40
#define	LSR_TXRDY	0x20
#define	LSR_BI		0x10
#define	LSR_FE		0x08
#define	LSR_PE		0x04
#define	LSR_OE		0x02
#define	LSR_RXRDY	0x01
#define	LSR_RCV_MASK	0x1f

typedef struct
{
    void *UARTx;
    unsigned int baudrate;
    BOOL rx_enable;
}ls1b_uart_info_t;

void uart_init(ls1b_uart_info_t *uart_info_p);
void uart_putc(void *uartx, unsigned char ch);
void uart_print(void *uartx, const char *str);
#endif
