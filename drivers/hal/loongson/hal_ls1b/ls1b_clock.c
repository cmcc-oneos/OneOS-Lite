#include "ls1b_regs.h"
#include "ls1b_public.h"

#define OS_OSC_CLK 33000000

#define AHB_CLK                 (OS_OSC_CLK)
#define APB_CLK                 (AHB_CLK)

#define DIV_DC_EN			(0x1 << 31)
#define DIV_DC				(0x1f << 26)
#define DIV_CPU_EN			(0x1 << 25)
#define DIV_CPU				(0x1f << 20)
#define DIV_DDR_EN			(0x1 << 19)
#define DIV_DDR				(0x1f << 14)

#define DIV_DC_SHIFT			26
#define DIV_CPU_SHIFT			20
#define DIV_DDR_SHIFT			14

unsigned long clk_get_pll_rate(void)
{
    unsigned int ctrl;
    float pll_rate = 0;

    ctrl = reg_read_32((volatile unsigned int *)LS1B_START_FREQ);

    pll_rate = ((12 + (ctrl & 0x3f)) + ((ctrl >> 8) & 0x3ff)/1024.0) * (float)APB_CLK / 2.0;

    return (unsigned long)pll_rate;
}

unsigned long clk_get_cpu_rate(void)
{
    unsigned long pll_rate, cpu_rate;
    unsigned int ctrl;

    pll_rate = clk_get_pll_rate();
    ctrl = reg_read_32((volatile unsigned int *)LS1B_CLK_DIV_PARAM);
    cpu_rate = pll_rate / ((ctrl & DIV_CPU) >> DIV_CPU_SHIFT);

    return cpu_rate;
}

unsigned long clk_get_ddr_rate(void)
{
    unsigned long pll_rate, ddr_rate;
    unsigned int ctrl;

    pll_rate = clk_get_pll_rate();
    ctrl = reg_read_32((volatile unsigned int *)LS1B_CLK_DIV_PARAM);

    ddr_rate = pll_rate / ((ctrl & DIV_DDR) >> DIV_DDR_SHIFT);

    return ddr_rate;
}

unsigned long clk_get_apb_rate(void)
{
    return clk_get_ddr_rate() / 2;
}

unsigned long clk_get_dc_rate(void)
{
    unsigned long pll_rate, dc_rate;
    unsigned int ctrl;

    pll_rate = clk_get_pll_rate();
    ctrl = reg_read_32((volatile unsigned int *)LS1B_CLK_DIV_PARAM);

    dc_rate = pll_rate ;

    return dc_rate;
}
