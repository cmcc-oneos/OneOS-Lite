/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <string.h>
#include <os_types.h>
#include <os_stddef.h>

#include <arch_interrupt.h>
#include <os_clock.h>
#include <os_memory.h>
#include <oneos_config.h>

#include <drv_common.h>
#include <board.h>
#include <drv_gpio.h>

#ifdef OS_USING_DMA
#include <dma.h>
#endif

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

extern uint32_t    SystemCoreClock;
static os_uint32_t _SysTick_Config(os_uint32_t ticks)
{
    if ((ticks - 1) > 0xFFFFFFFF)
    {
        return 1;
    }

    NVIC_SetPriority(SysTicK_IRQn, 0xff);
    NVIC_EnableIRQ(SysTicK_IRQn);
    SysTick->CTLR = 0;
    SysTick->SR   = 0;
    SysTick->CNT  = 0;
    SysTick->CMP  = ticks - 1;
    SysTick->CTLR = 0xF;
    return 0;
}

void SysTick_Handler(void) __attribute__((interrupt()));
void SysTick_Handler(void)
{
    TOGGLE_SP();
    SysTick->SR = 0;
    os_tick_increase();
    TOGGLE_SP();
}

void hardware_init(void)
{
    NVIC_SetPriority(Software_IRQn, 0xff);
    NVIC_EnableIRQ(Software_IRQn);
}

void os_hw_cpu_reset(void)
{
    NVIC_SystemReset();
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial STM32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    hardware_init();

    /* Heap initialization */
#ifdef OS_USING_SYS_HEAP
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_sys_heap_init();
        os_sys_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
    os_dma_mem_init();
#endif

    return OS_EOK;
}
OS_CORE_INIT(os_hw_board_init, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

    _SysTick_Config(SystemCoreClock / OS_TICK_PER_SECOND);

    return OS_EOK;
}
OS_POSTCORE_INIT(board_post_init, OS_INIT_SUBLEVEL_LOW);
