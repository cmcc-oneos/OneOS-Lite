/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_eth.c
 *
 * @brief       This file implements usart devices for ingenic uart
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_eth.h>

#ifdef BSP_USING_RGMII0
struct loongson_eth_info eth0_info = {.baseaddr = (void *)LS2K_GMAC0_BASE,
                                      .irqno    = LS2K_GMAC0_IRQ,
                                      .plb      = 32,
                                      .mac      = {0x64, 0x48, 0x48, 0x48, 0x48, 0x60}};
OS_HAL_DEVICE_DEFINE("ETH_Type", "et0", eth0_info);
#endif

#ifdef BSP_USING_RGMII1
struct loongson_eth_info eth1_info = {.baseaddr = (void *)LS2K_GMAC1_BASE,
                                      .irqno    = LS2K_GMAC1_IRQ,
                                      .plb      = 32,
                                      .mac      = {0x64, 0x48, 0x48, 0x48, 0x48, 0x61}};
OS_HAL_DEVICE_DEFINE("ETH_Type", "et1", eth1_info);
#endif
