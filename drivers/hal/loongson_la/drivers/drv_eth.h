/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.h
 *
 * @brief       The head file of eth driver for loongson
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_ETH_H__
#define __DRV_ETH_H__

#include <ls2k_stmmac.h>
#include <ls2k500_regs.h>
#include <os_types.h>
#include <drv_cfg.h>

#define DEFAULT_BUFSIZE     (1536)
#define STMMAC_RX_COPYBREAK (256)

#define CONFIG_L1_CACHE_SHIFT        (6)
#define SMP_CACHE_BYTES              (1 << CONFIG_L1_CACHE_SHIFT)
#define __ALIGN_KERNEL_MASK(x, mask) (((x) + (mask)) & ~(mask))
#define __ALIGN_KERNEL(x, a)         __ALIGN_KERNEL_MASK(x, (typeof(x))(a)-1)
#define ALIGN(x, a)                  __ALIGN_KERNEL((x), (a))
#define STMMAC_ALIGN(x)              ALIGN(ALIGN(x, SMP_CACHE_BYTES), 16)

#define stmmac_do_void_callback(device, __module, __cname, __arg0, __args...)                                          \
    ({                                                                                                                 \
        int __result = -1;                                                                                             \
        if ((device)->hw->__module && (device)->hw->__module->__cname)                                                 \
        {                                                                                                              \
            (device)->hw->__module->__cname((__arg0), ##__args);                                                       \
            __result = 0;                                                                                              \
        }                                                                                                              \
        __result;                                                                                                      \
    })

#define stmmac_do_callback(device, __module, __cname, __arg0, __args...)                                               \
    ({                                                                                                                 \
        int __result = -1;                                                                                             \
        if ((device)->hw->__module && (device)->hw->__module->__cname)                                                 \
            __result = (device)->hw->__module->__cname((__arg0), ##__args);                                            \
        __result;                                                                                                      \
    })

#define stmmac_core_init(device, __args...)        stmmac_do_void_callback(device, mac, core_init, __args)
#define stmmac_mac_set(device, __args...)          stmmac_do_void_callback(device, mac, set_mac, __args)
#define stmmac_rx_ipc(device, __args...)           stmmac_do_callback(device, mac, rx_ipc, __args)
#define stmmac_rx_queue_enable(device, __args...)  stmmac_do_void_callback(device, mac, rx_queue_enable, __args)
#define stmmac_rx_queue_prio(device, __args...)    stmmac_do_void_callback(device, mac, rx_queue_prio, __args)
#define stmmac_tx_queue_prio(device, __args...)    stmmac_do_void_callback(device, mac, tx_queue_prio, __args)
#define stmmac_rx_queue_routing(device, __args...) stmmac_do_void_callback(device, mac, rx_queue_routing, __args)
#define stmmac_prog_mtl_rx_algorithms(device, __args...)                                                               \
    stmmac_do_void_callback(device, mac, prog_mtl_rx_algorithms, __args)
#define stmmac_prog_mtl_tx_algorithms(device, __args...)                                                               \
    stmmac_do_void_callback(device, mac, prog_mtl_tx_algorithms, __args)
#define stmmac_set_mtl_tx_queue_weight(device, __args...)                                                              \
    stmmac_do_void_callback(device, mac, set_mtl_tx_queue_weight, __args)
#define stmmac_map_mtl_to_dma(device, __args...)         stmmac_do_void_callback(device, mac, map_mtl_to_dma, __args)
#define stmmac_config_cbs(device, __args...)             stmmac_do_void_callback(device, mac, config_cbs, __args)
#define stmmac_dump_mac_regs(device, __args...)          stmmac_do_void_callback(device, mac, dump_regs, __args)
#define stmmac_host_irq_status(device, __args...)        stmmac_do_callback(device, mac, host_irq_status, __args)
#define stmmac_host_mtl_irq_status(device, __args...)    stmmac_do_callback(device, mac, host_mtl_irq_status, __args)
#define stmmac_set_filter(device, __args...)             stmmac_do_void_callback(device, mac, set_filter, __args)
#define stmmac_flow_ctrl(device, __args...)              stmmac_do_void_callback(device, mac, flow_ctrl, __args)
#define stmmac_pmt(device, __args...)                    stmmac_do_void_callback(device, mac, pmt, __args)
#define stmmac_set_umac_addr(device, __args...)          stmmac_do_void_callback(device, mac, set_umac_addr, __args)
#define stmmac_get_umac_addr(device, __args...)          stmmac_do_void_callback(device, mac, get_umac_addr, __args)
#define stmmac_set_eee_mode(device, __args...)           stmmac_do_void_callback(device, mac, set_eee_mode, __args)
#define stmmac_reset_eee_mode(device, __args...)         stmmac_do_void_callback(device, mac, reset_eee_mode, __args)
#define stmmac_set_eee_timer(device, __args...)          stmmac_do_void_callback(device, mac, set_eee_timer, __args)
#define stmmac_set_eee_pls(device, __args...)            stmmac_do_void_callback(device, mac, set_eee_pls, __args)
#define stmmac_mac_debug(device, __args...)              stmmac_do_void_callback(device, mac, debug, __args)
#define stmmac_pcs_ctrl_ane(device, __args...)           stmmac_do_void_callback(device, mac, pcs_ctrl_ane, __args)
#define stmmac_pcs_rane(device, __args...)               stmmac_do_void_callback(device, mac, pcs_rane, __args)
#define stmmac_pcs_get_adv_lp(device, __args...)         stmmac_do_void_callback(device, mac, pcs_get_adv_lp, __args)
#define stmmac_safety_feat_config(device, __args...)     stmmac_do_callback(device, mac, safety_feat_config, __args)
#define stmmac_safety_feat_irq_status(device, __args...) stmmac_do_callback(device, mac, safety_feat_irq_status, __args)
#define stmmac_safety_feat_dump(device, __args...)       stmmac_do_callback(device, mac, safety_feat_dump, __args)
#define stmmac_rxp_config(device, __args...)             stmmac_do_callback(device, mac, rxp_config, __args)
#define stmmac_flex_pps_config(device, __args...)        stmmac_do_callback(device, mac, flex_pps_config, __args)

struct stmmac_tc_ops
{
    int (*init)(struct os_loongson_eth *device);
    int (*setup_cls_u32)(struct os_loongson_eth *device);
    int (*setup_cbs)(struct os_loongson_eth *device);
};

#define stmmac_tc_init(device, __args...)          stmmac_do_callback(device, tc, init, __args)
#define stmmac_tc_setup_cls_u32(device, __args...) stmmac_do_callback(device, tc, setup_cls_u32, __args)
#define stmmac_tc_setup_cbs(device, __args...)     stmmac_do_callback(device, tc, setup_cbs, __args)

struct stmmac_mode_ops
{
    void (*init)(void *des, dma_addr_t phy_addr, unsigned int size, unsigned int extend_desc);
    unsigned int (*is_jumbo_frm)(int len, int ehn_desc);
    int (*jumbo_frm)(void *priv, int csum);
    int (*set_16kib_bfsize)(int mtu);
    void (*init_desc3)(struct dma_desc *p);
    void (*refill_desc3)(void *priv, struct dma_desc *p);
    void (*clean_desc3)(void *priv, struct dma_desc *p);
};

#define stmmac_mode_init(device, __args...)        stmmac_do_void_callback(device, mode, init, __args)
#define stmmac_is_jumbo_frm(device, __args...)     stmmac_do_callback(device, mode, is_jumbo_frm, __args)
#define stmmac_jumbo_frm(device, __args...)        stmmac_do_callback(device, mode, jumbo_frm, __args)
#define stmmac_set_16kib_bfsize(device, __args...) stmmac_do_callback(device, mode, set_16kib_bfsize, __args)
#define stmmac_init_desc3(device, __args...)       stmmac_do_void_callback(device, mode, init_desc3, __args)
#define stmmac_refill_desc3(device, __args...)     stmmac_do_void_callback(device, mode, refill_desc3, __args)
#define stmmac_clean_desc3(device, __args...)      stmmac_do_void_callback(device, mode, clean_desc3, __args)

struct stmmac_dma_ops
{
    /* DMA core initialization */
    int (*reset)(void *ioaddr);
    void (*init)(void *ioaddr, struct stmmac_dma_cfg *dma_cfg, int atds);
    void (*init_chan)(void *ioaddr, struct stmmac_dma_cfg *dma_cfg, unsigned int chan);
    void (*init_rx_chan)(void *ioaddr, struct stmmac_dma_cfg *dma_cfg, unsigned int dma_rx_phy, unsigned int chan);
    void (*init_tx_chan)(void *ioaddr, struct stmmac_dma_cfg *dma_cfg, unsigned int dma_tx_phy, unsigned int chan);
    /* Configure the AXI Bus Mode Register */
    void (*axi)(void *ioaddr);
    /* Dump DMA registers */
    void (*dump_regs)(void *ioaddr, unsigned int *reg_space);
    void (*dma_rx_mode)(void *ioaddr, int mode, unsigned int channel, int fifosz, unsigned char qmode);
    void (*dma_tx_mode)(void *ioaddr, int mode, unsigned int channel, int fifosz, unsigned char qmode);
    /* To track extra statistic (if supported) */
    void (*dma_diagnostic_fr)(void *datavoid, void *ioaddr);
    void (*enable_dma_transmission)(void *ioaddr);
    void (*enable_dma_irq)(void *ioaddr, unsigned int chan);
    void (*disable_dma_irq)(void *ioaddr, unsigned int chan);
    void (*start_tx)(void *ioaddr, unsigned int chan);
    void (*stop_tx)(void *ioaddr, unsigned int chan);
    void (*start_rx)(void *ioaddr, unsigned int chan);
    void (*stop_rx)(void *ioaddr, unsigned int chan);
    int (*dma_interrupt)(void *ioaddr, unsigned int chan);
    /* If supported then get the optional core features */
    void (*get_hw_feature)(void *ioaddr, struct dma_features *dma_cap);
    /* Program the HW RX Watchdog */
    void (*rx_watchdog)(void *ioaddr, unsigned int riwt, unsigned int number_chan);
    void (*set_tx_ring_len)(void *ioaddr, unsigned int len, unsigned int chan);
    void (*set_rx_ring_len)(void *ioaddr, unsigned int len, unsigned int chan);
    void (*set_rx_tail_ptr)(void *ioaddr, unsigned int tail_ptr, unsigned int chan);
    void (*set_tx_tail_ptr)(void *ioaddr, unsigned int tail_ptr, unsigned int chan);
    void (*enable_tso)(void *ioaddr, unsigned int en, unsigned int chan);
    void (*qmode)(void *ioaddr, unsigned int channel, unsigned char qmode);
    void (*set_bfsize)(void *ioaddr, int bfsize, unsigned int chan);
};

#define stmmac_reset(device, __args...)             stmmac_do_callback(device, dma, reset, __args)
#define stmmac_dma_init(device, __args...)          stmmac_do_void_callback(device, dma, init, __args)
#define stmmac_init_chan(device, __args...)         stmmac_do_void_callback(device, dma, init_chan, __args)
#define stmmac_init_rx_chan(device, __args...)      stmmac_do_void_callback(device, dma, init_rx_chan, __args)
#define stmmac_init_tx_chan(device, __args...)      stmmac_do_void_callback(device, dma, init_tx_chan, __args)
#define stmmac_axi(device, __args...)               stmmac_do_void_callback(device, dma, axi, __args)
#define stmmac_dump_dma_regs(device, __args...)     stmmac_do_void_callback(device, dma, dump_regs, __args)
#define stmmac_dma_rx_mode(device, __args...)       stmmac_do_void_callback(device, dma, dma_rx_mode, __args)
#define stmmac_dma_tx_mode(device, __args...)       stmmac_do_void_callback(device, dma, dma_tx_mode, __args)
#define stmmac_dma_diagnostic_fr(device, __args...) stmmac_do_void_callback(device, dma, dma_diagnostic_fr, __args)
#define stmmac_enable_dma_transmission(device, __args...)                                                              \
    stmmac_do_void_callback(device, dma, enable_dma_transmission, __args)
#define stmmac_enable_dma_irq(device, __args...)       stmmac_do_void_callback(device, dma, enable_dma_irq, __args)
#define stmmac_disable_dma_irq(device, __args...)      stmmac_do_void_callback(device, dma, disable_dma_irq, __args)
#define stmmac_start_tx(device, __args...)             stmmac_do_void_callback(device, dma, start_tx, __args)
#define stmmac_stop_tx(device, __args...)              stmmac_do_void_callback(device, dma, stop_tx, __args)
#define stmmac_start_rx(device, __args...)             stmmac_do_void_callback(device, dma, start_rx, __args)
#define stmmac_stop_rx(device, __args...)              stmmac_do_void_callback(device, dma, stop_rx, __args)
#define stmmac_dma_interrupt_status(device, __args...) stmmac_do_callback(device, dma, dma_interrupt, __args)
#define stmmac_get_hw_feature(device, __args...)       stmmac_do_void_callback(device, dma, get_hw_feature, __args)
#define stmmac_rx_watchdog(device, __args...)          stmmac_do_void_callback(device, dma, rx_watchdog, __args)
#define stmmac_set_tx_ring_len(device, __args...)      stmmac_do_void_callback(device, dma, set_tx_ring_len, __args)
#define stmmac_set_rx_ring_len(device, __args...)      stmmac_do_void_callback(device, dma, set_rx_ring_len, __args)
#define stmmac_set_rx_tail_ptr(device, __args...)      stmmac_do_void_callback(device, dma, set_rx_tail_ptr, __args)
#define stmmac_set_tx_tail_ptr(device, __args...)      stmmac_do_void_callback(device, dma, set_tx_tail_ptr, __args)
#define stmmac_enable_tso(device, __args...)           stmmac_do_void_callback(device, dma, enable_tso, __args)
#define stmmac_dma_qmode(device, __args...)            stmmac_do_void_callback(device, dma, qmode, __args)
#define stmmac_set_dma_bfsize(device, __args...)       stmmac_do_void_callback(device, dma, set_bfsize, __args)

struct stmmac_desc_ops
{
    /* DMA RX descriptor ring initialization */
    void (*init_rx_desc)(struct dma_desc *p, int disable_rx_ic, int mode, int end, int bfsize);
    /* DMA TX descriptor ring initialization */
    void (*init_tx_desc)(struct dma_desc *p, int mode, int end);
    /* Invoked by the xmit function to prepare the tx descriptor */
    void (*prepare_tx_desc)(struct dma_desc *p,
                            int              is_fs,
                            int              len,
                            unsigned int     csum_flag,
                            int              mode,
                            unsigned int     tx_own,
                            unsigned int     ls,
                            unsigned int     tot_pkt_len);
    void (*prepare_tso_tx_desc)(struct dma_desc *p,
                                int              is_fs,
                                int              len1,
                                int              len2,
                                unsigned int     tx_own,
                                unsigned int     ls,
                                unsigned int     tcphdrlen,
                                unsigned int     tcppayloadlen);
    /* Set/get the owner of the descriptor */
    void (*set_tx_owner)(struct dma_desc *p);
    int (*get_tx_owner)(struct dma_desc *p);
    /* Clean the tx descriptor as soon as the tx irq is received */
    void (*release_tx_desc)(struct dma_desc *p, int mode);
    /* Clear interrupt on tx frame completion. When this bit is
     * set an interrupt happens as soon as the frame is transmitted */
    void (*set_tx_ic)(struct dma_desc *p);
    /* Last tx segment reports the transmit status */
    int (*get_tx_ls)(struct dma_desc *p);
    /* Return the transmit status looking at the TDES1 */
    int (*tx_status)(struct dma_desc *p, void *ioaddr);
    /* Get the buffer size from the descriptor */
    int (*get_tx_len)(struct dma_desc *p);
    /* Handle extra events on specific interrupts hw dependent */
    void (*set_rx_owner)(struct dma_desc *p, int disable_rx_ic);
    /* Get the receive frame size */
    int (*get_rx_frame_len)(struct dma_desc *p, int rx_coe_type);
    /* Return the reception status looking at the RDES1 */
    int (*rx_status)(struct dma_desc *p);
    void (*rx_extended_status)(void *data, struct dma_extended_desc *p);
    /* Set tx timestamp enable bit */
    void (*enable_tx_timestamp)(struct dma_desc *p);
    /* get tx timestamp status */
    int (*get_tx_timestamp_status)(struct dma_desc *p);
    /* get timestamp value */
    void (*get_timestamp)(void *desc, unsigned int ats, unsigned long *ts);
    /* get rx timestamp status */
    int (*get_rx_timestamp_status)(void *desc, void *next_desc, unsigned int ats);
    /* Display ring */
    void (*display_ring)(void *head, unsigned int size, unsigned int rx);
    /* set MSS via context descriptor */
    void (*set_mss)(struct dma_desc *p, unsigned int mss);
    /* get descriptor skbuff address */
    void (*get_addr)(struct dma_desc *p, unsigned int *addr);
    /* set descriptor skbuff address */
    void (*set_addr)(struct dma_desc *p, dma_addr_t addr);
    /* clear descriptor */
    void (*clear)(struct dma_desc *p);
};

#define stmmac_init_rx_desc(device, __args...)        stmmac_do_void_callback(device, desc, init_rx_desc, __args)
#define stmmac_init_tx_desc(device, __args...)        stmmac_do_void_callback(device, desc, init_tx_desc, __args)
#define stmmac_prepare_tx_desc(device, __args...)     stmmac_do_void_callback(device, desc, prepare_tx_desc, __args)
#define stmmac_prepare_tso_tx_desc(device, __args...) stmmac_do_void_callback(device, desc, prepare_tso_tx_desc, __args)
#define stmmac_set_tx_owner(device, __args...)        stmmac_do_void_callback(device, desc, set_tx_owner, __args)
#define stmmac_get_tx_owner(device, __args...)        stmmac_do_callback(device, desc, get_tx_owner, __args)
#define stmmac_release_tx_desc(device, __args...)     stmmac_do_void_callback(device, desc, release_tx_desc, __args)
#define stmmac_set_tx_ic(device, __args...)           stmmac_do_void_callback(device, desc, set_tx_ic, __args)
#define stmmac_get_tx_ls(device, __args...)           stmmac_do_callback(device, desc, get_tx_ls, __args)
#define stmmac_tx_status(device, __args...)           stmmac_do_callback(device, desc, tx_status, __args)
#define stmmac_get_tx_len(device, __args...)          stmmac_do_callback(device, desc, get_tx_len, __args)
#define stmmac_set_rx_owner(device, __args...)        stmmac_do_void_callback(device, desc, set_rx_owner, __args)
#define stmmac_get_rx_frame_len(device, __args...)    stmmac_do_callback(device, desc, get_rx_frame_len, __args)
#define stmmac_rx_status(device, __args...)           stmmac_do_callback(device, desc, rx_status, __args)
#define stmmac_rx_extended_status(device, __args...)  stmmac_do_void_callback(device, desc, rx_extended_status, __args)
#define stmmac_enable_tx_timestamp(device, __args...) stmmac_do_void_callback(device, desc, enable_tx_timestamp, __args)
#define stmmac_get_tx_timestamp_status(device, __args...)                                                              \
    stmmac_do_callback(device, desc, get_tx_timestamp_status, __args)
#define stmmac_get_timestamp(device, __args...) stmmac_do_void_callback(device, desc, get_timestamp, __args)
#define stmmac_get_rx_timestamp_status(device, __args...)                                                              \
    stmmac_do_callback(device, desc, get_rx_timestamp_status, __args)
#define stmmac_display_ring(device, __args...)  stmmac_do_void_callback(device, desc, display_ring, __args)
#define stmmac_set_mss(device, __args...)       stmmac_do_void_callback(device, desc, set_mss, __args)
#define stmmac_get_desc_addr(device, __args...) stmmac_do_void_callback(device, desc, get_addr, __args)
#define stmmac_set_desc_addr(device, __args...) stmmac_do_void_callback(device, desc, set_addr, __args)
#define stmmac_clear_desc(device, __args...)    stmmac_do_void_callback(device, desc, clear, __args)

struct stmmac_hwtimestamp
{
    void (*config_hw_tstamping)(void *ioaddr, unsigned int data);
    void (*config_sub_second_increment)(void *ioaddr, unsigned int ptp_clock, int gmac4, unsigned int *ssinc);
    int (*init_systime)(void *ioaddr, unsigned int sec, unsigned int nsec);
    int (*config_addend)(void *ioaddr, unsigned int addend);
    int (*adjust_systime)(void *ioaddr, unsigned int sec, unsigned int nsec, int add_sub, int gmac4);
    void (*get_systime)(void *ioaddr, unsigned long *systime);
};

#define stmmac_config_hw_tstamping(device, __args...) stmmac_do_void_callback(device, ptp, config_hw_tstamping, __args)
#define stmmac_config_sub_second_increment(device, __args...)                                                          \
    stmmac_do_void_callback(device, ptp, config_sub_second_increment, __args)
#define stmmac_init_systime(device, __args...)   stmmac_do_callback(device, ptp, init_systime, __args)
#define stmmac_config_addend(device, __args...)  stmmac_do_callback(device, ptp, config_addend, __args)
#define stmmac_adjust_systime(device, __args...) stmmac_do_callback(device, ptp, adjust_systime, __args)
#define stmmac_get_systime(device, __args...)    stmmac_do_void_callback(device, ptp, get_systime, __args)

#endif
