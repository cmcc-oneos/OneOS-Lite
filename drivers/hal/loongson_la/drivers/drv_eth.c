/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       The driver file of eth driver for loongson
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <drv_eth.h>
#include <os_memory.h>
#include <string.h>
#include <bus/bus.h>
#include <os_mb.h>
#include <os_list.h>
#include <os_mutex.h>
#include <interrupt.h>
#include <driver.h>
#include <os_mb.h>
#include <netif/ethernetif.h>
#include "lwipopts.h"

//#define ETH_DEBUG_ENABLE

#if defined(ETH_DEBUG_ENABLE)
static struct os_mb     eth_rx_task_mb_test;
struct os_loongson_eth *device_debug;
static char             eth_rx_task_mb_pool_test[512 * 8];
#endif

static int                      buf_sz = DEFAULT_BUFSIZE;
extern struct stmmac_hwif_entry stmmac_hw;

#if defined(ETH_DEBUG_ENABLE)
#if 0
static void print_pkt(unsigned int entry,unsigned char *buf, int len)
{
    int j;
    os_kprintf("entry = %d,len = %d byte, buf addr: 0x%p",entry, len, buf);
    for (j = 0; j < len; j++) 
    {
        if ((j % 16) == 0)
        {
            os_kprintf("\n\r %03x:", j);
        }
        os_kprintf(" %02x", buf[j]);
    }
    os_kprintf("\n\r");
}
#endif
#endif

static void stmmac_adjust_link(struct os_loongson_eth *device)
{
    struct phy_device *phydev    = &device->phy_dev;
    unsigned int       new_state = 0;

    if (phydev->link)
    {
        unsigned int ctrl = readl(device->plat_dat.addr + MAC_CTRL_REG);

        /* Now we make sure that we can be in full duplex mode.
         * If not, we operate in half-duplex mode. */
        if (phydev->duplex != device->phy_dev.oldduplex)
        {
            new_state = 1;
            if (!phydev->duplex)
            {
                ctrl &= ~device->hw->link.duplex;
            }
            else
            {
                ctrl |= device->hw->link.duplex;
            }
            device->phy_dev.oldduplex = phydev->duplex;
        }

        if (phydev->speed != phydev->oldspeed)
        {
            new_state = 1;
            ctrl &= ~device->hw->link.speed_mask;
            switch (phydev->speed)
            {
            case SPEED_1000:
                ctrl |= device->hw->link.speed1000;
                break;
            case SPEED_100:
                ctrl |= device->hw->link.speed100;
                break;
            case SPEED_10:
                ctrl |= device->hw->link.speed10;
                break;
            default:
                phydev->speed = SPEED_UNKNOWN;
                break;
            }
            phydev->oldspeed = phydev->speed;
        }

        writel(ctrl, device->plat_dat.addr + MAC_CTRL_REG);

        if (!device->phy_dev.oldlink)
        {
            new_state               = 1;
            device->phy_dev.oldlink = 1;
        }
    }
    else if (device->phy_dev.oldlink)
    {
        new_state                 = 1;
        device->phy_dev.oldlink   = 0;
        device->phy_dev.oldspeed  = SPEED_UNKNOWN;
        device->phy_dev.oldduplex = DUPLEX_UNKNOWN;
    }

    if (new_state)
    {
        os_kprintf("phy link %s\n\r", phydev->link ? "up" : "down");
        os_net_linkchange(&device->net_dev, phydev->link ? OS_TRUE : OS_FALSE);
    }
    return;
}

static void phy_prepare_link(struct phy_device *phydev, void (*handler)(struct os_loongson_eth *))
{
    phydev->adjust_link = handler;
    return;
}

void phy_link_change(struct phy_device *phydev, unsigned int up, unsigned int do_carrier)
{
    struct os_loongson_eth *device = phydev->device;
    phydev->adjust_link(device);
    return;
}

int phy_attach_direct(struct os_loongson_eth *device,
                      struct phy_device      *phydev,
                      unsigned int            flags,
                      phy_interface_t         interface)
{
    int err;

    phydev->phy_link_change = phy_link_change;
    phydev->interface       = interface;
    phydev->state           = PHY_READY;

    err = phy_init_hw(phydev);
    if (err)
    {
        return OS_ERROR;
    }

    phy_resume(phydev);

    return err;
}

void phy_start(struct phy_device *phydev)
{
    os_mutex_lock(&phydev->lock, OS_WAIT_FOREVER);

    switch (phydev->state)
    {
    case PHY_STARTING:
        phydev->state = PHY_PENDING;
        break;
    case PHY_READY:
        phydev->state = PHY_UP;
        break;
    case PHY_HALTED:
        __phy_resume(phydev);
        phydev->state = PHY_RESUMING;
        break;
    default:
        break;
    }
    os_mutex_unlock(&phydev->lock);

    phy_trigger_machine(phydev);

    return;
}

static void phy_link_up(struct phy_device *phydev)
{
    phydev->phy_link_change(phydev, 1, 1);
    return;
}

static void phy_link_down(struct phy_device *phydev, unsigned int do_carrier)
{
    phydev->phy_link_change(phydev, 0, do_carrier);
    return;
}

static void phy_state_machine(void *parameter)
{
    int                err = 0;
    int                old_link;
    struct phy_device *phydev     = (struct phy_device *)parameter;
    unsigned int       needs_aneg = 0;

    while (1)
    {
        os_sem_wait(&phydev->sem, OS_TICK_PER_SECOND);
        os_mutex_lock(&phydev->lock, OS_WAIT_FOREVER);
        needs_aneg = 0;

        switch (phydev->state)
        {
        case PHY_DOWN:
        case PHY_STARTING:
        case PHY_READY:
        case PHY_PENDING:
            break;
        case PHY_UP:
            needs_aneg           = 1;
            phydev->link_timeout = PHY_AN_TIMEOUT;
            break;
        case PHY_AN:
            err = phy_read_status(phydev);
            if (err < 0)
            {
                break;
            }
            /* If the link is down, give up on negotiation for now */
            if (!phydev->link)
            {
                phydev->state = PHY_NOLINK;
                phy_link_down(phydev, 1);
                break;
            }
            /* Check if negotiation is done.  Break if there's an error */
            err = phy_aneg_done(phydev);
            if (err < 0)
            {
                break;
            }
            /* If AN is done, we're running */
            if (err > 0)
            {
                phydev->state = PHY_RUNNING;
                phy_link_up(phydev);
            }
            else if (0 == phydev->link_timeout--)
            {
                needs_aneg = 1;
            }
            break;
        case PHY_NOLINK:
            err = phy_read_status(phydev);
            if (err)
            {
                break;
            }
            if (phydev->link)
            {
                if (AUTONEG_ENABLE == phydev->autoneg)
                {
                    err = phy_aneg_done(phydev);
                    if (err < 0)
                    {
                        break;
                    }
                    if (!err)
                    {
                        phydev->state        = PHY_AN;
                        phydev->link_timeout = PHY_AN_TIMEOUT;
                        break;
                    }
                }
                phydev->state = PHY_RUNNING;
                phy_link_up(phydev);
            }
            break;
        case PHY_FORCING:
            err = genphy_update_link(phydev);
            if (err)
            {
                break;
            }
            if (phydev->link)
            {
                phydev->state = PHY_RUNNING;
                phy_link_up(phydev);
            }
            else
            {
                if (0 == phydev->link_timeout--)
                {
                    needs_aneg = 1;
                }
                phy_link_down(phydev, 0);
            }
            break;
        case PHY_RUNNING:
            /* Only register a CHANGE if we are polling and link changed
             * since latest checking.
             */
            old_link = phydev->link;
            err      = phy_read_status(phydev);
            if (err)
            {
                break;
            }
            if (old_link != phydev->link)
            {
                phydev->state = PHY_CHANGELINK;
            }
            /*
             * Failsafe: check that nobody set phydev->link=0 between two
             * poll cycles, otherwise we won't leave RUNNING state as long
             * as link remains down.
             */
            if (!phydev->link && phydev->state == PHY_RUNNING)
            {
                phydev->state = PHY_CHANGELINK;
            }
            break;
        case PHY_CHANGELINK:
            err = phy_read_status(phydev);
            if (err)
            {
                break;
            }
            if (phydev->link)
            {
                phydev->state = PHY_RUNNING;
                phy_link_up(phydev);
            }
            else
            {
                phydev->state = PHY_NOLINK;
                phy_link_down(phydev, 1);
            }
            break;
        case PHY_HALTED:
            if (phydev->link)
            {
                phydev->link = 0;
                phy_link_down(phydev, 1);
            }
            break;
        case PHY_RESUMING:
            if (AUTONEG_ENABLE == phydev->autoneg)
            {
                err = phy_aneg_done(phydev);
                if (err < 0)
                {
                    break;
                }
                /* err > 0 if AN is done.
                 * Otherwise, it's 0, and we're  still waiting for AN
                 */
                if (err > 0)
                {
                    err = phy_read_status(phydev);
                    if (err)
                    {
                        break;
                    }
                    if (phydev->link)
                    {
                        phydev->state = PHY_RUNNING;
                        phy_link_up(phydev);
                    }
                    else
                    {
                        phydev->state = PHY_NOLINK;
                        phy_link_down(phydev, 0);
                    }
                }
                else
                {
                    phydev->state        = PHY_AN;
                    phydev->link_timeout = PHY_AN_TIMEOUT;
                }
            }
            else
            {
                err = phy_read_status(phydev);
                if (err)
                {
                    break;
                }

                if (phydev->link)
                {
                    phydev->state = PHY_RUNNING;
                    phy_link_up(phydev);
                }
                else
                {
                    phydev->state = PHY_NOLINK;
                    phy_link_down(phydev, 0);
                }
            }
            break;
        }

        os_mutex_unlock(&phydev->lock);

        if (needs_aneg)
        {
            err = phy_start_aneg_priv(phydev, 0);
        }
    }
}

void phy_start_machine(struct phy_device *phydev)
{
    os_sem_init(&phydev->sem, OS_NULL, 0, 1);
    phydev->task = os_task_create("phy_poll", phy_state_machine, (void *)phydev, 4096, 4);

    if (phydev->task != OS_NULL)
    {
        os_task_startup(phydev->task);
    }

    return;
}

int phy_connect_direct(struct os_loongson_eth *device,
                       struct phy_device      *phydev,
                       void (*handler)(struct os_loongson_eth *),
                       phy_interface_t interface)
{
    int rc;

    rc = phy_attach_direct(device, phydev, phydev->dev_flags, interface);
    if (rc)
    {
        return rc;
    }

    phy_prepare_link(phydev, handler);
    phy_start_machine(phydev);

    return 0;
}

int phy_connect(struct os_loongson_eth *device, void (*handler)(struct os_loongson_eth *), phy_interface_t interface)
{
    int rc;

    rc = phy_connect_direct(device, &device->phy_dev, handler, interface);

    return rc;
}

static int stmmac_init_phy(struct os_loongson_eth *device)
{
    device->phy_dev.oldlink   = 0;
    device->phy_dev.oldspeed  = SPEED_UNKNOWN;
    device->phy_dev.oldduplex = DUPLEX_UNKNOWN;
    device->phy_dev.autoneg   = AUTONEG_ENABLE;
    device->phy_dev.device    = device;

    os_mutex_init(&device->phy_dev.lock, OS_NULL, OS_FALSE);

    phy_connect(device, &stmmac_adjust_link, device->plat_dat.interface);

    return 0;
}

static int alloc_dma_rx_desc_resources(struct os_loongson_eth *priv)
{
    unsigned int            queue;
    struct stmmac_rx_queue *rx_q;
    unsigned int            rx_count = priv->plat_dat.rx_queues_to_use;
    int                     ret      = OS_ENOMEM;

    /* RX queues buffers and DMA */
    for (queue = 0; queue < rx_count; queue++)
    {
        rx_q              = &priv->rx_queue[queue];
        rx_q->queue_index = queue;
        rx_q->priv_data   = priv;
        rx_q->rx_buff_dma = os_aligned_malloc(STMMAC_ALIGN_SIZE, DMA_RX_SIZE * sizeof(dma_addr_t));

        if (!rx_q->rx_buff_dma)
            goto err_dma;

        memset((void *)rx_q->rx_buff_dma, 0, DMA_RX_SIZE * sizeof(dma_addr_t));
        rx_q->rx_buff = os_aligned_malloc(STMMAC_ALIGN_SIZE, DMA_RX_SIZE * sizeof(unsigned char *));

        if (!rx_q->rx_buff)
            goto err_dma;

        memset((void *)rx_q->rx_buff, 0, DMA_RX_SIZE * sizeof(unsigned char *));
        rx_q->dma_erx    = os_aligned_malloc(STMMAC_DMA_ALIGN_SIZE, DMA_RX_SIZE * sizeof(struct dma_extended_desc));
        rx_q->dma_erx    = (struct dma_extended_desc *)CACHED_TO_UNCACHED(rx_q->dma_erx);
        rx_q->dma_rx_phy = VA_TO_PHYS(rx_q->dma_erx);

        if (!rx_q->dma_erx)
            goto err_dma;

        memset((void *)rx_q->dma_erx, 0, DMA_RX_SIZE * sizeof(struct dma_extended_desc));
    }

    return 0;

err_dma:

    return ret;
}

static int alloc_dma_tx_desc_resources(struct os_loongson_eth *priv)
{
    struct stmmac_tx_queue *tx_q;
    unsigned int            queue;
    unsigned int            tx_count = priv->plat_dat.tx_queues_to_use;
    int                     ret      = OS_ENOMEM;

    for (queue = 0; queue < tx_count; queue++)
    {
        tx_q              = &priv->tx_queue[queue];
        tx_q->queue_index = queue;
        tx_q->priv_data   = priv;
        tx_q->tx_buff_dma = os_aligned_malloc(STMMAC_ALIGN_SIZE, DMA_TX_SIZE * sizeof(struct stmmac_tx_info));

        if (!tx_q->tx_buff_dma)
            goto err_dma;

        memset((void *)tx_q->tx_buff_dma, 0, DMA_TX_SIZE * sizeof(struct stmmac_tx_info));
        tx_q->tx_buff = os_aligned_malloc(STMMAC_ALIGN_SIZE, DMA_TX_SIZE * sizeof(unsigned char *));

        if (!tx_q->tx_buff)
            goto err_dma;

        memset((void *)tx_q->tx_buff, 0, DMA_TX_SIZE * sizeof(unsigned char *));
        tx_q->dma_etx = os_aligned_malloc(STMMAC_DMA_ALIGN_SIZE, DMA_TX_SIZE * sizeof(struct dma_extended_desc));

        if (!tx_q->dma_etx)
            goto err_dma;

        tx_q->dma_etx    = (struct dma_extended_desc *)CACHED_TO_UNCACHED(tx_q->dma_etx);
        tx_q->dma_tx_phy = VA_TO_PHYS(tx_q->dma_etx);
        memset((void *)tx_q->dma_etx, 0, DMA_TX_SIZE * sizeof(struct dma_extended_desc));
    }

    return 0;

err_dma:

    return ret;
}

static int alloc_dma_desc_resources(struct os_loongson_eth *priv)
{
    int ret = alloc_dma_rx_desc_resources(priv);

    if (ret)
    {
        return ret;
    }

    ret = alloc_dma_tx_desc_resources(priv);

    return ret;
}

static void stmmac_clear_rx_descriptors(struct os_loongson_eth *device, unsigned int queue)
{
    int                     i;
    struct stmmac_rx_queue *rx_q = &device->rx_queue[queue];

    for (i = 0; i < DMA_RX_SIZE; i++)
    {
        stmmac_init_rx_desc(device,
                            &rx_q->dma_erx[i].basic,
                            device->plat_dat.use_riwt,
                            device->plat_dat.mode,
                            (i == DMA_RX_SIZE - 1),
                            device->dma_buf_sz);
    }

    return;
}

static void stmmac_clear_tx_descriptors(struct os_loongson_eth *device, unsigned int queue)
{
    int                     i;
    struct stmmac_tx_queue *tx_q = &device->tx_queue[queue];

    for (i = 0; i < DMA_TX_SIZE; i++)
    {
        stmmac_init_tx_desc(device, &tx_q->dma_etx[i].basic, device->plat_dat.mode, (i == DMA_TX_SIZE - 1));
    }

    return;
}

static int stmmac_set_bfsize(int mtu, int bufsize)
{
    int ret = bufsize;

    if (mtu >= BUF_SIZE_8KiB)
    {
        ret = BUF_SIZE_16KiB;
    }
    else if (mtu >= BUF_SIZE_4KiB)
    {
        ret = BUF_SIZE_8KiB;
    }
    else if (mtu >= BUF_SIZE_2KiB)
    {
        ret = BUF_SIZE_4KiB;
    }
    else if (mtu > DEFAULT_BUFSIZE)
    {
        ret = BUF_SIZE_2KiB;
    }
    else
    {
        ret = DEFAULT_BUFSIZE;
    }

    return ret;
}

static int stmmac_init_rx_buffers(struct os_loongson_eth *priv, struct dma_desc *p, int i, unsigned int queue)
{
    unsigned char          *rx_buf;
    struct stmmac_rx_queue *rx_q = &priv->rx_queue[queue];

    rx_buf = os_aligned_malloc(STMMAC_BUFF_ALIGN_SIZE, priv->dma_buf_sz);

    if (!rx_buf)
    {
        return OS_ENOMEM;
    }

    rx_q->rx_buff[i]     = rx_buf;
    rx_q->rx_buff_dma[i] = VA_TO_PHYS(rx_buf);

    stmmac_set_desc_addr(priv, p, rx_q->rx_buff_dma[i]);

    if (priv->dma_buf_sz == BUF_SIZE_16KiB)
    {
        stmmac_init_desc3(priv, p);
    }

    return 0;
}

static int stmmac_init_tx_buffers(struct os_loongson_eth *priv, struct dma_desc *p, int i, unsigned int queue)
{
    unsigned char          *tx_buf;
    struct stmmac_tx_queue *tx_q = &priv->tx_queue[queue];

    tx_buf = os_aligned_malloc(STMMAC_BUFF_ALIGN_SIZE, priv->dma_buf_sz);

    if (!tx_buf)
    {
        return OS_ENOMEM;
    }

    tx_q->tx_buff[i]         = (unsigned char *)CACHED_TO_UNCACHED(tx_buf);
    tx_q->tx_buff_dma[i].buf = VA_TO_PHYS(tx_buf);

    stmmac_set_desc_addr(priv, p, tx_q->tx_buff_dma[i].buf);

    if (priv->dma_buf_sz == BUF_SIZE_16KiB)
    {
        stmmac_init_desc3(priv, p);
    }

    return 0;
}

static void stmmac_free_rx_buffer(struct os_loongson_eth *priv, unsigned int queue, int i)
{
    struct stmmac_rx_queue *rx_q = &priv->rx_queue[queue];

    if (rx_q->rx_buff[i])
    {
        os_free(rx_q->rx_buff[i]);
        rx_q->rx_buff_dma[i] = 0;
    }
    rx_q->rx_buff[i] = OS_NULL;

    return;
}

static int init_dma_rx_desc_rings(struct os_loongson_eth *device)
{
    int              i;
    int              queue;
    struct dma_desc *p;
    int              bfsize   = 0;
    int              ret      = OS_ENOMEM;
    unsigned int     rx_count = device->plat_dat.rx_queues_to_use;

    bfsize = stmmac_set_16kib_bfsize(device, device->mtu);

    if (bfsize < 0)
    {
        bfsize = 0;
    }

    if (bfsize < BUF_SIZE_16KiB)
    {
        bfsize = stmmac_set_bfsize(device->mtu, device->dma_buf_sz);
    }

    device->dma_buf_sz = bfsize;

    /* RX INITIALIZATION */
    for (queue = 0; queue < rx_count; queue++)
    {
        struct stmmac_rx_queue *rx_q = &device->rx_queue[queue];

        for (i = 0; i < DMA_RX_SIZE; i++)
        {
            p   = &((rx_q->dma_erx + i)->basic);
            ret = stmmac_init_rx_buffers(device, p, i, queue);

            if (ret)
                goto err_init_rx_buffers;
        }

        rx_q->cur_rx   = 0;
        rx_q->dirty_rx = (unsigned int)(i - DMA_RX_SIZE);

        stmmac_clear_rx_descriptors(device, queue);
    }

    buf_sz = bfsize;

    return 0;

err_init_rx_buffers:
    while (queue >= 0)
    {
        while (--i >= 0)
        {
            stmmac_free_rx_buffer(device, queue, i);
        }

        if (queue == 0)
        {
            break;
        }

        i = DMA_RX_SIZE;
        queue--;
    }

    return ret;
}

static int init_dma_tx_desc_rings(struct os_loongson_eth *device)
{
    int                     i;
    unsigned int            queue;
    struct dma_desc        *p;
    struct stmmac_tx_queue *tx_q;
    unsigned int            tx_queue_cnt = device->plat_dat.tx_queues_to_use;

    for (queue = 0; queue < tx_queue_cnt; queue++)
    {
        tx_q = &device->tx_queue[queue];

        for (i = 0; i < DMA_TX_SIZE; i++)
        {
            p = &((tx_q->dma_etx + i)->basic);
            stmmac_clear_desc(device, p);
            tx_q->tx_buff_dma[i].map_as_page  = 0;
            tx_q->tx_buff_dma[i].len          = 0;
            tx_q->tx_buff_dma[i].last_segment = 0;
            stmmac_init_tx_buffers(device, p, i, queue);
            stmmac_set_tx_ic(device, p);
        }

        tx_q->dirty_tx = 0;
        tx_q->cur_tx   = 0;
        tx_q->mss      = 0;
    }

    return 0;
}

static void stmmac_clear_descriptors(struct os_loongson_eth *device)
{
    unsigned int rx_queue_cnt = device->plat_dat.rx_queues_to_use;
    unsigned int tx_queue_cnt = device->plat_dat.tx_queues_to_use;
    unsigned int queue;

    for (queue = 0; queue < rx_queue_cnt; queue++)
    {
        stmmac_clear_rx_descriptors(device, queue);
    }

    for (queue = 0; queue < tx_queue_cnt; queue++)
    {
        stmmac_clear_tx_descriptors(device, queue);
    }

    return;
}

static int init_dma_desc_rings(struct os_loongson_eth *device)
{
    int ret;

    ret = init_dma_rx_desc_rings(device);

    if (ret)
    {
        return ret;
    }

    ret = init_dma_tx_desc_rings(device);

    stmmac_clear_descriptors(device);

    return ret;
}

static int stmmac_init_dma_engine(struct os_loongson_eth *device)
{
    unsigned int            rx_channels_count = device->plat_dat.rx_queues_to_use;
    unsigned int            tx_channels_count = device->plat_dat.tx_queues_to_use;
    struct stmmac_rx_queue *rx_q;
    struct stmmac_tx_queue *tx_q;
    unsigned int            chan = 0;
    int                     atds = 1;
    int                     ret  = 0;

    ret = stmmac_reset(device, device->plat_dat.addr);

    if (ret)
    {
        return ret;
    }

    /* DMA Configuration */
    stmmac_dma_init(device, device->plat_dat.addr, &device->plat_dat.dma_cfg, atds);

    /* DMA RX Channel Configuration */
    for (chan = 0; chan < rx_channels_count; chan++)
    {
        rx_q = &device->rx_queue[chan];
        stmmac_init_rx_chan(device, device->plat_dat.addr, &device->plat_dat.dma_cfg, rx_q->dma_rx_phy, chan);
    }

    /* DMA TX Channel Configuration */
    for (chan = 0; chan < tx_channels_count; chan++)
    {
        tx_q = &device->tx_queue[chan];
        stmmac_init_tx_chan(device, device->plat_dat.addr, &device->plat_dat.dma_cfg, tx_q->dma_tx_phy, chan);
    }

    return ret;
}

static void stmmac_start_rx_dma(struct os_loongson_eth *priv, unsigned int chan)
{
    stmmac_start_rx(priv, priv->plat_dat.addr, chan);

    return;
}

static void stmmac_start_tx_dma(struct os_loongson_eth *priv, unsigned int chan)
{
    stmmac_start_tx(priv, priv->plat_dat.addr, chan);

    return;
}

static void stmmac_start_all_dma(struct os_loongson_eth *priv)
{
    unsigned int rx_channels_count = priv->plat_dat.rx_queues_to_use;
    unsigned int tx_channels_count = priv->plat_dat.tx_queues_to_use;
    unsigned int chan              = 0;

    for (chan = 0; chan < rx_channels_count; chan++)
    {
        stmmac_start_rx_dma(priv, chan);
    }

    for (chan = 0; chan < tx_channels_count; chan++)
    {
        stmmac_start_tx_dma(priv, chan);
    }

    return;
}

static void stmmac_set_rings_length(struct os_loongson_eth *priv)
{
    unsigned int rx_channels_count = priv->plat_dat.rx_queues_to_use;
    unsigned int tx_channels_count = priv->plat_dat.tx_queues_to_use;
    unsigned int chan;

    for (chan = 0; chan < tx_channels_count; chan++)
    {
        stmmac_set_tx_ring_len(priv, priv->plat_dat.addr, (DMA_TX_SIZE - 1), chan);
    }

    /* set RX ring length */
    for (chan = 0; chan < rx_channels_count; chan++)
    {
        stmmac_set_rx_ring_len(priv, priv->plat_dat.addr, (DMA_RX_SIZE - 1), chan);
    }

    return;
}

static void stmmac_rx_queue_dma_chan_map(struct os_loongson_eth *priv)
{
    unsigned int rx_queues_count = priv->plat_dat.rx_queues_to_use;
    unsigned int queue;
    unsigned int chan = 0;

    for (queue = 0; queue < rx_queues_count; queue++)
    {
        stmmac_map_mtl_to_dma(priv, priv->hw, queue, chan);
        chan++;
    }

    return;
}

static void stmmac_mac_enable_rx_queues(struct os_loongson_eth *priv)
{
    unsigned int rx_queues_count = priv->plat_dat.rx_queues_to_use;
    int          queue;
    unsigned int mode;

    for (queue = 0; queue < rx_queues_count; queue++)
    {
        mode = 1;
        stmmac_rx_queue_enable(priv, priv->hw, mode, queue);
    }

    return;
}

static void stmmac_mtl_configuration(struct os_loongson_eth *priv)
{
    /* Map RX MTL to DMA channels */
    stmmac_rx_queue_dma_chan_map(priv);
    /* Enable MAC RX Queues */
    stmmac_mac_enable_rx_queues(priv);

    return;
}

static void stmmac_safety_feat_configuration(struct os_loongson_eth *priv)
{
    return;
}
static void stmmac_dma_operation_mode(struct os_loongson_eth *priv)
{
    unsigned int rx_channels_count = priv->plat_dat.rx_queues_to_use;
    unsigned int tx_channels_count = priv->plat_dat.tx_queues_to_use;
    int          rxfifosz          = priv->plat_dat.rx_fifo_size;
    int          txfifosz          = priv->plat_dat.tx_fifo_size;
    unsigned int txmode            = 0;
    unsigned int rxmode            = 0;
    unsigned int chan              = 0;
    unsigned int qmode             = 0;

    if (rxfifosz == 0)
    {
        rxfifosz = priv->plat_dat.dma_cap.rx_fifo_size;
    }

    if (txfifosz == 0)
    {
        txfifosz = priv->plat_dat.dma_cap.tx_fifo_size;
    }

    /* Adjust for real per queue fifo size */
    rxfifosz /= rx_channels_count;
    txfifosz /= tx_channels_count;

    /*
     * In case of GMAC, SF mode can be enabled
     * to perform the TX COE in HW. This depends on:
     * 1) TX COE if actually supported
     * 2) There is no bugged Jumbo frame support
     *    that needs to not insert csum in the TDES.
     */
    txmode = SF_DMA_MODE;
    rxmode = SF_DMA_MODE;

    /* configure all channels */
    for (chan = 0; chan < rx_channels_count; chan++)
    {
        qmode = 1;
        stmmac_dma_rx_mode(priv, priv->plat_dat.addr, rxmode, chan, rxfifosz, qmode);
        stmmac_set_dma_bfsize(priv, priv->plat_dat.addr, priv->dma_buf_sz, chan);
    }

    for (chan = 0; chan < tx_channels_count; chan++)
    {
        qmode = 1;
        stmmac_dma_tx_mode(priv, priv->plat_dat.addr, txmode, chan, txfifosz, qmode);
    }

    return;
}

static int stmmac_hw_setup(struct os_loongson_eth *device, unsigned int init_ptp)
{
    unsigned int rx_cnt = device->plat_dat.rx_queues_to_use;
    int          ret;

    /* DMA initialization and SW reset */
    ret = stmmac_init_dma_engine(device);

    if (ret < 0)
    {
        return ret;
    }

    /* Copy the MAC addr into the HW  */
    stmmac_set_umac_addr(device, device->hw, device->dev_addr, 0);

    /* Initialize the MAC Core */
    stmmac_core_init(device, device->hw, device);

    /* Initialize MTL*/
    stmmac_mtl_configuration(device);

    /* Initialize Safety Features */
    stmmac_safety_feat_configuration(device);

    ret = stmmac_rx_ipc(device, device->hw);

    if (!ret)
    {
        device->plat_dat.rx_coe = STMMAC_RX_COE_NONE;
        device->hw->rx_csum     = 0;
    }

    /* Enable the MAC Rx/Tx */
    stmmac_mac_set(device, device->plat_dat.addr, 1);

    /* Set the HW DMA mode and the COE */
    stmmac_dma_operation_mode(device);

    if (device->plat_dat.use_riwt)
    {
        ret = stmmac_rx_watchdog(device, device->plat_dat.addr, MAX_DMA_RIWT, rx_cnt);
        if (!ret)
        {
            device->plat_dat.rx_riwt = MAX_DMA_RIWT;
        }
    }

    /* set TX and RX rings length */
    stmmac_set_rings_length(device);

    /* Start the ball rolling... */
    stmmac_start_all_dma(device);

    return 0;
}

static inline unsigned int stmmac_rx_dirty(struct os_loongson_eth *priv, unsigned int queue)
{
    struct stmmac_rx_queue *rx_q = &priv->rx_queue[queue];
    unsigned int            dirty;

    if (rx_q->dirty_rx <= rx_q->cur_rx)
    {
        dirty = rx_q->cur_rx - rx_q->dirty_rx;
    }
    else
    {
        dirty = DMA_RX_SIZE - rx_q->dirty_rx + rx_q->cur_rx;
    }

    return dirty;
}

static inline void stmmac_rx_refill(struct os_loongson_eth *priv, unsigned int queue)
{
    struct dma_desc        *p;
    struct stmmac_rx_queue *rx_q  = &priv->rx_queue[queue];
    int                     dirty = stmmac_rx_dirty(priv, queue);
    unsigned int            entry = rx_q->dirty_rx;

    while (dirty-- > 0)
    {
        p = (struct dma_desc *)(rx_q->dma_erx + entry);
        stmmac_set_desc_addr(priv, p, rx_q->rx_buff_dma[entry]);
        stmmac_refill_desc3(priv, rx_q, p);

        if (rx_q->rx_zeroc_thresh > 0)
        {
            rx_q->rx_zeroc_thresh--;
        }

        stmmac_set_rx_owner(priv, p, priv->plat_dat.use_riwt);
        entry = STMMAC_GET_ENTRY(entry, DMA_RX_SIZE);
    }

    rx_q->dirty_rx = entry;

    return;
}

static os_err_t stmmac_xmit(struct os_loongson_eth *dev, unsigned char *tx_buff, unsigned int tx_len)
{
    int                     csum_insertion = 0;
    unsigned int            queue          = 0;
    unsigned int            last_segment   = 1;
    int                     entry;
    unsigned int            first_entry;
    struct dma_desc        *desc, *first;
    struct stmmac_tx_queue *tx_q;
    dma_addr_t              des;
    os_ubase_t              level = 0;
    level                         = os_irq_lock();
    tx_q                          = &dev->tx_queue[queue];
    entry                         = tx_q->cur_tx;
    first_entry                   = entry;
    desc                          = (struct dma_desc *)(tx_q->dma_etx + entry);
    first                         = desc;

    /* We've used all descriptors we need for this skb, however,
     * advance cur_tx so that it references a fresh descriptor.
     * ndo_start_xmit will fill this descriptor the next time it's
     * called and stmmac_tx_clean may clean up to this descriptor.
     */
    entry        = STMMAC_GET_ENTRY(entry, DMA_TX_SIZE);
    tx_q->cur_tx = entry;

    /* According to the coalesce parameter the IC bit for the latest
     * segment is reset and the timer re-started to clean the tx status.
     * This approach takes care about the fragments: desc is the first
     * element in case of no SG.
     */

    tx_q->tx_count_frames = 0;

    /* Ready to fill the first descriptor and set the OWN bit w/o any
     * problems because all the descriptors are actually ready to be
     * passed to the DMA engine.
     */

    des = tx_q->tx_buff_dma[first_entry].buf;
    stmmac_set_desc_addr(dev, first, des);
    tx_q->tx_buff_dma[first_entry].len          = tx_len;
    tx_q->tx_buff_dma[first_entry].last_segment = last_segment;
    memset(tx_q->tx_buff[first_entry], 0, dev->dma_buf_sz);
    memcpy(tx_q->tx_buff[first_entry], tx_buff, tx_len);
    stmmac_set_desc_addr(dev, first, des);

    /* Prepare the first descriptor setting the OWN bit too */
    stmmac_prepare_tx_desc(dev, first, 1, tx_len, csum_insertion, dev->plat_dat.mode, 1, last_segment, tx_len);

    /* The own bit must be the latest setting done when prepare the
     * descriptor and then barrier is needed to make sure that
     * all is coherent before granting the DMA engine.
     */

    stmmac_enable_dma_transmission(dev, dev->plat_dat.addr);
    tx_q->tx_tail_addr = tx_q->dma_tx_phy + (tx_q->cur_tx * sizeof(*desc));
    stmmac_set_tx_tail_ptr(dev, dev->plat_dat.addr, tx_q->tx_tail_addr, queue);
    os_irq_unlock(level);
    // stmmac_tx_timer_arm(priv, queue);

    return OS_EOK;
}

static int stmmac_tx_clean(struct os_loongson_eth *priv, int budget, unsigned int queue)
{
    struct stmmac_tx_queue *tx_q = &priv->tx_queue[queue];
    unsigned int            entry, count = 0;
    struct dma_desc        *p;
    int                     status;
    os_ubase_t              level;
    level = os_irq_lock();

    entry = tx_q->dirty_tx;
    while ((entry != tx_q->cur_tx) && (count < budget))
    {

        if ((entry <= tx_q->cur_tx) && ((tx_q->cur_tx - entry) < 10))
        {
            break;
        }

        if ((entry > tx_q->cur_tx) && ((DMA_TX_SIZE + tx_q->cur_tx - entry) < 10))
        {
            break;
        }

        p      = (struct dma_desc *)(tx_q->dma_etx + entry);
        status = stmmac_tx_status(priv, p, priv->plat_dat.addr);

        /* Check if the descriptor is owned by the DMA */
        if (status & tx_dma_own)
        {
            break;
        }

        count++;
        stmmac_clean_desc3(priv, tx_q, p);
        stmmac_release_tx_desc(priv, p, priv->plat_dat.mode);
        entry = STMMAC_GET_ENTRY(entry, DMA_TX_SIZE);
    }

    tx_q->dirty_tx = entry;
    os_irq_unlock(level);
    return count;
}

static int stmmac_rx(struct os_loongson_eth *priv, int limit, unsigned int queue)
{
    struct stmmac_rx_queue  *rx_q       = &priv->rx_queue[queue];
    unsigned int             next_entry = rx_q->cur_rx;
    int                      coe        = priv->hw->rx_csum;
    unsigned int             count      = 0;
    struct rx_buf_list_node *rx_node;
    struct dma_desc         *p;
    int                      entry, status;
    unsigned char           *rx_buff;
    int                      frame_len;
    unsigned int             des;

    while (count < limit)
    {

        entry = next_entry;
        p     = (struct dma_desc *)(rx_q->dma_erx + entry);

        /* read the status of the incoming frame */
        status = stmmac_rx_status(priv, p);

        /* check if managed by the DMA otherwise go ahead */
        if (status & dma_own)
        {
            break;
        }

        count++;
        rx_q->cur_rx = STMMAC_GET_ENTRY(rx_q->cur_rx, DMA_RX_SIZE);
        next_entry   = rx_q->cur_rx;

        if (status == discard_frame)
        {
            continue;
        }
        else
        {
            stmmac_get_desc_addr(priv, p, &des);
            frame_len = stmmac_get_rx_frame_len(priv, p, coe);

            /*  If frame length is greater than skb buffer size
             *  (preallocated during init) then the packet is
             *  ignored
             */
            if (frame_len > priv->dma_buf_sz)
            {
                continue;
            }

            /* The zero-copy is always used for all the sizes
             * in case of GMAC4 because it needs
             * to refill the used descriptors, always.
             */
            rx_buff = rx_q->rx_buff[entry];
            rx_q->rx_zeroc_thresh++;
            rx_node = os_malloc(sizeof(struct rx_buf_list_node));

            if (rx_node == OS_NULL)
            {
                continue;
            }

            os_net_xfer_data_t *data = os_net_get_buff(frame_len);
            if (data == OS_NULL)
            {
                continue;
            }

            memcpy(data->buff, rx_buff, frame_len);

            os_net_rx_report_data(&priv->net_dev, data);

#if defined(ETH_DEBUG_ENABLE)
            os_mb_send(&eth_rx_task_mb_test, 0, OS_NO_WAIT);
#endif
        }
    }

    stmmac_rx_refill(priv, queue);

    return count;
}

static void stmmac_napi_poll(void *parameter)
{
    struct stmmac_channel  *ch   = (struct stmmac_channel *)parameter;
    struct os_loongson_eth *priv = (struct os_loongson_eth *)(ch->priv_data);
    int                     work_done, rx_done = 0, tx_done = 0;
    int                     budget = 64;
    unsigned int            chan   = ch->index;

    while (1)
    {
        os_sem_wait(&ch->rx_sem, OS_WAIT_FOREVER);
    loop:
#if 0
        if(ch->has_tx)
        {
            tx_done = stmmac_tx_clean(priv, budget, chan);
        }
#endif

        if (ch->has_rx)
        {
            rx_done = stmmac_rx(priv, budget, chan);
        }

        work_done = rx_done > tx_done ? rx_done : tx_done;
        work_done = work_done > budget ? budget : work_done;

        if (work_done < budget)
        {
            int stat;

            stmmac_enable_dma_irq(priv, priv->plat_dat.addr, chan);
            stat = stmmac_dma_interrupt_status(priv, priv->plat_dat.addr, chan);
            if (stat)
            {
                stmmac_disable_dma_irq(priv, priv->plat_dat.addr, chan);
                goto loop;
            }
        }
        else
        {
            goto loop;
        }

        stmmac_enable_dma_irq(priv, priv->plat_dat.addr, ch->index);
        stmmac_start_rx(priv, priv->plat_dat.addr, ch->index);
    }
    return;
}

static int stmmac_napi_check(struct os_loongson_eth *priv, unsigned int chan)
{
    int                    status     = stmmac_dma_interrupt_status(priv, priv->plat_dat.addr, chan);
    struct stmmac_channel *ch         = &priv->channel[chan];
    unsigned int           needs_work = 0;

    if ((status & handle_rx) && ch->has_rx)
    {
        needs_work = 1;
    }
    else
    {
        status &= ~handle_rx;
    }

    if ((status & handle_tx) && ch->has_tx)
    {
        stmmac_disable_dma_irq(priv, priv->plat_dat.addr, chan);
        stmmac_tx_clean(priv, 64, chan);
        stmmac_enable_dma_irq(priv, priv->plat_dat.addr, chan);
    }
    else
    {
        status &= ~handle_tx;
    }

    if (needs_work)
    {
        stmmac_disable_dma_irq(priv, priv->plat_dat.addr, chan);
        stmmac_stop_rx(priv, priv->plat_dat.addr, chan);
        os_sem_post(&ch->rx_sem);
    }

    return status;
}

static void stmmac_dma_interrupt(struct os_loongson_eth *priv)
{
    unsigned int tx_channel_count  = priv->plat_dat.tx_queues_to_use;
    unsigned int rx_channel_count  = priv->plat_dat.rx_queues_to_use;
    unsigned int channels_to_check = tx_channel_count > rx_channel_count ? tx_channel_count : rx_channel_count;
    unsigned int chan;
    int          status[MTL_MAX_RX_QUEUES];

    /* Make sure we never check beyond our status buffer. */
    if (channels_to_check > ARRAY_SIZE(status))
    {
        channels_to_check = ARRAY_SIZE(status);
    }

    for (chan = 0; chan < channels_to_check; chan++)
    {
        status[chan] = stmmac_napi_check(priv, chan);
    }

    return;
}

static void stmmac_interrupt(int irqno, void *param)
{
    struct os_loongson_eth *device = (struct os_loongson_eth *)param;

    stmmac_dma_interrupt(device);

    return;
}

os_err_t os_loongson_eth_init(struct os_net_device *net_dev)
{
    int                     ret    = 0;
    struct os_loongson_eth *device = (struct os_loongson_eth *)net_dev;

    ret = stmmac_init_phy(device);

    if (ret)
    {
        return OS_ERROR;
    }

    device->dma_buf_sz   = STMMAC_ALIGN(buf_sz);
    device->rx_copybreak = STMMAC_RX_COPYBREAK;
    ret                  = alloc_dma_desc_resources(device);

    if (ret < 0)
    {
        return OS_ERROR;
    }

    ret = init_dma_desc_rings(device);

    if (ret < 0)
    {
        return OS_ERROR;
    }

    ret = stmmac_hw_setup(device, 0);

    if (ret < 0)
    {
        return OS_ERROR;
    }

    phy_start(&device->phy_dev);
    os_hw_interrupt_install(device->eth_info->irqno, stmmac_interrupt, device, dev->name);
    os_hw_interrupt_umask(device->eth_info->irqno);
    os_kprintf("[%s:%d]eth init OK!\n\r", __FUNCTION__, __LINE__);

    return OS_EOK;
}

os_err_t os_loongson_eth_deinit(struct os_net_device *net_dev)
{
    return OS_EOK;
}

os_err_t os_loongson_eth_get_macaddr(struct os_net_device *net_dev, os_uint8_t *addr)
{
    struct os_loongson_eth *device = (struct os_loongson_eth *)net_dev;

    memcpy(addr, device->dev_addr, OS_NET_MAC_LENGTH);

    return OS_EOK;
}
os_err_t os_loongson_eth_write(struct os_net_device *net_dev, os_net_xfer_data_t *data)
{
    struct os_loongson_eth *device = (struct os_loongson_eth *)net_dev;

    os_err_t reval = OS_ERROR;

    reval = stmmac_xmit(device, data->buff, data->size);

    return reval;
}

const static struct os_net_device_ops net_dev_ops = {
    .init       = os_loongson_eth_init,
    .deinit     = os_loongson_eth_deinit,
    .recv       = OS_NULL,
    .send       = os_loongson_eth_write,
    .get_mac    = os_loongson_eth_get_macaddr,
    .set_filter = OS_NULL,
};

int stmmac_hwif_init(struct os_loongson_eth *device)
{
    unsigned int              needs_xgmac = device->plat_dat.has_xgmac;
    unsigned int              needs_gmac4 = device->plat_dat.has_gmac4;
    unsigned int              needs_gmac  = device->plat_dat.has_gmac;
    struct stmmac_hwif_entry *entry;
    struct mac_device_info   *mac;
    unsigned int              needs_setup = 1;
    int                       ret;
    unsigned int              id;

    if (needs_gmac)
    {
        id = stmmac_get_id(device->plat_dat.addr, GMAC_VERSION);
    }
    else if (needs_gmac4 || needs_xgmac)
    {
        id = stmmac_get_id(device->plat_dat.addr, GMAC4_VERSION);
    }
    else
    {
        id = 0;
    }

    /* Save ID for later use */
    device->plat_dat.synopsys_id = id;

    /* Lets assume some safe values first */
    device->plat_dat.ptpaddr = device->plat_dat.addr + (needs_gmac4 ? PTP_GMAC4_OFFSET : PTP_GMAC3_X_OFFSET);
    device->plat_dat.mmcaddr = device->plat_dat.addr + (needs_gmac4 ? MMC_GMAC4_OFFSET : MMC_GMAC3_X_OFFSET);

    /* Check for HW specific setup first */
    mac = os_malloc(sizeof(struct mac_device_info));

    if (!mac)
    {
        return OS_ENOMEM;
    }

    memset(mac, 0, sizeof(struct mac_device_info));
    entry = &stmmac_hw;
    /* Only use generic HW helpers if needed */
    mac->desc                = mac->desc ?: entry->desc;
    mac->dma                 = mac->dma ?: entry->dma;
    mac->mac                 = mac->mac ?: entry->mac;
    mac->ptp                 = mac->ptp ?: entry->hwtimestamp;
    mac->mode                = mac->mode ?: entry->mode;
    mac->tc                  = mac->tc ?: entry->tc;
    mac->ps                  = 0;
    device->hw               = mac;
    device->plat_dat.ptpaddr = device->plat_dat.addr + entry->regs.ptp_off;
    device->plat_dat.mmcaddr = device->plat_dat.addr + entry->regs.mmc_off;

    /* Entry found */
    if (needs_setup)
    {
        ret = entry->setup(device);
        if (ret)
        {
            return ret;
        }
    }

    /* Save quirks, if needed for posterior use */
    device->plat_dat.hwif_quirks = entry->quirks;

    return OS_EOK;
}

static int stmmac_get_hw_features(struct os_loongson_eth *device)
{
    return stmmac_get_hw_feature(device, device->plat_dat.addr, &device->plat_dat.dma_cap) == 0;
}

static int stmmac_hw_init(struct os_loongson_eth *device)
{
    int ret;
    /* Initialize HW Interface */
    ret = stmmac_hwif_init(device);

    if (ret)
    {
        return ret;
    }
    /* Get the HW capability (new GMAC newer than 3.50a) */
    device->plat_dat.hw_cap_support = stmmac_get_hw_features(device);

    if (device->plat_dat.hw_cap_support)
    {

        /* We can override some gmac/dma configuration fields: e.g.
         * enh_desc, tx_coe (e.g. that are passed through the
         * platform) with the values from the HW capability
         * register (if supported).
         */
        device->plat_dat.enh_desc = device->plat_dat.dma_cap.enh_desc;
        device->plat_dat.pmt      = device->plat_dat.dma_cap.pmt_remote_wake_up;
        device->hw->pmt           = device->plat_dat.pmt;

        /* TXCOE doesn't work in thresh DMA mode */
        if (device->plat_dat.force_thresh_dma_mode)
        {
            device->plat_dat.tx_coe = 0;
        }
        else
        {
            device->plat_dat.tx_coe = device->plat_dat.dma_cap.tx_coe;
        }
        /* In case of GMAC4 rx_coe is from HW cap register. */
        device->plat_dat.rx_coe = device->plat_dat.dma_cap.rx_coe;

        if (device->plat_dat.dma_cap.rx_coe_type2)
        {
            device->plat_dat.rx_coe = STMMAC_RX_COE_TYPE2;
        }
        else if (device->plat_dat.dma_cap.rx_coe_type1)
        {
            device->plat_dat.rx_coe = STMMAC_RX_COE_TYPE1;
        }
    }

    if (device->plat_dat.rx_coe)
    {
        device->hw->rx_csum = device->plat_dat.rx_coe;
    }

    /* Run HW quirks, if any */
    if (device->plat_dat.hwif_quirks)
    {
        ret = device->plat_dat.hwif_quirks(device);
        if (ret)
        {
            return ret;
        }
    }

    /* Rx Watchdog is available in the COREs newer than the 3.40.
     * In some case, for example on bugged HW this feature
     * has to be disable and this can be done by passing the
     * riwt_off field from the platform.
     */
    if (((device->plat_dat.synopsys_id >= DWMAC_CORE_3_50) || (device->plat_dat.has_xgmac)) &&
        (!device->plat_dat.riwt_off))
    {
        device->plat_dat.use_riwt = 1;
    }

    return 0;
}

int stmmac_mdio_register(struct os_loongson_eth *device, const char *name)
{
    int i;
    device->mii_bus.device   = device;
    device->mii_bus.read     = &stmmac_mdio_read;
    device->mii_bus.write    = &stmmac_mdio_write;
    device->mii_bus.reset    = OS_NULL;
    device->mii_bus.phy_mask = 0;

    os_mutex_init(&device->mii_bus.mdio_lock, name, OS_FALSE);

    if (device->mii_bus.reset)
    {
        device->mii_bus.reset(&device->mii_bus);
    }

    for (i = 0; i < PHY_MAX_ADDR; i++)
    {
        if ((device->mii_bus.phy_mask & (1 << i)) == 0)
        {
            if (OS_EOK == mdiobus_scan(&device->mii_bus, i, &device->phy_id))
            {
                device->phy_addr = i;
                return OS_EOK;
            }
        }
    }

    return OS_ERROR;
}

#if defined(ETH_DEBUG_ENABLE)
static void eth_test_task(void *parameter)
{
    struct os_loongson_eth *device = (struct os_loongson_eth *)parameter;
    os_base_t               value;
    unsigned char          *recv = OS_NULL;
    unsigned int            size;

    while (1)
    {
        if (os_mb_recv(&eth_rx_task_mb_test, (os_ubase_t *)&value, OS_WAIT_FOREVER) == OS_EOK)
        {
            recv = os_loongson_eth_rx(&device->parent, &size);

            memcpy(recv + 6, device->dev_addr, 6);

            os_loongson_eth_tx(&device->parent, recv, size);
            os_free(recv);
        }
    }

    return;
}
#endif

static int loongson_eth_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    int                     state;
    unsigned int            queue, maxq;
    int                     ret    = 0;
    struct os_loongson_eth *device = OS_NULL;

    device = os_malloc(sizeof(struct os_loongson_eth));

    if (device == OS_NULL)
    {
        os_kprintf("memery malloc failed!\n\r");
        return OS_ERROR;
    }

#if defined(ETH_DEBUG_ENABLE)
    os_mb_init(&eth_rx_task_mb_test, "erxmb", &eth_rx_task_mb_pool_test[0], sizeof(eth_rx_task_mb_pool_test));
    os_task_t *test_task = os_task_create("eth_test_task", eth_test_task, (void *)device, 32768, 2);

    if (test_task)
    {
        os_task_startup(test_task);
    }

    device_debug = device;
#endif
    memset(device, 0, sizeof(struct os_loongson_eth));
    device->eth_info      = dev->info;
    device->mtu           = 1500;
    device->plat_dat.addr = device->eth_info->baseaddr;
    device->plat_dat.irq  = device->eth_info->irqno;
    memcpy(device->dev_addr, device->eth_info->mac, sizeof(device->dev_addr));
    device->plat_dat.phy_addr              = -1;
    device->plat_dat.chain_mode            = 0;
    device->plat_dat.has_gmac              = 1;
    device->plat_dat.has_gmac4             = 0;
    device->plat_dat.has_xgmac             = 0;
    device->plat_dat.force_thresh_dma_mode = 0;
    device->plat_dat.dma_cfg.pbl           = 8;
    device->plat_dat.dma_cfg.txpbl         = 0;
    device->plat_dat.dma_cfg.rxpbl         = 0;
    device->plat_dat.dma_cfg.pblx8         = 1;
    device->plat_dat.dma_cfg.fixed_burst   = 0;
    device->plat_dat.dma_cfg.mixed_burst   = 0;
    device->plat_dat.dma_cfg.aal           = 0;
    device->plat_dat.dma_cap.asp           = 0;

    device->plat_dat.rx_queues_to_use = 0;
    device->plat_dat.tx_queues_to_use = 0;
    device->plat_dat.rx_fifo_size     = 0;
    device->plat_dat.tx_fifo_size     = 0;

    ret = stmmac_hw_init(device);

    if (ret)
    {
        os_free(device);
        return OS_EIO;
    }

    stmmac_tc_init(device, device);

    device->plat_dat.tso_en           = 0;
    device->plat_dat.maxmtu           = 1500;
    device->plat_dat.rx_queues_to_use = 1;
    device->plat_dat.tx_queues_to_use = 1;
    maxq = (device->plat_dat.rx_queues_to_use >= device->plat_dat.tx_queues_to_use) ? device->plat_dat.rx_queues_to_use
                                                                                    : device->plat_dat.tx_queues_to_use;

    for (queue = 0; queue < maxq; queue++)
    {
        struct stmmac_channel *ch = &device->channel[queue];

        ch->priv_data = device;
        ch->index     = queue;

        if (queue < device->plat_dat.rx_queues_to_use)
            ch->has_rx = 1;
        if (queue < device->plat_dat.tx_queues_to_use)
            ch->has_tx = 1;

        os_sem_init(&ch->rx_sem, OS_NULL, 0, 1);

        ch->rx_task = os_task_create("recv", stmmac_napi_poll, (void *)ch, 32768, 1);

        if (ch->rx_task != OS_NULL)
        {
            os_task_startup(ch->rx_task);
        }
    }

    device->plat_dat.has_sun8i = 0;
    device->plat_dat.has_xgmac = 0;
    device->plat_dat.clk_csr   = 0;

    stmmac_clk_csr_set(&device->plat_dat.clk_csr);

    device->hw->pcs            = 0;
    device->plat_dat.interface = PHY_INTERFACE_MODE_RGMII;

    ret = stmmac_mdio_register(device, dev->name);

    if (ret < 0)
    {
        os_free(device);
        return OS_EIO;
    }

    device->net_dev.info.MTU       = device->mtu;
    device->net_dev.info.MRU       = device->mtu;
    device->net_dev.info.mode      = net_dev_mode_sta;
    device->net_dev.info.intf_type = net_dev_intf_ether;
    device->net_dev.info.xfer_flag = OS_NET_XFER_TX_TASK;
    device->net_dev.ops            = &net_dev_ops;
    if (os_net_device_register(&device->net_dev, "enc28j60") != OS_EOK)
    {
        os_kprintf(DRV_EXT_TAG, "os_net_device_register failed");
    }

#if defined(ETH_DEBUG_ENABLE)
    os_loongson_eth_init(&device->parent.parent);
#endif

    return OS_EOK;
}

OS_DRIVER_INFO loongson_eth_driver = {
    .name  = "ETH_Type",
    .probe = loongson_eth_probe,
};

OS_DRIVER_DEFINE(loongson_eth_driver, DEVICE, OS_INIT_SUBLEVEL_LOW);

#if defined(ETH_DEBUG_ENABLE)
#include <shell.h>

static int dump_reg(int argc, char *argv[])
{
    stmmac_dump_mac_regs(device_debug, device_debug->hw, 0);
    stmmac_dump_dma_regs(device_debug, device_debug->plat_dat.addr, 0);

    return 0;
}

SH_CMD_EXPORT(dump_reg, dump_reg, "dump_reg");

unsigned char buf_test[] = {0x6C, 0x4B, 0x90, 0xB3, 0x33, 0x51, 0x64, 0x48, 0x48, 0x48, 0x48, 0x60, 0x08, 0x00,
                            0x45, 0x00, 0x00, 0x54, 0x14, 0x08, 0x40, 0x00, 0x40, 0x01, 0xA4, 0x2E, 0xC0, 0xA8,
                            0x00, 0xC8, 0xC0, 0xA8, 0x00, 0x5A, 0x08, 0x00, 0x99, 0xD7, 0x00, 0x02, 0x00, 0x15,
                            0x53, 0xF8, 0x9F, 0x60, 0x00, 0x00, 0x00, 0x00, 0xA1, 0xE5, 0x0A, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B,
                            0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
                            0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37};

static int send_pkt(int argc, char *argv[])
{
    unsigned char *buf = os_aligned_malloc(64, 1500);
    buf                = (unsigned char *)CACHED_TO_UNCACHED(buf);
    memcpy((void *)buf, (void *)buf_test, 98);
    stmmac_xmit(device_debug, buf, 98);
    return 0;
}

SH_CMD_EXPORT(send_pkt, send_pkt, "send_pkt");
#endif
