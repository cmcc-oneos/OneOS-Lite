/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        interrupt.h
 *
 * @brief       This file provides ostick function.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef INTERRUPT_H__
#define INTERRUPT_H__

#include <os_types.h>
#include <ls2k500_regs.h>

#define EXTINT_IRQ(x) (x)
#define LIOINT_IRQ(x) ((x) + 128)

#define GENERAL_CONFIGURE_REG0          (0x800000001fe10100)
#define GENERAL_CONFIGURE_EXTINT_ENABLE (27)

#define EXTINT_IEN0  (0x800000001fe11600)
#define EXTINT_IEN1  (0x800000001fe11608)
#define CORE_EXTISR0 (0x800000001fe11800)
#define CORE_EXTISR1 (0x800000001fe11808)
#define EXTINT_ICLR0 (0x800000001fe11700)
#define EXTINT_ICLR1 (0x800000001fe11708)

#define EXTINT_MAP0 (0x800000001fe114c0)
#define EXTINT_MAP1 (0x800000001fe114c1)
#define EXTINT_MAP2 (0x800000001fe114c2)
#define EXTINT_MAP3 (0x800000001fe114c3)

#define LIOINT_BASE      (0x800000001fe11400)
#define LIOINT_LOW_NUM   (32)
#define LIOINT_TOTAL_NUM (64)

#define EXTINT_LOW_NUM   (64)
#define EXTINT_TOTAL_NUM (128)

#define CORE_LIOISR0 (0x800000001fe11420)
#define CORE_LIOISR1 (0x800000001fe11460)
#define LIOINT_ICLR0 (0x800000001fe1142c)
#define LIOINT_ICLR1 (0x800000001fe1146c)
#define LIOINT_IEN0  (0x800000001fe11428)
#define LIOINT_IEN1  (0x800000001fe11468)

#define INTERRUPTS_MAX 192

/*
 * Interrupt handler definition
 */
typedef void (*os_isr_handler_t)(int vector, void *param);

struct os_irq_desc
{
    os_isr_handler_t handler;
    void            *param;
};

void             os_hw_exception_init(void);
void             os_hw_interrupt_init(void);
void             os_interrupt_dispatch(void);
void             os_hw_interrupt_umask(int vector);
void             os_hw_interrupt_mask(int vector);
os_isr_handler_t os_hw_interrupt_install(int vector, os_isr_handler_t handler, void *param, const char *name);

#endif
