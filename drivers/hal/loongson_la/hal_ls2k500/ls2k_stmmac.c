/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k_stmmac.c
 *
 * @brief       The driver file of eth driver for loongson
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <ls2k_stmmac.h>
#include <drv_eth.h>
#include <os_clock.h>

static void mdelay(unsigned int ms)
{
    os_tick_t tick_end, tick_now;

    tick_end = os_tick_get() + ((ms * OS_TICK_PER_SECOND) / MSEC_PER_SEC);

    while (1)
    {
        tick_now = os_tick_get();

        if (tick_now >= tick_end)
        {
            return;
        }
    }

    return;
}

static void dwmac1000_dma_axi(void *ioaddr)
{
    return;
}

static void dwmac1000_dma_init(void *ioaddr, struct stmmac_dma_cfg *dma_cfg, int atds)
{
    unsigned int value = readl(ioaddr + DMA_BUS_MODE);
    int          txpbl = dma_cfg->txpbl ?: dma_cfg->pbl;
    int          rxpbl = dma_cfg->rxpbl ?: dma_cfg->pbl;

    /*
     * Set the DMA PBL (Programmable Burst Length) mode.
     *
     * Note: before stmmac core 3.50 this mode bit was 4xPBL, and
     * post 3.5 mode bit acts as 8*PBL.
     */
    if (dma_cfg->pblx8)
        value |= DMA_BUS_MODE_MAXPBL;
    value |= DMA_BUS_MODE_USP;
    value &= ~(DMA_BUS_MODE_PBL_MASK | DMA_BUS_MODE_RPBL_MASK);
    value |= (txpbl << DMA_BUS_MODE_PBL_SHIFT);
    value |= (rxpbl << DMA_BUS_MODE_RPBL_SHIFT);

    /* Set the Fixed burst mode */
    if (dma_cfg->fixed_burst)
        value |= DMA_BUS_MODE_FB;

    /* Mixed Burst has no effect when fb is set */
    if (dma_cfg->mixed_burst)
        value |= DMA_BUS_MODE_MB;

    if (atds)
        value |= DMA_BUS_MODE_ATDS;

    if (dma_cfg->aal)
        value |= DMA_BUS_MODE_AAL;

    writel(value, ioaddr + DMA_BUS_MODE);
    /* Mask interrupts by writing to CSR7 */
    writel(DMA_INTR_DEFAULT_MASK, ioaddr + DMA_INTR_ENA);

    return;
}

/* clang-format off */
static void dwmac1000_dma_init_rx(void *ioaddr, struct stmmac_dma_cfg *dma_cfg, unsigned int dma_rx_phy, unsigned int chan)
{
    /* RX descriptor base address list must be written into DMA CSR3 */
    writel(dma_rx_phy, ioaddr + DMA_RCV_BASE_ADDR);
}

static void dwmac1000_dma_init_tx(void *ioaddr, struct stmmac_dma_cfg *dma_cfg, unsigned int dma_tx_phy, unsigned int chan)
{
    /* TX descriptor base address list must be written into DMA CSR4 */
    writel(dma_tx_phy, ioaddr + DMA_TX_BASE_ADDR);
}

static unsigned int dwmac1000_configure_fc(unsigned int csr6, int rxfifosz)
{
    csr6 &= ~DMA_CONTROL_RFA_MASK;
    csr6 &= ~DMA_CONTROL_RFD_MASK;

    /* Leave flow control disabled if receive fifo size is less than
     * 4K or 0. Otherwise, send XOFF when fifo is 1K less than full,
     * and send XON when 2K less than full.
     */
    if (rxfifosz < 4096)
    {
        csr6 &= ~DMA_CONTROL_EFC;
    }
    else
    {
        csr6 |= DMA_CONTROL_EFC;
        csr6 |= RFA_FULL_MINUS_1K;
        csr6 |= RFD_FULL_MINUS_2K;
    }
    return csr6;
}

static void dwmac1000_dma_operation_mode_rx(void *ioaddr, int mode, unsigned int channel, int fifosz, unsigned char qmode)
{
    unsigned int csr6 = readl(ioaddr + DMA_CONTROL);

    if (mode == SF_DMA_MODE)
    {
        csr6 |= DMA_CONTROL_RSF;
    }
    else
    {
        csr6 &= ~DMA_CONTROL_RSF;
        csr6 &= DMA_CONTROL_TC_RX_MASK;
        if (mode <= 32)
            csr6 |= DMA_CONTROL_RTC_32;
        else if (mode <= 64)
            csr6 |= DMA_CONTROL_RTC_64;
        else if (mode <= 96)
            csr6 |= DMA_CONTROL_RTC_96;
        else
            csr6 |= DMA_CONTROL_RTC_128;
    }

    /* Configure flow control based on rx fifo size */
    csr6 = dwmac1000_configure_fc(csr6, fifosz);
    writel(csr6, ioaddr + DMA_CONTROL);
}

static void dwmac1000_dma_operation_mode_tx(void *ioaddr, int mode, unsigned int channel, int fifosz, unsigned char qmode)
{
    unsigned int csr6 = readl(ioaddr + DMA_CONTROL);

    if (mode == SF_DMA_MODE)
    {
        /* Transmit COE type 2 cannot be done in cut-through mode. */
        csr6 |= DMA_CONTROL_TSF;
        /* Operating on second frame increase the performance
         * especially when transmit store-and-forward is used.
         */
        csr6 |= DMA_CONTROL_OSF;
    }
    else
    {
        csr6 &= ~DMA_CONTROL_TSF;
        csr6 &= DMA_CONTROL_TC_TX_MASK;
        /* Set the transmit threshold */
        if (mode <= 32)
            csr6 |= DMA_CONTROL_TTC_32;
        else if (mode <= 64)
            csr6 |= DMA_CONTROL_TTC_64;
        else if (mode <= 128)
            csr6 |= DMA_CONTROL_TTC_128;
        else if (mode <= 192)
            csr6 |= DMA_CONTROL_TTC_192;
        else
            csr6 |= DMA_CONTROL_TTC_256;
    }

    writel(csr6, ioaddr + DMA_CONTROL);
}
/* clang-format on */

static void dwmac1000_dump_dma_regs(void *ioaddr, unsigned int *reg_space)
{
    int i;

    for (i = 0; i < NUM_DWMAC1000_DMA_REGS; i++)
    {
        if ((i < 12) || (i > 17))
        {

            if (reg_space)
            {
                reg_space[DMA_BUS_MODE / 4 + i] = readl(ioaddr + DMA_BUS_MODE + i * 4);
            }
            else
            {
                os_kprintf("reg = %d,value = %08x\n\r", i, readl(ioaddr + DMA_BUS_MODE + i * 4));
            }
        }
    }
}

static void dwmac1000_get_hw_feature(void *ioaddr, struct dma_features *dma_cap)
{
    unsigned int hw_cap = readl(ioaddr + DMA_HW_FEATURE);

    dma_cap->mbps_10_100        = (hw_cap & DMA_HW_FEAT_MIISEL);
    dma_cap->mbps_1000          = (hw_cap & DMA_HW_FEAT_GMIISEL) >> 1;
    dma_cap->half_duplex        = (hw_cap & DMA_HW_FEAT_HDSEL) >> 2;
    dma_cap->hash_filter        = (hw_cap & DMA_HW_FEAT_HASHSEL) >> 4;
    dma_cap->multi_addr         = (hw_cap & DMA_HW_FEAT_ADDMAC) >> 5;
    dma_cap->pcs                = (hw_cap & DMA_HW_FEAT_PCSSEL) >> 6;
    dma_cap->sma_mdio           = (hw_cap & DMA_HW_FEAT_SMASEL) >> 8;
    dma_cap->pmt_remote_wake_up = (hw_cap & DMA_HW_FEAT_RWKSEL) >> 9;
    dma_cap->pmt_magic_frame    = (hw_cap & DMA_HW_FEAT_MGKSEL) >> 10;
    /* MMC */
    dma_cap->rmon = (hw_cap & DMA_HW_FEAT_MMCSEL) >> 11;
    /* IEEE 1588-2002 */
    dma_cap->time_stamp = (hw_cap & DMA_HW_FEAT_TSVER1SEL) >> 12;
    /* IEEE 1588-2008 */
    dma_cap->atime_stamp = (hw_cap & DMA_HW_FEAT_TSVER2SEL) >> 13;
    /* 802.3az - Energy-Efficient Ethernet (EEE) */
    dma_cap->eee = (hw_cap & DMA_HW_FEAT_EEESEL) >> 14;
    dma_cap->av  = (hw_cap & DMA_HW_FEAT_AVSEL) >> 15;
    /* TX and RX csum */
    dma_cap->tx_coe           = (hw_cap & DMA_HW_FEAT_TXCOESEL) >> 16;
    dma_cap->rx_coe_type1     = (hw_cap & DMA_HW_FEAT_RXTYP1COE) >> 17;
    dma_cap->rx_coe_type2     = (hw_cap & DMA_HW_FEAT_RXTYP2COE) >> 18;
    dma_cap->rxfifo_over_2048 = (hw_cap & DMA_HW_FEAT_RXFIFOSIZE) >> 19;
    /* TX and RX number of channels */
    dma_cap->number_rx_channel = (hw_cap & DMA_HW_FEAT_RXCHCNT) >> 20;
    dma_cap->number_tx_channel = (hw_cap & DMA_HW_FEAT_TXCHCNT) >> 22;
    /* Alternate (enhanced) DESC mode */
    dma_cap->enh_desc = (hw_cap & DMA_HW_FEAT_ENHDESSEL) >> 24;
}

static void dwmac1000_rx_watchdog(void *ioaddr, unsigned int riwt, unsigned int number_chan)
{
    writel(riwt, ioaddr + DMA_RX_WATCHDOG);
}

const struct stmmac_dma_ops dwmac1000_dma_ops = {
    .reset                   = dwmac_dma_reset,
    .init                    = dwmac1000_dma_init,
    .init_rx_chan            = dwmac1000_dma_init_rx,
    .init_tx_chan            = dwmac1000_dma_init_tx,
    .axi                     = dwmac1000_dma_axi,
    .dump_regs               = dwmac1000_dump_dma_regs,
    .dma_rx_mode             = dwmac1000_dma_operation_mode_rx,
    .dma_tx_mode             = dwmac1000_dma_operation_mode_tx,
    .enable_dma_transmission = dwmac_enable_dma_transmission,
    .enable_dma_irq          = dwmac_enable_dma_irq,
    .disable_dma_irq         = dwmac_disable_dma_irq,
    .start_tx                = dwmac_dma_start_tx,
    .stop_tx                 = dwmac_dma_stop_tx,
    .start_rx                = dwmac_dma_start_rx,
    .stop_rx                 = dwmac_dma_stop_rx,
    .dma_interrupt           = dwmac_dma_interrupt,
    .get_hw_feature          = dwmac1000_get_hw_feature,
    .rx_watchdog             = dwmac1000_rx_watchdog,
};

#define GMAC_HI_REG_AE 0x80000000

int dwmac_dma_reset(void *ioaddr)
{
    unsigned int value = readl(ioaddr + DMA_BUS_MODE);
    //	int err;
    unsigned int retry = 0;

    /* DMA SW reset */
    value |= DMA_BUS_MODE_SFT_RESET;
    writel(value, ioaddr + DMA_BUS_MODE);

    while (1)
    {
        value = readl(ioaddr + DMA_BUS_MODE);

        if (!(value & DMA_BUS_MODE_SFT_RESET))
        {
            break;
        }
        mdelay(10);
        retry++;
        if (retry >= 10)
        {
            return OS_EBUSY;
        }
    }
    return 0;
}

/* CSR1 enables the transmit DMA to check for new descriptor */
void dwmac_enable_dma_transmission(void *ioaddr)
{
    writel(1, ioaddr + DMA_XMT_POLL_DEMAND);
}

void dwmac_enable_dma_irq(void *ioaddr, unsigned int chan)
{
    writel(DMA_INTR_DEFAULT_MASK, ioaddr + DMA_INTR_ENA);
}

void dwmac_disable_dma_irq(void *ioaddr, unsigned int chan)
{
    writel(0, ioaddr + DMA_INTR_ENA);
}

void dwmac_dma_start_tx(void *ioaddr, unsigned int chan)
{
    unsigned int value = readl(ioaddr + DMA_CONTROL);
    value |= DMA_CONTROL_ST;
    writel(value, ioaddr + DMA_CONTROL);
}

void dwmac_dma_stop_tx(void *ioaddr, unsigned int chan)
{
    unsigned int value = readl(ioaddr + DMA_CONTROL);
    value &= ~DMA_CONTROL_ST;
    writel(value, ioaddr + DMA_CONTROL);
}

void dwmac_dma_start_rx(void *ioaddr, unsigned int chan)
{
    unsigned int value = readl(ioaddr + DMA_CONTROL);
    value |= DMA_CONTROL_SR;

    writel(value, ioaddr + DMA_CONTROL);
}

void dwmac_dma_stop_rx(void *ioaddr, unsigned int chan)
{
    unsigned int value = readl(ioaddr + DMA_CONTROL);
    value &= ~DMA_CONTROL_SR;
    writel(value, ioaddr + DMA_CONTROL);
}

int dwmac_dma_interrupt(void *ioaddr, unsigned int chan)
{
    int ret = 0;
    /* read the status register (CSR5) */
    unsigned int intr_status = readl(ioaddr + DMA_STATUS);
    /* TX/RX NORMAL interrupts */
    if (intr_status & DMA_STATUS_NIS)
    {

        if (intr_status & DMA_STATUS_RI)
        {
            unsigned int value = readl(ioaddr + DMA_INTR_ENA);
            /* to schedule NAPI on real RIE event. */
            if (value & DMA_INTR_ENA_RIE)
            {

                ret |= handle_rx;
            }
        }
        if (intr_status & DMA_STATUS_TI)
        {
            ret |= handle_tx;
        }
    }
    /* Clear the interrupt by writing a logic 1 to the CSR5[15-0] */
    writel((intr_status & 0x1ffff), ioaddr + DMA_STATUS);

    return ret;
}

void dwmac_dma_flush_tx_fifo(void *ioaddr)
{
    unsigned int csr6 = readl(ioaddr + DMA_CONTROL);
    writel((csr6 | DMA_CONTROL_FTF), ioaddr + DMA_CONTROL);

    do
    {
    } while ((readl(ioaddr + DMA_CONTROL) & DMA_CONTROL_FTF));
}

void stmmac_set_mac_addr(void *ioaddr, unsigned char addr[6], unsigned int high, unsigned int low)
{
    unsigned long data;

    data = (addr[5] << 8) | addr[4];
    /* For MAC Addr registers we have to set the Address Enable (AE)
     * bit that has no effect on the High Reg 0 where the bit 31 (MO)
     * is RO.
     */

    writel(data | GMAC_HI_REG_AE, ioaddr + high);
    data = (addr[3] << 24) | (addr[2] << 16) | (addr[1] << 8) | addr[0];
    writel(data, ioaddr + low);
}

/* Enable disable MAC RX/TX */
void stmmac_set_mac(void *ioaddr, unsigned int enable)
{
    unsigned int value = readl(ioaddr + MAC_CTRL_REG);

    if (enable)
        value |= MAC_ENABLE_RX | MAC_ENABLE_TX;
    else
        value &= ~(MAC_ENABLE_TX | MAC_ENABLE_RX);

    writel(value, ioaddr + MAC_CTRL_REG);
}

void stmmac_get_mac_addr(void *ioaddr, unsigned char *addr, unsigned int high, unsigned int low)
{
    unsigned int hi_addr, lo_addr;

    /* Read the MAC address from the hardware */
    hi_addr = readl(ioaddr + high);
    lo_addr = readl(ioaddr + low);

    /* Extract the MAC address from the high and low words */
    addr[0] = lo_addr & 0xff;
    addr[1] = (lo_addr >> 8) & 0xff;
    addr[2] = (lo_addr >> 16) & 0xff;
    addr[3] = (lo_addr >> 24) & 0xff;
    addr[4] = hi_addr & 0xff;
    addr[5] = (hi_addr >> 8) & 0xff;
}

static void dwmac1000_core_init(struct mac_device_info *hw, struct os_loongson_eth *device)
{
    // struct stmmac_priv *priv = netdev_priv(dev);
    void        *ioaddr = hw->pcsr;
    unsigned int value  = readl(ioaddr + GMAC_CONTROL);
    int          mtu    = device->plat_dat.maxmtu;

    /* Configure GMAC core */
    value |= GMAC_CORE_INIT;

    /* Clear ACS bit because Ethernet switch tagging formats such as
     * Broadcom tags can look like invalid LLC/SNAP packets and cause the
     * hardware to truncate packets on reception.
     */

    if (mtu > 1500)
        value |= GMAC_CONTROL_2K;
    if (mtu > 2000)
        value |= GMAC_CONTROL_JE;

    if (hw->ps)
    {
        value |= GMAC_CONTROL_TE;

        value &= ~hw->link.speed_mask;
        switch (hw->ps)
        {
        case SPEED_1000:
            value |= hw->link.speed1000;
            break;
        case SPEED_100:
            value |= hw->link.speed100;
            break;
        case SPEED_10:
            value |= hw->link.speed10;
            break;
        }
    }

    writel(value, ioaddr + GMAC_CONTROL);

    /* Mask GMAC interrupts */
    value = GMAC_INT_DEFAULT_MASK;

    if (hw->pcs)
        value &= ~GMAC_INT_DISABLE_PCS;

    writel(value, ioaddr + GMAC_INT_MASK);

    /* Tag detection without filtering */
    writel(0x0, ioaddr + GMAC_VLAN_TAG);
}

static int dwmac1000_rx_ipc_enable(struct mac_device_info *hw)
{
    void        *ioaddr = hw->pcsr;
    unsigned int value  = readl(ioaddr + GMAC_CONTROL);

    if (hw->rx_csum)
        value |= GMAC_CONTROL_IPC;
    else
        value &= ~GMAC_CONTROL_IPC;

    writel(value, ioaddr + GMAC_CONTROL);

    value = readl(ioaddr + GMAC_CONTROL);

    return !!(value & GMAC_CONTROL_IPC);
}

static void dwmac1000_dump_regs(struct mac_device_info *hw, unsigned int *reg_space)
{
    void *ioaddr = hw->pcsr;
    int   i;

    for (i = 0; i < 55; i++)
    {
        if (reg_space)
        {
            reg_space[i] = readl(ioaddr + i * 4);
        }
        else
        {
            os_kprintf("reg = %d,value = %08x\n\r", i, readl(ioaddr + i * 4));
        }
    }
}

static void dwmac1000_set_umac_addr(struct mac_device_info *hw, unsigned char *addr, unsigned int reg_n)
{
    void *ioaddr = hw->pcsr;
    stmmac_set_mac_addr(ioaddr, addr, GMAC_ADDR_HIGH(reg_n), GMAC_ADDR_LOW(reg_n));
}

static void dwmac1000_get_umac_addr(struct mac_device_info *hw, unsigned char *addr, unsigned int reg_n)
{
    void *ioaddr = hw->pcsr;
    stmmac_get_mac_addr(ioaddr, addr, GMAC_ADDR_HIGH(reg_n), GMAC_ADDR_LOW(reg_n));
}

static void dwmac1000_set_filter(struct mac_device_info *hw)
{
}

static void dwmac1000_flow_ctrl(struct mac_device_info *hw,
                                unsigned int            duplex,
                                unsigned int            fc,
                                unsigned int            pause_time,
                                unsigned int            tx_cnt)
{
    void *ioaddr = hw->pcsr;
    /* Set flow such that DZPQ in Mac Register 6 is 0,
     * and unicast pause detect is enabled.
     */
    unsigned int flow = GMAC_FLOW_CTRL_UP;

    if (fc & FLOW_RX)
    {
        flow |= GMAC_FLOW_CTRL_RFE;
    }
    if (fc & FLOW_TX)
    {
        flow |= GMAC_FLOW_CTRL_TFE;
    }

    if (duplex)
    {
        flow |= (pause_time << GMAC_FLOW_CTRL_PT_SHIFT);
    }

    writel(flow, ioaddr + GMAC_FLOW_CTRL);
}

static void dwmac1000_pmt(struct mac_device_info *hw, unsigned long mode)
{
}

static int dwmac1000_irq_status(struct mac_device_info *hw)
{
    return 0;
}

static void dwmac1000_set_eee_mode(struct mac_device_info *hw, unsigned int en_tx_lpi_clockgating)
{
    void        *ioaddr = hw->pcsr;
    unsigned int value;

    /*TODO - en_tx_lpi_clockgating treatment */

    /* Enable the link status receive on RGMII, SGMII ore SMII
     * receive path and instruct the transmit to enter in LPI
     * state.
     */
    value = readl(ioaddr + LPI_CTRL_STATUS);
    value |= LPI_CTRL_STATUS_LPIEN | LPI_CTRL_STATUS_LPITXA;
    writel(value, ioaddr + LPI_CTRL_STATUS);
}

static void dwmac1000_reset_eee_mode(struct mac_device_info *hw)
{
    void        *ioaddr = hw->pcsr;
    unsigned int value;

    value = readl(ioaddr + LPI_CTRL_STATUS);
    value &= ~(LPI_CTRL_STATUS_LPIEN | LPI_CTRL_STATUS_LPITXA);
    writel(value, ioaddr + LPI_CTRL_STATUS);
}

static void dwmac1000_set_eee_pls(struct mac_device_info *hw, int link)
{
    void        *ioaddr = hw->pcsr;
    unsigned int value;

    value = readl(ioaddr + LPI_CTRL_STATUS);

    if (link)
        value |= LPI_CTRL_STATUS_PLS;
    else
        value &= ~LPI_CTRL_STATUS_PLS;

    writel(value, ioaddr + LPI_CTRL_STATUS);
}

static void dwmac1000_set_eee_timer(struct mac_device_info *hw, int ls, int tw)
{
    void *ioaddr = hw->pcsr;
    int   value  = ((tw & 0xffff)) | ((ls & 0x7ff) << 16);

    /* Program the timers in the LPI timer control register:
     * LS: minimum time (ms) for which the link
     *  status from PHY should be ok before transmitting
     *  the LPI pattern.
     * TW: minimum time (us) for which the core waits
     *  after it has stopped transmitting the LPI pattern.
     */
    writel(value, ioaddr + LPI_TIMER_CTRL);
}

/* clang-format off */
static inline void dwmac_ctrl_ane(void *ioaddr, unsigned int reg, unsigned int ane, unsigned int srgmi_ral, unsigned int loopback)
{
    unsigned int value = readl(ioaddr + GMAC_AN_CTRL(reg));

    /* Enable and restart the Auto-Negotiation */
    if (ane)
        value |= GMAC_AN_CTRL_ANE | GMAC_AN_CTRL_RAN;

    /* In case of MAC-2-MAC connection, block is configured to operate
     * according to MAC conf register.
     */
    if (srgmi_ral)
        value |= GMAC_AN_CTRL_SGMRAL;

    if (loopback)
        value |= GMAC_AN_CTRL_ELE;

    writel(value, ioaddr + GMAC_AN_CTRL(reg));
}

static inline void dwmac_rane(void *ioaddr, unsigned int reg, unsigned int restart)
{
    unsigned int value = readl(ioaddr + GMAC_AN_CTRL(reg));

    if (restart)
        value |= GMAC_AN_CTRL_RAN;

    writel(value, ioaddr + GMAC_AN_CTRL(reg));
}
/* clang-format on */

static inline void dwmac_get_adv_lp(void *ioaddr, unsigned int reg, struct rgmii_adv *adv_lp)
{
    unsigned int value = readl(ioaddr + GMAC_ANE_ADV(reg));

    if (value & GMAC_ANE_FD)
        adv_lp->duplex = DUPLEX_FULL;
    if (value & GMAC_ANE_HD)
        adv_lp->duplex |= DUPLEX_HALF;

    adv_lp->pause = (value & GMAC_ANE_PSE) >> GMAC_ANE_PSE_SHIFT;

    value = readl(ioaddr + GMAC_ANE_LPA(reg));

    if (value & GMAC_ANE_FD)
        adv_lp->lp_duplex = DUPLEX_FULL;
    if (value & GMAC_ANE_HD)
        adv_lp->lp_duplex = DUPLEX_HALF;

    adv_lp->lp_pause = (value & GMAC_ANE_PSE) >> GMAC_ANE_PSE_SHIFT;
}

static void dwmac1000_ctrl_ane(void *ioaddr, unsigned int ane, unsigned int srgmi_ral, unsigned int loopback)
{
    dwmac_ctrl_ane(ioaddr, GMAC_PCS_BASE, ane, srgmi_ral, loopback);
}

static void dwmac1000_rane(void *ioaddr, unsigned int restart)
{
    dwmac_rane(ioaddr, GMAC_PCS_BASE, restart);
}

static void dwmac1000_get_adv_lp(void *ioaddr, struct rgmii_adv *adv)
{
    dwmac_get_adv_lp(ioaddr, GMAC_PCS_BASE, adv);
}

static void dwmac1000_debug(void *ioaddr, unsigned int rx_queues, unsigned int tx_queues)
{
}

const struct stmmac_ops dwmac1000_ops = {
    .core_init       = dwmac1000_core_init,
    .set_mac         = stmmac_set_mac,
    .rx_ipc          = dwmac1000_rx_ipc_enable,
    .dump_regs       = dwmac1000_dump_regs,
    .host_irq_status = dwmac1000_irq_status,
    .set_filter      = dwmac1000_set_filter,
    .flow_ctrl       = dwmac1000_flow_ctrl,
    .pmt             = dwmac1000_pmt,
    .set_umac_addr   = dwmac1000_set_umac_addr,
    .get_umac_addr   = dwmac1000_get_umac_addr,
    .set_eee_mode    = dwmac1000_set_eee_mode,
    .reset_eee_mode  = dwmac1000_reset_eee_mode,
    .set_eee_timer   = dwmac1000_set_eee_timer,
    .set_eee_pls     = dwmac1000_set_eee_pls,
    .debug           = dwmac1000_debug,
    .pcs_ctrl_ane    = dwmac1000_ctrl_ane,
    .pcs_rane        = dwmac1000_rane,
    .pcs_get_adv_lp  = dwmac1000_get_adv_lp,
};

int dwmac1000_setup(struct os_loongson_eth *device)
{
    struct mac_device_info *mac = device->hw;

    device->plat_dat.multicast_filter_bins  = 64;
    device->plat_dat.unicast_filter_entries = 1;

    mac->pcsr                   = device->plat_dat.addr;
    mac->multicast_filter_bins  = device->plat_dat.multicast_filter_bins;
    mac->unicast_filter_entries = device->plat_dat.unicast_filter_entries;
    mac->mcast_bits_log2        = 0;

    if (mac->multicast_filter_bins)
        mac->mcast_bits_log2 = 6;    // ilog2(mac->multicast_filter_bins);

    mac->link.duplex       = GMAC_CONTROL_DM;
    mac->link.speed10      = GMAC_CONTROL_PS;
    mac->link.speed100     = GMAC_CONTROL_PS | GMAC_CONTROL_FES;
    mac->link.speed1000    = 0;
    mac->link.speed_mask   = GMAC_CONTROL_PS | GMAC_CONTROL_FES;
    mac->mii.addr          = GMAC_MII_ADDR;
    mac->mii.data          = GMAC_MII_DATA;
    mac->mii.addr_shift    = 11;
    mac->mii.addr_mask     = 0x0000F800;
    mac->mii.reg_shift     = 6;
    mac->mii.reg_mask      = 0x000007C0;
    mac->mii.clk_csr_shift = 2;
    mac->mii.clk_csr_mask  = GENMASK(5, 2);

    return 0;
}

static void config_hw_tstamping(void *ioaddr, unsigned int data)
{
    writel(data, ioaddr + PTP_TCR);
}

static void config_sub_second_increment(void *ioaddr, unsigned int ptp_clock, int gmac4, unsigned int *ssinc)
{
    unsigned int  value = readl(ioaddr + PTP_TCR);
    unsigned long data;
    unsigned int  reg_value;

    /* For GMAC3.x, 4.x versions, in "fine adjustement mode" set sub-second
     * increment to twice the number of nanoseconds of a clock cycle.
     * The calculation of the default_addend value by the caller will set it
     * to mid-range = 2^31 when the remainder of this division is zero,
     * which will make the accumulator overflow once every 2 ptp_clock
     * cycles, adding twice the number of nanoseconds of a clock cycle :
     * 2000000000ULL / ptp_clock.
     */
    if (value & PTP_TCR_TSCFUPDT)
        data = (2000000000ULL / ptp_clock);
    else
        data = (1000000000ULL / ptp_clock);

    /* 0.465ns accuracy */
    if (!(value & PTP_TCR_TSCTRLSSR))
        data = (data * 1000) / 465;

    data &= PTP_SSIR_SSINC_MASK;

    reg_value = data;
    if (gmac4)
        reg_value <<= GMAC4_PTP_SSIR_SSINC_SHIFT;

    writel(reg_value, ioaddr + PTP_SSIR);

    if (ssinc)
        *ssinc = data;
}

static int init_systime(void *ioaddr, unsigned int sec, unsigned int nsec)
{
    int          limit;
    unsigned int value;

    writel(sec, ioaddr + PTP_STSUR);
    writel(nsec, ioaddr + PTP_STNSUR);
    /* issue command to initialize the system time value */
    value = readl(ioaddr + PTP_TCR);
    value |= PTP_TCR_TSINIT;
    writel(value, ioaddr + PTP_TCR);

    /* wait for present system time initialize to complete */
    limit = 10;
    while (limit--)
    {
        if (!(readl(ioaddr + PTP_TCR) & PTP_TCR_TSINIT))
            break;
        mdelay(10);
    }
    if (limit < 0)
        return OS_EBUSY;

    return 0;
}

static int config_addend(void *ioaddr, unsigned int addend)
{
    unsigned int value;
    int          limit;

    writel(addend, ioaddr + PTP_TAR);
    /* issue command to update the addend value */
    value = readl(ioaddr + PTP_TCR);
    value |= PTP_TCR_TSADDREG;
    writel(value, ioaddr + PTP_TCR);

    /* wait for present addend update to complete */
    limit = 10;
    while (limit--)
    {
        if (!(readl(ioaddr + PTP_TCR) & PTP_TCR_TSADDREG))
            break;
        mdelay(10);
    }
    if (limit < 0)
        return OS_EBUSY;

    return 0;
}

static int adjust_systime(void *ioaddr, unsigned int sec, unsigned int nsec, int add_sub, int gmac4)
{
    unsigned int value;
    int          limit;

    if (add_sub)
    {
        /* If the new sec value needs to be subtracted with
         * the system time, then MAC_STSUR reg should be
         * programmed with (2^32 – <new_sec_value>)
         */
        if (gmac4)
            sec = -sec;

        value = readl(ioaddr + PTP_TCR);
        if (value & PTP_TCR_TSCTRLSSR)
            nsec = (PTP_DIGITAL_ROLLOVER_MODE - nsec);
        else
            nsec = (PTP_BINARY_ROLLOVER_MODE - nsec);
    }

    writel(sec, ioaddr + PTP_STSUR);
    value = (add_sub << PTP_STNSUR_ADDSUB_SHIFT) | nsec;
    writel(value, ioaddr + PTP_STNSUR);

    /* issue command to initialize the system time value */
    value = readl(ioaddr + PTP_TCR);
    value |= PTP_TCR_TSUPDT;
    writel(value, ioaddr + PTP_TCR);

    /* wait for present system time adjust/update to complete */
    limit = 10;
    while (limit--)
    {
        if (!(readl(ioaddr + PTP_TCR) & PTP_TCR_TSUPDT))
            break;
        mdelay(10);
    }
    if (limit < 0)
        return OS_EBUSY;

    return 0;
}

static void get_systime(void *ioaddr, unsigned long *systime)
{
    unsigned long ns;

    /* Get the TSSS value */
    ns = readl(ioaddr + PTP_STNSR);
    /* Get the TSS and convert sec time value to nanosecond */
    ns += readl(ioaddr + PTP_STSR) * 1000000000ULL;

    if (systime)
        *systime = ns;
}

const struct stmmac_hwtimestamp stmmac_ptp = {
    .config_hw_tstamping         = config_hw_tstamping,
    .init_systime                = init_systime,
    .config_sub_second_increment = config_sub_second_increment,
    .config_addend               = config_addend,
    .adjust_systime              = adjust_systime,
    .get_systime                 = get_systime,
};

static unsigned int is_jumbo_frm(int len, int enh_desc)
{
    unsigned int ret = 0;

    if (len >= BUF_SIZE_4KiB)
        ret = 1;

    return ret;
}

static void refill_desc3(void *priv_ptr, struct dma_desc *p)
{
}

/* In ring mode we need to fill the desc3 because it is used as buffer */
static void init_desc3(struct dma_desc *p)
{
    p->des3 = cpu_to_le32(le32_to_cpu(p->des2) + BUF_SIZE_8KiB);
}

static void clean_desc3(void *priv_ptr, struct dma_desc *p)
{
}

static int set_16kib_bfsize(int mtu)
{
    int ret = 0;
    if (mtu > BUF_SIZE_8KiB)
        ret = BUF_SIZE_16KiB;
    return ret;
}

const struct stmmac_mode_ops ring_mode_ops = {
    .is_jumbo_frm     = is_jumbo_frm,
    .refill_desc3     = refill_desc3,
    .init_desc3       = init_desc3,
    .clean_desc3      = clean_desc3,
    .set_16kib_bfsize = set_16kib_bfsize,
};

static inline void ehn_desc_rx_set_on_ring(struct dma_desc *p, int end, int bfsize)
{
    if (bfsize == BUF_SIZE_16KiB)
        p->des1 |= cpu_to_le32((BUF_SIZE_8KiB << ERDES1_BUFFER2_SIZE_SHIFT) & ERDES1_BUFFER2_SIZE_MASK);

    if (end)
        p->des1 |= cpu_to_le32(ERDES1_END_RING);
}

static inline void enh_desc_end_tx_desc_on_ring(struct dma_desc *p, int end)
{
    if (end)
        p->des0 |= cpu_to_le32(ETDES0_END_RING);
    else
        p->des0 &= cpu_to_le32(~ETDES0_END_RING);
}

static inline void enh_set_tx_desc_len_on_ring(struct dma_desc *p, int len)
{
    if (len > BUF_SIZE_4KiB)
    {
        p->des1 |= cpu_to_le32((((len - BUF_SIZE_4KiB) << ETDES1_BUFFER2_SIZE_SHIFT) & ETDES1_BUFFER2_SIZE_MASK) |
                               (BUF_SIZE_4KiB & ETDES1_BUFFER1_SIZE_MASK));
    }
    else
        p->des1 |= cpu_to_le32((len & ETDES1_BUFFER1_SIZE_MASK));
}

/* Normal descriptors */
static inline void ndesc_rx_set_on_ring(struct dma_desc *p, int end, int bfsize)
{
    if (bfsize >= BUF_SIZE_2KiB)
    {
        int bfsize2;

        bfsize2 = min(bfsize - BUF_SIZE_2KiB + 1, BUF_SIZE_2KiB - 1);
        p->des1 |= cpu_to_le32((bfsize2 << RDES1_BUFFER2_SIZE_SHIFT) & RDES1_BUFFER2_SIZE_MASK);
    }

    if (end)
        p->des1 |= cpu_to_le32(RDES1_END_RING);
}

static inline void ndesc_end_tx_desc_on_ring(struct dma_desc *p, int end)
{
    if (end)
        p->des1 |= cpu_to_le32(TDES1_END_RING);
    else
        p->des1 &= cpu_to_le32(~TDES1_END_RING);
}

static inline void norm_set_tx_desc_len_on_ring(struct dma_desc *p, int len)
{
    if (len > BUF_SIZE_2KiB)
    {
        unsigned int buffer1 = (BUF_SIZE_2KiB - 1) & TDES1_BUFFER1_SIZE_MASK;
        p->des1 |= cpu_to_le32((((len - buffer1) << TDES1_BUFFER2_SIZE_SHIFT) & TDES1_BUFFER2_SIZE_MASK) | buffer1);
    }
    else
        p->des1 |= cpu_to_le32((len & TDES1_BUFFER1_SIZE_MASK));
}

/* Specific functions used for Chain mode */

/* Enhanced descriptors */
static inline void ehn_desc_rx_set_on_chain(struct dma_desc *p)
{
    p->des1 |= cpu_to_le32(ERDES1_SECOND_ADDRESS_CHAINED);
}

static inline void enh_desc_end_tx_desc_on_chain(struct dma_desc *p)
{
    p->des0 |= cpu_to_le32(ETDES0_SECOND_ADDRESS_CHAINED);
}

static inline void enh_set_tx_desc_len_on_chain(struct dma_desc *p, int len)
{
    p->des1 |= cpu_to_le32(len & ETDES1_BUFFER1_SIZE_MASK);
}

/* Normal descriptors */
static inline void ndesc_rx_set_on_chain(struct dma_desc *p, int end)
{
    p->des1 |= cpu_to_le32(RDES1_SECOND_ADDRESS_CHAINED);
}

static inline void ndesc_tx_set_on_chain(struct dma_desc *p)
{
    p->des1 |= cpu_to_le32(TDES1_SECOND_ADDRESS_CHAINED);
}

static inline void norm_set_tx_desc_len_on_chain(struct dma_desc *p, int len)
{
    p->des1 |= cpu_to_le32(len & TDES1_BUFFER1_SIZE_MASK);
}

//---------------------------------------------------------
static int enh_desc_get_tx_status(struct dma_desc *p, void *ioaddr)
{
    unsigned int tdes0 = le32_to_cpu(p->des0);
    int          ret   = tx_done;

    /* Get tx owner first */
    if (tdes0 & ETDES0_OWN)
        return tx_dma_own;

    /* Verify tx error by looking at the last segment. */
    if (!(tdes0 & ETDES0_LAST_SEGMENT))
        return tx_not_ls;

    if (tdes0 & ETDES0_ERROR_SUMMARY)
    {

        ret = tx_err;
    }

    return ret;
}

static int enh_desc_get_tx_len(struct dma_desc *p)
{
    return (le32_to_cpu(p->des1) & ETDES1_BUFFER1_SIZE_MASK);
}

static int enh_desc_coe_rdes0(int ipc_err, int type, int payload_err)
{
    int          ret    = good_frame;
    unsigned int status = (type << 2 | ipc_err << 1 | payload_err) & 0x7;

    /* bits 5 7 0 | Frame status
     * ----------------------------------------------------------
     *      0 0 0 | IEEE 802.3 Type frame (length < 1536 octects)
     *      1 0 0 | IPv4/6 No CSUM errorS.
     *      1 0 1 | IPv4/6 CSUM PAYLOAD error
     *      1 1 0 | IPv4/6 CSUM IP HR error
     *      1 1 1 | IPv4/6 IP PAYLOAD AND HEADER errorS
     *      0 0 1 | IPv4/6 unsupported IP PAYLOAD
     *      0 1 1 | COE bypassed.. no IPv4/6 frame
     *      0 1 0 | Reserved.
     */
    if (status == 0x0)
        ret = llc_snap;
    else if (status == 0x4)
        ret = good_frame;
    else if (status == 0x5)
        ret = csum_none;
    else if (status == 0x6)
        ret = csum_none;
    else if (status == 0x7)
        ret = csum_none;
    else if (status == 0x1)
        ret = discard_frame;
    else if (status == 0x3)
        ret = discard_frame;
    return ret;
}

static void enh_desc_get_ext_status(void *data, struct dma_extended_desc *p)
{
}

static int enh_desc_get_rx_status(struct dma_desc *p)
{
    unsigned int rdes0 = le32_to_cpu(p->des0);
    int          ret   = good_frame;

    if (rdes0 & RDES0_OWN)
        return dma_own;

    if (!(rdes0 & RDES0_LAST_DESCRIPTOR))
    {
        return discard_frame;
    }

    if (rdes0 & RDES0_ERROR_SUMMARY)
    {
        ret = discard_frame;
    }

    /* After a payload csum error, the ES bit is set.
     * It doesn't match with the information reported into the databook.
     * At any rate, we need to understand if the CSUM hw computation is ok
     * and report this info to the upper layers. */
    if (ret == good_frame)
        ret = enh_desc_coe_rdes0(!!(rdes0 & RDES0_IPC_CSUM_ERROR),
                                 !!(rdes0 & RDES0_FRAME_TYPE),
                                 !!(rdes0 & ERDES0_RX_MAC_ADDR));

    if (rdes0 & RDES0_SA_FILTER_FAIL)
    {
        ret = discard_frame;
    }
    if (rdes0 & RDES0_DA_FILTER_FAIL)
    {
        ret = discard_frame;
    }
    if (rdes0 & RDES0_LENGTH_ERROR)
    {
        ret = discard_frame;
    }

    return ret;
}

static void enh_desc_init_rx_desc(struct dma_desc *p, int disable_rx_ic, int mode, int end, int bfsize)
{
    int bfsize1;

    p->des0 |= cpu_to_le32(RDES0_OWN);

    bfsize1 = min(bfsize, BUF_SIZE_8KiB);
    p->des1 |= cpu_to_le32(bfsize1 & ERDES1_BUFFER1_SIZE_MASK);

    if (mode == STMMAC_CHAIN_MODE)
    {
    }
    else
    {
        ehn_desc_rx_set_on_ring(p, end, bfsize);
    }
    if (disable_rx_ic)
        p->des1 |= cpu_to_le32(ERDES1_DISABLE_IC);
}

static void enh_desc_init_tx_desc(struct dma_desc *p, int mode, int end)
{
    p->des0 &= cpu_to_le32(~ETDES0_OWN);
    if (mode == STMMAC_CHAIN_MODE)
    {
    }
    else
    {
        enh_desc_end_tx_desc_on_ring(p, end);
    }
}

static int enh_desc_get_tx_owner(struct dma_desc *p)
{
    return (le32_to_cpu(p->des0) & ETDES0_OWN) >> 31;
}

static void enh_desc_set_tx_owner(struct dma_desc *p)
{
    p->des0 |= cpu_to_le32(ETDES0_OWN);
}

static void enh_desc_set_rx_owner(struct dma_desc *p, int disable_rx_ic)
{
    p->des0 |= cpu_to_le32(RDES0_OWN);
}

static int enh_desc_get_tx_ls(struct dma_desc *p)
{
    return (le32_to_cpu(p->des0) & ETDES0_LAST_SEGMENT) >> 29;
}

static void enh_desc_release_tx_desc(struct dma_desc *p, int mode)
{
    int ter = (le32_to_cpu(p->des0) & ETDES0_END_RING) >> 21;

    memset(p, 0, offsetof(struct dma_desc, des3));
    if (mode == STMMAC_CHAIN_MODE)
    {
    }
    else
    {
        enh_desc_end_tx_desc_on_ring(p, ter);
    }
}

static void enh_desc_prepare_tx_desc(struct dma_desc *p,
                                     int              is_fs,
                                     int              len,
                                     unsigned int     csum_flag,
                                     int              mode,
                                     unsigned int     tx_own,
                                     unsigned int     ls,
                                     unsigned int     tot_pkt_len)
{
    unsigned int tdes0 = le32_to_cpu(p->des0);

    if (mode == STMMAC_CHAIN_MODE)
    {
    }
    else
    {
        enh_set_tx_desc_len_on_ring(p, len);
    }
    if (is_fs)
        tdes0 |= ETDES0_FIRST_SEGMENT;
    else
        tdes0 &= ~ETDES0_FIRST_SEGMENT;

    if (csum_flag)
        tdes0 |= (TX_CIC_FULL << ETDES0_CHECKSUM_INSERTION_SHIFT);
    else
        tdes0 &= ~(TX_CIC_FULL << ETDES0_CHECKSUM_INSERTION_SHIFT);

    if (ls)
        tdes0 |= ETDES0_LAST_SEGMENT;

    /* Finally set the OWN bit. Later the DMA will start! */
    if (tx_own)
        tdes0 |= ETDES0_OWN;

    p->des0 = cpu_to_le32(tdes0);
}

static void enh_desc_set_tx_ic(struct dma_desc *p)
{
    p->des0 |= cpu_to_le32(ETDES0_INTERRUPT);
}

static int enh_desc_get_rx_frame_len(struct dma_desc *p, int rx_coe_type)
{
    unsigned int csum = 0;
    /* The type-1 checksum offload engines append the checksum at
     * the end of frame and the two bytes of checksum are added in
     * the length.
     * Adjust for that in the framelen for type-1 checksum offload
     * engines.
     */
    if (rx_coe_type == STMMAC_RX_COE_TYPE1)
        csum = 2;

    return (((le32_to_cpu(p->des0) & RDES0_FRAME_LEN_MASK) >> RDES0_FRAME_LEN_SHIFT) - csum);
}

static void enh_desc_enable_tx_timestamp(struct dma_desc *p)
{
    p->des0 |= cpu_to_le32(ETDES0_TIME_STAMP_ENABLE);
}

static int enh_desc_get_tx_timestamp_status(struct dma_desc *p)
{
    return (le32_to_cpu(p->des0) & ETDES0_TIME_STAMP_STATUS) >> 17;
}

static void enh_desc_get_timestamp(void *desc, unsigned int ats, unsigned long *ts)
{
    unsigned long ns;

    if (ats)
    {
        struct dma_extended_desc *p = (struct dma_extended_desc *)desc;
        ns                          = le32_to_cpu(p->des6);
        /* convert high/sec time stamp value to nanosecond */
        ns += le32_to_cpu(p->des7) * 1000000000ULL;
    }
    else
    {
        struct dma_desc *p = (struct dma_desc *)desc;
        ns                 = le32_to_cpu(p->des2);
        ns += le32_to_cpu(p->des3) * 1000000000ULL;
    }

    *ts = ns;
}

static int enh_desc_get_rx_timestamp_status(void *desc, void *next_desc, unsigned int ats)
{
    if (ats)
    {
        struct dma_extended_desc *p = (struct dma_extended_desc *)desc;
        return (le32_to_cpu(p->basic.des0) & RDES0_IPC_CSUM_ERROR) >> 7;
    }
    else
    {
        struct dma_desc *p = (struct dma_desc *)desc;
        if ((le32_to_cpu(p->des2) == 0xffffffff) && (le32_to_cpu(p->des3) == 0xffffffff))
            /* timestamp is corrupted, hence don't store it */
            return 0;
        else
            return 1;
    }
}

static void enh_desc_display_ring(void *head, unsigned int size, unsigned int rx)
{
}

static void enh_desc_get_addr(struct dma_desc *p, unsigned int *addr)
{
    *addr = le32_to_cpu(p->des2);
}

static void enh_desc_set_addr(struct dma_desc *p, dma_addr_t addr)
{
    p->des2 = cpu_to_le32(addr);
}

static void enh_desc_clear(struct dma_desc *p)
{
    p->des2 = 0;
}

const struct stmmac_desc_ops enh_desc_ops = {
    .tx_status               = enh_desc_get_tx_status,
    .rx_status               = enh_desc_get_rx_status,
    .get_tx_len              = enh_desc_get_tx_len,
    .init_rx_desc            = enh_desc_init_rx_desc,
    .init_tx_desc            = enh_desc_init_tx_desc,
    .get_tx_owner            = enh_desc_get_tx_owner,
    .release_tx_desc         = enh_desc_release_tx_desc,
    .prepare_tx_desc         = enh_desc_prepare_tx_desc,
    .set_tx_ic               = enh_desc_set_tx_ic,
    .get_tx_ls               = enh_desc_get_tx_ls,
    .set_tx_owner            = enh_desc_set_tx_owner,
    .set_rx_owner            = enh_desc_set_rx_owner,
    .get_rx_frame_len        = enh_desc_get_rx_frame_len,
    .rx_extended_status      = enh_desc_get_ext_status,
    .enable_tx_timestamp     = enh_desc_enable_tx_timestamp,
    .get_tx_timestamp_status = enh_desc_get_tx_timestamp_status,
    .get_timestamp           = enh_desc_get_timestamp,
    .get_rx_timestamp_status = enh_desc_get_rx_timestamp_status,
    .display_ring            = enh_desc_display_ring,
    .get_addr                = enh_desc_get_addr,
    .set_addr                = enh_desc_set_addr,
    .clear                   = enh_desc_clear,
};

static void stmmac_dwmac_mode_quirk(struct os_loongson_eth *device)
{
    struct mac_device_info *mac = device->hw;

    if (device->plat_dat.chain_mode)
    {
    }
    else
    {
        device->plat_dat.mode = STMMAC_RING_MODE;
        mac->mode             = &ring_mode_ops;
    }
}

static int stmmac_dwmac1_quirks(struct os_loongson_eth *device)
{
    struct mac_device_info *mac = device->hw;

    if (device->plat_dat.enh_desc)
    {

        /* GMAC older than 3.50 has no extended descriptors */
        if (device->plat_dat.synopsys_id >= DWMAC_CORE_3_50)
        {
            device->plat_dat.extend_desc = 1;
        }
        else
        {
        }

        mac->desc = &enh_desc_ops;
    }
    else
    {
    }

    stmmac_dwmac_mode_quirk(device);
    return 0;
}

struct stmmac_hwif_entry stmmac_hw = {
    .gmac   = 1,
    .gmac4  = 0,
    .xgmac  = 0,
    .min_id = 0,
    .regs =
        {
            .ptp_off = PTP_GMAC3_X_OFFSET,
            .mmc_off = MMC_GMAC3_X_OFFSET,
        },
    .desc        = OS_NULL,
    .dma         = &dwmac1000_dma_ops,
    .mac         = &dwmac1000_ops,
    .hwtimestamp = &stmmac_ptp,
    .mode        = OS_NULL,
    .tc          = OS_NULL,
    .setup       = dwmac1000_setup,
    .quirks      = stmmac_dwmac1_quirks,
};

void stmmac_clk_csr_set(int *clk_csr)
{
    unsigned int clk_rate = 0;

    /* Platform provided default clk_csr would be assumed valid
     * for all other cases except for the below mentioned ones.
     * For values higher than the IEEE 802.3 specified frequency
     * we can not estimate the proper divider as it is not known
     * the frequency of clk_csr_i. So we do not change the default
     * divider.
     */
    if (!(*clk_csr & MAC_CSR_H_FRQ_MASK))
    {
        if (clk_rate < CSR_F_35M)
            *clk_csr = STMMAC_CSR_20_35M;
        else if ((clk_rate >= CSR_F_35M) && (clk_rate < CSR_F_60M))
            *clk_csr = STMMAC_CSR_35_60M;
        else if ((clk_rate >= CSR_F_60M) && (clk_rate < CSR_F_100M))
            *clk_csr = STMMAC_CSR_60_100M;
        else if ((clk_rate >= CSR_F_100M) && (clk_rate < CSR_F_150M))
            *clk_csr = STMMAC_CSR_100_150M;
        else if ((clk_rate >= CSR_F_150M) && (clk_rate < CSR_F_250M))
            *clk_csr = STMMAC_CSR_150_250M;
        else if ((clk_rate >= CSR_F_250M) && (clk_rate < CSR_F_300M))
            *clk_csr = STMMAC_CSR_250_300M;
    }
}

unsigned int stmmac_get_id(void *ioaddr, unsigned int id_reg)
{
    unsigned int reg = readl(ioaddr + id_reg);

    if (!reg)
    {
        return 0x0;
    }

    return reg & 0xFF;
}

int stmmac_mdio_read(struct mii_bus *bus, int phyaddr, int phyreg)
{
    struct os_loongson_eth *device      = bus->device;
    unsigned int            mii_address = device->hw->mii.addr;
    unsigned int            mii_data    = device->hw->mii.data;
    unsigned int            v;
    int                     data;
    unsigned int            value = MII_BUSY;
    unsigned int            retry = 0;

    value |= (phyaddr << device->hw->mii.addr_shift) & device->hw->mii.addr_mask;
    value |= (phyreg << device->hw->mii.reg_shift) & device->hw->mii.reg_mask;
    value |= (device->plat_dat.clk_csr << device->hw->mii.clk_csr_shift) & device->hw->mii.clk_csr_mask;

    while (1)
    {
        v = readl(device->plat_dat.addr + mii_address);

        if (!(v & MII_BUSY))
        {
            break;
        }
        mdelay(10);
        retry++;
        if (retry >= 10)
        {
            return OS_EBUSY;
        }
    }

    writel(value, device->plat_dat.addr + mii_address);

    retry = 0;

    while (1)
    {
        v = readl(device->plat_dat.addr + mii_address);

        if (!(v & MII_BUSY))
        {
            break;
        }
        mdelay(10);
        retry++;
        if (retry >= 10)
        {
            return OS_EBUSY;
        }
    }
    /* Read the data from the MII data register */
    data = (int)readl(device->plat_dat.addr + mii_data);

    return data;
}

int stmmac_mdio_write(struct mii_bus *bus, int phyaddr, int phyreg, unsigned short phydata)
{
    struct os_loongson_eth *device      = bus->device;
    unsigned int            mii_address = device->hw->mii.addr;
    unsigned int            mii_data    = device->hw->mii.data;
    unsigned int            v;
    unsigned int            value = MII_BUSY;
    unsigned int            retry = 0;

    value |= (phyaddr << device->hw->mii.addr_shift) & device->hw->mii.addr_mask;
    value |= (phyreg << device->hw->mii.reg_shift) & device->hw->mii.reg_mask;

    value |= (device->plat_dat.clk_csr << device->hw->mii.clk_csr_shift) & device->hw->mii.clk_csr_mask;
    value |= MII_WRITE;

    /* Wait until any existing MII operation is complete */

    retry = 0;

    while (1)
    {
        v = readl(device->plat_dat.addr + mii_address);

        if (!(v & MII_BUSY))
        {
            break;
        }
        mdelay(10);
        retry++;
        if (retry >= 10)
        {
            return OS_EBUSY;
        }
    }
    /* Set the MII address register to write */
    writel(phydata, device->plat_dat.addr + mii_data);
    writel(value, device->plat_dat.addr + mii_address);

    /* Wait until any existing MII operation is complete */

    retry = 0;

    while (1)
    {
        v = readl(device->plat_dat.addr + mii_address);

        if (!(v & MII_BUSY))
        {
            break;
        }
        mdelay(10);
        retry++;
        if (retry >= 10)
        {
            return OS_EBUSY;
        }
    }
    return OS_EOK;
}

int __mdiobus_read(struct mii_bus *bus, int addr, unsigned int regnum)
{
    int retval;

    retval = bus->read(bus, addr, regnum);

    return retval;
}

int mdiobus_read(struct mii_bus *bus, int addr, unsigned int regnum)
{
    int retval;

    os_mutex_lock(&bus->mdio_lock, OS_WAIT_FOREVER);
    retval = __mdiobus_read(bus, addr, regnum);
    os_mutex_unlock(&bus->mdio_lock);

    return retval;
}

int __mdiobus_write(struct mii_bus *bus, int addr, unsigned int regnum, unsigned short val)
{
    int err;

    err = bus->write(bus, addr, regnum, val);
    return err;
}

int mdiobus_write(struct mii_bus *bus, int addr, unsigned int regnum, unsigned short val)
{
    int err;

    os_mutex_lock(&bus->mdio_lock, OS_WAIT_FOREVER);
    err = __mdiobus_write(bus, addr, regnum, val);
    os_mutex_unlock(&bus->mdio_lock);

    return err;
}

int get_phy_id(struct mii_bus *bus, int addr, unsigned int *phy_id)
{
    int phy_reg;

    /* Grab the bits from PHYIR1, and put them in the upper half */
    phy_reg = mdiobus_read(bus, addr, MII_PHYSID1);
    if (phy_reg < 0)
    {

        return OS_ERROR;
    }

    *phy_id = (phy_reg & 0xffff) << 16;

    /* Grab the bits from PHYIR2, and put them in the lower half */
    phy_reg = mdiobus_read(bus, addr, MII_PHYSID2);
    if (phy_reg < 0)
    {
        /* returning -ENODEV doesn't stop bus scanning */
        return OS_ERROR;
    }

    *phy_id |= (phy_reg & 0xffff);

    return OS_EOK;
}

int mdiobus_scan(struct mii_bus *bus, int addr, unsigned int *phy_id)
{
    int err;

    err = get_phy_id(bus, addr, phy_id);
    if (err)
    {
        return err;
    }
    if ((*phy_id & 0x1fffffff) == 0x1fffffff)
    {
        return OS_ERROR;
    }

    return OS_EOK;
}

static inline int phy_read(struct phy_device *phydev, unsigned int regnum)
{
    return mdiobus_read(&phydev->device->mii_bus, phydev->device->phy_addr, regnum);
}

static inline int __phy_read(struct phy_device *phydev, unsigned int regnum)
{
    return __mdiobus_read(&phydev->device->mii_bus, phydev->device->phy_addr, regnum);
}

static inline int phy_write(struct phy_device *phydev, unsigned int regnum, unsigned short val)
{
    return mdiobus_write(&phydev->device->mii_bus, phydev->device->phy_addr, regnum, val);
}

static inline int __phy_write(struct phy_device *phydev, unsigned int regnum, unsigned short val)
{
    return __mdiobus_write(&phydev->device->mii_bus, phydev->device->phy_addr, regnum, val);
}

int __phy_modify(struct phy_device *phydev, unsigned int regnum, unsigned short mask, unsigned short set)
{
    int ret;

    ret = __phy_read(phydev, regnum);
    if (ret < 0)
        return ret;

    ret = __phy_write(phydev, regnum, (ret & ~mask) | set);

    return ret < 0 ? ret : 0;
}

int phy_modify(struct phy_device *phydev, unsigned int regnum, unsigned short mask, unsigned short set)
{
    int ret;

    os_mutex_lock(&phydev->device->mii_bus.mdio_lock, OS_WAIT_FOREVER);
    ret = __phy_modify(phydev, regnum, mask, set);
    os_mutex_unlock(&phydev->device->mii_bus.mdio_lock);

    return ret;
}

int genphy_resume(struct phy_device *phydev)
{
    return phy_modify(phydev, MII_BMCR, BMCR_PDOWN, 0);
}

int __phy_resume(struct phy_device *phydev)
{
    int ret = 0;

    ret = genphy_resume(phydev);

    return ret;
}

int phy_resume(struct phy_device *phydev)
{
    int ret;

    os_mutex_lock(&phydev->lock, OS_WAIT_FOREVER);
    ret = __phy_resume(phydev);
    os_mutex_unlock(&phydev->lock);

    return ret;
}

int genphy_config_init(struct phy_device *phydev)
{
    int          val;
    unsigned int features;

    phydev->supported   = 0x6fff;
    phydev->advertising = 0xfff;

    features = (SUPPORTED_TP | SUPPORTED_MII | SUPPORTED_AUI | SUPPORTED_FIBRE | SUPPORTED_BNC | SUPPORTED_Pause |
                SUPPORTED_Asym_Pause);

    /* Do we support autonegotiation? */
    val = phy_read(phydev, MII_BMSR);
    if (val < 0)
        return val;

    if (val & BMSR_ANEGCAPABLE)
        features |= SUPPORTED_Autoneg;

    if (val & BMSR_100FULL)
        features |= SUPPORTED_100baseT_Full;
    if (val & BMSR_100HALF)
        features |= SUPPORTED_100baseT_Half;
    if (val & BMSR_10FULL)
        features |= SUPPORTED_10baseT_Full;
    if (val & BMSR_10HALF)
        features |= SUPPORTED_10baseT_Half;

    if (val & BMSR_ESTATEN)
    {
        val = phy_read(phydev, MII_ESTATUS);
        if (val < 0)
            return val;

        if (val & ESTATUS_1000_TFULL)
            features |= SUPPORTED_1000baseT_Full;
        if (val & ESTATUS_1000_THALF)
            features |= SUPPORTED_1000baseT_Half;
    }

    phydev->supported &= features;
    phydev->advertising &= features;

    return 0;
}

int genphy_update_link(struct phy_device *phydev)
{
    int status;

    status = phy_read(phydev, MII_BMSR);
    if (status < 0)
        return status;
    phydev->link             = status & BMSR_LSTATUS ? 1 : 0;
    phydev->autoneg_complete = status & BMSR_ANEGCOMPLETE ? 1 : 0;

    return 0;
}

static inline unsigned int mii_adv_to_ethtool_adv_t(unsigned int adv)
{
    unsigned int result = 0;

    if (adv & ADVERTISE_10HALF)
        result |= ADVERTISED_10baseT_Half;
    if (adv & ADVERTISE_10FULL)
        result |= ADVERTISED_10baseT_Full;
    if (adv & ADVERTISE_100HALF)
        result |= ADVERTISED_100baseT_Half;
    if (adv & ADVERTISE_100FULL)
        result |= ADVERTISED_100baseT_Full;
    if (adv & ADVERTISE_PAUSE_CAP)
        result |= ADVERTISED_Pause;
    if (adv & ADVERTISE_PAUSE_ASYM)
        result |= ADVERTISED_Asym_Pause;

    return result;
}

static inline unsigned int ethtool_adv_to_mii_ctrl1000_t(unsigned int ethadv)
{
    unsigned int result = 0;

    if (ethadv & ADVERTISED_1000baseT_Half)
        result |= ADVERTISE_1000HALF;
    if (ethadv & ADVERTISED_1000baseT_Full)
        result |= ADVERTISE_1000FULL;

    return result;
}
static inline unsigned int ethtool_adv_to_mii_adv_t(unsigned int ethadv)
{
    unsigned int result = 0;

    if (ethadv & ADVERTISED_10baseT_Half)
        result |= ADVERTISE_10HALF;
    if (ethadv & ADVERTISED_10baseT_Full)
        result |= ADVERTISE_10FULL;
    if (ethadv & ADVERTISED_100baseT_Half)
        result |= ADVERTISE_100HALF;
    if (ethadv & ADVERTISED_100baseT_Full)
        result |= ADVERTISE_100FULL;
    if (ethadv & ADVERTISED_Pause)
        result |= ADVERTISE_PAUSE_CAP;
    if (ethadv & ADVERTISED_Asym_Pause)
        result |= ADVERTISE_PAUSE_ASYM;

    return result;
}

static inline unsigned int mii_stat1000_to_ethtool_lpa_t(unsigned int lpa)
{
    unsigned int result = 0;

    if (lpa & LPA_1000HALF)
        result |= ADVERTISED_1000baseT_Half;
    if (lpa & LPA_1000FULL)
        result |= ADVERTISED_1000baseT_Full;

    return result;
}

static inline unsigned int mii_lpa_to_ethtool_lpa_t(unsigned int lpa)
{
    unsigned int result = 0;

    if (lpa & LPA_LPACK)
        result |= ADVERTISED_Autoneg;

    return result | mii_adv_to_ethtool_adv_t(lpa);
}

int genphy_read_status(struct phy_device *phydev)
{
    int adv;
    int err;
    int lpa;
    int lpagb = 0;
    int common_adv;
    int common_adv_gb = 0;

    /* Update the link, but return if there was an error */
    err = genphy_update_link(phydev);
    if (err)
        return err;

    phydev->lp_advertising = 0;

    if (AUTONEG_ENABLE == phydev->autoneg && phydev->autoneg_complete)
    {
        if (phydev->supported & (SUPPORTED_1000baseT_Half | SUPPORTED_1000baseT_Full))
        {
            lpagb = phy_read(phydev, MII_STAT1000);
            if (lpagb < 0)
                return lpagb;

            adv = phy_read(phydev, MII_CTRL1000);
            if (adv < 0)
                return adv;

            if (lpagb & LPA_1000MSFAIL)
            {
                return OS_ERROR;
            }

            phydev->lp_advertising = mii_stat1000_to_ethtool_lpa_t(lpagb);
            common_adv_gb          = lpagb & adv << 2;
        }

        lpa = phy_read(phydev, MII_LPA);
        if (lpa < 0)
            return lpa;

        phydev->lp_advertising |= mii_lpa_to_ethtool_lpa_t(lpa);

        adv = phy_read(phydev, MII_ADVERTISE);
        if (adv < 0)
            return adv;

        common_adv = lpa & adv;

        phydev->speed      = SPEED_10;
        phydev->duplex     = DUPLEX_HALF;
        phydev->pause      = 0;
        phydev->asym_pause = 0;

        if (common_adv_gb & (LPA_1000FULL | LPA_1000HALF))
        {
            phydev->speed = SPEED_1000;

            if (common_adv_gb & LPA_1000FULL)
                phydev->duplex = DUPLEX_FULL;
        }
        else if (common_adv & (LPA_100FULL | LPA_100HALF))
        {
            phydev->speed = SPEED_100;

            if (common_adv & LPA_100FULL)
                phydev->duplex = DUPLEX_FULL;
        }
        else if (common_adv & LPA_10FULL)
            phydev->duplex = DUPLEX_FULL;

        if (phydev->duplex == DUPLEX_FULL)
        {
            phydev->pause      = lpa & LPA_PAUSE_CAP ? 1 : 0;
            phydev->asym_pause = lpa & LPA_PAUSE_ASYM ? 1 : 0;
        }
    }
    else if (phydev->autoneg == AUTONEG_DISABLE)
    {
        int bmcr = phy_read(phydev, MII_BMCR);

        if (bmcr < 0)
            return bmcr;

        if (bmcr & BMCR_FULLDPLX)
            phydev->duplex = DUPLEX_FULL;
        else
            phydev->duplex = DUPLEX_HALF;

        if (bmcr & BMCR_SPEED1000)
            phydev->speed = SPEED_1000;
        else if (bmcr & BMCR_SPEED100)
            phydev->speed = SPEED_100;
        else
            phydev->speed = SPEED_10;

        phydev->pause      = 0;
        phydev->asym_pause = 0;
    }

    return 0;
}

int phy_init_hw(struct phy_device *phydev)
{
    return genphy_config_init(phydev);
}

int phy_read_status(struct phy_device *phydev)
{
    return genphy_read_status(phydev);
}

int genphy_aneg_done(struct phy_device *phydev)
{
    int retval = phy_read(phydev, MII_BMSR);

    return (retval < 0) ? retval : (retval & BMSR_ANEGCOMPLETE);
}

int phy_aneg_done(struct phy_device *phydev)
{
    return genphy_aneg_done(phydev);
}

static void mmd_phy_indirect(struct mii_bus *bus, int phy_addr, int devad, unsigned short regnum)
{
    /* Write the desired MMD Devad */
    __mdiobus_write(bus, phy_addr, MII_MMD_CTRL, devad);

    /* Write the desired MMD register address */
    __mdiobus_write(bus, phy_addr, MII_MMD_DATA, regnum);

    /* Select the Function : DATA with no post increment */
    __mdiobus_write(bus, phy_addr, MII_MMD_CTRL, devad | MII_MMD_CTRL_NOINCR);
}

int phy_read_mmd(struct phy_device *phydev, int devad, unsigned int regnum)
{
    int val;

    if (regnum > (unsigned short)~0 || devad > 32)
        return OS_ERROR;

    struct mii_bus *bus      = &phydev->device->mii_bus;
    int             phy_addr = phydev->device->phy_addr;

    os_mutex_lock(&bus->mdio_lock, OS_WAIT_FOREVER);
    mmd_phy_indirect(bus, phy_addr, devad, regnum);

    /* Read the content of the MMD's selected register */
    val = __mdiobus_read(bus, phy_addr, MII_MMD_DATA);
    os_mutex_unlock(&bus->mdio_lock);
    return val;
}

int phy_write_mmd(struct phy_device *phydev, int devad, unsigned int regnum, unsigned short val)
{
    int ret;

    if (regnum > (unsigned short)~0 || devad > 32)
        return OS_ERROR;

    struct mii_bus *bus      = &phydev->device->mii_bus;
    int             phy_addr = phydev->device->phy_addr;

    os_mutex_lock(&bus->mdio_lock, OS_WAIT_FOREVER);
    mmd_phy_indirect(bus, phy_addr, devad, regnum);

    /* Write the data into MMD's selected register */
    __mdiobus_write(bus, phy_addr, MII_MMD_DATA, val);
    os_mutex_unlock(&bus->mdio_lock);

    ret = 0;
    return ret;
}

static int genphy_config_eee_advert(struct phy_device *phydev)
{
    int broken = phydev->eee_broken_modes;
    int old_adv, adv;

    /* Nothing to disable */
    if (!broken)
        return 0;

    /* If the following call fails, we assume that EEE is not
     * supported by the phy. If we read 0, EEE is not advertised
     * In both case, we don't need to continue
     */
    adv = phy_read_mmd(phydev, MDIO_MMD_AN, MDIO_AN_EEE_ADV);
    if (adv <= 0)
        return 0;

    old_adv = adv;
    adv &= ~broken;

    /* Advertising remains unchanged with the broken mask */
    if (old_adv == adv)
        return 0;

    phy_write_mmd(phydev, MDIO_MMD_AN, MDIO_AN_EEE_ADV, adv);

    return 1;
}

static int genphy_config_advert(struct phy_device *phydev)
{
    unsigned int advertise;
    int          oldadv, adv, bmsr;
    int          err, changed = 0;

    /* Only allow advertising what this PHY supports */
    phydev->advertising &= phydev->supported;
    advertise = phydev->advertising;

    /* Setup standard advertisement */
    adv = phy_read(phydev, MII_ADVERTISE);
    if (adv < 0)
        return adv;

    oldadv = adv;
    adv &= ~(ADVERTISE_ALL | ADVERTISE_100BASE4 | ADVERTISE_PAUSE_CAP | ADVERTISE_PAUSE_ASYM);
    adv |= ethtool_adv_to_mii_adv_t(advertise);

    if (adv != oldadv)
    {
        err = phy_write(phydev, MII_ADVERTISE, adv);

        if (err < 0)
            return err;
        changed = 1;
    }

    bmsr = phy_read(phydev, MII_BMSR);
    if (bmsr < 0)
        return bmsr;

    /* Per 802.3-2008, Section 22.2.4.2.16 Extended status all
     * 1000Mbits/sec capable PHYs shall have the BMSR_ESTATEN bit set to a
     * logical 1.
     */
    if (!(bmsr & BMSR_ESTATEN))
        return changed;

    /* Configure gigabit if it's supported */
    adv = phy_read(phydev, MII_CTRL1000);
    if (adv < 0)
        return adv;

    oldadv = adv;
    adv &= ~(ADVERTISE_1000FULL | ADVERTISE_1000HALF);

    if (phydev->supported & (SUPPORTED_1000baseT_Half | SUPPORTED_1000baseT_Full))
    {
        adv |= ethtool_adv_to_mii_ctrl1000_t(advertise);
    }

    if (adv != oldadv)
        changed = 1;

    err = phy_write(phydev, MII_CTRL1000, adv);
    if (err < 0)
        return err;

    return changed;
}

int genphy_restart_aneg(struct phy_device *phydev)
{
    /* Don't isolate the PHY if we're negotiating */
    return phy_modify(phydev, MII_BMCR, BMCR_ISOLATE, BMCR_ANENABLE | BMCR_ANRESTART);
}
int genphy_config_aneg(struct phy_device *phydev)
{
    int err, changed;

    changed = genphy_config_eee_advert(phydev);

    err = genphy_config_advert(phydev);
    if (err < 0) /* error */
        return err;

    changed |= err;
    if (changed == 0)
    {
        /* Advertisement hasn't changed, but maybe aneg was never on to
         * begin with?  Or maybe phy was isolated?
         */
        int ctl = phy_read(phydev, MII_BMCR);

        if (ctl < 0)
            return ctl;

        if (!(ctl & BMCR_ANENABLE) || (ctl & BMCR_ISOLATE))
            changed = 1; /* do restart aneg */
    }

    /* Only restart aneg if we are advertising something different
     * than we were before.
     */
    if (changed > 0)
        return genphy_restart_aneg(phydev);

    return 0;
}

static int phy_config_aneg(struct phy_device *phydev)
{
    return genphy_config_aneg(phydev);
}

void phy_trigger_machine(struct phy_device *phydev)
{
    os_sem_post(&phydev->sem);
}

int phy_start_aneg_priv(struct phy_device *phydev, unsigned int sync)
{
    unsigned int trigger = 0;
    int          err;

    os_mutex_lock(&phydev->lock, OS_WAIT_FOREVER);

    /* Invalidate LP advertising flags */
    phydev->lp_advertising = 0;

    err = phy_config_aneg(phydev);
    if (err < 0)
        goto out_unlock;

    if (phydev->state != PHY_HALTED)
    {
        if (AUTONEG_ENABLE == phydev->autoneg)
        {
            phydev->state        = PHY_AN;
            phydev->link_timeout = PHY_AN_TIMEOUT;
        }
        else
        {
            phydev->state        = PHY_FORCING;
            phydev->link_timeout = PHY_FORCE_TIMEOUT;
        }
    }

out_unlock:
    os_mutex_unlock(&phydev->lock);

    if (trigger)
        phy_trigger_machine(phydev);

    return err;
}
