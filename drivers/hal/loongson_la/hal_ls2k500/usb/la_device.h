/*	$OpenBSD: device.h,v 1.17 1999/08/08 00:37:09 niklas Exp $	*/
/*	$NetBSD: device.h,v 1.15 1996/04/09 20:55:24 cgd Exp $	*/

/*
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This software was developed by the Computer Systems Engineering group
 * at Lawrence Berkeley Laboratory under DARPA contract BG 91-66 and
 * contributed to Berkeley.
 *
 * All advertising materials mentioning features or use of this software
 * must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Lawrence Berkeley Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)device.h	8.2 (Berkeley) 2/17/94
 */

#ifndef _SYS_DEVICE_H_
#define	_SYS_DEVICE_H_

#include <stddef.h>

struct confargs {
    char	*ca_name;		/* Device name. */
    unsigned int ca_node;
    int ca_nreg;
    unsigned int *ca_reg;
    int ca_nintr;
    int *ca_intr;
    unsigned long ca_baseaddr;
};

/*
 * Minimal device structures.
 * Note that all ``system'' device types are listed here.
 */
enum devclass {
    DV_DULL,		/* generic, no special info */
    DV_CPU,			/* CPU (carries resource utilization) */
    DV_DISK,		/* disk drive (label, etc) */
    DV_IFNET,		/* network interface */
    DV_TAPE,		/* tape device */
    DV_TTY			/* serial line interface (???) */
};

/*
 * Actions for ca_activate.
 */
enum devact {
    DVACT_ACTIVATE,		/* activate the device */
    DVACT_DEACTIVATE,	/* deactivate the device */
};

//wan+ if
/*
 * Actions for ca_activate.
 */
#define DVACT_ACTIVATE      0   /* activate the device */
#define DVACT_DEACTIVATE    1   /* deactivate the device */
#define DVACT_SUSPEND       2   /* suspend the device */
#define DVACT_RESUME        3   /* resume the device */
#define DVACT_QUIESCE       4   /* warn the device about suspend */
//wan+ end

struct la_device {
    enum	devclass dv_class;	/* this device's classification */
    //TAILQ_ENTRY(device) dv_list;	/* entry on list of all devices */
    struct	cfdata *dv_cfdata;	/* config data that found us */
    int	dv_unit;		/* device unit number */
    char	dv_xname[16];		/* external name (name + unit) */
    struct	la_device *dv_parent;	/* pointer to parent device */
    int	dv_flags;		/* misc. flags; see below */
    int	dv_ref;			/* ref count */
};

/* dv_flags */
#define	DVF_ACTIVE	0x0001		/* device is activated */

/*
 * Configuration data (i.e., data placed in ioconf.c).
 */
struct cfdata {
    struct	cfattach *cf_attach;	/* config attachment */
    struct	cfdriver *cf_driver;	/* config driver */
    short	cf_unit;		/* unit number */
    short	cf_fstate;		/* finding state (below) */
    int	*cf_loc;		/* locators (machine dependent) */
    int	cf_flags;		/* flags from config */
    short	*cf_parents;		/* potential parents */
    int	cf_locnames;		/* start of names */
    void (**cf_ivstubs)(void);/* config-generated vectors, if any */
    short	cf_starunit1;		/* 1st usable unit number by STAR */
};
extern struct cfdata cfdata[];
#define FSTATE_NOTFOUND	0	/* has not been found */
#define	FSTATE_FOUND	1	/* has been found */
#define	FSTATE_STAR	2	/* duplicable */
#define FSTATE_DNOTFOUND 3	/* has not been found, and is disabled */
#define FSTATE_DSTAR	4	/* duplicable, and is disabled */

typedef int (*cfmatch_t)(struct la_device *, void *, void *);
typedef void (*cfscan_t)(struct la_device *, void *);

/*
 * `configuration' attachment and driver (what the machine-independent
 * autoconf uses).  As devices are found, they are applied against all
 * the potential matches.  The one with the best match is taken, and a
 * device structure (plus any other data desired) is allocated.  Pointers
 * to these are placed into an array of pointers.  The array itself must
 * be dynamic since devices can be found long after the machine is up
 * and running.
 *
 * Devices can have multiple configuration attachments if they attach
 * to different attributes (busses, or whatever), to allow specification
 * of multiple match and attach functions.  There is only one configuration
 * driver per driver, so that things like unit numbers and the device
 * structure array will be shared.
 */
struct cfattach {
    size_t	  ca_devsize;		/* size of dev data (for malloc) */
    cfmatch_t ca_match;		/* returns a match level */
    void	(*ca_attach)(struct la_device *, struct la_device *, void *);
    int	(*ca_detach)(struct la_device *, int);
    int	(*ca_activate)(struct la_device *, enum devact);
    void    (*ca_zeroref)(struct la_device *);
};

/* Flags given to config_detach(), and the ca_detach function. */
#define	DETACH_FORCE	0x01		/* force detachment; hardware gone */
#define	DETACH_QUIET	0x02		/* don't print a notice */

struct cfdriver {
    void	**cd_devs;		/* devices found */
    char	*cd_name;		/* device name */
    enum	devclass cd_class;	/* device classification */
    int	cd_indirect;		/* indirectly configure subdevices */
    int	cd_ndevs;		/* size of cd_devs array */
};

/*
 * Configuration printing functions, and their return codes.  The second
 * argument is NULL if the device was configured; otherwise it is the name
 * of the parent device.  The return value is ignored if the device was
 * configured, so most functions can return UNCONF unconditionally.
 */
typedef int (*cfprint_t)(void *, const char *);
#define	QUIET	0		/* print nothing */
#define	UNCONF	1		/* print " not configured\n" */
#define	UNSUPP	2		/* print " not supported\n" */

/*
 * Pseudo-device attach information (function + number of pseudo-devs).
 */
struct pdevinit {
    void	(*pdev_attach)(int);
    int	pdev_count;
};

struct cftable {
    struct cfdata *tab;
};

struct la_device *config_found_sm(struct la_device *, void *, cfprint_t,cfmatch_t);
struct la_device *config_attach(struct la_device *, void *, void *, cfprint_t);
struct la_device *config_make_softc(struct la_device *parent,struct cfdata *cf);
void device_ref(struct la_device *);
void device_unref(struct la_device *);

struct nam2blk { //wan+
    char	*name;
    int	maj;
};

/* compatibility definitions */
#define config_found(d, a, p)	config_found_sm((d), (a), (p), NULL)

#endif /* !_SYS_DEVICE_H_ */

