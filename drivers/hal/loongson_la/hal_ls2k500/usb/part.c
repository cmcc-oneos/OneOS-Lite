/* $Id: devfs.c,v 1.1.1.1 2006/03/10 03:27:14 cpu Exp $ */

/*
 * Copyright (c) 1998-2003 Opsycon AB (www.opsycon.se)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by Opsycon AB, Sweden.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/*
 *	_Very_ simplified support functions to i/o subsystem.
 */

#include "la_device.h"
#include "usb.h"
#include <os_memory.h>
#include <os_stddef.h>
#include <os_util.h>
#include <string.h>

extern block_dev_desc_t usb_dev_desc[USB_MAX_STOR_DEV];

/*GPT header magic: EFI''PART*/
static unsigned char gpt_magic[8] = {
    0x45, 0x46, 0x49, 0x20, 0x50, 0x41, 0x52, 0x54
};

inline static unsigned int get_part_size(unsigned char* rb_entry)
{
    unsigned int size;
    size = *(unsigned short *)(rb_entry + 12 + 2);
    size <<= 16;
    size += *(unsigned short *)(rb_entry + 12);

    return size;
}

inline static unsigned int get_logical_part_offset(unsigned char * rb_entry)
{
    unsigned int offset;
    offset = *(unsigned short *)(rb_entry + 8 + 2);
    offset <<= 16;
    offset += *(unsigned short *)(rb_entry + 8);
    return offset;
}

static int read_mbr_part_table(int device, unsigned int mbr_sec_off, DiskPartitionTable **table, unsigned char *leadbuf)
{
    unsigned char *partbuf;
    int i;
    unsigned char tag;
    DiskPartitionTable* part;
    int cnt = 0;
    int id = 1;
    unsigned int size;
    unsigned int sec_off;
    int part_index = 0;
    block_dev_desc_t *usb_desc = &usb_dev_desc[device];

    if (table == NULL) {
        return 0;
    }
    if ((partbuf = (unsigned char *) os_malloc(usb_desc->blksz)) == OS_NULL) {
        os_kprintf("Can't alloc memory for the partition block!\n\r");
        return 0;
    }
    //search the partion table to find the partition with id=0x83 and 0x05
    for (cnt = 0, i = 446; i < 510; i += 0x10) {
        tag = leadbuf[i + 4];	//0x83
        sec_off = get_logical_part_offset(leadbuf + i);
        size = get_part_size(leadbuf + i);

        if (tag == 0 && sec_off == 0 && size == 0) {
            id++;
            continue;
        }

        part = (DiskPartitionTable *)os_malloc(sizeof(DiskPartitionTable));
        if (part == OS_NULL) {
            os_kprintf("Can't alloc memory for the part pointer!\n\r");
            continue;
        }
        memset(part, 0, sizeof(DiskPartitionTable));
        part->tag = tag;
        part->id = id;
        part->bootflag = 0;
        part->sec_begin = sec_off;
        part->size = size;
        part->sec_end = part->size + part->sec_begin;
        part->Next = OS_NULL;
        part->logical = OS_NULL;
        part->fs = OS_NULL;
        //do the fix ,because Partition table do not tell
        //the file system type of partition so correctly.
        if(1 != usb_desc->block_read(device,sec_off,1,(unsigned long *)partbuf,OS_NULL)){
            os_free(partbuf);
            os_kprintf("Can't read the leading block from disk!\n\r");
            return 0;
        }

        table[part_index] = part;
        part_index++;
        cnt++;
        id++;
    }
    os_free(leadbuf);
    os_free(partbuf);
    return cnt;
}

static int read_gpt_part_table(int device, DiskPartitionTable **table, unsigned char *leadbuf)
{
    int n, k;
    int cnt = 0;
    int id = 1;
    int part_index = 0;
    unsigned char *partbuf;
    gpt_header header;
    DiskPartitionTable* part;
    gpt_partentry entry;
    block_dev_desc_t *usb_desc = &usb_dev_desc[device];

    if ((partbuf = (unsigned char *) os_malloc(usb_desc->blksz)) == OS_NULL) {
        os_kprintf("Can't alloc memory for the partition block!\n\r");
        return 0;
    }
    /* Read the GPT header */
    if(1 != usb_desc->block_read(device,1,1,(unsigned long *)&(header.gpt[0]),OS_NULL)){
        os_free(leadbuf);
        os_kprintf("Can't read the leading block from disk!\n\r");
        return 0;
    }

    if(memcmp(header.magic, gpt_magic, sizeof(gpt_magic)) == 0) {
        os_kprintf("This is GPT partition disk\n\r");
    } else {
        os_kprintf("read [EFI PART] is erro!\n\r");
        return 0;
    }

    /*GPT parition table*/
    for (n = 8; n < (header.maxpart); n++) {
        k = n % 4;
        if (k == 0) {            
            if(1 != usb_desc->block_read(device,n/4,1,(unsigned long *)leadbuf,OS_NULL)){
                os_free(leadbuf);
                os_kprintf("Can't read the leading block from disk!\n\r");
                return 0;
            }
        }
        memcpy(entry.ent, (char *)((unsigned long)leadbuf + k * 128), sizeof(entry.ent));

        part = (DiskPartitionTable *)os_malloc(sizeof(DiskPartitionTable));
        if (entry.start == 0 && entry.end == 0) {
            id++;
            /* this parition is NULL nothing to do */
            continue;
        }
        /*0x83 is a default value*/
        part->tag = 0x83;
        part->id = id;
        part->bootflag = 0;
        part->sec_begin = entry.start;
        part->size = entry.end - entry.start;
        part->sec_end = entry.end;
        part->Next = NULL;
        part->logical = NULL;
        part->fs = NULL;

        //do the fix ,because Partition table do not tell
        //the file system type of partition so correctly.
        if(1 != usb_desc->block_read(device,entry.start,1,(unsigned long *)partbuf,OS_NULL)){
            os_free(leadbuf);
            os_kprintf("Can't read the leading block from disk!\n\r");
            return 0;
        }

        table[part_index] = part;
        part_index++;
        cnt++;
        id++;

    }
    os_free(leadbuf);
    os_free(partbuf);
    return cnt;
}

int dev_part_read(int device, DiskPartitionTable** ppTable)
{
    unsigned char *leadbuf;
    int cnt = 0;
    int i;
    block_dev_desc_t *usb_desc = &usb_dev_desc[device];

    if ((leadbuf = (unsigned char *) os_malloc(usb_desc->blksz)) == OS_NULL) {
        os_kprintf("Can't alloc memory for the super block!\n\r");
        return 0;
    }

    /*the first MBR partition*/
    if(1 != usb_desc->block_read(device,0,1,(unsigned long *)leadbuf,OS_NULL)){
        os_free(leadbuf);
        os_kprintf("Can't read the leading block from disk!\n\r");
        return 0;
    }

    if (leadbuf[510] != 0x55 || leadbuf[511] != 0xaa) {  
        /*read MBR partition failed*/
        return 0; 
    }
    /* Make sure the MBR is a protective MBR(GPT) and not a normal MBR */
    i = 446;
    while ((i < 510) && (leadbuf[i + 4] != 0xee)){
        i+= 0x10;
    }
    /* GPT partition */
    if (i < 510) {
        os_kprintf("This is GPT partition disk\n\r");
        cnt = read_gpt_part_table(device, ppTable, leadbuf);
        if (cnt <= 0) {
            return 0;
        }
        return cnt;

    } else {
        /*mbr*/
        os_kprintf("This is MBR partition disk\n\r");
        cnt = read_mbr_part_table(device, 0, ppTable, leadbuf);
        if (cnt <= 0) {
            return 0;
        }
        return cnt;
    }
}
