#ifndef _LOONGARCH_CPU_H_
#define _LOONGARCH_CPU_H_

#define KUSEG_ADDR		0x0
#define CACHED_MEMORY_ADDR	0x8000000000000000
#define UNCACHED_MEMORY_ADDR	0x8000000000000000
#define MAX_MEM_ADDR		PHYS_TO_UNCACHED(0x1e000000)
#define	RESERVED_ADDR		PHYS_TO_UNCACHED(0x1fc80000)
#define IS_CACHED_ADDR(x)	(!!(((x) & 0xff00000000000000ULL) == CACHED_MEMORY_ADDR))

#define CACHED_TO_PHYS(x)	VA_TO_PHYS(x)
#define UNCACHED_TO_PHYS(x)	VA_TO_PHYS(x)

#define __le16_to_cpu(x) ((unsigned short)(x))

typedef	unsigned long 	vm_offset_t;
#define VA_TO_PA(x)     UNCACHED_TO_PHYS(x)
#define PA_TO_VA(x)     PHYS_TO_CACHED(x)
#define _pci_dmamap(va, len) (VA_TO_PA(va))
#define	vtophys(p)			_pci_dmamap((vm_offset_t)p, 1)

#define	PHYS_TO_CACHED(x)	((unsigned long long)(x) | CACHED_MEMORY_ADDR)
#define	PHYS_TO_UNCACHED(x) 	((unsigned long long)(x) | UNCACHED_MEMORY_ADDR)
#define	CACHED_TO_UNCACHED(x)	(PHYS_TO_UNCACHED(VA_TO_PHYS(x)))
#define UNCACHED_TO_CACHED(x)	(PHYS_TO_CACHED(VA_TO_PHYS(x)))
#define VA_TO_PHYS(x)		((unsigned long)(x) & 0xffffffffffffUL)

#endif /* !_LOONGARCH_CPU_H_ */
