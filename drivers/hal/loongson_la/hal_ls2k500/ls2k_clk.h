/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k_clk.h
 *
 * @brief       This file implements clock driver for loongson ls2k500
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __LS2K_CLK_H
#define __LS2K_CLK_H

#include <ls2k500_regs.h>

#define XTL_FREQ (100000000)

#define NODE_PLL_CONF_REG0        (0x800000001fe10400)
#define NODE_PLL_ODIV_OFFSET      (24)
#define NODE_PLL_ODIV_MASK        (0x3F)
#define NODE_PLL_DIV_LOOPC_OFFSET (16)
#define NODE_PLL_DIV_LOOPC_MASK   (0xFF)
#define NODE_PLL_DIV_REFC_OFFSET  (8)
#define NODE_PLL_DIV_REFC_MASK    (0x3F)

#define SOC_PLL_CONF_REG0        (0x800000001fe10410)
#define SOC_PLL_ODIV_GPU_OFFSET  (24)
#define SOC_PLL_ODIV_GPU_MASK    (0x3F)
#define SOC_PLL_DIV_LOOPC_OFFSET (16)
#define SOC_PLL_DIV_LOOPC_MASK   (0xFF)
#define SOC_PLL_DIV_REFC_OFFSET  (8)
#define SOC_PLL_DIV_REFC_MASK    (0x3F)

#define SOC_PLL_CONF_REG1        (0x800000001fe10414)
#define SOC_PLL_ODIV_GMAC_OFFSET (8)
#define SOC_PLL_ODIV_GMAC_MASK   (0x3F)
#define SOC_PLL_ODIV_SB_OFFSET   (0)
#define SOC_PLL_ODIV_SB_MASK     (0x3F)

#define SB_FREQ_SCALE_REG       (0x800000001fe10428)
#define NODE_FREQ_SCALE_OFFSET  (0)
#define NODE_FREQ_SCALE_MASK    (0x7)
#define GPU_FREQ_SCALE_OFFSET   (4)
#define GPU_FREQ_SCALE_MASK     (0x7)
#define SB_FREQ_SCALE_OFFSET    (8)
#define SB_FREQ_SCALE_MASK      (0x7)
#define SATA_FREQ_SCALE_OFFSET  (12)
#define SATA_FREQ_SCALE_MASK    (0x7)
#define USB_FREQ_SCALE_OFFSET   (16)
#define USB_FREQ_SCALE_MASK     (0x7)
#define APB_FREQ_SCALE_OFFSET   (20)
#define APB_FREQ_SCALE_MASK     (0x7)
#define PRINT_FREQ_SCALE_OFFSET (24)
#define PRINT_FREQ_SCALE_MASK   (0x7)
#define LSU_FREQ_SCALE_OFFSET   (27)
#define LSU_FREQ_SCALE_MASK     (0x1f)

int clk_get_cpu_freq(void);
int clk_get_sb_freq(void);
int clk_get_apb_freq(void);
int clk_get_gmac_freq(void);
#endif
