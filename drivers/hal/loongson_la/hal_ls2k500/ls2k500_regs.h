/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k500_regs.h
 *
 * @brief       This file provides ostick function.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef LS2K500_REGS_H__
#define LS2K500_REGS_H__

#include <os_types.h>

/* Timer registers */
#define CSR_TCFG_VAL_SHIFT    2UL
#define CSR_TCFG_VAL          (0x3fffffffffffUL << CSR_TCFG_VAL_SHIFT)
#define CSR_TCFG_PERIOD_SHIFT 1UL
#define CSR_TCFG_PERIOD       (0x1UL << CSR_TCFG_PERIOD_SHIFT)
#define CSR_TCFG_EN           (0x1UL)

#define CSR_TINTCLR_TI_SHIFT 0UL
#define CSR_TINTCLR_TI       (1UL << CSR_TINTCLR_TI_SHIFT)

#define LOONGARCH_CPUCFG2 0x2
#define LOONGARCH_CPUCFG4 0x4
#define LOONGARCH_CPUCFG5 0x5

#define CPUCFG2_LLFTP (1UL << 14)

#define CSR_ECFG_LIE_MASK   (0x1FFF)
#define CSR_ECFG_LIE_OFFSET (0)
#define CSR_ECFG_VS_MASK    (0x7)
#define CSR_ECFG_VS_OFFSET  (16)

#define CAUSEF_TI  ((unsigned int)(1) << 11)
#define CAUSEF_IP2 ((unsigned int)(1) << 2)
#define CAUSEF_IP3 ((unsigned int)(1) << 3)
#define CAUSEF_IP4 ((unsigned int)(1) << 4)
#define CAUSEF_IP5 ((unsigned int)(1) << 5)

#define readb(reg) (*((volatile unsigned char *)(reg)))
#define readw(reg) (*((volatile unsigned short *)(reg)))
#define readl(reg) (*((volatile unsigned int *)(reg)))
#define readq(reg) (*((volatile unsigned long *)(reg)))

#define writeb(data, reg) ((*((volatile unsigned char *)(reg))) = (unsigned char)(data))
#define writew(data, reg) ((*((volatile unsigned short *)(reg))) = (unsigned short)(data))
#define writel(data, reg) ((*((volatile unsigned int *)(reg))) = (unsigned int)(data))
#define writeq(data, reg) ((*((volatile unsigned long *)(reg))) = (unsigned long)(data))

os_ubase_t    ecfg_csrxchg(os_ubase_t val, os_ubase_t mask);
os_ubase_t    ecfg_csrrd(void);
void          ecfg_csrwr(os_ubase_t val);
os_ubase_t    estate_csrrd(void);
void          estate_csrwr(os_ubase_t val);
os_ubase_t    eentry_csrxchg(os_ubase_t val, os_ubase_t mask);
os_ubase_t    eentry_csrrd(void);
void          eentry_csrwr(os_ubase_t val);
unsigned long tcfg_csrrd(void);
void          tcfg_csrwr(unsigned long val);
void          ticlr_csrwr(unsigned long val);
unsigned int  read_cfg(unsigned int reg);

#endif
