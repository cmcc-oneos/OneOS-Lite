/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k500_regs.c
 *
 * @brief       This file provides ostick function.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <ls2k500_regs.h>

/* clang-format off */
os_ubase_t ecfg_csrxchg(os_ubase_t val, os_ubase_t mask)
{
    __asm__ __volatile__(
            "csrxchg %0, %1, 0x4\n"
            : "+r"(val),"+r"(mask)
            :
            : "memory");
    return val;
}

os_ubase_t ecfg_csrrd(void)
{
    os_ubase_t val = 0;

    __asm__ __volatile__(
            "csrrd %0, 0x4\n"
            : "+r"(val)
            :
            : "memory");
    return val;
}

void ecfg_csrwr(os_ubase_t val)
{
    __asm__ __volatile__(
            "csrwr %0, 0x4\n"
            : "+r"(val)
            :
            : "memory");
}

os_ubase_t estate_csrrd(void)
{
    os_ubase_t val = 0;

    __asm__ __volatile__(
            "csrrd %0, 0x5\n"
            : "+r"(val)
            :
            : "memory");
    return val;
}
void estate_csrwr(os_ubase_t val)
{
    __asm__ __volatile__(
            "csrwr %0, 0x5\n"
            : "+r"(val)
            :
            : "memory");
}

os_ubase_t eentry_csrxchg(os_ubase_t val, os_ubase_t mask)
{
    __asm__ __volatile__(
            "csrxchg %0, %1, 0xc\n"
            : "+r"(val),"+r"(mask)
            :
            : "memory");
    return val;
}

os_ubase_t eentry_csrrd(void)
{
    os_ubase_t val = 0;

    __asm__ __volatile__(
            "csrrd %0, 0xc\n"
            : "+r"(val)
            :
            : "memory");
    return val;
}

void eentry_csrwr(os_ubase_t val)
{
    __asm__ __volatile__(
            "csrwr %0, 0xc\n"
            : "+r"(val)
            :
            : "memory");
}

unsigned long tcfg_csrrd(void)
{
    unsigned long val = 0;

    __asm__ __volatile__(
            "csrrd %0, 0x41\n"
            : "+r"(val)
            :
            : "memory");
    return val;
}

void tcfg_csrwr(unsigned long val)
{
    __asm__ __volatile__(
            "csrwr %0, 0x41\n"
            : "+r"(val)
            :
            : "memory");
}

void ticlr_csrwr(unsigned long val)
{
    __asm__ __volatile__(
            "csrwr %0, 0x44\n"
            : "+r"(val)
            :
            : "memory");
}

unsigned int read_cfg(unsigned int reg)
{
    unsigned int val = 0;

    __asm__ __volatile__(
            "cpucfg %0, %1\n"
            : "+r"(val),"+r"(reg)
            :
            : "memory");
    return val;
}
/* clang-format on */
