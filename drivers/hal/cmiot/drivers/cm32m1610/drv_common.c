/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_clock.h>
#include <os_memory.h>
#include "drv_common.h"
#include "drv_usart.h"

#ifdef OS_USING_SHELL
#include "shell.h"
#endif

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

#include <board.h>
#include <drv_gpio.h>

#include <cm_wdt.h>
#include <cm_sysctrl.h>
#include <cm_systick.h>

void SysTick_Handler(void)
{
    os_tick_increase();

#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

/**
 * This function will initial CM32 board.
 */
static os_err_t os_hw_board_init(void)
{

    WDT_Disable();

    GPIO_Config(GPIOB, GPIO_Pin_0, GPIO_MODE_JTAG_SWDAT);
    GPIO_Config(GPIOB, GPIO_Pin_1, GPIO_MODE_JTAG_SWCLK);

    *(volatile byte *)(reg_map_m0(0x1f000)) = 0x1c;    // Enable cm0 debug

    SYSCTRL_CLKConfig(CLOCK_48M);

    SysTick_Config(SYSCTRL_GetSystemClock() / OS_TICK_PER_SECOND);

    os_irq_enable();

    /* Heap initialization */
#if defined(OS_USING_SYS_HEAP)
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_sys_heap_init();
        os_sys_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif
    return OS_EOK;
}
OS_CORE_INIT(os_hw_board_init, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{

#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

    return OS_EOK;
}
OS_POSTCORE_INIT(board_post_init, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_SHELL
void os_hw_cpu_reset(void)
{
    SYSCTRL_SoftReset();
}
#endif
