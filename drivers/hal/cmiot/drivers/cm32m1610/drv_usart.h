/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief       The head file of usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_USART_H__
#define __DRV_USART_H__

#include <cm_gpio.h>
#include <cm_nvic.h>
#include <os_types.h>

struct cm32_usart_info
{
    USART_TypeDef uart;
    IRQn_Type     irqno;

    GPIO_TypeDef    gpio_port;
    GPIO_FunTypeDef gpio_pin_mode_tx;
    GPIO_FunTypeDef gpio_pin_mode_rx;
    os_int32_t      gpio_pin_tx;
    os_int32_t      gpio_pin_rx;

    os_int8_t dma_support;
};

int os_hw_usart_init(void);

#endif /* __DRV_USART_H__ */
