/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.c
 *
 * @brief       This file implements spi driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_cfg.h>
#include <device.h>
#include <os_memory.h>
#include <string.h>
#include "board.h"
#include "drv_spi.h"

#define DBG_TAG "drv.spi"
#include <dlog.h>

#define SPI_DUMMY_BYTE 0xA5

static os_list_node_t cm32_spi_list = OS_LIST_INIT(cm32_spi_list);

typedef struct cm32_spi
{
    struct os_spi_bus spi_bus;

    struct cm32_spi_info        *info;
    struct os_spi_configuration *cfg;

    os_list_node_t list;
} cm32_spi_t;

static inline void cm32_spi_pin_init(struct cm32_spi *spi_drv)
{
    GPIO_InitType GPIO_InitStructure;

    GPIO_InitStruct(&GPIO_InitStructure);

    uint32_t RCC_APB2_PERIPH_CLK_PORT, RCC_APB2_PERIPH_MISO_PORT, RCC_APB2_PERIPH_MOSI_PORT;

    /* sck */
    if (spi_drv->info->port_sck == GPIOA)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (spi_drv->info->port_sck == GPIOB)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (spi_drv->info->port_sck == GPIOC)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (spi_drv->info->port_sck == GPIOD)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOD;
    }
    else
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOE;
    }

    /* miso */
    if (spi_drv->info->port_miso == GPIOA)
    {
        RCC_APB2_PERIPH_MISO_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (spi_drv->info->port_miso == GPIOB)
    {
        RCC_APB2_PERIPH_MISO_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (spi_drv->info->port_miso == GPIOC)
    {
        RCC_APB2_PERIPH_MISO_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (spi_drv->info->port_miso == GPIOD)
    {
        RCC_APB2_PERIPH_MISO_PORT = RCC_APB2_PERIPH_GPIOD;
    }
    else
    {
        RCC_APB2_PERIPH_MISO_PORT = RCC_APB2_PERIPH_GPIOE;
    }

    /* mosi */
    if (spi_drv->info->port_mosi == GPIOA)
    {
        RCC_APB2_PERIPH_MOSI_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (spi_drv->info->port_miso == GPIOB)
    {
        RCC_APB2_PERIPH_MOSI_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (spi_drv->info->port_miso == GPIOC)
    {
        RCC_APB2_PERIPH_MOSI_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (spi_drv->info->port_miso == GPIOD)
    {
        RCC_APB2_PERIPH_MOSI_PORT = RCC_APB2_PERIPH_GPIOD;
    }
    else
    {
        RCC_APB2_PERIPH_MOSI_PORT = RCC_APB2_PERIPH_GPIOE;
    }

    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_CLK_PORT | RCC_APB2_PERIPH_MISO_PORT | RCC_APB2_PERIPH_MOSI_PORT, ENABLE);

    if (spi_drv->info->spi_base == SPI1)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_SPI1, ENABLE);
    }
    else if (spi_drv->info->spi_base == SPI2)
    {
        RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_SPI2, ENABLE);
    }
    else if (spi_drv->info->spi_base == SPI3)
    {
        RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_SPI3, ENABLE);
    }

    if (spi_drv->info->gpio_remap != 0)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO, ENABLE);
        GPIO_ConfigPinRemap(spi_drv->info->gpio_remap, ENABLE);
    }

    GPIO_InitStructure.Pin        = spi_drv->info->pin_sck;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(spi_drv->info->port_sck, &GPIO_InitStructure);

    GPIO_InitStructure.Pin = spi_drv->info->pin_mosi;
    GPIO_Init(spi_drv->info->port_mosi, &GPIO_InitStructure);

    GPIO_InitStructure.Pin       = spi_drv->info->pin_miso;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(spi_drv->info->port_miso, &GPIO_InitStructure);
}

static os_err_t cm32_spi_init(struct cm32_spi *spi_drv)
{
    struct os_spi_configuration *cfg;
    SPI_InitType                 SPI_InitStructure;
    uint32_t                     spi_clk;
    RCC_ClocksType               clks;

    cfg = spi_drv->cfg;

    OS_ASSERT(spi_drv != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    cm32_spi_pin_init(spi_drv);

    /* SPI configuration */
    SPI_InitStructure.DataDirection = SPI_DIR_DOUBLELINE_FULLDUPLEX;

    if (cfg->mode & OS_SPI_SLAVE)
    {
        SPI_InitStructure.SpiMode = SPI_MODE_SLAVE;
    }
    else
    {
        SPI_InitStructure.SpiMode = SPI_MODE_MASTER;
    }

    if (cfg->data_width == 8)
    {
        SPI_InitStructure.DataLen = SPI_DATA_SIZE_8BITS;
    }
    else if (cfg->data_width == 16)
    {
        SPI_InitStructure.DataLen = SPI_DATA_SIZE_16BITS;
    }
    else
    {
        return OS_EIO;
    }

    if (cfg->mode & OS_SPI_CPOL)
    {
        SPI_InitStructure.CLKPOL = SPI_CLKPOL_HIGH;
    }
    else
    {
        SPI_InitStructure.CLKPOL = SPI_CLKPOL_LOW;
    }

    if (cfg->mode & OS_SPI_CPHA)
    {
        SPI_InitStructure.CLKPHA = SPI_CLKPHA_SECOND_EDGE;
    }
    else
    {
        SPI_InitStructure.CLKPHA = SPI_CLKPHA_FIRST_EDGE;
    }

    SPI_InitStructure.NSS = SPI_NSS_SOFT;

    RCC_GetClocksFreqValue(&clks);
    spi_clk = (uint32_t)clks.Pclk2Freq;

    if (cfg->max_hz >= spi_clk / 2)
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_2;
    }
    else if (cfg->max_hz >= spi_clk / 4)
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_4;
    }
    else if (cfg->max_hz >= spi_clk / 8)
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_8;
    }
    else if (cfg->max_hz >= spi_clk / 16)
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_16;
    }
    else if (cfg->max_hz >= spi_clk / 32)
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_32;
    }
    else if (cfg->max_hz >= spi_clk / 64)
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_64;
    }
    else if (cfg->max_hz >= spi_clk / 128)
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_128;
    }
    else
    {
        SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_256;
    }

    if (cfg->mode & OS_SPI_MSB)
    {
        SPI_InitStructure.FirstBit = SPI_FB_MSB;
    }
    else
    {
        SPI_InitStructure.FirstBit = SPI_FB_LSB;
    }
    SPI_InitStructure.CRCPoly = 7;
    SPI_Init(spi_drv->info->spi_base, &SPI_InitStructure);

    /* Enable the SPI */
    SPI_Enable(spi_drv->info->spi_base, ENABLE);

    LOG_D(DBG_TAG, "%s init done", spi_drv->name);
    return OS_EOK;
}

static os_err_t spi_configure(struct os_spi_device *device, struct os_spi_configuration *configuration)
{
    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(configuration != OS_NULL);

    struct cm32_spi *spi_drv = os_container_of(device->bus, struct cm32_spi, spi_bus);
    spi_drv->cfg             = configuration;

    return cm32_spi_init(spi_drv);
}

static os_err_t spi_sendbuf(SPI_Module *SPIx, uint8_t *pu8Buf, uint32_t u32Len)
{
    uint32_t u32Index = 0;

    for (u32Index = 0; u32Index < u32Len; u32Index++)
    {
        while (SPI_I2S_GetStatus(SPIx, SPI_I2S_TE_FLAG) == RESET)
            ;
        SPI_I2S_TransmitData(SPIx, pu8Buf[u32Index]);

        while (SPI_I2S_GetStatus(SPIx, SPI_I2S_RNE_FLAG) == RESET)
            ;
        SPI_I2S_ReceiveData(SPIx);
    }

    return OS_EOK;
}

static os_err_t spi_recvbuf(SPI_Module *SPIx, uint8_t *pu8Buf, uint32_t u32Len)
{
    while (u32Len--)
    {
        while (SPI_I2S_GetStatus(SPIx, SPI_I2S_TE_FLAG) == RESET)
            ;
        SPI_I2S_TransmitData(SPIx, SPI_DUMMY_BYTE);

        while (SPI_I2S_GetStatus(SPIx, SPI_I2S_RNE_FLAG) == RESET)
            ;
        *pu8Buf = SPI_I2S_ReceiveData(SPIx);
        pu8Buf++;
    }

    return OS_EOK;
}

static os_uint32_t spixfer(struct os_spi_device *device, struct os_spi_message *message)
{
    os_err_t          state;
    os_size_t         message_length, already_send_length;
    os_uint32_t       send_length;
    os_uint8_t       *recv_buf;
    const os_uint8_t *send_buf;

    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(device->bus != OS_NULL);
    OS_ASSERT(message != OS_NULL);

    struct cm32_spi *spi_drv = os_container_of(device->bus, struct cm32_spi, spi_bus);

    if (message->cs_take)
    {
        os_pin_write(device->cs_pin, PIN_LOW);
    }

    LOG_D(DBG_TAG, "%s transfer prepare and start", spi_drv->name);
    LOG_D(DBG_TAG,
          "%s sendbuf: %X, recvbuf: %X, length: %d",
          spi_drv->name,
          (uint32_t)message->send_buf,
          (uint32_t)message->recv_buf,
          message->length);

    message_length = message->length;
    recv_buf       = message->recv_buf;
    send_buf       = message->send_buf;
    while (message_length)
    {
        /* the HC library use uint32 to save the data length */
        if (message_length > (uint32_t)(~0))
        {
            send_length    = (uint32_t)(~0);
            message_length = message_length - (uint32_t)(~0);
        }
        else
        {
            send_length    = message_length;
            message_length = 0;
        }

        /* calculate the start address */
        already_send_length = message->length - send_length - message_length;
        send_buf            = (os_uint8_t *)message->send_buf + already_send_length;
        recv_buf            = (os_uint8_t *)message->recv_buf + already_send_length;

        /* start once data exchange in DMA mode */
        if (message->send_buf && message->recv_buf)
        {
            memset((uint8_t *)recv_buf, 0xff, send_length);

            state = spi_sendbuf(spi_drv->info->spi_base, (uint8_t *)send_buf, send_length);

            if (state != OS_EOK)
                goto done;

            state = spi_recvbuf(spi_drv->info->spi_base, (uint8_t *)recv_buf, send_length);
        }
        else if (message->send_buf)
        {
            state = spi_sendbuf(spi_drv->info->spi_base, (uint8_t *)send_buf, send_length);
        }
        else
        {
            memset((uint8_t *)recv_buf, 0xff, send_length);
            state = spi_recvbuf(spi_drv->info->spi_base, (uint8_t *)recv_buf, send_length);
        }
    done:
        if (state != OS_EOK)
        {
            LOG_I(DBG_TAG, "spi transfer error : %d", state);
            message->length = 0;
        }

        else
        {
            LOG_D(DBG_TAG, "%s transfer done", spi_drv->name);
        }
    }

    if (message->cs_release)
    {
        os_pin_write(device->cs_pin, PIN_HIGH);
    }

    return message->length;
}

static const struct os_spi_ops cm32_spi_ops = {
    .configure = spi_configure,
    .xfer      = spixfer,
};

static int cm32_spi_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t  result = OS_ERROR;
    os_base_t level;

    struct cm32_spi *cm_spi = os_calloc(1, sizeof(struct cm32_spi));

    OS_ASSERT(cm_spi);

    cm_spi->info = (struct cm32_spi_info *)dev->info;

    struct os_spi_bus *spi_bus = &cm_spi->spi_bus;

    level = os_irq_lock();
    os_list_add_tail(&cm32_spi_list, &cm_spi->list);
    os_irq_unlock(level);

    result = os_spi_bus_register(spi_bus, dev->name, &cm32_spi_ops);
    OS_ASSERT(result == OS_EOK);

    LOG_D(DBG_TAG, "%s spi bus init done", dev->name);

    return result;
}

OS_DRIVER_INFO cm32_spi_driver = {
    .name  = "SPI_HandleTypeDef",
    .probe = cm32_spi_probe,
};

OS_DRIVER_DEFINE(cm32_spi_driver, PREV, OS_INIT_SUBLEVEL_HIGH);
