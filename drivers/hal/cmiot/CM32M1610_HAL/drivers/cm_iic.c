  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#include "cm_iic.h"

void IIC_Init(IIC_InitTypeDef *IIC_InitStruct)
{
    if (IIC_InitStruct == NULL)
    {
        return;
    }

    HWRITE(CORE_IICD_CTRL,0X07);
    HWRITE(CORE_IICD_SCL_LOW,IIC_InitStruct->IIC_Scll);
    HWRITE(CORE_IICD_SCL_HIGH,IIC_InitStruct->IIC_Sclh);
    HWRITE(CORE_IICD_START_SETUP,IIC_InitStruct->IIC_Stsu);
    HWRITE(CORE_IICD_START_HOLD,IIC_InitStruct->IIC_Sthd);
    HWRITE(CORE_IICD_STOP_SETUP,IIC_InitStruct->IIC_Sosu);
    HWRITE(CORE_IICD_DATA_SETUP,IIC_InitStruct->IIC_Dtsu);
    HWRITE(CORE_IICD_DATA_HOLD,IIC_InitStruct->IIC_Dthd);
}

void IIC_SendData(uint8_t *Src, uint16_t len)
{
    if(Src == NULL || len==0)
    {
        return;
    }
    HWRITEW(CORE_IICD_TXADDR,(uint16_t)(uint32_t)Src);
    HWRITEW(CORE_IICD_TXLEN,len);
    HWRITEW(CORE_IICD_RXLEN,0);
    HWOR(CORE_DMA_START,BIT_2);

    while(!(HREAD(CORE_DMA_STATUS) & BIT_5));

}

void IIC_ReceiveData(uint8_t *Src, uint16_t Srclen, uint8_t *Dest, uint16_t Destlen)
{
    if(Src == NULL || Srclen == 0 || Dest == NULL || Destlen ==0)
    {
        return;
    }
    HWRITEW(CORE_IICD_TXADDR,(uint16_t)(uint32_t)Src);
    HWRITEW(CORE_IICD_RXADDR,(uint16_t)(uint32_t)Dest);
    HWRITEW(CORE_IICD_TXLEN,Srclen);
    HWRITEW(CORE_IICD_RXLEN,Destlen);
    HWOR(CORE_DMA_START,BIT_2);
    while(!(HREAD(CORE_DMA_STATUS) & BIT_5));
}

