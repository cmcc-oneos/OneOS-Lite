  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_lpm.h"
#include "cm_sysctrl.h"
#include "cm_bt.h"

uint32_t LPM_Read(uint16_t addr)
{
    return HREADL(addr);
}

void LPM_Write(LPM_TypeDef type, uint32_t val)
{
    if (type<8)
    {
        HWRITEL(CORE_LPM_REG,val);
        HWRITE(CORE_LPM_WR,((1)<<(type)));
        delay_us(100);
    }
    else
    {
        type -= 8;
        HWRITEL(CORE_LPM_REG,val);
        HWRITE(CORE_LPM_WR2,((1)<<(type)));
        delay_us(100);
    }

}

void LPM_GpioUnusedPd(void)
{
    int i=0;
    for ( i = 0; i < 32; ++i)
    {
        if(HREAD(CORE_GPIO_CONF+i) == 0x00 || HREAD(CORE_GPIO_CONF+i) == 0x10)
        {
            HWRITE(CORE_GPIO_CONF+i,GPIO_MODE_PULLDOWN);
        }
    }
}

void LPM_Sleep(uint32_t time)
{
    if (time>0)
    {
        HWRITEL(mem_lpm_sleep_time,time);
    }
    else
    {
        HWRITEL(mem_lpm_sleep_time,0xffffffff);
    }
    HWRITE(mem_enter_lpm_flag,1);
    while(HREAD(mem_ipc_mcu_state) != 1);
    HWRITE(mem_ipc_mcu_state,LPM_MCU_STATE_STOP);
    delay_ms(2);
}

Boolean LPM_ChipSleep(uint32_t time, uint32_t GPIO_Wakeup_LOW, uint32_t GPIO_Wakeup_HIGH)
{
    if (GPIO_Wakeup_LOW & GPIO_Wakeup_HIGH)
    {
        return FALSE;
    }
    for(int i = 0;i < 32;i++)
    {
        if((GPIO_Wakeup_LOW & (1<<i)) != 0)
        {
             GPIO_CONFIG(i) = GPCFG_PULLUP;
        }
        else if((GPIO_Wakeup_HIGH & (1<<i)) != 0)
        {
             GPIO_CONFIG(i) = GPCFG_PULLDOWN;
        }
    }
    //注释:休眠时IO设置为浮空
	GPIO_CONFIG(10) = GPCFG_INPUT;
    HWRITEL(mem_gpio_wakeup_low,GPIO_Wakeup_LOW);
    HWRITEL(mem_gpio_wakeup_high,GPIO_Wakeup_HIGH);
	if (time > 0)
    {
        HWRITEL(mem_lpm_interval,time);
    }
    else
    {
        HWRITEL(mem_lpm_interval,0xffffffff);
    }
	OS_ENTER_CRITICAL();                    					//Enter low power
    HWRITE(IPC_MCU_STATE,IPC_MCU_STATE_STOP);
	
    return TRUE;
}

Boolean HIBERNATE_ChipSleep(uint32_t time, uint32_t GPIO_Wakeup_LOW, uint32_t GPIO_Wakeup_HIGH)
{
    if (GPIO_Wakeup_LOW & GPIO_Wakeup_HIGH)
    {
        return FALSE;
    }
    for(int i = 0;i < 32;i++)
    {
        if((GPIO_Wakeup_LOW & (1<<i)) != 0)
        {
             GPIO_CONFIG(i) = GPCFG_PULLUP;        }
        else if((GPIO_Wakeup_HIGH & (1<<i)) != 0)
        {
             GPIO_CONFIG(i) = GPCFG_PULLDOWN;
        }
    }
    HWRITEL(mem_gpio_wakeup_low,GPIO_Wakeup_LOW);
    HWRITEL(mem_gpio_wakeup_high,GPIO_Wakeup_HIGH);
	BT_SendBleCmd(BT_CMD_ENTER_HIBERNATE);
	
    LPM_Sleep(time);
    return TRUE;
}
