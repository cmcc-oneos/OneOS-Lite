  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_exti.h"
#include "cm_timer.h"
#include "cm_systick.h"
#include "cm_lpm.h"

static uint8_t ExtiEn[EXIT_Num]                = {0};
static uint8_t ExtiTrig[EXIT_Num]              = {0};  //Rising or Falling interrupt
static uint8_t ExtiRisingFallingTrig[EXIT_Num] = {0};	//Rising and Falling interrupt
static uint8_t ExtiStatus[EXIT_Num]            = {0};

extern void EXTI0_IRQHandler(void);
extern void EXTI1_IRQHandler(void);
extern void EXTI2_IRQHandler(void);
extern void EXTI3_IRQHandler(void);

void EXTI_ClearITPendingBit(uint32_t EXTI_Line, uint32_t EXTI_PinSource)
{
    EXTI_PinSource = EXTI_PinSource << (EXTI_Line * 8);
    uint32_t gpioHigh=HREADL(CORE_GPIO_WAKEUP_HIGH);
    uint32_t gpioLow=HREADL(CORE_GPIO_WAKEUP_LOW);
    if(gpioHigh & EXTI_PinSource)
    {
        gpioHigh &= ~EXTI_PinSource;
        LPM_Write(LPM_GPIOHITH,gpioHigh);
        gpioLow |= EXTI_PinSource;
        LPM_Write(LPM_GPIOLOW,gpioLow);

        EXTI_PinSource = EXTI_PinSource >> (EXTI_Line * 8);
        ExtiStatus[EXTI_Line] &= ~ ((uint8_t)EXTI_PinSource);
    }
    else
    {
        gpioLow &= ~EXTI_PinSource;
        LPM_Write(LPM_GPIOLOW,gpioLow);
        gpioHigh |= EXTI_PinSource;
        LPM_Write(LPM_GPIOHITH,gpioHigh);

        EXTI_PinSource = EXTI_PinSource >> (EXTI_Line * 8);
        ExtiStatus[EXTI_Line] &= ~ ((uint8_t)EXTI_PinSource);
    }
}

void EXTI_DeInit()
{
    uint8_t i;
    for (i = 0; i < EXIT_Num; i++)
    {
        ExtiEn[i]                = 0;
        ExtiTrig[i]              = 0;
        ExtiRisingFallingTrig[i] = 0;
        ExtiStatus[i]            = 0;
    }
        LPM_Write(LPM_GPIOHITH,0);
        LPM_Write(LPM_GPIOLOW,0);
}

uint8_t EXTI_GetITLineStatus(uint32_t EXTI_Line)
{
    return ExtiStatus[EXTI_Line];
}

void EXTI_LineConfig(uint32_t EXTI_Line, uint8_t EXTI_PinSource, EXTI_TriggerTypeDef EXTI_Trigger)
{
    uint32_t gpioHigh = HREADL(CORE_GPIO_WAKEUP_HIGH);
    uint32_t gpioLow = HREADL(CORE_GPIO_WAKEUP_LOW);

    switch (EXTI_Trigger)
    {
    case EXTI_Trigger_Off:
        ExtiEn[EXTI_Line]      &= ~EXTI_PinSource;
        gpioHigh  &= ~ (((uint32_t)EXTI_PinSource) << (EXTI_Line*8));
        LPM_Write(LPM_GPIOHITH,gpioHigh);
        gpioLow  &= ~ (((uint32_t)EXTI_PinSource) << (EXTI_Line*8));
        LPM_Write(LPM_GPIOLOW,gpioLow);
        break;

    case EXTI_Trigger_Rising:
        ExtiEn[EXTI_Line]                |= EXTI_PinSource;
        ExtiTrig[EXTI_Line]              &= ~EXTI_PinSource;
        gpioHigh |= (((uint32_t)EXTI_PinSource) << (EXTI_Line*8));
        LPM_Write(LPM_GPIOHITH,gpioHigh);
        break;

    case EXTI_Trigger_Falling:
        ExtiEn[EXTI_Line]                |= EXTI_PinSource;
        ExtiTrig[EXTI_Line]              |= EXTI_PinSource;
        gpioLow |= (((uint32_t)EXTI_PinSource) << (EXTI_Line*8));
        LPM_Write(LPM_GPIOLOW,gpioLow);
        break;

    case EXTI_Trigger_Rising_Falling:
        ExtiEn[EXTI_Line]                |= EXTI_PinSource;
        ExtiRisingFallingTrig[EXTI_Line] |= EXTI_PinSource;
        gpioLow |= (((uint32_t)EXTI_PinSource) << (EXTI_Line*8));
        LPM_Write(LPM_GPIOLOW,gpioLow);
        gpioHigh |= (((uint32_t)EXTI_PinSource) << (EXTI_Line*8));
        LPM_Write(LPM_GPIOHITH,gpioHigh);
        break;

    default:
        break;
    }
}

void GPIO_IRQHandler()
{
    disable_intr(GPIO_INTID);

    int i, j;

    for (i = 0; i < EXIT_Num; i++)
    {
        for (j = 0; j < EXIT_Pin_Num; j++)
        {
            if ((ExtiEn[i] & (1 << j)))     //Check whether gpio i interrupt are turned on
            {
                if((((HREAD(CORE_GPIO_WAKEUP_HIGH+i)&(1<<j))>0) && ((HREAD(i+CORE_GPIO_IN) & (1 << j))>0))
                  ||(((HREAD(CORE_GPIO_WAKEUP_LOW+i)&(1<<j))>0) && (((HREAD(i+CORE_GPIO_IN) & (1 << j)) == 0))))//Check for interruptions
                {
                    if (ExtiRisingFallingTrig[i] & (1 << j))
                    {
                        ExtiStatus[i] |= (1 << j);
                        switch (i)
                        {
                        case EXTI_Line0:
                            EXTI0_IRQHandler();
                            break;

                        case EXTI_Line1:
                            EXTI1_IRQHandler();
                            break;

                        case EXTI_Line2:
                            EXTI2_IRQHandler();
                            break;
                        case EXTI_Line3:
                            EXTI3_IRQHandler();
                            break;

                        default:
                            break;
                        }
                        EXTI_ClearITPendingBit(i, 1 << j);
                    }
                    else
                    {
                        if((((HREAD(CORE_GPIO_WAKEUP_HIGH+i)&(1<<j))>0) && ((ExtiTrig[i] & (1 << j)) == 0 ))
                  ||(((HREAD(CORE_GPIO_WAKEUP_LOW+i)&(1<<j))>0) && ((ExtiTrig[i] & (1 << j)) > 0)))//Check for interruptions
                        {
                                ExtiStatus[i] |= (1 << j);
                                switch (i)
                                {
                                case EXTI_Line0:
                                    EXTI0_IRQHandler();
                                    break;

                                case EXTI_Line1:
                                    EXTI1_IRQHandler();
                                    break;
                                case EXTI_Line2:
                                    EXTI2_IRQHandler();
                                    break;
                                  case EXTI_Line3:
                                    EXTI3_IRQHandler();
                                    break;
                                default:
                                    break;
                                }
                        }

                        EXTI_ClearITPendingBit(i, 1 << j);
                    }
                }
            }
        }
    }
    enable_intr(GPIO_INTID);
}

