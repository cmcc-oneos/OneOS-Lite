  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_sysctrl.h"
#include "cm_timer.h"
uint16_t tick = 6;
uint32_t rc_clk = 0;
uint8_t rc_calibrated=0;

#define LPO_CAL_CNT_REG (10)
#define LPO_CAL_CNT (1024)

void SYSCTRL_AHBPeriphClockCmd(uint16_t SYSCTRL_AHBPeriph, FunctionalState NewState)
{
    uint16_t en_ahb = 0;
    en_ahb = HREADW(CORE_CLKOFF);
    if (NewState == DISABLE)
    {
        en_ahb |= SYSCTRL_AHBPeriph;
    }
    else
    {
        en_ahb &= ~SYSCTRL_AHBPeriph;
    }
    HWRITEW(CORE_CLKOFF, en_ahb);
}

void SYSCTRL_ClkSourceSel(SysClkSource_TypeDef source)
{
    uint8_t hclk_source=0;
    if (Sysclk_lpoclk == source)
    {
        hclk_source = HREAD(CORE_ENCRYPT);
        hclk_source |= 0x08;
        HWRITE(CORE_ENCRYPT, hclk_source);
    }
    hclk_source=0;
    hclk_source = HREAD(CORE_CLKSEL);
    hclk_source &= 0xfc;
    hclk_source |= source;
    HWRITE(CORE_CLKSEL, hclk_source);
}

void SYSCTRL_CLKConfig(Sysclk_TypeDef CLK_multiple)
{
    __asm("CPSID i");
    uint8_t clk_multiple=0;
    if (HREAD(mem_xtal_flag) == 0)
    {
        SYSCTRL_ClkSourceSel(Sysclk_rc);
    }
    else
    {
        switch(CLK_multiple)
        {
            case CLOCK_24M:
                SYSCTRL_ClkSourceSel(Sysclk_crystal);
                break;
            case CLOCK_29M:
                SYSCTRL_ClkSourceSel(Sysclk_rc);
                break;
            case CLOCK_48M:
                SYSCTRL_ClkSourceSel(Sysclk_dpll);
                clk_multiple = HREAD(CORE_CONFIG);
                clk_multiple &= ~0x06;
                HWRITE(CORE_CONFIG, clk_multiple);
                break;
            case CLOCK_96M:
                SYSCTRL_ClkSourceSel(Sysclk_dpll);
                clk_multiple = HREAD(CORE_CONFIG);
                clk_multiple &= ~0x06;
                clk_multiple |= 0x04;
                HWRITE(CORE_CONFIG, clk_multiple);
                break;
            case CLOCK_153M:
                SYSCTRL_ClkSourceSel(Sysclk_dpll);
                clk_multiple = HREAD(CORE_CONFIG);
                clk_multiple &= ~0x06;
                clk_multiple |= 0x02;
                HWRITE(CORE_CONFIG, clk_multiple);
                break;
            case CLOCK_192M:
                SYSCTRL_ClkSourceSel(Sysclk_dpll);
                clk_multiple = HREAD(CORE_CONFIG);
                clk_multiple &= ~0x06;
                clk_multiple |= 0x06;
                HWRITE(CORE_CONFIG, clk_multiple);
                break;
            case CLOCK_32K:
                SYSCTRL_ClkSourceSel(Sysclk_lpoclk);
                break;
        }

    }
    __asm("CPSIE i");
    //Delay a while for work stable
    SYSCTRL_GetSystemClock();
    hw_delay();
}

uint32_t SYSCTRL_GetSystemClock(void)
{
    uint8_t rtnVal = 0;
    uint8_t clksource = HREAD(CORE_CLKSEL);
    if ((clksource & 0x03) == 2)
    {
        return SYSCTRL_GetRcClk();
    }
    if ((clksource & 0x03) == 3)
    {
        tick = 0;
        return 32000;
    }
    if ((clksource & 0x03) == 0)
    {
        rtnVal = 24;
    }
    else if ((clksource & 0x03) == 1)
    {
        uint8_t tmp = HREAD(CORE_CONFIG);
        uint8_t tmpVal = (tmp >> 1) & 0x03;
        switch(tmpVal)
        {
            case 0:{
            rtnVal = 48;
            break;
            }
            case 1:{
            rtnVal = 153;
            break;
            }
            case 2:{
            rtnVal = 96;
            break;
            }
            default:{
            rtnVal = 192;
            break;
            }
        }
    }
    tick = rtnVal/4;
    return rtnVal*MHz;
}

extern void delay(uint32_t num);
void delay_us(uint32_t us)
{
  delay(us*tick);//(((CPU_MHZ/1000000)*us)/4)
}

void delay_ms(uint32_t ms)
{
    if (tick)
    {
        delay(ms*tick*1000);//(((CPU_MHZ/1000)*us)/4)
    }
    else
    {
        delay(ms*8); // 32k clk
    }

}

uint32_t SYSCTRL_GetRcClk(void)
{
    if(rc_calibrated != 1)
    {
        uint32_t lpoClk = HREAD24BIT(mem_efuse_lpo_timer_cnt);//lop timer cal value

        if(lpoClk == 0)
        {
            lpoClk = 0x067c01;//init it when  no value
        }
        lpoClk = 0x493E0000/(lpoClk/10);
        // FT use 24M xtal, system clk is 12M, cal 1024 lpo time.
        // 1s tick =  12 * 1024 * 1000 * 1000 / efuse
        // avoid value overflow, cal is: 1s tick = 12 * 1024 * 1000 * 100 / (efuse /10) = 0x493E0000 / (efuse /10)
        HWRITE(CORE_BIST_CTRL,(LPO_CAL_CNT_REG << 4));
        HWRITE(CORE_DMA_START,0x80);

        while(!((HREAD(CORE_PERF_STATUS)) & BIT_1));//wait clock calibration is done
        uint32_t temp = HREAD24BIT(CORE_CCNT_COUNTER);
        HWRITE(CORE_BIST_CTRL,0x00);

        rc_clk = ((temp / LPO_CAL_CNT) * lpoClk);
        uint8_t clkDiv = (HREAD(CORE_CLKSEL) >> 2) & 0x07;
        uint16_t clkDivMul = 1<< (clkDiv+1);
        rc_clk = rc_clk * clkDivMul;
        tick = (rc_clk/MHz)/4;
        rc_calibrated=1;
    }
    return rc_clk;
}

void SYSCTRL_CalibrationRcClk(void)
{
    rc_calibrated=0;
    SYSCTRL_GetRcClk();
}

void SYSCTRL_IceCmd(FunctionalState NewState)
{
	uint8_t ICE_reg  = 0;
	if(NewState == DISABLE)
	{
		ICE_reg = HREAD(CORE_CLKOFF) | (0x1<<2);
		HWRITE(CORE_CLKOFF,ICE_reg);
	}
	else{
		ICE_reg = HREAD(CORE_CLKOFF) & (~(0x1<<2));
		HWRITE(CORE_CLKOFF,ICE_reg);
	}
}

void SYSCTRL_SoftReset(void)
{
	HWRITE(CORE_RESET,3);
	delay_ms(100);
}

