  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __USB_HCD_H__
#define __USB_HCD_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Include ------------------------------------------------------------------*/
#include "usb_core.h"
#include "usb_regs.h"
/* Exported types -----------------------------------------------------------*/
/* Exported constants -------------------------------------------------------*/
/* Exported macro -----------------------------------------------------------*/
/* Exported functions -------------------------------------------------------*/
/* Exported variables -------------------------------------------------------*/
/** @addtogroup USB_OTG_DRIVER
  * @{
  */

/** @defgroup USB_HCD
  * @brief This file is the
  * @{
  */

/** @defgroup USB_HCD_Exported_Defines
  * @{
  */

/** @defgroup USB_HCD_Exported_Types
  * @{
  */

/** @defgroup USB_HCD_Exported_Macros
  * @{
  */

/** @defgroup USB_HCD_Exported_Variables
  * @{
  */

/** @defgroup USB_HCD_Exported_FunctionsPrototype
  * @{
  */
uint32_t  HCD_Init(USB_OTG_CORE_HANDLE *pdev,
                   USB_OTG_CORE_ID_TypeDef coreID);
uint32_t  HCD_HC_Init(USB_OTG_CORE_HANDLE *pdev,
                      uint8_t hc_num);
uint32_t  HCD_SubmitRequest(USB_OTG_CORE_HANDLE *pdev,
                            uint8_t hc_num) ;
uint32_t  HCD_GetCurrentSpeed(USB_OTG_CORE_HANDLE *pdev);
uint32_t  HCD_ResetPort(USB_OTG_CORE_HANDLE *pdev);
uint32_t  HCD_IsDeviceConnected(USB_OTG_CORE_HANDLE *pdev);
uint16_t  HCD_GetCurrentFrame(USB_OTG_CORE_HANDLE *pdev) ;
URB_STATE HCD_GetURB_State(USB_OTG_CORE_HANDLE *pdev,  uint8_t ch_num);
uint32_t  HCD_GetXferCnt(USB_OTG_CORE_HANDLE *pdev,  uint8_t ch_num);
HC_STATUS HCD_GetHCState(USB_OTG_CORE_HANDLE *pdev,  uint8_t ch_num) ;

#ifdef __cplusplus
}
#endif

#endif  /* __USB_HCD_H__ */

