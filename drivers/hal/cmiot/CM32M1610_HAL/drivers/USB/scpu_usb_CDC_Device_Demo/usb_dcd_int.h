  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __USB_DCD_INT_H__
#define __USB_DCD_INT_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Include ------------------------------------------------------------------*/
#include "usb_dcd.h"
/* Exported types -----------------------------------------------------------*/
/* Exported constants -------------------------------------------------------*/
/* Exported macro -----------------------------------------------------------*/
/* Exported functions -------------------------------------------------------*/
/* Exported variables -------------------------------------------------------*/
/** @addtogroup USB_OTG_DRIVER
  * @{
  */

/** @defgroup USB_DCD_INT
  * @brief This file is the
  * @{
  */

/** @defgroup USB_DCD_INT_Exported_Defines
  * @{
  */

typedef struct _USBD_DCD_INT
{
    uint8_t (* DataOutStage)(USB_OTG_CORE_HANDLE *pdev, uint8_t epnum);
    uint8_t (* DataInStage)(USB_OTG_CORE_HANDLE *pdev, uint8_t epnum);
    uint8_t (* SetupStage)(USB_OTG_CORE_HANDLE *pdev);
    uint8_t (* SOF)(USB_OTG_CORE_HANDLE *pdev);
    uint8_t (* Reset)(USB_OTG_CORE_HANDLE *pdev);
    uint8_t (* Suspend)(USB_OTG_CORE_HANDLE *pdev);
    uint8_t (* Resume)(USB_OTG_CORE_HANDLE *pdev);
    uint8_t (* IsoINIncomplete)(USB_OTG_CORE_HANDLE *pdev);
    uint8_t (* IsoOUTIncomplete)(USB_OTG_CORE_HANDLE *pdev);

    uint8_t (* DevConnected)(USB_OTG_CORE_HANDLE *pdev);
    uint8_t (* DevDisconnected)(USB_OTG_CORE_HANDLE *pdev);

} USBD_DCD_INT_cb_TypeDef;

extern USBD_DCD_INT_cb_TypeDef *USBD_DCD_INT_fops;

/** @defgroup USB_DCD_INT_Exported_Types
  * @{
  */

/** @defgroup USB_DCD_INT_Exported_Macros
  * @{
  */
//#define CLEAR_IN_EP_INTR(epnum,intr)
#define CLEAR_IN_EP_INTR(epnum,intr) \
  txcsrl.d8=0; \
  txcsrl.b.intr = 0; \
  USBHWRITE(&pdev->regs.CSRREGS[epnum]->TXCSRL,txcsrl.d8);

#define CLEAR_OUT_EP_INTR(epnum,intr) \
  rxcsrl.d8=0; \
  rxcsrl.b.intr = 0; \
  USBHWRITE(&pdev->regs.CSRREGS[epnum]->RXCSRL,rxcsrl.d8);

/** @defgroup USB_DCD_INT_Exported_Variables
  * @{
  */

/** @defgroup USB_DCD_INT_Exported_FunctionsPrototype
  * @{
  */
uint32_t USBD_OTG_ISR_Handler(USB_OTG_CORE_HANDLE *pdev);

//uint32_t USBD_OTG_DMA_ISR_Handler (USB_OTG_CORE_HANDLE *pdev);
uint32_t USBD_OTG_DMA_ISR_Handler(USB_OTG_CORE_HANDLE *pdev, uint8_t dma_intr_value);

void _delay_(uint32_t t);

byte usb_getbyte(void);
word usb_getword(void);

#ifdef __cplusplus
}
#endif

#endif  /* __USB_DCD_INT_H__ */

