  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_BT_H_
#define __CM_BT_H_

#include "cm_type.h"
#include "cm_ipc.h"
#include "cm1610.h"
#include "cm_drv_common.h"

#define ADV_MAX_LEN 32
#define DATA_MAX_LEN 255

#define DELETE_SERVICE          0x01
#define ADD_SERVICE             0x02
#define ADD_CHARACTERISTIC      0x03

typedef enum
{
    PAIR_NONE             =       0x00,
    PAIR_JUST_WORK        =       0x01,
    PAIR_PASSKEY          =       0x02
}PAIR_TypeDef;

union GET_BT_STATE
{
    uint8_t  all;
    struct
    {
        uint8_t  bt_candiscover             : 1;  // BT 3.0 can be discover
        uint8_t  bt_canconnect              : 1;  // BT 3.0 can be connect
        uint8_t  ble_can_discover_connect   : 1;  // BT 4.0 can be discover and discover
        uint8_t  bt_connectstate            : 1;  // BT 3.0 connect
        uint8_t  ble_connectstate           : 1;  // BT 4.0 connect
        uint8_t  usned                      : 3;
    } bit;
};

/**
 * @brief  send ble control cmd
 *
 * @param  cmd : ble cmd
 *
 * @retval none
 */
void BT_SendBleCmd(uint8_t cmd);

/**
 * @brief  set ble adv package data
 *
 * @param  *advdata : pointer to adv data.
 *  			 DataLen: data length(must be 0x1f)
 *
 * @retval none
 */
void BT_SetBleAdv(uint8_t *advdata, uint8_t DataLen);

/**
 * @brief  set ble scan package data
 *
 * @param  scan_data: pointer to  scan data.
 *         DataLen: data length(length < 0x20)
 * @retval none
 */
void BT_SetBleScanData(uint8_t* scan_data, int DataLen);

/**
 * @brief   Set the address of the bluetooth 3.0 device
 *
 * @param  *bt_addr : pointer to  address(6 bytes)
 *
 * @retval none
 */
void BT_SetSppAddr(uint8_t *bt_addr);

/**
 * @brief   Set the address of the bluetooth 4.0 device
 *
 * @param  *bt_addr : pointer to  address(6 bytes)
 *
 * @retval none
 */
void BT_SetBleAddr(uint8_t * bt_addr);

/**
  * @brief   Set the name of the bluetooth 3.0 device
  * @param  spp_name: pointer to  address information.
  *         name_len: name length, less 16 byte,not include '\0'
  * @retval none
  */
void BT_SetSppName(uint8_t * spp_name, uint16_t name_len);

/**
  * @brief   Set the name of the bluetooth 4.0 device
  * @param  ble_name: pointer to  name information.
  *         name_len: name length,less 16 byte, not include '\0'
  * @retval none
  */
void BT_SetBleName(uint8_t* ble_name, uint16_t name_len);

/**
 * @brief  Ble send data
  * @param  ble_data: pointer to  ble data ,includes two bytes handle(byte0-byte1 is ble handle).
  *         len: data length(max len is 255)
 * @retval none
 */
void BT_SendBleData(uint8_t * ble_data, uint16_t DataLen);

/**
  * @brief  Set visibility mode.
  * @param  bt_discoverable: 0--bt_discoverable OFF;1--bt_discoverable ON.
  *         ble_discoverable:0--ble_discoverable OFF;1--ble_discoverable ON.
  * @retval none
  */
void BT_SetVisibility(Boolean bt_discoverable, Boolean ble_discoverable);

/**
  * @brief  Ble delete service
  * @param  none
  * @retval none
  */
void BT_DeleteBleService(void);

/**
  * @brief  add ble custom Service
  * @param  ble_service_uuid:
	*						byte0: uuid length(2 or 16)
	*						byte1-2(16): uuid
	*					service_uuid_len: sizeof(ble_service_uuid)
  * @retval none
  */
void BT_AddBleService(uint8_t* ble_service_uuid, uint16_t service_uuid_len);

/**
  * @brief  add ble custom Characteristic
  * @param  ble_Characteristic_uuid:
	*								byte0: characterisitic attribute
	*								bit0		Broadcast
	*								bit1		Read
	*								bit2		Write without Response
	*								bit3		Write
	*								bit4		Notify
	*								bit5		Indicate
	*								bit6		Authentication Signed Write
	*								bit7		Extended Properties
	*								byte1:			characterisitic uuid length(2 or 16)
	*								byte2-3(17):characterisitic uuid
	*								byte4(18):	write/read payload length(1--20;default:1)
	*								byte5(19)-x:write/read payload(default:00)
	*					service_Characteristic_payload_len: sizeof(ble_Characteristic_uuid)
  * @retval 0:failed
  *         other:Characteristic handle
  */
uint16_t BT_AddBleCharacteristic(uint8_t* ble_Characteristic_uuid, uint16_t service_Characteristic_payload_len);

/**
  * @brief  get bt state
  * @param  none
  * @retval uint8_t
    bit0:BT 3.0 Can be discover
    bit1:BT 3.0 Can be connect
    bit2:BT 4.0 Can be discover and connect
    bit4:BT 3.0 connected
    bit5:BT 4.0 connected
  */
uint8_t BT_GetBtStatus(void);
/**
  * @brief  disconnect Ble
  * @param  none
  * @retval none
  */
void BT_BleDisconnect(void);

/**
  * @brief  ble send update ble connect param request(must be connected ble before call this function)
  * @param  data:
  *             byte0-byte1:min connect interval[6~3200],minConnectionInterval=(byte0+(byte1<<8))*1.25ms
  *             byte2-byte3:max connect interval[6~3200],maxConnectionInterval=(byte2+(byte3<<8))*1.25ms
  *             byte4-byte5:slaveLatency[0 ~ 499]
  *             byte6-byte7:Connection Supervision Timeout[10 ~ 3200],SupervisionTimeout=(byte6+(byte7<<8))*10ms
  *
  *             SupervisionTimeout >(1+slaveLatency)*(maxConnectionInterval)
  *
  *         len:len must be 8 bytes
 * @retval  none
  */
void BT_BleConnectParmUpdateReq(uint8_t *data, int len);

/**
 * @brief  Ble update mtu request
 * @param  newMtuNum: update  mtu num, data range <23 - 517>
 * @retval none
 */
void BT_BleUpdateMtuReq(uint16_t newMtuNum);

/**
 * @brief  Set Ble pair mode
 * @param  mode: enum PAIR_TypeDef
 *    PAIR_NONE         none pair
      PAIR_JUST_WORK    just work
      PAIR_PASSKEY      passkey
 * @retval none
 */
void BT_SetBleParing(PAIR_TypeDef mode);

/**
 * @brief  read NVRAM data
 * @param  nvdata: read to buf pointer
 *         len: read nvram data len ,len must NVRAM_LEN
 * @retval none
 */
void BT_ReadNVRAM(uint8_t* nvdata, uint8_t len);

/**
 * @brief  Set NVRAM data
 * @param  NvData: pointer to  set nvram
 *         len: data len
 * @retval none
 */
void BT_SetNVRAM(uint8_t * NvData, int len);

/**
 * @brief  Get Bt Version
 * @param  none
 * @retval bt version num
 */
uint16_t BT_GetVersion(void);

#endif

