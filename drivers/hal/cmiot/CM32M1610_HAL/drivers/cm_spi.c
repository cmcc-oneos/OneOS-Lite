  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#include "cm_spi.h"

void SPI_Init(SPI_InitTypeDef* SPI_InitStruct)
{
    register uint16_t regspictrl = 0;
    register uint16_t regspidelay = 0;

    _ASSERT(IS_SPI_Clk_Div(SPI_InitStruct->BaudRatePrescaler));
    _ASSERT(IS_SPI_CPOL(SPI_InitStruct->CPOL));
    _ASSERT(IS_SPI_CPHA(SPI_InitStruct->CPHA));
    _ASSERT(IS_SPI_RW_Delay(SPI_InitStruct->RW_Delay));

    regspictrl = SPI_InitStruct->BaudRatePrescaler| \
               SPI_InitStruct->CPOL | \
               SPI_InitStruct->CPHA | \
               SPI_AUTO_INCR_ADDR;

    regspidelay = SPI_InitStruct->RW_Delay| \
                                SPI_64K_MRAM;

    HWRITE((uint32_t)CORE_SPID_CTRL, regspictrl);
    HWRITE((uint32_t)CORE_SPID_DELAY, regspidelay);
}

void SPI_DeInit(void)
{
    HWRITEL(CORE_SPID_CTRL, 0x00);
    HWRITEL(CORE_SPID_DELAY, 0x00);
    HWRITEL(CORE_SPID_TXADDR, 0x00);
    HWRITEL(CORE_SPID_TXLEN, 0x00);
    HWRITEL(CORE_SPID_RXADDR, 0x00);
    HWRITEL(CORE_SPID_RXLEN, 0x00);
}

void SPI_SendData( uint8_t data)
{
    volatile unsigned char pdata;
  pdata = data;

    HWRITEW(CORE_SPID_TXADDR ,(int)(&pdata) );
    HWRITEW(CORE_SPID_TXLEN , 1);
    HWRITEW(CORE_SPID_RXADDR , 0);
    HWRITEW(CORE_SPID_RXLEN , 0);
    HWRITE(CORE_DMA_START , 2);

    while(!(HREAD(CORE_DMA_STATUS) & 0x40) );
}

void  SPI_SendBuff(uint8_t *buff, int len)
{
    HWRITEW(CORE_SPID_TXADDR ,(int)buff);
    HWRITEW(CORE_SPID_TXLEN , len);
    HWRITEW(CORE_SPID_RXADDR , 0);
    HWRITEW(CORE_SPID_RXLEN , 0);
    HWRITE(CORE_DMA_START , 2);

    while(!(HREAD(CORE_DMA_STATUS) & 0x40));
}

void SPI_SendAndReceiveData(uint8_t *TxBuff, uint16_t TxLen, uint8_t *RxBuff, uint16_t RxLen)
{
    HWRITEW(CORE_SPID_TXADDR ,(int)TxBuff );
    HWRITEW(CORE_SPID_TXLEN , TxLen);
    HWRITEW(CORE_SPID_RXADDR , (int)RxBuff);
    HWRITEW(CORE_SPID_RXLEN , RxLen);
    HWRITE(CORE_DMA_START , 2);

    while(!(HREAD(CORE_DMA_STATUS) & 0x40) );
}

