/**
 ******************************************************************************
 * @file    fm15f3xx_ll_bkp.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_BKP_H
#define FM15F3XX_LL_BKP_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


/** @defgroup TamperPin_LL_INIT   BKPTamperPin Exported Init structure
 * @{
 */
typedef struct {
    uint32_t    Active0PinPoly;     /* Tamper pin active0 cfg. */
    uint32_t    Active0ShiftReg;

    uint32_t    Active1PinPoly;     /* Tamper pin active1 cfg. */
    uint32_t    Active1ShiftReg;

    uint32_t    FilterClockSource;  /* Tamper pin glitch filter. */
    uint32_t    FilterWidth;
    uint32_t    FilterEn;

    uint32_t    PinSampleFreq;      /* Tamper pin sample. */
    uint32_t    PinSampleWidth;

    uint32_t    PinPullSelect;      /* Tamper pin pull. */
    uint32_t    PinPullEn;

    uint32_t PinExpectData;         /* Tamper pin expect data. */

    uint32_t PinPolarity;           /* Tamper pin polarity. */

    uint32_t PinDir;                /* Tamper pin direction. */

    uint32_t Pin;                   /* Tamper pin. */

    uint32_t    PinHysterSel;       /* Tamper pin cfg. */
    uint32_t    PinPassfilterEn;
    uint32_t    PinDriveStrength;
    uint32_t    PinSlewRate;

    uint32_t    Pin2SignalOut;      /* Dig Signal out. */
    uint32_t    Pin2SignalSel;
} LL_TamperPin_InitTypeDef;


/**
 * @}
 */


/** @defgroup GPIO_LL_EC_PIN PIN
 * @{
 */


/**
 * @brief  gpio_pin
 */
#define LL_BKP_GPIO_PIN0    (0x00000001U)
#define LL_BKP_GPIO_PIN1    (0x00000002U)
#define LL_BKP_GPIO_PIN2    (0x00000004U)
#define LL_BKP_GPIO_PIN3    (0x00000008U)
#define LL_BKP_GPIO_PIN4    (0x00000010U)
#define LL_BKP_GPIO_PIN5    (0x00000020U)
#define LL_BKP_GPIO_PIN6    (0x00000040U)
#define LL_BKP_GPIO_PIN7    (0x00000080U)
#define LL_BKP_GPIO_ALLPIN  (0x000000FFU)

#define LL_BKP_PIN0     (0x00000001U)
#define LL_BKP_PIN1     (0x00000002U)
#define LL_BKP_PIN2     (0x00000004U)
#define LL_BKP_PIN3     (0x00000008U)
#define LL_BKP_PIN4     (0x00000010U)
#define LL_BKP_PIN5     (0x00000020U)
#define LL_BKP_PIN6     (0x00000040U)
#define LL_BKP_PIN7     (0x00000080U)
#define LL_BKP_ALLPIN   (0x000000FFU)


/**
 * @brief  tamper
 */
#define LL_BKP_TAMPER_0 BKP_TAMPER_EN_PIN0DE
#define LL_BKP_TAMPER_1 BKP_TAMPER_EN_PIN1DE
#define LL_BKP_TAMPER_2 BKP_TAMPER_EN_PIN2DE
#define LL_BKP_TAMPER_3 BKP_TAMPER_EN_PIN3DE
#define LL_BKP_TAMPER_4 BKP_TAMPER_EN_PIN4DE
#define LL_BKP_TAMPER_5 BKP_TAMPER_EN_PIN5DE
#define LL_BKP_TAMPER_6 BKP_TAMPER_EN_PIN6DE
#define LL_BKP_TAMPER_7 BKP_TAMPER_EN_PIN7DE


/**
 * @brief  tamper_pinn_clkin
 */
#define LL_BKP_CLKIN_PIN0   (0x00U)
#define LL_BKP_CLKIN_PIN1   (0x01U)
#define LL_BKP_CLKIN_PIN2   (0x02U)
#define LL_BKP_CLKIN_PIN3   (0x03U)
#define LL_BKP_CLKIN_PIN4   (0x04U)
#define LL_BKP_CLKIN_PIN5   (0x05U)
#define LL_BKP_CLKIN_PIN6   (0x06U)
#define LL_BKP_CLKIN_PIN7   (0x07U)


/**
 * @brief  gpio_pinn en
 */
#define LL_BKP_GPIO_ENABLE  (0x00000001U)
#define LL_BKP_GPIO_DISABLE (0x00000000U)


/**
 * @brief  active_tampern_shift_reg
 */
#define  LL_BKP_ACTIVE0REG_INITVAL  (0x0000FCF5U)
#define  LL_BKP_ACTIVE0REG_DISABLE  (0x00000000U << BKP_ACTIVE_TAMPERN_POLY_Pos)
#define  LL_BKP_ACTIVE0REG_POLY     (0x0000FFF6U << BKP_ACTIVE_TAMPERN_POLY_Pos)


/**
 * @brief  active_tampern_shift_reg
 */
#define  LL_BKP_ACTIVE1REG_INITVAL  (0x0000FCF5U)
#define  LL_BKP_ACTIVE1REG_DISABLE  (0x00000000U << BKP_ACTIVE_TAMPERN_POLY_Pos)
#define  LL_BKP_ACTIVE1REG_POLY     (0x00008016U << BKP_ACTIVE_TAMPERN_POLY_Pos)


/**
 * @brief glitch_filter_clk_srouce
 */
#define  LL_BKP_FILTERCLKSRC_512Hz      (0x00000000U)
#define  LL_BKP_FILTERCLKSRC_32768Hz    BKP_PIN_GLITCH_FILTER_PSC


/**
 * @brief glitch_filter_width
 */
#define  LL_BKP_FILTERWIDTH_0   (0x00000000U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_1   (0x00000001U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_2   (0x00000002U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_3   (0x00000003U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_4   (0x00000004U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_5   (0x00000005U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_6   (0x00000006U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_7   (0x00000007U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_8   (0x00000008U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_9   (0x00000009U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_10  (0x0000000aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_11  (0x0000000bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_12  (0x0000000cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_13  (0x0000000dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_14  (0x0000000eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_15  (0x0000000fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_16  (0x00000010U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_17  (0x00000011U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_18  (0x00000012U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_19  (0x00000013U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_20  (0x00000014U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_21  (0x00000015U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_22  (0x00000016U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_23  (0x00000017U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_24  (0x00000018U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_25  (0x00000019U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_26  (0x0000001aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_27  (0x0000001bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_28  (0x0000001cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_29  (0x0000001dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_30  (0x0000001eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_31  (0x0000001fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_32  (0x00000020U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_33  (0x00000021U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_34  (0x00000022U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_35  (0x00000023U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_36  (0x00000024U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_37  (0x00000025U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_38  (0x00000026U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_39  (0x00000027U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_40  (0x00000028U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_41  (0x00000029U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_42  (0x0000002aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_43  (0x0000002bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_44  (0x0000002cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_45  (0x0000002dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_46  (0x0000002eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_47  (0x0000002fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_48  (0x00000030U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_49  (0x00000031U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_50  (0x00000032U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_51  (0x00000033U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_52  (0x00000034U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_53  (0x00000035U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_54  (0x00000036U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_55  (0x00000037U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_56  (0x00000038U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_57  (0x00000039U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_58  (0x0000003aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_59  (0x0000003bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_60  (0x0000003cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_61  (0x0000003dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_62  (0x0000003eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define  LL_BKP_FILTERWIDTH_63  (0x0000003fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)


/**
 * @brief  pinn_glitch_filter_en
 */
#define  LL_BKP_PINFILTER_DISABLE   0x00000000U
#define  LL_BKP_PINFILTER_ENABLE    BKP_PIN_GLITCH_FILTER_EN


/**
 * @brief  pinn_sample_freq
 */
#define LL_BKP_PINSAMPLEFREQ_0  0x00000000U                                             //0-?8?????
#define LL_BKP_PINSAMPLEFREQ_1  (0x00000001U << BKP_PIN_SAMPLE_FREQ_Pos)                //1-?32?????
#define LL_BKP_PINSAMPLEFREQ_2  (0x00000002U << BKP_PIN_SAMPLE_FREQ_Pos)                //2-?128?????
#define LL_BKP_PINSAMPLEFREQ_3  (0x00000003U << BKP_PIN_SAMPLE_FREQ_Pos)                //3-?512?????


/**
 * @brief  pinn_sample_width
 */
#define LL_BKP_PINSAMPLEWIDTH_0 0x00000000U
#define LL_BKP_PINSAMPLEWIDTH_1 (0x00000001U << BKP_PIN_SAMPLE_WIDTH_Pos)
#define LL_BKP_PINSAMPLEWIDTH_2 (0x00000002U << BKP_PIN_SAMPLE_WIDTH_Pos)
#define LL_BKP_PINSAMPLEWIDTH_3 (0x00000003U << BKP_PIN_SAMPLE_WIDTH_Pos)


/**
 * @brief  pinn_pull_select
 */
#define LL_BKP_PINPULLSEL_PULLDOWN  0x00000000U
#define LL_BKP_PINPULLSEL_PULLUP    (0x00000001U << BKP_PIN_PULL_SELECT_Pos)


/**
 * @brief  pinn_pull_enable
 */
#define LL_BKP_PINPULL_DISABLE  0x00000000U
#define LL_BKP_PINPULL_ENABLE   (0x00000001U << BKP_PIN_PULL_ENABLE_Pos)


/**
 * @brief pinn_expect_data
 */
#define LL_BKP_PINEXPECTDATA_LOW        (0x00000000U << BKP_PIN_EXPECT_DATA_Pos)
#define LL_BKP_PINEXPECTDATA_ACTIVE0    (0x00000001U << BKP_PIN_EXPECT_DATA_Pos)
#define LL_BKP_PINEXPECTDATA_ACTIVE1    (0x00000002U << BKP_PIN_EXPECT_DATA_Pos)
#define LL_BKP_PINEXPECTDATA_A0XORA1    (0x00000003U << BKP_PIN_EXPECT_DATA_Pos)


/**
 * @brief pin_pol
 */
#define LL_BKP_PIN0POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN0POLARITY_NEGATIVE    (0x00000001U)

#define LL_BKP_PIN1POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN1POLARITY_NEGATIVE    (0x00000002U)

#define LL_BKP_PIN2POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN2POLARITY_NEGATIVE    (0x00000004U)

#define LL_BKP_PIN3POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN3POLARITY_NEGATIVE    (0x00000008U)

#define LL_BKP_PIN4POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN4POLARITY_NEGATIVE    (0x00000010U)

#define LL_BKP_PIN5POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN5POLARITY_NEGATIVE    (0x00000020U)

#define LL_BKP_PIN6POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN6POLARITY_NEGATIVE    (0x00000040U)

#define LL_BKP_PIN7POLARITY_POSITIVE    0x00000000U
#define LL_BKP_PIN7POLARITY_NEGATIVE    (0x00000080U)


/**
 * @brief pin_direction
 */
#define LL_BKP_PIN0DIR_INPUT    0x00000000U
#define LL_BKP_PIN0DIR_OUTPUT   (0x00000001U)

#define LL_BKP_PIN1DIR_INPUT    0x00000000U
#define LL_BKP_PIN1DIR_OUTPUT   (0x00000002U)

#define LL_BKP_PIN2DIR_INPUT    0x00000000U
#define LL_BKP_PIN2DIR_OUTPUT   (0x00000004U)

#define LL_BKP_PIN3DIR_INPUT    0x00000000U
#define LL_BKP_PIN3DIR_OUTPUT   (0x00000008U)

#define LL_BKP_PIN4DIR_INPUT    0x00000000U
#define LL_BKP_PIN4DIR_OUTPUT   (0x00000010U)

#define LL_BKP_PIN5DIR_INPUT    0x00000000U
#define LL_BKP_PIN5DIR_OUTPUT   (0x00000020U)

#define LL_BKP_PIN6DIR_INPUT    0x00000000U
#define LL_BKP_PIN6DIR_OUTPUT   (0x00000040U)

#define LL_BKP_PIN7DIR_INPUT    0x00000000U
#define LL_BKP_PIN7DIR_OUTPUT   (0x00000080U)

#define LL_BKP_PINALLINPUT 0x00000000U


/**
 * @brief pin_hysteresis_select
 */
#define LL_BKP_PINHYSTER_LEVEL0 0x00000000U         //0- 305mv~440mv
#define LL_BKP_PINHYSTER_LEVEL1 (0x00000001U)       //1- 490mv~705mv


/**
 * @brief pin_passive_filter_en
 */
#define LL_BKP_PINPASSFILTER_DISABLE    (0x00000000U << BKP_PIN_PASSIVE_FILTEN_Pos)
#define LL_BKP_PINPASSFILTER_ENABLE     (0x00000001U << BKP_PIN_PASSIVE_FILTEN_Pos)


/**
 * @brief pin_drive_strength_en
 */
#define LL_BKP_PINDRISTRENGTH_LOW   (0x00000000U << BKP_PIN_DRI_STRENGTHEN_Pos)
#define LL_BKP_PINDRISTRENGTH_HIGH  (0x00000001U << BKP_PIN_DRI_STRENGTHEN_Pos)


/**
 * @brief pin_slew_rate
 */
#define LL_BKP_PINSLEW_SLOW (0x00000000U << BKP_PIN_SLEW_RATE_Pos)
#define LL_BKP_PINSLEW_FAST (0x00000001U << BKP_PIN_SLEW_RATE_Pos)


/**
 * @brief  dig_signal_out_en
 */
#define LL_BKP_PIN2SIGNALOUT_DISABLE    (0x00000000U << BKP_DIG_SIGNAL_OUT_EN_Pos)
#define LL_BKP_PIN2SIGNALOUT_ENABLE     (0x00000001U << BKP_DIG_SIGNAL_OUT_EN_Pos)


/**
 * @brief pinn_pull_select
 */
#define LL_BKP_PIN2SIGNALSEL_TEMPH  (0x00000000U << BKP_DIG_SIGNAL_SEL_Pos)
#define LL_BKP_PIN2SIGNALSEL_TEMPL  (0x00000001U << BKP_DIG_SIGNAL_SEL_Pos)
#define LL_BKP_PIN2SIGNALSEL_VOLH   (0x00000002U << BKP_DIG_SIGNAL_SEL_Pos)
#define LL_BKP_PIN2SIGNALSEL_VOLL   (0x00000003U << BKP_DIG_SIGNAL_SEL_Pos)
#define LL_BKP_PIN2SIGNALSEL_FDETH  (0x00000004U << BKP_DIG_SIGNAL_SEL_Pos)
#define LL_BKP_PIN2SIGNALSEL_FDETL  (0x00000005U << BKP_DIG_SIGNAL_SEL_Pos)
#define LL_BKP_PIN2SIGNALSEL_FDETS  (0x00000006U << BKP_DIG_SIGNAL_SEL_Pos)
#define LL_BKP_PIN2SIGNALSEL_RFU    (0x00000007U << BKP_DIG_SIGNAL_SEL_Pos)


#define LL_BKP_ACTIVELFSR0_TAP0 (0x0 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR0_TAP1 (0x1 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR0_TAP2 (0x2 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR0_TAP3 (0x3 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR0_TAP4 (0x4 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR0_TAP5 (0x5 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR0_TAP6 (0x6 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR0_TAP7 (0x7 << BKP_ACTIVE_LFSR0TAPSEL_Pos)

#define LL_BKP_ACTIVELFSR1_TAP0 (0x0 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR1_TAP1 (0x1 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR1_TAP2 (0x2 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR1_TAP3 (0x3 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR1_TAP4 (0x4 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR1_TAP5 (0x5 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR1_TAP6 (0x6 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define LL_BKP_ACTIVELFSR1_TAP7 (0x7 << BKP_ACTIVE_LFSR1TAPSEL_Pos)


/**
 * @brief  tamper_pass_cfg
 */
#define LL_BKP_TAMPERFLAG_PASS_ALL      (0x1FFFF)
#define LL_BKP_TAMPERFLAG_PASS_DEFAULT  (0x0FF)


/**
 * @brief  tamper status reg
 */
#define LL_BKP_FLAG_TAMPER          (0x00000001U)
#define LL_BKP_FLAG_TAMPER_ACK      (0x00000002U)
#define LL_BKP_FLAG_FREQ_TAMPER     (0x00000004U)
#define LL_BKP_FLAG_VOL_TAMPER      (0x00000008U)
#define LL_BKP_FLAG_TEMP_TAMPER     (0x00000010U)
#define LL_BKP_FLAG_GLITCH_TAMPER   (0x00000020U)
#define LL_BKP_FLAG_TAMPER_0        (0x00000040U)
#define LL_BKP_FLAG_TAMPER_1        (0x00000080U)
#define LL_BKP_FLAG_TAMPER_2        (0x00000100U)
#define LL_BKP_FLAG_TAMPER_3        (0x00000200U)
#define LL_BKP_FLAG_TAMPER_4        (0x00000400U)
#define LL_BKP_FLAG_TAMPER_5        (0x00000800U)
#define LL_BKP_FLAG_TAMPER_6        (0x00001000U)
#define LL_BKP_FLAG_TAMPER_7        (0x00002000U)
#define LL_BKP_FLAG_SHIELD_TAMPER   (0x00004000U)
#define LL_BKP_FLAG_STFREQ_TAMPER   (0x00008000U)
#define LL_BKP_FLAG_STVOL_TAMPER    (0x00010000U)
#define LL_BKP_FLAG_STTEMP_TAMPER   (0x00020000U)
#define LL_BKP_FLAG_STGLITCH_TAMPER (0x00040000U)
#define LL_BKP_FLAG_RTC_STOP_FDET   (0x00100000U)
#define LL_BKP_FLAG_RTC_LOW_FDET    (0x00200000U)
#define LL_BKP_FLAG_RTC_HIGH_FDET   (0x00400000U)
#define LL_BKP_FLAG_RTC_LOW_VOL     (0x00800000U)
#define LL_BKP_FLAG_RTC_HIGH_VOL    (0x01000000U)
#define LL_BKP_FLAG_RTC_LOW_TEMP    (0x02000000U)
#define LL_BKP_FLAG_RTC_HIGH_TEMP   (0x04000000U)
#define LL_BKP_FLAG_All             (0x0007FFFFU)


/**
 * @brief  tamper int
 */
#define LL_BKP_INT_TAMPER           (0x0001U)
#define LL_BKP_INT_FREQ_TAMPER      (0x0002U)
#define LL_BKP_INT_VOL_TAMPER       (0x0004U)
#define LL_BKP_INT_TEMP_TAMPER      (0x0008U)
#define LL_BKP_INT_GLITCH_TAMPER    (0x0010U)
#define LL_BKP_INT_TAMPER_0         (0x0020U)
#define LL_BKP_INT_TAMPER_1         (0x0040U)
#define LL_BKP_INT_TAMPER_2         (0x0080U)
#define LL_BKP_INT_TAMPER_3         (0x0100U)
#define LL_BKP_INT_TAMPER_4         (0x0200U)
#define LL_BKP_INT_TAMPER_5         (0x0400U)
#define LL_BKP_INT_TAMPER_6         (0x0800U)
#define LL_BKP_INT_TAMPER_7         (0x1000U)
#define LL_BKP_INT_SHIELD_TAMPER    (0x2000U)
#define LL_BKP_INT_STFREQ_TAMPER    (0x4000U)
#define LL_BKP_INT_STVOL_TAMPER     (0x8000U)
#define LL_BKP_INT_STTEMP_TAMPER    (0x1000U)
#define LL_BKP_INT_STGLITCH_TAMPER  (0x2000U)
#define LL_BKP_INT_ALL              (0x3FFFU)


#define LL_BKP_PASSFLAG_FREQTAMPER      (0x00001U)
#define LL_BKP_PASSFLAG_VOLTAMPER       (0x00002U)
#define LL_BKP_PASSFLAG_TEMPTAMPER      (0x00004U)
#define LL_BKP_PASSFLAG_GLIHTAMPER      (0x00008U)
#define LL_BKP_PASSFLAG_TAMPER_0        (0x00010U)
#define LL_BKP_PASSFLAG_TAMPER_1        (0x00020U)
#define LL_BKP_PASSFLAG_TAMPER_2        (0x00040U)
#define LL_BKP_PASSFLAG_TAMPER_3        (0x00080U)
#define LL_BKP_PASSFLAG_TAMPER_4        (0x00100U)
#define LL_BKP_PASSFLAG_TAMPER_5        (0x00200U)
#define LL_BKP_PASSFLAG_TAMPER_6        (0x00400U)
#define LL_BKP_PASSFLAG_TAMPER_7        (0x00800U)
#define LL_BKP_PASSFLAG_STFREQTAMPER    (0x01000U)
#define LL_BKP_PASSFLAG_STVOLTAMPER     (0x02000U)
#define LL_BKP_PASSFLAG_STTEMPTAMPER    (0x04000U)
#define LL_BKP_PASSFLAG_STGLIHTAMPER    (0x08000U)
#define LL_BKP_PASSFLAG_STSTOPTAMPER    (0x10000U)

/**********************************************BKP Vmain*******************************************************/


/**
 * @brief  Enable Commuincation Vmain to Vbat
 * @param  BKP_VMAIN_BASEx pointer to BKP_VMAIN
 * @retval None
 */
__STATIC_INLINE void LL_BKP_EnableVmain2Vbat(BKP_VMAIN_TypeDef *BKP_VMAIN_BASEx)
{
    SET_BIT(BKP_VMAIN_BASEx->ISOC, BKP_VMAIN_ISOC_BISON_Msk | BKP_VMAIN_ISOC_VMISOO_Msk);
}


/**
 * @brief  Disable Commuincation Vmain to Vbat
 * @param  BKP_VMAIN_BASEx pointer to BKP_VMAIN
 * @retval None
 */
__STATIC_INLINE void LL_BKP_DisableVmain2Vbat(BKP_VMAIN_TypeDef *BKP_VMAIN_BASEx)
{
    CLEAR_BIT(BKP_VMAIN_BASEx->ISOC, BKP_VMAIN_ISOC_BISON_Msk | BKP_VMAIN_ISOC_VMISOO_Msk);
}


/**
 * @brief  Return if the Bat connect ready
 * @param  BKP_VMAIN_BASEx pointer to BKP_VMAIN
 * @retval Returned value can be one of the following values:
 *         @arg @ref (1 or 0)
 */
__STATIC_INLINE uint32_t LL_BKP_IsReady(BKP_VMAIN_TypeDef *BKP_VMAIN_BASEx)
{
    return (READ_BIT(BKP_VMAIN_BASEx->ISOC, BKP_VMAIN_ISOC_BCRDY_Msk) == BKP_VMAIN_ISOC_BCRDY);
}


/**********************************************BKP  TAMPER *****************************************************/


/**
 * @brief  Enable Tamper Detect
 * @param  TamperDetect This parameter can be one of the following values:
 *         @arg @ref LL_BKP_TAMPER_0
 *         @arg @ref LL_BKP_TAMPER_1
 *         @arg @ref LL_BKP_TAMPER_2
 *         @arg @ref LL_BKP_TAMPER_3
 *         @arg @ref LL_BKP_TAMPER_4
 *         @arg @ref LL_BKP_TAMPER_5
 *         @arg @ref LL_BKP_TAMPER_6
 *         @arg @ref LL_BKP_TAMPER_7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_EnalbeTamperDetect(uint32_t TamperDetect)
{
    SET_BIT(BKP->EN, TamperDetect);
}


/**
 * @brief  Disable Tamper Detect
 * @param  TamperDetect This parameter can be one of the following values:
 *         @arg @ref LL_BKP_TAMPER_0
 *         @arg @ref LL_BKP_TAMPER_1
 *         @arg @ref LL_BKP_TAMPER_2
 *         @arg @ref LL_BKP_TAMPER_3
 *         @arg @ref LL_BKP_TAMPER_4
 *         @arg @ref LL_BKP_TAMPER_5
 *         @arg @ref LL_BKP_TAMPER_6
 *         @arg @ref LL_BKP_TAMPER_7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_DisableTamperDetect(uint32_t TamperDetect)
{
    CLEAR_BIT(BKP->EN, TamperDetect);
}


/**
 * @brief  Disable All Tamper Detect
 * @retval None
 */
__STATIC_INLINE void LL_BKP_DisableAllTamperDetect(void)
{
    CLEAR_BIT(BKP->EN, 0xffffffD7);
}


/**
 * @brief  Is Tamper Flag Active
 * @param  TamperDetect This parameter can be one of the following values:
 *         @arg @ref LL_BKP_FLAG_TAMPER
 *         @arg @ref LL_BKP_FLAG_TAMPER_ACK
 *         @arg @ref LL_BKP_FLAG_FREQ_TAMPER
 *         @arg @ref LL_BKP_FLAG_VOL_TAMPER
 *         @arg @ref LL_BKP_FLAG_TEMP_TAMPER
 *         @arg @ref LL_BKP_FLAG_GLITCH_TAMPER
 *         @arg @ref LL_BKP_FLAG_TAMPER_0
 *         @arg @ref LL_BKP_FLAG_TAMPER_1
 *         @arg @ref LL_BKP_FLAG_TAMPER_2
 *         @arg @ref LL_BKP_FLAG_TAMPER_3
 *         @arg @ref LL_BKP_FLAG_TAMPER_4
 *         @arg @ref LL_BKP_FLAG_TAMPER_5
 *         @arg @ref LL_BKP_FLAG_TAMPER_6
 *         @arg @ref LL_BKP_FLAG_TAMPER_7
 *         @arg @ref LL_BKP_FLAG_SHIELD_TAMPER
 *         @arg @ref LL_BKP_FLAG_STFREQ_TAMPER
 *         @arg @ref LL_BKP_FLAG_STVOL_TAMPER
 *         @arg @ref LL_BKP_FLAG_STTEMP_TAMPER
 *         @arg @ref LL_BKP_FLAG_STGLITCH_TAMPER
 *         @arg @ref LL_BKP_FLAG_RTC_STOP_FDET
 *         @arg @ref LL_BKP_FLAG_RTC_LOW_FDET
 *         @arg @ref LL_BKP_FLAG_RTC_HIGH_FDET
 *         @arg @ref LL_BKP_FLAG_RTC_LOW_VOL
 *         @arg @ref LL_BKP_FLAG_RTC_HIGH_VOL
 *         @arg @ref LL_BKP_FLAG_RTC_LOW_TEMP
 *         @arg @ref LL_BKP_FLAG_RTC_HIGH_TEMP
 * @retval None
 */
__STATIC_INLINE uint32_t LL_BKP_IsActiveTamperFlag(uint32_t flag)
{
    return (READ_BIT(BKP->STATUS, flag) == flag);
}


/**
 * @brief  Disable Tamper Detect
 * @param  TamperDetect This parameter can be one of the following values:
 *         @arg @ref LL_BKP_FLAG_TAMPER
 *         @arg @ref LL_BKP_FLAG_TAMPER_ACK
 *         @arg @ref LL_BKP_FLAG_FREQ_TAMPER
 *         @arg @ref LL_BKP_FLAG_VOL_TAMPER
 *         @arg @ref LL_BKP_FLAG_TEMP_TAMPER
 *         @arg @ref LL_BKP_FLAG_GLITCH_TAMPER
 *         @arg @ref LL_BKP_FLAG_TAMPER_0
 *         @arg @ref LL_BKP_FLAG_TAMPER_1
 *         @arg @ref LL_BKP_FLAG_TAMPER_2
 *         @arg @ref LL_BKP_FLAG_TAMPER_3
 *         @arg @ref LL_BKP_FLAG_TAMPER_4
 *         @arg @ref LL_BKP_FLAG_TAMPER_5
 *         @arg @ref LL_BKP_FLAG_TAMPER_6
 *         @arg @ref LL_BKP_FLAG_TAMPER_7
 *         @arg @ref LL_BKP_FLAG_SHIELD_TAMPER
 *         @arg @ref LL_BKP_FLAG_STFREQ_TAMPER
 *         @arg @ref LL_BKP_FLAG_STVOL_TAMPER
 *         @arg @ref LL_BKP_FLAG_STTEMP_TAMPER
 *         @arg @ref LL_BKP_FLAG_STGLITCH_TAMPER
 *         @arg @ref LL_BKP_FLAG_All
 * @retval None
 */
__STATIC_INLINE void LL_BKP_ClearFlag_Tamper(uint32_t ClearTamperStatus)
{
    SET_BIT(BKP->STATUS, ClearTamperStatus);   //write 1 to clear
}


/**
 * @brief  Soft Reset Tamper
 * @retval None
 */
__STATIC_INLINE void LL_BKP_SoftResetTamper(void)
{
    WRITE_REG(BKP->SOFTRST, 0x77887788);
    WRITE_REG(BKP->SOFTRST, 0x95279527);

    WRITE_REG(BKP->SOFTRST, 0x77887788);
    WRITE_REG(BKP->SOFTRST, 0x95279527);
}


/**
 * @brief  Enable Tamper INT
 * @param  TamperDetect This parameter can be one of the following values:
 *         @arg @ref LL_BKP_INT_TAMPER
 *         @arg @ref LL_BKP_INT_FREQ_TAMPER
 *         @arg @ref LL_BKP_INT_VOL_TAMPER
 *         @arg @ref LL_BKP_INT_TEMP_TAMPER
 *         @arg @ref LL_BKP_INT_GLITCH_TAMPER
 *         @arg @ref LL_BKP_INT_TAMPER_0
 *         @arg @ref LL_BKP_INT_TAMPER_1
 *         @arg @ref LL_BKP_INT_TAMPER_2
 *         @arg @ref LL_BKP_INT_TAMPER_3
 *         @arg @ref LL_BKP_INT_TAMPER_4
 *         @arg @ref LL_BKP_INT_TAMPER_5
 *         @arg @ref LL_BKP_INT_TAMPER_6
 *         @arg @ref LL_BKP_INT_TAMPER_7
 *         @arg @ref LL_BKP_INT_SHIELD_TAMPER
 *         @arg @ref LL_BKP_INT_STFREQ_TAMPER
 *         @arg @ref LL_BKP_INT_STVOL_TAMPER
 *         @arg @ref LL_BKP_INT_STTEMP_TAMPER
 *         @arg @ref LL_BKP_INT_STGLITCH_TAMPER
 * @retval None
 */
__STATIC_INLINE void LL_BKP_EnableTamperINT(uint32_t inten)
{
    SET_BIT(BKP->INTEN, inten);
}


/**
 * @brief  Disable Tamper INT
 * @param  TamperDetect This parameter can be one of the following values:
 *         @arg @ref LL_BKP_INT_TAMPER
 *         @arg @ref LL_BKP_INT_FREQ_TAMPER
 *         @arg @ref LL_BKP_INT_VOL_TAMPER
 *         @arg @ref LL_BKP_INT_TEMP_TAMPER
 *         @arg @ref LL_BKP_INT_GLITCH_TAMPER
 *         @arg @ref LL_BKP_INT_TAMPER_0
 *         @arg @ref LL_BKP_INT_TAMPER_1
 *         @arg @ref LL_BKP_INT_TAMPER_2
 *         @arg @ref LL_BKP_INT_TAMPER_3
 *         @arg @ref LL_BKP_INT_TAMPER_4
 *         @arg @ref LL_BKP_INT_TAMPER_5
 *         @arg @ref LL_BKP_INT_TAMPER_6
 *         @arg @ref LL_BKP_INT_TAMPER_7
 *         @arg @ref LL_BKP_INT_SHIELD_TAMPER
 *         @arg @ref LL_BKP_INT_STFREQ_TAMPER
 *         @arg @ref LL_BKP_INT_STVOL_TAMPER
 *         @arg @ref LL_BKP_INT_STTEMP_TAMPER
 *         @arg @ref LL_BKP_INT_STGLITCH_TAMPER
 *         @arg @ref LL_BKP_INT_ALL
 * @retval None
 */
__STATIC_INLINE void LL_BKP_DisableTamperINT(uint32_t inten)
{
    CLEAR_BIT(BKP->INTEN, inten);
}


/**
 * @brief  Open Normal Reg file Auth
 * @retval None
 */
__STATIC_INLINE void LL_BKP_OpenNormalRegfileAuth(void)
{
    SET_BIT(BKP->CTRL, BKP_NORMAL_REGFILE_AUTH_Msk);
}


/**
 * @brief  Close Normal Reg file Auth
 * @retval None
 */
__STATIC_INLINE void LL_BKP_CloseNormalRegfileAuth(void)
{
    CLEAR_BIT(BKP->CTRL, BKP_NORMAL_REGFILE_AUTH_Msk);
}


/**********************************************BKP  GPIO *****************************************************/


/**
 * @brief  Enable Tamper pin as GPIO
 * @rmtoll GPIO_EN        GPIO_EN       LL_BKP_GPIO_Enable
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_Enable(void)
{
    SET_BIT(BKP->GPIOEN, LL_BKP_GPIO_ENABLE);
}


/**
 * @brief  Disable Tamper pin as GPIO
 * @rmtoll GPIO_EN        GPIO_EN       LL_BKP_GPIO_Disable
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_Disable(void)
{
    CLEAR_BIT(BKP->GPIOEN, LL_BKP_GPIO_DISABLE);
}


/**
 * @brief  Get Tamper Pin is configured as GPIO
 * @rmtoll GPIO_EN        GPIO_EN       LL_BKP_GPIO_IsEnable
 * @param  TamperPinx This parameter can be one combination of the following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 * @retval Returned value can be one of the combination following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 */
__STATIC_INLINE uint32_t LL_BKP_GPIO_IsEnable(uint32_t TamperPinx)
{
    return ((uint32_t)(READ_BIT(BKP->GPIOEN, TamperPinx) == TamperPinx));
}


/**
 * @brief  Set Tamper Pin as GPIO output
 * @rmtoll GPIO_DIR        GPIO_DIR       LL_BKP_GPIO_SetPinOutputDir
 * @param  TamperPinx This parameter can be one of the following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_SetPinOutputDir(uint32_t TamperPinx)
{
    SET_BIT(BKP->GPIODIR, TamperPinx);
}


/**
 * @brief  Set Tamper Pin as GPIO input
 * @rmtoll GPIO_DIR        GPIO_DIR       LL_BKP_GPIO_SetPinInputDir
 * @param  TamperPinx This parameter can be one of the following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_SetPinInputDir(uint32_t TamperPinx)
{
    CLEAR_BIT(BKP->GPIODIR, TamperPinx);
}


/**
 * @brief  Set Output Pin Data
 * @rmtoll GPIO_DIR        GPIO_DIR       LL_BKP_GPIO_WriteOutputPort
 * @param  Data=[0:7]
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_WriteOutputPort(uint32_t Data)
{
    WRITE_REG(BKP->GPIODOUT, Data);
}


/**
 * @brief  Set TamperGPIO pins to high level on dedicated gpio port.
 * @rmtoll GPIO_DOUT_SET        GPIO_DOUT_SET       LL_BKP_GPIO_SetPin
 * @param  TamperPinx This parameter can be one of the following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_SetOutputPin(uint32_t TamperPinx)
{
    SET_BIT(BKP->GPIODOS, TamperPinx);
}


/**
 * @brief  Set TamperGPIO pins to low level on dedicated gpio port.
 * @rmtoll GPIO_DOUT_CLR        GPIO_DOUT_CLR       LL_BKP_GPIO_ResetPin
 * @param  TamperPinx This parameter can be one of the following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_ResetOutputPin(uint32_t TamperPinx)
{
    SET_BIT(BKP->GPIODOC, TamperPinx);
}


/**
 * @brief  Toggle data value for several pin of dedicated port.
 * @rmtoll GPIO_DOUT_SET        GPIO_DOUT_SET       LL_BKP_GPIO_TogglePin
 * @param  TamperPinx This parameter can be one of the following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_TogglePin(uint32_t TamperPinx)
{
    SET_BIT(BKP->GPIODOT, TamperPinx);
}


/**
 * @brief  Return if input data level for several pins of dedicated port is high or low.
 * @rmtoll GPIODIN                     LL_BKP_GPIO_ReadInputDataBit
 * @param  PinMask This parameter can be one of the following values:
 *         @arg @ref LL_BKP_GPIO_PIN0
 *         @arg @ref LL_BKP_GPIO_PIN1
 *         @arg @ref LL_BKP_GPIO_PIN2
 *         @arg @ref LL_BKP_GPIO_PIN3
 *         @arg @ref LL_BKP_GPIO_PIN4
 *         @arg @ref LL_BKP_GPIO_PIN5
 *         @arg @ref LL_BKP_GPIO_PIN6
 *         @arg @ref LL_BKP_GPIO_PIN7
 * @retval State of bit.
 *          @arg @ref GPIO_PIN_SET
 *          @arg @ref GPIO_PIN_RESET
 */
__STATIC_INLINE uint32_t LL_BKP_GPIO_ReadInputDataBit(uint32_t PinMask)
{
    if ((BKP->GPIODIN & PinMask) == PinMask)
        return (1);
    else
        return (0);
}


/**
 * @brief  Return if input data level for several pins of dedicated port is high or low.
 * @rmtoll GPIODIN                     LL_BKP_GPIO_ReadInputPort
 * @param  None
 * @retval Input data register value of port
 */
__STATIC_INLINE uint32_t LL_BKP_GPIO_ReadInputPort(void)
{
    return ((uint32_t) READ_REG(BKP->GPIODIN));
}


/**
 * @brief  Select PadCLK Input.
 * @rmtoll BKP->GPIOCLKC        TAMPERPAD_CLKIN_MASK       LL_BKP_GPIO_SetClkIn
 * @param  Pinx This parameter can be one of the following values:
 *         @arg @ref LL_BKP_CLKIN_PIN0
 *         @arg @ref LL_BKP_CLKIN_PIN1
 *         @arg @ref LL_BKP_CLKIN_PIN2
 *         @arg @ref LL_BKP_CLKIN_PIN3
 *         @arg @ref LL_BKP_CLKIN_PIN4
 *         @arg @ref LL_BKP_CLKIN_PIN5
 *         @arg @ref LL_BKP_CLKIN_PIN6
 *         @arg @ref LL_BKP_CLKIN_PIN7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_SetPadClkInSel(uint32_t Pinx)
{
    MODIFY_REG(BKP->GPIOCLKC, BKP_GPIO_CLK_SEL_Msk, Pinx << 4);
}


/**
 * @brief  Enable Pad ClkIn
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_EnablePadClkIn(void)
{
    SET_BIT(BKP->GPIOCLKC, 0x1);
}


/**
 * @brief  Disable Pad ClkIn
 * @retval None
 */
__STATIC_INLINE void LL_BKP_GPIO_DisablePadClkIn(void)
{
    CLEAR_BIT(BKP->GPIOCLKC, 0x1);
}


/**********************************************BKP TAMPER APP  *****************************************************/


/**
 * @brief Set SelfTest Pulse Mode
 * @retval None
 */
__STATIC_INLINE void LL_BKP_SetSelfTestPulseMode(void)
{
    CLEAR_BIT(BKP->APPCFG, BKP_SELFTEST_MODE);
}


/**
 * @brief Set SelfTest Always Mode
 * @retval None
 */
__STATIC_INLINE void LL_BKP_SetSelfTestAlwaysMode(void)
{
    SET_BIT(BKP->APPCFG, BKP_SELFTEST_MODE);
}


/**
 * @brief Disable Glitch Actual Filter
 * @retval None
 */
__STATIC_INLINE void LL_BKP_DisableGlitchActualFilter(void)
{
    CLEAR_BIT(BKP->APPCFG, BKP_GLITCH_ACTUALFILEN);
}


/**
 * @brief Enable Glitch Actual Filter
 * @retval None
 */
__STATIC_INLINE void LL_BKP_EnableGlitchActualFilter(void)
{
    SET_BIT(BKP->APPCFG, BKP_GLITCH_ACTUALFILEN);
}


/**
 * @brief  Set TamperLfsr InitData.
 * @param  InitData=[0:15]
 * @retval None
 */
__STATIC_INLINE void LL_BKP_SetTamperLfsrInitData(uint32_t InitData)
{
    MODIFY_REG(BKP->TAMLFSRDATA, BKP_TAMPER_LFSRINITDA_Msk, InitData);
}


/**
 * @brief  Set Tamper ActiveLfsr0 Tap
 * @param  Tap This parameter can be one of the following values:
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP0
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP1
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP2
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP3
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP4
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP5
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP6
 *         @arg @ref LL_BKP_ACTIVELFSR0_TAP7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_SetTamperActiveLfsr0Tap(uint32_t Tap)
{
    MODIFY_REG(BKP->TAMLFSRDATA, BKP_ACTIVE_LFSR0TAPSEL_Msk, Tap);
}


/**
 * @brief  Set Tamper ActiveLfsr0 Tap
 * @param  Tap This parameter can be one of the following values:
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP0
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP1
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP2
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP3
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP4
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP5
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP6
 *         @arg @ref LL_BKP_ACTIVELFSR1_TAP7
 * @retval None
 */
__STATIC_INLINE void LL_BKP_SetTamperActiveLfsr1Tap(uint32_t Tap)
{
    MODIFY_REG(BKP->TAMLFSRDATA, BKP_ACTIVE_LFSR1TAPSEL_Msk, Tap);
}


/**
 * @brief  Enable Tamper ActiveLfsr0
 * @retval None
 */
__STATIC_INLINE void LL_BKP_EnableTamperActiveLfsr0(void)
{
    SET_BIT(BKP->TAMLFSRDATA, BKP_ACTIVE_LFSR0EN);
}


/**
 * @brief  Disable Tamper ActiveLfsr0
 * @param  InitData=[0:15]
 * @retval None
 */
__STATIC_INLINE void LL_BKP_DisableTamperActiveLfsr0(void)
{
    CLEAR_BIT(BKP->TAMLFSRDATA, BKP_ACTIVE_LFSR0EN);
}


/**
 * @brief  Enable Tamper ActiveLfsr1
 * @retval None
 */
__STATIC_INLINE void LL_BKP_EnableTamperActiveLfsr1(void)
{
    SET_BIT(BKP->TAMLFSRDATA, BKP_ACTIVE_LFSR1EN);
}


/**
 * @brief  Disable Tamper ActiveLfsr1
 * @param  InitData=[0:15]
 * @retval None
 */
__STATIC_INLINE void LL_BKP_DisableTamperActiveLfsr1(void)
{
    CLEAR_BIT(BKP->TAMLFSRDATA, BKP_ACTIVE_LFSR1EN);
}


/**
 * @brief Start Tamper Lfsr Sequence
 * @retval None
 */
__STATIC_INLINE void LL_BKP_StartTamperLfsrSequence(void)
{
    WRITE_REG(BKP->TAMLFSRSEQ, 0x77887788);
    WRITE_REG(BKP->TAMLFSRSEQ, 0x95279527);
}


/**
 * @brief  Open Tamper Pass
 * @param  PassTamperFlag This parameter can be one of the following values:
 *         @arg @ref LL_BKP_PASSFLAG_FREQTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_VOLTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_TEMPTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_GLIHTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_0
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_1
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_2
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_3
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_4
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_5
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_6
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_7
 *         @arg @ref LL_BKP_PASSFLAG_STFREQTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STVOLTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STTEMPTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STGLIHTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STSTOPTAMPER
 * @retval None
 */
__STATIC_INLINE void LL_BKP_OpenTamperPass(uint32_t PassTamperFlag)
{
    SET_BIT(BKP->TAMPASSCFG, PassTamperFlag);
}


/**
 * @brief  Close Tamper Pass
 * @param  PassTamperFlag This parameter can be one of the following values:
 *         @arg @ref LL_BKP_PASSFLAG_FREQTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_VOLTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_TEMPTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_GLIHTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_0
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_1
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_2
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_3
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_4
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_5
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_6
 *         @arg @ref LL_BKP_PASSFLAG_TAMPER_7
 *         @arg @ref LL_BKP_PASSFLAG_STFREQTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STVOLTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STTEMPTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STGLIHTAMPER
 *         @arg @ref LL_BKP_PASSFLAG_STSTOPTAMPER
 * @retval None
 */
__STATIC_INLINE void LL_BKP_CloseTamperPass(uint32_t PassTamperFlag)
{
    CLEAR_BIT(BKP->TAMPASSCFG, PassTamperFlag);
}


/**
 * @brief read KeyVaildReg
 * @retval None
 */
__STATIC_INLINE uint32_t LL_BKP_GetSatus_KeyVaild(void)
{
    return (READ_REG(BKP->VALID));
}


/**
 * @brief clear KeyVaildReg
 * @retval None
 */
__STATIC_INLINE void LL_BKP_ClearStatus_KeyVaild(uint32_t statusmask)
{
    WRITE_REG(BKP->VALID, 0x77887788);
    WRITE_REG(BKP->VALID, 0x95279527);
    SET_BIT(BKP->VALID, statusmask);
}


/**
 * @brief write KeyREGFile
 * @param  data:Write data buff
 * @param  len:data len(<8)
 * @retval None
 */
__STATIC_INLINE void LL_BKP_WriteKeyRegFilex(uint32_t *data, uint8_t len)
{
    if (len > 8)
        len = 8;
    for (uint32_t i = 0; i < len; i++)
        WRITE_REG(BKP->KEYRF[i], data[i]);
}


/**
 * @brief write KeyREGFile[n]
 * @param  data:Write data
 * @param  n:reg index(<8)
 * @retval None
 */
__STATIC_INLINE void LL_BKP_WriteKeyRegFileIndex(uint32_t data, uint8_t n)
{
    if (n > 7)
        n = 7;
    WRITE_REG(BKP->KEYRF[n], data);
}


/**
 * @brief Read KeyREGFile
 * @param  data:Receive data buff
 * @param  len:data len(<8)
 * @retval None
 */
__STATIC_INLINE void LL_BKP_ReadKeyRegFile(uint32_t *data, uint8_t len)
{
    if (len > 8)
        len = 8;
    for (int i = 0; i < len; i++)
        data[i] = READ_REG(BKP->KEYRF[i]);
}


/**
 * @brief Read KeyREGFile[n]
 * @param  n:reg index(<8)
 * @retval reg[n]
 */
__STATIC_INLINE uint32_t LL_BKP_ReadKeyRegFileIndex(uint8_t n)
{
    if (n > 7)
        n = 7;
    return (READ_REG(BKP->KEYRF[n]));
}


/**
 * @brief write NormalREGFile
 * @param  data:Write data buff
 * @param  len:data len(<32)
 * @retval None
 */
__STATIC_INLINE void LL_BKP_WriteNormalRegFilex(uint32_t *data, uint8_t len)
{
    if (len > 32)
        len = 32;
    for (uint32_t i = 0; i < len; i++)
        WRITE_REG(BKP->NORMRF[i], data[i]);
}


/**
 * @brief write NormalREGFile[n]
 * @param  data:Write data
 * @param  n:reg index(<32)
 * @retval None
 */
__STATIC_INLINE void LL_BKP_WriteNormalRegFileIndex(uint32_t data, uint8_t n)
{
    if (n > 31)
        n = 31;
    WRITE_REG(BKP->NORMRF[n], data);
}


/**
 * @brief Read NormalREGFile
 * @param  data:Receive data buff
 * @param  len:data len(<32)
 * @retval None
 */
__STATIC_INLINE void LL_BKP_ReadNormalRegFile(uint32_t *data, uint8_t len)
{
    if (len > 32)
        len = 32;
    for (int i = 0; i < len; i++)
        data[i] = READ_REG(BKP->NORMRF[i]);
}


/**
 * @brief Read NormalREGFileIndex[n]
 * @param  n:reg index(<8)
 * @retval reg[n]
 */
__STATIC_INLINE uint32_t LL_BKP_ReadNormalRegFileIndex(uint8_t n)
{
    if (n > 31)
        n = 31;
    return (READ_REG(BKP->NORMRF[n]));
}


void LL_BKP_Tamper_Init(LL_TamperPin_InitTypeDef *TamperPinInitStruct);


void LL_BKP_Tamper_DeInit(void);


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
