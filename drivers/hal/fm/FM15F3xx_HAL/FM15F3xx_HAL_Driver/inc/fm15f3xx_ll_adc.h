/**
 ******************************************************************************
 * @file    fm15f3xx_ll_adc.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15f3XX_LL_ADC_H
#define __FM15f3XX_LL_ADC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

#if defined (ADC0)


/** @defgroup ADC_LL ADC
 * @{
 */

/* Private macro ------------------------------------------------------------*/
/* Exported typedef --------------------------------------------------------*/


/** @defgroup ADC_LL_ExADCed_Functions ADC ExSPIed Functions
 * @{
 */


/**
 * @brief  ADC Init structures definition
 */
typedef struct {
    ///////////////////////
    uint32_t ADCEN;                     /* Specifies ADC module enable or disable. adc_ctrl reg.
                                            This parameter can be a value of @ref ADC_LL_ADC_EN.*/
    ///////////ref CMU module
    uint32_t WorkClkSel;                /* Specifies refrence select. adc_clk_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_WORKCLK_SEL.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_GetWorkClkSel().*/
    uint32_t WorkClkDiv;                /* Specifies refrence select. adc_clk_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_WorkClkPres.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetWorkClkPres().*/
    uint32_t ClkSel;                    /* Specifies refrence select. adc_clk_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_CLK_SEL.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetClkSel().*/
    ////////////////
    uint32_t VrefSel;                   /* Specifies refrence select. adc_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_VREF_SEL.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetVrefSel().*/
    uint32_t LowPowerMode;              /* Specifies low power mode.  adc_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_LowPowerEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetMode().*/
    uint32_t AutoSleep;                 /* Specifies refrence select. adc_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_VREF_SEL.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetClockPolarity().*/
    uint32_t VcomEn;                    /* Specifies refrence select. adc_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_VREF_SEL.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetClockPolarity().*/
    uint32_t SleepWaitTime;             /* Specifies low power mode.  adc_cfg reg.
                                           This parameter can be a value of @ref ADC_LL_LowPowerEn.
                                           This feature can be modified afterwards using unitary function @ref LL_ADC_SetMode().*/
    ////////////////
    uint32_t WorkMode;                  /* Specifies pointer mode:single,dly,loop,RUF.  adc_work_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_PointerMode.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetTransferBitOrder().*/
    uint32_t OneShot;                   /* Specifies scan loop continiualy or only onetime. adc_work_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanOneShot.
                                            @note The communication clock is derived from the master clock. The slave clock does not need to be set.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetBaudRatePrescaler().*/
    uint32_t SampleTime;                /* Specifies scan mode.  adc_work_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ReadyTime;                 /* Specifies scan mode.  adc_work_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ConvertTime;               /* Specifies scan mode.  adc_work_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t SampleHoldTime;            /* Specifies scan mode.  adc_work_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
} LL_ADC_InitTypeDef;


/**
 * @brief  ADC ChannelInit structures definition
 */
typedef struct {
    //------adc_chx_cfg-------
    uint32_t ChxEn;                     /* Specifies ch0\ch1\ch2\ch3 en.  adc_chx_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxDiff;                   /* Specifies single input or diff input.  adc_chx_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxIn;                     /* Specifies 32 input source[6:2] select.  adc_chx_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxTrigSrc;                /* Specifies select trigger source[11:8] 1/16.  adc_chx_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxTrigInvEn;              /* Enable the trigger inverter.   adc_chx_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_TrigInvEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetNSSMode().*/
    uint32_t ChxTrigEdge;               /* Specifies rising or falling edge trig.  adc_chx_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_TrigEdge.
                                           This feature can be modified afterwards using unitary function @ref LL_ADC_SetNSSMode().*/
    uint32_t ChxTrigDly;                /* Specifies trigger delay[23:16]; adc_chx_cfg reg.
                                            This parameter can be a value of @ref ADC_LL_TrigFilt.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetNSSMode().*/
    //------adc_chx_dr_stage-------
    uint32_t ChxOffsetEn;               /* Specifies ch0\ch1\ch2\ch3 adc converter offset adj en.  adc_chx_dr_stage reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxAverageEn;              /* Specifies convert average function en.  adc_chx_dr_stage reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxAverageMode;            /* Specifies average mode(period/smooth).  adc_chx_dr_stage reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxAverageNum;             /* Specifies average number[7:6].  adc_chx_dr_stage reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxCompareEn;              /* Specifies convert compare function en.  adc_chx_dr_stage reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
    uint32_t ChxCompareMode;            /* Specifies compare[10:9].  adc_chx_dr_stage reg.
                                            This parameter can be a value of @ref ADC_LL_ScanModeEn.
                                            This feature can be modified afterwards using unitary function @ref LL_ADC_SetSSNMode().*/
} LL_ADC_ChannelInitTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup ADC_CHANNEL
 * @{
 */
#define LL_ADC_CHANNEL_0    0x00U
#define LL_ADC_CHANNEL_1    0x01U
#define LL_ADC_CHANNEL_2    0x02U
#define LL_ADC_CHANNEL_3    0x03U


/**
 * @}
 */


/** @defgroup ADC_LL_ADC_EN module enable
 * @{
 */
#define LL_ADC_DISABLE  0x00000000U                                 /* First clock transition is the first data capture edge(default)  */
#define LL_ADC_ENABLE   (ADC_CTRL_ADCEN)                            /* Second clock transition is the first data capture edge */


/**
 * @}
 */

#define LL_ADC_RSTN_0   0X00000000U
#define LL_ADC_RSTN_1   (ADC_CTRL_ADCRSTN)


/** @defgroup ADC_LL_WORKCLK_SEL select
 * @{
 */
#define LL_ADC_WORKCLKSEL_4M    0x00000000U                         /* 4Mhz */
#define LL_ADC_WORKCLKSEL_16M   (0x1U << CMU_MDLCLKB_ADCSEL_Pos)    /* 16Mhz */
#define LL_ADC_WORKCLKSEL_OSC   (0x2U << CMU_MDLCLKB_ADCSEL_Pos)    /* Osc */
#define LL_ADC_WORKCLKSEL_PLL   (0x3U << CMU_MDLCLKB_ADCSEL_Pos)    /* PLL */


/**
 * @}
 */


/** @defgroup ADC_LL_CLK_SEL select
 * @{
 */
#define LL_ADC_CLKSEL_WORKCLKDIV1   (0x0U << ADC_CFG_CLKSEL_Pos)    /* WorkClk Div1 */
#define LL_ADC_CLKSEL_WORKCLKDIV2   (0x1U << ADC_CFG_CLKSEL_Pos)    /* WorkClk Div2 */
#define LL_ADC_CLKSEL_FORCE0        (0x2U << ADC_CFG_CLKSEL_Pos)    /* Force0 */
#define LL_ADC_CLKSEL_FORCE1        (0x3U << ADC_CFG_CLKSEL_Pos)    /* Force1 */
#define LL_ADC_CLKSEL_BUSCLKDIV2    (0x4U << ADC_CFG_CLKSEL_Pos)    /* BusClk Div2 */
#define LL_ADC_CLKSEL_BUSCLKDIV4    (0x5U << ADC_CFG_CLKSEL_Pos)    /* BusClk Div4 */
#define LL_ADC_CLKSEL_BUSCLKDIV6    (0x6U << ADC_CFG_CLKSEL_Pos)    /* BusClk Div6 */
#define LL_ADC_CLKSEL_BUSCLKDIV8    (0x7U << ADC_CFG_CLKSEL_Pos)    /* BusClk Div8 */


/**
 * @}
 */


/** @defgroup ADC_LL_Pres_Cfg
 * @{
 */
#define LL_ADC_WORKCLKDIV_1 0x00000000U                             /* no */
#define LL_ADC_WORKCLKDIV_2 (0x1U << CMU_MDLCLKB_ADCDIV_Pos)        /* 1/2 */
#define LL_ADC_WORKCLKDIV_4 (0x2U << CMU_MDLCLKB_ADCDIV_Pos)        /* 1/4 */
#define LL_ADC_WORKCLKDIV_8 (0x3U << CMU_MDLCLKB_ADCDIV_Pos)        /* 1/8 */


/**
 * @}
 */


/** @defgroup ADC_LL_VREF_SEL refrence select
 * @{
 */
#define LL_ADC_VREFH 0x00000000U                                    /* ADC Vrefrence select VREFH */


/**
 * @}
 */


/** @defgroup ADC_LL_LowPower low power Mode
 * @{
 */
#define LL_ADC_LOWPOWER_DISABLE 0x00000000U                         /* normal mode   */
#define LL_ADC_LOWPOWER_ENABLE  (ADC_CFG_LPM)                       /* low power mode  */


/**
 * @}
 */


/** @defgroup ADC_LL_AutoSleep enable auto sleep mode
 * @{
 */
#define LL_ADC_AUTOSLEEP_DISABLE    0x00000000U                     /* normal mode   */
#define LL_ADC_AUTOSLEEP_ENABLE     (ADC_CFG_ASLEEP)                /* autosleep mode  */


/**
 * @}
 */


/** @defgroup ADC_LL_VcomEn enable auto sleep mode
 * @{
 */
#define LL_ADC_VCOM_DISABLE 0x00000000U                             /* normal mode   */
#define LL_ADC_VCOM_ENABLE  (ADC_CFG_VCOMEN)                        /* 1/2*(vrefh-vrefl) or 1/2*(valth-valtl)  */


/**
 * @}
 */


/** @defgroup ADC_LL_ClkWaitGateEn
 * @{
 */
#define LL_ADC_CLKWAITGATE_DISABLE  0x00000000U                     /* normal mode   */
#define LL_ADC_CLKWAITGATE_ENABLE   (ADC_CFG_CLKWAITGATE)           /* aute gate while "wait"  */


/**
 * @}
 */


/** @defgroup ADC_LL_ClkIdleGateEn
 * @{
 */
#define LL_ADC_CLKIDLEGATE_DISABLE  0x00000000U                     /* normal mode   */
#define LL_ADC_CLKIDLEGATE_ENABLE   (ADC_CFG_CLKIDLEGATE)           /* aute gate while "idle"  */


/**
 * @}
 */


/** @defgroup ADC_LL_ClkOutPor
 * @{
 */
#define LL_ADC_CLKOUT_NORMAL    0x00000000U                         /* normal mode   */
#define LL_ADC_CLKOUT_INVERT    (ADC_CFG_CLKOUTPOR)                 /* aute gate while "idle"  */


/**
 * @}
 */

////ADC_WORK


/** @defgroup ADC_LL_WorkMode
 * @{
 */
#define LL_ADC_WORKMODE_SINGLE  0x00000000U                         /* no */
#define LL_ADC_WORKMODE_POLLING (ADC_WORK_MODE)                     /* only for 366:AABCCCD... */


/**
 * @}
 */


/** @defgroup ADC_LL_WorkSTEN
 * @{
 */
#define LL_ADC_WORKST_DISABLE   0x00000000U                         /* no */
#define LL_ADC_WORKST_ENABLE    (ADC_WORK_SAMPLETIMEEN)             /* only for 366:AABCCCD... */


/**
 * @}
 */


/** @defgroup ADC_LL_OneShot one shou enable
 * @{
 */
#define LL_ADC_ONESHOT_DISABLE  0x00000000U                         /* single convert   */
#define LL_ADC_ONESHOT_ENABLE   (ADC_WORK_ONESHOT)                  /* oneshot mode  */


/**
 * @}
 */


/** @defgroup ADC_LL_DoneChkEn one shou enable
 * @{
 */
#define LL_ADC_DONECHECK_DISABLE    0x00000000U                     /* single convert   */
#define LL_ADC_DONECHECK_ENABLE     (ADC_WORK_DONECHECKEN)          /* oneshot mode  */


/**
 * @}
 */


/** @defgroup ADC_LL_DoneStpEn one shou enable
 * @{
 */
#define LL_ADC_DONESTOP_DISABLE 0x00000000U                         /* single convert   */
#define LL_ADC_DONESTOP_ENABLE  (ADC_WORK_DONESTOPEN)               /* oneshot mode  */


/**
 * @}
 */


/** @defgroup ADC_LL_ConflictConvertEn one shou enable
 * @{
 */
#define LL_ADC_CONFLICTCONVERT_DISABLE  0x00000000U                 /* single convert   */
#define LL_ADC_CONFLICTCONVERT_ENABLE   (ADC_WORK_CONFLICTCONTEN)   /* oneshot mode  */


/**
 * @}
 */


/** @defgroup ADC_LL_ConflictMode one shou enable
 * @{
 */
#define LL_ADC_CONFLICTMODE_NORMAL  0x00000000U                     /* single convert   */
#define LL_ADC_CONFLICTMODE_LOOP    (ADC_WORK_CONFLICTMODE)         /* oneshot mode  */


/**
 * @}
 */


/** @defgroup ADC_LL_DoneSampleMode
 * @{
 */
#define LL_ADC_DONESAMPLEMODE_PULSE     0x00000000U
#define LL_ADC_DONESAMPLEMODE_SYSCLK    (0x1U << ADC_WORK_DONESAMPMODE_Pos)
#define LL_ADC_DONESAMPLEMODE_ADCCLKPOS (0x2U << ADC_WORK_DONESAMPMODE_Pos)
#define LL_ADC_DONESAMPLEMODE_ADCCLKNEG (0x3U << ADC_WORK_DONESAMPMODE_Pos)


/**
 * @}
 */


/** @defgroup ADC_LL_ResultGetMode
 * @{
 */
#define LL_ADC_RESULTGETMODE_0CYCLE 0x00000000U
#define LL_ADC_RESULTGETMODE_2CYCLE (ADC_WORK_RESULTGETMODE)


/**
 * @}
 */


/////ADC_poll_seq


/** @defgroup ADC_LL_Sequence anf Channel
 * @{
 */
#define LL_ADC_POLLSEQUENCE0_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE0_CH1    (0x01U << ADC_POLLSEQ_CH00SEL_Pos)
#define LL_ADC_POLLSEQUENCE0_CH2    (0x02U << ADC_POLLSEQ_CH00SEL_Pos)
#define LL_ADC_POLLSEQUENCE0_CH3    (0x03U << ADC_POLLSEQ_CH00SEL_Pos)
#define LL_ADC_POLLSEQUENCE1_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE1_CH1    (0x01U << ADC_POLLSEQ_CH01SEL_Pos)
#define LL_ADC_POLLSEQUENCE1_CH2    (0x02U << ADC_POLLSEQ_CH01SEL_Pos)
#define LL_ADC_POLLSEQUENCE1_CH3    (0x03U << ADC_POLLSEQ_CH01SEL_Pos)
#define LL_ADC_POLLSEQUENCE2_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE2_CH1    (0x01U << ADC_POLLSEQ_CH02SEL_Pos)
#define LL_ADC_POLLSEQUENCE2_CH2    (0x02U << ADC_POLLSEQ_CH02SEL_Pos)
#define LL_ADC_POLLSEQUENCE2_CH3    (0x03U << ADC_POLLSEQ_CH02SEL_Pos)
#define LL_ADC_POLLSEQUENCE3_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE3_CH1    (0x01U << ADC_POLLSEQ_CH03SEL_Pos)
#define LL_ADC_POLLSEQUENCE3_CH2    (0x02U << ADC_POLLSEQ_CH03SEL_Pos)
#define LL_ADC_POLLSEQUENCE3_CH3    (0x03U << ADC_POLLSEQ_CH03SEL_Pos)
#define LL_ADC_POLLSEQUENCE4_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE4_CH1    (0x01U << ADC_POLLSEQ_CH04SEL_Pos)
#define LL_ADC_POLLSEQUENCE4_CH2    (0x02U << ADC_POLLSEQ_CH04SEL_Pos)
#define LL_ADC_POLLSEQUENCE4_CH3    (0x03U << ADC_POLLSEQ_CH04SEL_Pos)
#define LL_ADC_POLLSEQUENCE5_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE5_CH1    (0x01U << ADC_POLLSEQ_CH05SEL_Pos)
#define LL_ADC_POLLSEQUENCE5_CH2    (0x02U << ADC_POLLSEQ_CH05SEL_Pos)
#define LL_ADC_POLLSEQUENCE5_CH3    (0x03U << ADC_POLLSEQ_CH05SEL_Pos)
#define LL_ADC_POLLSEQUENCE6_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE6_CH1    (0x01U << ADC_POLLSEQ_CH06SEL_Pos)
#define LL_ADC_POLLSEQUENCE6_CH2    (0x02U << ADC_POLLSEQ_CH06SEL_Pos)
#define LL_ADC_POLLSEQUENCE6_CH3    (0x03U << ADC_POLLSEQ_CH06SEL_Pos)
#define LL_ADC_POLLSEQUENCE7_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE7_CH1    (0x01U << ADC_POLLSEQ_CH07SEL_Pos)
#define LL_ADC_POLLSEQUENCE7_CH2    (0x02U << ADC_POLLSEQ_CH07SEL_Pos)
#define LL_ADC_POLLSEQUENCE7_CH3    (0x03U << ADC_POLLSEQ_CH07SEL_Pos)
#define LL_ADC_POLLSEQUENCE8_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE8_CH1    (0x01U << ADC_POLLSEQ_CH08SEL_Pos)
#define LL_ADC_POLLSEQUENCE8_CH2    (0x02U << ADC_POLLSEQ_CH08SEL_Pos)
#define LL_ADC_POLLSEQUENCE8_CH3    (0x03U << ADC_POLLSEQ_CH08SEL_Pos)
#define LL_ADC_POLLSEQUENCE9_CH0    (0x00U)
#define LL_ADC_POLLSEQUENCE9_CH1    (0x01U << ADC_POLLSEQ_CH09SEL_Pos)
#define LL_ADC_POLLSEQUENCE9_CH2    (0x02U << ADC_POLLSEQ_CH09SEL_Pos)
#define LL_ADC_POLLSEQUENCE9_CH3    (0x03U << ADC_POLLSEQ_CH09SEL_Pos)
#define LL_ADC_POLLSEQUENCE10_CH0   (0x00U)
#define LL_ADC_POLLSEQUENCE10_CH1   (0x01U << ADC_POLLSEQ_CH10SEL_Pos)
#define LL_ADC_POLLSEQUENCE10_CH2   (0x02U << ADC_POLLSEQ_CH10SEL_Pos)
#define LL_ADC_POLLSEQUENCE10_CH3   (0x03U << ADC_POLLSEQ_CH10SEL_Pos)
#define LL_ADC_POLLSEQUENCE11_CH0   (0x00U)
#define LL_ADC_POLLSEQUENCE11_CH1   (0x01U << ADC_POLLSEQ_CH11SEL_Pos)
#define LL_ADC_POLLSEQUENCE11_CH2   (0x02U << ADC_POLLSEQ_CH11SEL_Pos)
#define LL_ADC_POLLSEQUENCE11_CH3   (0x03U << ADC_POLLSEQ_CH11SEL_Pos)
#define LL_ADC_POLLSEQUENCE12_CH0   (0x00U)
#define LL_ADC_POLLSEQUENCE12_CH1   (0x01U << ADC_POLLSEQ_CH12SEL_Pos)
#define LL_ADC_POLLSEQUENCE12_CH2   (0x02U << ADC_POLLSEQ_CH12SEL_Pos)
#define LL_ADC_POLLSEQUENCE12_CH3   (0x03U << ADC_POLLSEQ_CH12SEL_Pos)
#define LL_ADC_POLLSEQUENCE13_CH0   (0x00U)
#define LL_ADC_POLLSEQUENCE13_CH1   (0x01U << ADC_POLLSEQ_CH13SEL_Pos)
#define LL_ADC_POLLSEQUENCE13_CH2   (0x02U << ADC_POLLSEQ_CH13SEL_Pos)
#define LL_ADC_POLLSEQUENCE13_CH3   (0x03U << ADC_POLLSEQ_CH13SEL_Pos)
#define LL_ADC_POLLSEQUENCE14_CH0   (0x00U)
#define LL_ADC_POLLSEQUENCE14_CH1   (0x01U << ADC_POLLSEQ_CH14SEL_Pos)
#define LL_ADC_POLLSEQUENCE14_CH2   (0x02U << ADC_POLLSEQ_CH14SEL_Pos)
#define LL_ADC_POLLSEQUENCE14_CH3   (0x03U << ADC_POLLSEQ_CH14SEL_Pos)
#define LL_ADC_POLLSEQUENCE15_CH0   (0x00U)
#define LL_ADC_POLLSEQUENCE15_CH1   (0x01U << ADC_POLLSEQ_CH15SEL_Pos)
#define LL_ADC_POLLSEQUENCE15_CH2   (0x02U << ADC_POLLSEQ_CH15SEL_Pos)
#define LL_ADC_POLLSEQUENCE15_CH3   (0x03U << ADC_POLLSEQ_CH15SEL_Pos)


/**
 * @}
 */


/** @defgroup ADC_LL_Poll Sequence
 * @{
 */
#define LL_ADC_POLLSEQUENCE0    (0x00U)
#define LL_ADC_POLLSEQUENCE1    (0x01U)
#define LL_ADC_POLLSEQUENCE2    (0x02U)
#define LL_ADC_POLLSEQUENCE3    (0x03U)
#define LL_ADC_POLLSEQUENCE4    (0x04U)
#define LL_ADC_POLLSEQUENCE5    (0x05U)
#define LL_ADC_POLLSEQUENCE6    (0x06U)
#define LL_ADC_POLLSEQUENCE7    (0x07U)
#define LL_ADC_POLLSEQUENCE8    (0x08U)
#define LL_ADC_POLLSEQUENCE9    (0x09U)
#define LL_ADC_POLLSEQUENCE10   (0x0AU)
#define LL_ADC_POLLSEQUENCE11   (0x0BU)
#define LL_ADC_POLLSEQUENCE12   (0x0CU)
#define LL_ADC_POLLSEQUENCE13   (0x0DU)
#define LL_ADC_POLLSEQUENCE14   (0x0EU)
#define LL_ADC_POLLSEQUENCE15   (0x0FU)


/**
 * @}
 */


/** @defgroup ADC_LL_Poll channel
 * @{
 */
#define LL_ADC_SEQUENCECHANNEL_0    (0x00U)
#define LL_ADC_SEQUENCECHANNEL_1    (0x01U)
#define LL_ADC_SEQUENCECHANNEL_2    (0x02U)
#define LL_ADC_SEQUENCECHANNEL_3    (0x03U)


/**
 * @}
 */

////ADC_CHx_cfg


/** @defgroup ADC_LL_ChxEn  cha(chb\chc\chd) enable
 * @{
 */
#define LL_ADC_CHx_DISABLE  0x00000000U
#define LL_ADC_CHx_ENABLE   (ADC_CHxCFG_EN)


/**
 * @}
 */


/** @defgroup ADC_LL_ChxDiff
 * @{
 */
#define LL_ADC_CHxDIFF_DISABLE  0x00000000U                             /*    */
#define LL_ADC_CHxDIFF_ENABLE   (ADC_CHxCFG_DIFF)                       /*   */


/**
 * @}
 */


/** @defgroup ADC_LL_ChxIn
 * @{
 */
#define LL_ADC_CHxIN_DP0            0x00000000U                         /*0  p2a_adc_dad_p_0  */
#define LL_ADC_CHxIN_DP1            (0x1U << ADC_CHxCFG_IN_Pos)
#define LL_ADC_CHxIN_DP2            (0x2U << ADC_CHxCFG_IN_Pos)
#define LL_ADC_CHxIN_ADC3           (0x3U << ADC_CHxCFG_IN_Pos)         /*3  adc_se3, ptf[4]  */
#define LL_ADC_CHxIN_ADC4           (0x4U << ADC_CHxCFG_IN_Pos)         /*4  adc_se4, ptf[3]  */
#define LL_ADC_CHxIN_ADC5           (0x5U << ADC_CHxCFG_IN_Pos)         /*5  cmp_in2, ptd[7]  */
#define LL_ADC_CHxIN_ADC6           (0x6U << ADC_CHxCFG_IN_Pos)         /*6  cmp_in3, ptd[6]  */
#define LL_ADC_CHxIN_ADC7           (0x7U << ADC_CHxCFG_IN_Pos)         /*7  adc_se7, ptd[5]  */
#define LL_ADC_CHxIN_ADC8           (0x8U << ADC_CHxCFG_IN_Pos)         /*8  adc_se8, ptd[4]  */
#define LL_ADC_CHxIN_ADC9           (0x9U << ADC_CHxCFG_IN_Pos)         /*9  adc_se9, ptd[3]  */
#define LL_ADC_CHxIN_ADC10          (0xAU << ADC_CHxCFG_IN_Pos)         /*10 adc_se10, ptd[2] */
#define LL_ADC_CHxIN_ADC11          (0xBU << ADC_CHxCFG_IN_Pos)         /*11 adc_se11, ptd[1] */
#define LL_ADC_CHxIN_ADC12          (0xCU << ADC_CHxCFG_IN_Pos)         /*12 adc_se12, ptd[0] */
#define LL_ADC_CHxIN_ADC13          (0xDU << ADC_CHxCFG_IN_Pos)         /*13 adc_se13, ptc[7] */
#define LL_ADC_CHxIN_ADC14          (0xEU << ADC_CHxCFG_IN_Pos)         /*14 adc_se14, ptc[6] */
#define LL_ADC_CHxIN_VCHARGE_IN     (0xFU << ADC_CHxCFG_IN_Pos)         /*15 Vcharge_in_div6  */
#define LL_ADC_CHxIN_VCHARGE_OUT    (0x10U << ADC_CHxCFG_IN_Pos)        /*16 Vcharge_out_div6 */
#define LL_ADC_CHxIN_12BITDAC       (0x12U << ADC_CHxCFG_IN_Pos)        /*18 DAC_OUT(PAD)     */
#define LL_ADC_CHxIN_TEMPERTURE     (0x13U << ADC_CHxCFG_IN_Pos)        /*19 Temper Sensor    */
#define LL_ADC_CHxIN_VREF           (0x14U << ADC_CHxCFG_IN_Pos)        /*20 VREF12       */
#define LL_ADC_CHxIN_VDDBKP         (0x16U << ADC_CHxCFG_IN_Pos)        /*23 Vbattery_div4    */
#define LL_ADC_CHxIN_6BITDAC        (0x18U << ADC_CHxCFG_IN_Pos)        /*25 CMP 6Bits DAC    */


/**
 * @}
 */


/** @defgroup ADC_LL_ChxTrigSourceSel
 * @{
 */
#define LL_ADC_CHxTRIGSRC_PAD           0x00000000U                     /*    */
#define LL_ADC_CHxTRIGSRC_CMP           (0x1U << ADC_CHxCFG_TRGSC_Pos)  /*  */
#define LL_ADC_CHxTRIGSRC_STIMER0       (0x2U << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_STIMER1       (0x3U << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_STIMER2       (0x4U << ADC_CHxCFG_TRGSC_Pos)  /*  */
#define LL_ADC_CHxTRIGSRC_STIMER3       (0x5U << ADC_CHxCFG_TRGSC_Pos)  /*  */
#define LL_ADC_CHxTRIGSRC_GTIMER0       (0x6U << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_GTIMER1       (0x7U << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_ATIMER        (0x8U << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_LPTIMER0      (0x9U << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_LPTIMER1      (0xAU << ADC_CHxCFG_TRGSC_Pos)  /*  */
#define LL_ADC_CHxTRIGSRC_RTC_ALARM     (0xBU << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_TAMPER_ALARM  (0xCU << ADC_CHxCFG_TRGSC_Pos)  /*  */
#define LL_ADC_CHxTRIGSRC_RTC_1S        (0xDU << ADC_CHxCFG_TRGSC_Pos)  /*  */
#define LL_ADC_CHxTRIGSRC_SOFTWARE      (0xEU << ADC_CHxCFG_TRGSC_Pos)  /*   */
#define LL_ADC_CHxTRIGSRC_RFU           (0xFU << ADC_CHxCFG_TRGSC_Pos)  /*   */

/**
 * @}
 */


/** @defgroup ADC_LL_ChxTrigInvEn trigger via inverter
 * @{
 */
#define LL_ADC_CHxTRIGINV_DISABLE   0x00000000U                         /* normal trig  */
#define LL_ADC_CHxTRIGINV_ENABLE    (ADC_CHxCFG_TRGINV)                 /* invert trig  */


/**
 * @}
 */


/** @defgroup ADC_LL_CHxTrigEdge Level/rising or falling edge trig
 * @{
 */
#define LL_ADC_CHxTRIGEDGE_HIGH     0x00000000U                         /* High Level trig  */
#define LL_ADC_CHxTRIGEDGE_RISING   (0x1U << ADC_CHxCFG_TRGEDGE_Pos)    /* Rising Edge trig  */
#define LL_ADC_CHxTRIGEDGE_FALLING  (0x2U << ADC_CHxCFG_TRGEDGE_Pos)    /* Falling Edge trig  */
#define LL_ADC_CHxTRIGEDGE_BOTHEDGE (0x3U << ADC_CHxCFG_TRGEDGE_Pos)    /* rising or falling edge trig  */


/**
 * @}
 */

//ADC_CHxDRS


/** @defgroup ADC_LL_ChxOffsetEn
 * @{
 */
#define LL_ADC_CHxOFFSET_DISABLE    0x00000000U                             /* normal trig  */
#define LL_ADC_CHxOFFSET_ENABLE     (ADC_CHxDRS_OFFEN)                      /* invert trig  */


/**
 * @}
 */


/** @defgroup ADC_LL_ChxAverageEn
 * @{
 */
#define LL_ADC_CHxAVERAGE_DISABLE   0x00000000U                             /* normal trig  */
#define LL_ADC_CHxAVERAGE_ENABLE    (ADC_CHxDRS_AVGEN)                      /* invert trig  */


/**
 * @}
 */


/** @defgroup ADC_LL_ChxAverageNum :
 * @{
 */
#define LL_ADC_CHxAVERAGENUM_2  0x00000000U                                 /*   */
#define LL_ADC_CHxAVERAGENUM_4  (0x1U << ADC_CHxDRS_AVGNUM_Pos)             /*   */
#define LL_ADC_CHxAVERAGENUM_6  (0x2U << ADC_CHxDRS_AVGNUM_Pos)             /*     */
#define LL_ADC_CHxAVERAGENUM_8  (0x3U << ADC_CHxDRS_AVGNUM_Pos)             /*     */


/**
 * @}
 */


/** @defgroup ADC_LL_ChxCompareEn
 * @{
 */
#define LL_ADC_CHxCOMPARE_DISABLE   0x00000000U                             /* normal trig  */
#define LL_ADC_CHxCOMPARE_ENABLE    (ADC_CHxDRS_CMPEN)                      /* invert trig  */


/**
 * @}
 */


/** @defgroup ADC_LL_ChxCompareMode :
 * @{
 */
#define LL_ADC_CHxCOMPAREMODE_LARGER    0x00000000U                         /*单阈值大于CV2          */
#define LL_ADC_CHxCOMPAREMODE_LESS      (0x1U << ADC_CHxDRS_CMPM_Pos)       /*单阈值小于CV1          */
#define LL_ADC_CHxCOMPAREMODE_BETWEEN   (0x2U << ADC_CHxDRS_CMPM_Pos)       /*双阈值之间(>CV1, < CV2)*/
#define LL_ADC_CHxCOMPAREMODE_OUTSIDE   (0x3U << ADC_CHxDRS_CMPM_Pos)       /*双阈值之外(>CV2, < CV1)*/


/**
 * @}
 */


/** @defgroup ADC_LL_INTEN select
 * @{
 */
#define LL_ADC_CH0IE        ADC_INTEN_CH0CONVCO
#define LL_ADC_CH1IE        ADC_INTEN_CH1CONVCO
#define LL_ADC_CH2IE        ADC_INTEN_CH2CONVCO
#define LL_ADC_CH3IE        ADC_INTEN_CH3CONVCO
#define LL_ADC_CH0DMAE      (ADC_INTEN_CH0CONVCO | ADC_INTEN_CH0CODMAS)
#define LL_ADC_CH1DMAE      (ADC_INTEN_CH1CONVCO | ADC_INTEN_CH1CODMAS)
#define LL_ADC_CH2DMAE      (ADC_INTEN_CH2CONVCO | ADC_INTEN_CH2CODMAS)
#define LL_ADC_CH3DMAE      (ADC_INTEN_CH3CONVCO | ADC_INTEN_CH3CODMAS)
#define LL_ADC_CH0OVRUNIE   ADC_INTEN_CH0OVRUN
#define LL_ADC_CH1OVRUNIE   ADC_INTEN_CH1OVRUN
#define LL_ADC_CH2OVRUNIE   ADC_INTEN_CH2OVRUN
#define LL_ADC_CH3OVRUNIE   ADC_INTEN_CH3OVRUN
#define LL_ADC_READYIE      ADC_INTEN_READY
#define LL_ADC_DONEERRIE    ADC_INTEN_DONEERR


/**
 * @}
 */


/** @defgroup ADC_LL_INTEN Flag select
 * @{
 */
#define LL_ADC_FLAG_DONEERR (ADC_SRCL_DONEERR)
#define LL_ADC_FLAG_READY   (ADC_SRCL_READY)
#define LL_ADC_FLAG_EOC0    (ADC_SRCL_CH0CONVCO)
#define LL_ADC_FLAG_EOC1    (ADC_SRCL_CH1CONVCO)
#define LL_ADC_FLAG_EOC2    (ADC_SRCL_CH2CONVCO)
#define LL_ADC_FLAG_EOC3    (ADC_SRCL_CH3CONVCO)
#define LL_ADC_FLAG_CH0OR   (ADC_SRCL_CH0OVRUN)
#define LL_ADC_FLAG_CH1OR   (ADC_SRCL_CH1OVRUN)
#define LL_ADC_FLAG_CH2OR   (ADC_SRCL_CH2OVRUN)
#define LL_ADC_FLAG_CH3OR   (ADC_SRCL_CH3OVRUN)
#define LL_ADC_FLAG_SOC_DP0 (0x00010000U)
#define LL_ADC_FLAG_EOC_DP0 (0x00020000U)
#define LL_ADC_FLAG_SOC_DP1 (0x00040000U)
#define LL_ADC_FLAG_EOC_DP1 (0x00080000U)
#define LL_ADC_FLAG_SOC_DP2 (0x00100000U)
#define LL_ADC_FLAG_EOC_DP2 (0x00200000U)
#define LL_ADC_FLAG_ALL     (0x3F3F0FU)


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup ADC_LL_Functions ADC Functions
 * @{
 */


/** @defgroup ADC_LL_Configuration Configuration
 * @{
 */


/**
 * @brief  enable ADCEN ADC module
 * @rmtoll CTRL          adc_en      LL_ADC_Enable
 * @param  ADC ADC Instance
 * @retval None
 */
__STATIC_INLINE void LL_ADC_Enable(ADC_TypeDef *ADCx)
{
    SET_BIT(ADCx->CTRL, ADC_CTRL_ADCEN | ADC_CTRL_ADCRSTN);
}


/**
 * @brief  Disable ADCEN ADC module
 * @rmtoll CTRL          adc_en      LL_ADC_Disable
 * @param  ADC ADC Instance
 * @retval None
 */
__STATIC_INLINE void LL_ADC_Disable(ADC_TypeDef *ADCx)
{
    CLEAR_BIT(ADCx->CTRL, ADC_CTRL_ADCEN);
}


/**
 * @brief  Set ADCEN ADC module enable
 * @rmtoll CTRL          adc_en      LL_ADC_SetADCEN
 * @param  ADC ADC Instance
 * @param  adc_en This parameter can be one of the following values:
 *         @arg @ref LL_ADC_DISABLE
 *         @arg @ref LL_ADC_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetADCEn(ADC_TypeDef *ADCx, uint32_t adc_en)
{
    MODIFY_REG(ADCx->CTRL, ADC_CTRL_ADCEN, adc_en);
}


/**
 * @brief  Get ADCEN
 * @rmtoll CTRL          adc_en          LL_ADC_IsEnable
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_DISABLE
 *         @arg @ref LL_ADC_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnable(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->CTRL, ADC_CTRL_ADCEN) == ADC_CTRL_ADCEN);
}


/**
 * @brief  Set ADCrstn ADC module enable
 * @rmtoll CTRL          rst_n      LL_ADC_ResetEn
 * @param  ADC ADC Instance
 * @param  rst_n This parameter can be one of the following values:
 *         @arg @ref LL_ADC_RSTN_0
 *         @arg @ref LL_ADC_RSTN_1
 * @retval None
 */
__STATIC_INLINE void LL_ADC_ResetEn(ADC_TypeDef *ADCx, uint32_t rst_n)
{
    MODIFY_REG(ADCx->CTRL, ADC_CTRL_ADCRSTN, rst_n);
}


/**
 * @brief  Get ADCrstn
 * @rmtoll CTRL          rst_n          LL_ADC_IsReset
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_RST_0
 *         @arg @ref LL_ADC_RST_1
 */
__STATIC_INLINE uint32_t LL_ADC_IsReset(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->CTRL, ADC_CTRL_ADCRSTN) == ADC_CTRL_ADCRSTN);
}


/**
 * @brief  Set WORKCLKSEL ADC clock select
 * @rmtoll CMU_CLKCTRL->MDLCLKB          workclk_sel      LL_ADC_SetWorkClkSrc
 * @param  ADC ADC Instance
 * @param  clk_sel This parameter can be one of the following values:
 *         @arg @ref LL_ADC_WORKCLKSEL_4M
 *         @arg @ref LL_ADC_WORKCLKSEL_16M
 *         @arg @ref LL_ADC_WORKCLKSEL_OSC
 *         @arg @ref LL_ADC_WORKCLKSEL_PLL
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetWorkClkSrc(uint32_t workclk_sel)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_ADCSEL, workclk_sel);
}


/**
 * @brief  Get WORKCLKSEL
 * @rmtoll CMU_CLKCTRL->MDLCLKB          workclk_sel      LL_ADC_GetWorkClkSrc
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_WORKCLKSEL_4M
 *         @arg @ref LL_ADC_WORKCLKSEL_16M
 *         @arg @ref LL_ADC_WORKCLKSEL_OSC
 *         @arg @ref LL_ADC_WORKCLKSEL_PLL
 */
__STATIC_INLINE uint32_t LL_ADC_GetWorkClkSrc(void)
{
    return (READ_BIT(CMU->MDLCLKB, CMU_MDLCLKB_ADCSEL));
}


/**
 * @brief  Set WorkClkPres ADC clock pre div
 * @rmtoll CMU_CLKCTRL->MDLCLKB          pres_cfg      LL_ADC_SetWorkClkDiv
 * @param  ADC ADC Instance
 * @param  pres_cfg This parameter can be one of the following values:
 *         @arg @ref LL_ADC_WORKCLKDIV_1
 *         @arg @ref LL_ADC_WORKCLKDIV_2
 *         @arg @ref LL_ADC_WORKCLKDIV_4
 *         @arg @ref LL_ADC_WORKCLKDIV_8
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetWorkClkDiv(uint32_t pres_cfg)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_ADCDIV, pres_cfg);
}


/**
 * @brief  Get WorkClkPres
 * @rmtoll CMU_CLKCTRL->MDLCLKB          pres_cfg      LL_ADC_GetWorkClkDiv
 * @param  ADC ADC Instance
 */
__STATIC_INLINE uint32_t LL_ADC_GetWorkClkDiv(void)
{
    return (READ_BIT(CMU->MDLCLKB, CMU_MDLCLKB_ADCDIV));
}


/**
 * @brief  Set CLKSEL ADC clock select
 * @rmtoll ADC->CFG          clk_sel      LL_ADC_SetClkSrc
 * @param  ADC ADC Instance
 * @param  clk_sel This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CLKSEL_WORKCLKDIV1
 *         @arg @ref LL_ADC_CLKSEL_WORKCLKDIV2
 *         @arg @ref LL_ADC_CLKSEL_FORCE0
 *         @arg @ref LL_ADC_CLKSEL_FORCE1
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV1
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV2
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV4
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV8
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetClkSrc(ADC_TypeDef *ADCx, uint32_t clk_sel)
{
    MODIFY_REG(ADCx->CFG, ADC_CFG_CLKSEL, clk_sel);
}


/**
 * @brief  Get CLKSEL
 * @rmtoll ADC->CFG          clk_sel      LL_ADC_GetClkSrc
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CLKSEL_WORKCLKDIV1
 *         @arg @ref LL_ADC_CLKSEL_WORKCLKDIV2
 *         @arg @ref LL_ADC_CLKSEL_FORCE0
 *         @arg @ref LL_ADC_CLKSEL_FORCE1
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV1
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV2
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV4
 *         @arg @ref LL_ADC_CLKSEL_BUSCLKDIV8
 */
__STATIC_INLINE uint32_t LL_ADC_GetClkSrc(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->CFG, ADC_CFG_CLKSEL));
}


/**
 * @brief  Set VrefSel select ADC refrence 1.2V or Vcc_ana
 * @rmtoll CFG          vref_sel      LL_ADC_SetVrefSel
 * @param  ADC ADC Instance
 * @param  vref_sel This parameter can be one of the following values:
 *         @arg @ref LL_ADC_VREFH
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetVrefSrc(ADC_TypeDef *ADCx, uint32_t vref_sel)
{
    MODIFY_REG(ADCx->CFG, ADC_CFG_VREFSEL, vref_sel);
}


/**
 * @brief  Get VrefSel
 * @rmtoll CFG          vref_sel          LL_ADC_GetVrefSrc
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_VREFH
 */
__STATIC_INLINE uint32_t LL_ADC_GetVrefSrc(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->CFG, ADC_CFG_VREFSEL));
}


/**
 * @brief  Set LowPowerEn:enable ADC low power mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFG          low_power_en      LL_ADC_SetLowPowerEn
 * @param  ADC ADC Instance
 * @param  low_power_en This parameter can be one of the following values:
 *         @arg @ref LL_ADC_LOWPOWER_DISABLE
 *         @arg @ref LL_ADC_LOWPOWER_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetLowPowerEn(ADC_TypeDef *ADCx, uint32_t low_power_en)
{
    MODIFY_REG(ADCx->CFG, ADC_CFG_LPM, low_power_en);
}


/**
 * @brief  Get LowPowerEn
 * @rmtoll CFG          low_power_en          LL_ADC_IsEnableLowPower
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_LOWPOWER_DISABLE
 *         @arg @ref LL_ADC_LOWPOWER_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableLowPower(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->CFG, ADC_CFG_LPM) == ADC_CFG_LPM);
}


/**
 * @brief  Set AutoSleep:enable ADC auto sleep mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFG          auto_sleep      LL_ADC_SetAutoSleepEn
 * @param  ADC ADC Instance
 * @param  auto_sleep This parameter can be one of the following values:
 *         @arg @ref LL_ADC_AUTOSLEEP_DISABLE
 *         @arg @ref LL_ADC_AUTOSLEEP_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetAutoSleepEn(ADC_TypeDef *ADCx, uint32_t auto_sleep)
{
    MODIFY_REG(ADCx->CFG, ADC_CFG_ASLEEP, auto_sleep);
}


/**
 * @brief  Get AutoSleep
 * @rmtoll CFG          auto_sleep          LL_ADC_IsEnableAutoSleep
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_AUTOSLEEP_DISABLE
 *         @arg @ref LL_ADC_AUTOSLEEP_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableAutoSleep(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->CFG, ADC_CFG_ASLEEP) == ADC_CFG_ASLEEP);
}


/**
 * @brief  Set SleepWaitTime:set Sleep wait cycle(n sys_clk) to sleep (n=0~0xFFF)
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFG                LL_ADC_SetSleepWaitTime
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetSleepWaitTime(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->CFG, ADC_CFG_TSLEEP, Value << ADC_CFG_TSLEEP_Pos);
}


/**
 * @brief  Get SleepWaitTime
 * @rmtoll CFG                  LL_ADC_GetSleepWaitTime
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref SleepWaitTime
 */
__STATIC_INLINE uint32_t LL_ADC_GetSleepWaitTime(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->CFG, ADC_CFG_TSLEEP));
}


////TIME CFG reg


/**
 * @brief  Set SampleTime:set SampleTime [11:0],ADC sample period (sample_time+1)
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TIME               LL_ADC_SetSampleTime
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value :0x0000U~0xFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetSampleTime(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->TIME, ADC_TIME_SAMPLE, Value << ADC_TIME_SAMPLE_Pos);
}


/**
 * @brief  Get SampleTime
 * @rmtoll TIME                   LL_ADC_GetSampleTime
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref SampleTime
 */
__STATIC_INLINE uint32_t LL_ADC_GetSampleTime(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->TIME, ADC_TIME_SAMPLE));
}


/**
 * @brief  Set ReadyTime:set ReadyTime [22:12],ADC sample ready period
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TIME              LL_ADC_SetReadyTime
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetReadyTime(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->TIME, ADC_TIME_READY, Value << ADC_TIME_READY_Pos);
}


/**
 * @brief  Get ReadyTime
 * @rmtoll TIME                   LL_ADC_GetReadyTime
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref ReadyTime
 */
__STATIC_INLINE uint32_t LL_ADC_GetReadyTime(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->TIME, ADC_TIME_READY));
}


/**
 * @brief  Set ConvertTime:set ConvertTime [27:24],ADC convert period
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TIME                LL_ADC_SetConvertTime
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetConvertTime(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->TIME, ADC_TIME_CONVERT, Value << ADC_TIME_CONVERT_Pos);
}


/**
 * @brief  Get ConvertTime
 * @rmtoll TIME                   LL_ADC_GetConvertTime
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref ConvertTime
 */
__STATIC_INLINE uint32_t LL_ADC_GetConvertTime(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->TIME, ADC_TIME_CONVERT));
}


/**
 * @brief  Set SampleHoldTime:set SampleHoldTime [31:28],ADC sample hole period
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TIME               LL_ADC_SetSampleHoldTime
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0x000FU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetSampleHoldTime(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->TIME, ADC_TIME_SAMPLHOLD, Value << ADC_TIME_SAMPLHOLD_Pos);
}


/**
 * @brief  Get SampleHoldTime
 * @rmtoll TIME                  LL_ADC_GetSampleHoldTime
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref SampleHoldTime
 */
__STATIC_INLINE uint32_t LL_ADC_GetSampleHoldTime(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->TIME, ADC_TIME_SAMPLHOLD));
}


/////WORK reg***********


/**
 * @brief  Set WorkMode:set ADC work Mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          work_mode      LL_ADC_SetWorkMode
 * @param  ADC ADC Instance
 * @param  work_mode This parameter can be one of the following values:
 *         @arg @ref LL_ADC_WORKMODE_SINGLE
 *         @arg @ref LL_ADC_WORKMODE_POLLING
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetWorkMode(ADC_TypeDef *ADCx, uint32_t work_mode)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_MODE, work_mode);
}


/**
 * @brief  Get WorkMode
 * @rmtoll WORK          work_mode          LL_ADC_GetWorkMode
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_WORKMODE_SINGLE
 *         @arg @ref LL_ADC_WORKMODE_POLLING
 */
__STATIC_INLINE uint32_t LL_ADC_GetWorkMode(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_MODE));
}


/**
 * @brief  Set WorkSampleTimeEn:set ADC work SampleTimeEn
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          work_sampletimeen      LL_ADC_SetWorkSampleTimeEn
 * @param  ADC ADC Instance
 * @param  work_mode This parameter can be one of the following values:
 *         @arg @ref LL_ADC_WORKST_DISABLE
 *         @arg @ref LL_ADC_WORKST_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetWorkSampleTimeEn(ADC_TypeDef *ADCx, uint32_t work_sampletimeen)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_SAMPLETIMEEN, work_sampletimeen);
}


/**
 * @brief  Get WorkSampleTimeEn
 * @rmtoll WORK          work_sampletimeen          LL_ADC_IsEnableWorkSampleTime
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_WorkST_Disable
 *         @arg @ref LL_ADC_WorkST_Enable
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableWorkSampleTime(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_SAMPLETIMEEN) == ADC_WORK_SAMPLETIMEEN);
}


/**
 * @brief  Set OneShot:enable ADC oneshot
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          one_shot      LL_ADC_SetOneShotEn
 * @param  ADC ADC Instance
 * @param  one_shot This parameter can be one of the following values:
 *         @arg @ref LL_ADC_ONESHOT_DISABLE
 *         @arg @ref LL_ADC_ONESHOT_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetOneShotEn(ADC_TypeDef *ADCx, uint32_t one_shot)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_ONESHOT, one_shot);
}


/**
 * @brief  Get OneShot
 * @rmtoll WORK          one_shot          LL_ADC_IsEnableOneShot
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_ONESHOT_DISABLE
 *         @arg @ref LL_ADC_ONESHOT_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableOneShot(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_ONESHOT) == ADC_WORK_ONESHOT);
}


/**
 * @brief  Set DoneChkEn:enable ADC done check
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          done_check      LL_ADC_SetDoneCheckEn
 * @param  ADC ADC Instance
 * @param  one_shot This parameter can be one of the following values:
 *         @arg @ref LL_ADC_DONECHECK_DISABLE
 *         @arg @ref LL_ADC_DONECHECK_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetDoneCheckEn(ADC_TypeDef *ADCx, uint32_t done_check)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_DONECHECKEN, done_check);
}


/**
 * @brief  Get DoneChkEn
 * @rmtoll WORK          done_check          LL_ADC_IsEnableDoneCheck
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_DONECHECK_DISABLE
 *         @arg @ref LL_ADC_DONECHECK_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableDoneCheck(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_DONECHECKEN) == ADC_WORK_DONECHECKEN);
}


/**
 * @brief  Set DoneStpEn:enable ADC done stop
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          done_stop      LL_ADC_SetDoneStopEn
 * @param  ADC ADC Instance
 * @param  one_shot This parameter can be one of the following values:
 *         @arg @ref LL_ADC_DONESTOP_DISABLE
 *         @arg @ref LL_ADC_DONESTOP_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetDoneStopEn(ADC_TypeDef *ADCx, uint32_t done_stop)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_DONESTOPEN, done_stop);
}


/**
 * @brief  Get DoneStpEn
 * @rmtoll WORK          done_stop          LL_ADC_IsEnableDoneStop
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_DONESTOP_DISABLE
 *         @arg @ref LL_ADC_DONESTOP_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableDoneStop(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_DONESTOPEN) == ADC_WORK_DONESTOPEN);
}


/**
 * @brief  Set ConflictContinueEn:enable ADC conflict comtinue
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          conflict_continue      LL_ADC_SetConflictConvertEn
 * @param  ADC ADC Instance
 * @param  one_shot This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CONFLICTCONVERT_DISABLE
 *         @arg @ref LL_ADC_CONFLICTCONVERT_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetConflictConvertEn(ADC_TypeDef *ADCx, uint32_t conflict_continue)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_CONFLICTCONTEN, conflict_continue);
}


/**
 * @brief  Get ConflictContinueEn
 * @rmtoll WORK          conflict_continue          LL_ADC_IsEnbaleConflictConvert
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CONFLICTCONVERT_DISABLE
 *         @arg @ref LL_ADC_CONFLICTCONVERT_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnbaleConflictConvert(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_CONFLICTCONTEN) == ADC_WORK_CONFLICTCONTEN);
}


/**
 * @brief  Set ConflictMode:set ADC conflict mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          conflict_mode      LL_ADC_SetConflictMode
 * @param  ADC ADC Instance
 * @param  one_shot This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CONFLICTMODE_NORMAL
 *         @arg @ref LL_ADC_CONFLICTMODE_LOOP
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetConflictMode(ADC_TypeDef *ADCx, uint32_t conflict_mode)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_CONFLICTMODE, conflict_mode);
}


/**
 * @brief  Get ConflictMode
 * @rmtoll WORK          conflict_mode          LL_ADC_GetConflictMode
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CONFLICTMODE_NORMAL
 *         @arg @ref LL_ADC_CONFLICTMODE_LOOP
 */
__STATIC_INLINE uint32_t LL_ADC_GetConflictMode(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_CONFLICTMODE));
}


/**
 * @brief  Set PollingNum:set PollingNum [11:8],ADC polling num
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK              LL_ADC_SetPollingNum
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetPollingNum(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_POLLINGNUM, Value << ADC_WORK_POLLINGNUM_Pos);
}


/**
 * @brief  Get PollingNum
 * @rmtoll WORK                   LL_ADC_GetPollingNum
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFU
 */
__STATIC_INLINE uint32_t LL_ADC_GetPollingNum(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_POLLINGNUM) >> ADC_WORK_POLLINGNUM_Pos);
}


/**
 * @brief  Set DoneSampleMode:set ADC done sample mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          donesample_mode      LL_ADC_SetDoneSampleMode
 * @param  ADC ADC Instance
 * @param  one_shot This parameter can be one of the following values:
 *         @arg @ref LL_ADC_DONESAMPLEMODE_PULSE
 *         @arg @ref LL_ADC_DONESAMPLEMODE_SYSCLK
 *         @arg @ref LL_ADC_DONESAMPLEMODE_ADCCLKPOS
 *         @arg @ref LL_ADC_DONESAMPLEMODE_ADCCLKNEG
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetDoneSampleMode(ADC_TypeDef *ADCx, uint32_t donesample_mode)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_DONESAMPMODE, donesample_mode);
}


/**
 * @brief  Get DoneSampleMode
 * @rmtoll WORK          donesample_mode          LL_ADC_GetDoneSampleMode
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_DONESAMPLEMODE_PULSE
 *         @arg @ref LL_ADC_DONESAMPLEMODE_SYSCLK
 *         @arg @ref LL_ADC_DONESAMPLEMODE_ADCCLKPOS
 *         @arg @ref LL_ADC_DONESAMPLEMODE_ADCCLKNEG
 */
__STATIC_INLINE uint32_t LL_ADC_GetDoneSampleMode(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_DONESAMPMODE));
}


/**
 * @brief  Set ResultGetMode:set ADC result get mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          resultget_mode      LL_ADC_SetResultGetMode
 * @param  ADC ADC Instance
 * @param  one_shot This parameter can be one of the following values:
 *         @arg @ref LL_ADC_RESULTGETMODE_0CYCLE
 *         @arg @ref LL_ADC_RESULTGETMODE_2CYCLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetResultGetMode(ADC_TypeDef *ADCx, uint32_t resultget_mode)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_RESULTGETMODE, resultget_mode);
}


/**
 * @brief  Get ResultGetMode
 * @rmtoll WORK          resultget_mode          LL_ADC_GetResultGetMode
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_RESULTGETMODE_0CYCLE
 *         @arg @ref LL_ADC_RESULTGETMODE_2CYCLE
 */
__STATIC_INLINE uint32_t LL_ADC_GetResultGetMode(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_RESULTGETMODE));
}


/**
 * @brief  Set ResultGetTime:set ResultGetTime [27:24],ADC result get
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK               LL_ADC_SetResultGetTime
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetResultGetTime(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->WORK, ADC_WORK_RESULTGETTM, Value << ADC_WORK_RESULTGETTM_Pos);
}


/**
 * @brief  Get ResultGetTime
 * @rmtoll WORK          LL_ADC_ResultGetTime(_VALUE_)          LL_ADC_GetResultGetTime
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFU
 */
__STATIC_INLINE uint32_t LL_ADC_GetResultGetTime(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->WORK, ADC_WORK_RESULTGETTM) >> ADC_WORK_RESULTGETTM_Pos);
}


/***********adc_poll_seq**********/


/**
 * @brief  Set PollSequence Channel
 * @note   This bit should not be changed when communication is ongoing.
 * @param  ADC ADC Instance
 * @param  Chx This parameter must be a combination of the following values::
 *         @arg @ref LL_ADC_POLLSEQUENCE0_CH0
 *         @arg @ref LL_ADC_POLLSEQUENCE0_CH1
 *         @arg @ref LL_ADC_POLLSEQUENCE0_CH2
 *         @arg @ref LL_ADC_POLLSEQUENCE0_CH3
 *         @arg @ref ......
 *         @arg @ref LL_ADC_POLLSEQUENCE15_CH0
 *         @arg @ref LL_ADC_POLLSEQUENCE15_CH1
 *         @arg @ref LL_ADC_POLLSEQUENCE15_CH2
 *         @arg @ref LL_ADC_POLLSEQUENCE15_CH3
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetPollSequence(ADC_TypeDef *ADCx, uint32_t Chx)
{
    WRITE_REG(ADCx->POLLSEQ, Chx);
}


/**
 * @brief  Get PollSequence
 * @param  ADC ADC Instance
 * @param  SeqIndex This parameter can be one of the following values:
 *         @arg @ref LL_ADC_POLLSEQUENCE_0
 *         @arg @ref LL_ADC_POLLSEQUENCE_1
 *         @arg @ref LL_ADC_POLLSEQUENCE_2
 *         @arg @ref LL_ADC_POLLSEQUENCE_3
 *         @arg @ref LL_ADC_POLLSEQUENCE_4
 *         @arg @ref LL_ADC_POLLSEQUENCE_5
 *         @arg @ref LL_ADC_POLLSEQUENCE_6
 *         @arg @ref LL_ADC_POLLSEQUENCE_7
 *         @arg @ref LL_ADC_POLLSEQUENCE_8
 *         @arg @ref LL_ADC_POLLSEQUENCE_9
 *         @arg @ref LL_ADC_POLLSEQUENCE_10
 *         @arg @ref LL_ADC_POLLSEQUENCE_11
 *         @arg @ref LL_ADC_POLLSEQUENCE_12
 *         @arg @ref LL_ADC_POLLSEQUENCE_13
 *         @arg @ref LL_ADC_POLLSEQUENCE_14
 *         @arg @ref LL_ADC_POLLSEQUENCE_15
 */
__STATIC_INLINE uint8_t LL_ADC_GetPollSequence(ADC_TypeDef *ADCx, uint8_t SeqIndex)
{
    return ((uint8_t)(READ_BIT(ADCx->POLLSEQ, ADC_POLLSEQ_CH00SEL_Msk << (2 * SeqIndex)) >> (2 * SeqIndex)));
}


/***********adc_poll_delay**********/


/**
 * @brief  Set PollDelay:set PollDelay [15:0],ADC polling delay time
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll POLLDLY                LL_ADC_SetPollDelay
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetPollDelay(ADC_TypeDef *ADCx, uint32_t Value)
{
    MODIFY_REG(ADCx->POLLDLY, ADC_POLLDLY_DELAY, Value << ADC_POLLDLY_DELAY_Pos);
}


/**
 * @brief  Get PollDelay
 * @rmtoll POLLDLY                  LL_ADC_GetPollDelay
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFFFU
 */
__STATIC_INLINE uint32_t LL_ADC_GetPollDelay(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->POLLDLY, ADC_POLLDLY_DELAY) >> ADC_POLLDLY_DELAY_Pos);
}


/********adc_sw_trg_ctrl*******
 * @retval None
 */


/**
 * @brief  Set SwTrigCtrl:set 1 start trig ADC convert if trigsource select the
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SWTRG          sw_trg_ctrl      LL_ADC_SetSoftwareTrig
 * @param  ADC ADC Instance
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SoftTrig(ADC_TypeDef *ADCx)
{
    SET_BIT(ADCx->SWTRG, ADC_SWTRG_CTRL);
}


/**
 * @brief  Is SwTrigCtrl:set 1 start trig ADC convert if trigsource select the
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SWTRG          sw_trg_ctrl      LL_ADC_IsSoftwareTrig
 * @param  ADC ADC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ADC_IsSoftTrig(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, LL_ADC_CHxTRIGSRC_SOFTWARE) == LL_ADC_CHxTRIGSRC_SOFTWARE);
}


/********adc_chx_cfg*******
 * @retval None
 */


/** @defgroup ADC_LL_CHxConfiguration Configuration
 * @{
 */


/**
 * @brief  Set ChxEn CHx enable :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].CFG          chx_en      LL_ADC_SetChxEn
 * @param  ADC ADC Instance
 * @param  chx_en This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHx_DISABLE
 *         @arg @ref LL_ADC_CHx_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxEn(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_en)
{
    MODIFY_REG(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_EN, chx_en);
}


/**
 * @brief  Clear ChxEn CHx enable :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].CFG          chx_en      LL_ADC_DisableAllChx
 * @param  ADC ADC Instance
 * @retval None
 */
__STATIC_INLINE void LL_ADC_DisableAllChx(ADC_TypeDef *ADCx)
{
    CLEAR_BIT(ADCx->CHANNEL[0].CFG, ADC_CHxCFG_EN);
    CLEAR_BIT(ADCx->CHANNEL[1].CFG, ADC_CHxCFG_EN);
    CLEAR_BIT(ADCx->CHANNEL[2].CFG, ADC_CHxCFG_EN);
    CLEAR_BIT(ADCx->CHANNEL[3].CFG, ADC_CHxCFG_EN);
}


/**
 * @brief  Get ChxEn
 * @rmtoll CHANNEL[n].CFG          chx_en          LL_ADC_IsEnableChx
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHx_DISABLE
 *         @arg @ref LL_ADC_CHx_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableChx(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_EN) == ADC_CHxCFG_EN);
}


/**
 * @brief  Set ChxDiff CHx enable :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].CFG          chx_diff      LL_ADC_SetChxDiffEn
 * @param  ADC ADC Instance
 * @param  chx_diff This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxDIFF_DISABLE
 *         @arg @ref LL_ADC_CHxDIFF_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxDiffEn(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_diff)
{
    MODIFY_REG(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_DIFF, chx_diff);
}


/**
 * @brief  Get ChxDiff
 * @rmtoll CHANNEL[n].CFG          chx_diff          LL_ADC_SetChxDiffEn
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxDIFF_DISABLE
 *         @arg @ref LL_ADC_CHxDIFF_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableCHxDiff(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_DIFF) == ADC_CHxCFG_DIFF);
}

/**
 * @brief  Set ChxIn CHx input select
 * @rmtoll CHANNEL[n].CFG          chx_in      LL_ADC_SetChxIn
 * @param  ADC ADC Instance
 * @param  chx_in This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxIN_DP0
 *         @arg @ref LL_ADC_CHxIN_DP1
 *         @arg @ref LL_ADC_CHxIN_DP2
 *         @arg @ref LL_ADC_CHxIN_ADC3
 *         @arg @ref LL_ADC_CHxIN_ADC4
 *         @arg @ref LL_ADC_CHxIN_ADC5
 *         @arg @ref LL_ADC_CHxIN_ADC6
 *         @arg @ref LL_ADC_CHxIN_ADC7
 *         @arg @ref LL_ADC_CHxIN_ADC8
 *         @arg @ref LL_ADC_CHxIN_ADC9
 *         @arg @ref LL_ADC_CHxIN_ADC10
 *         @arg @ref LL_ADC_CHxIN_ADC11
 *         @arg @ref LL_ADC_CHxIN_ADC12
 *         @arg @ref LL_ADC_CHxIN_ADC13
 *         @arg @ref LL_ADC_CHxIN_ADC14
 *         @arg @ref LL_ADC_CHxIN_ADC15
 *         @arg @ref LL_ADC_CHxIN_ADC16
 *         @arg @ref LL_ADC_CHxIN_12BITDAC
 *         @arg @ref LL_ADC_CHxIN_TEMPERTURE
 *         @arg @ref LL_ADC_CHxIN_VREF
 *         @arg @ref LL_ADC_CHxIN_VDDBKP
 *         @arg @ref LL_ADC_CHxIN_6BITDAC
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxIn(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_in)
{
    MODIFY_REG(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_IN, chx_in);
}


/**
 * @brief  Get ChxIn
 * @rmtoll CHANNEL[n].CFG          chx_in          LL_ADC_GetChxIn
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxIN_DADP0
 *         @arg @ref LL_ADC_CHxIN_ADC3
 *         @arg @ref LL_ADC_CHxIN_ADC4
 *         @arg @ref LL_ADC_CHxIN_ADC5
 *         @arg @ref LL_ADC_CHxIN_ADC6
 *         @arg @ref LL_ADC_CHxIN_ADC7
 *         @arg @ref LL_ADC_CHxIN_ADC8
 *         @arg @ref LL_ADC_CHxIN_ADC9
 *         @arg @ref LL_ADC_CHxIN_ADC10
 *         @arg @ref LL_ADC_CHxIN_ADC11
 *         @arg @ref LL_ADC_CHxIN_ADC12
 *         @arg @ref LL_ADC_CHxIN_ADC13
 *         @arg @ref LL_ADC_CHxIN_ADC14
 *         @arg @ref LL_ADC_CHxIN_VCHARGE_IN
 *         @arg @ref LL_ADC_CHxIN_VCHARGE_OUT
 *         @arg @ref LL_ADC_CHxIN_12BITDAC
 *         @arg @ref LL_ADC_CHxIN_TEMPERTURE
 *         @arg @ref LL_ADC_CHxIN_VREF
 *         @arg @ref LL_ADC_CHxIN_VDDBKP
 *         @arg @ref LL_ADC_CHxIN_6BITDAC
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxIn(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_IN));
}


/**
 * @brief  Set ChxTrigSourceSel CHx trig source select
 * @rmtoll CHANNEL[n].CFG          chx_trg_sc      LL_ADC_SetChxTrigSrc
 * @param  ADC ADC Instance
 * @param  chx_trg_sc This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxTRIGSRC_PAD
 *         @arg @ref LL_ADC_CHxTRIGSRC_CMP
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER0
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER1
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER2
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER3
 *         @arg @ref LL_ADC_CHxTRIGSRC_GTIMER0
 *         @arg @ref LL_ADC_CHxTRIGSRC_GTIMER1
 *         @arg @ref LL_ADC_CHxTRIGSRC_ATIMER
 *         @arg @ref LL_ADC_CHxTRIGSRC_LPTIMER0
 *         @arg @ref LL_ADC_CHxTRIGSRC_LPTIMER1
 *         @arg @ref LL_ADC_CHxTRIGSRC_RTCOTHERALARM
 *         @arg @ref LL_ADC_CHxTRIGSRC_RTCTAMPERALARM
 *         @arg @ref LL_ADC_CHxTRIGSRC_RTCSECONDS
 *         @arg @ref LL_ADC_CHxTRIGSRC_SOFTWARE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxTrigSrc(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_trg_sc)
{
    MODIFY_REG(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGSC, chx_trg_sc);
}


/**
 * @brief  Get ChxTrigSourceSel
 * @rmtoll CHANNEL[n].CFG          chx_trg_sc          LL_ADC_GetChxTrigSrc
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxTRIGSRC_PAD
 *         @arg @ref LL_ADC_CHxTRIGSRC_CMP
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER0
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER1
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER2
 *         @arg @ref LL_ADC_CHxTRIGSRC_STIMER3
 *         @arg @ref LL_ADC_CHxTRIGSRC_GTIMER0
 *         @arg @ref LL_ADC_CHxTRIGSRC_GTIMER1
 *         @arg @ref LL_ADC_CHxTRIGSRC_ATIMER
 *         @arg @ref LL_ADC_CHxTRIGSRC_LPTIMER0
 *         @arg @ref LL_ADC_CHxTRIGSRC_LPTIMER1
 *         @arg @ref LL_ADC_CHxTRIGSRC_RTCOTHERALARM
 *         @arg @ref LL_ADC_CHxTRIGSRC_RTCTAMPERALARM
 *         @arg @ref LL_ADC_CHxTRIGSRC_RTCSECONDS
 *         @arg @ref LL_ADC_CHxTRIGSRC_SOFTWARE
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxTrigSrc(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGSC));
}


/**
 * @brief  Set ChxTrgInv:set trigger inverter
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CHANNEL[n].CFG          chx_trg_inv      LL_ADC_SetChxTrigInvertEn
 * @param  ADC ADC Instance
 * @param  chx_trg_inv This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxTRIGINV_DISABLE
 *         @arg @ref LL_ADC_CHxTRIGINV_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxTrigInvertEn(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_trg_inv)
{
    MODIFY_REG(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGINV, chx_trg_inv);
}


/**
 * @brief  Get ChxTrgInv
 * @rmtoll CHANNEL[n].CFG          chx_trg_inv          LL_ADC_IsEnableChxTrigInv
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxTRIGINV_DISABLE
 *         @arg @ref LL_ADC_CHxTRIGINV_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableChxTrigInv(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGINV) == ADC_CHxCFG_TRGINV);
}


/**
 * @brief  Set ChxTrgEdge:set trigger edge
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CHANNEL[n].CFG          chx_trg_edge      LL_ADC_SetChxTrgEdge
 * @param  ADC ADC Instance
 * @param  chx_trg_edge This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxTRIGEDGE_HLEVEL
 *         @arg @ref LL_ADC_CHxTRIGEDGE_RISING
 *         @arg @ref LL_ADC_CHxTRIGEDGE_FALLING
 *         @arg @ref LL_ADC_CHxTRIGEDGE_BOTHEDGE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxTrigEdge(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_trg_edge)
{
    MODIFY_REG(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGEDGE, chx_trg_edge);
}


/**
 * @brief  Get ChxTrgEdge
 * @rmtoll CHANNEL[n].CFG          chx_trg_edge          LL_ADC_GetChxTrgEdge
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxTRIGEDGE_HLEVEL
 *         @arg @ref LL_ADC_CHxTRIGEDGE_RISING
 *         @arg @ref LL_ADC_CHxTRIGEDGE_FALLING
 *         @arg @ref LL_ADC_CHxTRIGEDGE_BOTHEDGE
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxTrigEdge(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGEDGE));
}


/**
 * @brief  Set ChxTrgDly:
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CHANNEL[n].CFG                LL_ADC_SetChxTrgDly
 * @param  ADC ADC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxTrigDelay(ADC_TypeDef *ADCx, uint8_t n, uint32_t Value)
{
    MODIFY_REG(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGDLY, Value << ADC_CHxCFG_TRGDLY_Pos);
}


/**
 * @brief  Get ChxTrgDly
 * @rmtoll CHANNEL[n].CFG                    LL_ADC_GetChxTrgDly
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFU
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxTrigDelay(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CFG, ADC_CHxCFG_TRGDLY) >> ADC_CHxCFG_TRGDLY_Pos);
}


/********adc_chx_dr_stage*******
 * @retval None
 */


/**
 * @brief  Set ChxOffsetEn CHx offset enable :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].DRS          chx_offset_en      LL_ADC_SetChxOffsetEn
 * @param  ADC ADC Instance
 * @param  chx_offset_en This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxOFFSET_DISABLE
 *         @arg @ref LL_ADC_CHxOFFSET_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxOffsetEn(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_offset_en)
{
    MODIFY_REG(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_OFFEN, chx_offset_en);
}


/**
 * @brief  Get ChxOffsetEn
 * @rmtoll CHANNEL[n].DRS          chx_offset_en          LL_ADC_IsEnableChxOffset
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxOFFSET_DISABLE
 *         @arg @ref LL_ADC_CHxOFFSET_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableChxOffset(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_OFFEN) == ADC_CHxDRS_OFFEN);
}


/**
 * @brief  Set ChxAverageEn CHx average enable :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].DRS          chx_average_en      LL_ADC_SetChxAverageEn
 * @param  ADC ADC Instance
 * @param  chx_average_en This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxAVERAGE_DISABLE
 *         @arg @ref LL_ADC_CHxAVERAGE_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxAverageEn(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_average_en)
{
    MODIFY_REG(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_AVGEN, chx_average_en);
}


/**
 * @brief  Get ChxAverageEn
 * @rmtoll CHANNEL[n].DRS          chx_average_en          LL_ADC_IsEnableChxAverage
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxAVERAGE_DISABLE
 *         @arg @ref LL_ADC_CHxAVERAGE_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableChxAverage(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_AVGEN) == ADC_CHxDRS_AVGEN);
}


/**
 * @brief  Set ChxAverageNum CHx average mode :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].DRS          chx_average_num      LL_ADC_SetChxAverageNum
 * @param  ADC ADC Instance
 * @param  chx_average_num This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxAVERAGENUM_2
 *         @arg @ref LL_ADC_CHxAVERAGENUM_4
 *         @arg @ref LL_ADC_CHxAVERAGENUM_6
 *         @arg @ref LL_ADC_CHxAVERAGENUM_8
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxAverageNum(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_average_num)
{
    MODIFY_REG(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_AVGNUM, chx_average_num);
}


/**
 * @brief  Get ChxAverageNum
 * @rmtoll CHANNEL[n].DRS          chx_average_num          LL_ADC_GetChxAverageMode
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxAVERAGENUM_2
 *         @arg @ref LL_ADC_CHxAVERAGENUM_4
 *         @arg @ref LL_ADC_CHxAVERAGENUM_6
 *         @arg @ref LL_ADC_CHxAVERAGENUM_8
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxAverageNum(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_AVGNUM));
}


/**
 * @brief  Set ChxCompareEn CHx compare function enable :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].DRS          chx_compare_en      LL_ADC_SetChxCompareEn
 * @param  ADC ADC Instance
 * @param  chx_compare_en This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxCOMPARE_DISABLE
 *         @arg @ref LL_ADC_CHxCOMPARE_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxCompareEn(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_compare_en)
{
    MODIFY_REG(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_CMPEN, chx_compare_en);
}


/**
 * @brief  Get ChxCompareEn
 * @rmtoll CHANNEL[n].DRS          chx_compare_en          LL_ADC_IsEnableChxCompare
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxCOMPARE_DISABLE
 *         @arg @ref LL_ADC_CHxCOMPARE_ENABLE
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableChxCompare(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_CMPEN) == ADC_CHxDRS_CMPEN);
}


/**
 * @brief  Set ChxCompareMode CHx compare mode :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].DRS          chx_compare_mode      LL_ADC_SetChxCompareMode
 * @param  ADC ADC Instance
 * @param  chx_compare_mode This parameter can be one of the following values:
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_LARGER
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_LESS
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_BETWEEN
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_OUTSIDE
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxCompareMode(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_compare_mode)
{
    MODIFY_REG(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_CMPM, chx_compare_mode);
}


/**
 * @brief  Get ChxCompareMode
 * @rmtoll CHANNEL[n].DRS          chx_compare_mode          LL_ADC_GetChxCompareMode
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_LARGER
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_LESS
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_BETWEEN
 *         @arg @ref LL_ADC_CHxCOMPAREMODE_OUTSIDE
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxCompareMode(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].DRS, ADC_CHxDRS_CMPM));
}


/********adc_chx_result*******
 * @retval None
 */


/**
 * @brief  Get ChxResult n=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].RESULT          chx_result          LL_ADC_GetChxResult
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref 0x00U~0xFFU
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxResult(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].RESULT, ADC_CHxRESULT_RESULT));
}


/********adc_chx_offset*******
 * @retval None
 */


/**
 * @brief  Set ChxOffsetVal CHx offset :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].OFFSET          chx_offset_val      LL_ADC_SetChxOffsetVal
 * @param  ADC ADC Instance
 * @param  chx_offset_val This parameter can be one of the following values:
 *         @arg @ref 0x0000U~0xFFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxOffsetVal(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_offset_val)
{
    MODIFY_REG(ADCx->CHANNEL[n].OFFSET, ADC_CHxOFFSET_OFFAVAL, chx_offset_val);
}


/**
 * @brief  Get ChxOffsetVal
 * @rmtoll CHANNEL[n].OFFSET          chx_offset_val          LL_ADC_GetChxOffsetVal
 * @param  ADC ADC Instance
 * @retval Returned
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxOffsetVal(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].OFFSET, ADC_CHxOFFSET_OFFAVAL));
}


/********adc_chx_cmpval*******
 * @retval None
 */


/**
 * @brief  Set ChxCompareVal1 CHx offset :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].CMPVAL          chx_compare_val1      LL_ADC_SetChxCompareValue1
 * @param  ADC ADC Instance
 * @param  chx_compare_val1 This parameter can be one of the following values:
 *         @arg @ref 0x0000U~0xFFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxCompareValue1(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_compare_val1)
{
    MODIFY_REG(ADCx->CHANNEL[n].CMPVAL, ADC_CHxCMPVAL_CV1, chx_compare_val1);
}


/**
 * @brief  Get ChxCompareVal1
 * @rmtoll CHANNEL[n].CMPVAL          chx_compare_val1          LL_ADC_GetChxCompareValue1
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref 0x0000U~0xFFFFU
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxCompareValue1(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CMPVAL, ADC_CHxCMPVAL_CV1));
}


/**
 * @brief  Set ChxCompareVal2 CHx offset :x=0->a,1->b,2->c,3->d
 * @rmtoll CHANNEL[n].CMPVAL          chx_compare_val2      LL_ADC_SetChxCompareValue2
 * @param  ADC ADC Instance
 * @param  chx_compare_val2 This parameter can be one of the following values:
 *         @arg @ref 0x0000U~0xFFFFU
 * @retval None
 */
__STATIC_INLINE void LL_ADC_SetChxCompareValue2(ADC_TypeDef *ADCx, uint8_t n, uint32_t chx_compare_val2)
{
    MODIFY_REG(ADCx->CHANNEL[n].CMPVAL, ADC_CHxCMPVAL_CV2, chx_compare_val2 << ADC_CHxCMPVAL_CV2_Pos);
}


/**
 * @brief  Get ChxCompareVal2
 * @rmtoll CHANNEL[n].CMPVAL          chx_compare_val2          LL_ADC_GetChxCompareValue2
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref 0x0000U~0xFFFFU
 */
__STATIC_INLINE uint32_t LL_ADC_GetChxCompareValue2(ADC_TypeDef *ADCx, uint8_t n)
{
    return (READ_BIT(ADCx->CHANNEL[n].CMPVAL, ADC_CHxCMPVAL_CV2));
}


/**
 * @brief  enable adc int
 * @param  adc Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_ADC_CH0IE
 *         @arg @ref LL_ADC_CH1IE
 *         @arg @ref LL_ADC_CH2IE
 *         @arg @ref LL_ADC_CH3IE
 *         @arg @ref LL_ADC_CH0DMAE
 *         @arg @ref LL_ADC_CH1DMAE
 *         @arg @ref LL_ADC_CH2DMAE
 *         @arg @ref LL_ADC_CH3DMAE
 *         @arg @ref LL_ADC_CH0OVRUNIE
 *         @arg @ref LL_ADC_CH1OVRUNIE
 *         @arg @ref LL_ADC_CH2OVRUNIE
 *         @arg @ref LL_ADC_CH3OVRUNIE
 *         @arg @ref LL_ADC_READYIE
 *         @arg @ref LL_ADC_DONEERRIE
 * @retval
 */
__STATIC_INLINE void LL_ADC_EnableINT(ADC_TypeDef *ADCx, uint32_t intsrc)
{
    SET_BIT(ADCx->INTEN, intsrc);
}


/**
 * @brief  disable ADC int
 * @param  adc Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_ADC_CH0IE
 *         @arg @ref LL_ADC_CH1IE
 *         @arg @ref LL_ADC_CH2IE
 *         @arg @ref LL_ADC_CH3IE
 *         @arg @ref LL_ADC_CH0DMAE
 *         @arg @ref LL_ADC_CH1DMAE
 *         @arg @ref LL_ADC_CH2DMAE
 *         @arg @ref LL_ADC_CH3DMAE
 *         @arg @ref LL_ADC_CH0OVRUNIE
 *         @arg @ref LL_ADC_CH1OVRUNIE
 *         @arg @ref LL_ADC_CH2OVRUNIE
 *         @arg @ref LL_ADC_CH3OVRUNIE
 *         @arg @ref LL_ADC_READYIE
 *         @arg @ref LL_ADC_DONEERRIE
 * @retval
 */
__STATIC_INLINE void LL_ADC_DisableINT(ADC_TypeDef *ADCx, uint32_t intsrc)
{
    CLEAR_BIT(ADCx->INTEN, intsrc);
}


/**
 * @brief  disable all int
 * @param  adc Instance
 * @retval
 */
__STATIC_INLINE void LL_ADC_DisableAllINT(ADC_TypeDef *ADCx)
{
    CLEAR_REG(ADCx->INTEN);
}

/**
 * @brief  Is Enable ADC DMA
 * @param  adc Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref ADC_INTEN_CH0CODMAS
 *         @arg @ref ADC_INTEN_CH1CODMAS
 *         @arg @ref ADC_INTEN_CH2CODMAS
 *         @arg @ref ADC_INTEN_CH3CODMAS
 * @retval
 */
__STATIC_INLINE uint32_t LL_ADC_IsEnableDMA(ADC_TypeDef *ADCx, uint32_t intsrc)
{
    return (READ_BIT(ADCx->INTEN, (intsrc & (ADC_INTEN_CH0CODMAS | ADC_INTEN_CH1CODMAS | ADC_INTEN_CH2CODMAS | ADC_INTEN_CH3CODMAS))));
}


/**
 * @brief  Get AdcStatueRecord
 * @rmtoll Statue RECORD          adc_Statuerecord          LL_ADC_GetAdcStatueFlag
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref
 */
__STATIC_INLINE uint32_t LL_ADC_GetFlag_Eoc(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->STATUSRCD, ADC_STATUSRCD_CH0CONVCO | ADC_STATUSRCD_CH1CONVCO | ADC_STATUSRCD_CH2CONVCO | ADC_STATUSRCD_CH3CONVCO));
}


/**
 * @brief  Get AdcOvrunFlag
 * @rmtoll Statue RECORD          adc_Statuerecord          LL_ADC_GetAdcOvrunFlag
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref
 */
__STATIC_INLINE uint32_t LL_ADC_GetFlag_OverRun(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->STATUSRCD, ADC_STATUSRCD_CH0OVRUN | ADC_STATUSRCD_CH1OVRUN | ADC_STATUSRCD_CH2OVRUN | ADC_STATUSRCD_CH3OVRUN | ADC_STATUSRCD_READY));
}


/**
 * @brief  Get AdcReadyFlag
 * @rmtoll Statue RECORD          adc_Statuerecord          LL_ADC_IsReady
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref
 */
__STATIC_INLINE uint32_t LL_ADC_IsReady(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->STATUS, ADC_STATUS_READY) == ADC_STATUS_READY);
}


/**
 * @brief  Get AdcDoneErrFlag
 * @rmtoll Statue RECORD          adc_Statuerecord          LL_ADC_IsActiveDoneErrFlag
 * @param  ADC ADC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref
 */
__STATIC_INLINE uint32_t LL_ADC_IsActiveFlag_DoneErr(ADC_TypeDef *ADCx)
{
    return (READ_BIT(ADCx->STATUSRCD, ADC_STATUSRCD_DONEERR) == ADC_STATUSRCD_DONEERR);
}
/**
 * @brief  Get Status Record
 * @rmtoll STATUSRCD               adc_Statuerecord
 * @param  ADC ADC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_ADC_FLAG_DONEERRR
            @arg @ref LL_ADC_FLAG_READY
            @arg @ref LL_ADC_FLAG_EOC0
            @arg @ref LL_ADC_FLAG_EOC1
            @arg @ref LL_ADC_FLAG_EOC2
            @arg @ref LL_ADC_FLAG_EOC3
            @arg @ref LL_ADC_FLAG_CH0OR
            @arg @ref LL_ADC_FLAG_CH1OR
            @arg @ref LL_ADC_FLAG_CH2OR
            @arg @ref LL_ADC_FLAG_CH3OR
            @arg @ref LL_ADC_FLAG_ALL
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ADC_GetStatus(ADC_TypeDef *ADCx, uint32_t flag)
{
    return (READ_BIT(ADCx->STATUSRCD, flag));
}

/**
 * @brief  Status Record Clear
 * @rmtoll SRCL               LL_ADC_ClearStatus
 * @param  ADC ADC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_ADC_FLAG_DONEERRR
            @arg @ref LL_ADC_FLAG_READY
            @arg @ref LL_ADC_FLAG_EOC0
            @arg @ref LL_ADC_FLAG_EOC1
            @arg @ref LL_ADC_FLAG_EOC2
            @arg @ref LL_ADC_FLAG_EOC3
            @arg @ref LL_ADC_FLAG_CH0OR
            @arg @ref LL_ADC_FLAG_CH1OR
            @arg @ref LL_ADC_FLAG_CH2OR
            @arg @ref LL_ADC_FLAG_CH3OR
            @arg @ref LL_ADC_FLAG_ALL
 * @retval None
 */
__STATIC_INLINE void LL_ADC_ClearStatus(ADC_TypeDef *ADCx, uint32_t flag)
{
    SET_BIT(ADCx->SRCL, flag);
}


/**
 * @brief  Clear all Status Record Clear
 * @rmtoll SRCL               LL_ADC_ClearAllStatus
 * @param  ADC ADC Instance
 * @retval None
 */
__STATIC_INLINE void LL_ADC_ClearAllStatus(ADC_TypeDef *ADCx)
{
    SET_BIT(ADCx->SRCL, 0x3F3F0F);
}


/**
 * @brief  ENABLE VREF 1.2V
 * @retval None
 */
__STATIC_INLINE void LL_VREF12_Enable(void)
{
    MODIFY_REG(VREF->CTRL, VREF_CTRL_VREFEN_Msk | VREF_CTRL_LDOEN_Msk, (1 << VREF_CTRL_SCMODE_Pos) | VREF_CTRL_VREFEN | VREF_CTRL_LDOEN);
}

/**
 * @brief  TRIM VREF 1.2V
 * @retval None
 */
__STATIC_INLINE void LL_VREF12_Trim(uint32_t val)
{
    MODIFY_REG(VREF->TRIM, 0x7F, val);
}

/**
 * @brief  DISABLE VREF 1.2V
 * @retval None
 */
__STATIC_INLINE void LL_VREF12_Disable(void)
{
    CLEAR_BIT(VREF->CTRL, VREF_CTRL_VREFEN | VREF_CTRL_LDOEN);
}


/**
 * @brief  Get the VREF12 BG Flag.
 * @retval None
 */
__STATIC_INLINE uint32_t LL_VREF12_IsReady(void)
{
    return (READ_BIT(VREF->BG_FLAG, VREF_BG_FLAG) == VREF_BG_FLAG);
}


void LL_ADC_StructInitParam(LL_ADC_InitTypeDef *ADC_ParamInitStruct);


void LL_ADC_Init(ADC_TypeDef *ADCx, LL_ADC_InitTypeDef *ADC_InitStruct);


void LL_ADC_ChannelInit(ADC_TypeDef *ADCx, uint32_t Channel, LL_ADC_ChannelInitTypeDef *ADC_ChannelInitParamTypeDef);


#endif /* defined (ADC0) */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM366_LL_ADC_H */

/************************ (C) COPYRIGHT ...... *****END OF FILE****/
