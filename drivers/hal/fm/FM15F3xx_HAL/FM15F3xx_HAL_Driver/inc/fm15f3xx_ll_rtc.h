/**
 ******************************************************************************
 * @file    fm15f3xx_ll_rtc.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_RTC_H
#define FM15F3XX_LL_RTC_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


typedef struct {
    uint32_t    TimeSeconds;
    uint32_t    TimeCycle;
    uint32_t    TimePrescaler;

    uint32_t    SecondsAlarm;
    uint32_t    CycleAlarm;

    uint32_t    CompensationDirection;
    uint32_t    CompensationInterVal;
    uint32_t    CompensationValue;

    uint32_t    CpTestMode;
    uint32_t    WakeUpMode;
    uint32_t    WakeUpPolarity;
    uint32_t    WakeUpOpenDrain;
    uint32_t    LockMode;

    uint32_t ClkOuputEn;
} LL_UtcTimer_InitTypeDef;


/**********************************************RTC  TIMER *****************************************************/


/** @defgroup UtcTimer_LL_INIT  RTC UtcTimer  Exported Init structure
 * @{
 */

#define  LL_RTC_ENABLE  (0x00000001U)
#define  LL_RTC_DISABLE (0x00000000U)
/* RTC_VBAT_TIME  time_compensation reg   */
#define  LL_RTC_RAISE_COMPENSATION  (0x00000000U)
#define  LL_RTC_FALL_COMPENSATION   RTC_VBAT_COMP_SIGN_Msk

/* RTC_VBAT_TIME time_control reg               */
#define  LL_RTC_DISABLE_ACCESS  (0x00000000U)
#define  LL_RTC_ENABLE_ACCESS   RTC_TIME_CTRL_NACCESS_Msk

#define  LL_RTC_TAMP_NOSTOPTIMER    (0x00000000U)
#define  LL_RTC_TAMP_STOPTIMER      RTC_TIME_CTRL_STOPMODE_Msk

#define  LL_RTC_CLOSE_CPTEST_MODE   (0x00000000U)
#define  LL_RTC_OPEN_CPTEST_MODE    RTC_TIME_CTRL_CPMODE

#define  LL_RTC_CLOSE_CPMODE    (0x00000000U)
#define  LL_RTC_OPEN_CPMODE     RTC_TIME_CTRL_CPMODE_Msk
/* RTC_VBAT_TIME time_clk_cfg reg               */
#define  LL_RTC_UCTCLKSEL_32XTAL    (0x00000000U)
#define  LL_RTC_UCTCLKSEL_32IRC     (0x00000001U)
#define  LL_RTC_UCTCLKSEL_TESTCLK   (0x00000002U)
#define  LL_RTC_UCTCLKSEL_PADCLK    (0x00000003U)

#define  LL_RTC_UCTCLKSEL_DISABLE_CLKOUT    (0x00000000U)
#define  LL_RTC_UCTCLKSEL_ENALBE_CLKOUT     RTC_TIME_CLKCFG_CLKOUTPUT_Msk

/* RTC_VBAT_TIME time_wakeup_cfg   */
#define  LL_RTC_CLOSE_WAKUP_PIN (0x00000000U)
#define  LL_RTC_OPEN_WAKUP_PIN  RTC_TIME_WAKUPCFG_PINON

#define  LL_RTC_CLOSE_WAKUP_PININV  (0x00000000U)
#define  LL_RTC_OPEN_WAKUP_PININV   RTC_TIME_WAKUPCFG_PININV_Msk

#define  LL_RTC_CLOSE_WAKUP_PINODEN (0x00000000U)
#define  LL_RTC_OPEN_WAKUP_PINODEN  RTC_TIME_WAKUPCFG_PINODEN_Msk
/* RTC_VBAT_TIME time_lock   */
#define  LL_RTC_NO_REG_LOCK (0x00000000U)

/* RTC_VBAT_TIME time_int_en   */
#define  LL_RTC_UTC_ENABLE_INVLDIE  RTC_TIME_TIMEIE_INVLDIE_Msk
#define  LL_RTC_UTC_ENABLE_OVIE     RTC_TIME_TIMEIE_OVIE_Msk
#define  LL_RTC_UTC_ENABLE_SECONDIE RTC_TIME_TIMEIE_SECONDIE_Msk
#define  LL_RTC_UTC_ENABLE_1SIE     RTC_TIME_TIMEIE_1SIE_Msk
#define  LL_RTC_UTC_ENABLE_CYCLEIE  RTC_TIME_TIMEIE_CYCLEIE_Msk
#define  LL_RTC_UTC_ENABLE_1SFLAGIE RTC_TIME_TIMEIE_1SFLAGIE_Msk

#define  LL_RTC_UTC_DISABLE_ALLIE   (0x00000000U)
#define  LL_RTC_UTC_ENABLE_ALLIE    RTC_TIME_TIMEIE_INVLDIE_Msk | RTC_TIME_TIMEIE_OVIE_Msk | RTC_TIME_TIMEIE_SECONDIE_Msk | RTC_TIME_TIMEIE_1SIE_Msk | RTC_TIME_TIMEIE_CYCLEIE_Msk | RTC_TIME_TIMEIE_1SFLAGIE_Msk


/*time_status*/
#define ALLFLAG         0x0000001FU
#define ALLRECORD       0x001F0000U
#define ALLRECORDPOS    (16U)


/* RTC&BKP_RD_PARITY_ERR()  RTC_RDPE_reg   */
__STATIC_INLINE uint32_t LL_RTC_IsActiveFlag_Err(void)
{
    return (READ_BIT(RTC_VMAIN->RDPE, RTC_VMAIN_RDPE_RDPE_Msk) == RTC_VMAIN_RDPE_RDPE);
}


__STATIC_INLINE void LL_RTC_ClearFlag_Err(void)
{
    SET_BIT(RTC_VMAIN->RDPE, RTC_VMAIN_RDPE_RDPE_Msk);
}


/* RTC_VBAT_TIME  time_seconds reg   */
__STATIC_INLINE void LL_RTC_SetSecondTimerValue(uint32_t UTC_time_seconds)
{
    WRITE_REG(RTC_TIME->SECONDS, UTC_time_seconds);
}


__STATIC_INLINE uint32_t LL_RTC_GetSecondTimerValue(void)
{
    return (READ_REG(RTC_TIME->SECONDS));
}


/* RTC_VBAT_TIME  time_cycle reg   */
__STATIC_INLINE void LL_RTC_SetCycleTimerValue(uint32_t UTC_time_cycle)
{
    WRITE_REG(RTC_TIME->CYCLE, UTC_time_cycle);
}


__STATIC_INLINE uint32_t LL_RTC_GetCycleTimerValue(void)
{
    return (READ_REG(RTC_TIME->CYCLE));
}


/* RTC_VBAT_TIME  time_prescaler   */
__STATIC_INLINE void LL_RTC_SetTimerPrescaler(uint32_t UTC_time_prescaler)
{
    WRITE_REG(RTC_TIME->PSC, UTC_time_prescaler);
}


__STATIC_INLINE uint32_t LL_RTC_GetTimerPrescaler(void)
{
    return (READ_REG(RTC_TIME->PSC));
}


/* RTC_VBAT_TIME  time_seconds_alarm reg   */
__STATIC_INLINE void LL_RTC_SetSecondTimerAlarm(uint32_t UTC_seconds_alarm)
{
    WRITE_REG(RTC_TIME->SECALARM, UTC_seconds_alarm);
}


/* RTC_VBAT_TIME  time_cycle_alarm reg   */
__STATIC_INLINE void LL_RTC_SetCycleTimerAlarm(uint32_t UTC_cycle_alarm)
{
    WRITE_REG(RTC_TIME->CYCALARM, UTC_cycle_alarm);
}


__STATIC_INLINE uint32_t LL_RTC_GetCycleTimerAlarm(void)
{
    return (READ_REG(RTC_TIME->CYCALARM));
}


/* RTC_VBAT_TIME  time_compensation reg   */
__STATIC_INLINE void LL_RTC_EnableRaiseCompensation(void)
{
    CLEAR_BIT(RTC_TIME->COMPENSATION, RTC_VBAT_COMP_SIGN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableFallCompensation(void)
{
    SET_BIT(RTC_TIME->COMPENSATION, RTC_VBAT_COMP_SIGN_Msk);
}


__STATIC_INLINE void LL_RTC_SetCompensationValue(uint32_t UTC_Compensation_val)
{
    MODIFY_REG(RTC_TIME->COMPENSATION, RTC_VBAT_COMP_VAL_Msk, UTC_Compensation_val);
}


__STATIC_INLINE void LL_RTC_SetCompensationInterval(uint32_t UTC_Compensation_Interval)
{
    MODIFY_REG(RTC_TIME->COMPENSATION, RTC_VBAT_COMP_INVL_Msk, UTC_Compensation_Interval << RTC_VBAT_COMP_INVL_Pos);
}


/* RTC_VBAT_TIME time_control reg   */

__STATIC_INLINE void LL_RTC_DisableNonSupervisorAccess(void)
{
    CLEAR_BIT(RTC_TIME->CTRL, RTC_TIME_CTRL_NACCESS_Msk);
}


__STATIC_INLINE void LL_RTC_EnableNonSupervisorAccess(void)
{
    SET_BIT(RTC_TIME->CTRL, RTC_TIME_CTRL_NACCESS_Msk);
}


__STATIC_INLINE void LL_RTC_DisableTamperStopMode(void)
{
    CLEAR_BIT(RTC_TIME->CTRL, RTC_TIME_CTRL_STOPMODE_Msk);
}


__STATIC_INLINE void LL_RTC_EnableTamperStopMode(void)
{
    SET_BIT(RTC_TIME->CTRL, RTC_TIME_CTRL_STOPMODE_Msk);
}


/* RTC_VBAT_TIME time_clk_cfg reg   */


__STATIC_INLINE void LL_RTC_Enable32kXtalClk(void)
{
    SET_BIT(RTC_TIME->CLKCFG, RTC_TIME_CLKCFG_32XTALEN_Msk);
}


__STATIC_INLINE void LL_RTC_Disable32kXtalClk(void)
{
    CLEAR_BIT(RTC_TIME->CLKCFG, RTC_TIME_CLKCFG_32XTALEN_Msk);
}


__STATIC_INLINE void LL_RTC_Enable32kOutput(void)
{
    SET_BIT(RTC_TIME->CLKCFG, RTC_TIME_CLKCFG_CLKOUTPUT_Msk);
}


__STATIC_INLINE void LL_RTC_Disable32kOutput(void)
{
    CLEAR_BIT(RTC_TIME->CLKCFG, RTC_TIME_CLKCFG_CLKOUTPUT_Msk);
}


/* RTC_VBAT_TIME time_cnt_en reg   */

__STATIC_INLINE void LL_RTC_EnableSecondsTimer(void)
{
    SET_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_SECCNTEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableSecondsTimer(void)
{
    CLEAR_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_SECCNTEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableCycleTimer(void)
{
    SET_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_CYCCNTEN_MSK);
}


__STATIC_INLINE void LL_RTC_DisableCycleTimer(void)
{
    CLEAR_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_CYCCNTEN_MSK);
}


__STATIC_INLINE void LL_RTC_EnableTimerPrescale(void)
{
    SET_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_PSCCNTEN_MSK);
}


__STATIC_INLINE void LL_RTC_DisableTimerPrescale(void)
{
    CLEAR_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_PSCCNTEN_MSK);
}


/* prescale_cnt_en & seconds_cnt_en */
__STATIC_INLINE void LL_RTC_EnableUtc(void)
{
    SET_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_SECCNTEN_Msk | RTC_TIME_CNTEN_PSCCNTEN_MSK);
}


__STATIC_INLINE void LL_RTC_DisableUtc(void)
{
    CLEAR_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_SECCNTEN_Msk);
}


/* prescale_cnt_en & seconds_cnt_en & csr_cycle_cnt_en????*/
__STATIC_INLINE void LL_RTC_EnableAllTimer(void)
{
    SET_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_SECCNTEN_Msk | RTC_TIME_CNTEN_CYCCNTEN_MSK | RTC_TIME_CNTEN_PSCCNTEN_MSK);
}


__STATIC_INLINE void LL_RTC_DisableAllTimer(void)
{
    CLEAR_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_SECCNTEN_Msk | RTC_TIME_CNTEN_CYCCNTEN_MSK | RTC_TIME_CNTEN_PSCCNTEN_MSK);
}


/**
 * @brief  Get RTC_TIME->CNTEN
 * @rmtoll CNTEN                LL_RTC_IsEnable
 * @param  none
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_RTC_DISABLE
 *         @arg @ref LL_RTC_ENABLE
 */
__STATIC_INLINE uint32_t LL_RTC_IsEnable(void)
{
    return (READ_BIT(RTC_TIME->CNTEN, RTC_TIME_CNTEN_SECCNTEN_Msk) == RTC_TIME_CNTEN_SECCNTEN);
}


/* time_wakeup_cfg*/
__STATIC_INLINE void LL_RTC_EnableWakeupPin(void)
{
    SET_BIT(RTC_TIME->WAKUPCFG, RTC_TIME_WAKUPCFG_PINON_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakeupPin(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPCFG, RTC_TIME_WAKUPCFG_PINON_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupPinInv(void)
{
    SET_BIT(RTC_TIME->WAKUPCFG, RTC_TIME_WAKUPCFG_PININV_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupPinInv(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPCFG, RTC_TIME_WAKUPCFG_PININV_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupPinOD(void)
{
    SET_BIT(RTC_TIME->WAKUPCFG, RTC_TIME_WAKUPCFG_PINODEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupPinOD(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPCFG, RTC_TIME_WAKUPCFG_PINODEN_Msk);
}


/* time_int_en*/
__STATIC_INLINE void LL_RTC_EnableINT_Invalid(void)
{
    SET_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_INVLDIE_Msk);
}


__STATIC_INLINE void LL_RTC_DisableINT_Invalid(void)
{
    CLEAR_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_INVLDIE_Msk);
}


__STATIC_INLINE void LL_RTC_EnableINT_OverFlow(void)
{
    SET_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_OVIE_Msk);
}


__STATIC_INLINE void LL_RTC_DisableINT_OverFlow(void)
{
    CLEAR_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_OVIE_Msk);
}


__STATIC_INLINE void LL_RTC_EnableINT_SecondTimerAlarm(void)
{
    SET_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_SECONDIE_Msk);
}


__STATIC_INLINE void LL_RTC_DisableINT_SecondTimerAlarm(void)
{
    CLEAR_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_SECONDIE_Msk);
}


__STATIC_INLINE void LL_RTC_EnableINT_1s(void)
{
    SET_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_1SIE_Msk);
}


__STATIC_INLINE void LL_RTC_DisableINT_1s(void)
{
    CLEAR_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_1SIE_Msk);
}


__STATIC_INLINE void LL_RTC_EnableINT_CycleTimerAlarm(void)
{
    SET_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_CYCLEIE_Msk);
}


__STATIC_INLINE void LL_RTC_DisableINT_CycleTimerAlarm(void)
{
    CLEAR_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_CYCLEIE_Msk);
}


__STATIC_INLINE void LL_RTC_EnableINT_1sFlag(void)
{
    SET_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_1SFLAGIE_Msk);
}


__STATIC_INLINE void LL_RTC_DisableINT_1sFlag(void)
{
    CLEAR_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_1SFLAGIE_Msk);
}


__STATIC_INLINE void LL_RTC_EnableINT_All(void)
{
    SET_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_INVLDIE_Msk | RTC_TIME_TIMEIE_OVIE_Msk | RTC_TIME_TIMEIE_SECONDIE_Msk
            | RTC_TIME_TIMEIE_1SIE_Msk | RTC_TIME_TIMEIE_CYCLEIE_Msk | RTC_TIME_TIMEIE_1SFLAGIE_Msk);
}


__STATIC_INLINE void LL_RTC_DisableINT_All(void)
{
    CLEAR_BIT(RTC_TIME->TIMEIE, RTC_TIME_TIMEIE_INVLDIE_Msk | RTC_TIME_TIMEIE_OVIE_Msk | RTC_TIME_TIMEIE_SECONDIE_Msk
              | RTC_TIME_TIMEIE_1SIE_Msk | RTC_TIME_TIMEIE_CYCLEIE_Msk | RTC_TIME_TIMEIE_1SFLAGIE_Msk);
}


/* time_Wakeup en*/
__STATIC_INLINE void LL_RTC_EnableWakupOutInvalid(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_INVLDWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutInvalid(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_INVLDWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOutOverflow(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_OVWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutOverflow(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_OVWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOutSecondTimerAlarm(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_SECONDWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutSecondTimerAlarm(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_SECONDWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOut1s(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_1SWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOut1s(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_1SWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOutCycleTimer(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_CYCWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutCycleTimer(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_CYCWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOutInvalidFlag(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_INVLDFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutInvalidFlag(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_INVLDFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOutOverflowFlag(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_OVFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutOverflowFlag(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_OVFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOutSecondTimerAlarmFlag(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_SECONDFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutSecondTimerAlarmFlag(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_SECONDFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOut1sFlag(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_1SFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOut1sFlag(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_1SFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_EnableWakupOutCycleTimerAlarmFlag(void)
{
    SET_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_CYCFGWUEN_Msk);
}


__STATIC_INLINE void LL_RTC_DisableWakupOutCycleTimerAlarmFlag(void)
{
    CLEAR_BIT(RTC_TIME->WAKUPEN, RTC_TIME_WAKUPEN_CYCFGWUEN_Msk);
}


/* time_record_clr*/
__STATIC_INLINE void LL_RTC_ClearRecord_Invalid(void)
{
    SET_BIT(RTC_TIME->RECORDCLR, RTC_TIME_RECORDCLR_INVLDCLR);
}


__STATIC_INLINE void LL_RTC_ClearRecord_Overflow(void)
{
    SET_BIT(RTC_TIME->RECORDCLR, RTC_TIME_RECORDCLR_OVCLR);
}


__STATIC_INLINE void LL_RTC_ClearRecord_Alarm(void)
{
    SET_BIT(RTC_TIME->RECORDCLR, RTC_TIME_RECORDCLR_SECONDCLR);
}


__STATIC_INLINE void LL_RTC_ClearRecord_1s(void)
{
    SET_BIT(RTC_TIME->RECORDCLR, RTC_TIME_RECORDCLR_1SCLR);
}


__STATIC_INLINE void LL_RTC_ClearRecord_Cycle(void)
{
    SET_BIT(RTC_TIME->RECORDCLR, RTC_TIME_RECORDCLR_CYCCLR);
}


__STATIC_INLINE void LL_RTC_ClearRecord_All(void)
{
    SET_BIT(RTC_TIME->RECORDCLR, RTC_TIME_RECORDCLR_INVLDCLR | RTC_TIME_RECORDCLR_OVCLR | RTC_TIME_RECORDCLR_SECONDCLR \
            | RTC_TIME_RECORDCLR_1SCLR | RTC_TIME_RECORDCLR_CYCCLR);
}


__STATIC_INLINE uint32_t LL_RTC_GetFlag(void)
{
    return (READ_REG(RTC_TIME->STATUS) & ALLFLAG);
}


__STATIC_INLINE uint32_t LL_RTC_GetRecord(void)
{
    return (READ_REG(RTC_TIME->STATUS) & ALLRECORD);
}


/* RTC_VBAT_TIME time_softrst reg   */
__STATIC_INLINE void LL_RTC_SoftReset(void)
{
    WRITE_REG(RTC_TIME->CTRL, 0x77887788);
    WRITE_REG(RTC_TIME->CTRL, 0x95279527);

    WRITE_REG(RTC_TIME->CTRL, 0x77887788);
    WRITE_REG(RTC_TIME->CTRL, 0x95279527);
}


/*time_lock */
__STATIC_INLINE void LL_RTC_LockCompensation(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_COMPLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockControl(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_CTRLLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockClkCfg(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_CLKCFGLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockCntEn(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_CNTENLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockWakeUpCfg(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_WAKUPCFGLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockIntEn(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_INTENLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockWakeUpEn(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_WAKUPENLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockRecordClr(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_RECORDCLRLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockSoftRst(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_SOFTRSTLK_Msk);
}


__STATIC_INLINE void LL_RTC_LockLock(void)
{
    SET_BIT(RTC_TIME->LOCK, RTC_TIME_LOCK_LOCKLK_Msk);
}


__STATIC_INLINE void LL_RTC_Lock_All(void)
{
    SET_BIT(RTC_TIME->LOCK, ~((uint32_t) RTC_TIME_LOCK_RECORDCLRLK));
}


__STATIC_INLINE void LL_RTC_LockReg(uint32_t RegBit)
{
    MODIFY_REG(RTC_TIME->LOCK, ~RegBit, RegBit);
}


/*time_compensation_status*/

__STATIC_INLINE uint32_t LL_RTC_GetCompensationValue(void)
{
    return ((READ_REG(RTC_TIME->COMPSTAUTS) & RTC_VBAT_COMPSTAUTS_VAL_Msk) >> RTC_VBAT_COMPSTAUTS_VAL_Pos);
}


__STATIC_INLINE uint32_t LL_RTC_GetCompensationDir(void)
{
    return (READ_BIT(RTC_TIME->COMPSTAUTS, RTC_VBAT_COMPSTAUTS_SIGN_Msk));
}


__STATIC_INLINE uint32_t LL_RTC_GetCompensationInterval(void)
{
    return ((READ_REG(RTC_TIME->COMPSTAUTS) & RTC_VBAT_COMPSTAUTS_INVL_Msk) >> RTC_VBAT_COMPSTAUTS_INVL_Pos);
}


/*time_tamper_time_seconds*/
__STATIC_INLINE uint32_t LL_RTC_GetTamperEventTime(void)
{
    return (READ_REG(RTC_TIME->COMPSTAUTS));
}


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

