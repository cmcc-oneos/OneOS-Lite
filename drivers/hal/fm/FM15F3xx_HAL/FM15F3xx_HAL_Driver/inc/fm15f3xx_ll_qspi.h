/**
 ******************************************************************************
 * @file    fm15f3xx_ll_qspi.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_LL_QSPI_H
#define __FM15F3XX_LL_QSPI_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"

#if defined (QSPI)


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */


/** @addtogroup QSPI_LL QSPI
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup QSPI_Exported_Types QSPI Exported Types
 * @{
 */


/**
 * @brief  QSPI Init structure definition
 */
typedef struct {
    uint32_t WorkMode;          /* Specifies the MMAP or Indirect Work mode */

    uint32_t AccessFlash;       /* Specifies the FlashA or FlashB will be used. */

    uint32_t SpeedMode;         /* Specifies the DDR or SDR mode. */

    uint32_t EndianMode;        /* Specifies the litter endian or big endian. */

    uint32_t SignificantBit;    /* Specifies the MSB or LSB of rxfifo in MMAP mode. */

    uint32_t IoMode;            /* Specifies the unused IO statue in single/dual mode.*/

    uint32_t SampleShift;       /* Specifies enable Sample Shift and Sample Shift Num.*/

    uint32_t SamplePoint;       /* Specifies enable Sample Point and Sample Point Num.*/

    uint32_t ClkMode;           /* Specifies the Clock Mode. */

    uint32_t ClkDiv;            /* Specifies the prescaler value used to divide the QSPI clock. */

    uint32_t TxfifoWm;          /* Specifies txfifo watermark. */

    uint32_t RxfifoWm;          /* Specifies rxfifo watermark. */

    uint32_t QuitStallCnt;      /* Specifies enable quit stall and the stall connter value. */
} LL_QSPI_InitTypeDef;


/**
 * @}
 */

/**
 * @brief  QSPI Command structure definition
 */
typedef struct {
    uint32_t Instruction;                       /* Specifies the Instruction to be sent
                                                   This parameter can be a value (8-bit) between 0x00 and 0xFF */
    uint32_t Address;                           /* Specifies the Address to be sent (Size from 1 to 4 bytes according AddressSize)
                                                   This parameter can be a value (32-bits) between 0x0 and 0xFFFFFFFF */
    uint32_t AddressSize;                       /* Specifies the Address Size
                                                   This parameter can be a value of @ref QSPI_AddressSize */
    uint32_t Alternate;                         /* Specifies the Alternate
                                                   This parameter can be a value of @ref QSPI_Alternate */
    uint32_t AlternateSize;                     /* Specifies the Alternate Bytes Size
                                                   This parameter can be a value of @ref QSPI_AlternateBytesSize */
    uint32_t DummyCycles;                       /* Specifies the Number of Dummy Cycles.
                                                   This parameter can be a number between 0 and 31 */
    uint32_t InstructionLines;                  /* Specifies the Instruction Mode
                                                   This parameter can be a value of @ref QSPI_InstructionMode */
    uint32_t AddressLines;                      /* Specifies the Address Mode
                                                   This parameter can be a value of @ref QSPI_AddressMode */
    uint32_t AlternateLines;                    /* Specifies the Alternate Bytes Mode
                                                   This parameter can be a value of @ref QSPI_AlternateBytesMode */
    uint32_t DataLines;                         /* Specifies the Data Mode (used for dummy cycles and data phases)
                                                   This parameter can be a value of @ref QSPI_DataMode */
    uint32_t NumData;                           /* Specifies the number of data to transfer.
                                                   This parameter can be any value between 0 and 0xFFFFFFFF (0 means undefined length
                                                   until end of memory)*/
    uint32_t TxOrRxData;                        /* Specifies rx or tx data. */
} LL_QSPI_CommandTypeDef;

/**
 * @}
 */


/** @defgroup QSPI_Exported_Constants QSPI Exported Constants
 * @{
 */


/** @defgroup LL_QSPI_FlashSel
 * @{
 */
#define LL_QSPI_ACCESS_FLASHA   QSPI_CFG_ACCESSA
#define LL_QSPI_ACCESS_FLASHB   QSPI_CFG_ACCESSB


/**
 * @}
 */


/** @defgroup LL_QSPI_Speed
 * @{
 */
#define LL_QSPI_SPEED_SDR   0x0U
#define LL_QSPI_SPEED_DDR   QSPI_CFG_DDREN


/**
 * @}
 */


/** @defgroup LL_QSPI_SingleModeDateLineSel
 * @{
 */
#define LL_QSPI_SINGLEMODE_DISABLE  0x0U
#define LL_QSPI_SINGLEMODE_ENABLE   QSPI_CFG_S2DLEN


/**
 * @}
 */


/** @defgroup LL_QSPI_TxBufferDataOrder LL_QSPI_DataOrder
 * @{
 */
#define LL_QSPI_ENDIAN_BIG      0x0U
#define LL_QSPI_ENDIAN_LITTLE   QSPI_DORDER_LITENDIAN


/**
 * @}
 */


/** @defgroup LL_QSPI_TxFiFoDataOrder LL_QSPI_RxFiFoDataOrder
 * @{
 */
#define LL_QSPI_RxFIFO_LSB  0x0U
#define LL_QSPI_RxFIFO_MSB  QSPI_DORDER_RXFIFOMMAP


/**
 * @}
 */


/** @defgroup LL_QSPI_SetSTROBESample
 * @{
 */
#define LL_QSPI_STROBE_DISABLE  0x0U
#define LL_QSPI_STROBE_ENABLE   QSPI_MODE_STROBEMEN


/**
 * @}
 */


/** @defgroup  LL_QSPI_MMAPMode
 * @{
 */
#define LL_QSPI_WORK_INDIRECT   0x0U
#define LL_QSPI_WORK_MMAP       QSPI_MODE_MMAPMEN


/**
 * @}
 */


/** @defgroup LL_QSPI_IOMode
 * @{
 */
#define LL_QSPI_IO_HIGH 0x0U
#define LL_QSPI_IO_SYNC QSPI_MODE_IOMODE


/**
 * @}
 */


/** @defgroup LL_QSPI_IOMode
 * @{
 */
#define LL_QSPI_LINE_NONE     (0x0U)
#define LL_QSPI_LINE_SINGLE (0x1U)
#define LL_QSPI_LINE_DUAL     (0x2U)
#define LL_QSPI_LINE_QUAD     (0x3U)


/**
 * @}
 */


/** @defgroup DummyCycle
 * @{
 */
#define LL_QSPI_DUMMY_NONE  (0x0U << QSPI_IOMODE_DUMMYCYC_Pos)
#define LL_QSPI_DUMMY_1       (0x1U << QSPI_IOMODE_DUMMYCYC_Pos)
#define LL_QSPI_DUMMY_2       (0x2U << QSPI_IOMODE_DUMMYCYC_Pos)
#define LL_QSPI_DUMMY_3       (0x3U << QSPI_IOMODE_DUMMYCYC_Pos)


/**
 * @}
 */


/** @defgroup send byte
 * @{
 */
#define LL_QSPI_BYTE_1  (0x0U)
#define LL_QSPI_BYTE_2  (0x1U)
#define LL_QSPI_BYTE_3  (0x2U)
#define LL_QSPI_BYTE_4  (0x3U)


/**
 * @}
 */


/** @defgroup LL_QSPI_SetSendOrRec
 * @{
 */
#define LL_QSPI_INDIRECT_TxDATA QSPI_CMD_RDNWRCMDREG
#define LL_QSPI_INDIRECT_RxDATA 0x0U


/**
 * @}
 */


/** @defgroup LL_QSPI_GetRxfifoStatus
 * @{
 */
#define LL_QSPI_FLAG_RxFIFO_FULL          QSPI_RXFIFOS_FULL
#define LL_QSPI_FLAG_RxFIFO_MMAPFULL    QSPI_RXFIFOS_MMAPFULL
#define LL_QSPI_FLAG_RxFIFO_NEARFULL    QSPI_RXFIFOS_NEARFULL
#define LL_QSPI_FLAG_RxFIFO_CNT           QSPI_RXFIFOS_CNTS


/**
 * @}
 */


/** @defgroup LL_QSPI_GetRxfifoStatus
 * @{
 */
#define LL_QSPI_FLAG_TxFIFO_FULL          QSPI_TXFIFOS_FULL
#define LL_QSPI_FLAG_TxFIFO_EMPTY         QSPI_TXFIFOS_EMPTY
#define LL_QSPI_FLAG_TxFIFO_NEAREMPTY   QSPI_TXFIFOS_NEMPTY
#define LL_QSPI_FLAG_TxFIFO_CNT           QSPI_TXFIFOS_CNTS


/**
 * @}
 */


/** @defgroup LL_QSPI_ClrStatus
 * @{
 */
#define LL_QSPI_FLAG_CLEAR_BUFFER0      QSPI_SOFTCLR_BUFFER0
#define LL_QSPI_FLAG_CLEAR_BUFFER1      QSPI_SOFTCLR_BUFFER1
#define LL_QSPI_FLAG_CLEAR_TxFIFO         QSPI_SOFTCLR_TXFIFO
#define LL_QSPI_FLAG_CLEAR_RxFIFO         QSPI_SOFTCLR_RXFIFO
#define LL_QSPI_FLAG_CLEAR_TRANSABORT   QSPI_SOFTCLR_TRANABORT
#define LL_QSPI_FLAG_CLEAR_ALL            (0x4FU)


/**
 * @}
 */


/** @defgroup LL_QSPI_SetSamplePoint
 * @{
 */
#define LL_QSPI_SAMPLEPOINT_DISABLE     0x0U
#define LL_QSPI_SAMPLEPOINT_1_RISING    (QSPI_SAMPCFG_POINTEN | (0x0U << QSPI_SAMPCFG_POINTNUM_Pos) )
#define LL_QSPI_SAMPLEPOINT_1_FALLING   (QSPI_SAMPCFG_POINTEN | (0x1U << QSPI_SAMPCFG_POINTNUM_Pos) )
#define LL_QSPI_SAMPLEPOINT_2_RISING    (QSPI_SAMPCFG_POINTEN | (0x2U << QSPI_SAMPCFG_POINTNUM_Pos) )
#define LL_QSPI_SAMPLEPOINT_2_FALLING   (QSPI_SAMPCFG_POINTEN | (0x3U << QSPI_SAMPCFG_POINTNUM_Pos) )


/**
 * @}
 */


/** @defgroup LL_QSPI_SetSampleShift
 * @{
 */
#define LL_QSPI_SAMPLESHIFT_NONE                 0x0U
#define LL_QSPI_SAMPLESHIFT_HALFCYCLE         (QSPI_SAMPCFG_SHIFTEN | (0x0U << QSPI_SAMPCFG_SHIFTNUM_Pos) )
#define LL_QSPI_SAMPLESHIFT_ONECYCLE          (QSPI_SAMPCFG_SHIFTEN | (0x1U << QSPI_SAMPCFG_SHIFTNUM_Pos) )
#define LL_QSPI_SAMPLESHIFT_ONEHALFCYCLE    (QSPI_SAMPCFG_SHIFTEN | (0x2U << QSPI_SAMPCFG_SHIFTNUM_Pos) )
#define LL_QSPI_SAMPLESHIFT_TWOCYCLE          (QSPI_SAMPCFG_SHIFTEN | (0x3U << QSPI_SAMPCFG_SHIFTNUM_Pos) )


/**
 * @}
 */


/** @defgroup LL_QSPI_ClkMode
 * @{
 */
#define LL_QSPI_CLK_MODE0   0x0U
#define LL_QSPI_CLK_MODE3   QSPI_CLKCFG_CLKMODE


/**
 * @}
 */


/** @defgroup LL_QSPI_ClkDiv
 * @{
 */
#define LL_QSPI_CLK_DIV2    (0x0U << QSPI_CLKCFG_CLKDIV_Pos)
#define LL_QSPI_CLK_DIV4    (0x1U << QSPI_CLKCFG_CLKDIV_Pos)
#define LL_QSPI_CLK_DIV8    (0x2U << QSPI_CLKCFG_CLKDIV_Pos)
#define LL_QSPI_CLK_DIV16   (0x3U << QSPI_CLKCFG_CLKDIV_Pos)
#define LL_QSPI_CLK_DIV32   (0x4U << QSPI_CLKCFG_CLKDIV_Pos)
#define LL_QSPI_CLK_DIV64   (0x5U << QSPI_CLKCFG_CLKDIV_Pos)


/**
 * @}
 */


/** @defgroup LL_QSPI_STALL
 * @{
 */
#define LL_QSPI_STALL_QUITCNT_DISABLE   0x0U
#define LL_QSPI_STALL_QUITCNT_ENABLE    QSPI_ABORTCFG_CSTALLEN


/**
 * @}
 */


/** @defgroup LL_QSPI_GetStatus
 * @{
 */
#define LL_QSPI_FLAG_TRANSOVER  QSPI_STATUS_TRANSOVER
#define LL_QSPI_FLAG_STALL        QSPI_STATUS_STALLSTAT
#define LL_QSPI_FLAG_TRANSABORT QSPI_STATUS_TRANSABORT


/**
 * @}
 */


/** @defgroup LL_QSPI_INTEnable
 * @{
 */
#define LL_QSPI_INT_TRANSOVER         QSPI_INTEN_TRANSOVER
#define LL_QSPI_INT_STALL               QSPI_INTEN_STALL
#define LL_QSPI_INT_ABORT               QSPI_INTEN_ABORT
#define LL_QSPI_INT_TxFIFONEMPTY    QSPI_INTEN_TFIFONEMPTY
#define LL_QSPI_INT_TxFIFOEMPTY     QSPI_INTEN_TXFIFOEMPTY
#define LL_QSPI_INT_RxFIFONFULL     QSPI_INTEN_RXFIFONFULL
#define LL_QSPI_INT_RxFIFOFULL      QSPI_INTEN_RXFIFOFULL
#define LL_QSPI_DMA_TxFIFONEMPTY    (QSPI_INTEN_TFIFONEMPTY|(QSPI_INTEN_TFIFONEMPTY << 12))
#define LL_QSPI_DMA_TxFIFOEMPTY     (QSPI_INTEN_TXFIFOEMPTY|(QSPI_INTEN_TXFIFOEMPTY << 12))
#define LL_QSPI_DMA_RxFIFONFULL     (QSPI_INTEN_RXFIFONFULL|(QSPI_INTEN_RXFIFONFULL << 12))
#define LL_QSPI_DMA_RxFIFOFULL      (QSPI_INTEN_RXFIFOFULL|(QSPI_INTEN_RXFIFOFULL << 12))

/**
 * @}
 */


/**
 * @}
 */

/* Exported macros -----------------------------------------------------------*/


/** @defgroup QSPI_Exported_Macros QSPI Exported Macros
 * @{
 */

/*************************************************************CFG REG*************************************************/


/** @brief  Enable the QSPI peripheral.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_Enable(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->CFG, QSPI_CFG_EN);
}


/** @brief  Disable the QSPI peripheral.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_Disable(QSPI_TypeDef *QSPIx)
{
    CLEAR_BIT(QSPIx->CFG, QSPI_CFG_EN);
}


/** @brief  Select the QSPI Access Flash.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_ACCESS_FLASHA
 *         @arg @ref LL_QSPI_ACCESS_FLASHB
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetAccessFlash(QSPI_TypeDef *QSPIx, uint32_t flash)
{
    MODIFY_REG(QSPIx->CFG, QSPI_CFG_ACCESSA | QSPI_CFG_ACCESSB, flash);
}


/** @brief  the QSPI Indirect Mode Start Tx.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_StartTxIndirectMode(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->CFG, QSPI_CFG_TXSTARTREG);
}


/** @brief  Enable or disable the QSPI DDR.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetDDREn(QSPI_TypeDef *QSPIx, uint32_t en)
{
    MODIFY_REG(QSPIx->CFG, QSPI_CFG_DDREN, en);
}


/** @brief  Single Mode Date Line Select.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetSingleModeDateLine(QSPI_TypeDef *QSPIx, uint32_t rxortx)
{
    MODIFY_REG(QSPIx->CFG, QSPI_CFG_S2DLEN, rxortx);
}


/*************************************************************DORDER REG*************************************************/


/** @brief  Rx Buffer Data Order in MMAP mode.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_RxFIFO_LSB
 *         @arg @ref LL_QSPI_RxFIFO_MSB
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetRxfifoDataOrder(QSPI_TypeDef *QSPIx, uint32_t order)
{
    MODIFY_REG(QSPIx->DORDER, QSPI_DORDER_RXFIFOMMAP, order << QSPI_DORDER_RXFIFOMMAP_Pos);
}


/** @brief  Data Order.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_ENDIAN_BIG
 *         @arg @ref LL_QSPI_ENDIAN_LITTLE
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetDataOrder(QSPI_TypeDef *QSPIx, uint32_t order)
{
    MODIFY_REG(QSPIx->DORDER, QSPI_DORDER_LITENDIAN, order << QSPI_DORDER_LITENDIAN_Pos);
}


/** @brief  Enable Strobe.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 * @retval None
 */
/*************************************************************MODE REG*************************************************/
__STATIC_INLINE void LL_QSPI_EnableStrobeMode(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->MODE, QSPI_MODE_STROBEMEN);
}


/** @brief  Disable Strobe.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_DisableStrobeMode(QSPI_TypeDef *QSPIx)
{
    CLEAR_BIT(QSPIx->MODE, QSPI_MODE_STROBEMEN);
}


/** @brief  Enable or disable MMAP mode.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetMMAPEn(QSPI_TypeDef *QSPIx, uint32_t en)
{
    MODIFY_REG(QSPIx->MODE, QSPI_MODE_MMAPMEN, en);
}


/** @brief  set unused IO statue in single/dual mode.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_IOMODE_OUTPUT
 *         @arg @ref LL_QSPI_IOMODE_SYNC //keep in the same direction with other IO
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetIOMode(QSPI_TypeDef *QSPIx, uint32_t mode)
{
    MODIFY_REG(QSPIx->MODE, QSPI_MODE_IOMODE, mode << QSPI_MODE_IOMODE_Pos);
}


/*************************************************************IOMODE REG*************************************************/


/** @brief  cmd IO num.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_LINE_NONE
 *         @arg @ref LL_QSPI_LINE_1
 *         @arg @ref LL_QSPI_LINE_2
 *         @arg @ref LL_QSPI_LINE_4
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetCmdIOMode(QSPI_TypeDef *QSPIx, uint32_t mode)
{
    MODIFY_REG(QSPIx->IOMODE, QSPI_IOMODE_CMDIOM, mode << QSPI_IOMODE_CMDIOM_Pos);
}


/** @brief  address IO num.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_LINE_NONE
 *         @arg @ref LL_QSPI_LINE_1
 *         @arg @ref LL_QSPI_LINE_2
 *         @arg @ref LL_QSPI_LINE_4
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetAddrIOMode(QSPI_TypeDef *QSPIx, uint32_t mode)
{
    MODIFY_REG(QSPIx->IOMODE, QSPI_IOMODE_ADDRIOM, mode << QSPI_IOMODE_ADDRIOM_Pos);
}


/** @brief  ALT IO num.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_LINE_NONE
 *         @arg @ref LL_QSPI_LINE_1
 *         @arg @ref LL_QSPI_LINE_2
 *         @arg @ref LL_QSPI_LINE_4
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetAltIOMode(QSPI_TypeDef *QSPIx, uint32_t mode)
{
    MODIFY_REG(QSPIx->IOMODE, QSPI_IOMODE_MODEIOM, mode << QSPI_IOMODE_MODEIOM_Pos);
}


/** @brief  DATA IO num.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_LINE_NONE
 *         @arg @ref LL_QSPI_LINE_1
 *         @arg @ref LL_QSPI_LINE_2
 *         @arg @ref LL_QSPI_LINE_4
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetDataIOMode(QSPI_TypeDef *QSPIx, uint32_t mode)
{
    MODIFY_REG(QSPIx->IOMODE, QSPI_IOMODE_DATAIOM, mode << QSPI_IOMODE_DATAIOM_Pos);
}


/** @brief  dummy_cycle.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x3U
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetDummyCycle(QSPI_TypeDef *QSPIx, uint32_t num)
{
    MODIFY_REG(QSPIx->IOMODE, QSPI_IOMODE_DUMMYCYC, num << QSPI_IOMODE_DUMMYCYC_Pos);
}


/** @brief  Addr Size.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x3U
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetAddrSize(QSPI_TypeDef *QSPIx, uint32_t size)
{
    MODIFY_REG(QSPIx->IOMODE, QSPI_IOMODE_ADDRSIZE, size << QSPI_IOMODE_ADDRSIZE_Pos);
}


/** @brief  Alt Size.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x3U
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetAltSize(QSPI_TypeDef *QSPIx, uint32_t size)
{
    MODIFY_REG(QSPIx->IOMODE, QSPI_IOMODE_MODESIZE, size << QSPI_IOMODE_MODESIZE_Pos);
}


/*************************************************************SCNTTHR REG*************************************************/


/** @brief  Stall Cnt Threshold.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFFU
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetStallCntThd(QSPI_TypeDef *QSPIx, uint32_t num)
{
    WRITE_REG(QSPIx->SCNTTHR, num);
}


/*************************************************************DLEN REG*************************************************/


/** @brief  Indirect Mode send/recieve data len.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFFU
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetDataLen(QSPI_TypeDef *QSPIx, uint32_t num)
{
    WRITE_REG(QSPIx->DLEN, num);
}


/*************************************************************CMD REG*************************************************/


/** @brief  Indirect Mode Set Instruction.
 * @param  QSPI Instance
 * @param  Instruction
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetInstruction(QSPI_TypeDef *QSPIx, uint32_t instruction)
{
    MODIFY_REG(QSPIx->CMD, QSPI_CMD_INSTRUCT, instruction);
}


/** @brief  Indirect Mode Set Instruction.
 * @param  QSPI Instance
 * @param  Instruction
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetSendOrRecv(QSPI_TypeDef *QSPIx, uint32_t mode)
{
    MODIFY_REG(QSPIx->CMD, QSPI_CMD_RDNWRCMDREG, mode << QSPI_CMD_RDNWRCMDREG_Pos);
}


/*************************************************************FADDR REG*************************************************/


/** @brief  set address send for flash.
 * @param  QSPI Instance
 * @param  Instruction
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetFlashAddr(QSPI_TypeDef *QSPIx, uint32_t addr)
{
    WRITE_REG(QSPIx->FADDR, addr);
}


/*************************************************************MODEALT REG*************************************************/


/** @brief  set ALT byte .
 * @param  QSPI Instance
 * @param  Instruction
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetAltByte(QSPI_TypeDef *QSPIx, uint32_t alt)
{
    WRITE_REG(QSPIx->MODEALT, alt);
}


/*************************************************************RXFIFOD REG*************************************************/


/** @brief  read rxfifo data.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE uint8_t LL_QSPI_ReceiveData(QSPI_TypeDef *QSPIx)
{
    return ((uint8_t) READ_REG(QSPIx->RXFIFOD));
}


/*************************************************************RXFIFOCFG REG*************************************************/


/** @brief  set rxfifo enable.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_EnableRxfifo(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->RXFIFOCFG, QSPI_RXFIFOCFG_EN);
}


/** @brief  set rxfifo disable.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_DisableRxfifo(QSPI_TypeDef *QSPIx)
{
    CLEAR_BIT(QSPIx->RXFIFOCFG, QSPI_RXFIFOCFG_EN);
}


/** @brief  set quad spi mode rxfifo wm.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x3FU
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetQuadRxfifoWM(QSPI_TypeDef *QSPIx, uint32_t num)
{
    MODIFY_REG(QSPIx->RXFIFOCFG, QSPI_RXFIFOCFG_QUADWM, num << QSPI_RXFIFOCFG_QUADWM_Pos);
}


/** @brief  set norm spi mode rxfifo wm.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x3FU
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetNormRxfifoWM(QSPI_TypeDef *QSPIx, uint32_t num)
{
    MODIFY_REG(QSPIx->RXFIFOCFG, QSPI_RXFIFOCFG_NORMWM, num << QSPI_RXFIFOCFG_NORMWM_Pos);
}


/*************************************************************RXFIFOCFG REG*************************************************/


/**
 * @brief  read rxfifo status Flag.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_FLAG_RxFIFOS_FULL
 *         @arg @ref LL_QSPI_FLAG_RxFIFOS_MMAPFUL
 *         @arg @ref LL_QSPI_FLAG_RxFIFOS_NEARFUL
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_QSPI_GetStatus_Rxfifo(QSPI_TypeDef *QSPIx, uint32_t flag)
{
    return (READ_BIT(QSPIx->RXFIFOS, flag));
}


/**
 * @brief  get rxfifo Unread Bytes.
 * @param  QSPI Instance
 * @retval null.
 */
__STATIC_INLINE uint32_t LL_QSPI_GetLengthRxfifo(QSPI_TypeDef *QSPIx)
{
    return (READ_BIT(QSPIx->RXFIFOS, QSPI_RXFIFOS_CNTS) >> 3);
}


/*************************************************************TXFIFOD REG*************************************************/


/** @brief  read txfifo data.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_TransmitData(QSPI_TypeDef *QSPIx, uint8_t Value)
{
    QSPIx->TXFIFOD = Value;
}


/*************************************************************TXFIFOCFG REG*************************************************/


/** @brief  set txfifo enable.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_EnableTxfifo(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->TXFIFOCFG, QSPI_TXFIFOCFG_EN);
}


/** @brief  set txfifo disable.
 * @param  QSPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_DisableTxfifo(QSPI_TypeDef *QSPIx)
{
    CLEAR_BIT(QSPIx->TXFIFOCFG, QSPI_TXFIFOCFG_EN);
}


/** @brief  set txfifo wm.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x1FU
 * @retval None
 */
__STATIC_INLINE void LL_QSPI_SetTxfifoWM(QSPI_TypeDef *QSPIx, uint32_t num)
{
    MODIFY_REG(QSPIx->TXFIFOCFG, QSPI_TXFIFOCFG_WM, num << QSPI_TXFIFOCFG_WM_Pos);
}


/*************************************************************TXFIFOCFG REG*************************************************/


/**
 * @brief  read rxfifo status Flag.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_FLAG_TxFIFOS_FULL
 *         @arg @ref LL_QSPI_FLAG_TxFIFOS_EMPTY
 *         @arg @ref LL_QSPI_FLAG_TxFIFOS_NEARFUL
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_QSPI_GetStatus_Txfifo(QSPI_TypeDef *QSPIx, uint32_t flag)
{
    return (READ_BIT(QSPIx->TXFIFOS, flag));
}


/**
 * @brief  get txfifo Unsend Bytes.
 * @param  QSPI Instance
 * @retval null.
 */
__STATIC_INLINE uint32_t LL_QSPI_GetLengthTxfifo(QSPI_TypeDef *QSPIx)
{
    return (READ_BIT(QSPIx->TXFIFOS, QSPI_TXFIFOS_CNTS) >> 4);
}


/*************************************************************SOFTCLR REG*************************************************/


/**
 * @brief  clear qspi status.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_CLEAR_BUFFER0
 *         @arg @ref LL_QSPI_CLEAR_BUFFER1
 *         @arg @ref LL_QSPI_CLEAR_TxFIFO
 *         @arg @ref LL_QSPI_CLEAR_RxFIFO
 *         @arg @ref LL_QSPI_CLEAR_TRANSABORT
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_ClearFlag(QSPI_TypeDef *QSPIx, uint32_t flag)
{
    SET_BIT(QSPIx->SOFTCLR, flag);
    CLEAR_BIT(QSPIx->SOFTCLR, flag);
}


/*************************************************************SOFTCLR REG*************************************************/


/**
 * @brief  clear qspi all status.
 * @param  QSPI Instance
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_ClearAllFlag(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->SOFTCLR, LL_QSPI_FLAG_CLEAR_ALL);
    CLEAR_BIT(QSPIx->SOFTCLR, LL_QSPI_FLAG_CLEAR_ALL);
}


/*************************************************************SAMPCFG REG*************************************************/


/**
 * @brief  set qspi sample point.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_SAMPLEPOINT_DISABLE
 *         @arg @ref LL_QSPI_SAMPLEPOINT_1_RISING
 *         @arg @ref LL_QSPI_SAMPLEPOINT_1_FALLING
 *         @arg @ref LL_QSPI_SAMPLEPOINT_2_RISING
 *         @arg @ref LL_QSPI_SAMPLEPOINT_2_FALLING
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_SetSamplePoint(QSPI_TypeDef *QSPIx, uint32_t point)
{
    MODIFY_REG(QSPIx->SAMPCFG, QSPI_SAMPCFG_POINTEN | QSPI_SAMPCFG_POINTNUM, point);
}


/**
 * @brief  set qspi sample shift.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_SAMPLESHIFT_DISABLE
 *         @arg @ref LL_QSPI_SAMPLESHIFT_0P5
 *         @arg @ref LL_QSPI_SAMPLESHIFT_1
 *         @arg @ref LL_QSPI_SAMPLESHIFT_1P5
 *         @arg @ref LL_QSPI_SAMPLESHIFT_2
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_SetSampleShift(QSPI_TypeDef *QSPIx, uint32_t cycle)
{
    MODIFY_REG(QSPIx->SAMPCFG, QSPI_SAMPCFG_SHIFTEN | QSPI_SAMPCFG_SHIFTNUM, cycle);
}


/*************************************************************CLKCFG REG*************************************************/


/**
 * @brief  set qspi sample shift.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_QSPI_CLK_MODE0
 *         @arg @ref LL_QSPI_CLK_MODE3
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_SetClkMode(QSPI_TypeDef *QSPIx, uint32_t mode)
{
    MODIFY_REG(QSPIx->CLKCFG, QSPI_CLKCFG_CLKMODE, mode);
}


/**
 * @brief  qspi clk div.
 * @param  QSPI Instance
 * @param  This parameter can be one of the following values:_VALUE_:0x0U~0x5U
 *         @arg @ref LL_QSPI_CLK_DIV2
 *         @arg @ref LL_QSPI_CLK_DIV4
 *         @arg @ref LL_QSPI_CLK_DIV8
 *         @arg @ref LL_QSPI_CLK_DIV16
 *         @arg @ref LL_QSPI_CLK_DIV32
 *         @arg @ref LL_QSPI_CLK_DIV64
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_SetClkDiv(QSPI_TypeDef *QSPIx, uint32_t div)
{
    MODIFY_REG(QSPIx->CLKCFG, QSPI_CLKCFG_CLKDIV, div);
}


/*************************************************************CLKCFG REG*************************************************/


/**
 * @brief  enable qspi software abort.
 * @param  QSPI Instance
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_EnableSoftwareAbort(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->ABORTCFG, QSPI_ABORTCFG_EN);
}


/**
 * @brief  disable qspi software abort.
 * @param  QSPI Instance
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_DisableSoftwareAbort(QSPI_TypeDef *QSPIx)
{
    CLEAR_BIT(QSPIx->ABORTCFG, QSPI_ABORTCFG_EN);
}


/**
 * @brief  enable qspi cnt stall.
 * @param  QSPI Instance
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_EnableCntStall(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->ABORTCFG, QSPI_ABORTCFG_CSTALLEN);
}


/**
 * @brief  disable qspi cnt stall.
 * @param  QSPI Instance
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_DisableCntStall(QSPI_TypeDef *QSPIx)
{
    CLEAR_BIT(QSPIx->ABORTCFG, QSPI_ABORTCFG_CSTALLEN);
}


/**
 * @brief  software produce abort singal.
 * @param  QSPI Instance
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_SoftwareAbort(QSPI_TypeDef *QSPIx)
{
    SET_BIT(QSPIx->ABORTCFG, QSPI_ABORTCFG_ABORT);
}


/*************************************************************STATUS REG*************************************************/


/**
 * @brief  get qspi status.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_FLAG_TRANSOVER
 *         @arg @ref LL_QSPI_FLAG_STALLSTAT
 *         @arg @ref LL_QSPI_FLAG_TRANSABORT
 * @retval null
 */
__STATIC_INLINE uint32_t LL_QSPI_GetStatus(QSPI_TypeDef *QSPIx, uint32_t flag)
{
    return (READ_BIT(QSPIx->STATUS, flag));
}


/*************************************************************INTEN REG*************************************************/


/**
 * @brief  enable qspi int.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_INT_TRANSOVER
 *         @arg @ref LL_QSPI_INT_STALL
 *         @arg @ref LL_QSPI_INT_ABORT
 *         @arg @ref LL_QSPI_INT_TxFIFONEMPTY
 *         @arg @ref LL_QSPI_INT_TxFIFOEMPTY
 *         @arg @ref LL_QSPI_INT_RxFIFONFULL
 *         @arg @ref LL_QSPI_INT_RxFIFOFULL
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_EnableINT(QSPI_TypeDef *QSPIx, uint32_t inten_xx)
{
    SET_BIT(QSPIx->INTEN, inten_xx);
}

/**
 * @brief  enable qspi int.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_DMA_TxFIFONEMPTY
 *         @arg @ref LL_QSPI_DMA_TxFIFOEMPTY
 *         @arg @ref LL_QSPI_DMA_RxFIFONFULL
 *         @arg @ref LL_QSPI_DMA_RxFIFOFULL
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_EnableDMA(QSPI_TypeDef *QSPIx, uint32_t inten_xx)
{
    SET_BIT(QSPIx->INTEN, inten_xx);
}

/**
 * @brief  disable qspi int.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_INT_TRANSOVER
 *         @arg @ref LL_QSPI_INT_STALL
 *         @arg @ref LL_QSPI_INT_ABORT
 *         @arg @ref LL_QSPI_INT_TFIFONEMPTY
 *         @arg @ref LL_QSPI_INT_TxFIFOEMPTY
 *         @arg @ref LL_QSPI_INT_RxFIFONFULL
 *         @arg @ref LL_QSPI_INT_RxFIFOFULL
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_DisableINT(QSPI_TypeDef *QSPIx, uint32_t inten_xx)
{
    CLEAR_BIT(QSPIx->INTEN, inten_xx);
}

/**
 * @brief  enable qspi int.
 * @param  QSPI Instance
 * @param  flag
 *         @arg @ref LL_QSPI_DMA_TxFIFONEMPTY
 *         @arg @ref LL_QSPI_DMA_TxFIFOEMPTY
 *         @arg @ref LL_QSPI_DMA_RxFIFONFULL
 *         @arg @ref LL_QSPI_DMA_RxFIFOFULL
 * @retval null
 */
__STATIC_INLINE void LL_QSPI_DisableDMA(QSPI_TypeDef *QSPIx, uint32_t inten_xx)
{
    CLEAR_BIT(QSPIx->INTEN, inten_xx);
}
/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup QSPI_Exported_Functions
 * @{
 */
/* Initialization/de-initialization functions  ********************************/
void LL_QSPI_StructInit(LL_QSPI_InitTypeDef *QSPIInitStruct);
void LL_QSPI_Init(QSPI_TypeDef *QSPIx, LL_QSPI_InitTypeDef* QSPI_InitStruct);
void LL_QSPI_Command(LL_QSPI_CommandTypeDef *cmd);
void LL_QSPI_Transmit(uint8_t *pData);
void LL_QSPI_Transmit_DMA(uint8_t DmaChannel, uint8_t* pData);
void LL_QSPI_Receive(uint8_t *pData);
void LL_QSPI_Receive_DMA(uint8_t DmaChannel, uint8_t* pData, uint8_t RxAlign);
/**
 * @}
 */
/* End of exported functions -------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/
/* End of private macros -----------------------------------------------------*/


/**
 * @}
 */


/**
 * @}
 */
#endif  /* defined (QSPI) */

#ifdef __cplusplus
}
#endif

#endif  /* __FM15F3XX_LL_QSPI_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
