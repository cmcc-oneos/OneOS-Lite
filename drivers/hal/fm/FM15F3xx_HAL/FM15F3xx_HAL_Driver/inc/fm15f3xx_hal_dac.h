/**
 ******************************************************************************
 * @file    fm15f3xx_hal_dac.h
 * @author  WYL
 * @brief   Header file of DAC HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_DAC_H
#define __FM15F3xx_HAL_DAC_H

#ifdef __cplusplus
extern "C" {
#endif


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_dac.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup DAC
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup DAC_Exported_Types DAC Exported Types
 * @{
 */


/**
 * @brief HAL State structures definition
 */
typedef enum {
    HAL_DAC_STATE_RESET     = 0x00U,    /*!< DAC not yet initialized or disabled  */
    HAL_DAC_STATE_READY     = 0x01U,    /*!< DAC initialized and ready for use    */
    HAL_DAC_STATE_BUSY      = 0x02U,    /*!< DAC internal processing is ongoing   */
    HAL_DAC_STATE_TIMEOUT   = 0x03U,    /*!< DAC timeout state                    */
    HAL_DAC_STATE_ERROR     = 0x04U     /*!< DAC error state                      */
} HAL_DAC_StateTypeDef;


/**
 * @brief  DAC Init structures definition
 */
typedef struct {
    uint32_t VrefSel;                   /* Specifies refrence select. dac_cfg reg. */

    uint32_t PowerMode;                 /* Specifies high or low power mode. dac_cfg reg. */

    uint32_t WorkMode;                  /* Specifies work mode:single,up,swing,RUF. */

    uint32_t TrigMode;                  /* Specifies scan or trigger mode. dac_work_cfg reg. */

    uint32_t ScanMode;                  /* Specifies scan loop continiualy or only onetime. dac_work_cfg reg. */

    uint32_t ScanIntervalCnt;           /* Specifies delay cycle(n sys_clk) for data out to convert in scan mode. dac_work_cfg reg. */

    uint32_t TrigSource;                /* Specifies Triger source select. dac_trig_cfg reg. */

    uint32_t TrigInvEn;                 /* Enable the trigger inverter. dac_trig_cfg reg.*/

    uint32_t TrigEdge;                  /* Specifies rising or falling edge trig.  dac_trig_cfg reg. */

    uint32_t TrigFilter;                /* Specifies trigger filter clock cycle; dac_trig_cfg reg. */
} DAC_InitTypeDef;


/**
 * @brief DAC handle Structure definition
 */
typedef struct {
    DAC_TypeDef *Instance;              /*!< Register base address             */

    DAC_InitTypeDef Init;               /*!< DAC Init structures               */

    __IO HAL_DAC_StateTypeDef State;    /*!< DAC communication state           */

//  DMA_HandleTypeDef           *DMA_Handle;  /*!< Pointer DMA handler              */

    __IO uint32_t ErrorCode;            /*!< DAC Error code                    */
} DAC_HandleTypeDef;


/* Exported constants --------------------------------------------------------*/


/** @defgroup DAC_Exported_Constants DAC Exported Constants
 * @{
 */


/** @defgroup DAC_Error_Code DAC Error Code
 * @{
 */
#define  HAL_DAC_ERROR_NONE 0x00U                                   /*!< No error                          */
#define  HAL_DAC_ERROR_DMA  0x04U                                   /*!< DMA error                         */


/**
 * @}
 */


/** @defgroup DAC VREF refrence select
 * @{
 */
#define DAC_VREF_1P2V   LL_DAC_VREFSEL_1P2V                         /* DAC Vrefrence select Vref12 */
#define DAC_VREF_VDDA   LL_DAC_VREFSEL_VDDA                         /* DAC Vrefrence select VDDA */


/**
 * @}
 */


/** @defgroup DAC Power Mode
 * @{
 */
#define DAC_POWERMODE_LOW   LL_DAC_HIGHPOWER_DISABLE                /* normal mode   */
#define DAC_POWERMODE_HIGH  LL_DAC_HIGHPOWER_ENABLE                 /* high power mode  */


/**
 * @}
 */


/** @defgroup DAC work Mode pointer mode
 * @{
 */
#define DAC_WORKMODE_FIX    LL_DAC_POINTERMODE_FIX                  /* fix mode   */
#define DAC_WORKMODE_UP     LL_DAC_POINTERMODE_UP                   /* up mode  */
#define DAC_WORKMODE_SWING  LL_DAC_POINTERMODE_SWING                /* swing mode  */


/**
 * @}
 */


/** @defgroup DAC Trigger mode
 * @{
 */
#define DAC_TRIGMODE_SINGLE LL_DAC_SCAN_TRIG                        /* DAC work in trig mode  */
#define DAC_TRIGMODE_SCAN   LL_DAC_SCAN_ENABLE                      /* DAC work in scan mode  */


/**
 * @}
 */


/** @defgroup DAC Scan mode
 * @{
 */
#define DAC_SCANMODE_CONTINUE   LL_DAC_SCANMODE_CONTINUE            /* scan loop continually run   */
#define DAC_SCANMODE_ONESHOT    LL_DAC_SCANMODE_ONESHOT             /* scan loop only run onetime */


/**
 * @}
 */


/** @defgroup DAC trigger selection
 * @{
 */
#define DAC_TRIGSRC_TRGIN           LL_DAC_TRIGSRC_TRIGIN           /* trigger source select TRG IN PAD       */
#define DAC_TRIGSRC_CMP             LL_DAC_TRIGSRC_CMP              /* trigger source select CMP              */
#define DAC_TRIGSRC_STIMER0         LL_DAC_TRIGSRC_ST0              /* trigger source select STimer0          */
#define DAC_TRIGSRC_STIMER1         LL_DAC_TRIGSRC_ST1              /* trigger source select STimer1          */
#define DAC_TRIGSRC_STIMER2         LL_DAC_TRIGSRC_ST2              /* trigger source select STimer2          */
#define DAC_TRIGSRC_STIMER3         LL_DAC_TRIGSRC_ST3              /* trigger source select STimer3          */
#define DAC_TRIGSRC_STIMER4         LL_DAC_TRIGSRC_ST4              /* trigger source select STimer4          */
#define DAC_TRIGSRC_STIMER5         LL_DAC_TRIGSRC_ST5              /* trigger source select STimer5          */
#define DAC_TRIGSRC_LPTIMER0        LL_DAC_TRIGSRC_LPTIM0           /* trigger source select LPTimer0         */
#define DAC_TRIGSRC_STIMERTGS_CH6   LL_DAC_TRIGSRC_TGSCH6           /* trigger source select STimer TGS CH6   */
#define DAC_TRIGSRC_STIMERTGS_CH7   LL_DAC_TRIGSRC_TGSCH7           /* trigger source select STimer TGS CH7   */
#define DAC_TRIGSRC_RTCALARM_IT     LL_DAC_TRIGSRC_RTCALARM_IT      /* trigger source select RTC Alarm INT    */
#define DAC_TRIGSRC_TAMPERALARM_IT  LL_DAC_TRIGSRC_TAMPERALARM_IT   /* trigger source select Tamper Alarm INT */
#define DAC_TRIGSRC_RTC1S_IT        LL_DAC_TRIGSRC_RTC1S_IT         /* trigger source select RTC Seconds INT  */
#define DAC_TRIGSRC_SOFTWARE        LL_DAC_TRIGSRC_SOFTWARE         /* trigger source select Software         */


/**
 * @}
 */


/** @defgroup DAC trigger via inverter
 * @{
 */
#define DAC_TRIGINV_DISABLE LL_DAC_TRIGINV_DISABLE                  /* normal trig  */
#define DAC_TRIGINV_ENABLE  LL_DAC_TRIGINV_ENABLE                   /* invert trig  */


/**
 * @}
 */


/** @defgroup DAC_LL_TrigFilt set filter cycle
 * @{
 */
#define DAC_TRIGFILTER_NoFILTER LL_DAC_TRIGFILTER_NOFILTER          /* not filt  */
#define DAC_TRIGFILTER_1CYCLE   LL_DAC_TRIGFILTER_1CYCLE            /* 1 cycle filter  */
#define DAC_TRIGFILTER_2CYCLE   LL_DAC_TRIGFILTER_2CYCLE            /* 2 cycle filter  */
#define DAC_TRIGFILTER_3CYCLE   LL_DAC_TRIGFILTER_3CYCLE            /* 3 cycle filter  */
#define DAC_TRIGFILTER_4CYCLE   LL_DAC_TRIGFILTER_4CYCLE            /* 4 cycle filter  */
#define DAC_TRIGFILTER_5CYCLE   LL_DAC_TRIGFILTER_5CYCLE            /* 5 cycle filter  */
#define DAC_TRIGFILTER_6CYCLE   LL_DAC_TRIGFILTER_6CYCLE            /* 6 cycle filter  */
#define DAC_TRIGFILTER_7CYCLE   LL_DAC_TRIGFILTER_7CYCLE            /* 7 cycle filter  */
#define DAC_TRIGFILTER_8CYCLE   LL_DAC_TRIGFILTER_8CYCLE            /* 8 cycle filter  */
#define DAC_TRIGFILTER_9CYCLE   LL_DAC_TRIGFILTER_9CYCLE            /* 9 cycle filter  */
#define DAC_TRIGFILTER_10CYCLE  LL_DAC_TRIGFILTER_10CYCLE           /* 10 cycle filter  */
#define DAC_TRIGFILTER_11CYCLE  LL_DAC_TRIGFILTER_11CYCLE           /* 11 cycle filter  */
#define DAC_TRIGFILTER_12CYCLE  LL_DAC_TRIGFILTER_12CYCLE           /* 12 cycle filter  */
#define DAC_TRIGFILTER_13CYCLE  LL_DAC_TRIGFILTER_13CYCLE           /* 13 cycle filter  */
#define DAC_TRIGFILTER_14CYCLE  LL_DAC_TRIGFILTER_14CYCLE           /* 14 cycle filter  */
#define DAC_TRIGFILTER_15CYCLE  LL_DAC_TRIGFILTER_15CYCLE           /* 15 cycle filter  */


/**
 * @}
 */


/** @defgroup DAC_LL_TrigEdge Level/rising or falling edge trig
 * @{
 */
#define DAC_TRIGEDGE_HIGH       LL_DAC_TRIGEDGE_HIGH                /* High Level trig  */
#define DAC_TRIGEDGE_RISING     LL_DAC_TRIGEDGE_RISING              /* Rising Edge trig  */
#define DAC_TRIGEDGE_FALLING    LL_DAC_TRIGEDGE_FALLING             /* Falling Edge trig  */
#define DAC_TRIGEDGE_BOTHEDGE   LL_DAC_TRIGEDGE_BOTHEDGE            /* rising or falling edge trig  */


/**
 * @}
 */


/** @defgroup DAC Flags Definition
 * @{
 */
#define DAC_FLAG_WATERMARK      (0x04U)
#define DAC_FLAG_TOP            (0x02U)
#define DAC_FLAG_BOTTOM         (0x01U)
#define DAC_FLAG_WATERMARK_IT   (0x04U)
#define DAC_FLAG_TOP_IT         (0x02U)
#define DAC_FLAG_BOTTOM_IT      (0x01U)


/**
 * @}
 */


/** @defgroup DAC IT Definition
 * @{
 */
#define DAC_IT_BOTTOM       DAC_INTEN_BTMIDE
#define DAC_IT_TOP          DAC_INTEN_TOPIDE
#define DAC_IT_WATERMARK    DAC_INTEN_WMKIDE
#define DAC_DMA_BOTTOM      (DAC_INTEN_BTMIDE | DAC_INTEN_BTMDMAS)
#define DAC_DMA_TOP         (DAC_INTEN_TOPIDE | DAC_INTEN_TOPDMAS)
#define DAC_DMA_WATERMARK   (DAC_INTEN_WMKIDE | DAC_INTEN_WMKDMAS)


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup DAC_Exported_Macros DAC Exported Macros
 * @{
 */


/** @brief Reset DAC handle state
 * @param  __HANDLE__ specifies the DAC handle.
 * @retval None
 */
#define __HAL_DAC_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_DAC_STATE_RESET)


/** @brief Enable the DAC
 * @param  __HANDLE__ specifies the DAC handle.
 * @retval None
 */
#define __HAL_DAC_ENABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->CTRL = 0x01)


/** @brief Disable the DAC
 * @param  __HANDLE__ specifies the DAC
 * @retval None
 */
#define __HAL_DAC_DISABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->CTRL = 0x00)


/** @brief Software trigger the DAC
 * @param  __HANDLE__ specifies the DAC handle
 * @retval None
 */
#define __HAL_DAC_SOFTWARETRIG( __HANDLE__ ) SET_BIT( (__HANDLE__)->Instance->SWTRIG, DAC_SWTRIG_CTRL )


/** @brief Enable the DAC interrupt
 * @param  __HANDLE__ specifies the DAC handle
 * @param  __INTERRUPT__ specifies the DAC interrupt.
 * @retval None
 */
#define __HAL_DAC_ENABLE_IT( __HANDLE__, __INTERRUPT__ ) ( (__HANDLE__)->Instance->INTEN)   &= ~( (__INTERRUPT__) | ( (__INTERRUPT__) << 3) ); \
    ( (__HANDLE__)->Instance->INTEN)                                                        |= (__INTERRUPT__)


/** @brief Disable the DAC interrupt
 * @param  __HANDLE__ specifies the DAC handle
 * @param  __INTERRUPT__ specifies the DAC interrupt.
 * @retval None
 */
#define __HAL_DAC_DISABLE_IT( __HANDLE__, __INTERRUPT__ ) ( ( (__HANDLE__)->Instance->INTEN) &= ~(__INTERRUPT__) )


/** @brief  Get the selected DAC's flag status.
 * @param  __HANDLE__ specifies the DAC handle.
 * @param  __FLAG__ specifies the flag to clear.
 * @retval None
 */
#define __HAL_DAC_GET_FLAG( __HANDLE__, __FLAG__ ) ( ( ( (__HANDLE__)->Instance->FLAG) & (__FLAG__) ) == (__FLAG__) )


/** @brief  Checks if the specified DAC interrupt Flag is set.
 * @param __HANDLE__ DAC handle
 * @param __INTERRUPT__ DAC interrupt flag to check
 * @retval State of interruption (SET or RESET)
 */
#define __HAL_DAC_GET_IT_FLAG( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->INT & (__FLAG__) ) == (__FLAG__) )


/** @brief  Clear the DAC's interrupt flag.
 * @param  __HANDLE__ specifies the DAC handle.
 * @param  __FLAG__ specifies the flag to clear.
 * @retval None
 */
#define __HAL_DAC_CLEAR_IT_FLAG( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->IFCL) = (__FLAG__) )


/**
 * @brief  ENABLE VREF 1.2V
 * @retval None
 */
#define __HAL_DAC_ENABLE_VREF12() MODIFY_REG( VREF->CTRL, VREF_CTRL_VREFEN_Msk | VREF_CTRL_LDOEN_Msk, VREF_CTRL_VREFEN | VREF_CTRL_LDOEN )


/**
 * @brief  DISABLE VREF 1.2V
 * @retval None
 */
#define __HAL_DAC_DISABLE_VREF12() CLEAR_BIT( VREF->CTRL, VREF_CTRL_VREFEN | VREF_CTRL_LDOEN )


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup DAC_Exported_Functions
 * @{
 */


/** @addtogroup DAC_Exported_Functions_Group1
 * @{
 */
/* Initialization/de-initialization functions *********************************/
HAL_StatusTypeDef HAL_DAC_Init(DAC_HandleTypeDef* hdac);


HAL_StatusTypeDef HAL_DAC_DeInit(DAC_HandleTypeDef* hdac);


void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac);


void HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac);


/**
 * @}
 */


/** @addtogroup DAC_Exported_Functions_Group2
 * @{
 */
/* I/O operation functions ****************************************************/
HAL_StatusTypeDef HAL_DAC_Start(DAC_HandleTypeDef* hdac);


HAL_StatusTypeDef HAL_DAC_Stop(DAC_HandleTypeDef* hdac);


HAL_StatusTypeDef HAL_DAC_Start_IT(DAC_HandleTypeDef* hdac, uint32_t IT, uint8_t WaterMark);


HAL_StatusTypeDef HAL_DAC_Stop_IT(DAC_HandleTypeDef* hdac);


/**
 * @}
 */


/** @addtogroup DAC_Exported_Functions_Group3
 * @{
 */
/* Peripheral Control functions ***********************************************/
HAL_StatusTypeDef HAL_DAC_SetValueForSingleTrig(DAC_HandleTypeDef* hdac, uint16_t Data);


HAL_StatusTypeDef HAL_DAC_SetValue(DAC_HandleTypeDef* hdac, uint8_t pointer, uint16_t Data);


/**
 * @}
 */


/** @addtogroup DAC_Exported_Functions_Group4
 * @{
 */
/* Peripheral State functions *************************************************/
HAL_DAC_StateTypeDef HAL_DAC_GetState(DAC_HandleTypeDef* hdac);


void HAL_DAC_IRQHandler(DAC_HandleTypeDef* hdac);


uint32_t HAL_DAC_GetError(DAC_HandleTypeDef *hdac);


void HAL_DAC_ConvBottomCallback(DAC_HandleTypeDef* hdac);


void HAL_DAC_ConvTopCallback(DAC_HandleTypeDef* hdac);


void HAL_DAC_ConvWaterMarkCallback(DAC_HandleTypeDef* hdac);


/**
 * @}
 */


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/


/** @defgroup DAC_Private_Constants DAC Private Constants
 * @{
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup DAC_Private_Macros DAC Private Macros
 * @{
 */

#define IS_DAC_INSTANCE( INSTANCE )     ( (INSTANCE) == DAC0)
#define IS_DAC_VERF( VREF )             ( ( (VREF) == DAC_VREF_1P2V) || \
                                          ( (VREF) == DAC_VREF_VDDA) )
#define IS_DAC_POWERMODE( MODE )        ( ( (MODE) == DAC_POWERMODE_LOW) || \
                                          ( (MODE) == DAC_POWERMODE_HIGH) )
#define IS_DAC_WORKMODE( MODE )         ( ( (MODE) == DAC_WORKMODE_FIX) || \
                                          ( (MODE) == DAC_WORKMODE_UP) || \
                                          ( (MODE) == DAC_WORKMODE_SWING) )
#define IS_DAC_TRIGMODE( MODE )         ( ( (MODE) == DAC_TRIGMODE_SINGLE) || \
                                          ( (MODE) == DAC_TRIGMODE_SCAN) )
#define IS_DAC_SCANMODE( MODE )         ( ( (MODE) == DAC_SCANMODE_CONTINUE) || \
                                          ( (MODE) == DAC_SCANMODE_ONESHOT) )
#define IS_DAC_SCANINTERVAL( DELAY )    ( (DELAY) <= 0xFFFFU)
#define IS_DAC_TRIGSRC( TRIGGER )       ( ( (TRIGGER) == DAC_TRIGSRC_TRGIN) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_CMP) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMER0) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMER1) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMER2) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMER3) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMER4) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMER5) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_LPTIMER0) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMERTGS_CH6) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_STIMERTGS_CH7) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_RTCALARM_IT) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_TAMPERALARM_IT) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_RTC1S_IT) || \
                                          ( (TRIGGER) == DAC_TRIGSRC_SOFTWARE) )
#define IS_DAC_TRIGINV( EN )            ( ( (EN) == DAC_TRIGINV_DISABLE) || \
                                          ( (EN) == DAC_TRIGINV_ENABLE) )
#define IS_DAC_TRIGEDGE( MODE )         ( ( (MODE) == DAC_TRIGEDGE_HIGH) || \
                                          ( (MODE) == DAC_TRIGEDGE_RISING) || \
                                          ( (MODE) == DAC_TRIGEDGE_FALLING) || \
                                          ( (MODE) == DAC_TRIGEDGE_BOTHEDGE) )
#define IS_DAC_TRIGFILTER( CYCLE )      ( (CYCLE) <= 0xFU)


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup DAC_Private_Functions DAC Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*__FM15F3xx_HAL_DAC_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
