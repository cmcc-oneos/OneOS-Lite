/**
 ******************************************************************************
 * @file    fm15f3xx_hal_dma.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-04-24
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#ifndef __FM15F3XX_HAL_DMA_H_
#define __FM15F3XX_HAL_DMA_H_


#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_dma.h"


/** @addtogroup DMA
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup DMA_Exported_Types DMA Exported Types
 * @brief    DMA Exported Types
 * @{
 */


/**
 * @brief  HAL DMA State structures definition
 */
typedef enum {
    HAL_DMA_STATE_RESET     = 0x00U,    /*!< DMA not yet initialized or disabled */
    HAL_DMA_STATE_READY     = 0x01U,    /*!< DMA initialized and ready for use   */
    HAL_DMA_STATE_BUSY      = 0x02U,    /*!< DMA process is ongoing              */
    HAL_DMA_STATE_TIMEOUT   = 0x03U,    /*!< DMA timeout state                   */
    HAL_DMA_STATE_ERROR     = 0x04U,    /*!< DMA error state                     */
    HAL_DMA_STATE_ABORT     = 0x05U,    /*!< DMA Abort state                     */
} HAL_DMA_StateTypeDef;


/**
 * @brief   DMA TcdAttr structures
 */
typedef struct {
    uint32_t SrcAddress;                /*!< Specifies the Source Address.                 */

    uint32_t DstAddress;                /*!< Specifies the Destination Address.            */

    uint32_t SrcDataWidth;              /*!< Specifies the Source data width.                 */

    uint32_t DstDataWidth;              /*!< Specifies the Destination data width.            */

    int32_t SrcOffset;                  /*!< Specifies the Source Address Offset after every Read/Write.                 */

    int32_t DstOffset;                  /*!< Specifies the Destination Address Offset after every Read/Write.            */

    uint32_t SrcBurst;                  /*!< Specifies the Burst transfer configuration for the Source transfers.
                                             It specifies the amount of data to be transferred in a single non interruptible transaction. */

    uint32_t DstBurst;                  /*!< Specifies the Burst transfer configuration for the Destination transfers.
                                             It specifies the amount of data to be transferred in a single non interruptible transaction. */

    uint32_t Priority;                  /*!< Specifies the software priority for the DMA Channel.*/
} DmaTcd;


/**
 * @brief   DMA Tcd Minor Loop structures
 */
typedef struct {
    uint32_t BytesNum;                  /*!< Specifies the Number of Bytes are transfered in Minor Loop.              */

    uint32_t OffsetMode;                /*!< Specifies the Offset Mode of Minor Loop.                                */

    int32_t Offset;                     /*!< Specifies the Offset of Minor Loop in Address Mode.                      */
} DmaMinorLoop;


/**
 * @brief   DMA Tcd Major Loop structures
 */
typedef struct {
    uint32_t Num;                       /*!< Specifies the Number of Minor Loop.                                       */

    int16_t SrcOffset;                  /*!< Specifies the Source Offset in Major Loop.                                */

    int16_t DstOffset;                  /*!< Specifies the Destination Offset in Major Loop.                           */
} DmaMajorLoop;


/**
 * @brief   DMA structures
 */
typedef struct {
    uint8_t Channel;                    /*!< Specifies the channel used for the DMA.                                        */

    uint8_t TrigSrc;                    /*!< Specifies the software priority for the DMA Channel.*/

    DmaTcd Tcd;                         /*!< Specifies the DMA Transfer Control Descriptor.                                 */

    DmaMinorLoop MinorLoop;             /*!< Specifies the DMA Minor Loop of Transfer Control Descriptor.                   */

    DmaMajorLoop MajorLoop;             /*!< Specifies the DMA Major Loop of Transfer Control Descriptor.                   */
} DMA_InitTypeDef;


/**
 * @brief  DMA handle Structure definition
 */
typedef struct {
    DMA_TypeDef *Instance;              /*!< Register base address                  */

    DMA_InitTypeDef Init;               /*!< DMA communication parameters           */

    __IO HAL_DMA_StateTypeDef State;    /*!< DMA transfer state                     */

    __IO uint32_t ErrorCode;            /*!< DMA Error code                          */
} DMA_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup DMA_Exported_Constants DMA Exported Constants
 * @brief    DMA Exported constants
 * @{
 */


/** @defgroup DMA_Error_Code DMA Error Code
 * @brief    DMA Error Code
 * @{
 */
#define HAL_DMA_ERROR_NONE          0x00000000U     /*!< No error                               */
#define HAL_DMA_ERROR_BUS           0x00000001U     /*!< Bus error                              */
#define HAL_DMA_ERROR_SG            0x00000002U     /*!< Scatter/Gather error                   */
#define HAL_DMA_ERROR_MAJORLOOPNUM  0x00000004U     /*!< NBYTES/CITER error                     */
#define HAL_DMA_ERROR_DSTOFFSET     0x00000020U     /*!< Destination Offset error               */
#define HAL_DMA_ERROR_DSTADDR       0x00000040U     /*!< Destination Address error              */
#define HAL_DMA_ERROR_SRCOFFSET     0x00000080U     /*!< Source Offset error                    */
#define HAL_DMA_ERROR_SRCADDR       0x00000100U     /*!< Source Address error                   */
#define HAL_DMA_ERROR_PRIOR         0x00000200U     /*!< Channel Priorty                        */
#define HAL_DMA_ERROR_CANCELTX      0x00000400U     /*!< Error Cancel Transfer                  */


/**
 * @}
 */


/** @defgroup DMA_Channel_selection DMA Channel selection
 * @{
 */
#define DMA_CHANNEL_0   (0x00U)                     /*!< DMA Channel 0 */
#define DMA_CHANNEL_1   (0x01U)                     /*!< DMA Channel 1 */
#define DMA_CHANNEL_2   (0x02U)                     /*!< DMA Channel 2 */
#define DMA_CHANNEL_3   (0x03U)                     /*!< DMA Channel 3 */
#define DMA_CHANNEL_4   (0x04U)                     /*!< DMA Channel 4 */
#define DMA_CHANNEL_5   (0x05U)                     /*!< DMA Channel 5 */
#define DMA_CHANNEL_6   (0x06U)                     /*!< DMA Channel 6 */
#define DMA_CHANNEL_7   (0x07U)                     /*!< DMA Channel 7 */


/**
 * @}
 */


/** @defgroup DMA_Transfer_Width DMA Transfer Width
 * @{
 */
#define DMA_WIDTH_8BIT  (0x0U)
#define DMA_WIDTH_16BIT (0x1U)
#define DMA_WIDTH_32BIT (0x2U)


/**
 * @}
 */


/** @defgroup DMA_Transfer_BURST DMA Transfer BURST
 * @{
 */
#define DMA_BURST_SINGLE    (0x0U)
#define DMA_BURST_INC4      (0x1U)
#define DMA_BURST_INC8      (0x2U)
#define DMA_BURST_INC16     (0x3U)


/**
 * @}
 */


/** @defgroup DMA_Transfer_MODULO DMA Transfer MODULO
 * @{
 */
#define DMA_CIRCULAR_DISABLE    (0x0U)              /*!< Disable modulo */
#define DMA_CIRCULAR_2BYTES     (0x1U)              /*!< Circular buffer size is 2 bytes.   */
#define DMA_CIRCULAR_4BYTES     (0x2U)              /*!< Circular buffer size is 4 bytes.   */
#define DMA_CIRCULAR_8BYTES     (0x3U)              /*!< Circular buffer size is 8 bytes.   */
#define DMA_CIRCULAR_16BYTES    (0x4U)              /*!< Circular buffer size is 16 bytes.  */
#define DMA_CIRCULAR_32BYTES    (0x5U)              /*!< Circular buffer size is 32 bytes.  */
#define DMA_CIRCULAR_64BYTES    (0x6U)              /*!< Circular buffer size is 64 bytes.  */
#define DMA_CIRCULAR_128BYTES   (0x7U)              /*!< Circular buffer size is 128 bytes. */
#define DMA_CIRCULAR_256BYTES   (0x8U)              /*!< Circular buffer size is 256 bytes. */
#define DMA_CIRCULAR_512BYTES   (0x9U)              /*!< Circular buffer size is 512 bytes. */
#define DMA_CIRCULAR_1KBYTES    (0xAU)              /*!< Circular buffer size is 1K bytes.  */
#define DMA_CIRCULAR_2KBYTES    (0xBU)              /*!< Circular buffer size is 2K bytes.  */
#define DMA_CIRCULAR_4KBYTES    (0xCU)              /*!< Circular buffer size is 4K bytes.  */
#define DMA_CIRCULAR_8KBYTES    (0xDU)              /*!< Circular buffer size is 8K bytes.  */
#define DMA_CIRCULAR_16KBYTES   (0xEU)              /*!< Circular buffer size is 16K bytes. */
#define DMA_CIRCULAR_32KBYTES   (0xFU)              /*!< Circular buffer size is 32K bytes. */


/**
 * @}
 */


/** @defgroup DMA_Transfer_PRIORITY DMA Transfer PRIORITY
 * @{
 */
#define DMA_PRIORITY_POLLING    (0x100 | DMA_CR_ERCA)
#define DMA_PRIORITY_FIX_0      (0x0U)
#define DMA_PRIORITY_FIX_1      (0x1U)
#define DMA_PRIORITY_FIX_2      (0x2U)
#define DMA_PRIORITY_FIX_3      (0x3U)
#define DMA_PRIORITY_FIX_4      (0x4U)
#define DMA_PRIORITY_FIX_5      (0x5U)
#define DMA_PRIORITY_FIX_6      (0x6U)
#define DMA_PRIORITY_FIX_7      (0x7U)


/**
 * @}
 */


/** @defgroup DMA_MINORLOOP_OFFSET_MODE DMA MinorLoop Offst Mode
 * @{
 */
#define DMA_MINORLOOP_OFFSET_DISABLE    (0x0U)
#define DMA_MINORLOOP_OFFSET_SRC        DMA_WORD2_SMLOENBYTES
#define DMA_MINORLOOP_OFFSET_DST        DMA_WORD2_DMLOENBYTES
#define DMA_MINORLOOP_OFFSET_BOTH       (DMA_WORD2_SMLOENBYTES | DMA_WORD2_DMLOENBYTES)


/**
 * @}
 */


/** @defgroup DMA_TRIG_Source
 * @{
 */
#define DMA_TRIG_SOFTWARE           (0x00U)
#define DMA_TRIG_UART0_RX_NOEMPTY   (0x01U)
#define DMA_TRIG_UART0_TX_EMPTY     (0x02U)
#define DMA_TRIG_UART1_RX_NOEMPTY   (0x03U)
#define DMA_TRIG_UART1_TX_EMPTY     (0x04U)
#define DMA_TRIG_UART2_RX_NOEMPTY   (0x05U)
#define DMA_TRIG_UART2_TX_EMPTY     (0x06U)
#define DMA_TRIG_SPI0_RX_NOEMPTY    (0x09U)
#define DMA_TRIG_SPI0_TX_EMPTY      (0x0AU)
#define DMA_TRIG_SPI1_RX_NOEMPTY    (0x0BU)
#define DMA_TRIG_SPI1_TX_EMPTY      (0x0CU)
#define DMA_TRIG_SPI2_RX_NOEMPTY    (0x0DU)
#define DMA_TRIG_SPI2_TX_EMPTY      (0x0EU)
#define DMA_TRIG_SPI3_RX_NOEMPTY    (0x0FU)
#define DMA_TRIG_SPI3_TX_EMPTY      (0x10U)
#define DMA_TRIG_I2C0_RX_NOEMPTY    (0x11U)
#define DMA_TRIG_I2C0_TX_EMPTY      (0x12U)
#define DMA_TRIG_I2C1_RX_NOEMPTY    (0x13U)
#define DMA_TRIG_I2C1_TX_EMPTY      (0x14U)
#define DMA_TRIG_CT0_RX_NOEMPTY     (0x19U)
#define DMA_TRIG_CT0_TX_EMPTY       (0x1AU)
#define DMA_TRIG_CT1_RX_NOEMPTY     (0x1BU)
#define DMA_TRIG_CT1_TX_EMPTY       (0x1CU)
#define DMA_TRIG_QSPI_RX_NOEMPTY    (0x1DU)
#define DMA_TRIG_QSPI_TX_EMPTY      (0x1EU)
#define DMA_TRIG_FSMC_RX_NOEMPTY    (0x1FU)
#define DMA_TRIG_FSMC_TX_EMPTY      (0x20U)
#define DMA_TRIG_STIMER0_TRIG       (0x22U)
#define DMA_TRIG_STIMER1_TRIG       (0x23U)
#define DMA_TRIG_STIMER2_TRIG       (0x24U)
#define DMA_TRIG_STIMER3_TRIG       (0x25U)
#define DMA_TRIG_STIMER4_TRIG       (0x26U)
#define DMA_TRIG_STIMER5_TRIG       (0x27U)
#define DMA_TRIG_SECURTY_DMA        (0x2AU)
#define DMA_TRIG_DCMI_DMA           (0x2BU)
#define DMA_TRIG_GPIOA_EDGE         (0x2CU)
#define DMA_TRIG_GPIOB_EDGE         (0x2DU)
#define DMA_TRIG_GPIOC_EDGE         (0x2EU)
#define DMA_TRIG_GPIOD_EDGE         (0x2FU)
#define DMA_TRIG_GPIOE_EDGE         (0x30U)
#define DMA_TRIG_GPIOF_EDGE         (0x31U)
#define DMA_TRIG_PAE_FINISH         (0x32U)
#define DMA_TRIG_BCA_FINISH         (0x33U)
#define DMA_TRIG_HASH_CUR_COMPUTE   (0x34U)
#define DMA_TRIG_HASH_LAST_COMPUTE  (0x35U)
#define DMA_TRIG_ADC_FINISH         (0x36U)
#define DMA_TRIG_DAC_WATERMARK      (0x37U)
#define DMA_TRIG_DAC_TOP            (0x38U)
#define DMA_TRIG_DAC_BOTTOM         (0x39U)
#define DMA_TRIG_CMP_EDGE           (0x3AU)
#define DMA_TRIG_ALARM              (0x3BU)
#define DMA_TRIG_DMA_MUX0           (0x3CU)
#define DMA_TRIG_DMA_MUX1           (0x3DU)
#define DMA_TRIG_DMA_MUX2           (0x3EU)
#define DMA_TRIG_DMA_MUX3           (0x3FU)


/**
 * @}
 */


/** @defgroup DMA_interrupt_enable_definitions DMA interrupt enable definitions
 * @brief    DMA interrupts definition
 * @{
 */
#define DMA_IT_TC               (0x02U)
#define DMA_IT_MAJORLOOP_HT     (0x04U)
#define DMA_IT_ERR              (0x08U)
#define DMA_IT_MAJORLOOP_MASK   (DMA_WORD6_INTMAJ | DMA_WORD6_INTHALF)
#define DMA_IT_ERR_MASK         DMA_IT_ERR


/**
 * @}
 */


/** @defgroup DMA_flag_definitions DMA flag definitions
 * @brief    DMA flag definitions
 * @{
 */
#define DMA_FLAG_ERR_BUS            DMA_ES_BE
#define DMA_FLAG_ERR_SG             DMA_ES_SGE
#define DMA_FLAG_ERR_MAJORLOOPNUM   DMA_ES_NCE
#define DMA_FLAG_ERR_DSTOFFSET      DMA_ES_DOE
#define DMA_FLAG_ERR_DSTADDR        DMA_ES_DAE
#define DMA_FLAG_ERR_SRCOFFSET      DMA_ES_SOE
#define DMA_FLAG_ERR_SRCADDR        DMA_ES_SAE
#define DMA_FLAG_ERR_CHANNEL        DMA_ES_ERRCHN
#define DMA_FLAG_ERR_PRIOR          DMA_ES_CPE
#define DMA_FLAG_ERR_CANCELTX       DMA_ES_ECX
#define DMA_FLAG_ERR                DMA_ES_VLD


/**
 * @}
 */


/**
 * @brief  Enable/Disable the specified DMA Stream.
 * @param  __HANDLE__ DMA handle
 * @retval None
 */
#define __HAL_DMA_MUX_ENABLE( __HANDLE__ )  (DMAMUX->CHCFG[(__HANDLE__)->Init.Channel] |= BUSMAT_CHCFG_ENBL)
#define __HAL_DMA_MUX_DISABLE( __HANDLE__ ) (DMAMUX->CHCFG[(__HANDLE__)->Init.Channel] &= ~BUSMAT_CHCFG_ENBL)


/**
 * @brief  Enable/Disable the specified DMA interrupts.
 * @param  __HANDLE__ DMA handle
 * @param  __INTERRUPT__ DMA Int
 * @retval None
 */
#define __HAL_DMA_ENABLE_IT( __HANDLE__, __INTERRUPT__ )    ( ( (__INTERRUPT__) == DMA_IT_ERR) ? \
                                                              ( (__HANDLE__)->Instance->EEI |= (0x1 << (__HANDLE__)->Init.Channel) ) : ( (__HANDLE__)->Instance->TCD[(__HANDLE__)->Init.Channel].CSR |= ( (__INTERRUPT__) & 0x06) ) )
#define __HAL_DMA_DISABLE_IT( __HANDLE__, __INTERRUPT__ )   ( ( (__INTERRUPT__) == DMA_IT_ERR) ? \
                                                              ( (__HANDLE__)->Instance->EEI &= ~(0x1 << (__HANDLE__)->Init.Channel) ) : ( (__HANDLE__)->Instance->TCD[(__HANDLE__)->Init.Channel].CSR &= ~( (__INTERRUPT__) & 0x06) ) )


/**
 * @brief  Get/Clear the Int Flag.
 * @param  __HANDLE__ DMA handle
 * @retval None
 */
#define __HAL_DMA_GET_FLAG_IT( __HANDLE__ )     ( (__HANDLE__)->Instance->INT & (1 << (__HANDLE__)->Init.Channel) )
#define __HAL_DMA_CLEAR_FLAG_IT( __HANDLE__ )   ( (__HANDLE__)->Instance->INT &= ~(1 << (__HANDLE__)->Init.Channel) )


/**
 * @brief   Abort the specified DMA Stream.
 * @param   __HANDLE__ DMA handle
 * @retval None
 */
#define __HAL_DMA_ABORT( __HANDLE__ ) ( (__HANDLE__)->Instance->CR |= DMA_CR_CX)


/**
 * @brief  GET/CLEAR the DMA Error Flag.
 * @param  __HANDLE__ DMA handle
 * @retval None
 */
#define __HAL_DMA_GET_FLAG_ERR( __HANDLE__, __FLAG__ )  ( (__HANDLE__)->Instance->ES & (__FLAG__) )
#define __HAL_DMA_CLEAR_FLAG_ERR( __HANDLE__ )          ( (__HANDLE__)->Instance->ERR &= ~(1 << (__HANDLE__)->Init.Channel) )


/**
 * @brief  GET/CLEAR the DMA xfer Done.
 * @param  __HANDLE__ DMA handle
 * @retval None
 */
#define __HAL_DMA_GET_DONE( __HANDLE__ )    ( (__HANDLE__)->Instance->DONE & (1 << (__HANDLE__)->Init.Channel) )
#define __HAL_DMA_CLEAR_DONE( __HANDLE__ )  ( (__HANDLE__)->Instance->DONE &= ~(1 << (__HANDLE__)->Init.Channel) )


/**
 * @brief  GET/CLEAR the DMA xfer Error.
 * @param  __HANDLE__ DMA handle
 * @retval None
 */
#define __HAL_DMA_GET_ERR( __HANDLE__ )     ( (__HANDLE__)->Instance->ERR & (1 << (__HANDLE__)->Init.Channel) )
#define __HAL_DMA_CLEAR_ERR( __HANDLE__ )   ( (__HANDLE__)->Instance->ERR &= ~(1 << (__HANDLE__)->Init.Channel) )


/**
 * @brief  GET the DMA xfer Active.
 * @param  __HANDLE__ DMA handle
 * @retval None
 */
#define __HAL_DMA_GET_ACTIVE( __HANDLE__ ) ( (__HANDLE__)->Instance->ACTIVE)


HAL_StatusTypeDef HAL_DMA_Init(DMA_HandleTypeDef *hdma);


HAL_StatusTypeDef HAL_DMA_Start(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum);


HAL_StatusTypeDef HAL_DMA_Start_IT(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum, uint32_t IT);


HAL_StatusTypeDef HAL_DMA_Abort(DMA_HandleTypeDef *hdma);


void HAL_DMA_IRQHandler(DMA_HandleTypeDef *hdma);


void HAL_DMA_Error_IRQHandler(DMA_HandleTypeDef *hdma);


void HAL_DMA_XferCpltCallback(DMA_HandleTypeDef *hdma);


void HAL_DMA_XferHalfCpltCallback(DMA_HandleTypeDef *hdma);


void HAL_DMA_ErrorCallback(DMA_HandleTypeDef *hdma);


HAL_DMA_StateTypeDef HAL_DMA_GetState(DMA_HandleTypeDef *hdma);


uint32_t HAL_DMA_GetError(DMA_HandleTypeDef *hdma);


uint32_t HAL_DMA_GetMajorLoopCiter(DMA_HandleTypeDef *hdma);


HAL_StatusTypeDef HAL_DMA_Trig_SoftWare(DMA_HandleTypeDef *hdma);


/* Private macros ------------------------------------------------------------*/


/** @defgroup FSMC_Private_Macros FSMC Private Macros
 * @{
 */
#define IS_DMA_ALL_INSTANCE( INSTANCE )     ( (INSTANCE) == DMA)
#define IS_DMA_CHANNEL( CHANNEL )           ( ( (CHANNEL) == DMA_CHANNEL_0) || \
                                              ( (CHANNEL) == DMA_CHANNEL_1) || \
                                              ( (CHANNEL) == DMA_CHANNEL_2) || \
                                              ( (CHANNEL) == DMA_CHANNEL_3) || \
                                              ( (CHANNEL) == DMA_CHANNEL_4) || \
                                              ( (CHANNEL) == DMA_CHANNEL_5) || \
                                              ( (CHANNEL) == DMA_CHANNEL_6) || \
                                              ( (CHANNEL) == DMA_CHANNEL_7) )
#define IS_DMA_DATA_WIDTH( WIDTH )          ( ( (WIDTH) == DMA_WIDTH_8BIT) || \
                                              ( (WIDTH) == DMA_WIDTH_16BIT) || \
                                              ( (WIDTH) == DMA_WIDTH_32BIT) )
#define IS_DMA_OFFSET( OFFSET )             ( ( (OFFSET) >= -128) && ( (OFFSET) < 128) )
#define IS_DMA_BURST( BURST )               ( ( (BURST) == DMA_BURST_SINGLE) || \
                                              ( (BURST) == DMA_BURST_INC4) || \
                                              ( (BURST) == DMA_BURST_INC8) || \
                                              ( (BURST) == DMA_BURST_INC16) )
#define IS_DMA_PRIORITY( PRIOR )            ( ( (PRIOR) == DMA_PRIORITY_POLLING) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_0) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_1) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_2) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_3) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_4) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_5) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_6) || \
                                              ( (PRIOR) == DMA_PRIORITY_FIX_7) )
#define IS_DMA_MINOR_OFFSET_MODE( MODE )    ( ( (MODE) == DMA_MINORLOOP_OFFSET_DISABLE) || \
                                              ( (MODE) == DMA_MINORLOOP_OFFSET_SRC) || \
                                              ( (MODE) == DMA_MINORLOOP_OFFSET_DST) || \
                                              ( (MODE) == DMA_MINORLOOP_OFFSET_BOTH) )
#define IS_DMA_TRIGSRC( SRC )               ( ( (SRC) == DMA_TRIG_SOFTWARE) || \
                                              ( (SRC) == DMA_TRIG_UART0_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_UART0_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_UART1_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_UART1_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_UART2_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_UART2_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI0_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI0_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI1_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI1_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI2_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI2_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI3_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_SPI3_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_I2C0_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_I2C0_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_I2C1_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_I2C1_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_CT0_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_CT0_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_CT1_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_CT1_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_QSPI_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_QSPI_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_FSMC_RX_NOEMPTY) || \
                                              ( (SRC) == DMA_TRIG_FSMC_TX_EMPTY) || \
                                              ( (SRC) == DMA_TRIG_STIMER0_TRIG) || \
                                              ( (SRC) == DMA_TRIG_STIMER1_TRIG) || \
                                              ( (SRC) == DMA_TRIG_STIMER2_TRIG) || \
                                              ( (SRC) == DMA_TRIG_STIMER3_TRIG) || \
                                              ( (SRC) == DMA_TRIG_STIMER4_TRIG) || \
                                              ( (SRC) == DMA_TRIG_STIMER5_TRIG) || \
                                              ( (SRC) == DMA_TRIG_SECURTY_DMA) || \
                                              ( (SRC) == DMA_TRIG_DCMI_DMA) || \
                                              ( (SRC) == DMA_TRIG_GPIOA_EDGE) || \
                                              ( (SRC) == DMA_TRIG_GPIOB_EDGE) || \
                                              ( (SRC) == DMA_TRIG_GPIOC_EDGE) || \
                                              ( (SRC) == DMA_TRIG_GPIOD_EDGE) || \
                                              ( (SRC) == DMA_TRIG_GPIOE_EDGE) || \
                                              ( (SRC) == DMA_TRIG_GPIOF_EDGE) || \
                                              ( (SRC) == DMA_TRIG_PAE_FINISH) || \
                                              ( (SRC) == DMA_TRIG_BCA_FINISH) || \
                                              ( (SRC) == DMA_TRIG_HASH_CUR_COMPUTE) || \
                                              ( (SRC) == DMA_TRIG_HASH_LAST_COMPUTE) || \
                                              ( (SRC) == DMA_TRIG_ADC_FINISH) || \
                                              ( (SRC) == DMA_TRIG_DAC_WATERMARK) || \
                                              ( (SRC) == DMA_TRIG_DAC_TOP) || \
                                              ( (SRC) == DMA_TRIG_DAC_BOTTOM) || \
                                              ( (SRC) == DMA_TRIG_CMP_EDGE) || \
                                              ( (SRC) == DMA_TRIG_ALARM) || \
                                              ( (SRC) == DMA_TRIG_DMA_MUX0) || \
                                              ( (SRC) == DMA_TRIG_DMA_MUX1) || \
                                              ( (SRC) == DMA_TRIG_DMA_MUX2) || \
                                              ( (SRC) == DMA_TRIG_DMA_MUX3) )
#define IS_DMA_MINLOOPOFFSET( OFFSET )      ( ( (OFFSET) >= -524288) && ( (OFFSET) < 524288) )
#define IS_DMA_MINLOOPTRANSBYTES( NUM )     ( (NUM) < 1024)
#define IS_DMA_MAJLOOPOFFSET( OFFSET )      ( ( (OFFSET) >= -32768) && ( (OFFSET) < 32768) )
#define IS_DMA_MAJLOOPTRANSBYTES( NUM )     ( (NUM) < 32768)


/**
 * @}
 */


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
