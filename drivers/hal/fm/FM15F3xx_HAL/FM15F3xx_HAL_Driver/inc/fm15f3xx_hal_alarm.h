/**
 ******************************************************************************
 * @file    fm15f3xx_hal_alarm.h
 * @author  WYL
 * @brief   Header file of ALM HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_ALARM_H
#define __FM15F3xx_HAL_ALARM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_alarm.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup ALM
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup ALM_Exported_Types ALM Exported Types
 * @{
 */


/**
 * @brief  ALM Monitor Group definition
 */
typedef enum {
    MONITOR_GROUPA  = 0x00U,
    MONITOR_GROUPB  = 0x01U,
    MONITOR_GROUPC  = 0x02U,
    MONITOR_GROUPD  = 0x03U,
    MONITOR_GROUPE  = 0x04U
} ALM_MonitorGroupTypeDef;


/**
 * @brief  ALM BaseTimer Group definition
 */
typedef enum {
    ALM_BASETIMER_0 = 0x00U,
    ALM_BASETIMER_1 = 0x01U
} ALM_BaseTimerTypeDef;


/**
 * @brief   ALM Sensors Type definition
 */
typedef struct {
    uint32_t Light;

    uint32_t VddDet;

    uint32_t Ldo12L;

    uint32_t Ldo16L;

    uint32_t VddGlitch;

    uint32_t Irc4M;

    uint32_t Shield;

    uint32_t BaseTimer0;

    uint32_t BaseTimer1;

    uint32_t StandBy_Vdet;

    uint32_t StandBy_Ldo12L;

    uint32_t StandBy_Ldo16L;

    uint32_t PowerDown_Vdet;

    uint32_t PowerDown_Ldo12L;

    uint32_t PowerDown_Ldo16L;
} ALM_SensorsTypeDef;


/**
 * @brief  ALM Monitor structure definition
 */
typedef struct {
    uint32_t Monitor;

    uint32_t RecordEn;

    uint32_t Input;

    uint32_t DmaEn;

    uint32_t IntEn;

    uint32_t RstEn;

    uint32_t Power;

    uint32_t Nrst;

    uint32_t Filter;
} ALM_MonitorInitTypeDef;


/**
 * @brief  ALM Base Timer Alarm structure definition
 */
typedef struct {
    uint32_t AlarmCntUpper;

    uint32_t AlarmCntLower;
} ALM_BaseTimerAlarmTypeDef;


/**
 * @brief  ALM Base Timer structure definition
 */
typedef struct {
    uint32_t WorkMode;

    uint32_t RefClkCnt;

    uint32_t FreqAlarmEn;

    ALM_BaseTimerAlarmTypeDef Alarm;
} ALM_BaseTimerInitTypeDef;


/**
 * @}
 */


/* Exported constants --------------------------------------------------------*/


/** @defgroup ALM_Exported_Constants ALM Exported Constants
 * @{
 */


/** @brief ALM SENSORS
 *
 */
#define ALM_SENSORS_LIGHT_ENABLE        LL_ALM_SENSORE0_LIGHT
#define ALM_SENSORS_LIGHT_DISABLE       0x0U
#define ALM_SENSORS_VDET_ENABLE         LL_ALM_SENSORE0_VDET
#define ALM_SENSORS_VDET_DISABLE        0x0U
#define ALM_SENSORS_LDO12L_ENABLE       LL_ALM_SENSORE0_LDO12L
#define ALM_SENSORS_LDO12L_DISABLE      0x0U
#define ALM_SENSORS_LDO16L_ENABLE       LL_ALM_SENSORE0_LDO16L
#define ALM_SENSORS_LDO16L_DISABLE      0x0U
#define ALM_SENSORS_VDDGLITCH_ENABLE    LL_ALM_SENSORE0_GLITCH
#define ALM_SENSORS_VDDGLITCH_DISABLE   0x0U
#define ALM_SENSORS_IRC4M_ENABLE        LL_ALM_SENSORE0_IRC4M
#define ALM_SENSORS_IRC4M_DISABLE       0x0U
#define ALM_SENSORS_SHIELD_ENABLE       LL_ALM_SENSORE1_SLD
#define ALM_SENSORS_SHIELD_DISABLE      0x0U
#define ALM_SENSORS_BASETIMER0_ENABLE   LL_ALM_SENSORE1_FRPTA
#define ALM_SENSORS_BASETIMER0_DISABLE  0x0U
#define ALM_SENSORS_BASETIMER1_ENABLE   LL_ALM_SENSORE1_FRPTA
#define ALM_SENSORS_BASETIMER1_DISABLE  0x0U


/** @brief ALM SENSORS in standby or powerdown mode
 *
 */
#define ALM_SENSORS_LP_VDET_CLSOE       0x0U
#define ALM_SENSORS_SB_VDET_AUTOCLSOE   ALM_SENSORE0_ENVDDATSTBY
#define ALM_SENSORS_PD_VDET_AUTOCLSOE   ALM_SENSORE0_ENVDDATPD

#define ALM_SENSORS_LP_LDO12L_CLSOE     0x0U
#define ALM_SENSORS_SB_LDO12L_AUTOCLSOE ALM_SENSORE0_ENLDO12LATSTBY
#define ALM_SENSORS_PD_LDO12L_AUTOCLSOE ALM_SENSORE0_ENLDO12LATPD

#define ALM_SENSORS_LP_LDO16L_CLSOE     0x0U
#define ALM_SENSORS_SB_LDO16L_AUTOCLSOE ALM_SENSORE0_ENLDO16LATSTBY
#define ALM_SENSORS_PD_LDO16L_AUTOCLSOE ALM_SENSORE0_ENLDO16LATPD


/** @brief ALM MONITOR Iint
 *
 */
#define ALM_MONITOR_RCD_ENABLE      (0x00U)
#define ALM_MONITOR_RCD_DISABLE     (0x01U)
#define ALM_MONITOR_INPUT_NORMAL    (0x00U)
#define ALM_MONITOR_INPUT_INVERT    (0x02U)
#define ALM_MONITOR_DMA_ENABLE      (0x04U)
#define ALM_MONITOR_DMA_DISABLE     (0x00U)
#define ALM_MONITOR_INT_ENABLE      (0x08U)
#define ALM_MONITOR_INT_DISABLE     (0x00U)
#define ALM_MONITOR_RST_ENABLE      (0x10U)
#define ALM_MONITOR_RST_DISABLE     (0x00U)
#define ALM_MONITOR_POWERREST_LDO12 (0x20U)
#define ALM_MONITOR_POWERREST_VDD   (0x00U)
#define ALM_MONITOR_NRST_HOLD       (0x40U)
#define ALM_MONITOR_NRST_LOW        (0x00U)
#define ALM_MONITOR_FILTER_ENABLE   (0x80U)
#define ALM_MONITOR_FILTER_DISABLE  (0x00U)


/** @brief ALM MONITOR Select
 *
 */
#define ALM_MONITOR_GROUPA0_LIGHT       (0x00000001U)
#define ALM_MONITOR_GROUPA0_VDDL        (0x00000002U)
#define ALM_MONITOR_GROUPA0_VDDH        (0x00000004U)
#define ALM_MONITOR_GROUPA0_LDO12L      (0x00000008U)
#define ALM_MONITOR_GROUPA1_LDO16L      (0x00000010U)
#define ALM_MONITOR_GROUPA1_VDDGLITCH   (0x00000020U)
#define ALM_MONITOR_GROUPA1_IRC4M       (0x00000040U)
#define ALM_MONITOR_GROUPA1_BASATIMER0  (0x00000080U)
#define ALM_MONITOR_GROUPA2_BASATIMER1  (0x00000100U)
#define ALM_MONITOR_GROUPA3_SHIELD      (0x00000200U)

#define ALM_MONITOR_GROUPB0_TAMPERALARM (0x00000001U)
#define ALM_MONITOR_GROUPB0_RTCALARM    (0x00000002U)
#define ALM_MONITOR_GROUPB0_RTC1S       (0x00000004U)
#define ALM_MONITOR_GROUPB0_BKPCHIPRST  (0x00000008U)

#define ALM_MONITOR_GROUPC0_FLASHPE1    (0x00000001U)
#define ALM_MONITOR_GROUPC0_FLASHPE2    (0x00000002U)
#define ALM_MONITOR_GROUPC0_FLASHPEFF   (0x00000004U)
#define ALM_MONITOR_GROUPC0_FLASHNEFF   (0x00000008U)
#define ALM_MONITOR_GROUPC1_FLASHCE     (0x00000010U)
#define ALM_MONITOR_GROUPC1_RAMPE       (0x00000020U)
#define ALM_MONITOR_GROUPC1_RAERPE      (0x00000040U)
#define ALM_MONITOR_GROUPC1_CACHERPE    (0x00000080U)
#define ALM_MONITOR_GROUPC2_PMUTOE      (0x00000100U)
#define ALM_MONITOR_GROUPC2_BUSTOE      (0x00000200U)
#define ALM_MONITOR_GROUPC2_CPULOCKUP   (0x00000400U)
#define ALM_MONITOR_GROUPC2_WDT         (0x00000800U)
#define ALM_MONITOR_GROUPC3_REGPE       (0x00001000U)
#define ALM_MONITOR_GROUPC3_MODECTRLRE  (0x00002000U)
#define ALM_MONITOR_GROUPC4_RNGE        (0x00004000U)
#define ALM_MONITOR_GROUPC4_BCAE        (0x00008000U)
#define ALM_MONITOR_GROUPC4_HASHE       (0x00010000U)
#define ALM_MONITOR_GROUPC4_PAEE        (0x00020000U)
#define ALM_MONITOR_GROUPC5_CPUSELF     (0x00040000U)

#define ALM_MONITOR_GROUPD0_SYSSELFALARM1   (0x00000001U)
#define ALM_MONITOR_GROUPD0_SYSSELFALARM2   (0x00000002U)
#define ALM_MONITOR_GROUPD0_GPIOA           (0x00000004U)
#define ALM_MONITOR_GROUPD0_GPIOB           (0x00000008U)
#define ALM_MONITOR_GROUPD1_GPIOC           (0x00000010U)
#define ALM_MONITOR_GROUPD1_GPIOD           (0x00000020U)
#define ALM_MONITOR_GROUPD1_GPIOE           (0x00000040U)
#define ALM_MONITOR_GROUPD1_GPIOF           (0x00000080U)


/**
 * @}
 */


/** @brief ALM SENSORS
 *
 */
#define ALM_ALARMRESETA_LIGHT       LL_ALM_ALARMRESETA_LIGHT
#define ALM_ALARMRESETA_VDDL        LL_ALM_ALARMRESETA_VDDL
#define ALM_ALARMRESETA_VDDH        LL_ALM_ALARMRESETA_VDDH
#define ALM_ALARMRESETA_LDO12L      LL_ALM_ALARMRESETA_LDO12L
#define ALM_ALARMRESETA_LDO16L      LL_ALM_ALARMRESETA_LDO16L
#define ALM_ALARMRESETA_VDDGLITCH   LL_ALM_ALARMRESETA_VDDGLITCH
#define ALM_ALARMRESETA_IRC4M       LL_ALM_ALARMRESETA_IRC4M
#define ALM_ALARMRESETA_BASATIMER0  LL_ALM_ALARMRESETA_BASATIMER0
#define ALM_ALARMRESETA_BASATIMER1  LL_ALM_ALARMRESETA_BASATIMER1
#define ALM_ALARMRESETA_SHIELD      LL_ALM_ALARMRESETA_SHIELD

#define ALM_ALARMRESETB_TAMPERALARM LL_ALM_ALARMRESETB_TAMPERALARM
#define ALM_ALARMRESETB_RTCALARM    LL_ALM_ALARMRESETB_RTCALARM
#define ALM_ALARMRESETB_RTC1S       LL_ALM_ALARMRESETB_RTC1S
#define ALM_ALARMRESETB_BKPCHIPRST  LL_ALM_ALARMRESETB_BKPCHIPRST

#define ALM_ALARMRESETC_FLASHPE1    LL_ALM_ALARMRESETC_FLASHPE1
#define ALM_ALARMRESETC_FLASHPE2    LL_ALM_ALARMRESETC_FLASHPE2
#define ALM_ALARMRESETC_FLASHPEFF   LL_ALM_ALARMRESETC_FLASHPEFF
#define ALM_ALARMRESETC_FLASHNEFF   LL_ALM_ALARMRESETC_FLASHNEFF
#define ALM_ALARMRESETC_FLASHCE     LL_ALM_ALARMRESETC_FLASHCE
#define ALM_ALARMRESETC_RAMPE       LL_ALM_ALARMRESETC_RAMPE
#define ALM_ALARMRESETC_RAERPE      LL_ALM_ALARMRESETC_RAERPE
#define ALM_ALARMRESETC_CACHERPE    LL_ALM_ALARMRESETC_CACHERPE
#define ALM_ALARMRESETC_PMUTOE      LL_ALM_ALARMRESETC_PMUTOE
#define ALM_ALARMRESETC_BUSTOE      LL_ALM_ALARMRESETC_BUSTOE
#define ALM_ALARMRESETC_CPULOCKUP   LL_ALM_ALARMRESETC_CPULOCKUP
#define ALM_ALARMRESETC_WDT         LL_ALM_ALARMRESETC_WDT
#define ALM_ALARMRESETC_REGPE       LL_ALM_ALARMRESETC_REGPE
#define ALM_ALARMRESETC_MODECTRLRE  LL_ALM_ALARMRESETC_MODECTRLRE
#define ALM_ALARMRESETC_RNGE        LL_ALM_ALARMRESETC_RNGE
#define ALM_ALARMRESETC_BCAE        LL_ALM_ALARMRESETC_BCAE
#define ALM_ALARMRESETC_HASHE       LL_ALM_ALARMRESETC_HASHE
#define ALM_ALARMRESETC_PAEE        LL_ALM_ALARMRESETC_PAEE
#define ALM_ALARMRESETC_CPUSELF     LL_ALM_ALARMRESETC_CPUSELF

#define ALM_ALARMRESETD_SYSSELFALARM1   LL_ALM_ALARMRESETD_SYSSELFALARM1
#define ALM_ALARMRESETD_SYSSELFALARM2   LL_ALM_ALARMRESETD_SYSSELFALARM2
#define ALM_ALARMRESETD_GPIOA           LL_ALM_ALARMRESETD_GPIOA
#define ALM_ALARMRESETD_GPIOB           LL_ALM_ALARMRESETD_GPIOB
#define ALM_ALARMRESETD_GPIOC           LL_ALM_ALARMRESETD_GPIOC
#define ALM_ALARMRESETD_GPIOD           LL_ALM_ALARMRESETD_GPIOD
#define ALM_ALARMRESETD_GPIOE           LL_ALM_ALARMRESETD_GPIOE
#define ALM_ALARMRESETD_GPIOF           LL_ALM_ALARMRESETD_GPIOF

#define ALM_RESETE_POR      LL_ALM_RESETE_POR
#define ALM_RESETE_BOR      LL_ALM_RESETE_BOR
#define ALM_RESETE_LDO12L   LL_ALM_RESETE_LDO12L
#define ALM_RESETE_LDO16L   LL_ALM_RESETE_LDO16L
#define ALM_RESETE_NRST     LL_ALM_RESETE_NRST
#define ALM_RESETE_LLWUEXIT LL_ALM_RESETE_LLWUEXIT
#define ALM_RESETE_LLWUERR  LL_ALM_RESETE_LLWUERR


/** @brief ALM NRST RESET Select
 *
 */
#define ALM_NRST_ALL    LL_ALM_NRST_ALL
#define ALM_NRST_LDO12  LL_ALM_NRST_LDO12


/**
 * @}
 */


/** @defgroup ALM_Exported_Constants BaseTimer Exported Constants
 * @{
 */


/** @brief ALM BaseTimer Clk Select
 *
 */
#define ALM_BT_CLK_SYSTEST      (0x00U)
#define ALM_BT_CLK_IRC4M        (0x01U)
#define ALM_BT_CLK_IRC16M       (0x02U)
#define ALM_BT_CLK_PLL          (0x03U)
#define ALM_BT_CLK_OSC          (0x04U)
#define ALM_BT_CLK_PLL_DIV8     (0x05U)
#define ALM_BT_CLK_MCG_PLL_DIV  (0x06U)
#define ALM_BT_CLK_GPIO         (0x07U)
#define ALM_BT_CLK_TRNG_ANA     (0x0AU)
#define ALM_BT_CLK_USB48M       (0x0BU)
#define ALM_BT_CLK_SYS          (0x0CU)
#define ALM_BT_CLK_SECURITY     (0x0DU)
#define ALM_BT_CLK_OSC32K       (0x0FU)


/** @brief ALM BaseTimer WorkMode
 *
 */
#define ALM_BT_WORKMODE_ONESHOT     (0x00U)
#define ALM_BT_WORKMODE_CONTINUE    ALM_BT0CFG_BST0MD


/** @brief ALM BaseTimer Frequency Alarm
 *
 */
#define ALM_BT_FREQALARM_DISABLE    (0x00U)
#define ALM_BT_FREQALARM_ENABLE     ALM_BT0CFG_BST0FDETEN


/**
 * @}
 */


/**
 * @}
 */


/* Exported macro ------------------------------------------------------------*/


/** @defgroup ALM_Exported_Macros ALM Exported Macros
 * @{
 */


/** @brief Clear BaseTimer Count
 * @param  __BTx__ specifies the BaseTimer.
 * @retval None
 */
#define __HAL_ALM_BTCNT_CLEAR( __BTx__ ) (ALM_SENSORS1->BT[__BTx__].CTRL |= ALM_BT0CTRL_CLR)


/** @brief Get BaseTimer Ref Count
 * @param  __BTx__ specifies the BaseTimer.
 * @retval None
 */
#define __HAL_ALM_BT_GET_REFCNT( __BTx__ ) (ALM_SENSORS1->BT[__BTx__].CLKAC)


/** @brief Enable or Disable BaseTimer
 * @param  __BTx__ specifies the BaseTimer.
 * @retval None
 */
#define __HAL_ALM_BT_ENABLE( __BTx__ )  (ALM_SENSORS1->BT[__BTx__].CFG |= ALM_BT0CFG_BST0EN)
#define __HAL_ALM_BT_DISABLE( __BTx__ ) (ALM_SENSORS1->BT[__BTx__].CFG &= ~ALM_BT0CFG_BST0EN)


/** @brief Get Frequency Warning
 * @param  __BTx__ specifies the BaseTimer.
 * @retval None
 */
#define __HAL_ALM_GET_WARNING_FREQH( __BTx__ )  ( (ALM_SENSORS1->BT[__BTx__].RPT & (ALM_BT0RPT_HWARN | ALM_BT0RPT_FWARN) ) == (ALM_BT0RPT_HWARN | ALM_BT0RPT_FWARN) )
#define __HAL_ALM_GET_WARNING_FREQL( __BTx__ )  ( (ALM_SENSORS1->BT[__BTx__].RPT & (ALM_BT0RPT_LWARN | ALM_BT0RPT_FWARN) ) == (ALM_BT0RPT_LWARN | ALM_BT0RPT_FWARN) )


/** @brief Get Frequency Warning
 * @param  __BTx__ specifies the BaseTimer.
 * @retval None
 */
#define __HAL_ALM_BT_ALARM_ENABLE( __BTx__ )    (ALM_SENSORS1->BT[__BTx__].CFG = ALM_BT0CFG_BST0FDETEN)
#define __HAL_ALM_BT_ALARM_DISABLE( __BTx__ )   (ALM_SENSORS1->BT[__BTx__].CFG &= ~ALM_BT0CFG_BST0FDETEN)


/** @brief Get BaseTimer Clock
 * @param  __BTx__ specifies the BaseTimer.
 * @retval None
 */
#define __HAL_ALM_BT_GET_CLOCK( __BTx__ ) ( (ALM_SENSORS1->BTCLKC >> (4 * __BTx__) ) & 0xF)


/* Exported functions --------------------------------------------------------*/


/** @addtogroup ALM_Exported_Functions
 * @{
 */


/** @addtogroup ALM_Exported_Functions_Group1
 * @{
 */
/* Initialization and de-initialization functions *****************************/
HAL_StatusTypeDef HAL_ALM_MonitorInit(ALM_MonitorGroupTypeDef GROUPx, ALM_MonitorInitTypeDef* MonitorInit);


void HAL_ALM_MonitorDeInit(void);


HAL_StatusTypeDef HAL_ALM_SensorsInit(ALM_SensorsTypeDef* SENSORSn);


void HAL_ALM_SensorsDeInit(void);


HAL_StatusTypeDef HAL_ALM_BaseTimerInit(ALM_BaseTimerTypeDef BTx, ALM_BaseTimerInitTypeDef* BaseTimerInit);


/**
 * @}
 */


/** @addtogroup ALM_Exported_Functions_Group2
 * @{
 */
/* IO operation functions *****************************************************/
uint32_t HAL_ALM_IsActiveAlarmRecord(ALM_MonitorGroupTypeDef GROUPx, uint32_t AlarmRecord);


void HAL_ALM_ClearAllAlarmAndRecord(void);


uint32_t HAL_ALM_IsActiveResetRecord(ALM_MonitorGroupTypeDef GROUPx, uint32_t ResetRecord);


void HAL_ALM_ClearAllResetRecord(void);


void HAL_ALM_SetNrstRange(uint32_t Range);


void HAL_ALM_EnanbleGroupEReset(uint32_t Reset);


uint32_t HAL_ALM_FrequencyTest_Start(ALM_BaseTimerTypeDef BTx, uint32_t RefClock, uint32_t TestClock);


/**
 * @}
 */


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/


/** @defgroup ALM_Private_Constants ALM Private Constants
 * @{
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup ALM_Private_Macros ALM Private Macros
 * @{
 */
#define IS_ALM_MONITOR_GROUP( GROUP )               ( ( (GROUP) == MONITOR_GROUPA) || \
                                                      ( (GROUP) == MONITOR_GROUPB) || \
                                                      ( (GROUP) == MONITOR_GROUPC) || \
                                                      ( (GROUP) == MONITOR_GROUPD) )
#define IS_ALM_RESET_GROUP( GROUP )                 ( ( (GROUP) == MONITOR_GROUPA) || \
                                                      ( (GROUP) == MONITOR_GROUPB) || \
                                                      ( (GROUP) == MONITOR_GROUPC) || \
                                                      ( (GROUP) == MONITOR_GROUPD) || \
                                                      ( (GROUP) == MONITOR_GROUPE) )
#define IS_ALM_MONITOR_RCD( ACTION )                ( ( (ACTION) == ALM_MONITOR_RCD_ENABLE) || ( (ACTION) == ALM_MONITOR_RCD_DISABLE) )
#define IS_ALM_MONITOR_INPUT( ACTION )              ( ( (ACTION) == ALM_MONITOR_INPUT_NORMAL) || ( (ACTION) == ALM_MONITOR_INPUT_INVERT) )
#define IS_ALM_MONITOR_DMAEN( ACTION )              ( ( (ACTION) == ALM_MONITOR_DMA_ENABLE) || ( (ACTION) == ALM_MONITOR_DMA_DISABLE) )
#define IS_ALM_MONITOR_INTEN( ACTION )              ( ( (ACTION) == ALM_MONITOR_INT_ENABLE) || ( (ACTION) == ALM_MONITOR_INT_DISABLE) )
#define IS_ALM_MONITOR_RSTEN( ACTION )              ( ( (ACTION) == ALM_MONITOR_RST_ENABLE) || ( (ACTION) == ALM_MONITOR_RST_DISABLE) )
#define IS_ALM_MONITOR_POWERRESRT( ACTION )         ( ( (ACTION) == ALM_MONITOR_POWERREST_LDO12) || ( (ACTION) == ALM_MONITOR_POWERREST_VDD) )
#define IS_ALM_MONITOR_NRST( ACTION )               ( ( (ACTION) == ALM_MONITOR_NRST_HOLD) || ( (ACTION) == ALM_MONITOR_NRST_LOW) )
#define IS_ALM_MONITOR_FILTER( ACTION )             ( ( (ACTION) == ALM_MONITOR_FILTER_ENABLE) || ( (ACTION) == ALM_MONITOR_FILTER_DISABLE) )
#define IS_ALM_SENSORS_LIGHT( ACTION )              ( ( (ACTION) == ALM_SENSORS_LIGHT_ENABLE) || ( (ACTION) == ALM_SENSORS_LIGHT_DISABLE) )
#define IS_ALM_SENSORS_VDET( ACTION )               ( ( (ACTION) == ALM_SENSORS_VDET_ENABLE) || ( (ACTION) == ALM_SENSORS_VDET_DISABLE) )
#define IS_ALM_SENSORS_LDO12L( ACTION )             ( ( (ACTION) == ALM_SENSORS_LDO12L_ENABLE) || ( (ACTION) == ALM_SENSORS_LDO12L_DISABLE) )
#define IS_ALM_SENSORS_LDO16L( ACTION )             ( ( (ACTION) == ALM_SENSORS_LDO16L_ENABLE) || ( (ACTION) == ALM_SENSORS_LDO16L_DISABLE) )
#define IS_ALM_SENSORS_VDDGLITCH( ACTION )          ( ( (ACTION) == ALM_SENSORS_VDDGLITCH_ENABLE) || ( (ACTION) == ALM_SENSORS_VDDGLITCH_DISABLE) )
#define IS_ALM_SENSORS_IRC4M( ACTION )              ( ( (ACTION) == ALM_SENSORS_IRC4M_ENABLE) || ( (ACTION) == ALM_SENSORS_IRC4M_DISABLE) )
#define IS_ALM_SENSORS_SHIELD( ACTION )             ( ( (ACTION) == ALM_SENSORS_SHIELD_ENABLE) || ( (ACTION) == ALM_SENSORS_SHIELD_DISABLE) )
#define IS_ALM_SENSORS_BASETIMER0( ACTION )         ( ( (ACTION) == ALM_SENSORS_BASETIMER0_ENABLE) || ( (ACTION) == ALM_SENSORS_BASETIMER0_DISABLE) )
#define IS_ALM_SENSORS_BASETIMER1( ACTION )         ( ( (ACTION) == ALM_SENSORS_BASETIMER1_ENABLE) || ( (ACTION) == ALM_SENSORS_BASETIMER1_DISABLE) )
#define IS_ALM_SENSORS_STANDBY_VDET( ACTION )       ( ( (ACTION) == ALM_SENSORS_SB_VDET_AUTOCLSOE) || ( (ACTION) == ALM_SENSORS_LP_VDET_CLSOE) )
#define IS_ALM_SENSORS_STANDBY_LDO12L( ACTION )     ( ( (ACTION) == ALM_SENSORS_SB_LDO12L_AUTOCLSOE) || ( (ACTION) == ALM_SENSORS_LP_LDO12L_CLSOE) )
#define IS_ALM_SENSORS_STANDBY_LDO16L( ACTION )     ( ( (ACTION) == ALM_SENSORS_SB_LDO16L_AUTOCLSOE) || ( (ACTION) == ALM_SENSORS_LP_LDO16L_CLSOE) )
#define IS_ALM_SENSORS_POWERDOWN_VDET( ACTION )     ( ( (ACTION) == ALM_SENSORS_PD_VDET_AUTOCLSOE) || ( (ACTION) == ALM_SENSORS_LP_VDET_CLSOE) )
#define IS_ALM_SENSORS_POWERDOWN_LDO12L( ACTION )   ( ( (ACTION) == ALM_SENSORS_PD_LDO12L_AUTOCLSOE) || ( (ACTION) == ALM_SENSORS_LP_LDO12L_CLSOE) )
#define IS_ALM_SENSORS_POWERDOWN_LDO16L( ACTION )   ( ( (ACTION) == ALM_SENSORS_PD_LDO16L_AUTOCLSOE) || ( (ACTION) == ALM_SENSORS_LP_LDO16L_CLSOE) )
#define IS_ALM_BASETIMER( BT )                      ( ( (BT) == ALM_BASETIMER_0) || ( (BT) == ALM_BASETIMER_1) )
#define IS_ALM_BT_CLOCK( CLOCK )                    ( ( (CLOCK) == ALM_BT_CLK_SYSTEST) || \
                                                      ( (CLOCK) == ALM_BT_CLK_IRC4M) || \
                                                      ( (CLOCK) == ALM_BT_CLK_IRC16M) || \
                                                      ( (CLOCK) == ALM_BT_CLK_PLL) || \
                                                      ( (CLOCK) == ALM_BT_CLK_OSC) || \
                                                      ( (CLOCK) == ALM_BT_CLK_PLL_DIV8) || \
                                                      ( (CLOCK) == ALM_BT_CLK_MCG_PLL_DIV) || \
                                                      ( (CLOCK) == ALM_BT_CLK_GPIO) || \
                                                      ( (CLOCK) == ALM_BT_CLK_TRNG_ANA) || \
                                                      ( (CLOCK) == ALM_BT_CLK_USB48M) || \
                                                      ( (CLOCK) == ALM_BT_CLK_SYS) || \
                                                      ( (CLOCK) == ALM_BT_CLK_SECURITY) || \
                                                      ( (CLOCK) == ALM_BT_CLK_OSC32K) )
#define IS_ALM_BT_WORKMODE( MODE )                  ( ( (MODE) == ALM_BT_WORKMODE_ONESHOT) || ( (MODE) == ALM_BT_WORKMODE_CONTINUE) )
#define IS_ALM_BT_FREQALARM( MODE )                 ( ( (MODE) == ALM_BT_FREQALARM_DISABLE) || ( (MODE) == ALM_BT_FREQALARM_ENABLE) )
#define IS_ALM_BT_COUNT( CNT )                      ( (CNT) <= 0x00FFFFFF)


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup ALM_Private_Functions ALM Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_ALM_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
