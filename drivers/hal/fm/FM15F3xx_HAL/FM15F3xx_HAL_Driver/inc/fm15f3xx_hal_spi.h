/**
 ******************************************************************************
 * @file    fm15f3xx_hal_spi.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-04-23
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#ifndef __FM15F3XX_HAL_SPI_H
#define __FM15F3XX_HAL_SPI_H


#ifdef __cplusplus
extern "C" {
#endif


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_spi.h"

typedef struct {
    uint32_t Mode;                                      /*!< Specifies the SPI operating mode.
                                                              This parameter can be a value of @ref SPI_Mode */
    uint32_t Direction;                                 /*!< Specifies the SPI bidirectional mode state.
                                                              This parameter can be a value of @ref SPI_Direction */
    uint32_t DataSize;                                  /*!< Specifies the SPI data size.
                                                              This parameter can be a value of @ref SPI_Data_Size */
    uint32_t ClkPolarity;                               /*!< Specifies the serial clock steady state.
                                                             This parameter can be a value of @ref SPI_Clock_Polarity */
    uint32_t ClkPhase;                                  /*!< Specifies the clock active edge for the bit capture.
                                                             This parameter can be a value of @ref SPI_Clock_Phase */
    uint32_t SSN;                                       /*!< Specifies whether the SSN signal is managed by
                                                             hardware (SSN pin) or by software using the SSI bit.
                                                             This parameter can be a value of @ref SPI_Slave_Select_management */
    uint32_t BaudRatePrescaler;                         /*!< Specifies the Baud Rate prescaler value which will be
                                                             used to configure the transmit and receive SCK clock.
                                                             This parameter can be a value of @ref SPI_BaudRate_Prescaler
                                                             @note The communication clock is derived from the master
                                                              clock. The slave clock does not need to be set. */
    uint32_t FirstBit;                                  /*!< Specifies whether data transfers start from MSB or LSB bit.
                                                             This parameter can be a value of @ref SPI_MSB_LSB_transmission */
} SPI_InitTypeDef;


/**
 * @brief  HAL SPI State structure definition
 */
typedef enum {
    HAL_SPI_STATE_RESET         = 0x00U,                /*!< Peripheral not Initialized                         */
    HAL_SPI_STATE_READY         = 0x01U,                /*!< Peripheral Initialized and ready for use           */
    HAL_SPI_STATE_BUSY          = 0x02U,                /*!< an internal process is ongoing                     */
    HAL_SPI_STATE_BUSY_TX       = 0x03U,                /*!< Data Transmission process is ongoing               */
    HAL_SPI_STATE_BUSY_RX       = 0x04U,                /*!< Data Reception process is ongoing                  */
    HAL_SPI_STATE_BUSY_TX_RX    = 0x05U,                /*!< Data Transmission and Reception process is ongoing */
    HAL_SPI_STATE_ERROR         = 0x06U                 /*!< SPI error state                                    */
} HAL_SPI_StateTypeDef;


/**
 * @brief  SPI handle Structure definition
 */
typedef struct __SPI_HandleTypeDef {
    SPI_TypeDef *Instance;                              /* SPI registers base address */

    SPI_InitTypeDef Init;                               /* SPI communication parameters */

    uint8_t *pTxBuffPtr;                                /* Pointer to SPI Tx transfer Buffer */

    uint16_t TxXferSize;                                /* SPI Tx Transfer size */

    __IO uint16_t TxXferCount;                          /* SPI Tx Transfer Counter */

    uint8_t *pRxBuffPtr;                                /* Pointer to SPI Rx transfer Buffer */

    uint16_t RxXferSize;                                /* SPI Rx Transfer size */

    __IO uint16_t RxXferCount;                          /* SPI Rx Transfer Counter */

    void (*RxISR)(struct __SPI_HandleTypeDef * hspi);   /* function pointer on Rx ISR */

    void (*TxISR)(struct __SPI_HandleTypeDef * hspi);   /* function pointer on Tx ISR */

//  DMA_HandleTypeDef          *hdmatx;      /* SPI Tx DMA Handle parameters   */

//  DMA_HandleTypeDef          *hdmarx;      /* SPI Rx DMA Handle parameters   */


    __IO HAL_SPI_StateTypeDef State;                    /* SPI communication state */

    __IO uint32_t ErrorCode;                            /* SPI Error code */
} SPI_HandleTypeDef;

/* Exported constants --------------------------------------------------------*/


/** @defgroup SPI_Exported_Constants SPI Exported Constants
 * @{
 */


/** @defgroup SPI_Error_Code SPI Error Code
 * @{
 */
#define HAL_SPI_ERROR_NONE  0x00000000U                 /*!< No error             */
#define HAL_SPI_ERROR_ERR   0x00000001U                 /*!< master/slave error           */
#define HAL_SPI_ERROR_CRC   0x00000002U                 /*!< CRC error            */
#define HAL_SPI_ERROR_OVR   0x00000004U                 /*!< OVR error            */
#define HAL_SPI_ERROR_FRE   0x00000008U                 /*!< FRE error            */
#define HAL_SPI_ERROR_DMA   0x00000010U                 /*!< DMA transfer error   */
#define HAL_SPI_ERROR_FLAG  0x00000020U                 /*!< Flag: RXNE,TXE, BSY  */


/**
 * @}
 */


/** @defgroup SPI_Mode SPI Mode
 * @{
 */
#define SPI_MODE_SLAVE  LL_SPI_MODE_SLAVE
#define SPI_MODE_MASTER LL_SPI_MODE_MASTER


/**
 * @}
 */


/** @defgroup SPI_Direction SPI Direction Mode
 * @{
 */
#define SPI_DIRECTION_2LINES    LL_SPI_TXONLYEN_DISABLE
#define SPI_DIRECTION_1LINE     LL_SPI_TXONLYEN_ENABLE


/**
 * @}
 */


/** @defgroup SPI_Data_Size SPI Data Size
 * @{
 */
#define SPI_DATASIZE_8BIT   LL_SPI_TBYTE_1BYTE
#define SPI_DATASIZE_16BIT  LL_SPI_TBYTE_2BYTE
#define SPI_DATASIZE_24BIT  LL_SPI_TBYTE_3BYTE
#define SPI_DATASIZE_32BIT  LL_SPI_TBYTE_4BYTE


/**
 * @}
 */


/** @defgroup SPI_Clock_Polarity SPI Clock Polarity
 * @{
 */
#define SPI_POLARITY_LOW    LL_SPI_CPOL_LOW
#define SPI_POLARITY_HIGH   LL_SPI_CPOL_HIGH


/**
 * @}
 */


/** @defgroup SPI_Clock_Phase SPI Clock Phase
 * @{
 */
#define SPI_PHASE_1EDGE LL_SPI_CPHA_1EDGE
#define SPI_PHASE_2EDGE LL_SPI_CPHA_2EDGE


/**
 * @}
 */


/** @defgroup SPI_Slave_Select_management SPI Slave Select Management
 * @{
 */
#define SPI_SSN_SOFT_HIGHT  (LL_SPI_SSNMCUEN_MCU | LL_SPI_SSNMCU_HIGH)
#define SPI_SSN_SOFT_LOW    (LL_SPI_SSNMCUEN_MCU | LL_SPI_SSNMCU_LOW)
#define SPI_SSN_HARD        LL_SPI_SSNMCUEN_HARDWARE


/**
 * @}
 */


/** @defgroup SPI_BaudRate_Prescaler SPI BaudRate Prescaler
 * @{
 */
#define SPI_BAUDRATEPRESCALER_2     LL_SPI_BAUDRATE_DIV2            /* BaudRate = fPCLK/2   */
#define SPI_BAUDRATEPRESCALER_4     LL_SPI_BAUDRATE_DIV4            /* BaudRate = fPCLK/4   */
#define SPI_BAUDRATEPRESCALER_8     LL_SPI_BAUDRATE_DIV8            /* BaudRate = fPCLK/8   */
#define SPI_BAUDRATEPRESCALER_16    LL_SPI_BAUDRATE_DIV16           /* BaudRate = fPCLK/16  */
#define SPI_BAUDRATEPRESCALER_32    LL_SPI_BAUDRATE_DIV32           /* BaudRate = fPCLK/32  */
#define SPI_BAUDRATEPRESCALER_64    LL_SPI_BAUDRATE_DIV64           /* BaudRate = fPCLK/64  */
#define SPI_BAUDRATEPRESCALER_128   LL_SPI_BAUDRATE_DIV128          /* BaudRate = fPCLK/128 */
#define SPI_BAUDRATEPRESCALER_256   LL_SPI_BAUDRATE_DIV256          /* BaudRate = fPCLK/256 */


/**
 * @}
 */


/** @defgroup SPI_MSB_LSB_transmission SPI MSB LSB Transmission
 * @{
 */
#define SPI_FIRSTBIT_MSB    LL_SPI_MSB_FIRST
#define SPI_FIRSTBIT_LSB    LL_SPI_LSB_FIRST


/**
 * @}
 */


/** @defgroup SPI_Interrupt_definition SPI Interrupt Definition
 * @{
 */
#define SPI_IT_TXE  SPI_SPIIE_TXEIE                         /*Tx buffer empty interrupt*/
#define SPI_IT_RXNE SPI_SPIIE_RXNEIE                        /*RX buffer not empty interrupt*/
#define SPI_IT_ERR  SPI_SPIIE_ERRIE                         /*Error interrupt*/
#define SPI_IT_RXHF SPI_SPIIE_RXHFIE                        /*RX buffer half full interrupt*/
#define SPI_IT_TXHF SPI_SPIIE_TXHEIE                        /*TX buffer half full interrupt*/


/**
 * @}
 */


/** @defgroup SPI_Flags_definition SPI Flags Definition
 * @{
 */
#define SPI_FLAG_FIFO_RXNE  SPI_SPDF_RXFIFONE               /* SPI status flag: Rx buffer not empty flag */
#define SPI_FLAG_FIFO_TXE   SPI_SPDF_TXFIFOE                /* SPI status flag: Tx buffer empty flag */

#define SPI_FLAG_BUSY   SPI_SPSR_BUSY                       /* SPI status flag: Busy flag */
#define SPI_FLAG_TXWCOL SPI_SPSR_TXWCOL                     /* SPI status flag: TXFIFO WCOL flag */
#define SPI_FLAG_RXWCOL SPI_SPSR_RXWCOL                     /* SPI status flag: RXFIFO WCOL flag */
#define SPI_FLAG_SERR   SPI_SPSR_SLAVEERROR                 /* SPI status flag: slave err flag */
#define SPI_FLAG_MERR   SPI_SPSR_MASTERERROR                /* SPI status flag: master err flag */


/**
 * @}
 */


/** @defgroup SPI_DMA_definition SPI DMA Definition
 * @{
 */
#define SPI_DMA_TXE     SPI_SPIIE_TXEDE                     /*Tx buffer empty DMA*/
#define SPI_DMA_RXNE    SPI_SPIIE_RXNEDE                    /*RX buffer not empty DMA*/


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup SPI_Exported_Macros SPI Exported Macros
 * @{
 */


/** @brief  Reset SPI handle state.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x: 0,1, 2, or 3 to select the SPI peripheral.
 * @retval None
 */
#define __HAL_SPI_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_SPI_STATE_RESET)


/** @brief  Enable or disable the specified SPI interrupts.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x:0, 1, 2, or 3 to select the SPI peripheral.
 * @param  __INTERRUPT__ specifies the interrupt source to enable or disable.
 *         This parameter can be one of the following values:
 *            @arg SPI_IT_TXE: Tx buffer empty interrupt enable
 *            @arg SPI_IT_RXNE: RX buffer not empty interrupt enable
 *            @arg SPI_IT_ERR: Error interrupt enable
 *            @arg SPI_IT_RXHF: RX buffer half full interrupt enable
 *            @arg SPI_IT_TXHF: TX buffer half full interrupt enable
 * @retval None
 */
#define __HAL_SPI_ENABLE_IT( __HANDLE__, __INTERRUPT__ )    ( (__HANDLE__)->Instance->SPIIE |= (__INTERRUPT__) )
#define __HAL_SPI_DISABLE_IT( __HANDLE__, __INTERRUPT__ )   ( (__HANDLE__)->Instance->SPIIE &= (~(__INTERRUPT__) ) )


/** @brief  Check whether the specified SPI interrupt source is enabled or not.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x:0, 1, 2, or 3 to select the SPI peripheral.
 * @param  __INTERRUPT__ specifies the SPI interrupt source to check.
 *          This parameter can be one of the following values:
 *            @arg SPI_IT_TXE: Tx buffer empty interrupt enable
 *            @arg SPI_IT_RXNE: RX buffer not empty interrupt enable
 *            @arg SPI_IT_ERR: Error interrupt enable
 *            @arg SPI_IT_RXHF: RX buffer half full interrupt enable
 *            @arg SPI_IT_TXHF: TX buffer half full interrupt enable
 * @retval The new state of __IT__ (TRUE or FALSE).
 */
#define __HAL_SPI_GET_IT_SOURCE( __HANDLE__, __INTERRUPT__ ) ( ( ( (__HANDLE__)->Instance->SPIIE & (__INTERRUPT__) ) == (__INTERRUPT__) ) ? SET : RESET)


/** @brief  Check whether the specified SPI flag is set or not.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x: 0,1, 2, or 3 to select the SPI peripheral.
 * @param  __FLAG__ specifies the flag to check.
 *         This parameter can be one of the following values:
 *            @arg SPI_FLAG_BSY: busy flag
 *            @arg SPI_FLAG_TXWCOL: txfifo wcol flag
 *            @arg SPI_FLAG_RXWCOL: rxfifo wcol flag
 *            @arg SPI_FLAG_SERR: slave err flag
 *            @arg SPI_FLAG_MERR: master err flag
 * @retval The new state of __FLAG__ (TRUE or FALSE).
 */
#define __HAL_SPI_GET_FLAG( __HANDLE__, __FLAG__ ) ( ( ( (__HANDLE__)->Instance->SPSR) & (__FLAG__) ) == (__FLAG__) )


/** @brief  Check whether the specified SPI flag is set or not.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x: 0,1, 2, or 3 to select the SPI peripheral.
 * @param  __FLAG__ specifies the flag to check.
 *         This parameter can be one of the following values:
 *            @arg SPIFIFO_FLAG_RXNE: Receive buffer not empty flag
 *            @arg SPIFIFO_FLAG_TXE: Transmit buffer empty flag
 * @retval The new state of __FLAG__ (TRUE or FALSE).
 */
#define __HAL_SPIFIFO_GET_FLAG( __HANDLE__, __FLAG__ ) ( ( ( (__HANDLE__)->Instance->SPDF) & (__FLAG__) ) == (__FLAG__) )


/** @brief  Enable the SPI peripheral.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x:0, 1, 2, or 3 to select the SPI peripheral.
 * @retval None
 */
#define __HAL_SPI_ENABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->SPCR1 |= SPI_SPCR1_SPIEN)


/** @brief  Disable the SPI peripheral.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x:0, 1, 2, or 3 to select the SPI peripheral.
 * @retval None
 */
#define __HAL_SPI_DISABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->SPCR1 &= (~SPI_SPCR1_SPIEN) )


/**
 * @}
 */


/** @brief  Clear the SPI OVR pending flag.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x:0, 1, 2, or 3 to select the SPI peripheral.
 * @retval None
 */
#define __HAL_SPI_CLEAR_OVRFLAG( __HANDLE__ )        \
    do {                                              \
        (__HANDLE__)->Instance->SPSR    = 0;       \
        (__HANDLE__)->Instance->SPCR4   |= SPI_SPCR4_CTXWCOL | SPI_SPCR4_CRXWCOL;   \
    } while ( 0U )


/** @brief  Clear the SPI ERR pending flag.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x:0, 1, 2, or 3 to select the SPI peripheral.
 * @retval None
 */
#define __HAL_SPI_CLEAR_ERRFLAG( __HANDLE__ )       \
    do {                                              \
        (__HANDLE__)->Instance->SPSR    = 0;                 \
        (__HANDLE__)->Instance->SPCR4   |= SPI_SPCR4_CMASTERER | SPI_SPCR4_CSLAVEER; \
    } while ( 0U )


/** @brief  Clear the SPI FIFO flag.
 * @param  __HANDLE__ specifies the SPI Handle.
 *         This parameter can be SPI where x:0, 1, 2, or 3 to select the SPI peripheral.
 * @retval None
 */
#define __HAL_SPI_CLEAR_FIFOFLAG( __HANDLE__ )       \
    do {                                              \
        (__HANDLE__)->Instance->SPSR    = 0;                 \
        (__HANDLE__)->Instance->SPCR4   |= SPI_SPCR4_CRXFIFO | SPI_SPCR4_CTXFIFO; \
    } while ( 0U )


/** @brief  Set the SPI transmit-only mode.
 * @param  __HANDLE__ specifies the SPI Handle.
 *          This parameter can be SPI where x: 0,1, 2, or 3 to select the SPI peripheral.
 * @retval None
 */
#define SPI_1LINE_TX( __HANDLE__ ) ( (__HANDLE__)->Instance->SPCR3 |= SPI_SPCR3_TXONLYEN)


/** @addtogroup SPI_Exported_Functions_Group1
 * @{
 */
/* Initialization/de-initialization functions  **********************************/
HAL_StatusTypeDef HAL_SPI_Init(SPI_HandleTypeDef *hspi);


HAL_StatusTypeDef HAL_SPI_DeInit(SPI_HandleTypeDef *hspi);


void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi);


void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi);


/**
 * @}
 */


/** @addtogroup SPI_Exported_Functions_Group2
 * @{
 */
/* I/O operation functions  *****************************************************/
HAL_StatusTypeDef HAL_SPI_Transmit(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);


HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);


HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout);


HAL_StatusTypeDef HAL_SPI_Transmit_IT(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size);


HAL_StatusTypeDef HAL_SPI_Receive_IT(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size);


HAL_StatusTypeDef HAL_SPI_TransmitReceive_IT(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size);


void HAL_SPI_IRQHandler(SPI_HandleTypeDef *hspi);


void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi);


void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi);


void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi);


void HAL_SPI_TxHalfCpltCallback(SPI_HandleTypeDef *hspi);


void HAL_SPI_RxHalfCpltCallback(SPI_HandleTypeDef *hspi);


void HAL_SPI_TxRxHalfCpltCallback(SPI_HandleTypeDef *hspi);


void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi);


void HAL_SPI_AbortCpltCallback(SPI_HandleTypeDef *hspi);


/******************************** SPI Instances *******************************/
#define IS_SPI_ALL_INSTANCE( INSTANCE )             ( ( (INSTANCE) == SPI0) || \
                                                      ( (INSTANCE) == SPI1) || \
                                                      ( (INSTANCE) == SPI2) || \
                                                      ( (INSTANCE) == SPI3) )
#define IS_SPI_MODE( MODE )                         ( ( (MODE) == SPI_MODE_SLAVE) || \
                                                      ( (MODE) == SPI_MODE_MASTER) )
#define IS_SPI_DIRECTION( MODE )                    ( ( (MODE) == SPI_DIRECTION_2LINES) || \
                                                      ( (MODE) == SPI_DIRECTION_1LINE) )
#define IS_SPI_DATASIZE( SIZE )                     ( ( (SIZE) == SPI_DATASIZE_8BIT) || \
                                                      ( (SIZE) == SPI_DATASIZE_16BIT) || \
                                                      ( (SIZE) == SPI_DATASIZE_24BIT) || \
                                                      ( (SIZE) == SPI_DATASIZE_32BIT) )
#define IS_SPI_SSN( MODE )                          ( ( (MODE) == SPI_SSN_SOFT_HIGHT) || \
                                                      ( (MODE) == SPI_SSN_SOFT_LOW) || \
                                                      ( (MODE) == SPI_SSN_HARD) )
#define IS_SPI_BAUDRATE_PRESCALER( VAL )            ( ( (VAL) == SPI_BAUDRATEPRESCALER_2) || \
                                                      ( (VAL) == SPI_BAUDRATEPRESCALER_4) || \
                                                      ( (VAL) == SPI_BAUDRATEPRESCALER_8) || \
                                                      ( (VAL) == SPI_BAUDRATEPRESCALER_16) || \
                                                      ( (VAL) == SPI_BAUDRATEPRESCALER_32) || \
                                                      ( (VAL) == SPI_BAUDRATEPRESCALER_64) || \
                                                      ( (VAL) == SPI_BAUDRATEPRESCALER_128) || \
                                                      ( (VAL) == SPI_BAUDRATEPRESCALER_256) )
#define IS_SPI_FIRST_BIT( MODE )                    ( ( (MODE) == SPI_FIRSTBIT_MSB) || \
                                                      ( (MODE) == SPI_FIRSTBIT_LSB) )
#define IS_SPI_CPOL( MODE )                         ( ( (MODE) == SPI_POLARITY_LOW) || \
                                                      ( (MODE) == SPI_POLARITY_HIGH) )
#define IS_SPI_CPHA( MODE )                         ( ( (MODE) == SPI_PHASE_1EDGE) || \
                                                      ( (MODE) == SPI_PHASE_2EDGE) )
#define IS_SPI_DIRECTION_2LINES( MODE )             ( (MODE) == SPI_DIRECTION_2LINES)
#define IS_SPI_DIRECTION_2LINES_OR_1LINE( MODE )    ( ( (MODE) == SPI_DIRECTION_2LINES) || \
                                                      ( (MODE) == SPI_DIRECTION_1LINE) )


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
