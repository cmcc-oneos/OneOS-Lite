/**
 ******************************************************************************
 * @file    fm15f3xx_ll_flash.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef __FM15F3XX_LL_FLASH_H_
#define __FM15F3XX_LL_FLASH_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"
#include "fm15f3xx_system.h"

typedef enum {
    FLASH_COMPLETE          = 0x00,
    FLASH_BUSY              = 0x01,
    CHECK_BLANK             = 0x02,
    CHECK_NOT_BLANK         = 0x03,
    FLASH_ERROR_RDCOL       = 0x04,
    FLASH_ERROR_ACC         = 0x05,
    FLASH_ERROR_AUTH        = 0x06,
    FLASH_ERROR_LVT         = 0x07,
    FLASH_ERROR_COMM        = 0x08,
    FLASH_ERROR_NORAML_READ = 0x09,
    FLASH_TIMEOUT           = 0x0A,
    FLASH_UNKNOWN           = 0x0B,
} FLASH_Status;


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */
#if defined (FLASH)


/** @defgroup FLASH_LL FLASH
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/** @defgroup FLASH_LL_READCHECK_OPTION Enable or disable programming read-check
 * @{
 */
#define LL_FLASH_READCHECK_ENABLE   1
#define LL_FLASH_READCHECK_DISABLE  0


/**
 * @}
 */


/** @defgroup FLASH_LL_EM_WRITE_READ Common Write and read registers Macros
 * @{
 */


/**
 * @brief  Write a value in FLASH register
 * @param  __INSTANCE__ FLASH Instance
 * @param  __REG__ Register to be written
 * @param  __VALUE__ Value to be written in the register
 * @retval None
 */
#define LL_FLASH_WriteReg( __INSTANCE__, __REG__, __VALUE__ ) WRITE_REG( __INSTANCE__->__REG__, (__VALUE__) )


/**
 * @brief  Read a value in FLASH register
 * @param  __INSTANCE__ FLASH Instance
 * @param  __REG__ Register to be read
 * @retval Register value
 */
#define LL_FLASH_ReadReg( __INSTANCE__, __REG__ ) READ_REG( __INSTANCE__->__REG__ )


/**
 * @}
 */

/* ExFLASHed functions --------------------------------------------------------*/


/** @defgroup FLASH_LL_ExFLASHed_Functions FLASH ExFLASHed Functions
 * @{
 */


/**
 * @brief  FLASH Init structures definition
 */


/** @defgroup CMDSTAT
 * @{
 */
#define LL_FLASH_RESULT_PASS    0x00000000U
#define LL_FLASH_RESULT_FAIL    (FLASH_CMDSTAT_RESULT)
#define LL_FLASH_CMD_NOERR      0x00000000U
#define LL_FLASH_CMD_ERR        (FLASH_CMDSTAT_COMMERR)
#define LL_FLASH_LVT_NOERR      0x00000000U
#define LL_FLASH_LVT_ERR        (FLASH_CMDSTAT_LVTERR)
#define LL_FLASH_AUTH_NOERR     0x00000000U
#define LL_FLASH_AUTH_ERR       (FLASH_CMDSTAT_AUTHERR)
#define LL_FLASH_ACC_NOERR      0x00000000U
#define LL_FLASH_ACC_ERR        (FLASH_CMDSTAT_ACCERR)
#define LL_FLASH_RDCOL_NOERR    0x00000000U
#define LL_FLASH_RDCOL_ERR      (FLASH_CMDSTAT_RDCOLERR)
#define LL_FLASH_DONE_ING       0x00000000U
#define LL_FLASH_DONE_FINISH    (FLASH_CMDSTAT_DONE)


/**
 * @}
 */

//*********CMDINTE reg**************************


/** @defgroup CMDINTE :int enable
 * @{
 */
#define LL_FLASH_LVTINT_ENABLE      (FLASH_CMDINTE_LVT)
#define LL_FLASH_LVTINT_DISABLE     0x00000000U
#define LL_FLASH_RDCOLINT_ENABLE    (FLASH_CMDINTE_RDCOL)
#define LL_FLASH_RDCOLINT_DISABLE   0x00000000U
#define LL_FLASH_DONEINT_ENABLE     (FLASH_CMDINTE_DONE)
#define LL_FLASH_DONEINT_DISABLE    0x00000000U


/**
 * @}
 */

//*********CMDINTF reg**************************


/** @defgroup CMDINTF :int Flag
 * @{
 */
#define LL_FLASH_FLAG_LVTIE     (FLASH_CMDINTF_LVT)
#define LL_FLASH_FLAG_RDCOLIE   (FLASH_CMDINTF_RDCOL)
#define LL_FLASH_FLAG_DONEIE    (FLASH_CMDINTF_DONE)


/**CMDSTAT****************************
 * @}
 */


/** @defgroup CMDSTA
 * @{
 */
#define LL_FLASH_RESULT_FAIL    (FLASH_CMDSTAT_RESULT)
#define LL_FLASH_ERROR_COMM     (FLASH_CMDSTAT_COMMERR)
#define LL_FLASH_ERROR_LVT      (FLASH_CMDSTAT_LVTERR)
#define LL_FLASH_ERROR_AUTH     (FLASH_CMDSTAT_AUTHERR)
#define LL_FLASH_ERROR_ACC      (FLASH_CMDSTAT_ACCERR)
#define LL_FLASH_ERROR_RDCOL    (FLASH_CMDSTAT_RDCOLERR)
#define LL_FLASH_DONE_FINISH    (FLASH_CMDSTAT_DONE)
#define LL_FLASH_ERROR_CLEAR    (FLASH_CMDSTAT_ACCERR | FLASH_CMDSTAT_AUTHERR | FLASH_CMDSTAT_LVTERR)


/**
 * @}
 */

//*********PORT reg**************************


/** @defgroup TRHR
 * @{
 */
#define LL_FLASH_RHR( __VALUE__ )   ( (__VALUE__) << FLASH_PORT_TRHR_Pos)
#define LL_FLASH_DPDH( __VALUE__ )  ( (__VALUE__) << FLASH_PORT_TDPDH_Pos)
#define LL_FLASH_RT( __VALUE__ )    ( (__VALUE__) << FLASH_PORT_TRT_Pos)
#define LL_FLASH_DPDSR( __VALUE__ ) ( (__VALUE__) << FLASH_PORT_TDPDSR_Pos)
#define LL_FLASH_PORTREG_UNLOCK 0x00000000U
#define LL_FLASH_PORTREG_LOCK   (FLASH_PORT_LOCK)


/**
 * @}
 */

//*********RDCFG reg**************************


/** @defgroup RDM
 * @{
 */
#define LL_FLASH_RDMODE( __VALUE__ )    ( (__VALUE__) << FLASH_RDCFG_RDM_Pos)
#define LL_FLASH_LPWRMODE( __VALUE__ )  ( (__VALUE__) << FLASH_RDCFG_LPWR_Pos)
#define LL_FLASH_RDBUF_DISABLE      0x00000000U
#define LL_FLASH_RDBUF_ENABLE       (FLASH_RDCFG_RDBUFEN)
#define LL_FLASH_RDCFGREG_UNLOCK    0x00000000U
#define LL_FLASH_RDCFGREG_LOCK      (FLASH_RDCFG_LOCK)


/**
 * @}
 */

//*********RDT reg**************************


/** @defgroup TMS
 * @{
 */
#define LL_FLASH_MS( __VALUE__ )    ( (__VALUE__) << FLASH_RDT_TMS_Pos)
#define LL_FLASH_ACCN( __VALUE__ )  ( (__VALUE__) << FLASH_RDT_TACCN_Pos)
#define LL_FLASH_ACCRV( __VALUE__ ) ( (__VALUE__) << FLASH_RDT_TACCRV_Pos)
#define LL_FLASH_CKH( __VALUE__ )   ( (__VALUE__) << FLASH_RDT_TCKH_Pos)
#define LL_FLASH_RDTREG_LOCK    0x00000000U
#define LL_FLASH_RDTREG_UNLOCK  (FLASH_RDT_LOCK)


/**
 * @}
 */

//*********LDOEN reg**************************


/** @defgroup LDO16_NUM[3:0]
 * @{
 */
#define LL_FLASH_LDO16NUM( __VALUE__ )  ( (__VALUE__) << FLASH_LDOEN_LDO16NUM_Pos)
#define LL_FLASH_LDO16WAIT( __VALUE__ ) ( (__VALUE__) << FLASH_LDOEN_LDO16WAIT_Pos)
#define LL_FLASH_LDO16PRESTART_Disable  0x00000000U
#define LL_FLASH_LDO16PRESTART_Enabl    (FLASH_LDOEN_LDO16EN)
#define LL_FLASH_LVTFILTERCNT( __VALUE__ ) ( (__VALUE__) << FLASH_LDOEN_LVTFCNT_Pos)
#define LL_FLASH_LVTALARM_OPEN  0x00000000U
#define LL_FLASH_LVTALARM_OFF   (FLASH_LDOEN_LVTOFF)
#define LL_FLASH_LDOREG_UNLOCK  0x00000000U
#define LL_FLASH_LDOREG_LOCK    (FLASH_LDOEN_LOCK)


/**
 * @}
 */

//*********ERASET0 reg**************************


/** @defgroup TNVSE
 * @{
 */
#define LL_FLASH_NVSE( __VALUE__ )  ( (__VALUE__) << FLASH_ERAST0_TNVSE_Pos)
#define LL_FLASH_ERSC( __VALUE__ )  ( (__VALUE__) << FLASH_ERAST0_TERSC_Pos)
#define LL_FLASH_RCVE( __VALUE__ )  ( (__VALUE__) << FLASH_ERAST0_TRCVE_Pos)
#define LL_FLASH_RWE( __VALUE__ )   ( (__VALUE__) << FLASH_ERAST0_TRWE_Pos)
#define LL_FLASH_ERAST0REG_UNLOCK   0x00000000U
#define LL_FLASH_ERAST0REG_LOCK     (FLASH_ERAST0_LOCK)


/**
 * @}
 */

//*********ERASET1 reg**************************


/** @defgroup TERSR
 * @{
 */
#define LL_FLASH_RETRYSECTORERASE( __VALUE__ )  ( (__VALUE__) << FLASH_ERAST1_TERSR_Pos)
#define LL_FLASH_SINGLESECTORERASE( __VALUE__ ) ( (__VALUE__) << FLASH_ERAST1_TERSS_Pos)
#define LL_FLASH_ERAST1REG_UNLOCK   0x00000000U
#define LL_FLASH_ERAST1REG_LOCK     (FLASH_ERAST1_LOCK)


/**
 * @}
 */

//*********PROGT0 reg**************************


/** @defgroup TNVSP
 * @{
 */
#define LL_FLASH_NVSP( __VALUE__ )  ( (__VALUE__) << FLASH_PROGT0_TNVSP_Pos)
#define LL_FLASH_RCVP( __VALUE__ )  ( (__VALUE__) << FLASH_PROGT0_TRCVP_Pos)
#define LL_FLASH_RWP( __VALUE__ )   ( (__VALUE__) << FLASH_PROGT0_TRWP_Pos)
#define LL_FLASH_PROGT0REG_UNLOCK   0x00000000U
#define LL_FLASH_PROGT0REG_LOCK     (FLASH_PROGT0_LOCK)


/**
 * @}
 */

//*********PROGT1 reg**************************


/** @defgroup TPG
 * @{
 */
#define LL_FLASH_PG( __VALUE__ )        ( (__VALUE__) << FLASH_PROGT1_TPG_Pos)
#define LL_FLASH_PGS( __VALUE__ )       ( (__VALUE__) << FLASH_PROGT1_TPGS_Pos)
#define LL_FLASH_PREPG( __VALUE__ )     ( (__VALUE__) << FLASH_PROGT1_TPREPG_Pos)
#define LL_FLASH_PREPGS( __VALUE__ )    ( (__VALUE__) << FLASH_PROGT1_TPREPGS_Pos)
#define LL_FLASH_PROGT1REG_UNLOCK   0x00000000U
#define LL_FLASH_PROGT1REG_LOCK     (FLASH_PROGT1_LOCK)


/**
 * @}
 */

//*********CFGT reg**************************


/** @defgroup TCFL
 * @{
 */
#define LL_FLASH_CFL( __VALUE__ )       ( (__VALUE__) << FLASH_CFGT_TCFL_Pos)
#define LL_FLASH_CFH( __VALUE__ )       ( (__VALUE__) << FLASH_CFGT_TCFH_Pos)
#define LL_FLASH_CONFEN( __VALUE__ )    ( (__VALUE__) << FLASH_CFGT_TCFEN_Pos)
#define LL_FLASH_CFGTREG_UNLOCK 0x00000000U
#define LL_FLASH_CFGTREG_LOCK   (FLASH_CFGT_LOCK)


/**
 * @}
 */

//*********VRDT reg**************************


/** @defgroup TMSV
 * @{
 */
#define LL_FLASH_MSV( __VALUE__ )   ( (__VALUE__) << FLASH_VRDT_TMSV_Pos)
#define LL_FLASH_AccV( __VALUE__ )  ( (__VALUE__) << FLASH_VRDT_TACCV_Pos)
#define LL_FLASH_CKHV( __VALUE__ )  ( (__VALUE__) << FLASH_VRDT_TCKHV_Pos)
#define LL_FLASH_VRDTREG_UNLOCK 0x00000000U
#define LL_FLASH_VRDTREG_LOCK   (FLASH_VRDT_LOCK)


/**
 * @}
 */

//*********CMDCFG reg**************************


/** @defgroup ER_RETRY
 * @{
 */
#define LL_FLASH_ERASERETRY( __VALUE__ ) ( (__VALUE__) << FLASH_CMDCFG_ERRETRY_Pos)
#define LL_FLASH_ERASEWAY_RETRY         0x00000000U
#define LL_FLASH_ERASEWAY_SINGLE        (FLASH_CMDCFG_ERWAY)
#define LL_FLASH_ERASEVREADL_NOVERIFY   0x00000000U
#define LL_FLASH_ERASEVREADL_VERIFY     (FLASH_CMDCFG_ERVRD)
#define LL_FLASH_CHIPERASENVR_NO        0x00000000U
#define LL_FLASH_CHIPERASENVR_YES       (FLASH_CMDCFG_ERNVR)


/**
 * @}
 */


/** @defgroup ER_SIZE[1:0]
 * @{
 */
#define LL_FLASH_ERASESIZE_SECTOR   0x00000000U
#define LL_FLASH_ERASESIZE_BLOCK    (0x1U << FLASH_CMDCFG_ERSIZE_Pos)
#define LL_FLASH_ERASESIZE_CHIP     (0x2U << FLASH_CMDCFG_ERSIZE_Pos)
//#define LL_FLASH_EraseSize_Sector        (0x3U << FLASH_CMDCFG_ERSIZE_Pos )


/**
 * @}
 */


/** @defgroup VRD_RECALL
 * @{
 */
#define LL_FLASH_VRDRECALL_DISABLE  0x00000000U
#define LL_FLASH_VRDRECALL_ENABLE   (FLASH_CMDCFG_VRDRECALL)
#define LL_FLASH_VRDVREAD_DISABLE   0x00000000U
#define LL_FLASH_VRDVREAD_ENABLE    (FLASH_CMDCFG_VRDVREAD)


/**
 * @}
 */


/** @defgroup VRD_SIZE[1:0]
 * @{
 */
#define LL_FLASH_VREADSIZE( __VALUE__ ) ( (__VALUE__) << FLASH_CMDCFG_VRDSIZE_Pos)
#define LL_FLASH_VREAD1SECTOR   LL_FLASH_VReadSize_( 0x0 )
#define LL_FLASH_VREAD1BLOCK    LL_FLASH_VReadSize_( 0x1 )
#define LL_FLASH_VREAD1WORD     LL_FLASH_VReadSize_( 0x8 )
#define LL_FLASH_VREAD4WORD     LL_FLASH_VReadSize_( 0x9 )
#define LL_FLASH_VREAD32WORD    LL_FLASH_VReadSize_( 0xA )


/**
 * @}
 */


/** @defgroup PREPG_ENB
 * @{
 */
#define LL_FLASH_PREPROGRAM_DISABLE 0x00000000U
#define LL_FLASH_PREPROGRAM_ENABLE  (FLASH_CMDCFG_PREPGEN)


/**
 * @}
 */


/** @defgroup PG_SIZE[1:0]
 * @{
 */
#define LL_FLASH_PROGRAMSIZE_4WORD  0x00000000U
#define LL_FLASH_PROGRAMSIZE_1WORD  (0x1U << FLASH_CMDCFG_PGSIZE_Pos)
#define LL_FLASH_PROGRAMSIZE_32WORD (0x2U << FLASH_CMDCFG_PGSIZE_Pos)
//#define LL_FLASH_ProgramSize_4word       (0x3U << FLASH_CMDCFG_PGSIZE_Pos )


/**
 * @}
 */


/** @defgroup
 * @{
 */
#define LL_FLASH_CMDCFGREG_UNLOCK   0x00000000U
#define LL_FLASH_CMDCFGREG_LOCK     (FLASH_CMDCFG_LOCK)


/**
 * @}
 */

//*********CMDEN reg**************************


/** @defgroup CMDEN
 * @{
 */
#define LL_FLASH_CMDEN( __VALUE__ ) ( (__VALUE__) << FLASH_CMDEN_CMDEN_Pos)
#define LL_FLASH_CMD_DISABLE    (0x0)
#define LL_FLASH_CMD_ENABLE     (0x5AA5A55AU)


/**
 * @}
 */

//*********COMM reg**************************


/** @defgroup COMM[31:0]
 * @{
 */
#define LL_FLASH_COMM_( __VALUE__ ) ( (__VALUE__) << FLASH_COMM_COMM_Pos)
#define LL_FLASH_COMM_FLASHCFG          LL_FLASH_COMM_( 0x5AA5A55AU )
#define LL_FLASH_COMM_VREAD1CHECK1      LL_FLASH_COMM_( 0xA55A5AA5U )
#define LL_FLASH_COMM_EREASCMD          LL_FLASH_COMM_( 0x55AAAA55U )
#define LL_FLASH_COMM_PROGRAMCMD        LL_FLASH_COMM_( 0xAA5555AAU )
#define LL_FLASH_COMM_PROGDATACHECKCMD  LL_FLASH_COMM_( 0xA5A5A5A5U )


/**
 * @}
 */

//*********CMD_ADDR reg**************************


/** @defgroup CMD_ADDR[31:0]
 * @{
 */
#define LL_FLASH_CMDADDR( __VALUE__ ) ( (__VALUE__) << FLASH_CMDADDR_CMDADDR_Pos)


/**
 * @}
 */

//*********CMDPARAn reg********n=0~31******************


/** @defgroup CMD_PARAn[31:0]
 * @{
 */
#define LL_FLASH_CMDPARAN( __VALUE__ ) ( (__VALUE__) << FLASH_CMDPARAn_CMDPARA_Pos)


/**
 * @}
 */

//*********CMDPF reg**************************


/** @defgroup PARAn_FLAG[31:0]
 * @{
 */
#define LL_FLASH_PARANFLAG( __VALUE__ ) ( (__VALUE__) << FLASH_CMDPF_PARAnFLAG_Pos)


/**
 * @}
 */

//*********MCCFG reg**************************


/** @defgroup MC_CFG[31:0]
 * @{
 */
#define LL_FLASH_CONFIGMODECTRLWORD( __VALUE__ ) ( (__VALUE__) << FLASH_MCCFG_MCCFG_Pos)


/**
 * @}
 */

//*********ENCRY reg**************************


/** @defgroup ADDR_ENCRY_EN
 * @{
 */
#define LL_FLASH_ADDRENCRY_DISABLE  0x00000000U
#define LL_FLASH_ADDRENCRY_ENABLE   (FLASH_ENCRY_ADDRENCRYEN)
#define LL_FLASH_DATAENCRY_DISABLE  0x00000000U
#define LL_FLASH_DATAENCRY_ENABLE   (FLASH_ENCRY_DATAENCRYEN)
#define LL_FLASH_DATAENCRY_2CYCLE   0x00000000U
#define LL_FLASH_DATAENCRY_3CYCLE   (FLASH_ENCRY_DATAENCRYCFG)
#define LL_FLASH_ECCRC_DISABLE      0x00000000U
#define LL_FLASH_ECCRC_ENABLE       (FLASH_ENCRY_ECCRCEN)
#define LL_FLASH_ECCRC_ECC          0x00000000U
#define LL_FLASH_ECCRC_CRC          (FLASH_ENCRY_ECCRCCFG)
#define LL_FLASH_ECCRCPAD_0FILL     0x00000000U
#define LL_FLASH_ECCRCPAD_1FILL     (FLASH_ENCRY_ECCRCPAD)
#define LL_FLASH_RMASK_DISABLE      0x00000000U
#define LL_FLASH_RMASK_ENABLE       (FLASH_ENCRY_RMASKEN)
#define LL_FLASH_WMASK_DISABLE      0x00000000U
#define LL_FLASH_WMASK_ENABLE       (FLASH_ENCRY_WMASKEN)
#define LL_FLASH_ENCRYREG_UNLOCK    0x00000000U
#define LL_FLASH_ENCRYREG_LOCK      (FLASH_ENCRY_LOCK)


/**
 * @}
 */

//*********RDNEN reg**************************


/** @defgroup RDN0_EN
 * @{
 */
#define LL_FLASH_RDN0_DISABLE       0x00000000U
#define LL_FLASH_RDN0_ENABLE        (FLASH_RDNEN_RDN0EN)
#define LL_FLASH_RDN1_DISABLE       0x00000000U
#define LL_FLASH_RDN1_ENABLE        (FLASH_RDNEN_RDN1EN)
#define LL_FLASH_RDN2_DISABLE       0x00000000U
#define LL_FLASH_RDN2_ENABLE        (FLASH_RDNEN_RDN2EN)
#define LL_FLASH_RDN3_DISABLE       0x00000000U
#define LL_FLASH_RDN3_ENABLE        (FLASH_RDNEN_RDN3EN)
#define LL_FLASH_RDNENREG_UNLOCK    0x00000000U
#define LL_FLASH_RDNENREG_LOCK      (FLASH_RDNEN_LOCK)


/**
 * @}
 */

//*********AKEY reg**************************


/** @defgroup AKEY[10:0]
 * @{
 */
#define LL_FLASH_ADDRENCRYKEY( __VALUE__ )  ( (__VALUE__) << FLASH_AKEY_AKEY_Pos)
#define LL_FLASH_DATAENCRYKEY( __VALUE__ )  ( (__VALUE__) << FLASH_DKEY_DKEY_Pos)


/**
 * @}
 */

//*********RDNn reg**************************


/** @defgroup RDNn[8:0]
 * @{
 */
#define LL_FLASH_RDN0ADDR( __VALUE__ )  ( (__VALUE__) << FLASH_RDNX_RDNX_Pos)
#define LL_FLASH_RDN1ADDR( __VALUE__ )  ( (__VALUE__) << FLASH_RDNX_RDNX_Pos)
#define LL_FLASH_RDN2ADDR( __VALUE__ )  ( (__VALUE__) << FLASH_RDNX_RDNX_Pos)
#define LL_FLASH_RDN3ADDR( __VALUE__ )  ( (__VALUE__) << FLASH_RDNX_RDNX_Pos)


/**
 * @}
 */


/** @defgroupRDN0_LOCK
 * @{
 */
#define LL_FLASH_RDN0REG_UNLOCK 0x00000000U
#define LL_FLASH_RDN0REG_LOCK   (FLASH_RDNX_LOCK)


/**
 * @}
 */


/** @defgroupRDN1_LOCK
 * @{
 */
#define LL_FLASH_RDN1REG_UNLOCK 0x00000000U
#define LL_FLASH_RDN1REG_LOCK   (FLASH_RDNX_LOCK)


/**
 * @}
 */


/** @defgroupRDN2_LOCK
 * @{
 */
#define LL_FLASH_RDN2REG_UNLOCK 0x00000000U
#define LL_FLASH_RDN2REG_LOCK   (FLASH_RDNX_LOCK)


/**
 * @}
 */


/** @defgroupRDN3_LOCK
 * @{
 */
#define LL_FLASH_RDN3REG_UNLOCK 0x00000000U
#define LL_FLASH_RDN3REG_LOCK   (FLASH_RDNX_LOCK)


/**
 * @}
 */


//*********MBLK_LOCK reg**************************


/** @defgroup BLKn_LOCK[19:0] ...1=lock
 * @{
 */
#define LL_FLASH_MAINBLOCK( __VALUE__ ) ( (__VALUE__) << FLASH_MBLKLOCK_BLKXLOCKB_Pos)


/**
 * @}
 */

//*********NVRU_LOCK reg**************************


/** @defgroup NVR0_LOCK
 * @{
 */
#define LL_FLASH_NVR0_LOCK      0x00000000U
#define LL_FLASH_NVR0_UNLOCK    (FLASH_NVRULOCK_NVR0LOCKB)
#define LL_FLASH_NVR1_LOCK      0x00000000U
#define LL_FLASH_NVR1_UNLOCK    (FLASH_NVRULOCK_NVR1LOCKB)


/**
 * @}
 */

//*********OTP_LOCK reg**************************


/** @defgroup OTPn_LOCK[31:0] ...1=lock
 * @{
 */
#define LL_FLASH_OTPNVR2SECT0LOCK( __VALUE__ ) ( (__VALUE__) << FLASH_OTPLOCK_OTPXLOCKB_Pos)


/**
 * @}
 */

/* functions --------------------------------------------------------*/


/** @defgroup FLASH_LL_Functions FLASH Functions
 * @{
 */


/** @defgroup FLASH_LL_CMDSTAT
 * @{
 */


/**
 * @brief  Get Result
 * @rmtoll CMDSTAT          RESULT          LL_FLASH_GetResult
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RESULT_PASS
 *         @arg @ref LL_FLASH_RESULT_FAIL
 */
__STATIC_INLINE uint32_t LL_FLASH_IsFail(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDSTAT, LL_FLASH_RESULT_FAIL) == LL_FLASH_RESULT_FAIL));
}


/**
 * @brief  Clear LVTERR
 * @note   This bit should not be changed when communication is ongoing.
 *         This bit is not used in FLASH TI mode.
 * @rmtoll CMDSTAT          LVTErr          LL_FLASH_ClearErr_LVT
 * @param  FLASHx FLASH Instance
 * @param  ClockPolarity This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LVT_ERR
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearErr_LVT(FLASH_TypeDef *FLASHx, uint32_t LVTErr)
{
    MODIFY_REG(FLASHx->CMDSTAT, FLASH_CMDSTAT_LVTERR, LVTErr);
}


/**
 * @brief  Get LVTERR
 * @rmtoll CMDSTAT          LVTERR          LL_FLASH_isActiveErr_LVT
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LVT_NOERR
 *         @arg @ref LL_FLASH_LVT_ERR
 */
__STATIC_INLINE uint32_t LL_FLASH_IsActiveErr_LVT(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDSTAT, FLASH_CMDSTAT_LVTERR) == FLASH_CMDSTAT_LVTERR));
}


/**
 * @brief  Clear AUTHERR
 * @note   This bit should not be changed when communication is ongoing.
 *         This bit is not used in FLASH TI mode.
 * @rmtoll CMDSTAT          AuthErr          LL_FLASH_ClearErr_Auth
 * @param  FLASHx FLASH Instance
 * @param  ClockPolarity This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_AUTH_ERR
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearErr_Auth(FLASH_TypeDef *FLASHx, uint32_t AuthErr)
{
    MODIFY_REG(FLASHx->CMDSTAT, FLASH_CMDSTAT_AUTHERR, AuthErr);
}


/**
 * @brief  Get AUTHERR
 * @rmtoll CMDSTAT          AuthErr          LL_FLASH_IsActiveErr_Auth
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_AUTH_NOERR
 *         @arg @ref LL_FLASH_AUTH_ERR
 */
__STATIC_INLINE uint32_t LL_FLASH_IsActiveErr_Auth(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDSTAT, FLASH_CMDSTAT_AUTHERR) == FLASH_CMDSTAT_AUTHERR));
}


/**
 * @brief  Clear ACCERR
 * @note   This bit should not be changed when communication is ongoing.
 *         This bit set 1 to clear ACCERR.
 * @rmtoll CMDSTAT          AccErr          LL_FLASH_ClearErr_Acc
 * @param  FLASHx FLASH Instance
 * @param  AccErr This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ACC_ERR
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearErr_Acc(FLASH_TypeDef *FLASHx, uint32_t AccErr)
{
    MODIFY_REG(FLASHx->CMDSTAT, FLASH_CMDSTAT_ACCERR, AccErr);
}


/**
 * @brief  Get ACCERR
 * @rmtoll CMDSTAT          AccErr          LL_FLASH_IsActiveErr_Acc
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ACC_NOERR
 *         @arg @ref LL_FLASH_ACC_ERR
 */
__STATIC_INLINE uint32_t LL_FLASH_IsActiveErr_Acc(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDSTAT, LL_FLASH_ACC_ERR) == LL_FLASH_ACC_ERR));
}


/**
 * @brief  Clear RDCOLERR
 * @note   This bit should not be changed when communication is ongoing.
 *         This bit set 1 to clear ACCERR.
 * @rmtoll CMDSTAT          RdcolErr          LL_FLASH_ClearRdcolErr
 * @param  FLASHx FLASH Instance
 * @param  RdcolErr This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDCOL_ERR
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearErr_Rdcol(FLASH_TypeDef *FLASHx, uint32_t RdcolErr)
{
    MODIFY_REG(FLASHx->CMDSTAT, FLASH_CMDSTAT_RDCOLERR, RdcolErr);
}


/**
 * @brief  Get RDCOLERR
 * @rmtoll CMDSTAT          RdcolErr          LL_FLASH_IsActiveErr_Rdcol
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDCOL_NOERR
 *         @arg @ref LL_FLASH_RDCOL_ERR
 */
__STATIC_INLINE uint32_t LL_FLASH_IsActiveErr_Rdcol(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDSTAT, FLASH_CMDSTAT_RDCOLERR) == FLASH_CMDSTAT_RDCOLERR));
}


/**
 * @brief  Clear DONE
 * @note   This bit should not be changed when communication is ongoing.
 *         This bit set 1 to clear ACCERR.
 * @rmtoll CMDSTAT          Done          LL_FLASH_ClearFlag_Done
 * @param  FLASHx FLASH Instance
 * @param  Done This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_DONE_FINISH
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearFlag_Done(FLASH_TypeDef *FLASHx, uint32_t Done)
{
    SET_BIT(FLASHx->CMDSTAT, LL_FLASH_ERROR_CLEAR);
    MODIFY_REG(FLASHx->CMDSTAT, FLASH_CMDSTAT_DONE, Done);
}


/**
 * @brief  Get DONE
 * @rmtoll CMDSTAT          Done          LL_FLASH_IsDone
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_DONE_ING
 *         @arg @ref LL_FLASH_DONE_FINISH
 */
__STATIC_INLINE uint32_t LL_FLASH_IsDone(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDSTAT, FLASH_CMDSTAT_DONE) == FLASH_CMDSTAT_DONE));
}


/**************CMDINTE**********************/


/**
 * @brief  Set INTE_LVT
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDINTE          IntE_LVT          LL_FLASH_SetINTLVTEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LVTINT_ENABLE
 *         @arg @ref LL_FLASH_LVTINT_DISABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetINTLVTEn(FLASH_TypeDef *FLASHx, uint32_t IntE_LVT)
{
    MODIFY_REG(FLASHx->CMDINTE, FLASH_CMDINTE_LVT, IntE_LVT);
}


/**
 * @brief  Get INTE_RDCOL
 * @rmtoll CMDINTE          IntE_LVT          LL_FLASH_IsEnableLVTInt
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LVTINT_ENABLE
 *         @arg @ref LL_FLASH_LVTINT_DISABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableINT_LVT(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDINTE, FLASH_CMDINTE_LVT) == FLASH_CMDINTE_LVT));
}


/**
 * @brief  Set INTE_RDCOL
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDINTE          IntE_Rdcol          LL_FLASH_SetINTRdcolEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDCOLINT_ENABLE
 *         @arg @ref LL_FLASH_RDCOLINT_DISABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetINTRdcolEn(FLASH_TypeDef *FLASHx, uint32_t IntE_Rdcol)
{
    MODIFY_REG(FLASHx->CMDINTE, FLASH_CMDINTE_RDCOL, IntE_Rdcol);
}


/**
 * @brief  Get INTE_RDCOL
 * @rmtoll CMDINTE          IntE_Rdcol          LL_FLASH_IsEnableRdcolInt
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDCOLINT_ENABLE
 *         @arg @ref LL_FLASH_RDCOLINT_DISABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableINT_Rdcol(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDINTE, FLASH_CMDINTE_RDCOL) == FLASH_CMDINTE_RDCOL));
}


/**
 * @brief  Set INTE_DONE
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDINTE          IntE_Done          LL_FLASH_SetDoneInt
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_DONEINT_ENABLE
 *         @arg @ref LL_FLASH_DONEINT_DISABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetINTDoneEn(FLASH_TypeDef *FLASHx, uint32_t IntE_Done)
{
    MODIFY_REG(FLASHx->CMDINTE, FLASH_CMDINTE_DONE, IntE_Done);
}


/**
 * @brief  Get INTE_DONE
 * @rmtoll CMDINTE          IntE_Done          LL_FLASH_IsEnableDoneInt
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_DONEINT_ENABLE
 *         @arg @ref LL_FLASH_DONEINT_DISABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableINT_Done(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDINTE, FLASH_CMDINTE_DONE) == FLASH_CMDINTE_DONE));
}


/**************CMDINTF**********************/


/**
 * @brief  Clear LVT_IntFlag
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDINTF          LVT_IntFlag          LL_FLASH_ClearLVTIntFlag
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_FLAG_LVTIE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearFlagLVT_IT(FLASH_TypeDef *FLASHx, uint32_t LVT_IntFlag)
{
    WRITE_REG(FLASHx->CMDINTF, LVT_IntFlag);
}


/**
 * @brief  Get LVT_IntFlag
 * @rmtoll CMDINTF          LVT_IntFlag          LL_FLASH_IsActiveLVTIntFlag
 * @param  FLASHx FLASH Instance
 * @retval
 */
__STATIC_INLINE uint32_t LL_FLASH_IsActiveFlagLVT_IT(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDINTF, FLASH_CMDINTE_LVT) == FLASH_CMDINTE_LVT));
}


/**
 * @brief  Clear RDCOL_IntFlag
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDINTF         Rdcol_IntFlag          LL_FLASH_ClearRdcolIntFlag
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_FLAG_RDCOLIE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearFlagRdcol_IT(FLASH_TypeDef *FLASHx, uint32_t Rdcol_IntFlag)
{
    WRITE_REG(FLASHx->CMDINTF, Rdcol_IntFlag);
}


/**
 * @brief  Get RDCOL_IntFlag
 * @rmtoll CMDINTF          Rdcol_IntFlag          LL_FLASH_IsActiveRdcolIntFlag
 * @param  FLASHx FLASH Instance
 * @retval
 */
__STATIC_INLINE uint32_t LL_FLASH_IsActiveFlagRdcol_IT(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDINTF, FLASH_CMDINTF_RDCOL) == FLASH_CMDINTF_RDCOL));
}


/**
 * @brief  Clear DONE_IntFlag
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDINTF         Done_IntFlag          LL_FLASH_ClearDoneIntFlag
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_FLAG_DONEIE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_ClearFlagDone_IT(FLASH_TypeDef *FLASHx, uint32_t Done_IntFlag)
{
    WRITE_REG(FLASHx->CMDINTF, Done_IntFlag);
}


/**
 * @brief  Get DONE_IntFlag
 * @rmtoll CMDINTF          Done_IntFlag          LL_FLASH_IsActiveDoneIntFlag
 * @param  FLASHx FLASH Instance
 * @retval
 */
__STATIC_INLINE uint32_t LL_FLASH_IsActiveFlagDone_IT(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDINTF, FLASH_CMDINTF_DONE) == FLASH_CMDINTF_DONE));
}


/**************PORT**********************/


/**
 * @brief  Set PORT_TRHR
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PORT          tRHR          LL_FLASH_SetRHR
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RHR(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRHR(FLASH_TypeDef *FLASHx, uint32_t tRHR)
{
    MODIFY_REG(FLASHx->PORT, FLASH_PORT_TRHR, tRHR);
}


/**
 * @brief  Get PORT_TRHR
 * @rmtoll PORT          tRHR          LL_FLASH_GetRHR
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RHR(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRHR(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PORT, FLASH_PORT_TRHR)));
}


/**
 * @brief  Set PORT_TDPDH
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PORT          tDPDH          LL_FLASH_SetDPDH
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_DPDH(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetDPDH(FLASH_TypeDef *FLASHx, uint32_t tDPDH)
{
    MODIFY_REG(FLASHx->PORT, FLASH_PORT_TDPDH, tDPDH);
}


/**
 * @brief  Get PORT_TDPDH
 * @rmtoll PORT          tDPDH          LL_FLASH_GetDPDH
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_DPDH(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetDPDH(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PORT, FLASH_PORT_TDPDH)));
}


/**
 * @brief  Set PORT_TRT
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PORT          tRT          LL_FLASH_SetRT
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RT(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRT(FLASH_TypeDef *FLASHx, uint32_t tRT)
{
    MODIFY_REG(FLASHx->PORT, FLASH_PORT_TRT, tRT);
}


/**
 * @brief  Get PORT_TRT
 * @rmtoll PORT          tRT          LL_FLASH_GetRT
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_tRT_(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRT(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PORT, FLASH_PORT_TRT)));
}


/**
 * @brief  Set PORT_TDPDSR
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PORT          tDPDSR          LL_FLASH_SetDPDSR
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_tDPDSR_(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetDPDSR(FLASH_TypeDef *FLASHx, uint32_t tDPDSR)
{
    MODIFY_REG(FLASHx->PORT, FLASH_PORT_TDPDSR, tDPDSR);
}


/**
 * @brief  Get PORT_TDPDSR
 * @rmtoll PORT          tDPDSR          LL_FLASH_GetDPDSR
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_DPDSR(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetDPDSR(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PORT, FLASH_PORT_TDPDSR)));
}


/**
 * @brief  Set PORT_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PORT          PORT_Lock          LL_FLASH_LockPORTReg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PORTREG_UNLOCK
 *         @arg @ref LL_FLASH_PORTREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockPORTReg(FLASH_TypeDef *FLASHx, uint32_t PORT_Lock)
{
    MODIFY_REG(FLASHx->PORT, FLASH_PORT_LOCK, PORT_Lock);
}


/**
 * @brief  Get PORT_LOCK
 * @rmtoll PORT          PORT_Lock          LL_FLASH_IsLockPORTReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PORTREG_UNLOCK
 *         @arg @ref LL_FLASH_PORTREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockPORTReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PORT, FLASH_PORT_LOCK) == FLASH_PORT_LOCK));
}


/**************RDCFG**********************/


/**
 * @brief  Set RDCFG_RDM[15:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDCFG          RDMode          LL_FLASH_SetRDMode
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDMODE(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDMode(FLASH_TypeDef *FLASHx, uint32_t RDMode)
{
    MODIFY_REG(FLASHx->RDCFG, FLASH_RDCFG_RDM, RDMode);
}


/**
 * @brief  Get RDCFG_RDM[15:0]
 * @rmtoll RDCFG          RDMode          LL_FLASH_GetRDMode
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDMode(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRDMode(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDCFG, FLASH_RDCFG_RDM)));
}


/**
 * @brief  Set RDCFG_LPWR[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDCFG          LPWREn          LL_FLASH_SetLPWREn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LPWREN(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetLPWREn(FLASH_TypeDef *FLASHx, uint32_t LPWREn)
{
    MODIFY_REG(FLASHx->RDCFG, FLASH_RDCFG_LPWR, LPWREn);
}


/**
 * @brief  Get RDCFG_LPWR[7:0]
 * @rmtoll RDCFG          LPWREn          LL_FLASH_IsEnableLPWR
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LPWREN(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableLPWR(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDCFG, FLASH_RDCFG_LPWR) == FLASH_RDCFG_LPWR));
}


/**
 * @brief  Set RDCFG_RDBUFEN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDCFG          RDBufEn          LL_FLASH_SetRDBufEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDBUF_DISABLE
 *         @arg @ref LL_FLASH_RDBUF_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDBufEn(FLASH_TypeDef *FLASHx, uint32_t RDBufEn)
{
    MODIFY_REG(FLASHx->RDCFG, FLASH_RDCFG_RDBUFEN, RDBufEn);
}


/**
 * @brief  Get RDCFG_RDBUFEN
 * @rmtoll RDCFG          RDBufEn          LL_FLASH_IsEnableRDBuf
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDBUF_DISABLE
 *         @arg @ref LL_FLASH_RDBUF_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableRDBuf(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDCFG, FLASH_RDCFG_RDBUFEN) == FLASH_RDCFG_RDBUFEN));
}


/**
 * @brief  Set RDCFG_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDCFG          RDCFG_Lock          LL_FLASH_LockRDCFGReg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDCFGREG_UNLOCK
 *         @arg @ref LL_FLASH_RDCFGREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockRDCFGReg(FLASH_TypeDef *FLASHx, uint32_t RDCFG_Lock)
{
    MODIFY_REG(FLASHx->RDCFG, FLASH_RDCFG_LOCK, RDCFG_Lock);
}


/**
 * @brief  Get RDCFG_LOCK
 * @rmtoll RDCFG          RDCFG_Lock          LL_FLASH_IsLock_RDCFGReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDCFGREG_UNLOCK
 *         @arg @ref LL_FLASH_RDCFGREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLock_RDCFGReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDCFG, FLASH_RDCFG_LOCK) == FLASH_RDCFG_LOCK));
}


/**************RDT**********************/


/**
 * @brief  Set RDT_TMS[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDT          tMS          LL_FLASH_SetMS
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_MS(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetMS(FLASH_TypeDef *FLASHx, uint32_t tMS)
{
    MODIFY_REG(FLASHx->RDT, FLASH_RDT_TMS, tMS);
}


/**
 * @brief  Get RDT_TMS[7:0]
 * @rmtoll RDT          tMS          LL_FLASH_GetMS
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_MS(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetMS(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDT, FLASH_RDT_TMS)));
}


/**
 * @brief  Set RDT_TACCN[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDT          tAccN          LL_FLASH_SetACCN
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ACCN(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetACCN(FLASH_TypeDef *FLASHx, uint32_t tAccN)
{
    MODIFY_REG(FLASHx->RDT, FLASH_RDT_TACCN, tAccN);
}


/**
 * @brief  Get RDT_TACCN[3:0]
 * @rmtoll RDT          tAccN          LL_FLASH_GetACCN
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ACCN(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetACCN(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDT, FLASH_RDT_TACCN)));
}


/**
 * @brief  Set RDT_TACCRV[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDT          tAccRV          LL_FLASH_SetACCRV
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ACCRV(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetACCRV(FLASH_TypeDef *FLASHx, uint32_t tAccRV)
{
    MODIFY_REG(FLASHx->RDT, FLASH_RDT_TACCRV, tAccRV);
}


/**
 * @brief  Get RDT_TACCRV[3:0]
 * @rmtoll RDT          tAccRV          LL_FLASH_GetACCRV
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ACCRV(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetACCRV(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDT, FLASH_RDT_TACCRV)));
}


/**
 * @brief  Set RDT_TCKH[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDT          tCKH          LL_FLASH_SetCKH
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CKH(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCKH(FLASH_TypeDef *FLASHx, uint32_t tCKH)
{
    MODIFY_REG(FLASHx->RDT, FLASH_RDT_TCKH, tCKH);
}


/**
 * @brief  Get RDT_TCKH[3:0]
 * @rmtoll RDT          tCKH          LL_FLASH_GetCKH
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CKH(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCKH(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDT, FLASH_RDT_TCKH)));
}


/**
 * @brief  Set RDT_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDT          RDT_Lock          LL_FLASH_SetRDTRegLock
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDTREG_LOCK
 *         @arg @ref LL_FLASH_RDTREG_UNLOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockRDTReg(FLASH_TypeDef *FLASHx, uint32_t RDT_Lock)
{
    MODIFY_REG(FLASHx->RDT, FLASH_RDT_LOCK, RDT_Lock);
}


/**
 * @brief  Get RDT_LOCK
 * @rmtoll RDT          RDT_Lock          LL_FLASH_IsLockRDTReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDTREG_LOCK
 *         @arg @ref LL_FLASH_RDTREG_UNLOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockRDTReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDT, FLASH_RDT_LOCK) == FLASH_RDT_LOCK));
}


/**************LDOEN**********************/


/**
 * @brief  Set LDOEN_LDO16NUM[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll LDOEN          LDO16Num          LL_FLASH_SetLDO16iLoadNum
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LDO16NUM(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetLdo16iLoadNum(FLASH_TypeDef *FLASHx, uint32_t LDO16Num)
{
    MODIFY_REG(FLASHx->LDOEN, FLASH_LDOEN_LDO16NUM, LDO16Num);
}


/**
 * @brief  Get LDOEN_LDO16NUM[3:0]
 * @rmtoll LDOEN          LDO16Num          LL_FLASH_GetLDO16iLoadNum
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LDO16NUM(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetLDO16iLoadNum(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->LDOEN, FLASH_LDOEN_LDO16NUM)));
}


/**
 * @brief  Set LDOEN_LDO16WAIT[2:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll LDOEN          LDO16Wait          LL_FLASH_SetLDO16WaitCnt
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LDO16WAIT(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetLdo16WaitCnt(FLASH_TypeDef *FLASHx, uint32_t LDO16Wait)
{
    MODIFY_REG(FLASHx->LDOEN, FLASH_LDOEN_LDO16WAIT, LDO16Wait);
}


/**
 * @brief  Get LDOEN_LDO16WAIT[2:0]
 * @rmtoll LDOEN          LDO16Wait          LL_FLASH_GetLDO16WaitCnt
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LDO16WAIT(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetLDO16WaitCnt(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->LDOEN, FLASH_LDOEN_LDO16WAIT)));
}


/**
 * @brief  Set LDOEN_LDO16EN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll LDOEN          LDO16PreStartEn          LL_FLASH_SetLDO16PreStartEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LDO16PRESTART_DISABLE
 *         @arg @ref LL_FLASH_LDO16PRESTART_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetLdo16PreStartEn(FLASH_TypeDef *FLASHx, uint32_t LDO16PreStartEn)
{
    MODIFY_REG(FLASHx->LDOEN, FLASH_LDOEN_LDO16EN, LDO16PreStartEn);
}


/**
 * @brief  Get LDOEN_LDO16EN
 * @rmtoll LDOEN          LDO16PreStartEn          LL_FLASH_GetLDO16PreStart
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LDO16PRESTART_DISABLE
 *         @arg @ref LL_FLASH_LDO16PRESTART_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableLdo16PreStart(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->LDOEN, FLASH_LDOEN_LDO16EN) == FLASH_LDOEN_LDO16EN));
}


/**
 * @brief  Set LDOEN_LVTFCNT[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll LDOEN          LVTFilterCnt          LL_FLASH_SetLdo16LVTFilterCnt
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LVTFILTERCNT(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetLdo16LVTFilterCnt(FLASH_TypeDef *FLASHx, uint32_t LVTFilterCnt)
{
    MODIFY_REG(FLASHx->LDOEN, FLASH_LDOEN_LVTFCNT, LVTFilterCnt);
}


/**
 * @brief  Get LDOEN_LVTFCNT[3:0]
 * @rmtoll LDOEN          LVTFilterCnt          LL_FLASH_GetLdo16LVTFilterCnt
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LVTFILTERCNT(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetLDO16LVTFilterCnt(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->LDOEN, FLASH_LDOEN_LVTFCNT)));
}


/**
 * @brief  Set LDOEN_LVTOFF
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll LDOEN          LDOEN_LVTOff          LL_FLASH_SetLdo16LVTAlarmEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LVTALARM_OPEN
 *         @arg @ref LL_FLASH_LVTALARM_OFF
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetLdo16LVTAlarmEn(FLASH_TypeDef *FLASHx, uint32_t LDOEN_LVTOff)
{
    MODIFY_REG(FLASHx->LDOEN, FLASH_LDOEN_LVTOFF, LDOEN_LVTOff);
}


/**
 * @brief  Get LDOEN_LVTOFF
 * @rmtoll LDOEN          LDOEN_LVTOff          LL_FLASH_IsEnableLdo16LVTAlarm
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LVTALARM_OPEN
 *         @arg @ref LL_FLASH_LVTALARM_OFF
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableLdo16LVTAlarm(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->LDOEN, FLASH_LDOEN_LVTOFF) == FLASH_LDOEN_LVTOFF));
}


/**
 * @brief  Set LDOEN_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll LDOEN         LDOEN_Lock          LL_FLASH_LockLDOENReg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_LDOREG_UNLOCK
 *         @arg @ref LL_FLASH_LDOREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockLDOENReg(FLASH_TypeDef *FLASHx, uint32_t LDOEN_Lock)
{
    MODIFY_REG(FLASHx->LDOEN, FLASH_LDOEN_LOCK, LDOEN_Lock);
}


/**
 * @brief  Get LDOEN_LOCK
 * @rmtoll LDOEN          LDOEN_Lock          LL_FLASH_GetRegLDOENLock
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_LDOREG_UNLOCK
 *         @arg @ref LL_FLASH_LDOREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockRegLDOEN(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->LDOEN, FLASH_LDOEN_LOCK) == FLASH_LDOEN_LOCK));
}


/**************ERAST0**********************/


/**
 * @brief  Set ERAST0_TNVSE[5:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST0          tNVSE          LL_FLASH_SetNVSE
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_NVSE(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetNVSE(FLASH_TypeDef *FLASHx, uint32_t tNVSE)
{
    MODIFY_REG(FLASHx->ERAST0, FLASH_ERAST0_TNVSE, tNVSE);
}


/**
 * @brief  Get ERAST0_TNVSE[5:0]
 * @rmtoll ERAST0          tNVSE          LL_FLASH_GetNVSE
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_NVSE(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetNVSE(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST0, FLASH_ERAST0_TNVSE)));
}


/**
 * @brief  Set ERAST0_TERSC[9:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST0          tERSC          LL_FLASH_SetERSC
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ERSC(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetERSC(FLASH_TypeDef *FLASHx, uint32_t tERSC)
{
    MODIFY_REG(FLASHx->ERAST0, FLASH_ERAST0_TERSC, tERSC);
}


/**
 * @brief  Get ERAST0_TERSC[9:0]
 * @rmtoll ERAST0          tERSC          LL_FLASH_GetERSC
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ERSC(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetERSC(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST0, FLASH_ERAST0_TERSC)));
}


/**
 * @brief  Set ERAST0_TRCVE[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST0          tRCVE          LL_FLASH_SetRCVE
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RCVE(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRCVE(FLASH_TypeDef *FLASHx, uint32_t tRCVE)
{
    MODIFY_REG(FLASHx->ERAST0, FLASH_ERAST0_TRCVE, tRCVE);
}


/**
 * @brief  Get ERAST0_TRCVE[7:0]
 * @rmtoll ERAST0          tRCVE          LL_FLASH_GetRCVE
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RCVE(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRCVE(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST0, FLASH_ERAST0_TRCVE)));
}


/**
 * @brief  Set ERAST0_TRWE[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST0          tRWE          LL_FLASH_SetRWE
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RWE(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRWE(FLASH_TypeDef *FLASHx, uint32_t tRWE)
{
    MODIFY_REG(FLASHx->ERAST0, FLASH_ERAST0_TRWE, tRWE);
}


/**
 * @brief  Get ERAST0_TRWE[3:0]
 * @rmtoll ERAST0          tRCVE          LL_FLASH_GetRWE
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RWE(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRWE(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST0, FLASH_ERAST0_TRWE)));
}


/**
 * @brief  Set ERAST0_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST0         ERAST0_Lock          LL_FLASH_LockERAST0Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ERAST0REG_UNLOCK
 *         @arg @ref LL_FLASH_ERAST0REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockERAST0Reg(FLASH_TypeDef *FLASHx, uint32_t ERAST0_Lock)
{
    MODIFY_REG(FLASHx->ERAST0, FLASH_ERAST0_LOCK, ERAST0_Lock);
}


/**
 * @brief  Get ERAST0_LOCK
 * @rmtoll ERAST0          ERAST0_Lock          LL_FLASH_IsLockERAST0Reg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ERAST0REG_UNLOCK
 *         @arg @ref LL_FLASH_ERAST0REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockERAST0Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST0, FLASH_ERAST0_LOCK) == FLASH_ERAST0_LOCK));
}


/**************ERAST1**********************/


/**
 * @brief  Set ERAST1_TERSR[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST1          tERSR          LL_FLASH_SetRetrySectorErase
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RETRYSECTORERASE(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRetrySectorErase(FLASH_TypeDef *FLASHx, uint32_t tERSR)
{
    MODIFY_REG(FLASHx->ERAST1, FLASH_ERAST1_TERSR, tERSR);
}


/**
 * @brief  Get ERAST1_TERSR[7:0]
 * @rmtoll ERAST1          tERSR          LL_FLASH_GetRetrySectorErase
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RETRYSECTORERASE(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRetrySectorErase(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST1, FLASH_ERAST1_TERSR)));
}


/**
 * @brief  Set ERAST1_TERSS[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST1          tERSS          LL_FLASH_SetSingleSectorErase
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_SINGLESECTORERASE(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetSingleSectorErase(FLASH_TypeDef *FLASHx, uint32_t tERSS)
{
    MODIFY_REG(FLASHx->ERAST1, FLASH_ERAST1_TERSS, tERSS);
}


/**
 * @brief  Get ERAST1_TERSS[7:0]
 * @rmtoll ERAST1          tERSS          LL_FLASH_GetSingleSectorErase
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_SINGLESECTORERASE(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetSingleSectorErase(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST1, FLASH_ERAST1_TERSS)));
}


/**
 * @brief  Set ERAST1_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ERAST1         ERAST1_Lock          LL_FLASH_LockERAST1Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ERAST1REG_UNLOCK
 *         @arg @ref LL_FLASH_ERAST1REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockERAST1Reg(FLASH_TypeDef *FLASHx, uint32_t ERAST1_Lock)
{
    MODIFY_REG(FLASHx->ERAST1, FLASH_ERAST1_LOCK, ERAST1_Lock);
}


/**
 * @brief  Get ERAST1_LOCK
 * @rmtoll ERAST1          ERAST1_Lock          LL_FLASH_IsLockERAST1Reg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ERAST1REG_UNLOCK
 *         @arg @ref LL_FLASH_ERAST1REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockERAST1Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ERAST1, FLASH_ERAST1_LOCK) == FLASH_ERAST1_LOCK));
}


/**************PROGT0**********************/


/**
 * @brief  Set PROGT0_TNVSP[5:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT0          tNVSP          LL_FLASH_SetNVSP
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_NVSP(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetNVSP(FLASH_TypeDef *FLASHx, uint32_t tNVSP)
{
    MODIFY_REG(FLASHx->PROGT0, FLASH_PROGT0_TNVSP, tNVSP);
}


/**
 * @brief  Get PROGT0_TNVSP[5:0]
 * @rmtoll PROGT0          tNVSP          LL_FLASH_GetNVSP
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_NVSP(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetNVSP(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT0, FLASH_PROGT0_TNVSP)));
}


/**
 * @brief  Set PROGT0_TRCVP[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT0          tRCVP          LL_FLASH_SetRCVP
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RCVP(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRCVP(FLASH_TypeDef *FLASHx, uint32_t tRCVP)
{
    MODIFY_REG(FLASHx->PROGT0, FLASH_PROGT0_TRCVP, tRCVP);
}


/**
 * @brief  Get PROGT0_TRCVP[7:0]
 * @rmtoll PROGT0          tRCVP          LL_FLASH_GetRCVP
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RCVP(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRCVP(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT0, FLASH_PROGT0_TRCVP)));
}


/**
 * @brief  Set PROGT0_TRWP[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT0          tRWP          LL_FLASH_SetRWP
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RWP(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRWP(FLASH_TypeDef *FLASHx, uint32_t tRWP)
{
    MODIFY_REG(FLASHx->PROGT0, FLASH_PROGT0_TRWP, tRWP);
}


/**
 * @brief  Get PROGT0_TRWP[3:0]
 * @rmtoll PROGT0          tRWP          LL_FLASH_GetRWP
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RWP(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRWP(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT0, FLASH_PROGT0_TRWP)));
}


/**
 * @brief  Set PROGT0_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT0         PROGT0_Lock          LL_FLASH_LockPROGT0Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PROGT0REG_UNLOCK
 *         @arg @ref LL_FLASH_PROGT0REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockPROGT0Reg(FLASH_TypeDef *FLASHx, uint32_t PROGT0_Lock)
{
    MODIFY_REG(FLASHx->PROGT0, FLASH_PROGT0_LOCK, PROGT0_Lock);
}


/**
 * @brief  Get PROGT0_LOCK
 * @rmtoll PROGT0          PROGT0_Lock          LL_FLASH_IsLockPROGT0Reg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PROGT0REG_UNLOCK
 *         @arg @ref LL_FLASH_PROGT0REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockPROGT0Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT0, FLASH_PROGT0_LOCK) == FLASH_PROGT0_LOCK));
}


/**************PROGT1**********************/


/**
 * @brief  Set PROGT1_TPG[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT1          tPG          LL_FLASH_SetPG
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PG(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetPG(FLASH_TypeDef *FLASHx, uint32_t tPG)
{
    MODIFY_REG(FLASHx->PROGT1, FLASH_PROGT1_TPG, tPG);
}


/**
 * @brief  Get PROGT1_TPG[7:0]
 * @rmtoll PROGT1          tPG          LL_FLASH_GetPG
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PG(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetPG(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT1, FLASH_PROGT1_TPG)));
}


/**
 * @brief  Set PROGT1_TPGS[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT1          tPGS          LL_FLASH_SetPGS
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PGS(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetPGS(FLASH_TypeDef *FLASHx, uint32_t tPGS)
{
    MODIFY_REG(FLASHx->PROGT1, FLASH_PROGT1_TPGS, tPGS);
}


/**
 * @brief  Get PROGT1_TPGS[7:0]
 * @rmtoll PROGT1          tPGS          LL_FLASH_GetPGS
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PGS(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetPGS(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT1, FLASH_PROGT1_TPGS)));
}


/**
 * @brief  Set PROGT1_TPREPG[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT1          tPrePG          LL_FLASH_SetPrePG
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PREPG(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetPREPG(FLASH_TypeDef *FLASHx, uint32_t tPrePG)
{
    MODIFY_REG(FLASHx->PROGT1, FLASH_PROGT1_TPREPG, tPrePG);
}


/**
 * @brief  Get PROGT1_TPREPG[7:0]
 * @rmtoll PROGT1          tPrePG          LL_FLASH_GetPrePG
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PREPG(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetPREPG(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT1, FLASH_PROGT1_TPREPG)));
}


/**
 * @brief  Set PROGT1_TPREPGS[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT1          tPrePGS          LL_FLASH_SetPrePGS
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PREPGS(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetPREPGS(FLASH_TypeDef *FLASHx, uint32_t tPrePGS)
{
    MODIFY_REG(FLASHx->PROGT1, FLASH_PROGT1_TPREPGS, tPrePGS);
}


/**
 * @brief  Get PROGT1_TPREPGS[7:0]
 * @rmtoll PROGT1          tPrePGS          LL_FLASH_GetPrePGS
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PREPGS(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetPREPGS(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT1, FLASH_PROGT1_TPREPGS)));
}


/**
 * @brief  Set PROGT1_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll PROGT1         PROGT1_Lock          LL_FLASH_LockPROGT1Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PROGT1REG_UNLOCK
 *         @arg @ref LL_FLASH_PROGT1REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockPROGT1Reg(FLASH_TypeDef *FLASHx, uint32_t PROGT1_Lock)
{
    MODIFY_REG(FLASHx->PROGT1, FLASH_PROGT1_LOCK, PROGT1_Lock);
}


/**
 * @brief  Get PROGT1_LOCK
 * @rmtoll PROGT1          PROGT1_Lock          LL_FLASH_IsLockPROGT1Reg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PROGT1REG_UNLOCK
 *         @arg @ref LL_FLASH_PROGT1REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockPROGT1Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->PROGT1, FLASH_PROGT1_LOCK) == FLASH_PROGT1_LOCK));
}


/**************CFGT**********************/


/**
 * @brief  Set CFGT_TCFL[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFGT          tCFL          LL_FLASH_SetCFL
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CFL(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCFL(FLASH_TypeDef *FLASHx, uint32_t tCFL)
{
    MODIFY_REG(FLASHx->CFGT, FLASH_CFGT_TCFL, tCFL);
}


/**
 * @brief  Get CFGT_TCFL[7:0]
 * @rmtoll CFGT          tCFL          LL_FLASH_GetCFL
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CFL(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCFL(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CFGT, FLASH_CFGT_TCFL)));
}


/**
 * @brief  Set CFGT_TCFH[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFGT          tCFH          LL_FLASH_SetCFH
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CFL(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCFH(FLASH_TypeDef *FLASHx, uint32_t tCFH)
{
    MODIFY_REG(FLASHx->CFGT, FLASH_CFGT_TCFH, tCFH);
}


/**
 * @brief  Get CFGT_TCFH[3:0]
 * @rmtoll CFGT          tCFH          LL_FLASH_GetCFH
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CFL(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCFH(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CFGT, FLASH_CFGT_TCFH)));
}


/**
 * @brief  Set CFGT_TCFEN[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFGT          tCFEN          LL_FLASH_SetCFEN
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CONFEN(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCFEN(FLASH_TypeDef *FLASHx, uint32_t tCFEN)
{
    MODIFY_REG(FLASHx->CFGT, FLASH_CFGT_TCFEN, tCFEN);
}


/**
 * @brief  Get CFGT_TCFEN[3:0]
 * @rmtoll CFGT          tCFEN          LL_FLASH_GetCFEN
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CONFEN(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCFEN(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CFGT, FLASH_CFGT_TCFEN)));
}


/**
 * @brief  Set CFGT_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFGT         CFGT_Lock          LL_FLASH_LockCFGTReg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CFGTREG_UNLOCK
 *         @arg @ref LL_FLASH_CFGTREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockCFGTReg(FLASH_TypeDef *FLASHx, uint32_t CFGT_Lock)
{
    MODIFY_REG(FLASHx->CFGT, FLASH_CFGT_LOCK, CFGT_Lock);
}


/**
 * @brief  Get CFGT_LOCK
 * @rmtoll CFGT          CFGT_Lock          LL_FLASH_IsLockCFGTReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CFGTREG_UNLOCK
 *         @arg @ref LL_FLASH_CFGTREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockCFGTReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CFGT, FLASH_CFGT_LOCK) == FLASH_CFGT_LOCK));
}


/**************VRDT**********************/


/**
 * @brief  Set VRDT_TMSV[7:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll VRDT          tMSV          LL_FLASH_SetMSV
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_MSV(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetMSV(FLASH_TypeDef *FLASHx, uint32_t tMSV)
{
    MODIFY_REG(FLASHx->VRDT, FLASH_VRDT_TMSV, tMSV);
}


/**
 * @brief  Get VRDT_TMSV[7:0]
 * @rmtoll VRDT          tMSV          LL_FLASH_GetMSV
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_MSV(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetMSV(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->VRDT, FLASH_VRDT_TMSV)));
}


/**
 * @brief  Set VRDT_TACCV[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll VRDT          tACCV          LL_FLASH_SetACCV
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ACCV(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetACCV(FLASH_TypeDef *FLASHx, uint32_t tACCV)
{
    MODIFY_REG(FLASHx->VRDT, FLASH_VRDT_TACCV, tACCV);
}


/**
 * @brief  Get VRDT_TACCV[3:0]
 * @rmtoll VRDT          tACCV          LL_FLASH_GetACCV
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ACCV(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetACCV(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->VRDT, FLASH_VRDT_TACCV)));
}


/**
 * @brief  Set VRDT_TCKHV[3:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll VRDT          tCKHV          LL_FLASH_SetCKHV
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CKHV(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCKHV(FLASH_TypeDef *FLASHx, uint32_t tCKHV)
{
    MODIFY_REG(FLASHx->VRDT, FLASH_VRDT_TCKHV, tCKHV);
}


/**
 * @brief  Get VRDT_TCKHV[3:0]
 * @rmtoll VRDT          tCKHV          LL_FLASH_GetCKHV
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CKHV(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCKHV(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->VRDT, FLASH_VRDT_TCKHV)));
}


/**
 * @brief  Set VRDT_LOCK
 * @note   This bit should not be changed when LL_FLASH_SetVRDTRegLock is ongoing.
 * @rmtoll VRDT         VRDT_Lock          LL_FLASH_LockVRDTReg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_VRDTREG_UNLOCK
 *         @arg @ref LL_FLASH_VRDTREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockVRDTReg(FLASH_TypeDef *FLASHx, uint32_t VRDT_Lock)
{
    MODIFY_REG(FLASHx->VRDT, FLASH_VRDT_LOCK, VRDT_Lock);
}


/**
 * @brief  Get VRDT_LOCK
 * @rmtoll VRDT          VRDT_Lock          LL_FLASH_IsLockVRDTReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_VRDTREG_UNLOCK
 *         @arg @ref LL_FLASH_VRDTREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockVRDTReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->VRDT, FLASH_VRDT_LOCK) == FLASH_VRDT_LOCK));
}


/**************CMDCFG**********************/


/**
 * @brief  Set CMDCFG_ERRETRY[1:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG          ER_RETRY          LL_FLASH_SetEraseRetry
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ERASERETRY(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetEraseRetry(FLASH_TypeDef *FLASHx, uint32_t ER_RETRY)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_ERRETRY, ER_RETRY);
}


/**
 * @brief  Get CMDCFG_ERRETRY[1:0]
 * @rmtoll CMDCFG          ER_RETRY          LL_FLASH_GetEraseRetry
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ERASERETRY(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetEraseRetry(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_ERRETRY)));
}


/**
 * @brief  Set CMDCFG_ERWAY
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         ER_WAY          LL_FLASH_SetEraseWay
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ERASEWAY_RETRY
 *         @arg @ref LL_FLASH_ERASEWAY_SINGLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetEraseWay(FLASH_TypeDef *FLASHx, uint32_t ER_WAY)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_ERWAY, ER_WAY);
}


/**
 * @brief  Get CMDCFG_ERWAY
 * @rmtoll CMDCFG          ER_WAY          LL_FLASH_GetEraseWay
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ERASEWAY_RETRY
 *         @arg @ref LL_FLASH_ERASEWAY_SINGLE
 */
__STATIC_INLINE uint32_t LL_FLASH_GetEraseWay(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_ERWAY)));
}


/**
 * @brief  Set CMDCFG_ERVRD
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         ER_VRD          LL_FLASH_SetEraseVReadl
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ERASEVREADL_NOVERIFY
 *         @arg @ref LL_FLASH_ERASEVREADL_VERIFY
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetEraseVReadl(FLASH_TypeDef *FLASHx, uint32_t ER_VRD)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_ERVRD, ER_VRD);
}


/**
 * @brief  Get CMDCFG_ERVRD
 * @rmtoll CMDCFG          ER_VRD          LL_FLASH_GetEraseVReadl
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ERASEVREADL_NOVERIFY
 *         @arg @ref LL_FLASH_ERASEVREADL_VERIFY
 */
__STATIC_INLINE uint32_t LL_FLASH_GetEraseVReadl(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_ERVRD)));
}


/**
 * @brief  Set CMDCFG_ERNVR
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         ER_NVR          LL_FLASH_SetChipEraseNVR
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CHIPERASENVR_NO
 *         @arg @ref LL_FLASH_CHIPERASENVR_YES
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetChipEraseNVR(FLASH_TypeDef *FLASHx, uint32_t ER_NVR)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_ERNVR, ER_NVR);
}


/**
 * @brief  Get CMDCFG_ERNVR
 * @rmtoll CMDCFG          ER_NVR          LL_FLASH_GetChipEraseNVR
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CHIPERASENVR_NO
 *         @arg @ref LL_FLASH_CHIPERASENVR_YES
 */
__STATIC_INLINE uint32_t LL_FLASH_GetChipEraseNVR(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_ERNVR)));
}


/**
 * @brief  Set CMDCFG_ERSIZE
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         ER_SIZE          LL_FLASH_SetEraseSize
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ERASESIZE_SECTOR
 *         @arg @ref LL_FLASH_ERASESIZE_BLOCK
 *         @arg @ref LL_FLASH_ERASESIZE_CHIP
 *         @arg @ref LL_FLASH_ERASESIZE_SECTOR
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetEraseSize(FLASH_TypeDef *FLASHx, uint32_t ER_SIZE)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_ERSIZE, ER_SIZE);
}


/**
 * @brief  Get CMDCFG_ERSIZE
 * @rmtoll CMDCFG          ER_SIZE          LL_FLASH_GetEraseSize
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ERASESIZE_SECTOR
 *         @arg @ref LL_FLASH_ERASESIZE_BLOCK
 *         @arg @ref LL_FLASH_ERASESIZE_CHIP
 *         @arg @ref LL_FLASH_ERASESIZE_SECTOR
 */
__STATIC_INLINE uint32_t LL_FLASH_GetEraseSize(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_ERSIZE)));
}


/**
 * @brief  Set CMDCFG_VRDRECALL
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         VRD_RECALL          LL_FLASH_SetVRDRecall
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_VRDRECALL_DISABLE
 *         @arg @ref LL_FLASH_VRDRECALL_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetVRDRecall(FLASH_TypeDef *FLASHx, uint32_t VRD_RECALL)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_VRDRECALL, VRD_RECALL);
}


/**
 * @brief  Get CMDCFG_VRDRECALL
 * @rmtoll CMDCFG          VRD_RECALL          LL_FLASH_GetVRDRecall
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_VRDRECALL_DISABLE
 *         @arg @ref LL_FLASH_VRDRECALL_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_GetVRDRecall(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_VRDRECALL)));
}


/**
 * @brief  Set CMDCFG_VRDVREAD
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         VRD_VREAD          LL_FLASH_SetVRDVread
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_VRDVREAD_DISABLE
 *         @arg @ref LL_FLASH_VRDVREAD_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetVRDVread(FLASH_TypeDef *FLASHx, uint32_t VRD_VREAD)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_VRDVREAD, VRD_VREAD);
}


/**
 * @brief  Get CMDCFG_VRDVREAD
 * @rmtoll CMDCFG          VRD_VREAD          LL_FLASH_GetVRDVread
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_VRDVREAD_DISABLE
 *         @arg @ref LL_FLASH_VRDVREAD_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_GetVRDVread(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_VRDVREAD)));
}


/**
 * @brief  Set CMDCFG_VRDSIZE
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         VRD_SIZE          LL_FLASH_SetVReadSize
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_VREADSIZE(__VALUE__)
 *         @arg @ref LL_FLASH_VREAD1SECTOR                 LL_FLASH_VReadSize_(0x0)
 *         @arg @ref LL_FLASH_VREAD1BLOCK                  LL_FLASH_VReadSize_(0x1)
 *         @arg @ref LL_FLASH_VREAD1WORD                   LL_FLASH_VReadSize_(0x8)
 *         @arg @ref LL_FLASH_VREAD4WORD                   LL_FLASH_VReadSize_(0x9)
 *         @arg @ref LL_FLASH_VREAD32WORD                  LL_FLASH_VReadSize_(0xA)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetVReadSize(FLASH_TypeDef *FLASHx, uint32_t VRD_SIZE)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_VRDSIZE, VRD_SIZE);
}


/**
 * @brief  Get CMDCFG_VRDSIZE
 * @rmtoll CMDCFG          VRD_SIZE          LL_FLASH_GetVReadSize
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_VREADSIZE(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetVReadSize(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_VRDSIZE)));
}


/**
 * @brief  Set CMDCFG_PREPGEN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         PREPG_ENB          LL_FLASH_SetPreProgramEnb
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PREPROGRAM_DISABLE
 *         @arg @ref LL_FLASH_PREPROGRAM_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetPreProgram(FLASH_TypeDef *FLASHx, uint32_t PREPG_ENB)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_PREPGEN, PREPG_ENB);
}


/**
 * @brief  Get CMDCFG_PREPGEN
 * @rmtoll CMDCFG          PREPG_ENB          LL_FLASH_GetPreProgramEnb
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PREPROGRAM_DISABLE
 *         @arg @ref LL_FLASH_PREPROGRAM_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_GetPreProgram(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_PREPGEN)));
}


/**
 * @brief  Set CMDCFG_PGSIZE
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         PG_SIZE          LL_FLASH_SetProgramSize
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PROGRAMSIZE_4WORD
 *         @arg @ref LL_FLASH_PROGRAMSIZE_1WORD
 *         @arg @ref LL_FLASH_PROGRAMSIZE_32WORD
 *         @arg @ref LL_FLASH_PROGRAMSIZE_4WORD
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetProgramSize(FLASH_TypeDef *FLASHx, uint32_t PG_SIZE)
{
    MODIFY_REG(FLASHx->CMDCFG, FLASH_CMDCFG_PGSIZE, PG_SIZE);
}


/**
 * @brief  Get CMDCFG_PGSIZE
 * @rmtoll CMDCFG          PG_SIZE          LL_FLASH_GetProgramSize
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PROGRAMSIZE_4WORD
 *         @arg @ref LL_FLASH_PROGRAMSIZE_1WORD
 *         @arg @ref LL_FLASH_PROGRAMSIZE_32WORD
 *         @arg @ref LL_FLASH_PROGRAMSIZE_4WORD
 */
__STATIC_INLINE uint32_t LL_FLASH_GetProgramSize(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDCFG, FLASH_CMDCFG_PGSIZE)));
}


/**
 * @brief  Set CMDCFG_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDCFG         CMDCFG_Lock          LL_FLASH_LockCMDCFGReg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CMDCFGREG_UNLOCK
 *         @arg @ref LL_FLASH_CMDCFGREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockCMDCFGReg(FLASH_TypeDef *FLASHx, uint32_t CMDCFG_Lock)
{
    MODIFY_REG(FLASHx->VRDT, FLASH_CMDCFG_LOCK, CMDCFG_Lock);
}


/**
 * @brief  Get CMDCFG_LOCK
 * @rmtoll CMDCFG          CMDCFG_Lock          LL_FLASH_IsLockCMDCFGReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CMDCFGREG_UNLOCK
 *         @arg @ref LL_FLASH_CMDCFGREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockCMDCFGReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->VRDT, FLASH_CMDCFG_LOCK) == FLASH_CMDCFG_LOCK));
}


/**************CMDEN**********************/


/**
 * @brief  Set CMDEN_CMDEN[31:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDEN          CMDEN_CMDEN          LL_FLASH_SetCMDEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CMDEN(__VALUE__)
 *         @arg @ref LL_FLASH_CMD_DISABLE   LL_FLASH_CMDEN_(0x0)
 *         @arg @ref LL_FLASH_CMD_ENABLE  LL_FLASH_CMDEN_(0x5AA5A55AU)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCMDEn(FLASH_TypeDef *FLASHx, uint32_t CMDEN_CMDEN)
{
    WRITE_REG(FLASHx->CMDEN, CMDEN_CMDEN);
}


/**
 * @brief  Get CMDEN_CMDEN[31:0]
 * @rmtoll CMDEN          CMDEN_CMDEN          LL_FLASH_IsEnableCMD
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CMDEN(__VALUE__)
 *         @arg @ref LL_FLASH_CMD_DISABLE   LL_FLASH_CMDEN_(0x0)
 *         @arg @ref LL_FLASH_CMD_ENABLE  LL_FLASH_CMDEN_(0x5AA5A55AU)
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableCMD(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDEN, FLASH_CMDEN_CMDEN) == FLASH_CMDEN_CMDEN));
}


/**************COMM**********************/


/**
 * @brief  Set COMM_COMM[31:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll COMM          COMM_COMM          LL_FLASH_SetCMD
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_COMM(__VALUE__)
 *         @arg @ref LL_FLASH_COMM_VREAD1CHECK1        LL_FLASH_COMM_(0xA55A5AA5U)
 *         @arg @ref LL_FLASH_COMM_EREASCMD            LL_FLASH_COMM_(0x55AAAA55U)
 *         @arg @ref LL_FLASH_COMM_PROGRAMCMD          LL_FLASH_COMM_(0xAA5555AAU)
 *         @arg @ref LL_FLASH_COMM_FLASHCFG            LL_FLASH_COMM_(0x5AA5A55A)
 *         @arg @ref LL_FLASH_COMM_PROGDATACHECKCMD    LL_FLASH_COMM_(0xA5A5A5A5U)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCMD(FLASH_TypeDef *FLASHx, uint32_t COMM_COMM)
{
    WRITE_REG(FLASHx->COMM, COMM_COMM);
}


/**
 * @brief  Get COMM_COMM[31:0]
 * @rmtoll COMM          COMM_COMM          LL_FLASH_GetCMD
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_COMM(__VALUE__)
 *         @arg @ref LL_FLASH_COMM_VREAD1CHECK1        LL_FLASH_COMM_(0xA55A5AA5U)
 *         @arg @ref LL_FLASH_COMM_EREASCMD            LL_FLASH_COMM_(0x55AAAA55U)
 *         @arg @ref LL_FLASH_COMM_PROGRAMCMD          LL_FLASH_COMM_(0xAA5555AAU)
 *         @arg @ref LL_FLASH_COMM_FLASHCFG            LL_FLASH_COMM_(0x5AA5A55A)
 *         @arg @ref LL_FLASH_COMM_PROGDATACHECKCMD    LL_FLASH_COMM_(0xA5A5A5A5U)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCMD(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->COMM, FLASH_COMM_COMM)));
}


/**************CMDADDR**********************/


/**
 * @brief  Set CMD_ADDR[31:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDADDR          CMD_ADDR          LL_FLASH_SetCMDAddr
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CMDADDR(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCMDAddr(FLASH_TypeDef *FLASHx, uint32_t CMD_ADDR)
{
    WRITE_REG(FLASHx->CMDADDR, CMD_ADDR);
}


/**
 * @brief  Get CMD_ADDR[31:0]
 * @rmtoll CMDADDR          CMD_ADDR          LL_FLASH_GetCMDAddr
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CMDADDR(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCMDAddr(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDADDR, FLASH_CMDADDR_CMDADDR)));
}


/**************CMDPARA[n]**********************/


/**
 * @brief  Set CMDPARA[n][31:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDPARA[n]          CMD_PARA          LL_FLASH_SetCMDPARAn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_CMDPARAN(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetCMDPARAn(FLASH_TypeDef *FLASHx, uint8_t n, uint32_t CMD_PARA)
{
    WRITE_REG(FLASHx->CMDPARA[n], CMD_PARA);
}


/**
 * @brief  Get CMDPARA[n][31:0]
 * @rmtoll CMDPARA[n]          CMD_PARA          LL_FLASH_GetCMDParan
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_CMDPARAN(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetCMDParan(FLASH_TypeDef *FLASHx, uint8_t n)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDPARA[n], FLASH_CMDPARAn_CMDPARA)));
}


/**************CMDPF**********************/


/**
 * @brief  Set PARA_FLAG[31:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDPF          PARA_FLAG          LL_FLASH_SetParaFlag
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_PARANFLAG(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetParaFlag(FLASH_TypeDef *FLASHx, uint32_t PARA_FLAG)
{
    WRITE_REG(FLASHx->CMDPF, PARA_FLAG);
}


/**
 * @brief  Get PARA_FLAG[31:0]
 * @rmtoll CMDPF          PARA_FLAG          LL_FLASH_GetParaFlag
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_PARANFLAG(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetParaFlag(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDPF, FLASH_CMDPF_PARAnFLAG)));
}


/**
 * @brief  Set PARABitN_FLAG
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CMDPF          PARAn_FLAG          LL_FLASH_SetParaBitNFlag
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref   0x1U << n
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetParaBitNFlag(FLASH_TypeDef *FLASHx, uint8_t n)
{
    MODIFY_REG(FLASHx->CMDPF, FLASH_CMDPF_PARAnFLAG, (0x1U << n));
}


/**
 * @brief  Get PARABitN_FLAG
 * @rmtoll CMDPF          PARAn_FLAG          LL_FLASH_GetParaBitNFlag
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref   0x1U << n
 */
__STATIC_INLINE uint32_t LL_FLASH_GetParaBitNFlag(FLASH_TypeDef *FLASHx, uint8_t n)
{
    return ((uint32_t)(READ_BIT(FLASHx->CMDPF, FLASH_CMDPF_PARAnFLAG) & (0x1U << n)));
}


/**************ENCRY**********************/


/**
 * @brief  Set ENCRY_ADDRENCRYEN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ENCRY          ENCRY_AddrEncryEn          LL_FLASH_SetAddrEncryEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ADDRENCRY_DISABLE
 *         @arg @ref LL_FLASH_ADDRENCRY_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetAddrEncryEn(FLASH_TypeDef *FLASHx, uint32_t ENCRY_AddrEncryEn)
{
    MODIFY_REG(FLASHx->ENCRY, FLASH_ENCRY_ADDRENCRYEN, ENCRY_AddrEncryEn);
}


/**
 * @brief  Get ENCRY_ADDRENCRYEN
 * @rmtoll ENCRY          ENCRY_AddrEncryEn          LL_FLASH_IsEnableAddrEncry
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ADDRENCRY_DISABLE
 *         @arg @ref LL_FLASH_ADDRENCRY_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableAddrEncry(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ENCRY, FLASH_ENCRY_ADDRENCRYEN) == FLASH_ENCRY_ADDRENCRYEN));
}


/**
 * @brief  Set ENCRY_DATAENCRYEN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ENCRY          ENCRY_DataEncryEn          LL_FLASH_SetDataEncryEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_DATAENCRY_DISABLE
 *         @arg @ref LL_FLASH_DATAENCRY_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetDataEncryEn(FLASH_TypeDef *FLASHx, uint32_t ENCRY_DataEncryEn)
{
    MODIFY_REG(FLASHx->ENCRY, FLASH_ENCRY_DATAENCRYEN, ENCRY_DataEncryEn);
}


/**
 * @brief  Get ENCRY_DATAENCRYEN
 * @rmtoll ENCRY          ENCRY_DataEncryEn          LL_FLASH_IsEnableDataEncry
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_DATAENCRY_DISABLE
 *         @arg @ref LL_FLASH_DATAENCRY_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableDataEncry(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ENCRY, FLASH_ENCRY_DATAENCRYEN) == FLASH_ENCRY_DATAENCRYEN));
}


/**
 * @brief  Set ENCRY_ECCRCEN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ENCRY          ENCRY_EccCRCEn          LL_FLASH_SetEccCrcEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ECCRC_DISABLE
 *         @arg @ref LL_FLASH_ECCRC_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetEccCrcEn(FLASH_TypeDef *FLASHx, uint32_t EccCRCEn)
{
    MODIFY_REG(FLASHx->ENCRY, FLASH_ENCRY_ECCRCEN, EccCRCEn);
}


/**
 * @brief  Get ENCRY_ECCRCEN
 * @rmtoll ENCRY          ENCRY_EccCRCEn          LL_FLASH_IsEnableEccCrc
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ECCRC_DISABLE
 *         @arg @ref LL_FLASH_ECCRC_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableEccCrc(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ENCRY, FLASH_ENCRY_ECCRCEN) == FLASH_ENCRY_ECCRCEN));
}


/**
 * @brief  Set ENCRY_ECCRCPAD
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ENCRY          ENCRY_EccCRCPad          LL_FLASH_SetEccCrcPadding
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ECCRCPAD_0FILL
 *         @arg @ref LL_FLASH_ECCRCPAD_1FILL
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetEccCrcPadding(FLASH_TypeDef *FLASHx, uint32_t ENCRY_EccCRCPad)
{
    MODIFY_REG(FLASHx->ENCRY, FLASH_ENCRY_ECCRCPAD, ENCRY_EccCRCPad);
}


/**
 * @brief  Get ENCRY_ECCRCPAD
 * @rmtoll ENCRY          ENCRY_EccCRCPad          LL_FLASH_GetEccCrcPadding
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ECCRCPAD_0FILL
 *         @arg @ref LL_FLASH_ECCRCPAD_1FILL
 */
__STATIC_INLINE uint32_t LL_FLASH_GetEccCrcPadding(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ENCRY, FLASH_ENCRY_ECCRCPAD)));
}


/**
 * @brief  Set ENCRY_RMASKEN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ENCRY          ENCRY_RMaskEn          LL_FLASH_SetRMaskEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RMASK_DISABLE
 *         @arg @ref LL_FLASH_RMASK_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRMaskEn(FLASH_TypeDef *FLASHx, uint32_t ENCRY_RMaskEn)
{
    MODIFY_REG(FLASHx->ENCRY, FLASH_ENCRY_RMASKEN, ENCRY_RMaskEn);
}


/**
 * @brief  Get ENCRY_RMASKEN
 * @rmtoll ENCRY          ENCRY_RMaskEn          LL_FLASH_IsEnableRMask
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RMASK_DISABLE
 *         @arg @ref LL_FLASH_RMASK_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableRMask(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ENCRY, FLASH_ENCRY_RMASKEN) == FLASH_ENCRY_RMASKEN));
}


/**
 * @brief  Set ENCRY_WMASKEN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ENCRY          ENCRY_WMaskEn          LL_FLASH_SetWMaskEn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_WMASK_DISABLE
 *         @arg @ref LL_FLASH_WMASK_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetWMaskEn(FLASH_TypeDef *FLASHx, uint32_t ENCRY_WMaskEn)
{
    MODIFY_REG(FLASHx->ENCRY, FLASH_ENCRY_WMASKEN, ENCRY_WMaskEn);
}


/**
 * @brief  Get ENCRY_WMASKEN
 * @rmtoll ENCRY          ENCRY_WMaskEn          LL_FLASH_GetWMask
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_WMASK_DISABLE
 *         @arg @ref LL_FLASH_WMASK_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableWMask(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ENCRY, FLASH_ENCRY_WMASKEN) == FLASH_ENCRY_WMASKEN));
}


/**
 * @brief  Set ENCRY_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll ENCRY          ENCRY_Lock          LL_FLASH_LockENCRYReg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ENCRYREG_UNLOCK
 *         @arg @ref LL_FLASH_ENCRYREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockENCRYReg(FLASH_TypeDef *FLASHx, uint32_t ENCRY_Lock)
{
    MODIFY_REG(FLASHx->ENCRY, FLASH_ENCRY_LOCK, ENCRY_Lock);
}


/**
 * @brief  Get ENCRY_LOCK
 * @rmtoll ENCRY          ENCRY_Lock          LL_FLASH_IsLockENCRYReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ENCRYREG_UNLOCK
 *         @arg @ref LL_FLASH_ENCRYREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockENCRYReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->ENCRY, FLASH_ENCRY_LOCK) == FLASH_ENCRY_LOCK));
}


/**************RDNEN**********************/


/**
 * @brief  Set RDNEN_RDN0EN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDNEN          RDNEN_RDN0EN          LL_FLASH_SetRDN0En
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN0_DISABLE
 *         @arg @ref LL_FLASH_RDN0_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN0En(FLASH_TypeDef *FLASHx, uint32_t RDNEN_RDN0EN)
{
    MODIFY_REG(FLASHx->RDNEN, FLASH_RDNEN_RDN0EN, RDNEN_RDN0EN);
}


/**
 * @brief  Get RDNEN_RDN0EN
 * @rmtoll RDNEN          RDNEN_RDN0EN          LL_FLASH_IsEnableRDN0
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN0_DISABLE
 *         @arg @ref LL_FLASH_RDN0_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableRDN0(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDNEN, FLASH_RDNEN_RDN0EN) == FLASH_RDNEN_RDN0EN));
}


/**
 * @brief  Set RDNEN_RDN1EN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDNEN          RDNEN_RDN1EN          LL_FLASH_SetRDN1En
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN1_DISABLE
 *         @arg @ref LL_FLASH_RDN1_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN1En(FLASH_TypeDef *FLASHx, uint32_t RDNEN_RDN1EN)
{
    MODIFY_REG(FLASHx->RDNEN, FLASH_RDNEN_RDN1EN, RDNEN_RDN1EN);
}


/**
 * @brief  Get RDNEN_RDN1EN
 * @rmtoll RDNEN          RDNEN_RDN1EN          LL_FLASH_IsEnableRDN1
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN1_DISABLE
 *         @arg @ref LL_FLASH_RDN1_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableRDN1(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDNEN, FLASH_RDNEN_RDN1EN) == FLASH_RDNEN_RDN1EN));
}


/**
 * @brief  Set RDNEN_RDN2EN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDNEN          RDNEN_RDN2EN          LL_FLASH_SetRDN2En
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN2_DISABLE
 *         @arg @ref LL_FLASH_RDN2_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN2En(FLASH_TypeDef *FLASHx, uint32_t RDNEN_RDN2EN)
{
    MODIFY_REG(FLASHx->RDNEN, FLASH_RDNEN_RDN2EN, RDNEN_RDN2EN);
}


/**
 * @brief  Get RDNEN_RDN2EN
 * @rmtoll RDNEN          RDNEN_RDN2EN          LL_FLASH_IsEnableRDN2
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN2_DISABLE
 *         @arg @ref LL_FLASH_RDN2_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableRDN2(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDNEN, FLASH_RDNEN_RDN2EN) == FLASH_RDNEN_RDN2EN));
}


/**
 * @brief  Set RDNEN_RDN3EN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDNEN          RDNEN_RDN3EN          LL_FLASH_SetRDN3En
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN3_DISABLE
 *         @arg @ref LL_FLASH_RDN3_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN3En(FLASH_TypeDef *FLASHx, uint32_t RDNEN_RDN3EN)
{
    MODIFY_REG(FLASHx->RDNEN, FLASH_RDNEN_RDN3EN, RDNEN_RDN3EN);
}


/**
 * @brief  Get RDNEN_RDN3EN
 * @rmtoll RDNEN          RDNEN_RDN3EN          LL_FLASH_IsEnableRDN3
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN3_DISABLE
 *         @arg @ref LL_FLASH_RDN3_ENABLE
 */
__STATIC_INLINE uint32_t LL_FLASH_IsEnableRDN3(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDNEN, FLASH_RDNEN_RDN3EN) == FLASH_RDNEN_RDN3EN));
}


/**
 * @brief  Set RDNEN_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDNEN          RDNEN_LOCK          LL_FLASH_SetRDNENRegLock
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDNENREG_UNLOCK
 *         @arg @ref LL_FLASH_RDNENREG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockRDNENReg(FLASH_TypeDef *FLASHx, uint32_t RDNEN_LOCK)
{
    MODIFY_REG(FLASHx->RDNEN, FLASH_RDNEN_LOCK, RDNEN_LOCK);
}


/**
 * @brief  Get RDNEN_LOCK
 * @rmtoll RDNEN          RDNEN_LOCK          LL_FLASH_IsLockRDNENReg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDNENREG_UNLOCK
 *         @arg @ref LL_FLASH_RDNENREG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockRDNENReg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDNEN, FLASH_RDNEN_LOCK) == FLASH_RDNEN_LOCK));
}


/**************AKEY**********************/


/**
 * @brief  Set AKEY[11:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll AKEY          AKEY_AKEY          LL_FLASH_SetAKEY
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_ADDRENCRYKEY(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetAKEY(FLASH_TypeDef *FLASHx, uint32_t AKEY_AKEY)
{
    MODIFY_REG(FLASHx->AKEY, FLASH_AKEY_AKEY, AKEY_AKEY);
}


/**
 * @brief  Get AKEY[11:0]
 * @rmtoll AKEY          AKEY_AKEY          LL_FLASH_GetAKEY
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_ADDRENCRYKEY(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetAKEY(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->AKEY, FLASH_AKEY_AKEY)));
}


/**************DKEY**********************/


/**
 * @brief  Set DKEY[31:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll DKEY          DKEY_DKEY          LL_FLASH_SetDKEY
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_DATAENCRYKEY(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetDKEY(FLASH_TypeDef *FLASHx, uint32_t DKEY_DKEY)
{
    MODIFY_REG(FLASHx->DKEY, FLASH_DKEY_DKEY, DKEY_DKEY);
}


/**
 * @brief  Get DKEY[31:0]
 * @rmtoll DKEY          DKEY_DKEY          LL_FLASH_GetDKEY
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_DATAENCRYKEY(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetDKEY(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->DKEY, FLASH_DKEY_DKEY)));
}


/**************RDN0**********************/


/**
 * @brief  Set RDN0[8:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN0          RDN0_SectorAddr          LL_FLASH_SetRDN0SectorAddr
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN0ADDR(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN0SectorAddr(FLASH_TypeDef *FLASHx, uint32_t RDN0_SectorAddr)
{
    MODIFY_REG(FLASHx->RDN0, FLASH_RDNX_RDNX, RDN0_SectorAddr);
}


/**
 * @brief  Get RDN0[8:0]
 * @rmtoll RDN0          RDN0_SectorAddr          LL_FLASH_GetRDN0SectorAddr
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN0ADDR(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRDN0SectorAddr(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN0, FLASH_RDNX_RDNX)));
}


/**
 * @brief  Set RDN0_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN0          RDN0_Lock          LL_FLASH_LockRDN0Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN0REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN0REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockRDN0Reg(FLASH_TypeDef *FLASHx, uint32_t RDN0_Lock)
{
    MODIFY_REG(FLASHx->RDN0, FLASH_RDNX_LOCK, RDN0_Lock);
}


/**
 * @brief  Get RDN0_LOCK
 * @rmtoll RDN0          RDN0_Lock          LL_FLASH_IsLockRDN0Reg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN0REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN0REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockRDN0Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN0, FLASH_RDNX_LOCK) == FLASH_RDNX_LOCK));
}


/**************RDN1**********************/


/**
 * @brief  Set RDN1[8:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN1          RDN1_SectorAddr          LL_FLASH_SetRDN1SectorAddr
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN1ADDR(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN1SectorAddr(FLASH_TypeDef *FLASHx, uint32_t RDN1_SectorAddr)
{
    MODIFY_REG(FLASHx->RDN1, FLASH_RDNX_RDNX, RDN1_SectorAddr);
}


/**
 * @brief  Get RDN1[8:0]
 * @rmtoll RDN1          RDN1_SectorAddr          LL_FLASH_GetRDN1SectorAddr
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN1ADDR(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRDN1SectorAddr(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN1, FLASH_RDNX_RDNX)));
}


/**
 * @brief  Set RDN1_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN1          RDN1_Lock          LL_FLASH_LockRDN1Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN1REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN1REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockRDN1Reg(FLASH_TypeDef *FLASHx, uint32_t RDN1_Lock)
{
    MODIFY_REG(FLASHx->RDN1, FLASH_RDNX_LOCK, RDN1_Lock);
}


/**
 * @brief  Get RDN1_LOCK
 * @rmtoll RDN1          RDN1_Lock          LL_FLASH_IsLockRDN1Reg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN1REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN1REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockRDN1Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN1, FLASH_RDNX_LOCK) == FLASH_RDNX_LOCK));
}


/**************RDN2**********************/


/**
 * @brief  Set RDN2[8:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN2          RDN2_SectorAddr          LL_FLASH_SetRDN2SectorAddr
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN2ADDR(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN2SectorAddr(FLASH_TypeDef *FLASHx, uint32_t RDN2_SectorAddr)
{
    MODIFY_REG(FLASHx->RDN2, FLASH_RDNX_RDNX, RDN2_SectorAddr);
}


/**
 * @brief  Get RDN2[8:0]
 * @rmtoll RDN2          RDN2_SectorAddr          LL_FLASH_GetRDN2SectorAddr
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN2ADDR(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRDN2SectorAddr(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN2, FLASH_RDNX_RDNX)));
}


/**
 * @brief  Set RDN2_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN2          RDN2_Lock          LL_FLASH_LockRDN2Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN2REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN2REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockRDN2Reg(FLASH_TypeDef *FLASHx, uint32_t RDN2_Lock)
{
    MODIFY_REG(FLASHx->RDN2, FLASH_RDNX_LOCK, RDN2_Lock);
}


/**
 * @brief  Get RDN2_LOCK
 * @rmtoll RDN2          RDN2_Lock          LL_FLASH_GetRDN2RegLock
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN2REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN2REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockRDN2Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN2, FLASH_RDNX_LOCK) == FLASH_RDNX_LOCK));
}


/**************RDN3**********************/


/**
 * @brief  Set RDN3[8:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN3          RDN3_SectorAddr          LL_FLASH_SetRDN3SectorAddr
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN3ADDR(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_SetRDN3SectorAddr(FLASH_TypeDef *FLASHx, uint32_t RDN3_SectorAddr)
{
    MODIFY_REG(FLASHx->RDN3, FLASH_RDNX_RDNX, RDN3_SectorAddr);
}


/**
 * @brief  Get RDN3[8:0]
 * @rmtoll RDN3          RDN3_SectorAddr          LL_FLASH_GetRDN3SectorAddr
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN3ADDR(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_GetRDN3SectorAddr(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN3, FLASH_RDNX_RDNX)));
}


/**
 * @brief  Set RDN3_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll RDN3          RDN3_Lock          LL_FLASH_LockRDN3Reg
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_RDN3REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN3REG_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockRDN3Reg(FLASH_TypeDef *FLASHx, uint32_t RDN3_Lock)
{
    MODIFY_REG(FLASHx->RDN3, FLASH_RDNX_LOCK, RDN3_Lock);
}


/**
 * @brief  Get RDN3_LOCK
 * @rmtoll RDN3          RDN3_Lock          LL_FLASH_IsLockRDN3Reg
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_RDN3REG_UNLOCK
 *         @arg @ref LL_FLASH_RDN3REG_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockRDN3Reg(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->RDN3, FLASH_RDNX_LOCK) == FLASH_RDNX_LOCK));
}


/**************MAINLOCK**********************/


/**
 * @brief  Set MAINLOCK_BLKLOCK[19:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll MAINLOCK          BLK_Lock          LL_FLASH_LockMainBLK
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_MAINBLOCK(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockMainBLK(FLASH_TypeDef *FLASHx, uint32_t BLK_Lock)
{
    MODIFY_REG(FLASHx->MAINLOCK, FLASH_MBLKLOCK_BLKXLOCKB, BLK_Lock);
}


/**
 * @brief  Get MAINLOCK_BLKLOCK[19:0]
 * @rmtoll MAINLOCK          BLK_Lock          LL_FLASH_IsLockMainBLK
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_MAINBLOCK(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockMainBLK(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->MAINLOCK, FLASH_MBLKLOCK_BLKXLOCKB) == FLASH_MBLKLOCK_BLKXLOCKB));
}


/**
 * @brief  Set MAINLOCK_BLKnLOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll MAINLOCK          BLKn_Lock          LL_FLASH_LockMainBLKn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref   0x1U << n
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockMainBLKn(FLASH_TypeDef *FLASHx, uint8_t n)
{
    MODIFY_REG(FLASHx->MAINLOCK, FLASH_MBLKLOCK_BLKXLOCKB, (0x1U << n));
}


/**
 * @brief  Get MAINLOCK_BLKnLOCK
 * @rmtoll MAINLOCK          BLKn_Lock          LL_FLASH_IsLockMainBLKn
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref   0x1U << n
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockMainBLKn(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->MAINLOCK, FLASH_MBLKLOCK_BLKXLOCKB) == FLASH_MBLKLOCK_BLKXLOCKB));
}


/**************NVRULOCK**********************/


/**
 * @brief  Set NVRULOCK_NVR0LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll NVRULOCK          NVR0_Lock          LL_FLASH_LockNVR0
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref   LL_FLASH_NVR0_NOLOCK
 *         @arg @ref   LL_FLASH_NVR0_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockNVR0(FLASH_TypeDef *FLASHx, uint32_t NVR0_Lock)
{
    MODIFY_REG(FLASHx->NVRULOCK, FLASH_NVRULOCK_NVR0LOCKB, NVR0_Lock);
}


/**
 * @brief  Get NVRULOCK_NVR0LOCK
 * @rmtoll NVRULOCK          NVR0_Lock          LL_FLASH_IsLockNVR0
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref   LL_FLASH_NVR0_NOLOCK
 *         @arg @ref   LL_FLASH_NVR0_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockNVR0(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->NVRULOCK, FLASH_NVRULOCK_NVR0LOCKB) == FLASH_NVRULOCK_NVR0LOCKB));
}


/**
 * @brief  Set NVRULOCK_NVR1LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll NVRULOCK          NVR1_Lock          LL_FLASH_LockNVR1
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref   LL_FLASH_NVR1_NOLOCK
 *         @arg @ref   LL_FLASH_NVR1_LOCK
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockNVR1(FLASH_TypeDef *FLASHx, uint32_t NVR1_Lock)
{
    MODIFY_REG(FLASHx->NVRULOCK, FLASH_NVRULOCK_NVR1LOCKB, NVR1_Lock);
}


/**
 * @brief  Get NVRULOCK_NVR1LOCK
 * @rmtoll NVRULOCK          NVR1_Lock          LL_FLASH_IsLockNVR1
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref   LL_FLASH_NVR1_NOLOCK
 *         @arg @ref   LL_FLASH_NVR1_LOCK
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockNVR1(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->NVRULOCK, FLASH_NVRULOCK_NVR1LOCKB) == FLASH_NVRULOCK_NVR1LOCKB));
}


/**************OTPLOCK**********************/


/**
 * @brief  Set OTP_LOCK[31:0]
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll OTPLOCK          OTP_Lock          LL_FLASH_LockOTP
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_FLASH_OTPNVR2SECT0LOCK(__VALUE__)
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockOTP(FLASH_TypeDef *FLASHx, uint32_t OTP_Lock)
{
    MODIFY_REG(FLASHx->OTPLOCK, FLASH_OTPLOCK_OTPXLOCKB, OTP_Lock);
}


/**
 * @brief  Get OTP_LOCK[31:0]
 * @rmtoll OTPLOCK          OTP_Lock          LL_FLASH_IsLockOTP
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_FLASH_OTPNVR2SECT0LOCK(__VALUE__)
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockOTP(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->OTPLOCK, FLASH_OTPLOCK_OTPXLOCKB) == FLASH_OTPLOCK_OTPXLOCKB));
}


/**
 * @brief  Set OTPn_LOCK
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll OTPLOCK          OTPn_Lock          LL_FLASH_LockOTPn
 * @param  FLASH FLASH Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref   0x1U << n
 * @retval None
 */
__STATIC_INLINE void LL_FLASH_LockOTPn(FLASH_TypeDef *FLASHx, uint8_t n)
{
    MODIFY_REG(FLASHx->OTPLOCK, FLASH_OTPLOCK_OTPXLOCKB, (0x1U << n));
}


/**
 * @brief  Get OTPn_LOCK
 * @rmtoll OTPLOCK          OTPn_Lock          LL_FLASH_IsLockOTPn
 * @param  FLASHx FLASH Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref   0x1U << n
 */
__STATIC_INLINE uint32_t LL_FLASH_IsLockOTPn(FLASH_TypeDef *FLASHx)
{
    return ((uint32_t)(READ_BIT(FLASHx->OTPLOCK, FLASH_OTPLOCK_OTPXLOCKB) == FLASH_OTPLOCK_OTPXLOCKB));
}


FLASH_Status LL_FLASH_Erase1Sector(uint32_t Addr);


FLASH_Status LL_FLASH_Erase1Sector_Single(uint32_t Addr);


FLASH_Status LL_FLASH_Prog1Word(uint32_t Addr, uint32_t Data, uint8_t ReadCheckEn);


FLASH_Status LL_FLASH_Prog4Word(uint32_t Addr, uint32_t Data_buf[4], uint8_t ReadCheckEn);


FLASH_Status LL_FLASH_Prog32Word(uint32_t Addr, uint32_t Data_buf[32], uint8_t ReadCheckEn);


FLASH_Status LL_FLASH_4WordProDataCheck(uint32_t addr, uint32_t Data_buf[4]);


FLASH_Status LL_FLASH_EnCry_4WordBlankCheck(uint32_t Addr);


#endif /* defined (FLASH) */


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

