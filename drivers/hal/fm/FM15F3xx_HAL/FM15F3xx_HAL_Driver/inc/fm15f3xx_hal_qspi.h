/**
 ******************************************************************************
 * @file    fm15f3xx_hal_qspi.h
 * @author  MCD Application Team
 * @brief   Header file of QSPI HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_QSPI_H
#define __FM15F3xx_HAL_QSPI_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_qspi.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup QSPI
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup QSPI_Exported_Types QSPI Exported Types
 * @{
 */


/**
 * @brief  QSPI Init structure definition
 */
#define QSPI_InitTypeDef LL_QSPI_InitTypeDef


/**
 * @brief HAL QSPI State structures definition
 */
typedef enum {
    HAL_QSPI_STATE_RESET            = 0x00U,    /*!< Peripheral not initialized                            */
    HAL_QSPI_STATE_READY            = 0x01U,    /*!< Peripheral initialized and ready for use              */
    HAL_QSPI_STATE_BUSY             = 0x02U,    /*!< Peripheral in indirect mode and busy                  */
    HAL_QSPI_STATE_BUSY_INDIRECT_Tx = 0x12U,    /*!< Peripheral in indirect mode with transmission ongoing */
    HAL_QSPI_STATE_BUSY_INDIRECT_Rx = 0x22U,    /*!< Peripheral in indirect mode with reception ongoing    */
    HAL_QSPI_STATE_BUSY_MEM_MAPPED  = 0x42U,    /*!< Peripheral in memory mapped mode ongoing              */
    HAL_QSPI_STATE_ABORT            = 0x08U,    /*!< Peripheral with abort request ongoing                 */
    HAL_QSPI_STATE_ERROR            = 0x04U     /*!< Peripheral in error                                   */
} HAL_QSPI_StateTypeDef;


/**
 * @brief  QSPI Handle Structure definition
 */
typedef struct {
    QSPI_TypeDef        *Instance;              /* QSPI registers base address        */
    QSPI_InitTypeDef    Init;                   /* QSPI communication parameters      */
    uint8_t             *pTxBuffPtr;            /* Pointer to QSPI Tx transfer Buffer */
    __IO uint32_t       TxXferSize;             /* QSPI Tx Transfer size              */
    __IO uint32_t       TxXferCount;            /* QSPI Tx Transfer Counter           */
    uint8_t             *pRxBuffPtr;            /* Pointer to QSPI Rx transfer Buffer */
    __IO uint32_t       RxXferSize;             /* QSPI Rx Transfer size              */
    __IO uint32_t       RxXferCount;            /* QSPI Rx Transfer Counter           */
//  DMA_HandleTypeDef          *hdma;            /* QSPI Rx/Tx DMA Handle parameters   */
    __IO HAL_QSPI_StateTypeDef  State;          /* QSPI communication state           */
    __IO uint32_t               ErrorCode;      /* QSPI Error code                    */
} QSPI_HandleTypeDef;


/**
 * @brief  QSPI Command structure definition
 */
typedef struct {
    uint32_t Instruction;                       /* Specifies the Instruction to be sent
                                                   This parameter can be a value (8-bit) between 0x00 and 0xFF */
    uint32_t Address;                           /* Specifies the Address to be sent (Size from 1 to 4 bytes according AddressSize)
                                                   This parameter can be a value (32-bits) between 0x0 and 0xFFFFFFFF */
    uint32_t AddressSize;                       /* Specifies the Address Size
                                                   This parameter can be a value of @ref QSPI_AddressSize */
    uint32_t Alternate;                         /* Specifies the Alternate
                                                   This parameter can be a value of @ref QSPI_Alternate */
    uint32_t AlternateSize;                     /* Specifies the Alternate Bytes Size
                                                   This parameter can be a value of @ref QSPI_AlternateBytesSize */
    uint32_t DummyCycles;                       /* Specifies the Number of Dummy Cycles.
                                                   This parameter can be a number between 0 and 31 */
    uint32_t InstructionLines;                  /* Specifies the Instruction Mode
                                                   This parameter can be a value of @ref QSPI_InstructionMode */
    uint32_t AddressLines;                      /* Specifies the Address Mode
                                                   This parameter can be a value of @ref QSPI_AddressMode */
    uint32_t AlternateLines;                    /* Specifies the Alternate Bytes Mode
                                                   This parameter can be a value of @ref QSPI_AlternateBytesMode */
    uint32_t DataLines;                         /* Specifies the Data Mode (used for dummy cycles and data phases)
                                                   This parameter can be a value of @ref QSPI_DataMode */
    uint32_t NumData;                           /* Specifies the number of data to transfer.
                                                   This parameter can be any value between 0 and 0xFFFFFFFF (0 means undefined length
                                                   until end of memory)*/
    uint32_t TxOrRxData;                        /* Specifies rx or tx data. */
} QSPI_CommandTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup QSPI_Exported_Constants QSPI Exported Constants
 * @{
 */


/** @defgroup QSPI_ErrorCode QSPI Error Code
 * @{
 */
#define HAL_QSPI_ERROR_NONE             ( (uint32_t) 0x00000000U)   /*!< No error           */
#define HAL_QSPI_ERROR_TIMEOUT          ( (uint32_t) 0x00000001U)   /*!< Timeout error      */
#define HAL_QSPI_ERROR_TRANSFER         ( (uint32_t) 0x00000002U)   /*!< Transfer error     */
#define HAL_QSPI_ERROR_DMA              ( (uint32_t) 0x00000004U)   /*!< DMA transfer error */
#define HAL_QSPI_ERROR_INVALID_PARAM    ( (uint32_t) 0x00000008U)   /*!< Invalid parameters error */


/**
 * @}
 */


/** @defgroup QSPI_FlashSel
 * @{
 */
#define QSPI_ACCESS_FLASHA  LL_QSPI_ACCESS_FLASHA
#define QSPI_ACCESS_FLASHB  LL_QSPI_ACCESS_FLASHB


/**
 * @}
 */


/** @defgroup QSPI_Speed
 * @{
 */
#define QSPI_SPEED_SDR  LL_QSPI_SPEED_SDR
#define QSPI_SPEED_DDR  LL_QSPI_SPEED_DDR


/**
 * @}
 */


/** @defgroup LL_QSPI_SingleModeDateLineSel
 * @{
 */
#define QSPI_SINGLEMODE_DISABLE LL_QSPI_SINGLEMODE_DISABLE
#define QSPI_SINGLEMODE_ENABLE  LL_QSPI_SINGLEMODE_ENABLE


/**
 * @}
 */


/** @defgroup QSPI_TxBufferDataOrder QSPI_DataOrder
 * @{
 */
#define QSPI_ENDIAN_BIG     LL_QSPI_ENDIAN_BIG
#define QSPI_ENDIAN_LITTLE  LL_QSPI_ENDIAN_LITTLE


/**
 * @}
 */


/** @defgroup QSPI_TxFiFoDataOrder QSPI_RxFiFoDataOrder
 * @{
 */
#define QSPI_RxFIFO_LSB LL_QSPI_RxFIFO_LSB
#define QSPI_RxFIFO_MSB LL_QSPI_RxFIFO_MSB


/**
 * @}
 */


/** @defgroup QSPI_SetSTROBESample
 * @{
 */
#define QSPI_STROBE_DISABLE LL_QSPI_STROBE_DISABLE
#define QSPI_STROBE_ENABLE  LL_QSPI_STROBE_ENABLE


/**
 * @}
 */


/** @defgroup  QSPI_MMAPMode
 * @{
 */
#define QSPI_WORK_INDIRECT  LL_QSPI_WORK_INDIRECT
#define QSPI_WORK_MMAP      LL_QSPI_WORK_MMAP


/**
 * @}
 */


/** @defgroup QSPI_IOMode
 * @{
 */
#define QSPI_IO_HIGH    LL_QSPI_IO_HIGH
#define QSPI_IO_SYNC    LL_QSPI_IO_SYNC


/**
 * @}
 */


/** @defgroup QSPI_IOMode
 * @{
 */
#define QSPI_LINE_NONE      LL_QSPI_LINE_NONE
#define QSPI_LINE_SINGLE    LL_QSPI_LINE_SINGLE
#define QSPI_LINE_DUAL      LL_QSPI_LINE_DUAL
#define QSPI_LINE_QUAD      LL_QSPI_LINE_QUAD


/**
 * @}
 */


/** @defgroup DummyCycle
 * @{
 */
#define QSPI_DUMMY_NONE LL_QSPI_DUMMY_NONE
#define QSPI_DUMMY_1    LL_QSPI_DUMMY_1
#define QSPI_DUMMY_2    LL_QSPI_DUMMY_2
#define QSPI_DUMMY_3    LL_QSPI_DUMMY_3


/**
 * @}
 */


/** @defgroup send byte
 * @{
 */
#define QSPI_BYTE_1 LL_QSPI_BYTE_1
#define QSPI_BYTE_2 LL_QSPI_BYTE_2
#define QSPI_BYTE_3 LL_QSPI_BYTE_3
#define QSPI_BYTE_4 LL_QSPI_BYTE_4


/**
 * @}
 */


/** @defgroup QSPI_SetSendOrRec
 * @{
 */
#define QSPI_INDIRECT_RxDATA    LL_QSPI_INDIRECT_RxDATA
#define QSPI_INDIRECT_TxDATA    LL_QSPI_INDIRECT_TxDATA
#define QSPI_INDIRECT_NoneDATA  LL_QSPI_INDIRECT_RxDATA


/**
 * @}
 */


/** @defgroup QSPI_GetRxfifoStatus
 * @{
 */
#define QSPI_FLAG_RxFIFO_FULL       LL_QSPI_FLAG_RxFIFO_FULL
#define QSPI_FLAG_RxFIFO_MMAPFULL   LL_QSPI_FLAG_RxFIFO_MMAPFULL
#define QSPI_FLAG_RxFIFO_NEARFULL   LL_QSPI_FLAG_RxFIFO_NEARFULL
#define QSPI_FLAG_RxFIFO_CNT        LL_QSPI_FLAG_RxFIFO_CNT


/**
 * @}
 */


/** @defgroup QSPI_GetRxfifoStatus
 * @{
 */
#define QSPI_FLAG_TxFIFO_FULL       LL_QSPI_FLAG_TxFIFO_FULL
#define QSPI_FLAG_TxFIFO_EMPTY      LL_QSPI_FLAG_TxFIFO_EMPTY
#define QSPI_FLAG_TxFIFO_NEAREMPTY  LL_QSPI_FLAG_TxFIFO_NEAREMPTY
#define QSPI_FLAG_TxFIFO_CNT        LL_QSPI_FLAG_TxFIFO_CNT


/**
 * @}
 */


/** @defgroup QSPI_ClrStatus
 * @{
 */
#define QSPI_FLAG_CLEAR_BUFFER0     LL_QSPI_FLAG_CLEAR_BUFFER0
#define QSPI_FLAG_CLEAR_BUFFER1     LL_QSPI_FLAG_CLEAR_BUFFER1
#define QSPI_FLAG_CLEAR_TxFIFO      LL_QSPI_FLAG_CLEAR_TxFIFO
#define QSPI_FLAG_CLEAR_RxFIFO      LL_QSPI_FLAG_CLEAR_RxFIFO
#define QSPI_FLAG_CLEAR_TRANSABORT  LL_QSPI_FLAG_CLEAR_TRANSABORT


/**
 * @}
 */


/** @defgroup QSPI_SetSamplePoint
 * @{
 */
#define QSPI_SAMPLEPOINT_DISABLE    LL_QSPI_SAMPLEPOINT_DISABLE
#define QSPI_SAMPLEPOINT_1_RISING   LL_QSPI_SAMPLEPOINT_1_RISING
#define QSPI_SAMPLEPOINT_1_FALLING  LL_QSPI_SAMPLEPOINT_1_FALLING
#define QSPI_SAMPLEPOINT_2_RISING   LL_QSPI_SAMPLEPOINT_2_RISING
#define QSPI_SAMPLEPOINT_2_FALLING  LL_QSPI_SAMPLEPOINT_2_FALLING


/**
 * @}
 */


/** @defgroup QSPI_SetSampleShift
 * @{
 */
#define QSPI_SAMPLESHIFT_NONE           LL_QSPI_SAMPLESHIFT_NONE
#define QSPI_SAMPLESHIFT_HALFCYCLE      LL_QSPI_SAMPLESHIFT_HALFCYCLE
#define QSPI_SAMPLESHIFT_ONECYCLE       LL_QSPI_SAMPLESHIFT_ONECYCLE
#define QSPI_SAMPLESHIFT_ONEHALFCYCLE   LL_QSPI_SAMPLESHIFT_ONEHALFCYCLE
#define QSPI_SAMPLESHIFT_TWOCYCLE       LL_QSPI_SAMPLESHIFT_TWOCYCLE


/**
 * @}
 */


/** @defgroup QSPI_ClkMode
 * @{
 */
#define QSPI_CLK_MODE0  LL_QSPI_CLK_MODE0
#define QSPI_CLK_MODE3  LL_QSPI_CLK_MODE3


/**
 * @}
 */


/** @defgroup QSPI_ClkDiv
 * @{
 */
#define QSPI_CLK_DIV2   LL_QSPI_CLK_DIV2
#define QSPI_CLK_DIV4   LL_QSPI_CLK_DIV4
#define QSPI_CLK_DIV8   LL_QSPI_CLK_DIV8
#define QSPI_CLK_DIV16  LL_QSPI_CLK_DIV16
#define QSPI_CLK_DIV32  LL_QSPI_CLK_DIV32
#define QSPI_CLK_DIV64  LL_QSPI_CLK_DIV64


/**
 * @}
 */


/** @defgroup LL_QSPI_STALL
 * @{
 */
#define QSPI_STALL_QUITCNT_DISABLE  0x0U
#define QSPI_STALL_QUITCNT_ENABLE   LL_QSPI_STALL_QUITCNT_ENABLE


/**
 * @}
 */


/** @defgroup QSPI_GetStatus
 * @{
 */
#define QSPI_FLAG_TRANSOVER     LL_QSPI_FLAG_TRANSOVER
#define QSPI_FLAG_STALL         LL_QSPI_FLAG_STALL
#define QSPI_FLAG_TRANSABORT    LL_QSPI_FLAG_TRANSABORT


/**
 * @}
 */


/** @defgroup QSPI_INTEnable
 * @{
 */
#define QSPI_INT_TRANSOVER      LL_QSPI_INT_TRANSOVER
#define QSPI_INT_STALL          LL_QSPI_INT_STALL
#define QSPI_INT_ABORT          LL_QSPI_INT_ABORT
#define QSPI_INT_TxFIFONEMPTY   LL_QSPI_INT_TxFIFONEMPTY
#define QSPI_INT_TxFIFOEMPTY    LL_QSPI_INT_TxFIFOEMPTY
#define QSPI_INT_RxFIFONFULL    LL_QSPI_INT_RxFIFONFULL
#define QSPI_INT_RxFIFOFULL     LL_QSPI_INT_RxFIFOFULL
#define QSPI_INT_ALL            (0xF7U)


/**
 * @}
 */


/** @defgroup QSPI_Timeout_definition QSPI Timeout definition
 * @{
 */
#define HAL_QPSI_TIMEOUT_DEFAULT_VALUE ( (uint32_t) 5000)/* 5 s */


/**
 * @}
 */


/**
 * @}
 */

/* Exported macros -----------------------------------------------------------*/


/** @defgroup QSPI_Exported_Macros QSPI Exported Macros
 * @{
 */


/** @brief Reset QSPI handle state
 * @param  __HANDLE__ QSPI handle.
 * @retval None
 */
#define __HAL_QSPI_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_QSPI_STATE_RESET)


/** @brief  Enable QSPI
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @retval None
 */
#define __HAL_QSPI_ENABLE( __HANDLE__ ) SET_BIT( (__HANDLE__)->Instance->CFG, QSPI_CFG_EN )


/** @brief  Disable QSPI
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @retval None
 */
#define __HAL_QSPI_DISABLE( __HANDLE__ ) CLEAR_BIT( (__HANDLE__)->Instance->CFG, QSPI_CFG_EN )


/** @brief  Start indirect mode write
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @retval None
 */
#define __HAL_QSPI_START_INDIRECT( __HANDLE__ ) SET_BIT( (__HANDLE__)->Instance->CFG, QSPI_CFG_TXSTARTREG )


/** @brief  Enables the specified QSPI interrupt.
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @param  __INTERRUPT__ specifies the QSPI interrupt source to enable.
 * @retval None
 */
#define __HAL_QSPI_ENABLE_IT( __HANDLE__, __INTERRUPT__ ) MODIFY_REG( (__HANDLE__)->Instance->INTEN, 0xF00FF, (__INTERRUPT__) )


/** @brief  Disables the specified QSPI interrupt.
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @param  __INTERRUPT__ specifies the QSPI interrupt source to disable.
 * @retval None
 */
#define __HAL_QSPI_DISABLE_IT( __HANDLE__, __INTERRUPT__ ) CLEAR_BIT( (__HANDLE__)->Instance->INTEN, (__INTERRUPT__) )


/** @brief  Checks whether the specified QSPI interrupt source is enabled.
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @param  __INTERRUPT__ specifies the QSPI interrupt source to check.
 * @retval The new state of __INTERRUPT__ (TRUE or FALSE).
 */
#define __HAL_QSPI_GET_IT_SOURCE( __HANDLE__, __INTERRUPT__ ) (READ_BIT( (__HANDLE__)->Instance->INTEN, (__INTERRUPT__) ) == (__INTERRUPT__) )


/**
 * @brief  Get the selected QSPI's flag status.
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @param  __FLAG__ specifies the QSPI flag to check.
 * @retval None
 */
#define __HAL_QSPI_IS_ACTIVE_FLAG( __HANDLE__, __FLAG__ ) (READ_BIT( (__HANDLE__), (__FLAG__) ) == (__FLAG__) )


/** @brief  Clears the specified QSPI's flag status.
 * @param  __HANDLE__ specifies the QSPI Handle.
 * @param  __FLAG__ specifies the QSPI clear register flag
 * @retval None
 */
#define __HAL_QSPI_CLEAR_FLAG( __HANDLE__, __FLAG__ ) SET_BIT( (__HANDLE__)->Instance->SOFTCLR, (__FLAG__) ); \
    CLEAR_BIT( (__HANDLE__)->Instance->SOFTCLR, (__FLAG__) )


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup QSPI_Exported_Functions
 * @{
 */


/** @addtogroup QSPI_Exported_Functions_Group1
 * @{
 */
/* Initialization/de-initialization functions  ********************************/
HAL_StatusTypeDef HAL_QSPI_Init(QSPI_HandleTypeDef *hqspi);


HAL_StatusTypeDef HAL_QSPI_DeInit(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_MspInit(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef *hqspi);


/**
 * @}
 */


/** @addtogroup QSPI_Exported_Functions_Group2
 * @{
 */
/* IO operation functions *****************************************************/
/* QSPI IRQ handler method */
void HAL_QSPI_IRQHandler(QSPI_HandleTypeDef *hqspi);


/* QSPI indirect mode */
HAL_StatusTypeDef HAL_QSPI_Command(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd, uint32_t Timeout);


HAL_StatusTypeDef HAL_QSPI_Transmit(QSPI_HandleTypeDef *hqspi, uint8_t *pData, uint32_t Timeout);


HAL_StatusTypeDef HAL_QSPI_Receive(QSPI_HandleTypeDef *hqspi, uint8_t *pData, uint32_t Timeout);


HAL_StatusTypeDef HAL_QSPI_Command_IT(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd, uint32_t IT);


HAL_StatusTypeDef HAL_QSPI_Transmit_IT(QSPI_HandleTypeDef *hqspi, uint8_t *pData);


HAL_StatusTypeDef HAL_QSPI_Receive_IT(QSPI_HandleTypeDef *hqspi, uint8_t *pData);


/* QSPI memory-mapped mode */
HAL_StatusTypeDef HAL_QSPI_MemoryMapped(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd);


/**
 * @}
 */


/** @addtogroup QSPI_Exported_Functions_Group3
 * @{
 */
/* Callback functions in non-blocking modes ***********************************/
void HAL_QSPI_ErrorCallback(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_AbortCpltCallback(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_FifoThresholdCallback(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_QuitStallCallback(QSPI_HandleTypeDef *hqspi);


/* QSPI indirect mode */
void HAL_QSPI_CmdCpltCallback(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_RxCpltCallback(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_TxCpltCallback(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_RxHalfCpltCallback(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_TxHalfCpltCallback(QSPI_HandleTypeDef *hqspi);


/* QSPI status flag polling mode */
void HAL_QSPI_StatusMatchCallback(QSPI_HandleTypeDef *hqspi);


/* QSPI memory-mapped mode */
void HAL_QSPI_TimeOutCallback(QSPI_HandleTypeDef *hqspi);


/**
 * @}
 */


/** @addtogroup QSPI_Exported_Functions_Group4
 * @{
 */
/* Peripheral Control and State functions  ************************************/
HAL_QSPI_StateTypeDef HAL_QSPI_GetState(QSPI_HandleTypeDef *hqspi);


uint32_t HAL_QSPI_GetError(QSPI_HandleTypeDef *hqspi);


HAL_StatusTypeDef HAL_QSPI_Abort(QSPI_HandleTypeDef *hqspi, uint32_t Timeout);


HAL_StatusTypeDef HAL_QSPI_Abort_IT(QSPI_HandleTypeDef *hqspi);


void HAL_QSPI_SetTimeout(QSPI_HandleTypeDef *hqspi, uint32_t Timeout);


HAL_StatusTypeDef HAL_QSPI_SetFifoThreshold(QSPI_HandleTypeDef *hqspi, uint32_t Threshold);


uint32_t HAL_QSPI_GetFifoThreshold(QSPI_HandleTypeDef *hqspi);


/**
 * @}
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup QSPI_Private_Macros QSPI Private Macros
 * @{
 */
#define IS_QSPI_ALL_INSTANCE( INSTANCE )    ( (INSTANCE) == QSPI)
#define IS_QSPI_WORKMODE( MODE )            ( ( (MODE) == QSPI_WORK_INDIRECT) || \
                                              ( (MODE) == QSPI_WORK_MMAP) )
#define IS_QSPI_ACCESSFLASH( FLA )          ( ( (FLA) == QSPI_ACCESS_FLASHA) || \
                                              ( (FLA) == QSPI_ACCESS_FLASHB) )
#define IS_QSPI_SPEEDMODE( MODE )           ( ( (MODE) == QSPI_SPEED_SDR) || \
                                              ( (MODE) == QSPI_SPEED_DDR) )
#define IS_QSPI_ENDIANMODE( MODE )          ( ( (MODE) == QSPI_ENDIAN_BIG) || \
                                              ( (MODE) == QSPI_ENDIAN_LITTLE) )
#define IS_QSPI_SIGNIFICANTBIT( MODE )      ( ( (MODE) == QSPI_RxFIFO_LSB) || \
                                              ( (MODE) == QSPI_RxFIFO_MSB) )
#define IS_QSPI_IOMODE( MODE )              ( ( (MODE) == QSPI_IO_HIGH) || \
                                              ( (MODE) == QSPI_IO_SYNC) )
#define IS_QSPI_SPOINT( POINT )             ( ( (POINT) == QSPI_SAMPLEPOINT_DISABLE) || \
                                              ( (POINT) == QSPI_SAMPLEPOINT_1_RISING) || \
                                              ( (POINT) == QSPI_SAMPLEPOINT_1_FALLING) || \
                                              ( (POINT) == QSPI_SAMPLEPOINT_2_RISING) || \
                                              ( (POINT) == QSPI_SAMPLEPOINT_2_FALLING) )
#define IS_QSPI_SSHIF( SSHIFT )             ( ( (SSHIFT) == QSPI_SAMPLESHIFT_NONE) || \
                                              ( (SSHIFT) == QSPI_SAMPLESHIFT_HALFCYCLE) || \
                                              ( (SSHIFT) == QSPI_SAMPLESHIFT_ONECYCLE) || \
                                              ( (SSHIFT) == QSPI_SAMPLESHIFT_ONEHALFCYCLE) || \
                                              ( (SSHIFT) == QSPI_SAMPLESHIFT_TWOCYCLE) )
#define IS_QSPI_CLKMODE( CLKMODE )          ( ( (CLKMODE) == QSPI_CLK_MODE0) || \
                                              ( (CLKMODE) == QSPI_CLK_MODE3) )
#define IS_QSPI_CLKDIV( DIV )               ( ( (DIV) == QSPI_CLK_DIV2) || \
                                              ( (DIV) == QSPI_CLK_DIV4) || \
                                              ( (DIV) == QSPI_CLK_DIV8) || \
                                              ( (DIV) == QSPI_CLK_DIV16) || \
                                              ( (DIV) == QSPI_CLK_DIV32) || \
                                              ( (DIV) == QSPI_CLK_DIV64) )
#define IS_QSPI_INSTRUCTION( INSTRUCTION )  ( (INSTRUCTION) <= 0xFF)
#define IS_QSPI_ADDRESS_SIZE( SIZE )        ( ( (SIZE) == QSPI_BYTE_1) || \
                                              ( (SIZE) == QSPI_BYTE_2) || \
                                              ( (SIZE) == QSPI_BYTE_3) || \
                                              ( (SIZE) == QSPI_BYTE_4) )
#define IS_QSPI_ALTERNATE_SIZE( SIZE )      ( ( (SIZE) == QSPI_BYTE_1) || \
                                              ( (SIZE) == QSPI_BYTE_2) || \
                                              ( (SIZE) == QSPI_BYTE_3) || \
                                              ( (SIZE) == QSPI_BYTE_4) )
#define IS_QSPI_DUMMY_CYCLES( DCY )         ( (DCY) <= 31)
#define IS_QSPI_INSTRUCTION_LINES( LINE )   ( ( (LINE) == QSPI_LINE_NONE) || \
                                              ( (LINE) == QSPI_LINE_SINGLE) || \
                                              ( (LINE) == QSPI_LINE_DUAL) || \
                                              ( (LINE) == QSPI_LINE_QUAD) )
#define IS_QSPI_ADDRESS_LINES( LINE )       ( ( (LINE) == QSPI_LINE_NONE) || \
                                              ( (LINE) == QSPI_LINE_SINGLE) || \
                                              ( (LINE) == QSPI_LINE_DUAL) || \
                                              ( (LINE) == QSPI_LINE_QUAD) )
#define IS_QSPI_ALTERNATE_LINES( LINE )     ( ( (LINE) == QSPI_LINE_NONE) || \
                                              ( (LINE) == QSPI_LINE_SINGLE) || \
                                              ( (LINE) == QSPI_LINE_DUAL) || \
                                              ( (LINE) == QSPI_LINE_QUAD) )
#define IS_QSPI_DATA_LINES( LINE )          ( ( (LINE) == QSPI_LINE_NONE) || \
                                              ( (LINE) == QSPI_LINE_SINGLE) || \
                                              ( (LINE) == QSPI_LINE_DUAL) || \
                                              ( (LINE) == QSPI_LINE_QUAD) )
#define IS_QSPI_FUNCTIONAL_MODE( MODE )     ( ( (MODE) == QSPI_INDIRECT_TxDATA) || \
                                              ( (MODE) == QSPI_INDIRECT_RxDATA) )


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup QSPI_Private_Functions QSPI Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_QSPI_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
