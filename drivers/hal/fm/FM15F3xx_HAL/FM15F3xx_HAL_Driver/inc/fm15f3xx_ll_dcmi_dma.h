/**
  ******************************************************************************
  * @file    fm15f3xx_ll_stimer.h
  * @author  SRG
  * @version V1.0.0
  * @date    2020-03-17
  * @brief
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */

#ifndef FM15F3XX_LL_DCMI_DMA_H
#define FM15F3XX_LL_DCMI_DMA_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


/** @defgroup DCMI_DMA_BIN Macros
  * @{
  */

/**
  * @}
  */

/** @defgroup Bit definition Macros
  * @{
  */

/*******************  Bit definition for dcmi_dma_cfg_b register  ********************/
#define DCMI_DMA_CFG_B_RCVEN_Pos        (0U)
#define DCMI_DMA_CFG_B_RCVEN_Msk        (0x1U << DCMI_DMA_CFG_B_RCVEN_Pos)      /*!< 0x00000001 */
#define DCMI_DMA_CFG_B_RCVEN            DCMI_DMA_CFG_B_RCVEN_Msk                /*!< rcv transfer enable */

#define DCMI_DMA_CFG_B_CYCLEEN_Pos      (4U)
#define DCMI_DMA_CFG_B_CYCLEEN_Msk      (0x1U << DCMI_DMA_CFG_B_CYCLEEN_Pos)    /*!< 0x00000010 */
#define DCMI_DMA_CFG_B_CYCLEEN          DCMI_DMA_CFG_B_CYCLEEN_Msk              /*!< rcv auto cycle enable */


#define DCMI_DMA_CFG_B_BINARYEN_Pos     (8U)
#define DCMI_DMA_CFG_B_BINARYEN_Msk     (0x1U << DCMI_DMA_CFG_B_BINARYEN_Pos)   /*!< 0x00000100 */
#define DCMI_DMA_CFG_B_BINARYEN         DCMI_DMA_CFG_B_BINARYEN_Msk             /*!< auto bit transfer enable */


/*******************  Bit definition for dcmi_dma_cfg_c register  ********************/
#define DCMI_DMA_CFG_C_RDWORDEN_Pos     (0U)
#define DCMI_DMA_CFG_C_RDWORDEN_Msk     (0x1U << DCMI_DMA_CFG_C_RDWORDEN_Pos)   /*!< 0x00000001 */
#define DCMI_DMA_CFG_C_RDWORDEN         DCMI_DMA_CFG_C_RDWORDEN_Msk             /*!< csr_rd_buf_en */

#define DCMI_DMA_CFG_C_BIGENDIAN_Pos    (1U)
#define DCMI_DMA_CFG_C_BIGENDIAN_Msk    (0x1U << DCMI_DMA_CFG_C_BIGENDIAN_Pos)  /*!< 0x00000002 */
#define DCMI_DMA_CFG_C_BIGENDIAN        DCMI_DMA_CFG_C_BIGENDIAN_Msk            /*!< csr_rd_buf_endian */

#define DCMI_DMA_CFG_C_BINARYMSB_Pos    (2U)
#define DCMI_DMA_CFG_C_BINARYMSB_Msk    (0x1U << DCMI_DMA_CFG_C_BINARYMSB_Pos)  /*!< 0x00000004 */
#define DCMI_DMA_CFG_C_BINARYMSB        DCMI_DMA_CFG_C_BINARYMSB_Msk            /*!< csr_wr_bit_endian */

#define DCMI_DMA_CFG_C_WRBINPAD_Pos     (3U)
#define DCMI_DMA_CFG_C_WRBINPAD_Msk     (0x1U << DCMI_DMA_CFG_C_WRBINPAD_Pos)   /*!< 0x00000008 */
#define DCMI_DMA_CFG_C_WRBINPAD         DCMI_DMA_CFG_C_WRBINPAD_Msk             /*!< csr_wr_bit_ret_mode */

#define DCMI_DMA_CFG_C_MEDIANEN_POS     (4U)
#define DCMI_DMA_CFG_C_MEDIANEN_Msk     (0x1U << DCMI_DMA_CFG_C_MEDIANEN_POS)   /*!< 0x00000010 */
#define DCMI_DMA_CFG_C_MEDIANEN         DCMI_DMA_CFG_C_MEDIANEN_Msk             /*!< csr_median_filter_en */

#define DCMI_DMA_CFG_C_WIDTH_POS        (24U)
#define DCMI_DMA_CFG_C_HEIGHT_POS       (16U)
#define DCMI_DMA_CFG_C_WSIZE_POS        (8U)


/*******************  Bit definition for dcmi_dma_auto_result_cfg_b register  ********************/

#define DCMI_DMA_AUTO_RES_CFG_B_BITREV_POS      (10U)
#define DCMI_DMA_AUTO_RES_CFG_B_BITREV_Msk      (0x1U << DCMI_DMA_AUTO_RES_CFG_B_BITREV_POS)    /*!< 0x00000400 */
#define DCMI_DMA_AUTO_RES_CFG_B_BITREV          DCMI_DMA_AUTO_RES_CFG_B_BITREV_Msk              /*!< csr_bit_reverse */

#define DCMI_DMA_AUTO_RES_CFG_B_SAMPLE_POS      (9U)
#define DCMI_DMA_AUTO_RES_CFG_B_SAMPLE_Msk      (0x1U << DCMI_DMA_AUTO_RES_CFG_B_SAMPLE_POS)    /*!< 0x00000200 */
#define DCMI_DMA_AUTO_RES_CFG_B_SAMPLE          DCMI_DMA_AUTO_RES_CFG_B_SAMPLE_Msk              /*!< csr_bit_simple_mode */

#define DCMI_DMA_AUTO_RES_CFG_B_ADJSIGN_POS     (8U)
#define DCMI_DMA_AUTO_RES_CFG_B_ADJSIGN_Msk     (0x1U << DCMI_DMA_AUTO_RES_CFG_B_ADJSIGN_POS)   /*!< 0x00000100 */
#define DCMI_DMA_AUTO_RES_CFG_B_ADJSIGN         DCMI_DMA_AUTO_RES_CFG_B_ADJSIGN_Msk             /*!< csr_bit_adj_dir */

#define LL_DCMI_DMA_ADJ_PLUS                    (1<<DCMI_DMA_AUTO_RES_CFG_B_ADJSIGN_POS)
#define LL_DCMI_DMA_ADJ_MINUS                   0

#define LL_DCMI_DMA_MODE_SAMPLE                 (1<<DCMI_DMA_AUTO_RES_CFG_B_SAMPLE_POS)
#define LL_DCMI_DMA_MODE_NORMAL                 0

/*******************  Bit definition for dcmi_dma_auto_result_cfg_a register  ********************/

#define DCMI_DMA_STATUS_FINISHED_POS		(31U)
#define DCMI_DMA_STATUS_FINISHED_Msk		(0x1U << DCMI_DMA_STATUS_FINISHED_POS)    /*!< 0x80000000 */
#define DCMI_DMA_STATUS_FINISHED			DCMI_DMA_STATUS_FINISHED_Msk              /*!< recv_finished */

#define DCMI_DMA_STATUS_BUSY_POS			(30U)
#define DCMI_DMA_STATUS_BUSY_Msk			(0x1U << DCMI_DMA_STATUS_BUSY_POS)		/*!< 0x40000000 */
#define DCMI_DMA_STATUS_BUSY				DCMI_DMA_STATUS_BUSY_Msk				/*!< recv_busy */



/*******************  Bit definition for dcmi_dma_auto_result_cfg_b register  ********************/

#define DCMI_DMA_STATUS_BIN_FINISHED_POS	(31U)
#define DCMI_DMA_STATUS_BIN_FINISHED_Msk	(0x1U << DCMI_DMA_STATUS_BIN_FINISHED_POS)    /*!< 0x80000000 */
#define DCMI_DMA_STATUS_BIN_FINISHED          DCMI_DMA_STATUS_BIN_FINISHED_Msk              /*!< autobit_finished */

#define DCMI_DMA_STATUS_BIN_BUSY_POS		(30U)
#define DCMI_DMA_STATUS_BIN_BUSY_Msk		(0x1U << DCMI_DMA_STATUS_BIN_BUSY_POS)    /*!< 0x40000000 */
#define DCMI_DMA_STATUS_BIN_BUSY			DCMI_DMA_STATUS_BIN_BUSY_Msk              /*!< autobit_busy */



#define LL_DCMI_DMA_STATUS_FINISHED         DCMI_DMA_STATUS_FINISHED
#define LL_DCMI_DMA_STATUS_BUSY             DCMI_DMA_STATUS_BUSY

#define LL_DCMI_DMA_STATUS_BIN_FINISHED    	DCMI_DMA_STATUS_BIN_FINISHED
#define LL_DCMI_DMA_STATUS_BIN_BUSY         DCMI_DMA_STATUS_BIN_BUSY

/**
  * @}
  */


/**
  * @brief  Disable DCMI_DMA DSP.
  * @rmtoll LL_DCMI_DMA_DisableDSP
  * @param  None
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_DisableDSP(void)
{
    WRITE_REG(DCMI_DMA->cfg_a, 0);
}

/**
  * @brief  Disable DCMI_DMA CSR.
  * @rmtoll LL_DCMI_DMA_DisableCR
  * @param  None
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_DisableCR(void)
{
    WRITE_REG(DCMI_DMA->cfg_b, 0);
}


/**
  * @brief  DCMI_DMA config windows.
  * @rmtoll LL_DCMI_DMA_ConfigWin
  * @param  config_cr
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_CrCfg(uint32_t config_cr)
{
    WRITE_REG(DCMI_DMA->cfg_b, config_cr);
}

/**
  * @brief  DCMI_DMA config windows.
  * @rmtoll LL_DCMI_DMA_ConfigWin
  * @param  config_win
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_ConfigWin(uint32_t config_win)
{
    WRITE_REG(DCMI_DMA->cfg_c, config_win);
}

/**
  * @brief  DCMI_DMA address of the dcmi data buf.
  * @rmtoll LL_DCMI_DMA_ADDR
  * @param  addr
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_ADDR(uint32_t addr)
{
    WRITE_REG(DCMI_DMA->rcv_cfg_a, addr);
}


/**
  * @brief  DCMI_DMA size of the dcmi data buf.
  * @rmtoll LL_DCMI_DMA_SIZE
  * @param  cycle_size,max_size
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_SIZE(uint32_t cycle_size, uint32_t max_size)
{
    WRITE_REG(DCMI_DMA->rcv_cfg_b, ((cycle_size / 256) << 16) | (max_size / 256));
}

/**
  * @brief  DCMI_DMA address of the dcmi tmp data buf.
  * @rmtoll LL_DCMI_DMA_ADDR
  * @param  addr
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_TMP_ADDR(uint32_t addr)
{
    WRITE_REG(DCMI_DMA->auto_buf_lu_cfg_a, addr);
}


/**
  * @brief  DCMI_DMA size of the dcmi tmp data buf.
  * @rmtoll LL_DCMI_DMA_TMP_SIZE
  * @param  tmp_size
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_TMP_SIZE(uint32_t tmp_size)
{
    WRITE_REG(DCMI_DMA->auto_buf_lu_cfg_b, (tmp_size / 256));
}


/**
  * @brief  DCMI_DMA address of the dcmi bin data buf.
  * @rmtoll LL_DCMI_DMA_BIN_ADDR
  * @param  addr
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_BIN_ADDR(uint32_t addr)
{
    WRITE_REG(DCMI_DMA->auto_result_cfg_a, addr);
}


/**
  * @brief  DCMI_DMA size of the dcmi bin data buf.
  * @rmtoll LL_DCMI_DMA_BIN_SIZE
  * @param  bin_size,
            adj_sign(0 : cmp_byte = org_byte - adj_byte,1 : cmp_byte = org_byte + adj_byte),
            adj_byte
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_BIN_SIZE(uint32_t bin_size, uint32_t bin_config, uint8_t adj_byte)
{
    WRITE_REG(DCMI_DMA->auto_result_cfg_b, ((bin_size / 256) << 16) | bin_config | adj_byte);
}


/**
  * @brief  Disable DCMI_DMA int.
  * @rmtoll LL_DCMI_DMA_DisableInt
  * @param  None
  * @retval None
  */
__STATIC_INLINE void LL_DCMI_DMA_DisableInt(void)
{
    WRITE_REG(DCMI_DMA->int_en, 0);
}


/**
  * @brief  Check if the DCMI DMA Status is set or not.
  * @rmtoll STATUS               LL_DCMI_DMA_GetStatus
  * @param  flag
  *         @arg @ref LL_DCMI_DMA_STATUS_FINISHED
  *         @arg @ref LL_DCMI_DMA_STATUS_BUSY
  * @retval State of bit (1 or 0).
  */
__STATIC_INLINE uint32_t LL_DCMI_DMA_GetStatus(uint32_t flag)
{
    return (READ_BIT(DCMI_DMA->status_a, flag) == (flag));
}

/**
  * @brief  Check if the DCMI DMA BIN Status is set or not.
  * @rmtoll STATUS               LL_DCMI_DMA_GetStatus
  * @param  flag
  *         @arg @ref LL_DCMI_DMA_STATUS_BIN_FINISHED
  *         @arg @ref LL_DCMI_DMA_STATUS_BIN_BUSY
  * @retval State of bit (1 or 0).
  */
__STATIC_INLINE uint32_t LL_DCMI_DMA_GetBinStatus(uint32_t flag)
{
    return (READ_BIT(DCMI_DMA->status_b, flag) == (flag));
}


/**
  * @brief  GET count of recv bytes in DCMI DMA Status.
  * @rmtoll STATUS   LL_DCMI_DMA_GetStatusBytes
  * @param  
  * @retval byte_count (20bit).
  */
__STATIC_INLINE uint32_t LL_DCMI_DMA_GetStatusBytes(void)
{
    return (READ_REG(DCMI_DMA->status_a)&0xFFFFF);
}

/**
  * @brief  GET count of bin bytes in DCMI DMA Status.
  * @rmtoll STATUS   LL_DCMI_DMA_GetStatusBinBytes
  * @param  
  * @retval byte_count (20bit).
  */
__STATIC_INLINE uint32_t LL_DCMI_DMA_GetBinStatusBytes(void)
{
    return (READ_REG(DCMI_DMA->status_b)&0xFFFFF);
}

/**
  * @brief  Clear DCMI DMA ��
  * @rmtoll STATUS               LL_DCMI_DMA_Clr
  * @param  NONE
  * @retval NONE.
  */
__STATIC_INLINE void LL_DCMI_DMA_Clr(void)
{
    WRITE_REG(DCMI_DMA->clr, 1);
}




/* Exported functions --------------------------------------------------------*/

void LL_DCMI_DMA_WinConfig(uint16_t colomn, uint16_t raw, uint8_t winsize);


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
