/**
  ******************************************************************************
  * @file    fm15f3xx_ll_dma.h
  * @author  SRG
  * @version V1.0.0
  * @date    2020-03-17
  * @brief
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */

#ifndef FM15F3XX_LL_DMA_H
#define FM15F3XX_LL_DMA_H
#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"
#include "fm15f3xx_typedef.h"

/** @addtogroup FM15F3XX_LL_Driver
  * @{
  */

#if defined (DMA)

/** @defgroup DMA_LL DMA
  * @{
  */

/* Exported typedef --------------------------------------------------------*/
/** @defgroup DMA_LL_ExADCed_Functions DMA Functions
  * @{
  */

/**
  * @brief  DMA transfer width enumeration
  */
typedef enum _dma_transfer_width {
    DMA_TRANSFERWIDTH_8BIT   = 0x0U,
    DMA_TRANSFERWIDTH_16BIT  = 0x1U,
    DMA_TRANSFERWIDTH_32BIT  = 0x2U,
    DMA_TRANSFERWIDTH_32BIT_ = 0x3U
} DmaTransferWidth_t;

/**
  * @brief  DMA transfer burst enumeration
  */
typedef enum _dma_transfer_burst {
    DMA_TRANSFERBURST_SINGLE = 0x0U,
    DMA_TRANSFERBURST_INC4   = 0x1U,
    DMA_TRANSFERBURST_INC8   = 0x2U,
    DMA_TRANSFERBURST_INC16  = 0x3U,
} DmaTransferBurst_t;

/**
  * @brief  DMA modulo enumeration
  */
typedef enum _dma_modulo {
    DMA_MODULO_DISABLE = 0x0U, /*!< Disable modulo */
    DMA_MODULO_2BYTES,         /*!< Circular buffer size is 2 bytes.   */
    DMA_MODULO_4BYTES,         /*!< Circular buffer size is 4 bytes.   */
    DMA_MODULO_8BYTES,         /*!< Circular buffer size is 8 bytes.   */
    DMA_MODULO_16BYTES,        /*!< Circular buffer size is 16 bytes.  */
    DMA_MODULO_32BYTES,        /*!< Circular buffer size is 32 bytes.  */
    DMA_MODULO_64BYTES,        /*!< Circular buffer size is 64 bytes.  */
    DMA_MODULO_128BYTES,       /*!< Circular buffer size is 128 bytes. */
    DMA_MODULO_256BYTES,       /*!< Circular buffer size is 256 bytes. */
    DMA_MODULO_512BYTES,       /*!< Circular buffer size is 512 bytes. */
    DMA_MODULO_1KBYTES,        /*!< Circular buffer size is 1K bytes.  */
    DMA_MODULO_2KBYTES,        /*!< Circular buffer size is 2K bytes.  */
    DMA_MODULO_4KBYTES,        /*!< Circular buffer size is 4K bytes.  */
    DMA_MODULO_8KBYTES,        /*!< Circular buffer size is 8K bytes.  */
    DMA_MODULO_16KBYTES,       /*!< Circular buffer size is 16K bytes. */
    DMA_MODULO_32KBYTES,       /*!< Circular buffer size is 32K bytes. */
} DmaModulo_t;

/**
  * @brief  DMA transfer type enumeration
  */
typedef enum _dma_transfer_type {
    DMA_MEMORYtoMEMORY = 0x0U,
    DMA_PERIPHERALtoMEMORY,
    DMA_MEMORYtoPERIPHERAL,
} DmaTransferType_t;

/**
  * @brief  DMA TcdAttr structures
  */
typedef struct _dma_trans_attr {
    DmaTransferWidth_t SrcTransferWidth;
    DmaTransferWidth_t DestTransferWidth;
    DmaTransferBurst_t SrcTransferBurst;
    DmaTransferBurst_t DestTransferBurst;
    DmaModulo_t Smod;
    DmaModulo_t Dmod;
    int8_t SrcOffset;
    int8_t DestOffset;
} Dma_TcdAttr_t;

/**
  * @brief  DMA Tcd Minor Loop structures
  */
typedef struct _dma_trans_MinorLoop {
    uint32_t MinorLoopBytes;
    FunctionalState  SmolEn;
    FunctionalState  DmolEn;
    uint32_t Moff;
    FunctionalState  MinorLinkEn;
    uint8_t  MinorLinkch;
} DmaTcdMinorLoop_t;

/**
  * @brief  DMA Tcd Major Loop structures
  */
typedef struct _dma_trans_MajorLoop {
    uint32_t MajorLoopCounts;
    FunctionalState  MajorLinkEn;
    uint8_t  MajorLinkch;
    FunctionalState  TcdScatterEn;
    int16_t SLastSGA;
    int16_t DLastSGA;
} DmaTcdMajorLoop_t;

/**
  * @brief  DMA Transfer Config structures
  */
typedef struct _dma_transfer_config {
    uint32_t SrcAddr;
    uint32_t DestAddr;
    Dma_TcdAttr_t TcdAttr;
    DmaTcdMinorLoop_t TcdMinorLoop;
    DmaTcdMajorLoop_t TcdMajorLoop;
} LL_DMA_TransferConfig;

/**
  * @}
  */
/* Exported constants --------------------------------------------------------*/
/** @defgroup DMA_CHANNEL
  * @{
  */
#define LL_DMA_CHANNEL_0        (0x00U)
#define LL_DMA_CHANNEL_1        (0x01U)
#define LL_DMA_CHANNEL_2        (0x02U)
#define LL_DMA_CHANNEL_3        (0x03U)
#define LL_DMA_CHANNEL_4        (0x04U)
#define LL_DMA_CHANNEL_5        (0x05U)
#define LL_DMA_CHANNEL_6        (0x06U)
#define LL_DMA_CHANNEL_7        (0x07U)

/**
  * @}
  */

/** @defgroup DMA_Transfer_PRIORITY DMA Transfer PRIORITY
 * @{
 */
#define LL_DMA_PRIORITY_POLLING (0x100 | DMA_CR_ERCA)
#define LL_DMA_PRIORITY_FIX_0       (0x0U)
#define LL_DMA_PRIORITY_FIX_1       (0x1U)
#define LL_DMA_PRIORITY_FIX_2       (0x2U)
#define LL_DMA_PRIORITY_FIX_3       (0x3U)
#define LL_DMA_PRIORITY_FIX_4       (0x4U)
#define LL_DMA_PRIORITY_FIX_5       (0x5U)
#define LL_DMA_PRIORITY_FIX_6       (0x6U)
#define LL_DMA_PRIORITY_FIX_7       (0x7U)

/**
 * @}
 */
/** @defgroup DMA_interrupt_enable_definitions DMA interrupt enable definitions
 * @brief    DMA interrupts definition
 * @{
 */
#define LL_DMA_IT_DISABLE                     (0x00U)
#define LL_DMA_IT_ENABLE                  (0x01U)
/**
 * @}
 */

/** @defgroup DMA_interrupt_enable_definitions DMA interrupt enable definitions
 * @brief    DMA interrupts definition
 * @{
 */
#define LL_DMA_IT_TC                      (DMA_WORD6_INTMAJ)
#define LL_DMA_IT_MAJORLOOP_HT      (DMA_WORD6_INTHALF)
#define LL_DMA_IT_ERR                     (0xFFU)
#define LL_DMA_IT_MAJORLOOP_MASK    (DMA_WORD6_INTMAJ | DMA_WORD6_INTHALF)
#define LL_DMA_IT_ERR_MASK            LL_DMA_IT_ERR
/**
 * @}
 */

/** @defgroup DMA_flag_definitions DMA flag definitions
 * @brief    DMA flag definitions
 * @{
 */
#define LL_DMA_FLAG_ERR_BUS               DMA_ES_BE
#define LL_DMA_FLAG_ERR_SG                  DMA_ES_SGE
#define LL_DMA_FLAG_ERR_MAJORLOOPNUM    DMA_ES_NCE
#define LL_DMA_FLAG_ERR_DSTOFFSET         DMA_ES_DOE
#define LL_DMA_FLAG_ERR_DSTADDR         DMA_ES_DAE
#define LL_DMA_FLAG_ERR_SRCOFFSET         DMA_ES_SOE
#define LL_DMA_FLAG_ERR_SRCADDR         DMA_ES_SAE
#define LL_DMA_FLAG_ERR_CHANNEL         DMA_ES_ERRCHN
#define LL_DMA_FLAG_ERR_PRIOR               DMA_ES_CPE
#define LL_DMA_FLAG_ERR_CANCELTX          DMA_ES_ECX
#define LL_DMA_FLAG_ERR                     DMA_ES_VLD


/**
 * @}
 */

/** @defgroup DMA_TRIG_Source
 * @{
 */
#define LL_DMA_TRIG_SOFTWARE            (0x00U)
#define LL_DMA_TRIG_UART0_RX_NOEMPTY    (0x01U)
#define LL_DMA_TRIG_UART0_TX_EMPTY      (0x02U)
#define LL_DMA_TRIG_UART1_RX_NOEMPTY    (0x03U)
#define LL_DMA_TRIG_UART1_TX_EMPTY      (0x04U)
#define LL_DMA_TRIG_UART2_RX_NOEMPTY    (0x05U)
#define LL_DMA_TRIG_UART2_TX_EMPTY      (0x06U)
#define LL_DMA_TRIG_SPI0_RX_NOEMPTY (0x09U)
#define LL_DMA_TRIG_SPI0_TX_EMPTY       (0x0AU)
#define LL_DMA_TRIG_SPI1_RX_NOEMPTY (0x0BU)
#define LL_DMA_TRIG_SPI1_TX_EMPTY       (0x0CU)
#define LL_DMA_TRIG_SPI2_RX_NOEMPTY (0x0DU)
#define LL_DMA_TRIG_SPI2_TX_EMPTY       (0x0EU)
#define LL_DMA_TRIG_SPI3_RX_NOEMPTY (0x0FU)
#define LL_DMA_TRIG_SPI3_TX_EMPTY       (0x10U)
#define LL_DMA_TRIG_I2C0_RX_NOEMPTY (0x11U)
#define LL_DMA_TRIG_I2C0_TX_EMPTY       (0x12U)
#define LL_DMA_TRIG_I2C1_RX_NOEMPTY (0x13U)
#define LL_DMA_TRIG_I2C1_TX_EMPTY       (0x14U)
#define LL_DMA_TRIG_CT0_RX_NOEMPTY      (0x19U)
#define LL_DMA_TRIG_CT0_TX_EMPTY        (0x1AU)
#define LL_DMA_TRIG_CT1_RX_NOEMPTY      (0x1BU)
#define LL_DMA_TRIG_CT1_TX_EMPTY        (0x1CU)
#define LL_DMA_TRIG_QSPI_RX_NOEMPTY (0x1DU)
#define LL_DMA_TRIG_QSPI_TX_EMPTY       (0x1EU)
#define LL_DMA_TRIG_FSMC_RX_NOEMPTY (0x1FU)
#define LL_DMA_TRIG_FSMC_TX_EMPTY       (0x20U)
#define LL_DMA_TRIG_STIMER0_TRIG        (0x22U)
#define LL_DMA_TRIG_STIMER1_TRIG        (0x23U)
#define LL_DMA_TRIG_STIMER2_TRIG        (0x24U)
#define LL_DMA_TRIG_STIMER3_TRIG        (0x25U)
#define LL_DMA_TRIG_STIMER4_TRIG        (0x26U)
#define LL_DMA_TRIG_STIMER5_TRIG        (0x27U)
#define LL_DMA_TRIG_SECURTY_DMA     (0x2AU)
#define LL_DMA_TRIG_DCMI_DMA            (0x2BU)
#define LL_DMA_TRIG_GPIOA_EDGE          (0x2CU)
#define LL_DMA_TRIG_GPIOB_EDGE          (0x2DU)
#define LL_DMA_TRIG_GPIOC_EDGE          (0x2EU)
#define LL_DMA_TRIG_GPIOD_EDGE          (0x2FU)
#define LL_DMA_TRIG_GPIOE_EDGE          (0x30U)
#define LL_DMA_TRIG_GPIOF_EDGE          (0x31U)
#define LL_DMA_TRIG_PAE_FINISH          (0x32U)
#define LL_DMA_TRIG_BCA_FINISH          (0x33U)
#define LL_DMA_TRIG_HASH_CUR_COMPUTE    (0x34U)
#define LL_DMA_TRIG_HASH_LAST_COMPUTE   (0x35U)
#define LL_DMA_TRIG_ADC_FINISH          (0x36U)
#define LL_DMA_TRIG_DAC_WATERMARK       (0x37U)
#define LL_DMA_TRIG_DAC_TOP         (0x38U)
#define LL_DMA_TRIG_DAC_BOTTOM          (0x39U)
#define LL_DMA_TRIG_CMP_EDGE            (0x3AU)
#define LL_DMA_TRIG_ALARM               (0x3BU)
#define LL_DMA_TRIG_DMA_MUX0            (0x3CU)
#define LL_DMA_TRIG_DMA_MUX1            (0x3DU)
#define LL_DMA_TRIG_DMA_MUX2            (0x3EU)
#define LL_DMA_TRIG_DMA_MUX3            (0x3FU)
/**
  * @}
  */

/**
 * @brief  Enable/Disable the specified DMA Stream.
 * @param  DMA channel
 * @retval None
 */
#define LL_DMA_MUX_ENABLE( channel )    (DMAMUX->CHCFG[(channel)] |=  BUSMAT_CHCFG_ENBL)
#define LL_DMA_MUX_DISABLE( channel ) (DMAMUX->CHCFG[(channel)] &= ~BUSMAT_CHCFG_ENBL)

/**
 * @brief  GET/CLEAR the DMA xfer Done.
 * @param  DMA channel
 * @retval None
 */
#define LL_DMA_GET_DONE( channel )    ( DMA->DONE &   (1 << (channel)) )
#define LL_DMA_CLEAR_DONE( channel )    ( DMA->DONE &= ~(1 << (channel)) )

/**
 * @brief  GET the DMA xfer Active.
 * @param  DMA channel
 * @retval None
 */
#define LL_DMA_GET_ACTIVE( channel) ( DMA->ACTIVE &   (1 << (channel)) )

/**
 * @brief   Abort the specified DMA Stream.
 * @retval None
 */
#define LL_DMA_ABORT(  ) ( DMA->CR |= DMA_CR_CX)

/**
 * @brief  Enable/Disable the specified DMA interrupts.
 * @param  channel DMA Channel
 * @param  INT
 * @retval None
 */
#define LL_DMA_ENABLE_IT( channel, INT )    ( ( (INT) == LL_DMA_IT_ERR) ? ( DMA->EEI |=  (0x1 << (channel)) ) : ( DMA->TCD[(channel)].CSR |=  ( (INT) & 0x06) ) )
#define LL_DMA_DISABLE_IT( channel, INT)    ( ( (INT) == LL_DMA_IT_ERR) ? ( DMA->EEI &= ~(0x1 << (channel)) ) : ( DMA->TCD[(channel)].CSR &= ~( (INT) & 0x06) ) )


/**
 * @brief  Get/Clear the Int Flag.
 * @param  DMA channel
 * @retval None
 */
#define LL_DMA_GET_FLAG_IT( channel )       ( DMA->INT &   (1 << (Channel)) )
#define LL_DMA_CLEAR_FLAG_IT( channel ) ( DMA->INT &= ~(1 << (Channel)) )

/**
 * @brief  GET/CLEAR the DMA Error Flag.
 * @param  DMA channel
 * @param  DMA FLAG
 * @retval None
 */
#define LL_DMA_GET_FLAG_ERR(  FLAG )            ( DMA->ES  & (FLAG) )
#define LL_DMA_CLEAR_FLAG_ERR( channel )      ( DMA->INT &= ~(1 << (Channel)) )
/**
 * @brief  GET/CLEAR the DMA xfer Error.
 * @retval None
 */
#define LL_DMA_GET_ERR(  )                      ( DMA->ERR )
#define LL_DMA_CLEAR_ERR(  )                    ( DMA->ERR &(~LL_DMA_IT_ERR) )


/**
 * @brief  GET the DMA Trig Source.
 * @param  DMA channel
 * @retval None   DMAMUX->CHCFG[channel]
 */
#define LL_DMA_GET_TRIGSRC( channel )               ( DMAMUX->CHCFG[channel] & 63 )

/**
 * @brief  Get MajorLoop number of remaining.
 * @param  DMA channel
 * @retval remaining Number
 */
#define  LL_DMA_GetMajorLoopCiter( channel )  ( (DMA->TCD[channel].BITER_CITER_ELINKNO & 0x7FFF0000) >> 16)


void LL_DMA_Init(uint32_t Channel, LL_DMA_TransferConfig *DMA_TransferStruct, dma_tcd_t *NextTcd);
void LL_DMA_ConfigTcd(dma_tcd_t *tcd, uint32_t channel, LL_DMA_TransferConfig * config, dma_tcd_t *nextTcd);
void DMA_SetTransferConfig(uint32_t channel, LL_DMA_TransferConfig *config, dma_tcd_t *nextTcd);
ErrorStatus LL_DMA_Start(uint32_t Channel, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum);
ErrorStatus LL_DMA_Start_IT(uint32_t Channel, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum, uint32_t IT);
void LL_DMA_IRQHandler(uint32_t Channel);
void LL_DMA_Error_IRQHandler(void);
/**
  * @}
  */

#endif /* defined (DMA) */


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
