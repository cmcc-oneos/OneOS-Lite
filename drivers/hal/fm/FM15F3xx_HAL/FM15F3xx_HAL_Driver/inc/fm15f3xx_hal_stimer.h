/**
 ******************************************************************************
 * @file    fm15f3xx_hal_stimer.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-06-29
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_STIM_H
#define __FM15F3xx_HAL_STIM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_stimer.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup TIM
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup TIM_Exported_Types TIM Exported Types
 * @{
 */


/**
 * @brief  TIM Time base Configuration Structure definition
 */
typedef struct {
    STn_Type STx;                       /*!< Specifies the number of st .
                                             This parameter can be a value between 0~5.*/

    uint32_t Prescaler;                 /*!< Specifies the prescaler value used to divide the TIM clock.
                                             This parameter can be a number between Min_Data = 0x0000U and Max_Data = 0xFFFFU */

    uint32_t WorkMode;                  /*!< Specifies the Work mode.
                                             This parameter can be a value of @ref TIM_Work_Mode */

    uint32_t CountMode;                 /*!< Specifies the counter mode.
                                             This parameter can be a value of @ref TIM_Counter_Mode */

    uint32_t CountPolarity;             /*!< Specifies the counter mode.
                                             This parameter can be a value of @ref TIM_Counter_Polarity */

    uint32_t AutoReload0;               /*!< Specifies the period value to be loaded into the active
                                             Auto-Reload Register at the next update event.
                                             This parameter can be a number between Min_Data = 0x0000U and Max_Data = 0xFFFFFFFF.  */

    uint32_t AutoReload1;               /*!< Specifies the period value to be loaded into the active
                                             Auto-Reload Register at the next update event.
                                             This parameter can be a number between Min_Data = 0x0000U and Max_Data = 0xFFFFFFFF.  */
} TIM_Base_InitTypeDef;


/**
 * @brief  TIM TGS Configuration Structure definition
 */
typedef struct {
    uint32_t Function;                  /*!< Specifies tgs function select.*/

    uint32_t Channel;                   /*!< Specifies trigger channel.*/

    uint32_t Action;                    /*!< Specifies trigger action.*/

    uint32_t ClkSel;                    /*!< Specifies the input clock select. */

    uint32_t TrigSrc;                   /*!< Specifies the input srouce. */

    uint32_t TrigEdge;                  /*!< Specifies the active edge of the input signal.*/

    uint32_t InputFilter;               /*!< Specifies the input capture filter.*/

    uint32_t InputInvert;               /*!< Specifies the active edge of the input polarity. */
} TIM_TGS_InitTypeDef;


/**
 * @brief  HAL State structures definition
 */
typedef enum {
    HAL_TIM_STATE_RESET     = 0x00U,    /*!< Peripheral not yet initialized or disabled  */
    HAL_TIM_STATE_READY     = 0x01U,    /*!< Peripheral Initialized and ready for use    */
    HAL_TIM_STATE_BUSY      = 0x02U,    /*!< An internal process is ongoing              */
    HAL_TIM_STATE_TIMEOUT   = 0x03U,    /*!< Timeout state                               */
    HAL_TIM_STATE_ERROR     = 0x04U     /*!< Reception process is ongoing                */
} HAL_TIM_StateTypeDef;


/**
 * @brief  TIM Time Base Handle Structure definition
 */
typedef struct {
    TIM_Base_InitTypeDef Init;          /*!< TIM Time Base required parameters */
//  DMA_HandleTypeDef           *hdma[7];      /*!< DMA Handlers array
//                                             This array is accessed by a @ref DMA_Handle_index */
    __IO HAL_TIM_StateTypeDef State;    /*!< TIM operation state               */
} TIM_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup TIM_Exported_Constants  TIM Exported Constants
 * @{
 */


/** @defgroup STIM_PSC Select simple timer prescaler
 * @{
 */
#define TIM_WORKCLK_DIV1    (0x0U << ST_PSC_STPSCCFG_Pos)
#define TIM_WORKCLK_DIV2    ( (0x1U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV4    ( (0x2U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV8    ( (0x3U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV16   ( (0x4U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV32   ( (0x5U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV64   ( (0x6U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV128  ( (0x7U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV256  ( (0x8U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)
#define TIM_WORKCLK_DIV512  ( (0x9U << ST_PSC_STPSCCFG_Pos) | ST_PSC_STPSCEN)


/**
 * @}
 */


/** @defgroup STIM_WORK_MODE Select Work Mode
 * @{
 */
#define TIM_WORKMODE_MODCNT     (0x0U << ST_CFG_MODE_Pos)                       /*!< MOD Count  */
#define TIM_WORKMODE_ONEPULSE   ( (0x0U << ST_CFG_MODE_Pos) | ST_CFG_OUTPULSE)  /*!< MOD Count One Pulse Mode */
#define TIM_WORKMODE_FREECNT    (0x1U << ST_CFG_MODE_Pos)                       /*!< Free Count */
#define TIM_WORKMODE_PWM        (0x2U << ST_CFG_MODE_Pos)                       /*!< PWM     */
#define TIM_WORKMODE_TGS        (0x3U << ST_CFG_MODE_Pos)                       /*!< Trigger Gather System */


/**
 * @}
 */


/** @defgroup STIM_COUNTER_MODE Select Count Mode
 * @{
 */
#define TIM_CNTMODE_CONTINUES   (0x0U)
#define TIM_CNTMODE_ONESHOT     (0x1U << ST_CFG_OSM_Pos)


/**
 * @}
 */


/** @defgroup STIM_STOP_MODE Stop Mode
 * @{
 */
#define TIM_STOPMODE_CNT_FREEZE     (0x0U)
#define TIM_STOPMODE_CNT_NOFREEZE   (0x1U << ST_CFG_OSM_Pos)


/**
 * @}
 */


/** @defgroup STIM_Output_Counter_Polarity TIM Output Counter Polarity
 * @{
 */
#define TIM_CNTPOLARITY_LOW     (0x0000U)
#define TIM_CNTPOLARITY_HIGH    (ST_CFG_OUTINV)


/**
 * @}
 */


/** @defgroup STIM_ITorDMA_SELECT STIM IT or DMA Select
 * @{
 */
#define TIM_REQ_IT  (ST_CFG_INTDMAE)
#define TIM_REQ_DMA (ST_CFG_DMASEL | ST_CFG_INTDMAE)


/**
 * @}
 */


/** @defgroup STIM_TGS_PADINV Select polarity of trigger gather sysytem pad
 * @{
 */
#define TIM_TGS_INPUT_NOINVERT  (0x0U << ST_TGSCH_PADINV)
#define TIM_TGS_INPUT_INVERT    (0x1U << ST_TGSCH_PADINV)


/**
 * @}
 */


/** @defgroup STIM_TGS_EDGE Select edge of trigger gather system input
 * @{
 */
#define TIM_TGS_EDGE_HIGH       (0x0U << ST_TGSCH_PADEDGE_Pos)
#define TIM_TGS_EDGE_RISING     (0x1U << ST_TGSCH_PADEDGE_Pos)
#define TIM_TGS_EDGE_FALLING    (0x2U << ST_TGSCH_PADEDGE_Pos)
#define TIM_TGS_EDGE_BOTHEDGE   (0x3U << ST_TGSCH_PADEDGE_Pos)


/**
 * @}
 */


/** @defgroup STIM_TRIN_ACTION Select action of trigger
 * @{
 */
#define TIM_TRIG_ACTION_STOP    (0x0U << ST_TRINCFG_ACTION_Pos)
#define TIM_TRIG_ACTION_START   (0x1U << ST_TRINCFG_ACTION_Pos)
#define TIM_TRIG_ACTION_PAUSE   (0x2U << ST_TRINCFG_ACTION_Pos)
#define TIM_TRIG_ACTION_COUNT   (0x3U << ST_TRINCFG_ACTION_Pos)


/**
 * @}
 */


/** @defgroup STIM_TRIN_ACTION Select action of fault
 * @{
 */
#define TIM_FAULT_ACTION_HOLDCNT    (0x0U << ST_FAULTCFG_RESET_Pos)
#define TIM_FAULT_ACTION_RESETCNT   (0x1U << ST_FAULTCFG_RESET_Pos)


/**
 * @}
 */


/** @defgroup STIM_TGS_FUNCTION Select tgs function
 * @{
 */
#define TIM_TGS_FUNCTION_TRIG   (0x0U)
#define TIM_TGS_FUNCTION_FAULT  (0x1U)


/**
 * @}
 */


/** @defgroup STIM_TRIN_TGS  Select tgs trigger channel
 * @{
 */
#define TIM_TGS_CH0 (0x0U << ST_TRINCFG_TGSCH_Pos)
#define TIM_TGS_CH1 (0x1U << ST_TRINCFG_TGSCH_Pos)
#define TIM_TGS_CH2 (0x2U << ST_TRINCFG_TGSCH_Pos)
#define TIM_TGS_CH3 (0x3U << ST_TRINCFG_TGSCH_Pos)
#define TIM_TGS_CH4 (0x4U << ST_TRINCFG_TGSCH_Pos)
#define TIM_TGS_CH5 (0x5U << ST_TRINCFG_TGSCH_Pos)
#define TIM_TGS_CH6 (0x6U << ST_TRINCFG_TGSCH_Pos)
#define TIM_TGS_CH7 (0x7U << ST_TRINCFG_TGSCH_Pos)


/**
 * @}
 */


/** @defgroup STIM_TGS_CLK Select CLK of trigger gather system input
 * @{
 */
#define TIM_TGS_CLK_ST0 (0x0U << ST_TGSCH_INCLKSEL_Pos)
#define TIM_TGS_CLK_ST1 (0x1U << ST_TGSCH_INCLKSEL_Pos)
#define TIM_TGS_CLK_ST2 (0x2U << ST_TGSCH_INCLKSEL_Pos)
#define TIM_TGS_CLK_ST3 (0x3U << ST_TGSCH_INCLKSEL_Pos)
#define TIM_TGS_CLK_ST4 (0x4U << ST_TGSCH_INCLKSEL_Pos)
#define TIM_TGS_CLK_ST5 (0x5U << ST_TGSCH_INCLKSEL_Pos)
#define TIM_TGS_CLK_BUS (0x6U << ST_TGSCH_INCLKSEL_Pos)
#define TIM_TGS_CLK_SYS (0x7U << ST_TGSCH_INCLKSEL_Pos)


/**
 * @}
 */


/** @defgroup STIM_TGS_Source Select CLK of trigger gather system input
 * @{
 */
#define TIM_TGS_TRIGSRC_SW                      (0x00U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_ST0                     (0x01U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_ST1                     (0x02U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_ST2                     (0x03U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_ST3                     (0x04U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_ST4                     (0x05U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_ST5                     (0x06U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_DMA_CH0                 (0x07U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_DMA_CH1                 (0x08U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_DMA_CH2                 (0x09U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_DMA_CH3                 (0x0AU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_CMP                     (0x0BU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_DAC_FINISH              (0x0CU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_ADC_CH0_FINISH          (0x0DU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_UART0_RXFIFO_NONEMPTY   (0x0EU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_UART0_TXFIFO_EMPTY      (0x0FU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_UART1_RXFIFO_NONEMPTY   (0x10U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_UART1_TXFIFO_EMPTY      (0x11U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_I2C0_RXBUF_NONEMPTY     (0x12U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_I2C0_TXBUF_EMPTY        (0x13U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_SPI0_RXFIFO_NONEMPTY    (0x14U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_SPI0_TXFIFO_EMPTY       (0x15U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_SPI1_RXFIFO_NONEMPTY    (0x16U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_SPI1_TXFIFO_EMPTY       (0x17U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_CT0_RXFIFO_NONEMPTY     (0x18U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_CT0_TXFIFO_EMPTY        (0x19U << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_GPIO0                   (0x1AU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_GPIO1                   (0x1BU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_GPIO2                   (0x1CU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_GPIO3                   (0x1DU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_GPIO4                   (0x1EU << ST_TGSCH_INSOURCE_Pos)
#define TIM_TGS_TRIGSRC_GPIO5                   (0x1FU << ST_TGSCH_INSOURCE_Pos)


/**
 * @}
 */


/** @defgroup STIM_CHAIN  select Chain
 * @{
 */
#define TIM_CHAIN_0TO1  (0x01U)
#define TIM_CHAIN_0TO2  (0x02U)
#define TIM_CHAIN_0TO3  (0x03U)
#define TIM_CHAIN_0TO4  (0x04U)
#define TIM_CHAIN_0TO5  (0x05U)


/**
 * @}
 */


/** @defgroup STIM_FlAG IT Flag
 * @{
 */
#define TIM_FLAG_ST0    ST_FLAG_ST0FLAG
#define TIM_FLAG_ST1    ST_FLAG_ST1FLAG
#define TIM_FLAG_ST2    ST_FLAG_ST2FLAG
#define TIM_FLAG_ST3    ST_FLAG_ST3FLAG
#define TIM_FLAG_ST4    ST_FLAG_ST4FLAG
#define TIM_FLAG_ST5    ST_FLAG_ST5FLAG


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup TIM_Exported_Macros TIM Exported Macros
 * @{
 */


/** @brief Reset TIM handle state
 * @param  __HANDLE__ TIM handle
 * @retval None
 */
#define __HAL_TIM_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_TIM_STATE_RESET)


/**
 * @brief  Enable the TIM peripheral.
 * @param  __TIMx__ TIM handle
 * @retval None
 */
#define __HAL_TIM_ENABLE( __TIMx__ ) (STIMER->CTRL |= (1 << (__TIMx__ * 4) ) )


/**
 * @brief  Disable the TIM peripheral.
 * @param  __TIMx__ TIM handle
 * @retval None
 */
#define __HAL_TIM_DISABLE( __TIMx__ ) (STIMER->CTRL &= ~(1 << (__TIMx__ * 4) ) )


/**
 * @brief  Reset the TIM peripheral.
 * @param  __TIMx__ TIM handle
 * @retval None
 */
#define __HAL_TIM_RESET( __TIMx__ ) (STIMER->RESET |= (1 << (__TIMx__ * 4) ) )


/** @brief  Enable the specified TIM interrupt.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_ENABLE_IT( __TIMx__ ) (STIMER->CFG[__TIMx__] &= ~(TIM_REQ_DMA) ); \
    (STIMER->CFG[__TIMx__] |= TIM_REQ_IT)


/** @brief  Disable the specified TIM interrupt.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_DISABLE_IT( __TIMx__ ) (STIMER->CFG[__TIMx__] &= ~(TIM_REQ_IT) )


/** @brief  Enable the specified TIM DMA.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_ENABLE_DMA( __TIMx__ ) (STIMER->CFG[__TIMx__] &= ~(TIM_REQ_DMA) ); \
    (STIMER->CFG[__TIMx__] |= TIM_REQ_DMA)


/** @brief  Disable the specified TIM DMA.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_DISABLE_DMA( __TIMx__ ) (STIMER->CFG[__TIMx__] &= ~(TIM_REQ_DMA) )


/** @brief  Enable the specified TIM Trigger.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_ENABLE_TRIG( __TIMx__ ) (STIMER->TRINCFG[__TIMx__] |= ST_TRINCFG_EN)


/** @brief  Disable the specified TIM Trigger.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_DISABLE_TRIG( __TIMx__ ) (STIMER->TRINCFG[__TIMx__] &= ~ST_TRINCFG_EN)


/** @brief  Enable the specified TIM Fault.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_ENABLE_FAULT( __TIMx__ ) (STIMER->FAULTCFG[__TIMx__] |= ST_FAULTCFG_EN)


/** @brief  Disable the specified TIM Fault.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_DISABLE_FAULT( __TIMx__ ) (STIMER->FAULTCFG[__TIMx__] &= ~ST_FAULTCFG_EN)


/** @brief  TGS Software Trigger.
 * @param  __TIMx__ specifies the TIM Handle.
 * @retval None
 */
#define __HAL_TIM_TGS_SWTRIG( __CHANNELx__ ) (STIMER->TGSSWTRIG |= (1 << (__CHANNELx__ * 4) ) )


/** @brief  Check whether the specified TIM interrupt flag is set or not.
 * @param  __HANDLE__ specifies the TIM Handle.
 * @param  __FLAG__ specifies the TIM interrupt flag to clear.
 *        This parameter can be one of the following values:
 *            @arg ST0:  interrupt flag
 *            @arg ST1:  interrupt flag
 *            @arg ST2:  interrupt flag
 *            @arg ST3:  interrupt flag
 *            @arg ST4:  interrupt flag
 *            @arg ST5:  interrupt flag
 * @retval The new __FLAG__ (TRUE or FALSE).
 */
#define __HAL_TIM_GET_FLAG( __FLAG__ ) ( (STIMER->FLAG & (__FLAG__) ) == (__FLAG__) )


/**
 * @brief  Check whether the specified TIM interrupt source is enabled or not.
 * @param  __HANDLE__ TIM handle
 * @param  __INTERRUPT__ specifies the TIM interrupt source to check.
 *        This parameter can be one of the following values:
 *            @arg ST0:  interrupt
 *            @arg ST1:  interrupt
 *            @arg ST2:  interrupt
 *            @arg ST3:  interrupt
 *            @arg ST4:  interrupt
 *            @arg ST5:  interrupt
 * @retval The state of TIM_IT (SET or RESET).
 */
#define __HAL_TIM_GET_IT_SOURCE( __TIMx__ ) ( (STIMER->CFG[__TIMx__] & (TIM_REQ_DMA) ) == (TIM_REQ_IT) )


/** @brief  Clear the specified TIM interrupt flag.
 * @param  __HANDLE__ specifies the TIM Handle.
 * @param  __FLAG__ specifies the TIM interrupt flag to clear.
 *        This parameter can be one of the following values:
 *            @arg TIM_FLAG_ST0:  interrupt flag
 *            @arg TIM_FLAG_ST1:  interrupt flag
 *            @arg TIM_FLAG_ST2:  interrupt flag
 *            @arg TIM_FLAG_ST3:  interrupt flag
 *            @arg TIM_FLAG_ST4:  interrupt flag
 *            @arg TIM_FLAG_ST5:  interrupt flag
 * @retval The new state of __FLAG__ (TRUE or FALSE).
 */
#define __HAL_TIM_CLEAR_IT( __FLAG__ ) (STIMER->IFCL |= (__FLAG__) )


/**
 * @}
 */


/* Exported functions --------------------------------------------------------*/


/** @addtogroup TIM_Exported_Functions
 * @{
 */


/** @addtogroup TIM_Exported_Functions_Group1
 * @{
 */

/* Time Base functions ********************************************************/
HAL_StatusTypeDef HAL_TIM_Base_Init(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_Base_DeInit(TIM_HandleTypeDef *htim);


void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim);


void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef *htim);


/* Blocking mode: Polling */
HAL_StatusTypeDef HAL_TIM_Base_Start(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_Base_Stop(TIM_HandleTypeDef *htim);


/* Non-Blocking mode: Interrupt */
HAL_StatusTypeDef HAL_TIM_Base_Start_IT(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_Base_Stop_IT(TIM_HandleTypeDef *htim);


/* Non-Blocking mode: DMA */
HAL_StatusTypeDef HAL_TIM_Base_Start_DMA(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_Base_Stop_DMA(TIM_HandleTypeDef *htim);


/**
 * @}
 */


/** @addtogroup TIM_Exported_Functions_Group3
 * @{
 */
/* Timer Input Capture functions ***********************************************/
HAL_StatusTypeDef HAL_TIM_TGS_Init(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_TGS_DeInit(TIM_HandleTypeDef *htim);


void HAL_TIM_TGS_MspInit(TIM_HandleTypeDef *htim);


void HAL_TIM_TGS_MspDeInit(TIM_HandleTypeDef *htim);


/* Blocking mode: Polling */
HAL_StatusTypeDef HAL_TIM_TRIG_Start(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_TRIG_Stop(TIM_HandleTypeDef *htim);


/* Non-Blocking mode: Interrupt */
HAL_StatusTypeDef HAL_TIM_TRIG_Start_IT(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_TRIG_Stop_IT(TIM_HandleTypeDef *htim);


/* Blocking mode: Polling */
HAL_StatusTypeDef HAL_TIM_Fault_Start(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_Fault_Stop(TIM_HandleTypeDef *htim);


/* Non-Blocking mode: Interrupt */
HAL_StatusTypeDef HAL_TIM_Fault_Start_IT(TIM_HandleTypeDef *htim);


HAL_StatusTypeDef HAL_TIM_Fault_Stop_IT(TIM_HandleTypeDef *htim);


/**
 * @}
 */


/** @addtogroup TIM_Exported_Functions_Group4
 * @{
 */
/* Interrupt Handler functions  **********************************************/
void HAL_TIM_IRQHandler(TIM_HandleTypeDef *htim);


/**
 * @}
 */


/** @addtogroup TIM_Exported_Functions_Group5
 * @{
 */
/* Control functions  *********************************************************/
HAL_StatusTypeDef HAL_TIM_TGS_ConfigChannel(TIM_HandleTypeDef *htim, TIM_TGS_InitTypeDef* sConfig);


HAL_StatusTypeDef HAL_TIM_Chain_Start(uint32_t Chain);


void HAL_TIM_SetStopModeCountFreeze(STn_Type TIMx, uint8_t Freeze);


uint32_t HAL_TIM_GetCountValue(STn_Type TIMx);


/**
 * @}
 */


/** @addtogroup TIM_Exported_Functions_Group6
 * @{
 */
/* Callback in non blocking modes (Interrupt) *************************/
void HAL_TIM_ST0Callback(TIM_HandleTypeDef *htim);


void HAL_TIM_ST1Callback(TIM_HandleTypeDef *htim);


void HAL_TIM_ST2Callback(TIM_HandleTypeDef *htim);


void HAL_TIM_ST3Callback(TIM_HandleTypeDef *htim);


void HAL_TIM_ST4Callback(TIM_HandleTypeDef *htim);


void HAL_TIM_ST5Callback(TIM_HandleTypeDef *htim);


/**
 * @}
 */


/** @addtogroup TIM_Exported_Functions_Group7
 * @{
 */
/* Peripheral State functions  **************************************************/
HAL_TIM_StateTypeDef HAL_TIM_GetState(TIM_HandleTypeDef *htim);


/**
 * @}
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup TIM_Private_Macros TIM Private Macros
 * @{
 */


/** @defgroup TIM_IS_TIM_Definitions TIM Private macros to check input parameters
 * @{
 */
#define IS_TIM_INSTANCE( STx )              ( ( (STx) == ST0) || \
                                              ( (STx) == ST1) || \
                                              ( (STx) == ST2) || \
                                              ( (STx) == ST3) || \
                                              ( (STx) == ST4) || \
                                              ( (STx) == ST5) )
#define IS_TIME_WORK_MODE( MODE )           ( ( (MODE) == TIM_WORKMODE_MODCNT) || \
                                              ( (MODE) == TIM_WORKMODE_ONEPULSE) || \
                                              ( (MODE) == TIM_WORKMODE_FREECNT) || \
                                              ( (MODE) == TIM_WORKMODE_PWM) || \
                                              ( (MODE) == TIM_WORKMODE_TGS) )
#define IS_TIME_COUNTER_MODE( MODE )        ( ( (MODE) == TIM_CNTMODE_CONTINUES) || \
                                              ( (MODE) == TIM_CNTMODE_ONESHOT) )
#define IS_TIM_WORKCLK_DIV( DIV )           ( ( (DIV) == TIM_WORKCLK_DIV1) || \
                                              ( (DIV) == TIM_WORKCLK_DIV2) || \
                                              ( (DIV) == TIM_WORKCLK_DIV4) || \
                                              ( (DIV) == TIM_WORKCLK_DIV8) || \
                                              ( (DIV) == TIM_WORKCLK_DIV16) || \
                                              ( (DIV) == TIM_WORKCLK_DIV32) || \
                                              ( (DIV) == TIM_WORKCLK_DIV64) || \
                                              ( (DIV) == TIM_WORKCLK_DIV128) || \
                                              ( (DIV) == TIM_WORKCLK_DIV256) || \
                                              ( (DIV) == TIM_WORKCLK_DIV512) )
#define IS_TIM_AUTORELOAD( LOAD )           ( (LOAD) <= 0xFFFFFFFFU)
#define IS_TIM_TGS_FUNCTION( FUNCTION )     ( ( (FUNCTION) == TIM_TGS_FUNCTION_TRIG) || \
                                              ( (FUNCTION) == TIM_TGS_FUNCTION_FAULT) )
#define IS_TIM_TGS_CHANNELS( CHANNEL )      ( ( (CHANNEL) == TIM_TGS_CH0) || \
                                              ( (CHANNEL) == TIM_TGS_CH1) || \
                                              ( (CHANNEL) == TIM_TGS_CH2) || \
                                              ( (CHANNEL) == TIM_TGS_CH3) || \
                                              ( (CHANNEL) == TIM_TGS_CH4) || \
                                              ( (CHANNEL) == TIM_TGS_CH5) || \
                                              ( (CHANNEL) == TIM_TGS_CH6) || \
                                              ( (CHANNEL) == TIM_TGS_CH7) )
#define IS_TIM_TGS_CLK( CLK )               ( ( (CLK) == TIM_TGS_CLK_ST0) || \
                                              ( (CLK) == TIM_TGS_CLK_ST1) || \
                                              ( (CLK) == TIM_TGS_CLK_ST2) || \
                                              ( (CLK) == TIM_TGS_CLK_ST3) || \
                                              ( (CLK) == TIM_TGS_CLK_ST4) || \
                                              ( (CLK) == TIM_TGS_CLK_ST5) || \
                                              ( (CLK) == TIM_TGS_CLK_BUS) || \
                                              ( (CLK) == TIM_TGS_CLK_SYS) )
#define IS_TIM_TGS_TRIGSRC( SRC )           ( ( (SRC) == TIM_TGS_TRIGSRC_SW) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_ST0) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_ST1) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_ST2) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_ST3) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_ST4) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_ST5) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_DMA_CH0) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_DMA_CH1) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_DMA_CH2) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_DMA_CH3) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_CMP) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_DAC_FINISH) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_ADC_CH0_FINISH) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_UART0_RXFIFO_NONEMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_UART0_TXFIFO_EMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_UART1_RXFIFO_NONEMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_UART1_TXFIFO_EMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_I2C0_RXBUF_NONEMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_I2C0_TXBUF_EMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_SPI0_RXFIFO_NONEMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_SPI0_TXFIFO_EMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_SPI1_RXFIFO_NONEMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_SPI1_TXFIFO_EMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_CT0_RXFIFO_NONEMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_CT0_TXFIFO_EMPTY) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_GPIO0) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_GPIO1) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_GPIO2) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_GPIO3) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_GPIO4) || \
                                              ( (SRC) == TIM_TGS_TRIGSRC_GPIO5) )
#define IS_TIM_TGS_TRIG_ACTION( ACTION )    ( ( (ACTION) == TIM_TRIG_ACTION_STOP) || \
                                              ( (ACTION) == TIM_TRIG_ACTION_START) || \
                                              ( (ACTION) == TIM_TRIG_ACTION_PAUSE) || \
                                              ( (ACTION) == TIM_TRIG_ACTION_COUNT) )
#define IS_TIM_TGS_FAULT_ACTION( ACTION )   ( ( (ACTION) == TIM_FAULT_ACTION_HOLDCNT) || \
                                              ( (ACTION) == TIM_FAULT_ACTION_RESETCNT) )
#define IS_TIM_TGS_EDGE( EDGE )             ( ( (EDGE) == TIM_TGS_EDGE_HIGH) || \
                                              ( (EDGE) == TIM_TGS_EDGE_RISING) || \
                                              ( (EDGE) == TIM_TGS_EDGE_FALLING) || \
                                              ( (EDGE) == TIM_TGS_EDGE_BOTHEDGE) )
#define IS_TIM_TGS_INPUTINVERT( INVERT )    ( ( (INVERT) == TIM_TGS_INPUT_NOINVERT) || \
                                              ( (INVERT) == TIM_TGS_INPUT_INVERT) )
#define IS_TIM_TGS_FILTER( ICFILTER )       ( (ICFILTER) <= 0x0FU)
#define IS_TIM_CHAIN( CHAIN )               ( ( (CHAIN) == TIM_CHAIN_0TO1) || \
                                              ( (CHAIN) == TIM_CHAIN_0TO2) || \
                                              ( (CHAIN) == TIM_CHAIN_0TO3) || \
                                              ( (CHAIN) == TIM_CHAIN_0TO4) || \
                                              ( (CHAIN) == TIM_CHAIN_0TO5) )


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_STIM_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
