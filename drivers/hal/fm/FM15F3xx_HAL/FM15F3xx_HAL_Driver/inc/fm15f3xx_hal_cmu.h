/**
 ******************************************************************************
 * @file    fm15f3xx_hal_cmu.h
 * @author  WYL
 * @version V1.0.0
 * @brief   Header file of CMU HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_CMU_H
#define __FM15F3xx_HAL_CMU_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_cmu.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup CMU
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup CMU_Exported_Types CMU Exported Types
 * @{
 */


/**
 * @brief  CMU Internal/External Clock configuration structure definition
 */
#define CMU_InitTypeDef LL_CMU_InitTypeDef

typedef struct {
    uint32_t    MODULESel;
    uint32_t    CLKSel;
    uint32_t    CLKDiv;
} CMU_ModuleClockInitTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup CMU_Exported_Constants CMU Exported Constants
 * @{
 */


/** @defgroup CMU_Oscillator_Type
 * @{
 */
#define CMU_OSCTYPE_IRC4M   LL_CMU_OSCTYPE_IRC4M            /*irc4M*/
#define CMU_OSCTYPE_IRC16M  LL_CMU_OSCTYPE_IRC16M           /*irc16M*/
#define CMU_OSCTYPE_XTAL    LL_CMU_OSCTYPE_XTAL             /*xtal*/


/**
 * @}
 */


/** @defgroup  PLL Config
 * @{
 */
#define CMU_PLL_OFF LL_CMU_PLL_OFF
#define CMU_PLL_ON  LL_CMU_PLL_ON


/**
 * @}
 */


/** @defgroup PLL Source Config
 * @{
 */
#define CMU_PLLCLKSRC_XTAL      LL_CMU_PLLCLK_XTAL          /*!< xtal */
#define CMU_PLLCLKSRC_IRC16M    LL_CMU_PLLCLK_IRC16M        /*!< irc16m */


/**
 * @}
 */


/** @defgroup MAINCLK Source Config
 * @{
 */
#define CMU_MAINCLKSRC_IRC4M    LL_CMU_MCGMAINCLK_IRC4M     /*!< irc4m  */
#define CMU_MAINCLKSRC_IRC16M   LL_CMU_MCGMAINCLK_IRC16M    /*!< irc16m  */
#define CMU_MAINCLKSRC_PLLCLK   LL_CMU_MCGMAINCLK_PLLCLK    /*!< pll_clk  */
#define CMU_MAINCLKSRC_OSCCLK   LL_CMU_MCGMAINCLK_OSCCLK    /*!< osc_clk  */
#define CMU_MAINCLKSRC_GPIOCLK  LL_CMU_MCGMAINCLK_GPIOCLK   /*!< gpio_clk  */


/**
 * @}
 */


/** @defgroup SEC CLK Config
 * @{
 */
#define CMU_SECCLK_SYNSYSCLK    0x00000000U                 /*!< Security Clk Synchronize with Sys clk*/
#define CMU_SECCLK_RNDSYSCLK    (CMU_MCLKS_DFSSCY)          /*!< Security Clk Randomization based on Sys clk  */


/**
 * @}
 */


/** @defgroup CMU_MAINCLK_DIV
 * @{
 */
#define CMU_MAINCLK_DIV1    LL_CMU_MAINCLK_DIV001
#define CMU_MAINCLK_DIV2    LL_CMU_MAINCLK_DIV002
#define CMU_MAINCLK_DIV3    LL_CMU_MAINCLK_DIV003
#define CMU_MAINCLK_DIV4    LL_CMU_MAINCLK_DIV004
#define CMU_MAINCLK_DIV5    LL_CMU_MAINCLK_DIV005
#define CMU_MAINCLK_DIV6    LL_CMU_MAINCLK_DIV006
#define CMU_MAINCLK_DIV7    LL_CMU_MAINCLK_DIV007
#define CMU_MAINCLK_DIV8    LL_CMU_MAINCLK_DIV008
#define CMU_MAINCLK_DIV9    LL_CMU_MAINCLK_DIV009
#define CMU_MAINCLK_DIV10   LL_CMU_MAINCLK_DIV010
#define CMU_MAINCLK_DIV11   LL_CMU_MAINCLK_DIV011
#define CMU_MAINCLK_DIV12   LL_CMU_MAINCLK_DIV012
#define CMU_MAINCLK_DIV13   LL_CMU_MAINCLK_DIV013
#define CMU_MAINCLK_DIV14   LL_CMU_MAINCLK_DIV014
#define CMU_MAINCLK_DIV15   LL_CMU_MAINCLK_DIV015
#define CMU_MAINCLK_DIV16   LL_CMU_MAINCLK_DIV016
#define CMU_MAINCLK_DIV18   LL_CMU_MAINCLK_DIV018
#define CMU_MAINCLK_DIV20   LL_CMU_MAINCLK_DIV020
#define CMU_MAINCLK_DIV22   LL_CMU_MAINCLK_DIV022
#define CMU_MAINCLK_DIV24   LL_CMU_MAINCLK_DIV024
#define CMU_MAINCLK_DIV26   LL_CMU_MAINCLK_DIV026
#define CMU_MAINCLK_DIV28   LL_CMU_MAINCLK_DIV028
#define CMU_MAINCLK_DIV30   LL_CMU_MAINCLK_DIV030
#define CMU_MAINCLK_DIV32   LL_CMU_MAINCLK_DIV032
#define CMU_MAINCLK_DIV36   LL_CMU_MAINCLK_DIV036
#define CMU_MAINCLK_DIV40   LL_CMU_MAINCLK_DIV040
#define CMU_MAINCLK_DIV44   LL_CMU_MAINCLK_DIV044
#define CMU_MAINCLK_DIV48   LL_CMU_MAINCLK_DIV048
#define CMU_MAINCLK_DIV52   LL_CMU_MAINCLK_DIV052
#define CMU_MAINCLK_DIV56   LL_CMU_MAINCLK_DIV056
#define CMU_MAINCLK_DIV60   LL_CMU_MAINCLK_DIV060
#define CMU_MAINCLK_DIV64   LL_CMU_MAINCLK_DIV064
#define CMU_MAINCLK_DIV72   LL_CMU_MAINCLK_DIV072
#define CMU_MAINCLK_DIV80   LL_CMU_MAINCLK_DIV080
#define CMU_MAINCLK_DIV88   LL_CMU_MAINCLK_DIV088
#define CMU_MAINCLK_DIV96   LL_CMU_MAINCLK_DIV096
#define CMU_MAINCLK_DIV104  LL_CMU_MAINCLK_DIV104
#define CMU_MAINCLK_DIV112  LL_CMU_MAINCLK_DIV112
#define CMU_MAINCLK_DIV120  LL_CMU_MAINCLK_DIV120
#define CMU_MAINCLK_DIV128  LL_CMU_MAINCLK_DIV128


/**
 * @}
 */


/** @defgroup CMU_EC_MCG_IODIV
 * @{
 */
#define CMU_MCGIOCLK_DIV1   LL_CMU_MCGIOCLK_DIV1            /*!< divider 1  */
#define CMU_MCGIOCLK_DIV2   LL_CMU_MCGIOCLK_DIV2            /*!< divider 2  */
#define CMU_MCGIOCLK_DIV3   LL_CMU_MCGIOCLK_DIV3            /*!< divider 3  */
#define CMU_MCGIOCLK_DIV4   LL_CMU_MCGIOCLK_DIV4            /*!< divider 4  */
#define CMU_MCGIOCLK_DIV5   LL_CMU_MCGIOCLK_DIV5            /*!< divider 5  */
#define CMU_MCGIOCLK_DIV6   LL_CMU_MCGIOCLK_DIV6            /*!< divider 6  */
#define CMU_MCGIOCLK_DIV7   LL_CMU_MCGIOCLK_DIV7            /*!< divider 7  */
#define CMU_MCGIOCLK_DIV8   LL_CMU_MCGIOCLK_DIV8            /*!< divider 8  */


/**
 * @}
 */


/** @defgroup MODULE Source
 * @{
 */
#define CMU_MODULE_TRNG         (0x00U)
#define CMU_MODULE_CT           (0x01U)
#define CMU_MODULE_UART         (0x02U)
#define CMU_MODULE_USBFS        (0x03U)
#define CMU_MODULE_SHIELD       (0x04U)
#define CMU_MODULE_ALWAYSON     (0x05U)
#define CMU_MODULE_RTCMAINBUS   (0x06U)
#define CMU_MODULE_WDT          (0x07U)
#define CMU_MODULE_ADC          (0x08U)
#define CMU_MODULE_CLKOUT0      (0x09U)
#define CMU_MODULE_CLKOUT1      (0x0AU)
#define CMU_MODULE_CLKOUT2      (0x0BU)
#define CMU_MODULE_BKPSELFTEST  (0x0CU)
#define CMU_MODULE_STIM0        (0x0DU)
#define CMU_MODULE_STIM1        (0x0EU)
#define CMU_MODULE_STIM2        (0x0FU)
#define CMU_MODULE_STIM3        (0x10U)
#define CMU_MODULE_STIM4        (0x11U)
#define CMU_MODULE_STIM5        (0x12U)
#define CMU_MODULE_USBPHY       (0x13U)
#define CMU_MODULE_SYSTICK      (0x14U)
#define CMU_MODULE_SYSTEST      (0x15U)
#define CMU_MODULE_PLLEXT       (0x16U)


/**
 * @}
 */


/** @defgroup CMU Clock Div Group 1
 * @{
 */
#define CMU_CLKDIV_NONE (0x0U)                      /*!< divider 1  */


/**
 * @}
 */


/** @defgroup CMU Clock Div Group 1
 * @{
 */
#define CMU_CLKDIVGP1_1     LL_CMU_REUSEDIV1_DIV1   /*!< divider 1  */
#define CMU_CLKDIVGP1_2     LL_CMU_REUSEDIV1_DIV2   /*!< divider 2  */
#define CMU_CLKDIVGP1_4     LL_CMU_REUSEDIV1_DIV4   /*!< divider 4  */
#define CMU_CLKDIVGP1_8     LL_CMU_REUSEDIV1_DIV8   /*!< divider 8  */
#define CMU_CLKDIVGP1_16    (0x4U)                  /*!< divider 16  */
#define CMU_CLKDIVGP1_32    (0x5U)                  /*!< divider 32  */
#define CMU_CLKDIVGP1_64    (0x6U)                  /*!< divider 64  */
#define CMU_CLKDIVGP1_128   (0x7U)                  /*!< divider 128  */


/**
 * @}
 */


/** @defgroup CMU Clock Div Group 2
 * @{
 */
#define CMU_CLKDIVGP2_2     LL_CMU_REUSEDIV2_DIV2   /*!< divider 2  */
#define CMU_CLKDIVGP2_4     LL_CMU_REUSEDIV2_DIV4   /*!< divider 4  */
#define CMU_CLKDIVGP2_8     LL_CMU_REUSEDIV2_DIV8   /*!< divider 8  */
#define CMU_CLKDIVGP2_16    LL_CMU_REUSEDIV2_DIV16  /*!< divider 16  */
#define CMU_CLKDIVGP2_32    LL_CMU_REUSEDIV2_DIV32  /*!< divider 32  */
#define CMU_CLKDIVGP2_64    LL_CMU_REUSEDIV2_DIV64  /*!< divider 64  */
#define CMU_CLKDIVGP2_128   LL_CMU_REUSEDIV2_DIV128 /*!< divider 128  */
#define CMU_CLKDIVGP2_256   LL_CMU_REUSEDIV2_DIV256 /*!< divider 256  */


/**
 * @}
 */


/** @defgroup CMU Clock Div Group 3
 * @{
 */
#define CMU_CLKDIVGP3_1     LL_CMU_REUSEDIV3_DIV1   /*!< divider 1  */
#define CMU_CLKDIVGP3_2     LL_CMU_REUSEDIV3_DIV2   /*!< divider 2  */
#define CMU_CLKDIVGP3_3     LL_CMU_REUSEDIV3_DIV3   /*!< divider 3  */
#define CMU_CLKDIVGP3_4     LL_CMU_REUSEDIV3_DIV4   /*!< divider 4  */
#define CMU_CLKDIVGP3_5     LL_CMU_REUSEDIV3_DIV5   /*!< divider 5  */
#define CMU_CLKDIVGP3_6     LL_CMU_REUSEDIV3_DIV6   /*!< divider 6  */
#define CMU_CLKDIVGP3_7     LL_CMU_REUSEDIV3_DIV7   /*!< divider 7  */
#define CMU_CLKDIVGP3_8     LL_CMU_REUSEDIV3_DIV8   /*!< divider 8  */
#define CMU_CLKDIVGP3_9     LL_CMU_REUSEDIV3_DIV9   /*!< divider 9  */
#define CMU_CLKDIVGP3_10    LL_CMU_REUSEDIV3_DIV10  /*!< divider 10  */
#define CMU_CLKDIVGP3_11    LL_CMU_REUSEDIV3_DIV11  /*!< divider 11  */
#define CMU_CLKDIVGP3_12    LL_CMU_REUSEDIV3_DIV12  /*!< divider 12  */
#define CMU_CLKDIVGP3_13    LL_CMU_REUSEDIV3_DIV13  /*!< divider 13  */
#define CMU_CLKDIVGP3_14    LL_CMU_REUSEDIV3_DIV14  /*!< divider 14  */
#define CMU_CLKDIVGP3_15    LL_CMU_REUSEDIV3_DIV15  /*!< divider 15  */
#define CMU_CLKDIVGP3_16    LL_CMU_REUSEDIV3_DIV16  /*!< divider 16  */


/**
 * @}
 */


/** @defgroup CMU_CLK CT Source
 * @{
 */
#define CMU_CTCLKSRC_SYSCLK (0x0)
#define CMU_CTCLKSRC_IOCLK  (0x1)
#define CMU_CTCLKSRC_IRC16M (0x2)
#define CMU_CTCLKSRC_OSCCLK (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK UART Source
 * @{
 */
#define CMU_UARTCLKSRC_SYSCLK   (0x0)
#define CMU_UARTCLKSRC_IOCLK    (0x1)
#define CMU_UARTCLKSRC_IRC16M   (0x2)
#define CMU_UARTCLKSRC_OSCCLK   (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK USBFS Source
 * @{
 */
#define CMU_USBFSCLKSRC_PLLCLK      (0x0)
#define CMU_USBFSCLKSRC_USB48M      (0x1)
#define CMU_USBFSCLKSRC_PLLDIV      (0x2)
#define CMU_USBFSCLKSRC_SYSTESTCLK  (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK SHIELD Source
 * @{
 */
#define CMU_SHIELDCLKSRC_IRC4M  (0x0)
#define CMU_SHIELDCLKSRC_LPO1K  (0x1)


/**
 * @}
 */


/** @defgroup CMU_CLK ALWAYSON Source
 * @{
 */
#define CMU_ALWAYSONCLKSRC_BUS      (0x0)
#define CMU_ALWAYSONCLKSRC_BUSDIV2  (0x1)
#define CMU_ALWAYSONCLKSRC_BUSDIV3  (0x2)
#define CMU_ALWAYSONCLKSRC_BUSDIV4  (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK RTCMAINBUSCLK Source
 * @{
 */
#define CMU_RTCMAINBUSCLKSRC_BUS        (0x0)
#define CMU_RTCMAINBUSCLKSRC_BUSDIV2    (0x1)
#define CMU_RTCMAINBUSCLKSRC_BUSDIV3    (0x2)
#define CMU_RTCMAINBUSCLKSRC_BUSDIV4    (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK WDT Source
 * @{
 */
#define CMU_WDTCLKSRC_LPO1K (0x0)
#define CMU_WDTCLKSRC_BUS   (0x1)


/**
 * @}
 */


/** @defgroup CMU_CLK ADC Source
 * @{
 */
#define CMU_ADCCLKSRC_IRC4M     (0x0)
#define CMU_ADCCLKSRC_IRC16M    (0x1)
#define CMU_ADCCLKSRC_OSCCLK    (0x2)
#define CMU_ADCCLKSRC_PLLDIV    (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK CLKOUT Source
 * @{
 */
#define CMU_CLKOUTCLKSRC_SYSTESTCLK (0x0)
#define CMU_CLKOUTCLKSRC_IRC4M      (0x1)
#define CMU_CLKOUTCLKSRC_IRC16M     (0x2)
#define CMU_CLKOUTCLKSRC_PLL        (0x3)
#define CMU_CLKOUTCLKSRC_OSCCLK     (0x4)
#define CMU_CLKOUTCLKSRC_PLLDIV8    (0x5)
#define CMU_CLKOUTCLKSRC_PLLDIV     (0x6)
#define CMU_CLKOUTCLKSRC_GPIO       (0x7)
#define CMU_CLKOUTCLKSRC_TRNG       (0xA)
#define CMU_CLKOUTCLKSRC_USB48M     (0xB)
#define CMU_CLKOUTCLKSRC_SYSCLK     (0xC)
#define CMU_CLKOUTCLKSRC_SECCLK     (0xD)
#define CMU_CLKOUTCLKSRC_OSC32K     (0xF)


/**
 * @}
 */


/** @defgroup CMU_CLK STM Source
 * @{
 */
#define CMU_STMCLKSRC_SYSCLK    (0x0)
#define CMU_STMCLKSRC_PLL       (0x1)
#define CMU_STMCLKSRC_OSCCLK    (0x2)
#define CMU_STMCLKSRC_IRC4M     (0x3)
#define CMU_STMCLKSRC_IRC16M    (0x4)
#define CMU_STMCLKSRC_GPIO      (0x5)
#define CMU_STMCLKSRC_PLLDIV    (0x6)
#define CMU_STMCLKSRC_USB48M    (0x7)


/**
 * @}
 */


/** @defgroup CMU_CLK USBPHY Source
 * @{
 */
#define CMU_USBPHYCLKSRC_OSCCLK (0x0)
#define CMU_USBPHYCLKSRC_PLL    (0x1)
#define CMU_USBPHYCLKSRC_GPIO   (0x2)
#define CMU_USBPHYCLKSRC_USB48M (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK SYSTICK Source
 * @{
 */
#define CMU_SYSTICKCLKSRC_IRC4M     (0x0)
#define CMU_SYSTICKCLKSRC_IRC16M    (0x1)
#define CMU_SYSTICKCLKSRC_OSCCLK    (0x2)
#define CMU_SYSTICKCLKSRC_GPIO      (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK PLLEXT Source
 * @{
 */
#define CMU_PLLEXTCLKSRC_CLOSE  (0x0)
#define CMU_PLLEXTCLKSRC_GPIO   (0x1)
#define CMU_PLLEXTCLKSRC_USB12M (0x2)
#define CMU_PLLEXTCLKSRC_USB48M (0x3)


/**
 * @}
 */


/** @defgroup CMU_CLK LOWPOWER Source
 * @{
 */
#define CMU_POWERDOWN_POR_ENABLE    CMU_CLKLP_PDPOREN
#define CMU_POWERDOWN_LPO_ENABLE    CMU_CLKLP_PDLPOEN
#define CMU_STANDBY_IRC4M_ENABLE    CMU_CLKLP_STBYIRC4MEN
#define CMU_STOP_IRC4M_ENABLE       CMU_CLKLP_STOPIRC4MEN


/**
 * @}
 */


/** @defgroup CMU_CLK IDLE Source
 * @{
 */
#define CMU_IDLE_FLL_ENABLE     CMU_CLKIDLE_ENFLL
#define CMU_IDLE_PLL_ENABLE     CMU_CLKIDLE_ENPLL
#define CMU_IDLE_OSC_ENABLE     CMU_CLKIDLE_ENOSC
#define CMU_IDLE_IRC16M_ENABLE  CMU_CLKIDLE_EN16M


/**
 * @}
 */


/** @defgroup CMU_CLK STOP Source
 * @{
 */
#define CMU_STOP_FLL_ENABLE     CMU_CLKSTOP_ENFLL
#define CMU_STOP_PLL_ENABLE     CMU_CLKSTOP_ENPLL
#define CMU_STOP_OSC_ENABLE     CMU_CLKSTOP_ENOSC
#define CMU_STOP_IRC16M_ENABLE  CMU_CLKSTOP_EN16M


/**
 * @}
 */


/** @defgroup CMU_CLK IDLE MODULECLK
 * @{
 */
#define CMU_IDLE_MODULECLK_SHIELD   CMU_GMDLCLK_SHIELD          /*!<shield_clks */
#define CMU_IDLE_MODULECLK_WDT      CMU_GMDLCLK_WDT             /*!<wdt_clks */
#define CMU_IDLE_MODULECLK_ADC      CMU_GMDLCLK_ADC             /*!<adc_clks */
#define CMU_IDLE_MODULECLK_TRNG     CMU_GMDLCLK_TRNG            /*!<trng_clks */
#define CMU_IDLE_MODULECLK_CT       CMU_GMDLCLK_CT              /*!<ct_clks */
#define CMU_IDLE_MODULECLK_UART     CMU_GMDLCLK_UART            /*!<uart_clks */
#define CMU_IDLE_MODULECLK_USBFS    CMU_GMDLCLK_USBFS           /*!<usb_fs_clks */
#define CMU_IDLE_MODULECLK_COUT0    CMU_GMDLCLK_TST0            /*!<test0_clks */
#define CMU_IDLE_MODULECLK_COUT1    CMU_GMDLCLK_TST1            /*!<test1_clks */
#define CMU_IDLE_MODULECLK_COUT2    CMU_GMDLCLK_TST2            /*!<test2_clks */
#define CMU_IDLE_MODULECLK_STIMER0  CMU_GMDLCLK_STM0            /*!<stimer0_clks */
#define CMU_IDLE_MODULECLK_STIMER1  CMU_GMDLCLK_STM1            /*!<stimer1_clks */
#define CMU_IDLE_MODULECLK_STIMER2  CMU_GMDLCLK_STM2            /*!<stimer2_clks */
#define CMU_IDLE_MODULECLK_STIMER3  CMU_GMDLCLK_STM3            /*!<stimer3_clks */
#define CMU_IDLE_MODULECLK_STIMER4  CMU_GMDLCLK_STM4            /*!<stimer4_clks */
#define CMU_IDLE_MODULECLK_STIMER5  CMU_GMDLCLK_STM5            /*!<stimer5_clks */
#define CMU_IDLE_MODULECLK_USBPHY   CMU_GMDLCLK_USBPHY          /*!<usb_phy_clks */
#define CMU_IDLE_MODULECLK_BKPTEST  CMU_GMDLCLK_BKPTST          /*!<bkp_test_clks */
#define CMU_IDLE_MODULECLK_SYSTICK  CMU_GMDLCLK_SYSTKRF         /*!<sys_tick_ref_clks */
#define CMU_IDLE_MODULECLK_SYSTEST  CMU_GMDLCLK_SYSTST          /*!<sys_test_clks */
#define CMU_IDLE_MODULECLK_PLLEXT   CMU_GMDLCLK_PLLEXT          /*!<pll_ext_clks */


/**
 * @}
 */


/** @defgroup CMU_CLK STOP MODULECLK
 * @{
 */
#define CMU_STOP_MODULECLK_SHIELD   CMU_PMGMDLCLK_SHIELD        /*!<shield_clks */
#define CMU_STOP_MODULECLK_ADC      CMU_PMGMDLCLK_ADC           /*!<adc_clks */
#define CMU_STOP_MODULECLK_TRNG     CMU_PMGMDLCLK_TRNG          /*!<trng_clks */
#define CMU_STOP_MODULECLK_CT       CMU_PMGMDLCLK_CT            /*!<ct_clks */
#define CMU_STOP_MODULECLK_UART     CMU_PMGMDLCLK_UART          /*!<uart_clks */
#define CMU_STOP_MODULECLK_USBFS    CMU_PMGMDLCLK_USBFS         /*!<usb_fs_clks */
#define CMU_STOP_MODULECLK_COUT0    CMU_PMGMDLCLK_TST0          /*!<test0_clks */
#define CMU_STOP_MODULECLK_COUT1    CMU_PMGMDLCLK_TST1          /*!<test1_clks */
#define CMU_STOP_MODULECLK_COUT2    CMU_PMGMDLCLK_TST2          /*!<test2_clks */
#define CMU_STOP_MODULECLK_STIMER0  CMU_PMGMDLCLK_STM0          /*!<stimer0_clks */
#define CMU_STOP_MODULECLK_STIMER1  CMU_PMGMDLCLK_STM1          /*!<stimer1_clks */
#define CMU_STOP_MODULECLK_STIMER2  CMU_PMGMDLCLK_STM2          /*!<stimer2_clks */
#define CMU_STOP_MODULECLK_STIMER3  CMU_PMGMDLCLK_STM3          /*!<stimer3_clks */
#define CMU_STOP_MODULECLK_STIMER4  CMU_PMGMDLCLK_STM4          /*!<stimer4_clks */
#define CMU_STOP_MODULECLK_STIMER5  CMU_PMGMDLCLK_STM5          /*!<stimer5_clks */
#define CMU_STOP_MODULECLK_USBPHY   CMU_PMGMDLCLK_USBPHY        /*!<usb_phy_clks */
#define CMU_STOP_MODULECLK_BKPTEST  CMU_PMGMDLCLK_BKPTST        /*!<bkp_test_clks */
#define CMU_STOP_MODULECLK_SYSTICK  CMU_PMGMDLCLK_SYSTKRF       /*!<sys_tick_ref_clks */
#define CMU_STOP_MODULECLK_SYSTEST  CMU_PMGMDLCLK_SYSTST        /*!<sys_test_clks */
#define CMU_STOP_MODULECLK_PLLEXT   CMU_PMGMDLCLK_PLLEXT        /*!<pll_ext_clks */


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup CMU_Exported_Macros CMU Exported Macros
 * @{
 */


/** @brief  Macro to get the oscillator used as PLL clock source.
 * @retval The oscillator used as PLL clock source.
 */
#define __HAL_CMU_GET_PLL_SOURCE() ( (uint32_t) (CMU->CFG_CLKS1 & CMU_CFGCLKS1_PLLCLKINS) )


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup CMU_Exported_Functions
 * @{
 */


/** @addtogroup CMU_Exported_Functions_Group1
 * @{
 */
/* Initialization and de-initialization functions  ******************************/
HAL_StatusTypeDef HAL_CMU_ClockInit(CMU_InitTypeDef *CMU_InitStruct);
HAL_StatusTypeDef HAL_CMU_SysClkRestoreToPLL(uint32_t sysclkfreq);

HAL_StatusTypeDef HAL_CMU_DeInit(void);


void HAL_CMU_ModuleClockConfig(uint8_t Module, uint32_t ClockSrc, uint32_t ClockDiv);


/**
 * @}
 */


/** @addtogroup CMU_Exported_Functions_Group2
 * @{
 */
/* Peripheral Control functions  ************************************************/
void HAL_CMU_MCOConfig(uint32_t CMU_MCOx, uint32_t CMU_MCOSource, uint32_t CMU_MCODiv);


uint32_t HAL_CMU_SystemCoreClockUpdate(void);


uint32_t HAL_CMU_GetBusClock(void);


uint32_t HAL_CMU_GetSysClock(void);


uint32_t HAL_CMU_GetOscClock(void);


void HAL_CMU_SECConfig(uint8_t mode);


/**
 * @}
 */


/**
 * @}
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/


/** @defgroup CMU_Private_Constants CMU Private Constants
 * @{
 */


/** @defgroup CMU_BitAddress_AliasRegion CMU BitAddress AliasRegion
 * @brief CMU registers bit address in the alias region
 * @{
 */


#define __HAL_CMU_GET_SYSDIV() ( (1 << LL_CMU_GetMCGMainClkPrescaler() ) * (LL_CMU_GetSysClkPrescaler() + 1) )

#define CLOCKSWITCH_TIMEOUT_VALUE 2000U  /* 2 s */


/**
 * @}
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup CMU_Private_Macros CMU Private Macros
 * @{
 */


/** @defgroup CMU_IS_CMU_Definitions CMU Private macros to check input parameters
 * @{
 */


/** @defgroup CMU_LL_EC_MCG_MAINCLK
 * @{
 */

#define IS_CMU_OSCTYPE( TYPE ) ( ( (TYPE) == CMU_OSCTYPE_IRC4M) || \
                                 ( (TYPE) == CMU_OSCTYPE_IRC16M) || \
                                 ( (TYPE) == CMU_OSCTYPE_XTAL) || \
                                 ( (TYPE) == (CMU_OSCTYPE_IRC4M | CMU_OSCTYPE_IRC16M) ) || \
                                 ( (TYPE) == (CMU_OSCTYPE_IRC4M | CMU_OSCTYPE_XTAL) ) || \
                                 ( (TYPE) == (CMU_OSCTYPE_IRC16M | CMU_OSCTYPE_XTAL) ) || \
                                 ( (TYPE) == (CMU_OSCTYPE_IRC4M | CMU_OSCTYPE_IRC16M | CMU_OSCTYPE_XTAL) ) )

#define IS_CMU_MAINCLKSRC( SOURCE ) ( ( (SOURCE) == CMU_MAINCLKSRC_IRC4M) || \
                                      ( (SOURCE) == CMU_MAINCLKSRC_IRC16M) || \
                                      ( (SOURCE) == CMU_MAINCLKSRC_PLLCLK) || \
                                      ( (SOURCE) == CMU_MAINCLKSRC_OSCCLK) || \
                                      ( (SOURCE) == CMU_MAINCLKSRC_GPIOCLK) )

#define IS_CMU_PLLCLKSRC( SOURCE ) ( ( (SOURCE) == CMU_PLLCLKSRC_XTAL) || \
                                     ( (SOURCE) == CMU_PLLCLKSRC_IRC16M) )

#define IS_CMU_OSCVALUE( Freq ) ( (Freq) <= 16000000)

#define IS_CMU_PLLCLK( Freq ) ( (Freq) <= 180000000)

#define IS_CMU_BUSCLKDIV( DIV ) ( ( (DIV) == CMU_CLKDIVGP3_1) || \
                                  ( (DIV) == CMU_CLKDIVGP3_2) || \
                                  ( (DIV) == CMU_CLKDIVGP3_3) || \
                                  ( (DIV) == CMU_CLKDIVGP3_4) || \
                                  ( (DIV) == CMU_CLKDIVGP3_5) || \
                                  ( (DIV) == CMU_CLKDIVGP3_6) || \
                                  ( (DIV) == CMU_CLKDIVGP3_7) || \
                                  ( (DIV) == CMU_CLKDIVGP3_8) || \
                                  ( (DIV) == CMU_CLKDIVGP3_9) || \
                                  ( (DIV) == CMU_CLKDIVGP3_10) || \
                                  ( (DIV) == CMU_CLKDIVGP3_11) || \
                                  ( (DIV) == CMU_CLKDIVGP3_12) || \
                                  ( (DIV) == CMU_CLKDIVGP3_13) || \
                                  ( (DIV) == CMU_CLKDIVGP3_14) || \
                                  ( (DIV) == CMU_CLKDIVGP3_15) || \
                                  ( (DIV) == CMU_CLKDIVGP3_16) )

#define IS_CMU_SECCLKCONFIG( MODE ) ( ( (MODE) == CMU_SECCLK_SYNSYSCLK) || \
                                      ( (MODE) == CMU_SECCLK_RNDSYSCLK) )
#define IS_CMU_MAINCLKDIV( DIV )    ( ( (DIV) == CMU_MAINCLK_DIV1) || \
                                      ( (DIV) == CMU_MAINCLK_DIV2) || \
                                      ( (DIV) == CMU_MAINCLK_DIV3) || \
                                      ( (DIV) == CMU_MAINCLK_DIV4) || \
                                      ( (DIV) == CMU_MAINCLK_DIV5) || \
                                      ( (DIV) == CMU_MAINCLK_DIV6) || \
                                      ( (DIV) == CMU_MAINCLK_DIV7) || \
                                      ( (DIV) == CMU_MAINCLK_DIV8) || \
                                      ( (DIV) == CMU_MAINCLK_DIV9) || \
                                      ( (DIV) == CMU_MAINCLK_DIV10) || \
                                      ( (DIV) == CMU_MAINCLK_DIV11) || \
                                      ( (DIV) == CMU_MAINCLK_DIV12) || \
                                      ( (DIV) == CMU_MAINCLK_DIV13) || \
                                      ( (DIV) == CMU_MAINCLK_DIV14) || \
                                      ( (DIV) == CMU_MAINCLK_DIV15) || \
                                      ( (DIV) == CMU_MAINCLK_DIV16) || \
                                      ( (DIV) == CMU_MAINCLK_DIV18) || \
                                      ( (DIV) == CMU_MAINCLK_DIV20) || \
                                      ( (DIV) == CMU_MAINCLK_DIV22) || \
                                      ( (DIV) == CMU_MAINCLK_DIV24) || \
                                      ( (DIV) == CMU_MAINCLK_DIV26) || \
                                      ( (DIV) == CMU_MAINCLK_DIV28) || \
                                      ( (DIV) == CMU_MAINCLK_DIV30) || \
                                      ( (DIV) == CMU_MAINCLK_DIV32) || \
                                      ( (DIV) == CMU_MAINCLK_DIV36) || \
                                      ( (DIV) == CMU_MAINCLK_DIV40) || \
                                      ( (DIV) == CMU_MAINCLK_DIV44) || \
                                      ( (DIV) == CMU_MAINCLK_DIV48) || \
                                      ( (DIV) == CMU_MAINCLK_DIV52) || \
                                      ( (DIV) == CMU_MAINCLK_DIV56) || \
                                      ( (DIV) == CMU_MAINCLK_DIV60) || \
                                      ( (DIV) == CMU_MAINCLK_DIV64) || \
                                      ( (DIV) == CMU_MAINCLK_DIV72) || \
                                      ( (DIV) == CMU_MAINCLK_DIV80) || \
                                      ( (DIV) == CMU_MAINCLK_DIV88) || \
                                      ( (DIV) == CMU_MAINCLK_DIV96) || \
                                      ( (DIV) == CMU_MAINCLK_DIV104) || \
                                      ( (DIV) == CMU_MAINCLK_DIV112) || \
                                      ( (DIV) == CMU_MAINCLK_DIV120) || \
                                      ( (DIV) == CMU_MAINCLK_DIV128) )

#define IS_CMU_MODULESEL( SEL ) ( ( (SEL) == CMU_MODULE_TRNG) || \
                                  ( (SEL) == CMU_MODULE_CT) || \
                                  ( (SEL) == CMU_MODULE_UART) || \
                                  ( (SEL) == CMU_MODULE_USBFS) || \
                                  ( (SEL) == CMU_MODULE_SHIELD) || \
                                  ( (SEL) == CMU_MODULE_ALWAYSON) || \
                                  ( (SEL) == CMU_MODULE_RTCMAINBUS) || \
                                  ( (SEL) == CMU_MODULE_WDT) || \
                                  ( (SEL) == CMU_MODULE_ADC) || \
                                  ( (SEL) == CMU_MODULE_CLKOUT0) || \
                                  ( (SEL) == CMU_MODULE_CLKOUT1) || \
                                  ( (SEL) == CMU_MODULE_CLKOUT2) || \
                                  ( (SEL) == CMU_MODULE_STIM0) || \
                                  ( (SEL) == CMU_MODULE_STIM1) || \
                                  ( (SEL) == CMU_MODULE_STIM2) || \
                                  ( (SEL) == CMU_MODULE_STIM3) || \
                                  ( (SEL) == CMU_MODULE_STIM4) || \
                                  ( (SEL) == CMU_MODULE_STIM5) || \
                                  ( (SEL) == CMU_MODULE_USBPHY) || \
                                  ( (SEL) == CMU_MODULE_SYSTICK) || \
                                  ( (SEL) == CMU_MODULE_SYSTEST) || \
                                  ( (SEL) == CMU_MODULE_PLLEXT) )

#define IS_CMU_TRNGCLKDIV( DIV ) ( ( (DIV) == CMU_CLKDIVGP2_2) || \
                                   ( (DIV) == CMU_CLKDIVGP2_4) || \
                                   ( (DIV) == CMU_CLKDIVGP2_8) || \
                                   ( (DIV) == CMU_CLKDIVGP2_16) || \
                                   ( (DIV) == CMU_CLKDIVGP2_32) || \
                                   ( (DIV) == CMU_CLKDIVGP2_64) || \
                                   ( (DIV) == CMU_CLKDIVGP2_128) || \
                                   ( (DIV) == CMU_CLKDIVGP2_256) )

#define IS_CMU_CTCLKSEL( SOURCE ) ( ( (SOURCE) == CMU_CTCLKSRC_SYSCLK) || \
                                    ( (SOURCE) == CMU_CTCLKSRC_IOCLK) || \
                                    ( (SOURCE) == CMU_CTCLKSRC_IRC16M) || \
                                    ( (SOURCE) == CMU_CTCLKSRC_OSCCLK) )

#define IS_CMU_CTCLKDIV( DIV ) ( ( (DIV) == CMU_CLKDIVGP1_1) || \
                                 ( (DIV) == CMU_CLKDIVGP1_2) || \
                                 ( (DIV) == CMU_CLKDIVGP1_4) || \
                                 ( (DIV) == CMU_CLKDIVGP1_8) )

#define IS_CMU_UARTCLKSEL( SOURCE ) ( ( (SOURCE) == CMU_UARTCLKSRC_SYSCLK) || \
                                      ( (SOURCE) == CMU_UARTCLKSRC_IOCLK) || \
                                      ( (SOURCE) == CMU_UARTCLKSRC_IRC16M) || \
                                      ( (SOURCE) == CMU_UARTCLKSRC_OSCCLK) )

#define IS_CMU_UARTCLKDIV( DIV ) ( ( (DIV) == CMU_CLKDIVGP1_1) || \
                                   ( (DIV) == CMU_CLKDIVGP1_2) || \
                                   ( (DIV) == CMU_CLKDIVGP1_4) || \
                                   ( (DIV) == CMU_CLKDIVGP1_8) )

#define IS_CMU_USBFSCLKSEL( SOURCE ) ( ( (SOURCE) == CMU_USBFSCLKSRC_PLLCLK) || \
                                       ( (SOURCE) == CMU_USBFSCLKSRC_USB48M) || \
                                       ( (SOURCE) == CMU_USBFSCLKSRC_PLLDIV) || \
                                       ( (SOURCE) == CMU_USBFSCLKSRC_SYSTESTCLK) )

#define IS_CMU_SHIELDCLKSEL( SOURCE ) ( ( (SOURCE) == CMU_SHIELDCLKSRC_IRC4M) || \
                                        ( (SOURCE) == CMU_SHIELDCLKSRC_LPO1K) )

#define IS_CMU_SHIELDCLKDIV( DIV ) ( ( (DIV) == CMU_CLKDIVGP1_1) || \
                                     ( (DIV) == CMU_CLKDIVGP1_2) || \
                                     ( (DIV) == CMU_CLKDIVGP1_4) || \
                                     ( (DIV) == CMU_CLKDIVGP1_8) || \
                                     ( (DIV) == CMU_CLKDIVGP1_16) || \
                                     ( (DIV) == CMU_CLKDIVGP1_32) || \
                                     ( (DIV) == CMU_CLKDIVGP1_64) || \
                                     ( (DIV) == CMU_CLKDIVGP1_128) )

#define IS_CMU_ALWAYSONCLKSEL( SOURCE ) ( ( (SOURCE) == CMU_ALWAYSONCLKSRC_BUS) || \
                                          ( (SOURCE) == CMU_ALWAYSONCLKSRC_BUSDIV2) || \
                                          ( (SOURCE) == CMU_ALWAYSONCLKSRC_BUSDIV3) || \
                                          ( (SOURCE) == CMU_ALWAYSONCLKSRC_BUSDIV4) )

#define IS_CMU_RTCMAINBUSCLKSEL( SOURCE )   ( ( (SOURCE) == CMU_RTCMAINBUSCLKSRC_BUS) || \
                                              ( (SOURCE) == CMU_RTCMAINBUSCLKSRC_BUSDIV2) || \
                                              ( (SOURCE) == CMU_RTCMAINBUSCLKSRC_BUSDIV3) || \
                                              ( (SOURCE) == CMU_RTCMAINBUSCLKSRC_BUSDIV4) )
#define IS_CMU_WTDCLKSEL( SOURCE )          ( ( (SOURCE) == CMU_WDTCLKSRC_LPO1K) || \
                                              ( (SOURCE) == CMU_WDTCLKSRC_BUS) )
#define IS_CMU_ADCCLKSEL( SOURCE )          ( ( (SOURCE) == CMU_ADCCLKSRC_IRC4M) || \
                                              ( (SOURCE) == CMU_ADCCLKSRC_IRC16M) || \
                                              ( (SOURCE) == CMU_ADCCLKSRC_OSCCLK) || \
                                              ( (SOURCE) == CMU_ADCCLKSRC_PLLDIV) )
#define IS_CMU_ADCCLKDIV( DIV )             ( ( (DIV) == CMU_CLKDIVGP1_1) || \
                                              ( (DIV) == CMU_CLKDIVGP1_2) || \
                                              ( (DIV) == CMU_CLKDIVGP1_4) || \
                                              ( (DIV) == CMU_CLKDIVGP1_8) )
#define IS_CMU_CLKOUTCLKSEL( SOURCE )       ( ( (SOURCE) == CMU_CLKOUTCLKSRC_SYSTESTCLK) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_IRC4M) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_IRC16M) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_PLL) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_OSCCLK) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_PLLDIV8) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_PLLDIV) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_GPIO) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_TRNG) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_USB48M) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_SYSCLK) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_SECCLK) || \
                                              ( (SOURCE) == CMU_CLKOUTCLKSRC_OSC32K) )
#define IS_CMU_BKPSELFTESTCLKDIV( DIV )     ( ( (DIV) == CMU_CLKDIVGP2_2) || \
                                              ( (DIV) == CMU_CLKDIVGP2_4) || \
                                              ( (DIV) == CMU_CLKDIVGP2_8) || \
                                              ( (DIV) == CMU_CLKDIVGP2_16) || \
                                              ( (DIV) == CMU_CLKDIVGP2_32) || \
                                              ( (DIV) == CMU_CLKDIVGP2_64) || \
                                              ( (DIV) == CMU_CLKDIVGP2_128) || \
                                              ( (DIV) == CMU_CLKDIVGP2_256) )
#define IS_CMU_STIMCLKSEL( SOURCE )         ( ( (SOURCE) == CMU_STMCLKSRC_SYSCLK) || \
                                              ( (SOURCE) == CMU_STMCLKSRC_PLL) || \
                                              ( (SOURCE) == CMU_STMCLKSRC_OSCCLK) || \
                                              ( (SOURCE) == CMU_STMCLKSRC_IRC4M) || \
                                              ( (SOURCE) == CMU_STMCLKSRC_IRC16M) || \
                                              ( (SOURCE) == CMU_STMCLKSRC_GPIO) || \
                                              ( (SOURCE) == CMU_STMCLKSRC_PLLDIV) || \
                                              ( (SOURCE) == CMU_STMCLKSRC_USB48M) )
#define IS_CMU_USBPHYCLKSEL( SOURCE )       ( ( (SOURCE) == CMU_USBPHYCLKSRC_OSCCLK) || \
                                              ( (SOURCE) == CMU_USBPHYCLKSRC_PLL) || \
                                              ( (SOURCE) == CMU_USBPHYCLKSRC_GPIO) || \
                                              ( (SOURCE) == CMU_USBPHYCLKSRC_USB48M) )
#define IS_CMU_USBPHYCLKDIV( DIV )          ( ( (DIV) == CMU_CLKDIVGP3_1) || \
                                              ( (DIV) == CMU_CLKDIVGP3_2) || \
                                              ( (DIV) == CMU_CLKDIVGP3_3) || \
                                              ( (DIV) == CMU_CLKDIVGP3_4) || \
                                              ( (DIV) == CMU_CLKDIVGP3_5) || \
                                              ( (DIV) == CMU_CLKDIVGP3_6) || \
                                              ( (DIV) == CMU_CLKDIVGP3_7) || \
                                              ( (DIV) == CMU_CLKDIVGP3_8) || \
                                              ( (DIV) == CMU_CLKDIVGP3_9) || \
                                              ( (DIV) == CMU_CLKDIVGP3_10) || \
                                              ( (DIV) == CMU_CLKDIVGP3_11) || \
                                              ( (DIV) == CMU_CLKDIVGP3_12) || \
                                              ( (DIV) == CMU_CLKDIVGP3_13) || \
                                              ( (DIV) == CMU_CLKDIVGP3_14) || \
                                              ( (DIV) == CMU_CLKDIVGP3_15) || \
                                              ( (DIV) == CMU_CLKDIVGP3_16) )
#define IS_CMU_SYSTICKCLKSEL( SOURCE )      ( ( (SOURCE) == CMU_SYSTICKCLKSRC_IRC4M) || \
                                              ( (SOURCE) == CMU_SYSTICKCLKSRC_IRC16M) || \
                                              ( (SOURCE) == CMU_SYSTICKCLKSRC_OSCCLK) || \
                                              ( (SOURCE) == CMU_SYSTICKCLKSRC_GPIO) )
#define IS_CMU_SYSTICKCLKDIV( DIV )         ( ( (DIV) == CMU_CLKDIVGP2_2) || \
                                              ( (DIV) == CMU_CLKDIVGP2_4) || \
                                              ( (DIV) == CMU_CLKDIVGP2_8) || \
                                              ( (DIV) == CMU_CLKDIVGP2_16) || \
                                              ( (DIV) == CMU_CLKDIVGP2_32) || \
                                              ( (DIV) == CMU_CLKDIVGP2_64) || \
                                              ( (DIV) == CMU_CLKDIVGP2_128) || \
                                              ( (DIV) == CMU_CLKDIVGP2_256) )
#define IS_CMU_SYSTESTCLKDIV( DIV )         ( ( (DIV) == CMU_CLKDIVGP3_1) || \
                                              ( (DIV) == CMU_CLKDIVGP3_2) || \
                                              ( (DIV) == CMU_CLKDIVGP3_3) || \
                                              ( (DIV) == CMU_CLKDIVGP3_4) || \
                                              ( (DIV) == CMU_CLKDIVGP3_5) || \
                                              ( (DIV) == CMU_CLKDIVGP3_6) || \
                                              ( (DIV) == CMU_CLKDIVGP3_7) || \
                                              ( (DIV) == CMU_CLKDIVGP3_8) || \
                                              ( (DIV) == CMU_CLKDIVGP3_9) || \
                                              ( (DIV) == CMU_CLKDIVGP3_10) || \
                                              ( (DIV) == CMU_CLKDIVGP3_11) || \
                                              ( (DIV) == CMU_CLKDIVGP3_12) || \
                                              ( (DIV) == CMU_CLKDIVGP3_13) || \
                                              ( (DIV) == CMU_CLKDIVGP3_14) || \
                                              ( (DIV) == CMU_CLKDIVGP3_15) || \
                                              ( (DIV) == CMU_CLKDIVGP3_16) )
#define IS_CMU_PLLEXTCLKSEL( SOURCE )       ( ( (SOURCE) == CMU_PLLEXTCLKSRC_CLOSE) || \
                                              ( (SOURCE) == CMU_PLLEXTCLKSRC_GPIO) || \
                                              ( (SOURCE) == CMU_PLLEXTCLKSRC_USB12M) || \
                                              ( (SOURCE) == CMU_PLLEXTCLKSRC_USB48M) )
#define IS_CMU_PLLEXTCLKDIV( DIV )          ( ( (DIV) == CMU_CLKDIVGP3_1) || \
                                              ( (DIV) == CMU_CLKDIVGP3_2) || \
                                              ( (DIV) == CMU_CLKDIVGP3_3) || \
                                              ( (DIV) == CMU_CLKDIVGP3_4) )
#define IS_CMU_LPMODECLKCFG( CFG )          ( ( (CFG) & 0xFFFFFFF0) == 0)
#define IS_CMU_IDLEMODECLKCFG( CFG )        ( ( (CFG) & 0xFFFFFF3C) == 0)
#define IS_CMU_STOPMODECLKCFG( CFG )        ( ( (CFG) & 0xFFFFFF3C) == 0)
#define IS_CMU_IDLEMODEMDLCLKCFG( CFG )     ( ( (CFG) & 0xF200058E) == 0)
#define IS_CMU_STOPMODEMDLCLKCFG( CFG )     ( ( (CFG) & 0xF200059E) == 0)


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_CMU_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
