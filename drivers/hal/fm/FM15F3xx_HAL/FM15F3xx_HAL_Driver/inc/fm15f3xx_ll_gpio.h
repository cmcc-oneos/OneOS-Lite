/**
  ******************************************************************************
  * @file    fm15f3xx_ll_gpio.h
  * @author  SRG
  * @version V1.0.0
  * @date    2020-03-17
  * @brief
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */

#ifndef FM15F3XX_LL_GPIO_H
#define FM15F3XX_LL_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"

#if defined (GPIOA) || defined (GPIOB) || defined (GPIOC) || defined (GPIOD) || defined (GPIOE) || defined (GPIOF)
#if defined (GPIOA_CFG) || defined (GPIOB_CFG) || defined (GPIOC_CFG) || defined (GPIOD_CFG) || defined (GPIOE_CFG) || defined (GPIOF_CFG)
/** @defgroup GPIO_LL GPIO
* @{
*/

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/** @defgroup GPIO_LL_ES_INIT GPIO ExGPIOed Init structures
  * @{
  */

/**
  * @brief LL GPIO Init Structure definition
  */
typedef struct {
    uint32_t Pin;/*@ref GPIO_LL_EC_PIN or GPIO_pins_define*/

    uint32_t Alt;/*@ref GPIO_AFSelection*/

    uint32_t Dir;/*@ref GPIO_LL_EC_Direction*/

    uint32_t Speed;/*@ref GPIO_LL_EC_SLEWRATE*/

    uint32_t OType;/*@ref GPIO_LL_EC_OUTPUT*/

    uint32_t PuPd;/*@ref GPIO_LL_EC_PULL*/

    uint32_t DigitFiltEn;/*GPIO_LL_EC_DIGITFILTE*/

    uint32_t DriveStrength;/*GPIO_LL_EC_DRIVES*/

    uint32_t Lock;/*GPIO_LL_EC_LK*/

    uint32_t Irq;/*GPIO_LL_EC_IRQ*/

    uint32_t WECconfig;/*GPIO_LL_EC_WKUP*/

} LL_GPIO_InitTypeDef;

/**
  * @brief  GPIO Bit SET and Bit RESET enumeration
  */
typedef enum {
    GPIO_PIN_RESET = 0,
    GPIO_PIN_SET
} GPIO_PinState;

/**
  * @}
  */


/* ExGPIOed constants --------------------------------------------------------*/
/** @defgroup GPIO_LL_ExGPIOed_Constants GPIO ExGPIOed Constants
  * @{
  */

/** @defgroup GPIO_LL_EC_PIN PIN
  * @{
  */
#define LL_GPIO_PIN_0                      ((uint16_t)0x0001U) /*!< Select pin 0 */
#define LL_GPIO_PIN_1                      ((uint16_t)0x0002U) /*!< Select pin 1 */
#define LL_GPIO_PIN_2                      ((uint16_t)0x0004U) /*!< Select pin 2 */
#define LL_GPIO_PIN_3                      ((uint16_t)0x0008U) /*!< Select pin 3 */
#define LL_GPIO_PIN_4                      ((uint16_t)0x0010U) /*!< Select pin 4 */
#define LL_GPIO_PIN_5                      ((uint16_t)0x0020U) /*!< Select pin 5 */
#define LL_GPIO_PIN_6                      ((uint16_t)0x0040U) /*!< Select pin 6 */
#define LL_GPIO_PIN_7                      ((uint16_t)0x0080U) /*!< Select pin 7 */
#define LL_GPIO_PIN_8                      ((uint16_t)0x0100U) /*!< Select pin 8 */
#define LL_GPIO_PIN_9                      ((uint16_t)0x0200U) /*!< Select pin 9 */
#define LL_GPIO_PIN_10                     ((uint16_t)0x0400U) /*!< Select pin 10 */
#define LL_GPIO_PIN_11                     ((uint16_t)0x0800U) /*!< Select pin 11 */
#define LL_GPIO_PIN_12                     ((uint16_t)0x1000U) /*!< Select pin 12 */
#define LL_GPIO_PIN_13                     ((uint16_t)0x2000U) /*!< Select pin 13 */
#define LL_GPIO_PIN_14                     ((uint16_t)0x4000U) /*!< Select pin 14 */
#define LL_GPIO_PIN_15                     ((uint16_t)0x8000U) /*!< Select pin 15 */
#define LL_GPIO_PIN_ALL                    ((uint16_t)0xFFFFU)  /*!< Select all pins */

//GPIO_pins_define
#define GPIO_PIN_0                  LL_GPIO_PIN_0
#define GPIO_PIN_1                  LL_GPIO_PIN_1
#define GPIO_PIN_2                  LL_GPIO_PIN_2
#define GPIO_PIN_3                  LL_GPIO_PIN_3
#define GPIO_PIN_4                  LL_GPIO_PIN_4
#define GPIO_PIN_5                  LL_GPIO_PIN_5
#define GPIO_PIN_6                  LL_GPIO_PIN_6
#define GPIO_PIN_7                  LL_GPIO_PIN_7
#define GPIO_PIN_8                  LL_GPIO_PIN_8
#define GPIO_PIN_9                  LL_GPIO_PIN_9
#define GPIO_PIN_10                 LL_GPIO_PIN_10
#define GPIO_PIN_11                 LL_GPIO_PIN_11
#define GPIO_PIN_12                 LL_GPIO_PIN_12
#define GPIO_PIN_13                 LL_GPIO_PIN_13
#define GPIO_PIN_14                 LL_GPIO_PIN_14
#define GPIO_PIN_15                 LL_GPIO_PIN_15
#define GPIO_PIN_All                LL_GPIO_PIN_ALL

/**
  * @}
  */

/** @defgroup GPIO_LL_EC_Direction data direction
  * @{
  */
#define LL_GPIO_DIRECTION_IN                (0x00000000U) /*!< data input */
#define LL_GPIO_DIRECTION_OUT               (0x00000001U) /*!< data output */

/**
  * @}
  */


/** @defgroup GPIO_LL_EC_PULL Pull Up Pull Down
  * @{
  */
#define LL_GPIO_PULL_NO                    (0x00000000U) /*!< Select I/O no pull */
#define LL_GPIO_PULL_DOWN                   GPIO_CFG_PCR_PE  /*!< Select I/O pull down*/
#define LL_GPIO_PULL_UP                     GPIO_CFG_PCR_PE | GPIO_CFG_PCR_PS /*!< Select I/O pull up */
/**
  * @}
  */

/** @defgroup GPIO_LL_EC_SLEWRATE Slew Rate
  * @{
  */
#define LL_GPIO_SLEWRATE_LOW               (0x00000000U) /*!< Select I/O fast slew rate    */
#define LL_GPIO_SLEWRATE_HIGH              GPIO_CFG_PCR_SRE /*!< Select I/O low slew rate   */
/**
  * @}
  */


/** @defgroup GPIO_LL_EC_DIGITFILTE digit filter enable
  * @{
  */
#define LL_GPIO_DIGITFILTE_NO               (0x00000000U) /*!< Disable passive filter enable */
#define LL_GPIO_DIGITFILTE_EN                GPIO_CFG_DFER_DFE /*!< Enable passive filter enable */
/**
  * @}
  */

/** @defgroup GPIO_LL_EC_OUTPUT Output Type
  * @{
  */
#define LL_GPIO_OUTPUT_NOOPENDRAIN         (0x00000000U) /*!< Disable open-drain */
#define LL_GPIO_OUTPUT_OPENDRAIN            GPIO_CFG_PCR_ODE /*!< Select open-drain as output type */
/**
  * @}
  */

/** @defgroup GPIO_LL_EC_DRIVES Output Type
  * @{
  */
#define LL_GPIO_DRIVES_WEEK                (0x00000000U) /*!< Week drive strength */
#define LL_GPIO_DRIVES_STRONG               GPIO_CFG_PCR_DSE /*!< Strong drive */
/**
  * @}
  */

/** @defgroup GPIO_LL_EC_ALT Alternate Function
  * @{
  */
#define LL_GPIO_ALT_0                       ((0x0000000U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 0,analog */
#define LL_GPIO_ALT_HIZ                     (LL_GPIO_ALT_0)
#define LL_GPIO_ALT_1                       ((0x0000001U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 1,gpio */
#define LL_GPIO_ALT_GPIO                    (LL_GPIO_ALT_1)
#define LL_GPIO_ALT_2                       ((0x0000002U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 2 */
#define LL_GPIO_ALT_3                       ((0x0000003U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 3 */
#define LL_GPIO_ALT_4                       ((0x0000004U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 4 */
#define LL_GPIO_ALT_5                       ((0x0000005U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 5 */
#define LL_GPIO_ALT_6                       ((0x0000006U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 6 */
#define LL_GPIO_ALT_7                       ((0x0000007U) << GPIO_CFG_PCR_MUX_Pos) /*!< Select alternate function 7 */


//GPIO_AFSelection
#define GPIO_AF_GPIO                LL_GPIO_ALT_1
#define GPIO_AF_UART0               LL_GPIO_ALT_3
#define GPIO_AF_UART1               LL_GPIO_ALT_3
#define GPIO_AF_UART2               LL_GPIO_ALT_3
#define GPIO_AF_SPTMR0              LL_GPIO_ALT_6
#define GPIO_AF_SPTMR2              LL_GPIO_ALT_6
#define GPIO_AF_SPTMR3              LL_GPIO_ALT_6
#define GPIO_AF_SPTMR4              LL_GPIO_ALT_6
#define GPIO_AF_SPI3                LL_GPIO_ALT_2
#define GPIO_AF_I2C1                LL_GPIO_ALT_4
#define GPIO_AF_DCMI                LL_GPIO_ALT_6
#define GPIO_AF_TIMER_TRG_IN        LL_GPIO_ALT_6

/**
  * @}
  */

/** @defgroup GPIO_LL_EC_LK Lock Register
  * @{
  */
#define LL_GPIO_LK_UNLOCK                (0x00000000U) /*!< Pin control register not lock */
#define LL_GPIO_LK_LOCK                   GPIO_CFG_PCR_LK  /*!< Pin control register locked */
/**
  * @}
  */

/** @defgroup GPIO_LL_EC_CS Digial filter clock select
  * @{
  */
#define LL_GPIO_CS_BUSCLK                (0x00000000U) /*!< Digital filters are clocked by the bus clock */
#define LL_GPIO_CS_1KHz                   GPIO_CFG_DFCR_CS_Msk  /*!< Pin control register locked */
/**
  * @}
  */


/** @defgroup GPIO_LL_EC_WKUP Alternate Function
  * @{
  */
#define LL_GPIO_WKUP_CLOSED              (0x0000000U) << GPIO_CFG_PCR_WKUPCFG_Pos /*!< Wakeup closed */
#define LL_GPIO_WKUP_RISE                (0x0000001U) << GPIO_CFG_PCR_WKUPCFG_Pos /*!< Wakeup rising edge */
#define LL_GPIO_WKUP_FALL                (0x0000002U) << GPIO_CFG_PCR_WKUPCFG_Pos /*!< Wakeup falling edge */
#define LL_GPIO_WKUP_BOTH                (0x0000003U) << GPIO_CFG_PCR_WKUPCFG_Pos /*!< Wakeup both edge */
#define LL_GPIO_WKUP_HIGH                (0x0000001U) << GPIO_CFG_PCR_WKUPCFG_Pos /*!< Wakeup high level*/
#define LL_GPIO_WKUP_LOW                 (0x0000002U) << GPIO_CFG_PCR_WKUPCFG_Pos /*!< Wakeup low level */
/**
  * @}
  */


/** @defgroup GPIO_LL_EC_IRQ Interrupt Configuration
  * @{
  */
#define LL_GPIO_INTorDMA_DISABLE            (0x0000000U) << GPIO_CFG_PCR_IRQC_Pos /*!< Diable interrupt or DMA */
#define LL_GPIO_DMAonRISE                   (0x0000001U) << GPIO_CFG_PCR_IRQC_Pos /*!< DMA request on rising edge */
#define LL_GPIO_DMAonFALL                   (0x0000002U) << GPIO_CFG_PCR_IRQC_Pos /*!< DMA request on falling edge */
#define LL_GPIO_DMAonBOTHEDGE               (0x0000003U) << GPIO_CFG_PCR_IRQC_Pos /*!< DMA request on either edge */
#define LL_GPIO_INTonZERO                   (0x0000008U) << GPIO_CFG_PCR_IRQC_Pos /*!< Interrupt when logic zero */
#define LL_GPIO_INTonRISE                   (0x0000009U) << GPIO_CFG_PCR_IRQC_Pos /*!< Interrupt on rising edge */
#define LL_GPIO_INTonFALL                   (0x000000AU) << GPIO_CFG_PCR_IRQC_Pos /*!< Interrupt on falling edge */
#define LL_GPIO_INTonBOTHEDGE               (0x000000BU) << GPIO_CFG_PCR_IRQC_Pos /*!< Interrupt on either edge */
#define LL_GPIO_INTonHIGH                   (0x000000CU) << GPIO_CFG_PCR_IRQC_Pos /*!< Interrupt when logic one */

/* ExGPIOed macro ------------------------------------------------------------*/
/** @defgroup GPIO_LL_ExGPIOed_Macros GPIO ExGPIOed Macros
  * @{
  */

/** @defgroup GPIO_LL_EM_WRITE_READ Common Write and read registers Macros
  * @{
  */

/**
  * @brief  Write a value in GPIO register
  * @param  __INSTANCE__ GPIO Instance
  * @param  __REG__ Register to be written
  * @param  __VALUE__ Value to be written in the register
  * @retval None
  */
#define LL_GPIO_WriteReg(__INSTANCE__, __REG__, __VALUE__) WRITE_REG(__INSTANCE__->__REG__, (__VALUE__))

/**
  * @brief  Read a value in GPIO register
  * @param  __INSTANCE__ GPIO Instance
  * @param  __REG__ Register to be read
  * @retval Register value
  */
#define LL_GPIO_ReadReg(__INSTANCE__, __REG__) READ_REG(__INSTANCE__->__REG__)
/**
  * @}
  */

/* ExGPIOed functions --------------------------------------------------------*/
/** @defgroup GPIO_LL_ExGPIOed_Functions GPIO ExGPIOed Functions
  * @{
  */

/** @defgroup GPIO_LL_EF_GPIO_Configuration GPIO Configuration
  * @{
  */


/**
  * @brief  Configure GPIO data direction for  dedicated pins on a dedicated GPIO.
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PDDR      PDD            LL_GPIO_SetPinsDirection
  * @param  GPIOx GPIO GPIO
  * @param  PinMask This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  Direction This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_DIRECTION_IN
  *         @arg @ref LL_GPIO_DIRECTION_OUT
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinDir(GPIO_TypeDef *GPIOx, uint32_t PinMask, uint32_t Direction)
{
    if (Direction == LL_GPIO_DIRECTION_OUT)
        SET_BIT(GPIOx->PDDR, PinMask);
    else
        CLEAR_BIT(GPIOx->PDDR, PinMask);


}

/**
  * @brief  Return GPIO data direction for a dedicated pin on a dedicated GPIO.
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PDDR       PDD          LL_GPIO_GetPinDirection
  * @param  GPIOx GPIO GPIO
  * @param  PinMask This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_GPIO_DIRECTION_IN
  *         @arg @ref LL_GPIO_DIRECTION_OUT
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinDir(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    return (uint32_t)(READ_BIT(GPIOx->PDDR, PinMask));
}

/** @defgroup GPIO_LL_EF_Data_Access Data Access
  * @{
  */
/**
  * @brief  Return full input data register value for a dedicated port.
  * @rmtoll PDIR          PDI           LL_GPIO_ReadInputPort
  * @param  GPIOx GPIO Port
  * @retval Input data register value of port
  */
__STATIC_INLINE uint32_t LL_GPIO_ReadInputPort(GPIO_TypeDef *GPIOx)
{
    return (uint32_t)(READ_REG(GPIOx->PDIR));
}

/**
  * @brief  Return if input data level for several pins of dedicated port is high or low.
  * @rmtoll PDIR          PDI           LL_GPIO_IsInputPinSet
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval State of bit.
  *         @arg @ref GPIO_PIN_SET
  *         @arg @ref GPIO_PIN_RESET
  */
//警告：当IO设置成输出或开漏输出，此时无法通过本接口获取IO状态。
//若要获取IO输出状态，可以通过PDOR寄存器。如果要获取IO外部输入状态，则需要先设置输入模式并关闭开漏输出功能。
__STATIC_INLINE GPIO_PinState LL_GPIO_ReadInputDataBit(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    if ((GPIOx->PDIR & PinMask) != (uint32_t)GPIO_PIN_RESET)
        return GPIO_PIN_SET;
    else
        return GPIO_PIN_RESET;
}


/**
  * @brief  Write output data register for the port.
  * @rmtoll PDOR          PDO           LL_GPIO_WriteOutputPort
  * @param  GPIOx GPIO Port
  * @param  PortValue Level value for each pin of the port
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_WriteOutputPort(GPIO_TypeDef *GPIOx, uint32_t PortValue)
{
    WRITE_REG(GPIOx->PDOR, PortValue);
}

/**
  * @brief  Return full output data register value for a dedicated port.
  * @rmtoll PDOR          PDO           LL_GPIO_ReadOutputPort
  * @param  GPIOx GPIO Port
  * @retval Output data register value of port
  */
__STATIC_INLINE uint32_t LL_GPIO_ReadOutputPort(GPIO_TypeDef *GPIOx)
{
    return (uint32_t)(READ_REG(GPIOx->PDOR));
}

/**
  * @brief  Return if output data level for several pins of dedicated port is high or low.
  * @rmtoll PDOR          PDO           LL_GPIO_IsOutputPinSet
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval State of bit.
  *         @arg @ref GPIO_PIN_SET
  *         @arg @ref GPIO_PIN_RESET
  */
__STATIC_INLINE GPIO_PinState LL_GPIO_ReadOutputDataBit(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    if ((GPIOx->PDOR & PinMask) != (uint32_t)GPIO_PIN_RESET)
        return GPIO_PIN_SET;
    else
        return GPIO_PIN_RESET;
}

/**
  * @brief  Set several pins to high level on dedicated gpio port.
  * @rmtoll PSOR         PTSO           LL_GPIO_SetOutputPin
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetOutputPin(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    WRITE_REG(GPIOx->PSOR, PinMask);
}

/**
  * @brief  Set several pins to low level on dedicated gpio port.
  * @rmtoll PCOR         PTCO           LL_GPIO_ResetOutputPin
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_ResetOutputPin(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    WRITE_REG(GPIOx->PCOR, PinMask);
}

/**
  * @brief  Toggle data value for several pin of dedicated port.
  * @rmtoll PTOR          PTTO           LL_GPIO_TogglePin
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_TogglePin(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    WRITE_REG(GPIOx->PTOR, PinMask);
}

__STATIC_INLINE GPIO_CFG_TypeDef *LL_GPIO_GetCFG(GPIO_TypeDef* GPIOx)
{
    GPIO_CFG_TypeDef *portx = 0/*NULL*/;
    if (GPIOA == GPIOx)
        portx = GPIOA_CFG;
    else if (GPIOB == GPIOx)
        portx = GPIOB_CFG;
    else if (GPIOC == GPIOx)
        portx = GPIOC_CFG;
    else if (GPIOD == GPIOx)
        portx = GPIOD_CFG;
    else if (GPIOE == GPIOx)
        portx = GPIOE_CFG;
    else if (GPIOF == GPIOx)
        portx = GPIOF_CFG;

    return portx;
}

/**
  * @brief  Configure GPIO pull-up or pull-down for a dedicated pin on a dedicated port.
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR      PS PE           LL_GPIO_SetPinPull
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  Pull This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PULL_NO
  *         @arg @ref LL_GPIO_PULL_UP
  *         @arg @ref LL_GPIO_PULL_DOWN
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinPull(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t Pull)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_PS_Msk | GPIO_CFG_PCR_PE_Msk, Pull);
        Pin >>= 1;
    }
}

/**
  * @brief  Return GPIO pull-up or pull-down for a dedicated pin on a dedicated port
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       PS PE          LL_GPIO_GetPinPull
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_GPIO_PULL_NO
  *         @arg @ref LL_GPIO_PULL_UP
  *         @arg @ref LL_GPIO_PULL_DOWN
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinPull(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_PS_Msk | GPIO_CFG_PCR_PE_Msk));
        Pin >>= 1;
    }
    return LL_GPIO_PULL_NO;
}


/**
  * @brief  Configure the slew rate for the selected pins.
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR        SRE         LL_GPIO_SetPinSlewRate
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  SlewRate This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_SLEWRATE_HIGH
  *         @arg @ref LL_GPIO_SLEWRATE_LOW
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinSlewRate(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t SlewRate)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_SRE_Msk, SlewRate);
        Pin >>= 1;
    }
}

/**
  * @brief  Return the slew rate for the selected pins.
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR        SRE         LL_GPIO_GetPinSlewRate
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_GPIO_SLEWRATE_HIGH
  *         @arg @ref LL_GPIO_SLEWRATE_LOW
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinSlewRate(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_SRE_Msk));
        Pin >>= 1;
    }
    return LL_GPIO_SLEWRATE_LOW;
}

/**
  * @brief  Configure GPIO output type for a dedicated pin on dedicated port.
  * @note   Output type as to be set when GPIO pin is in output or
  *         alternate modes. Possible type are Push-pull or Open-drain.
  * @rmtoll PCR       ODE           LL_GPIO_SetPinOutputType
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter must be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  OutputType This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_OUTPUT_NOOPENDRAIN
  *         @arg @ref LL_GPIO_OUTPUT_OPENDRAIN
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinOD(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t OutputType)
{
//  int i;
//  for(i=0;i<16;i++)
//  {
//      if(Pin&0x01)
//          MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_ODE_Msk, OutputType);
//      Pin>>=1;
//  }
    if (OutputType == LL_GPIO_OUTPUT_OPENDRAIN)
        SET_BIT(GPIOx->ODER, Pin);
    else
        CLEAR_BIT(GPIOx->ODER, Pin);
}

/**
  * @brief  Return GPIO output type for a dedicated pin on dedicated port.
  * @note   Output type as to be set when GPIO pin is in output or
  *         alternate modes. Possible type are Push-pull or Open-drain.
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       ODE           LL_GPIO_GetPinOutputType
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_GPIO_OUTPUT_NOOPENDRAIN
  *         @arg @ref LL_GPIO_OUTPUT_OPENDRAIN
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinOD(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_ODE_Msk));
        Pin >>= 1;
    }
    return LL_GPIO_OUTPUT_NOOPENDRAIN;
}


/**
  * @brief  Set GPIO drive strength for a dedicated pin on dedicated port.
  * @note
  * @rmtoll PCR       DSE           LL_GPIO_SetDriveStrength
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter must be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  drives drive strenth
  *         LL_GPIO_DRIVES_WEEK
  *         LL_GPIO_DRIVES_STRONG
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinDriveStrength(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t drives)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_DSE_Msk, drives);
        Pin >>= 1;
    }
}

/**
  * @brief  return GPIO drive strenth for a dedicated pin on dedicated port.
  * @note
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       DSE           LL_GPIO_GetDriveStrength
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval
  *         LL_GPIO_DRIVES_WEEK
  *         LL_GPIO_DRIVES_STRONG
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinDriveStrength(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_DSE_Msk));
        Pin >>= 1;
    }
    return LL_GPIO_DRIVES_WEEK;
}

/**
  * @brief  Set GPIO muxing for a dedicated pin on dedicated port.
  * @note
  * @rmtoll PCR       MUX           LL_GPIO_SetPinMux
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter must be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  mux This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_ALT_0
  *         @arg @ref LL_GPIO_ALT_1
  *         @arg @ref LL_GPIO_ALT_2
  *         @arg @ref LL_GPIO_ALT_3
  *         @arg @ref LL_GPIO_ALT_4
  *         @arg @ref LL_GPIO_ALT_5
  *         @arg @ref LL_GPIO_ALT_6
  *         @arg @ref LL_GPIO_ALT_7
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinMux(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t mux)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_MUX_Msk, mux);
        Pin >>= 1;
    }
}

/**
  * @brief  return GPIO muxing for a dedicated pin on dedicated port.
  * @note
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       MUX           LL_GPIO_GetPinMux
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_ALT_0
  *         @arg @ref LL_GPIO_ALT_1
  *         @arg @ref LL_GPIO_ALT_2
  *         @arg @ref LL_GPIO_ALT_3
  *         @arg @ref LL_GPIO_ALT_4
  *         @arg @ref LL_GPIO_ALT_5
  *         @arg @ref LL_GPIO_ALT_6
  *         @arg @ref LL_GPIO_ALT_7
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinMux(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_MUX_Msk));
        Pin >>= 1;
    }
    return LL_GPIO_ALT_0;
}

/**
  * @brief  Set Lock pin control register for a dedicated pin on dedicated port.
  * @note
  * @rmtoll PCR       LK           LL_GPIO_SetPinLk
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter must be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  lock This parameter must be one of the following values:
  *         @arg @ref LL_GPIO_LK_UNLOCK
  *         @arg @ref LL_GPIO_LK_LOCK
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinLock(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t lock)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_LK_Msk, lock);
        Pin >>= 1;
    }
}

/**
  * @brief  return GPIO lock register for a dedicated pin on dedicated port.
  * @note
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       LK           LL_GPIO_GetPinLk
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval  This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_LK_UNLOCK
  *         @arg @ref LL_GPIO_LK_LOCK
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinLock(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_LK_Msk));
        Pin >>= 1;
    }
    return LL_GPIO_LK_UNLOCK;
}

/**
  * @brief  Configure GPIO wakeup edge for a dedicated pin on a dedicated port.
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR      WKUPCFG           LL_GPIO_SetPinWakeupCFG
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  Wakeup This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_WKUP_CLOSED
  *         @arg @ref LL_GPIO_WKUP_RISE
  *         @arg @ref LL_GPIO_WKUP_FALL
  *         @arg @ref LL_GPIO_WKUP_BOTH
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinWakeupCFG(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t Wakeup)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01) {
            CLEAR_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_WKUPCFG_Msk);
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_WKUPCFG_Msk, Wakeup);
        }
        Pin >>= 1;
    }
}

/**
  * @brief  Return GPIO wakeup edge for a dedicated pin on a dedicated port
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       WKUPCFG          LL_GPIO_GetPinWakeupCFG
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_GPIO_WKUP_CLOSED
  *         @arg @ref LL_GPIO_WKUP_RISE
  *         @arg @ref LL_GPIO_WKUP_FALL
  *         @arg @ref LL_GPIO_WKUP_BOTH
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinWakeupCFG(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_WKUPCFG_Msk));
        Pin >>= 1;
    }
    return LL_GPIO_WKUP_CLOSED;
}

/**
  * @brief  Configure interrupt for a dedicated pin on dedicated port.
  * @note
  * @rmtoll PCR       IRQC           LL_GPIO_SetPinIrq
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter must be a combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @param  Irq This parameter must be one of the following values:
  *         @arg @ref LL_GPIO_INTorDMA_DISABLE
  *         @arg @ref LL_GPIO_DMAonRISE
  *         @arg @ref LL_GPIO_DMAonFALL
  *         @arg @ref LL_GPIO_DMAonBOTHEDGE
  *         @arg @ref LL_GPIO_INTonZERO
  *         @arg @ref LL_GPIO_INTonRISE
  *         @arg @ref LL_GPIO_INTonFALL
  *         @arg @ref LL_GPIO_INTonBOTHEDGE
  *         @arg @ref LL_GPIO_INTonHIGH
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetPinINT(GPIO_TypeDef *GPIOx, uint32_t Pin, uint32_t Irq)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_IRQC, Irq);
        Pin >>= 1;
    }
}

/**
  * @brief  return GPIO Interrupt configuration for a dedicated pin on dedicated port.
  * @note
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       IRQC           LL_GPIO_GetPinIrq
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval  This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_INTorDMA_DISABLE
  *         @arg @ref LL_GPIO_DMAonRISE
  *         @arg @ref LL_GPIO_DMAonFALL
  *         @arg @ref LL_GPIO_DMAonBOTHEDGE
  *         @arg @ref LL_GPIO_INTonZERO
  *         @arg @ref LL_GPIO_INTonRISE
  *         @arg @ref LL_GPIO_INTonFALL
  *         @arg @ref LL_GPIO_INTonBOTHEDGE
  *         @arg @ref LL_GPIO_INTonHIGH
  */
__STATIC_INLINE uint32_t LL_GPIO_GetPinINT(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
    int i;
    for (i = 0; i < 16; i++) {
        if (Pin & 0x01)
            return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_IRQC));
        Pin >>= 1;
    }
    return LL_GPIO_INTorDMA_DISABLE;
}

/**
  * @brief  Clear Interrupt status flag for  dedicated pins on dedicated port.
  * @note
  * @rmtoll ISFR       ISF           LL_GPIO_ClearPinsISF
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter can be combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_ClearFlagPin_IT(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    WRITE_REG(LL_GPIO_GetCFG(GPIOx)->ISFR, ~PinMask);
}

/**
  * @brief  return GPIO Interrupt configuration for a dedicated pin on dedicated port.
  * @note
  * @note   Warning: only one pin can be passed as parameter.
  * @rmtoll PCR       ISF           LL_GPIO_GetPinISF
  * @param  GPIOx GPIO Port
  * @param  Pin This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  * @retval  This parameter can be one of the following values:
  *         0 or !0
  */
__STATIC_INLINE uint32_t LL_GPIO_GetFlagPin_IT(GPIO_TypeDef *GPIOx, uint32_t Pin)
{
//  int i;
//  for(i=0;i<16;i++)
//  {
//      if(Pin&0x01)
//          return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->PCR[i],GPIO_CFG_PCR_ISF_Msk));
//      Pin>>=1;
//  }
//  return 0;
    return READ_BIT(LL_GPIO_GetCFG(GPIOx)->ISFR, Pin);
}

/**
  * @brief  return GPIO Interrupt status on dedicated port.
  * @note
  * @rmtoll ISFR       ISF           LL_GPIO_GetPortISF
  * @param  GPIOx GPIO Port
  * @retval  This parameter can be one of the following values:0x0000~0xFFFF
  */
__STATIC_INLINE uint32_t LL_GPIO_GetFlagPort_IT(GPIO_TypeDef *GPIOx)
{
    return (uint32_t)(READ_BIT(LL_GPIO_GetCFG(GPIOx)->ISFR, GPIO_CFG_ISFR_ISF_Msk));
}

/**
  * @brief  Enable GPIO digital filter for  dedicated pins on dedicated port.
  * @note
  * @rmtoll DFER       DFE           LL_GPIO_EnablePinsDigitalFilt
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter must be combination of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_EnablePinDigitalFilt(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    SET_BIT(LL_GPIO_GetCFG(GPIOx)->DFER, PinMask);
}

/**
  * @brief  Disable GPIO digital filter for  dedicated pins on dedicated port.
  * @note
  * @rmtoll DFER       DFE           LL_GPIO_DisablePinsDigitalFilt
  * @param  GPIOx GPIO Port
  * @param  PinMask This parameter can be one of the following values:
  *         @arg @ref LL_GPIO_PIN_0
  *         @arg @ref LL_GPIO_PIN_1
  *         @arg @ref LL_GPIO_PIN_2
  *         @arg @ref LL_GPIO_PIN_3
  *         @arg @ref LL_GPIO_PIN_4
  *         @arg @ref LL_GPIO_PIN_5
  *         @arg @ref LL_GPIO_PIN_6
  *         @arg @ref LL_GPIO_PIN_7
  *         @arg @ref LL_GPIO_PIN_8
  *         @arg @ref LL_GPIO_PIN_9
  *         @arg @ref LL_GPIO_PIN_10
  *         @arg @ref LL_GPIO_PIN_11
  *         @arg @ref LL_GPIO_PIN_12
  *         @arg @ref LL_GPIO_PIN_13
  *         @arg @ref LL_GPIO_PIN_14
  *         @arg @ref LL_GPIO_PIN_15
  *         @arg @ref LL_GPIO_PIN_ALL
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_DisablePinDigitalFilt(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
    CLEAR_BIT(LL_GPIO_GetCFG(GPIOx)->DFER,  PinMask);
}

/**
  * @brief  Select digial filter clock  on dedicated port.
  * @note
  * @rmtoll DFCR       CS           LL_GPIO_SetDigitFiltClock
  * @param  GPIOx GPIO Port
  * @param  Clock This parameter must be one of the following values:
  *         @arg @ref LL_GPIO_CS_BUSCLK
  *         @arg @ref LL_GPIO_CS_1KHz
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetDigitFilterClk(GPIO_TypeDef *GPIOx, uint32_t Clock)
{
    MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->DFCR, GPIO_CFG_DFCR_CS_Msk, Clock);
}

/**
  * @brief  Set digial filter width  on dedicated port.
  * @note
  * @rmtoll DFWR       Filt           LL_GPIO_SetDigitFiltWidth
  * @param  GPIOx GPIO Port
  * @param  Width This parameter must be one of the following values:
  *         0~0x1F
  * @retval None
  */
__STATIC_INLINE void LL_GPIO_SetDigitFilterWidth(GPIO_TypeDef *GPIOx, uint32_t Width)
{
    MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->DFCR, GPIO_CFG_DFWR_FILT_Msk, Width);
}


#endif /* defined (GPIOA_CFG) || defined (GPIOB_CFG) || defined (GPIOC_CFG) || defined (GPIOD_CFG) || defined (GPIOE_CFG) || defined (GPIOF_CFG) */
#endif /* defined (GPIOA) || defined (GPIOB) || defined (GPIOC) || defined (GPIOD) || defined (GPIOE) || defined (GPIOF) */


void LL_GPIO_Init(GPIO_TypeDef *GPIOx, LL_GPIO_InitTypeDef *GPIO_InitStruct);


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
