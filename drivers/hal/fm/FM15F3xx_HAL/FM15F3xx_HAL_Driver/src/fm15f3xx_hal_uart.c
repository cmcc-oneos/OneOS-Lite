/**
 ******************************************************************************
 * @file    fm15f3xx_hal_uart.c
 * @author  MCD Application Team
 * @brief   UART HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Universal Asynchronous Receiver Transmitter (UART) peripheral:
 *           + Initialization and de-initialization functions
 *           + IO operation functions
 *           + Peripheral Control functions
 *           + Peripheral State and Errors functions
 *
 ******************************************************************************
 * @attention
 * <h2><center>&copy; COPYRIGHT(c) 2020 FudanMicroelectronics</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"
#include <stdio.h>


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup UART UART
 * @brief HAL UART module driver
 * @{
 */
#ifdef UART_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @addtogroup UART_Private_Constants
 * @{
 */


/**
 * @}
 */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup UART_Private_Functions   UART Private Functions
 * @{
 */
//static void UART_EndTxTransfer(UART_HandleTypeDef *huart);
//static void UART_EndRxTransfer(UART_HandleTypeDef *huart);
static HAL_StatusTypeDef __UART_Transmit_IT(UART_HandleTypeDef *huart);


static HAL_StatusTypeDef __UART_EndTransmit_IT(UART_HandleTypeDef *huart);


static HAL_StatusTypeDef __UART_Receive_IT(UART_HandleTypeDef *huart);


static HAL_StatusTypeDef __UART_WaitOnFlagUntilTimeout(UART_HandleTypeDef *huart, uint32_t Flag, FlagStatus Status, uint32_t Tickstart, uint32_t Timeout);


static void __UART_Config(UART_HandleTypeDef *huart);


/**
 * @}
 */

/* Exported functions ---------------------------------------------------------*/


/** @defgroup UART_Exported_Functions UART Exported Functions
 * @{
 */


/** @defgroup  Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and Configuration functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the UART mode according to the specified parameters in
 *         the UART_InitTypeDef and create the associated handle.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_Init(UART_HandleTypeDef *huart)
{
    /* Check the UART handle allocation */
    if (huart == NULL) {
        return (HAL_ERROR);
    }

    assert_param(IS_UART_INSTANCE(huart->Instance));

    if (huart->gState == HAL_UART_STATE_RESET) {
        /* Init the low level hardware */
        HAL_UART_MspInit(huart);
    }

    huart->gState = HAL_UART_STATE_BUSY;

    /* Set the UART Communication parameters */
    __UART_Config(huart);

    /* Initialize the UART state */
    huart->ErrorCode    = HAL_UART_ERROR_NONE;
    huart->gState       = HAL_UART_STATE_READY;
    huart->RxState      = HAL_UART_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  DeInitializes the UART peripheral.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_DeInit(UART_HandleTypeDef *huart)
{
    /* Check the UART handle allocation */
    if (huart == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_UART_INSTANCE(huart->Instance));

    huart->gState = HAL_UART_STATE_BUSY;

    /* DeInit the low level hardware */
    HAL_UART_MspDeInit(huart);

    huart->ErrorCode    = HAL_UART_ERROR_NONE;
    huart->gState       = HAL_UART_STATE_RESET;
    huart->RxState      = HAL_UART_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief  UART MSP Init.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
__weak void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(huart);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_UART_MspInit could be implemented in the user file
     */
}


/**
 * @brief  UART MSP DeInit.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
__weak void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(huart);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_UART_MspDeInit could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup UART_Exported_Functions_Group2 IO operation functions
 *  @brief UART Transmit and Receive functions
 *
   @verbatim
   ==============================================================================
 ##### IO operation functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Sends an amount of data in blocking mode.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be sent
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    uint16_t    * tmp;
    uint32_t    tickstart = 0U;

    /* Check that a Tx process is not already ongoing */
    if (huart->gState == HAL_UART_STATE_READY) {
        if ((pData == NULL) || (Size == 0)) {
            return (HAL_ERROR);
        }

        huart->ErrorCode    = HAL_UART_ERROR_NONE;
        huart->gState       = HAL_UART_STATE_BUSY_TX;

        /* Init tickstart for timeout managment */
        tickstart = HAL_GetTick();

        huart->TxXferSize   = Size;
        huart->TxXferCount  = Size;
        while (huart->TxXferCount > 0U) {
            huart->TxXferCount--;
            if (huart->Init.WordLength == UART_WORDLENGTH_9B_NONE) {
                if (__UART_WaitOnFlagUntilTimeout(huart, UART_FLAG_TXE, RESET, tickstart, Timeout) != HAL_OK) {
                    return (HAL_TIMEOUT);
                }
                tmp                     = (uint16_t *) pData;
                huart->Instance->BIT9CR = (((uint32_t) * tmp << 8) & 0x00010000);
                huart->Instance->TXFIFO = (*tmp & (uint16_t) 0x00FF);
                pData                   += 2U;
            } else {
                if (__UART_WaitOnFlagUntilTimeout(huart, UART_FLAG_TXE, RESET, tickstart, Timeout) != HAL_OK) {
                    return (HAL_TIMEOUT);
                }
                huart->Instance->TXFIFO = (*pData++ & (uint8_t) 0xFF);
            }
        }

        if (__UART_WaitOnFlagUntilTimeout(huart, UART_FLAG_TC, RESET, tickstart, Timeout) != HAL_OK) {
            return (HAL_TIMEOUT);
        }

        /* At end of Tx process, restore huart->gState to Ready */
        huart->gState = HAL_UART_STATE_READY;

        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Receives an amount of data in blocking mode.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be received
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_Receive(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    uint16_t    * tmp;
    uint32_t    tickstart = 0U;

    /* Check that a Rx process is not already ongoing */
    if (huart->RxState == HAL_UART_STATE_READY) {
        if ((pData == NULL) || (Size == 0)) {
            return (HAL_ERROR);
        }

        huart->ErrorCode    = HAL_UART_ERROR_NONE;
        huart->RxState      = HAL_UART_STATE_BUSY_RX;

        /* Init tickstart for timeout managment */
        tickstart = HAL_GetTick();

        huart->RxXferSize   = 0;
        huart->RxXferCount  = Size;

        /* Check the remain data to be received */
        while (huart->RxXferCount > 0U) {
            huart->RxXferCount--;
            huart->RxXferSize ++;
            if (huart->Init.WordLength == UART_WORDLENGTH_9B_NONE) {
                if (__UART_WaitOnFlagUntilTimeout(huart, UART_FLAG_RXNE, RESET, tickstart, Timeout) != HAL_OK) {
                    return (HAL_TIMEOUT);
                }
                tmp     = (uint16_t *) pData;
                *tmp    = (uint16_t)((huart->Instance->RXFIFO & (uint16_t) 0x00FF) | ((huart->Instance->BIT9CR << 7) & (uint16_t) 0x0100));
                pData   += 2U;
            } else {
                if (__UART_WaitOnFlagUntilTimeout(huart, UART_FLAG_RXNE, RESET, tickstart, Timeout) != HAL_OK) {
                    return (HAL_TIMEOUT);
                }
                *pData++ = (uint8_t)(huart->Instance->RXFIFO & (uint8_t) 0x00FF);
            }
        }

        /* At end of Rx process, restore huart->RxState to Ready */
        huart->RxState = HAL_UART_STATE_READY;

        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Sends an amount of data in non blocking mode.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be sent
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_Transmit_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size)
{
    /* Check that a Tx process is not already ongoing */
    if (huart->gState == HAL_UART_STATE_READY) {
        if ((pData == NULL) || (Size == 0)) {
            return (HAL_ERROR);
        }

        huart->pTxBuffPtr   = pData;
        huart->TxXferSize   = Size;
        huart->TxXferCount  = Size;

        huart->ErrorCode    = HAL_UART_ERROR_NONE;
        huart->gState       = HAL_UART_STATE_BUSY_TX;

        /* Enable the UART Transmit data register empty Interrupt */
        __HAL_UART_ENABLE_IT(huart, UART_IT_TXE);

        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}

/**
 * @brief  Sends an amount of data in non blocking mode.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be sent
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_Transmit_DMA(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size)
{
    huart->gState       = HAL_UART_STATE_READY;
    //Check UART status //等待上一次发送完成
    if ((LL_DMA_GetMajorLoopCiter(huart->hdmatx->Init.Channel) > 0) || \
        (!LL_UART_GetFlag(huart->Instance, UART_FLAG_TC))) {
        huart->gState       = HAL_UART_STATE_BUSY_TX;
    }

    //Check DMA status//等待上一次DMA大循环结束done
    if ((!LL_DMA_GET_DONE(huart->hdmatx->Init.Channel)) && \
        LL_DMA_GET_ACTIVE(huart->hdmatx->Init.Channel)) {
        huart->gState       = HAL_UART_STATE_BUSY_TX;
    }

    /* Check that a Tx process is not already ongoing */
    if (huart->gState == HAL_UART_STATE_READY) {
        if ((pData == NULL) || (Size == 0)) {
            return (HAL_ERROR);
        }

        huart->pTxBuffPtr   = pData;
        huart->TxXferSize   = Size;
        huart->TxXferCount  = Size;

        huart->ErrorCode    = HAL_UART_ERROR_NONE;
        huart->gState       = HAL_UART_STATE_BUSY_TX;

        //clear err flag
        __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_TXOE | UART_FLAG_RXOE | UART_FLAG_PE | UART_FLAG_FE);

        /* Enable the UART Transmit data register empty DMA */
        __HAL_UART_ENABLE_DMA(huart, UART_IT_TXE_DMA);
        __HAL_UART_ENABLE_IT(huart,  UART_IT_ERR);

        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}
/**
 * @brief  Receives an amount of data in non blocking mode
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be received
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_Receive_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size)
{
    uint8_t tmp;
    /* Check that a Rx process is not already ongoing */
    if (huart->RxState == HAL_UART_STATE_READY) {
        if ((pData == NULL) || (Size == 0)) {
            return (HAL_ERROR);
        }

        huart->pRxBuffPtr    = pData;
        huart->RxXferSize    = 0;
        huart->RxXferCount   = Size;

        huart->ErrorCode     = HAL_UART_ERROR_NONE;
        huart->RxState       = HAL_UART_STATE_BUSY_RX;
        //clear rxfifo
        while (__HAL_UART_GET_RXFIFO_LEN(huart) || __HAL_UART_GET_FLAG(huart, UART_FLAG_RXBUSY)) {
            tmp = huart->Instance->RXFIFO;
        }
        (void)tmp;
        /* Enable the UART Data Register not empty Interrupts and Error Interrupt: (Frame error, Parity error, overrun error) */
        __HAL_UART_ENABLE_IT(huart, UART_IT_RXNE | UART_IT_ERR);

        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}

/**
 * @brief  Receives an amount of data in non blocking mode
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be received
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_UART_Receive_DMA(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size)
{
    uint8_t tmp;
    //Check DMA status
    if (!LL_DMA_GET_DONE(huart->hdmarx->Init.Channel) && \
        LL_DMA_GET_ACTIVE(huart->hdmarx->Init.Channel)) {
        huart->RxState       = HAL_UART_STATE_BUSY_RX;
    }

    /* Check that a Rx process is not already ongoing */
    if (huart->RxState == HAL_UART_STATE_READY) {
        if ((pData == NULL) || (Size == 0)) {
            return (HAL_ERROR);
        }

        huart->pRxBuffPtr    = pData;
        huart->RxXferSize    = 0;
        huart->RxXferCount   = Size;

        huart->ErrorCode     = HAL_UART_ERROR_NONE;
        huart->RxState       = HAL_UART_STATE_BUSY_RX;

        //clear rxfifo
        while (__HAL_UART_GET_RXFIFO_LEN(huart) || __HAL_UART_GET_FLAG(huart, UART_FLAG_RXBUSY)) {
            tmp = huart->Instance->RXFIFO;
        }
        (void)tmp;
        //clear err flag
        __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_PE | UART_FLAG_RXOE | UART_FLAG_FE);

        /* Enable the UART Data Register DMA not empty Interrupts and Error Interrupt: (Frame error, Parity error, overrun error) */
        __HAL_UART_ENABLE_DMA(huart, UART_IT_RXNE_DMA);
        __HAL_UART_ENABLE_IT(huart,  UART_IT_ERR);

        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  This function handles UART interrupt request.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
void HAL_UART_IRQHandler(UART_HandleTypeDef *huart)
{
    uint32_t    isrflags    = huart->Instance->STATUS;
    uint32_t    its         = huart->Instance->INTCFG;
    uint32_t    errorflags  = 0x00U;

    /* If no error occurs */
    errorflags = (isrflags & (uint32_t)(UART_FLAG_FE | UART_FLAG_PE | UART_FLAG_RXOE | UART_FLAG_TXOE));
    if (errorflags == RESET) {
        /* UART in mode Receiver -------------------------------------------------*/
        if (((isrflags & UART_FLAG_RXNE) != RESET) && ((its & UART_IT_RXNE) != RESET)) {
            __UART_Receive_IT(huart);
            return;
        }
    }

    /* If some errors occur */
    if ((errorflags != RESET) && (((its & UART_IT_ERR) != RESET))) {
        /* UART parity error interrupt occurred ----------------------------------*/
        if (((isrflags & UART_FLAG_PE) != RESET)) {
            huart->ErrorCode |= HAL_UART_ERROR_PE;
        }

        /* UART frame error interrupt occurred -----------------------------------*/
        if (((isrflags & UART_FLAG_FE) != RESET)) {
            huart->ErrorCode |= HAL_UART_ERROR_FE;
        }

        /* UART Over-Run interrupt occurred --------------------------------------*/
        if (((isrflags & UART_FLAG_RXOE) != RESET) || ((isrflags & UART_FLAG_TXOE) != RESET)) {
            huart->ErrorCode |= HAL_UART_ERROR_ORE;
        }

        /* Call UART Error Call back function--------------------------*/
        if (huart->ErrorCode != HAL_UART_ERROR_NONE) {
            /* UART in mode Receiver -----------------------------------------------*/
            if (((isrflags & UART_FLAG_RXNE) != RESET) && ((its & UART_IT_RXNE) != RESET)) {
                __UART_Receive_IT(huart);
            }

            HAL_UART_ErrorCallback(huart);
            /* reset status */
            __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_RXOE | UART_FLAG_FE | UART_FLAG_PE | UART_FLAG_TXOE);
            huart->ErrorCode = HAL_UART_ERROR_NONE;
        }
        return;
    } /* End if some error occurs */

    /* UART in mode Transmitter ------------------------------------------------*/
    if (((isrflags & UART_FLAG_TXE) != RESET) && ((its & UART_IT_TXE) != RESET)) {
        __UART_Transmit_IT(huart);
        return;
    }

    /* UART in mode Transmitter end --------------------------------------------*/
    if (((isrflags & UART_FLAG_TC) != RESET) && ((its & UART_IT_TC) != RESET)) {
        __UART_EndTransmit_IT(huart);
        return;
    }
}


/**
 * @brief  Tx Transfer completed callbacks.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
__weak void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(huart);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_UART_TxCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Tx Half Transfer completed callbacks.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
__weak void HAL_UART_TxHalfCpltCallback(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(huart);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_UART_TxCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Rx Transfer completed callbacks.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
__weak void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(huart);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_UART_TxCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Rx Half Transfer completed callbacks.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
__weak void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(huart);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_UART_TxCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  UART error callbacks.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
__weak void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(huart);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_UART_ErrorCallback could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup Peripheral State and Errors functions
 *  @brief   UART State and Errors functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral State and Errors functions #####
   ==============================================================================
   [..]
   This subsection provides a set of functions allowing to return the State of
   UART communication process, return Peripheral Errors occurred during communication
   process
   (+) HAL_UART_GetState() API can be helpful to check in run-time the state of the UART peripheral.
   (+) HAL_UART_GetError() check in run-time errors that could be occurred during communication.

   @endverbatim
 * @{
 */


/**
 * @brief  Returns the UART state.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval HAL state
 */
HAL_UART_StateTypeDef HAL_UART_GetState(UART_HandleTypeDef *huart)
{
    uint32_t temp1 = 0x00U, temp2 = 0x00U;
    temp1   = huart->gState;
    temp2   = huart->RxState;

    return ((HAL_UART_StateTypeDef)(temp1 | temp2));
}


/**
 * @brief  Return the UART error code
 * @param  huart  pointer to a UART_HandleTypeDef structure that contains
 *              the configuration information for the specified UART.
 * @retval UART Error Code
 */
uint32_t HAL_UART_GetError(UART_HandleTypeDef *huart)
{
    return (huart->ErrorCode);
}


/**
 * @}
 */


/**
 * @brief  This function handles UART Communication Timeout.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @param  Flag specifies the UART flag to check.
 * @param  Status The new Flag status (SET or RESET).
 * @param  Tickstart Tick start value
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
static HAL_StatusTypeDef __UART_WaitOnFlagUntilTimeout(UART_HandleTypeDef *huart, uint32_t Flag, FlagStatus Status, uint32_t Tickstart, uint32_t Timeout)
{
    /* Wait until flag is set */
    while ((__HAL_UART_GET_FLAG(huart, Flag) ? SET : RESET) == Status) {
        /* Check for the Timeout */
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0U) || ((HAL_GetTick() - Tickstart) > Timeout)) {
                /* clear ERR Flag (Frame error, parity error, overrun error) */
                __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_FE | UART_FLAG_PE | UART_FLAG_TXOE | UART_FLAG_RXOE);

                if (Flag == UART_FLAG_TXE || Flag == UART_FLAG_TC)
                    huart->gState  = HAL_UART_STATE_READY;
                else if (Flag == UART_FLAG_RXNE)
                    huart->RxState = HAL_UART_STATE_READY;

                return (HAL_TIMEOUT);
            }
        }
    }

    return (HAL_OK);
}


/**
 * @brief  End ongoing Tx transfer on UART peripheral (following error detection or Transmit completion).
 * @param  huart UART handle.
 * @retval None
 */
//static void UART_EndTxTransfer(UART_HandleTypeDef *huart)
//{
//  /* Disable TXEIE and TCIE interrupts */
//  __HAL_UART_DISABLE_IT(huart, UART_IT_TXE | UART_IT_TC);

//  /* At end of Tx process, restore huart->gState to Ready */
//  huart->gState = HAL_UART_STATE_READY;
//}


/**
 * @brief  End ongoing Rx transfer on UART peripheral (following error detection or Reception completion).
 * @param  huart UART handle.
 * @retval None
 */
//static void UART_EndRxTransfer(UART_HandleTypeDef *huart)
//{
//  /* Disable RXNE, and ERR interrupts */
//  __HAL_UART_DISABLE_IT(huart, UART_IT_RXNE | UART_IT_ERR);

//  /* At end of Rx process, restore huart->RxState to Ready */
//  huart->RxState = HAL_UART_STATE_READY;
//}


/**
 * @brief  Sends an amount of data in non blocking mode.
 * @param  huart Pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval HAL status
 */
static HAL_StatusTypeDef __UART_Transmit_IT(UART_HandleTypeDef *huart)
{
    uint16_t* tmp;

    /* Check that a Tx process is ongoing */
    if (huart->gState == HAL_UART_STATE_BUSY_TX) {
        if (huart->Init.WordLength == UART_WORDLENGTH_9B_NONE) {
            tmp = (uint16_t *) huart->pTxBuffPtr;

            huart->Instance->BIT9CR = (((uint32_t) * tmp << 8) & 0x00010000);
            huart->Instance->TXFIFO = (*tmp & (uint16_t) 0x00FF);
            huart->pTxBuffPtr       += 2U;
        } else {
            huart->Instance->TXFIFO = (uint8_t)(*huart->pTxBuffPtr++ & (uint8_t) 0x00FF);
        }

        if (--huart->TxXferCount == 0U) {
            /* Disable the UART Transmit data register empty Interrupt */
            __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);

            /* Enable the UART Transmit Complete Interrupt */
            __HAL_UART_ENABLE_IT(huart, UART_IT_TC);
        }
        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Wraps up transmission in non blocking mode.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval HAL status
 */
static HAL_StatusTypeDef __UART_EndTransmit_IT(UART_HandleTypeDef *huart)
{
    /* Disable the UART Transmit Complete Interrupt */
    __HAL_UART_DISABLE_IT(huart, UART_IT_TC);

    /* Tx process is ended, restore huart->gState to Ready */
    huart->gState = HAL_UART_STATE_READY;

    HAL_UART_TxCpltCallback(huart);

    return (HAL_OK);
}


/**
 * @brief  Receives an amount of data in non blocking mode
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval HAL status
 */
static HAL_StatusTypeDef __UART_Receive_IT(UART_HandleTypeDef *huart)
{
    uint16_t* tmp;

    /* Check that a Rx process is ongoing */
    if (huart->RxState == HAL_UART_STATE_BUSY_RX) {
        if (huart->Init.WordLength == UART_WORDLENGTH_9B_NONE) {
            tmp                 = (uint16_t *) huart->pRxBuffPtr;
            *tmp                = (uint16_t)(huart->Instance->RXFIFO | ((huart->Instance->BIT9CR << 7) & (uint16_t) 0x0100));
            huart->pRxBuffPtr   += 2U;
        } else {
            *huart->pRxBuffPtr++ = (uint8_t)(huart->Instance->RXFIFO & (uint8_t) 0x00FF);
        }
        huart->RxXferSize ++;
        if (--huart->RxXferCount == 0U) {
            /* Rx process is completed, restore huart->RxState to Ready */
            huart->RxState = HAL_UART_STATE_READY;

            HAL_UART_RxCpltCallback(huart);

            return (HAL_OK);
        }
        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Configures the UART peripheral.
 * @param  huart pointer to a UART_HandleTypeDef structure that contains
 *                the configuration information for the specified UART module.
 * @retval None
 */
static void __UART_Config(UART_HandleTypeDef *huart)
{
    uint32_t mdlclk, mdldiv, SampleFactor;
    /* Check the parameters */
    assert_param(IS_UART_BAUDRATE(huart->Init.BaudRate));
    assert_param(IS_UART_STOPBITS(huart->Init.StopBits));
    assert_param(IS_UART_WORD_LENGTH(huart->Init.WordLength));
    assert_param(IS_UART_TRANSFER_MODE(huart->Init.TransferMode));

    /* Get Clock */
    mdldiv = 1 << LL_CMU_GetUARTClkPrescaler();
    switch (LL_CMU_GetUARTClkSrc()) {
    case CMU_UARTCLKSRC_SYSCLK: {
        mdlclk = HAL_CMU_GetSysClock() / mdldiv;
        break;
    }
    case CMU_UARTCLKSRC_IRC16M: {
        mdlclk = IRC16M_VALUE / mdldiv;
        break;
    }
    case CMU_UARTCLKSRC_OSCCLK: {
        mdlclk = HAL_CMU_GetOscClock() / mdldiv;
        break;
    }
    default: {
        mdlclk = HAL_CMU_GetSysClock() / mdldiv;
        break;
    }
    }

    /* Configure the UART Baud Rate */
    if (huart->Init.BaudRate != 0U) {
        if (huart->Init.BaudRate > 1499999) {
            SampleFactor = UART_SampleFactor_4;
        } else {
            SampleFactor = UART_SampleFactor_16;
        }
        LL_UART_SetBaudRate(huart->Instance, mdlclk, SampleFactor, huart->Init.BaudRate);
    }
    /*USART CR Configuration*/
    MODIFY_REG(huart->Instance->CR, UART_CR_RXEN | UART_CR_TXEN | UART_CR_PDSEL | UART_CR_STOPSEL, \
               huart->Init.TransferMode | huart->Init.WordLength | huart->Init.StopBits);

    /* if BaudRate = 0, the UART Will Auto Detect Baud Rate */
    if (huart->Init.BaudRate == 0U) {
        LL_UART_StartAutoDetect(huart->Instance);
    }
}


/**
 * @}
 */

#endif /* UART_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
