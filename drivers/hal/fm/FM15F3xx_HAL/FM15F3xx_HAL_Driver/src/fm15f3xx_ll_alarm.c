/**
 ******************************************************************************
 * @file    fm15f3xx_ll_alarm.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_alarm.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */


/** @addtogroup ADC_LL
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* Exported functions --------------------------------------------------------*/


/** @addtogroup ALM_LL_Exported_Functions
 * @{
 */


/**
 * @brief  Initializes the ALMx GroupA/B/C/D Monitor
 * @note   This function is used to configure the global monitor features of the ALM.
 * @retval None
 */
void LL_ALM_GroupABCDMonitorInit(void)
{
    //GROUPA0
    WRITE_REG(ALM_MONITOR->GROUPA0, (LL_ALM_MONITOR_RSTEN << ALM_GROUPA0_A03_Pos) | (LL_ALM_MONITOR_RSTEN << ALM_GROUPA0_A02_Pos) | (LL_ALM_MONITOR_RSTEN << ALM_GROUPA0_A01_Pos) | (LL_ALM_MONITOR_RSTEN));    //Light Sensor/VDDL/VDDH/LDO12
    //GROUPA1
    WRITE_REG(ALM_MONITOR->GROUPA1, (LL_ALM_MONITOR_INTEN << ALM_GROUPA1_A07_Pos) | (LL_ALM_MONITOR_RSTEN << ALM_GROUPA1_A06_Pos) | (LL_ALM_MONITOR_RSTEN << ALM_GROUPA1_A05_Pos) | (LL_ALM_MONITOR_RSTEN));    //LDO16L/VDDGLITCH/IRC4M/TIMER0
    //GROUPA2
    WRITE_REG(ALM_MONITOR->GROUPA2, LL_ALM_MONITOR_INTEN);                                                                                                                                                      //TIMER1
    //GROUPA3
    WRITE_REG(ALM_MONITOR->GROUPA3, LL_ALM_MONITOR_INTEN);                                                                                                                                                      //SHIELD
    //GROUPB0
    WRITE_REG(ALM_MONITOR->GROUPB0, (LL_ALM_MONITOR_INTEN << ALM_GROUPB0_B03_Pos) | (LL_ALM_MONITOR_INTEN << ALM_GROUPB0_B02_Pos) | (LL_ALM_MONITOR_INTEN << ALM_GROUPB0_B01_Pos) | (LL_ALM_MONITOR_INTEN));    //SHIELD
    //GROUPC0
    WRITE_REG(ALM_MONITOR->GROUPC0, (LL_ALM_MONITOR_RECEN << ALM_GROUPC0_C03_Pos) | (LL_ALM_MONITOR_RECEN << ALM_GROUPC0_C02_Pos) | (LL_ALM_MONITOR_RSTEN << ALM_GROUPC0_C01_Pos) | (LL_ALM_MONITOR_INTEN));    //FLASH_PAR_ERR_1/FLASH_PAR_ERR_2/FLASH_PAR_ERR_FF/FLASH_NOT_ERRBUG_FF
    //GROUPC1
    WRITE_REG(ALM_MONITOR->GROUPC1, (LL_ALM_MONITOR_RECEN << ALM_GROUPC1_C07_Pos) | (LL_ALM_MONITOR_RECEN << ALM_GROUPC1_C06_Pos) | (LL_ALM_MONITOR_RECEN << ALM_GROUPC1_C05_Pos) | (LL_ALM_MONITOR_RECEN));    //FLASH_CTRL_ERR/RAM_PAR_ERR/RAE_RAM_PAR_ERR/CACHE_RAM_PAR_E_RR
    //GROUPC2
    WRITE_REG(ALM_MONITOR->GROUPC2, (LL_ALM_MONITOR_RSTEN << ALM_GROUPC2_C11_Pos) | (LL_ALM_MONITOR_RSTEN << ALM_GROUPC2_C10_Pos) | (LL_ALM_MONITOR_RECEN << ALM_GROUPC2_C09_Pos) | (LL_ALM_MONITOR_RSTEN));    //FLASH_CTRL_ERR/RAM_PAR_ERR/RAE_RAM_PAR_ERR/CACHE_RAM_PAR_E_RR
    //GROUPC3
    WRITE_REG(ALM_MONITOR->GROUPC3, (LL_ALM_MONITOR_RECEN << ALM_GROUPC3_C13_Pos) | (LL_ALM_MONITOR_RSTEN));                                                                                                    //REGISTER_PARITY_E_RR/MODECTRL_RPT_ER_R
    //GROUPC4
    WRITE_REG(ALM_MONITOR->GROUPC4, (LL_ALM_MONITOR_INTEN << ALM_GROUPC4_C17_Pos) | (LL_ALM_MONITOR_INTEN << ALM_GROUPC4_C16_Pos) | (LL_ALM_MONITOR_INTEN << ALM_GROUPC4_C15_Pos) | (LL_ALM_MONITOR_INTEN));    //RNG_ERR/BCA_ERR/HASH_ERR/PAE_ERR
    //GROUPC5
    WRITE_REG(ALM_MONITOR->GROUPC5, LL_ALM_MONITOR_RSTEN);                                                                                                                                                      //CPU_SELF
    //GROUPD0
    WRITE_REG(ALM_MONITOR->GROUPD0, (LL_ALM_MONITOR_RECEN << ALM_GROUPD0_D03_Pos) | (LL_ALM_MONITOR_RECEN << ALM_GROUPD0_D02_Pos) | (LL_ALM_MONITOR_RSTEN << ALM_GROUPD0_D01_Pos) | (LL_ALM_MONITOR_RSTEN));    //SYSTEM_SELF_ALARM1/SYSTEM_SELF_ALARM2/PA_ALARM/PB_ALARM/
    //GROUPD1
    WRITE_REG(ALM_MONITOR->GROUPD1, (LL_ALM_MONITOR_RECEN << ALM_GROUPD1_D07_Pos) | (LL_ALM_MONITOR_RECEN << ALM_GROUPD1_D06_Pos) | (LL_ALM_MONITOR_RECEN << ALM_GROUPD1_D05_Pos) | (LL_ALM_MONITOR_RECEN));    //PC_ALARM/PD_ALARM/PE_ALARM/PF_ALARM
}


/**
 * @brief  Initializes the ALMx GroupE Monitor
 * @note   This function is used to configure the global monitor features of the ALM.
 * @retval None
 */
void LL_ALM_GroupEMonitorInit(void)
{
    LL_ALM_EnableGroupEReset(LL_ALM_RESETE_POR | LL_ALM_RESETE_BOR | LL_ALM_RESETE_NRST | LL_ALM_RESETE_LDO12L | LL_ALM_RESETE_LDO16L);
}


/**
 * @brief  Initializes the Sensor
 * @note   This function is used to configure the global monitor features of the ALM.
 * @retval None
 */
void LL_ALM_SensorInit(void)
{
    LL_ALM_DisableSensors0(LL_ALM_SENSORE0_LIGHT | LL_ALM_SENSORE0_IRC4M | LL_ALM_SENSORE1_FRPTA | LL_ALM_SENSORE1_FRPTB | LL_ALM_SENSORE1_SLD);
    LL_ALM_EnableSensors0(LL_ALM_SENSORE0_VDET | LL_ALM_SENSORE0_LDO12L | LL_ALM_SENSORE0_LDO16L | LL_ALM_SENSORE0_GLITCH);
}


/**
 * @}
 */


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

