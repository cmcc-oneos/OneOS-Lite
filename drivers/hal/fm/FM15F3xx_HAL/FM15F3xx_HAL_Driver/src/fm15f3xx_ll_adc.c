/**
 ******************************************************************************
 * @file    fm15f3xx_ll_adc.c
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_ll_adc.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

#if defined (ADC0)


/** @addtogroup ADC_LL
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* Exported functions --------------------------------------------------------*/


/** @addtogroup ADC_LL_Exported_Functions
 * @{
 */


/**
 * @brief  Set each @ref LL_ADC_InitTypeDef field to default value.
 * @param  ADC_InitStruct pointer to a @ref LL_ADC_InitTypeDef structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_ADC_StructInitParam(LL_ADC_InitTypeDef *ADC_InitStruct)
{
    /* Set ADC_ParamInitStruct fields to default values */
    ADC_InitStruct->ADCEN           = LL_ADC_DISABLE;
    ADC_InitStruct->WorkClkSel      = LL_ADC_WORKCLKSEL_16M;
    ADC_InitStruct->ClkSel          = LL_ADC_CLKSEL_WORKCLKDIV1;
    ADC_InitStruct->WorkClkDiv      = LL_ADC_WORKCLKDIV_2;
    ADC_InitStruct->VrefSel         = LL_ADC_VREFH;
    ADC_InitStruct->LowPowerMode    = LL_ADC_LOWPOWER_DISABLE;
    ADC_InitStruct->AutoSleep       = LL_ADC_AUTOSLEEP_ENABLE;
    ADC_InitStruct->VcomEn          = LL_ADC_VCOM_DISABLE;
    ADC_InitStruct->SleepWaitTime   = 0x0008U;
    ADC_InitStruct->WorkMode        = LL_ADC_WORKMODE_POLLING;
    ADC_InitStruct->OneShot         = LL_ADC_ONESHOT_DISABLE;
    ADC_InitStruct->SampleTime      = 0x0U;
    ADC_InitStruct->ReadyTime       = 0x40U;
    ADC_InitStruct->ConvertTime     = 0x0AU;
    ADC_InitStruct->SampleHoldTime  = 0x3U;
}


/**
 * @brief  Set each @ref LL_ADC_ChannelInitTypeDef field to default value.
 * @param  ADC_ChannelStruct pointer to a @ref LL_ADC_ChannelInitTypeDef structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_ADC_ChannelStructInit(LL_ADC_ChannelInitTypeDef *ADC_ChannelInitStruct)
{
    ADC_ChannelInitStruct->ChxEn            = LL_ADC_CHx_DISABLE;
    ADC_ChannelInitStruct->ChxDiff          = LL_ADC_CHxDIFF_DISABLE;
    ADC_ChannelInitStruct->ChxIn            = LL_ADC_CHxIN_ADC12;
    ADC_ChannelInitStruct->ChxTrigSrc       = LL_ADC_CHxTRIGSRC_SOFTWARE;
    ADC_ChannelInitStruct->ChxTrigInvEn     = LL_ADC_CHxTRIGINV_DISABLE;
    ADC_ChannelInitStruct->ChxTrigEdge      = LL_ADC_CHxTRIGEDGE_RISING;
    ADC_ChannelInitStruct->ChxTrigDly       = 0x0U;
    ADC_ChannelInitStruct->ChxOffsetEn      = LL_ADC_CHxOFFSET_DISABLE;
    ADC_ChannelInitStruct->ChxAverageEn     = LL_ADC_CHxAVERAGE_ENABLE;
    ADC_ChannelInitStruct->ChxAverageNum    = LL_ADC_CHxAVERAGENUM_8;
    ADC_ChannelInitStruct->ChxCompareEn     = LL_ADC_CHxCOMPARE_DISABLE;
    ADC_ChannelInitStruct->ChxCompareMode   = LL_ADC_CHxCOMPAREMODE_LARGER;
}

/**
 * @brief  Initializes the ADCx peripheral according to the specified parameters
 *         in the LL_ADC_StructInitParam.
 * @note   This function is used to configure the global features of the ADC,
 *         however, the rest of the configuration parameters are specific to
 *         the channels group.
 * @param  ADCx: where x can be 0 to select the ADC peripheral.
 * @param  ADC_InitStruct: pointer to an LL_ADC_InitParamTypeDef structure that
 *         contains the configuration information for the specified ADC peripheral.
 * @retval None
 */
void LL_ADC_Init(ADC_TypeDef *ADCx, LL_ADC_InitTypeDef *ADC_InitStruct)
{
    MODIFY_REG(ADCx->CTRL, ADC_CTRL_ADCEN, ADC_InitStruct->ADCEN);
    //ADC_work clk cfg
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_ADCSEL | CMU_MDLCLKB_ADCDIV, ADC_InitStruct->WorkClkSel | ADC_InitStruct->WorkClkDiv);
    MODIFY_REG(ADCx->CFG,
               ADC_CFG_CLKSEL | ADC_CFG_VREFSEL | ADC_CFG_LPM | ADC_CFG_ASLEEP | ADC_CFG_VCOMEN | ADC_CFG_TSLEEP,
               ADC_InitStruct->ClkSel | ADC_InitStruct->VrefSel | ADC_InitStruct->LowPowerMode | ADC_InitStruct->AutoSleep | ADC_InitStruct->VcomEn | (ADC_InitStruct->SleepWaitTime << ADC_CFG_TSLEEP_Pos));

    MODIFY_REG(ADCx->TIME,
               ADC_TIME_SAMPLE | ADC_TIME_READY | ADC_TIME_CONVERT | ADC_TIME_SAMPLHOLD,
               (ADC_InitStruct->SampleTime << ADC_TIME_SAMPLE_Pos) | (ADC_InitStruct->ReadyTime << ADC_TIME_READY_Pos) | \
               (ADC_InitStruct->ConvertTime << ADC_TIME_CONVERT_Pos) | (ADC_InitStruct->SampleHoldTime << ADC_TIME_SAMPLHOLD_Pos));
    //adc_work Configuration:
    MODIFY_REG(ADCx->WORK,
               ADC_WORK_MODE | ADC_WORK_SAMPLETIMEEN | ADC_WORK_ONESHOT | ADC_WORK_DONESTOPEN | ADC_WORK_POLLINGNUM | ADC_WORK_DONESAMPMODE | ADC_WORK_RESULTGETMODE,
               ADC_InitStruct->WorkMode | LL_ADC_WORKST_DISABLE | ADC_InitStruct->OneShot | LL_ADC_DONESTOP_ENABLE | (0x00 << ADC_WORK_POLLINGNUM_Pos) | LL_ADC_WORKST_ENABLE | LL_ADC_DONESAMPLEMODE_ADCCLKNEG | LL_ADC_RESULTGETMODE_0CYCLE);
}


/**
 * @brief  Initializes the ADC Channel according to the specified parameters
 *         in the LL_ADC_StructInitParam.
 * @note   This function is used to configure the global features of the ADC
 *         Channel.
 * @param  ADCx: where x can be 0 to select the ADC peripheral.
 * @param  Channel: where x can be 0, 1, 2, 3 to select the Channel.
 * @param  ADC_InitStruct: pointer to an LL_ADC_InitParamTypeDef structure that
 *         contains the configuration information for the specified ADC peripheral.
 * @retval None
 */
void LL_ADC_ChannelInit(ADC_TypeDef *ADCx, uint32_t Channel, LL_ADC_ChannelInitTypeDef *ADC_ChannelInitTypeDef)
{
    //Chx_cfg Configuration:
    MODIFY_REG(ADCx->CHANNEL[Channel].CFG,
               ADC_CHxCFG_EN | ADC_CHxCFG_DIFF | ADC_CHxCFG_IN | ADC_CHxCFG_TRGSC | ADC_CHxCFG_TRGINV | ADC_CHxCFG_TRGEDGE | ADC_CHxCFG_TRGDLY,
               ADC_ChannelInitTypeDef->ChxEn | ADC_ChannelInitTypeDef->ChxDiff | ADC_ChannelInitTypeDef->ChxIn | ADC_ChannelInitTypeDef->ChxTrigSrc | \
               ADC_ChannelInitTypeDef->ChxTrigInvEn | ADC_ChannelInitTypeDef->ChxTrigEdge | (ADC_ChannelInitTypeDef->ChxTrigDly << ADC_CHxCFG_TRGDLY_Pos));
    //Chx_DRS Configuration:
    MODIFY_REG(ADCx->CHANNEL[Channel].DRS,
               ADC_CHxDRS_OFFEN | ADC_CHxDRS_AVGEN | ADC_CHxDRS_AVGNUM | ADC_CHxDRS_CMPEN | ADC_CHxDRS_CMPM,
               ADC_ChannelInitTypeDef->ChxOffsetEn | ADC_ChannelInitTypeDef->ChxAverageEn | ADC_ChannelInitTypeDef->ChxAverageNum | \
               ADC_ChannelInitTypeDef->ChxCompareEn | ADC_ChannelInitTypeDef->ChxCompareMode);
}


/**
 * @}
 */

#endif /* ADC0 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
