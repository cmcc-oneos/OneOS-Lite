/**
 ******************************************************************************
 * @file    fm15f3xx_ll_cmu.c
 * @author  ZJH
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_cmu.h"
#include "fm15f3xx_ll_pmu.h"
#include "fm15f3xx_system.h"


/*----------------------------------------------------------------------------
   Clock Variable definitions
 *----------------------------------------------------------------------------*/


/* ToDo: initialize SystemCoreClock with the system core clock frequency value
         achieved after system intitialization.
         This means system core clock frequency after call to SystemInit()    */
static uint32_t SystemCoreClock = IRC16M_VALUE;

static uint32_t OscClock = 12000000; //12MHz

static CMU_SysClk_CFG const SysClkCfg[40] = {
    { 1,  0x00 }, { 2,  0x01 }, { 3,  0x02 }, { 4,  0x03 }, { 5,  0x04 }, { 6,  0x05 }, { 7,   0x06 }, { 8,   0x07 }, { 9,   0x08 }, { 10,  0x09 },
    { 11, 0x0A }, { 12, 0x0B }, { 13, 0x0C }, { 14, 0x0D }, { 15, 0x0E }, { 16, 0x0F }, { 18,  0x18 }, { 20,  0x19 }, { 22,  0x1A }, { 24,  0x1B },
    { 26, 0x1C }, { 28, 0x1D }, { 30, 0x1E }, { 32, 0x1F }, { 36, 0x28 }, { 40, 0x29 }, { 44,  0x2A }, { 48,  0x2B }, { 52,  0x2C }, { 56,  0x2D },
    { 60, 0x2E }, { 64, 0x2F }, { 72, 0x38 }, { 80, 0x39 }, { 88, 0x3A }, { 96, 0x3B }, { 104, 0x3C }, { 112, 0x3D }, { 120, 0x3E }, { 128, 0x3F }
};


/*
   PLL CLK Range
   90 ,92 ,93 ,94 ,96 ,99 ,102 ,105 ,108 ,111 ,114 ,117 ,120 ,123 ,126 ,129 ,
   132 ,135 ,138 ,141 ,144 ,150 ,156 ,162 ,168 ,174 ,180
 */
static uint8_t cfg_pll_clk(uint32_t src_clk, uint32_t goal_clk, uint8_t *o_pre, uint8_t *o_fb)
{
    uint8_t pre = 0, fb = 0, div = 0;

    for (pre = 0; pre < 8; pre++) {
        div = goal_clk * 2 * (1 + pre) / src_clk;
        if (div >= 16 && div < 48) {
            if (goal_clk * 2 * (1 + pre) == (src_clk * div)) {
                fb = div - 16;
                //PLL clk=fosc*(16+fb)/(1+pre)/2; pre:0~7 fb: 0~31
                *o_pre  = pre;
                *o_fb   = fb;
                return (SUCCESS);
            }
        }
    }
    return (ERROR);
}


ErrorStatus LL_CMU_SysClkInit(LL_CMU_InitTypeDef *CMU_InitStruct)
{
    uint32_t        osc_en = 0, pll_en = 0;
    uint32_t        src_hz = IRC16M_VALUE, pll_hz = 0, main_hz;
    uint8_t         pre = 0, fb = 0;
    uint8_t         flash_latency = 0;
    uint32_t        real_clk = 0, div_v;
    __IO uint32_t   i;

    if (CMU_InitStruct->OscType & LL_CMU_OSCTYPE_XTAL) { //osc
        LL_CMU_IDLE_EnableOSC();
        while (!LL_CMU_IsReady_OSC())
            ;
        osc_en      = 1;
        OscClock    = CMU_InitStruct->XtalHz;
    }
    LL_CMU_SetIRC16MReadyCnt(200);
    if (LL_CMU_PLL_ON == CMU_InitStruct->PLL.PLLState) { //pll
        pll_hz = CMU_InitStruct->PLL.PLLFreqHz;
        if ((LL_CMU_PLLCLK_XTAL == CMU_InitStruct->PLL.PLLSource) && osc_en) {
            src_hz = CMU_InitStruct->XtalHz;
            LL_CMU_SetPLLClkSrc(LL_CMU_PLLCLK_XTAL);
        } else if (LL_CMU_PLLCLK_IRC16M == CMU_InitStruct->PLL.PLLSource)
            LL_CMU_SetPLLClkSrc(LL_CMU_PLLCLK_IRC16M);
        else
            return (ERROR);
        if (pll_hz >= src_hz) {
            LL_CMU_SetPLLReadyCnt(6900);  //waiting 1.8ms
            if (SUCCESS == cfg_pll_clk(src_hz, pll_hz, &pre, &fb)) {
                LL_CMU_ConfigPLL(pre, fb);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLLOCKCFG, 1 << CMU_CFGCLKS1_PLLLOCKCFG_Pos);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLTRIM, 1 << CMU_CFGCLKS1_PLLTRIM_Pos);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLCPICTRL, 0x7 << CMU_CFGCLKS1_PLLCPICTRL_Pos);

                LL_CMU_IDLE_EnablePLL();
                LL_CMU_EnablePLL();
                pll_en = 1;
            }
        }
    }
    switch (CMU_InitStruct->MainClkSrc) {
    case LL_CMU_MCGMAINCLK_IRC4M:
        main_hz = IRC4M_VALUE;
        break;
    case LL_CMU_MCGMAINCLK_IRC16M:
        main_hz = IRC16M_VALUE;
        break;
    case LL_CMU_MCGMAINCLK_PLLCLK:
        if (!pll_en)
            return (ERROR);
        main_hz = pll_hz;
        break;
    case LL_CMU_MCGMAINCLK_OSCCLK:
        if (!osc_en)
            return (ERROR);
        main_hz = src_hz;
        break;
    default:
        return (ERROR);
    }
    if (CMU_InitStruct->MainClkDiv > LL_CMU_MAINCLK_DIV128)
        return (ERROR);

    SystemCoreClock = main_hz / SysClkCfg[CMU_InitStruct->MainClkDiv].Div;
    div_v           = SysClkCfg[CMU_InitStruct->MainClkDiv].Cfg;
    LL_CMU_SetBusClkPrescaler(CMU_InitStruct->BusClkDiv);

    if (main_hz > 27000000) {
        //flash_latency config
        real_clk        = main_hz / 1000000;
        flash_latency   = (real_clk + 26) / 27;
        MODIFY_REG(FLASH->RDT, FLASH_RDT_TACCN, flash_latency << FLASH_RDT_TACCN_Pos);

        if (real_clk >= 100)
            MODIFY_REG(FLASH->RDT, FLASH_RDT_TCKH, 1 << FLASH_RDT_TCKH_Pos);
        else
            MODIFY_REG(FLASH->RDT, FLASH_RDT_TCKH, 0 << FLASH_RDT_TCKH_Pos);
    }

    if (pll_en) {
        if (SystemCoreClock > 48000000) {                       //sysclk>48MHz open iload
            LL_PMU_EnableiLoad();
            LL_CMU_SetSysClkPrescaler(LL_CMU_REUSEDIV3_DIV2);   //must set
        }
        //waiting pll ready
        while (!LL_CMU_IsLock_PLL())
            ;
        while (!LL_CMU_IsReady_PLL())
            ;
    }
    LL_CMU_SetMCGMainClkPrescaler((div_v & 0x70) >> 4);
    LL_CMU_SetMCGMainClkSrc(CMU_InitStruct->MainClkSrc);
    /* DATA and CODE CACHE_ENABLE */
    LL_Cache_Enable();
    if (SystemCoreClock > 96000000) {   //sysclk>96MHz add delay
        for (i = 0; i < 10; i++)
            ;                           //software delay
    }
    LL_CMU_SetSysClkPrescaler(div_v & 0x0F);
    return (SUCCESS);
}


/*
   Set PLL
   @param
   CMU_InitStruct->OscType
   OscClock=CMU_InitStruct->XtalHz;
   CMU_InitStruct->PLL.PLLState
   CMU_InitStruct->PLL.PLLSource
   CMU_InitStruct->PLL.PLLFreqHz
   @return
   SUCCESS or ERROR
 */
ErrorStatus LL_CMU_StartPLL(LL_CMU_InitTypeDef *CMU_InitStruct)
{
    uint32_t    osc_en = 0;
    uint32_t    src_hz = IRC16M_VALUE, pll_hz;
    uint8_t     pre = 0, fb = 0;

    if (CMU_InitStruct->OscType & LL_CMU_OSCTYPE_XTAL) { //osc
        LL_CMU_IDLE_EnableOSC();
        while (!LL_CMU_IsReady_OSC())
            ;
        osc_en      = 1;
        OscClock    = CMU_InitStruct->XtalHz;
    }
    LL_CMU_SetIRC16MReadyCnt(200);
    if (LL_CMU_PLL_ON == CMU_InitStruct->PLL.PLLState) { //pll
        pll_hz = CMU_InitStruct->PLL.PLLFreqHz;
        if ((LL_CMU_PLLCLK_XTAL == CMU_InitStruct->PLL.PLLSource) && osc_en) {
            src_hz = CMU_InitStruct->XtalHz;
            LL_CMU_SetPLLClkSrc(LL_CMU_PLLCLK_XTAL);
        } else if (LL_CMU_PLLCLK_IRC16M == CMU_InitStruct->PLL.PLLSource)
            LL_CMU_SetPLLClkSrc(LL_CMU_PLLCLK_IRC16M);
        else
            return (ERROR);
        if (pll_hz > src_hz) {
            LL_CMU_SetPLLReadyCnt(6900);  //waiting 1.8ms
            if (SUCCESS == cfg_pll_clk(src_hz, pll_hz, &pre, &fb)) {
                LL_CMU_ConfigPLL(pre, fb);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLLOCKCFG, 1 << CMU_CFGCLKS1_PLLLOCKCFG_Pos);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLTRIM, 1 << CMU_CFGCLKS1_PLLTRIM_Pos);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLCPICTRL, 0x7 << CMU_CFGCLKS1_PLLCPICTRL_Pos);

                LL_CMU_IDLE_EnablePLL();
                LL_CMU_EnablePLL();
                return (SUCCESS);
            }
        }
    }
    return (ERROR);
}


/*
   MainClk to PLL
   @param
   CMU_InitStruct->PLL.PLLFreqHz：仅作为计算参数，不会重新设置PLL输出频率
   CMU_InitStruct->MainClkDiv ：系统主总线时钟分频比，会重新设置
   CMU_InitStruct->BusClkDiv  ：外设总线时钟分频比，会重新设置
   @return
   SUCCESS or ERROR
 */
ErrorStatus LL_CMU_SysClkToPLL(uint32_t pll_freq_hz, uint32_t main_clk_div, uint32_t bus_clk_div)
{
    uint32_t    flash_latency;
    uint32_t    div_v, real_clk;
    uint32_t    i = 0;

    //waiting pll ready
    while (!LL_CMU_IsLock_PLL()) {
        if (++i > 1000 * 100)
            return (ERROR);
    }
    i = 0;
    while (!LL_CMU_IsReady_PLL()) {
        if (++i > 1000 * 100)
            return (ERROR);
    }
    SystemCoreClock = pll_freq_hz / SysClkCfg[main_clk_div].Div;
    div_v           = SysClkCfg[main_clk_div].Cfg;
    LL_CMU_SetBusClkPrescaler(bus_clk_div);
    /* DATA and CODE CACHE_ENABLE */
    LL_Cache_Enable();
    {
        //flash_latency config
        real_clk        = pll_freq_hz / 1000000;
        flash_latency   = (real_clk + 26) / 27;
        MODIFY_REG(FLASH->RDT, FLASH_RDT_TACCN, flash_latency << FLASH_RDT_TACCN_Pos);

        if (real_clk >= 100)
            MODIFY_REG(FLASH->RDT, FLASH_RDT_TCKH, 1 << FLASH_RDT_TCKH_Pos);
        else
            MODIFY_REG(FLASH->RDT, FLASH_RDT_TCKH, 0 << FLASH_RDT_TCKH_Pos);
    }
    i = SystemCoreClock / (8 * 1000000);
    if (SystemCoreClock > 48 * 1000000) {                   //sysclk>48MHz open iload
        LL_PMU_EnableiLoad();
        LL_CMU_SetSysClkPrescaler(LL_CMU_REUSEDIV3_DIV2);   //must set
    }
    LL_CMU_SetMCGMainClkPrescaler((div_v & 0x70) >> 4);
    LL_CMU_SetMCGMainClkSrc(LL_CMU_MCGMAINCLK_PLLCLK);
    //software delay
    while (i--)
        ;
    LL_CMU_SetSysClkPrescaler(div_v & 0x0F);
    return (SUCCESS);
}


/*
   时钟恢复为PLL clk
 */
ErrorStatus LL_CMU_SysClkRestoreToPLL(uint32_t sysclkfreq)
{
    uint32_t    div_v;
    uint32_t    i = 0;
    if (!sysclkfreq)
        return (ERROR);
    //waiting pll ready
    while (!LL_CMU_IsLock_PLL()) {
        if (++i > 1000 * 100)
            return (ERROR);
    }
    i = 0;
    while (!LL_CMU_IsReady_PLL()) {
        if (++i > 1000 * 100)
            return (ERROR);
    }
    div_v   = LL_CMU_GET_SYSCLKDIV();
    i       = sysclkfreq / (8 * 1000000);
    if (sysclkfreq > 48 * 1000000) {                        //sysclk>48MHz open iload
        LL_PMU_EnableiLoad();
        LL_CMU_SetSysClkPrescaler(LL_CMU_REUSEDIV3_DIV2);   //must set
    }
    LL_CMU_SetMCGMainClkSrc(LL_CMU_MCGMAINCLK_PLLCLK);
    //software dely
    while (i--)
        ;
    LL_CMU_SetSysClkPrescaler(div_v - 1);                   //切到目标频率
    return (SUCCESS);
}


/*----------------------------------------------------------------------------
   Clock functions
 *----------------------------------------------------------------------------*/
uint32_t LL_CMU_SystemCoreClockUpdate(void)                 /* Get Core Clock Frequency      */
{
    uint32_t    pllfb = 0U, pllp = 0U, div = 0U;
    uint32_t    sysclockfreq = 0U;

    /* Get SYSCLK source -------------------------------------------------------*/
    switch (LL_CMU_GetMCGMainClkSrc()) {
    case LL_CMU_MCGMAINCLK_IRC4M: { /* IRC4M used as system clock source */
        sysclockfreq = IRC4M_VALUE;
        break;
    }
    case LL_CMU_MCGMAINCLK_IRC16M: { /* IRC16M used as system clock  source */
        sysclockfreq = IRC16M_VALUE;
        break;
    }
    case LL_CMU_MCGMAINCLK_OSCCLK: { /* OSC used as system clock  source */
        sysclockfreq = OscClock;
        break;
    }
    case LL_CMU_MCGMAINCLK_PLLCLK: { /* PLL used as system clock  source */
        /* SYSCLK = (HSE_VALUE or IRC16M_VALUE) * (PLLFB + 16) / (2 * (PLLN+1) ) */

        pllfb   = LL_CMU_GetPLLFB();
        pllp    = LL_CMU_GetPLLPRE();

        if (LL_CMU_GetPLLClkSrc() != LL_CMU_PLLCLK_IRC16M) {
            /* HSE used as PLL clock source */
            sysclockfreq = (uint32_t)(OscClock * (pllfb + 16) / (2 * (pllp + 1)));
        } else {
            /* IRC16M used as PLL clock source */
            sysclockfreq = (uint32_t)((IRC16M_VALUE * (pllfb + 16)) / (2 * (pllp + 1)));
        }
        break;
    }
    default: {
        sysclockfreq = SystemCoreClock;
        break;
    }
    }
    div             = LL_CMU_GET_SYSCLKDIV();
    sysclockfreq    /= div;
    SystemCoreClock = sysclockfreq;
    return (sysclockfreq);
}


/* Get SystemCoreClock Frequency   */
uint32_t LL_CMU_GetSysClock(void)
{
    LL_CMU_SystemCoreClockUpdate();
    return (SystemCoreClock);
}


/* Get the BUS CLK frequency  */
uint32_t LL_CMU_GetBusClock(void)
{
    return (LL_CMU_GetSysClock() / (LL_CMU_GetBusClkPrescaler() + 1));
}


/* Get OSC Clock Frequency   */
uint32_t LL_CMU_GetOscClock(void)
{
    return (OscClock);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
