/**
 ******************************************************************************
 * @file    fm15f3xx_hal_rtc.c
 * @author  CLF
 * @brief   RTC HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Real Time Clock (RTC) peripheral:
 *           + Initialization and de-initialization functions
 *           + RTC Time functions
 *           + Peripheral Control functions
 *           + Peripheral State and Errors functions
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup RTC
 * @brief RTC HAL module driver
 * @{
 */

#ifdef RTC_MODULE_ENABLED


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @addtogroup RTC_Private_Constants
 * @{
 */

const uint8_t   month_table[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
char            *week_table[7] = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };


/**
 * @}
 */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup RTC_Private_Functions   RTC Private Functions
 * @{
 */
static uint8_t __IsLeapYear(uint32_t year);


static uint32_t __RTC_TimetoTimestamp(RTC_TimeTypeDef *sTime);


/**
 * @}
 */

/* Exported functions ---------------------------------------------------------*/


/** @defgroup RTC_Exported_Functions RTC Exported Functions
 * @{
 */


/** @defgroup  Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and Configuration functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the RTC peripheral
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_Init(RTC_HandleTypeDef *hrtc)
{
    /* Check the RTC peripheral state */
    if (hrtc == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_RTC_PSC(hrtc->TimePrescaler));

    hrtc->State = HAL_RTC_STATE_BUSY;

    /* Set the RTC Time Prescaler parameters */
    hrtc->Instance->PSC = (uint32_t)(hrtc->TimePrescaler);

    hrtc->State = HAL_RTC_STATE_READY;
    return (HAL_OK);
}


/**
 * @brief  DeInitializes the RTC peripheral
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_DeInit(RTC_HandleTypeDef *hrtc)
{
    /* Check the RTC peripheral state */
    if (hrtc == NULL) {
        return (HAL_ERROR);
    }

    /* Set RTC state */
    hrtc->State = HAL_RTC_STATE_BUSY;

    /* Reset registers */
    hrtc->Instance->CTRL            = 0x00000000U;
    hrtc->Instance->CLKCFG          = 0x00000000U;
    hrtc->Instance->CNTEN           = 0x00000000U;
    hrtc->Instance->SECONDS         = 0x00000000U;
    hrtc->Instance->CYCLE           = 0x00000000U;
    hrtc->Instance->PSC             = 0x00000000U;
    hrtc->Instance->SECALARM        = 0x00000000U;
    hrtc->Instance->CYCALARM        = 0x00000000U;
    hrtc->Instance->COMPENSATION    = 0x00000000U;
    hrtc->Instance->TIMEIE          = 0x00000000U;
    hrtc->Instance->RECORDCLR       = 0x00000000U;
    hrtc->Instance->WAKUPCFG        = 0x00000000U;
    hrtc->Instance->WAKUPEN         = 0x00000000U;

    /* De-Initialize RTC MSP */
    HAL_RTC_MspDeInit(hrtc);

    /* Set hrtc State */
    hrtc->State = HAL_RTC_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief  Initializes the RTC MSP.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval None
 */
__weak void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hrtc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes the RTC MSP.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval None
 */
__weak void HAL_RTC_MspDeInit(RTC_HandleTypeDef* hrtc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hrtc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_MspDeInit could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup RTC_Exported_Functions_Group2 RTC Time and compensation functions
 *  @brief   RTC Time and compensation functions
 *
   @verbatim
   ===============================================================================
 #####    RTC Time and Time compensation functions   #####
   ===============================================================================

   [..] This section provides functions allowing to configure Time features

   @endverbatim
 * @{
 */


/**
 * @brief  Set RTC current time.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  sTime Pointer to Time structure.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_SetTime(RTC_HandleTypeDef *hrtc, RTC_TimeTypeDef *sTime)
{
    uint32_t timestamp = 0U;

    hrtc->State = HAL_RTC_STATE_BUSY;

    assert_param(IS_RTC_HOUR(sTime->Hours));
    assert_param(IS_RTC_MINUTES(sTime->Minutes));
    assert_param(IS_RTC_SECONDS(sTime->Seconds));
    assert_param(IS_RTC_YEAR(sTime->Year));
    assert_param(IS_RTC_MONTH(sTime->Month));
    assert_param(IS_RTC_DATE(sTime->Date));

    timestamp = __RTC_TimetoTimestamp(sTime);
    LL_RTC_DisableAllTimer();
    LL_RTC_SetSecondTimerValue(timestamp);
    RTC_TIME->CNTEN |= 0x5;

    hrtc->State = HAL_RTC_STATE_READY;
    return (HAL_OK);
}


/**
 * @brief  Set RTC time Conpensation.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  Compen Pointer to Compensation structure
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_SetTimeCompensation(RTC_HandleTypeDef *hrtc, RTC_CompensationDef *Compen)
{
    hrtc->State = HAL_RTC_STATE_BUSY;

    RTC_TIME->COMPENSATION  |= (Compen->CompensationInterVal << 8);
    RTC_TIME->COMPENSATION  |= (Compen->CompensationDirection << 7);
    RTC_TIME->COMPENSATION  |= (Compen->CompensationValue);

    hrtc->State = HAL_RTC_STATE_READY;
    return (HAL_OK);
}


/**
 * @brief  Get RTC current time.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  sTime Pointer to Time structure
 * @retval HAL status
 */
RTC_TimeTypeDef HAL_RTC_GetTime(RTC_HandleTypeDef *hrtc)
{
    RTC_TimeTypeDef Time;
    uint32_t        seccount    = 0;
    uint32_t        daycount    = 0;
    uint32_t        weekcount   = 0;
    uint32_t        count       = 0;
    seccount    = LL_RTC_GetSecondTimerValue();
    daycount    = seccount / 86400;
    weekcount   = daycount;
    if (daycount != 0) {
        count = 1970;
        while (daycount >= 365) {
            if (__IsLeapYear(count)) {
                if (daycount >= 366)
                    daycount -= 366;
                else {
                    count++;
                    break;
                }
            } else
                daycount -= 365;
            count++;
        }
        Time.Year   = count;
        count       = 0;
        while (daycount >= 28) {
            if (__IsLeapYear(Time.Year) && count == 1) {
                if (daycount >= 29)
                    daycount -= 29;
                else
                    break;
            } else {
                if (daycount >= month_table[count])
                    daycount -= month_table[count];
                else
                    break;
            }
            count++;
        }
        Time.Month  = count + 1;
        Time.Date   = daycount + 1;
        while (1) {
            weekcount       = (weekcount + 5) % 7;
            Time.Weekday    = week_table[weekcount];
            break;
        }
    }
    seccount        %= 86400;
    Time.Hours      = seccount / 3600;
    Time.Minutes    = (seccount % 3600) / 60;
    Time.Seconds    = (seccount % 3600) % 60;
    return (Time);
}


/**
 * @}
 */


/** @defgroup RTC_Exported_Functions_Group3 RTC Alarm functions
 *  @brief   RTC Alarm functions
 *
   @verbatim
   ===============================================================================
 #####   RTC Alarm functions   #####
   ===============================================================================

   [..] This section provides functions allowing to configure Alarm features

   @endverbatim
 * @{
 */


/**
 * @brief  Sets the specified RTC Alarm.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  sAlarm Pointer to Alarm structure
 * @note   AlarmType contains RTC_ALARM_SECONDS and RTC_ALARM_CYCLE.AlarmType
           equals RTC_ALARM_CYCLE, Timing duration dose not exceed 65535 second.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_SetAlarmTime(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm)
{
    /* Check the parameters */
    assert_param(IS_RTC_ALARM(sAlarm->AlarmType));
    assert_param(IS_RTC_HOUR(sAlarm->AlarmTime.Hours));
    assert_param(IS_RTC_MINUTES(sAlarm->AlarmTime.Minutes));
    assert_param(IS_RTC_SECONDS(sAlarm->AlarmTime.Seconds));
    assert_param(IS_RTC_YEAR(sAlarm->AlarmTime.Year));
    assert_param(IS_RTC_MONTH(sAlarm->AlarmTime.Month));
    assert_param(IS_RTC_DATE(sAlarm->AlarmTime.Date));

    hrtc->State = HAL_RTC_STATE_BUSY;

    if (sAlarm->AlarmType == RTC_ALARM_SECONDS) {
        uint32_t        timestamp = 0u;
        RTC_TimeTypeDef time;

        time.Hours      = sAlarm->AlarmTime.Hours;
        time.Minutes    = sAlarm->AlarmTime.Minutes;
        time.Seconds    = sAlarm->AlarmTime.Seconds;
        time.Year       = sAlarm->AlarmTime.Year;
        time.Month      = sAlarm->AlarmTime.Month;
        time.Date       = sAlarm->AlarmTime.Date;

        timestamp = __RTC_TimetoTimestamp(&time);
        LL_RTC_SetSecondTimerAlarm(timestamp);
        RTC_TIME->CNTEN |= 0x5;
    } else {
        uint32_t seccount = 0;

        seccount    += (uint32_t)(sAlarm->AlarmTime.Hours) * 3600;
        seccount    += (uint32_t)(sAlarm->AlarmTime.Minutes) * 60;
        seccount    += (uint32_t)(sAlarm->AlarmTime.Seconds);

        LL_RTC_DisableCycleTimer();
        LL_RTC_SetCycleTimerValue(0);
        LL_RTC_SetCycleTimerAlarm(seccount);

        RTC_TIME->CNTEN |= 0x3;
    }
    /* Change RTC State */
    hrtc->State = HAL_RTC_STATE_READY;
    return (HAL_OK);
}


/**
 * @brief  Get the specified RTC Alarm
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  sAlarm Pointer to Alarm structure
 * @note   AlarmType contains RTC_ALARM_SECONDS and RTC_ALARM_CYCLE.AlarmType
           equals RTC_ALARM_CYCLE, Timing duration dose not exceed 65535 second.
 * @retval HAL status
 */
RTC_AlarmTypeDef HAL_RTC_GetAlarmTime(RTC_HandleTypeDef *hrtc, uint32_t Alarm)
{
    /* Check the parameters */
    assert_param(IS_RTC_ALARM(Alarm));
    RTC_AlarmTypeDef sAlarm;
    if (Alarm == RTC_ALARM_SECONDS) {
        sAlarm.AlarmType = RTC_ALARM_SECONDS;
        uint32_t    seccount    = 0;
        uint32_t    daycount    = 0;
        uint32_t    count       = 0;
        seccount    = RTC_TIME->SECALARM;
        daycount    = seccount / 86400;
        if (daycount != 0) {
            count = 1970;
            while (daycount >= 365) {
                if (__IsLeapYear(count)) {
                    if (daycount >= 366)
                        daycount -= 366;
                    else {
                        count++;
                        break;
                    }
                } else
                    daycount -= 365;
                count++;
            }
            sAlarm.AlarmTime.Year   = count;
            count                   = 0;
            while (daycount >= 28) {
                if (__IsLeapYear(sAlarm.AlarmTime.Year) && count == 1) {
                    if (daycount >= 29)
                        daycount -= 29;
                    else
                        break;
                } else {
                    if (daycount >= month_table[count])
                        daycount -= month_table[count];
                    else
                        break;
                }
                count++;
            }
            sAlarm.AlarmTime.Month  = count + 1;
            sAlarm.AlarmTime.Date   = daycount + 1;
        }
        seccount                    %= 86400;
        sAlarm.AlarmTime.Hours      = seccount / 3600;
        sAlarm.AlarmTime.Minutes    = (seccount % 3600) / 60;
        sAlarm.AlarmTime.Seconds    = (seccount % 3600) % 60;
    } else {
        sAlarm.AlarmType = RTC_ALARM_CYCLE;
        uint32_t seccount = 0;
        seccount = LL_RTC_GetCycleTimerAlarm();

        sAlarm.AlarmTime.Year       = 0;
        sAlarm.AlarmTime.Month      = 0;
        sAlarm.AlarmTime.Date       = 0;
        seccount                    %= 86400;
        sAlarm.AlarmTime.Hours      = seccount / 3600;
        sAlarm.AlarmTime.Minutes    = (seccount % 3600) / 60;
        sAlarm.AlarmTime.Seconds    = (seccount % 3600) % 60;
    }
    hrtc->State = HAL_RTC_STATE_READY;
    return (sAlarm);
}


/**
 * @brief  Deactivate the specified RTC Alarm
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  Alarm Specifies the Alarm.
 *          This parameter can be one of the following values:
 *            @arg RTC_ALARM_SECONDS OR RTC_ALARM_CYCLE
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_DeactivateAlarm(RTC_HandleTypeDef *hrtc, uint32_t Alarm)
{
    /* Check the parameters */
    assert_param(IS_RTC_ALARM(Alarm));

    hrtc->State = HAL_RTC_STATE_BUSY;

    if (Alarm == RTC_ALARM_CYCLE)
        LL_RTC_DisableCycleTimer();

    hrtc->State = HAL_RTC_STATE_READY;

    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup RTC_Exported_Functions_Group4 RTC IRQ functions
 *  @brief   RTC IRQ functions
 *
   @verbatim
   ===============================================================================
 ##### RTC IRQ functions  #####
   ===============================================================================
   [..] This section provides functions allowing to configure IRQ feature

   @endverbatim
 * @{
 */


/**
 * @brief  Sets the specified RTC Alarm Int.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  sAlarm Pointer to Alarm structure
 *              This parameter can be one of the following values:
 *            @arg RTC_ALARM_SECONDS OR RTC_ALARM_CYCLE
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_SetAlarmTime_Int(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm)
{
    /* Check the parameters */
    assert_param(IS_RTC_ALARM(sAlarm->AlarmType));
    assert_param(IS_RTC_HOUR(sAlarm->AlarmTime.Hours));
    assert_param(IS_RTC_MINUTES(sAlarm->AlarmTime.Minutes));
    assert_param(IS_RTC_SECONDS(sAlarm->AlarmTime.Seconds));
    assert_param(IS_RTC_YEAR(sAlarm->AlarmTime.Year));
    assert_param(IS_RTC_MONTH(sAlarm->AlarmTime.Month));
    assert_param(IS_RTC_DATE(sAlarm->AlarmTime.Date));

    hrtc->State = HAL_RTC_STATE_BUSY;

    if (sAlarm->AlarmType == RTC_ALARM_SECONDS) {
        uint32_t        timestamp = 0u;
        RTC_TimeTypeDef time;

        time.Hours      = sAlarm->AlarmTime.Hours;
        time.Minutes    = sAlarm->AlarmTime.Minutes;
        time.Seconds    = sAlarm->AlarmTime.Seconds;
        time.Year       = sAlarm->AlarmTime.Year;
        time.Month      = sAlarm->AlarmTime.Month;
        time.Date       = sAlarm->AlarmTime.Date;

        timestamp = __RTC_TimetoTimestamp(&time);
        LL_RTC_SetSecondTimerAlarm(timestamp);
        RTC_TIME->CNTEN |= 0x5;

        /* Clear Record Alarm */
        LL_RTC_ClearRecord_Alarm();
        /* Enable SecondTimerAlarm */
        /* Interrupt cam be turned on or off here */
        RTC_TIME->TIMEIE |= HAL_RTC_ENABLE_SECONDIE;
    } else {
        uint32_t seccount = 0;

        seccount    += (uint32_t)(sAlarm->AlarmTime.Hours) * 3600;
        seccount    += (uint32_t)(sAlarm->AlarmTime.Minutes) * 60;
        seccount    += (uint32_t)(sAlarm->AlarmTime.Seconds);

        LL_RTC_DisableCycleTimer();
        LL_RTC_SetCycleTimerValue(0);
        LL_RTC_SetCycleTimerAlarm(seccount);
        LL_RTC_EnableCycleTimer();

        /* Clear Record Alarm */
        LL_RTC_ClearRecord_Cycle();
        /* Enable CycleTimerAlarmINT */
        /* Interrupt cam be turned on or off here */
        RTC_TIME->TIMEIE |= HAL_RTC_ENABLE_CYCLEIE;
    }
    /* Change RTC State */
    hrtc->State = HAL_RTC_STATE_READY;
    return (HAL_OK);
}


/**
 * @brief  This function handles Alarm interrupt request.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval None
 */
void HAL_RTC_AlarmIRQHandler(RTC_HandleTypeDef *hrtc)
{
    hrtc->State = HAL_RTC_STATE_BUSY;

    if (RTC_TIME->TIMEIE & HAL_RTC_ENABLE_SECONDIE) {
        if (RTC_TIME->STATUS & RTC_UTC_ALARM_RECORD) {
            HAL_RTC_AlarmEventCallback(hrtc);
            LL_RTC_ClearRecord_Alarm();
        }
    }

    if (RTC_TIME->TIMEIE & HAL_RTC_ENABLE_CYCLEIE) {
        if (RTC_TIME->STATUS & RTC_UTC_CYCLE_RECORD) {
            HAL_RTC_AlarmEventCallback(hrtc);
            LL_RTC_ClearRecord_Cycle();
        }
    }

    hrtc->State = HAL_RTC_STATE_READY;
}


/**
 * @brief  Sets the specified RTC 1S Int.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @param  RTC_Int Specifies RTC_1S_INT
 * @note   RTC_Int contains HAL_RTC_ENABLE_1SIE and HAL_RTC_ENABLE_1SFLAGIE
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_RTC_1S_Int(RTC_HandleTypeDef *hrtc, uint32_t RTC_Int)
{
    assert_param(IS_RTC_INT(RTC_Int));

    hrtc->State = HAL_RTC_STATE_BUSY;

    RTC_TIME->TIMEIE |= RTC_Int;

    /* Change RTC State */
    hrtc->State = HAL_RTC_STATE_READY;
    return (HAL_OK);
}


/**
 * @brief  This function handles 1S interrupt request.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval None
 */
void HAL_RTC_1S_IRQHandler(RTC_HandleTypeDef *hrtc)
{
    hrtc->State = HAL_RTC_STATE_BUSY;

    if (RTC_TIME->TIMEIE & HAL_RTC_ENABLE_1SIE) {
        if (RTC_TIME->STATUS & RTC_UTC_1S_RECORD) {
            HAL_RTC_1S_EventCallback(hrtc);
            LL_RTC_ClearRecord_1s();
        }
    }

    if (RTC_TIME->TIMEIE & HAL_RTC_ENABLE_1SFLAGIE) {
        if (RTC_TIME->STATUS & RTC_UTC_1S_FLAG) {
            HAL_RTC_1S_EventCallback(hrtc);
        }
    }

    hrtc->State = HAL_RTC_STATE_READY;
}


/**
 * @brief  Seconds Alarm callback.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval None
 */
__weak void HAL_RTC_AlarmEventCallback(RTC_HandleTypeDef *hrtc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hrtc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_AlarmAEventCallback could be implemented in the user file
     */
}


/**
 * @brief  1S Int callback.
 * @param  hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                the configuration information for RTC.
 * @retval None
 */
__weak void HAL_RTC_1S_EventCallback(RTC_HandleTypeDef *hrtc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hrtc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_AlarmAEventCallback could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup RTC_Exported_Functions_Group6 Peripheral Control functions
 *  @brief   Peripheral Control functions
 *
   @verbatim
   ===============================================================================
 ##### Peripheral Control functions  #####
   ===============================================================================

   [..] This section provides functions: Peripheral Control functions

   @endverbatim
 * @{
 */


/**
 * @brief   Return the RTC state.
 * @param   hrtc pointer to a RTC_HandleTypeDef structure that contains
 *                           the configuration information for RTC.
 * @retval HAL State
 */
HAL_RTCStateTypeDef HAL_RTC_GetState(RTC_HandleTypeDef *hrtc)
{
    return (hrtc->State);
}


/**
 * @}
 */


/** @defgroup RTC_Exported_Functions_Group5 RTC Time transform functions
 *  @brief   RTC Time transform functions
 *
   @verbatim
   ===============================================================================
 ##### RTC Time transform functions  #####
   ===============================================================================

   [..] This section provides functions RTC Time transform

   @endverbatim
 * @{
 */


/**
 * @brief  Judge whether the year is a leap year
 * @param  year specifies the RTC time structure's Year
 * @retval 1:yes 0:not
 */
static uint8_t __IsLeapYear(uint32_t year)
{
    return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
}


/**
 * @brief  time to timestamp
 * @param  sTime specifies the RTC time
 * @retval timestamp
 */
static uint32_t __RTC_TimetoTimestamp(RTC_TimeTypeDef *sTime)
{
    uint8_t     t;
    uint32_t    seccount = 0;
    if ((sTime->Year - 1970) > 136)
        return (0);
    for (t = 0; t < (sTime->Year - 1970); t++) {
        if (__IsLeapYear(t))
            seccount += 31622400;
        else
            seccount += 31536000;
    }
    sTime->Month -= 1;
    for (t = 0; t < sTime->Month; t++) {
        seccount += (uint32_t) month_table[t] * 86400;
        if (__IsLeapYear(sTime->Year) && t == 1)
            seccount += 86400;
    }
    seccount    += (uint32_t)(sTime->Date - 1) * 86400;
    seccount    += (uint32_t)(sTime->Hours) * 3600;
    seccount    += (uint32_t)(sTime->Minutes) * 60;
    seccount    += (uint32_t)(sTime->Seconds);
    return (seccount);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* HAL_RTC_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

