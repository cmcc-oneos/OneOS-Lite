/**
 ******************************************************************************
 * @file    fm15f3xx_ll_bkp.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#include "fm15f3xx_ll_bkp.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup BKP_LL_Exported_Functions
 * @{
 */


/**
 * @brief  Initializes Tamper
 * @note   This function is used to configure the global Tamper features of the BKP.
 * @retval None
 */
void LL_BKP_Tamper_Init(LL_TamperPin_InitTypeDef *TamperPinInitStruct)
{
    LL_BKP_SoftResetTamper();

    if (TamperPinInitStruct->PinHysterSel)
        SET_BIT(BKP->PCFG, BKP_PIN_HYSTERESIS_SEL);
    if (TamperPinInitStruct->PinPassfilterEn)
        SET_BIT(BKP->PCFG, BKP_PIN_PASSIVE_FILTEN);
    if (TamperPinInitStruct->PinDriveStrength)
        SET_BIT(BKP->PCFG, BKP_PIN_DRI_STRENGTHEN);
    if (TamperPinInitStruct->PinSlewRate)
        SET_BIT(BKP->PCFG, BKP_PIN_SLEW_RATE);
    if (TamperPinInitStruct->Pin2SignalOut) {
        SET_BIT(BKP->PCFG, BKP_DIG_SIGNAL_OUT_EN);
        MODIFY_REG(BKP->PCFG, BKP_DIG_SIGNAL_SEL_Msk, TamperPinInitStruct->Pin2SignalSel);
    }

    uint32_t pin = TamperPinInitStruct->Pin;
    for (int i = 0; i < 8; i++) {
        if (pin & 0x01) {
            if (TamperPinInitStruct->FilterClockSource)
                SET_BIT(BKP->PxGF[i], BKP_PIN_GLITCH_FILTER_PSC);
            if (TamperPinInitStruct->FilterEn) {
                SET_BIT(BKP->PxGF[i], BKP_PIN_GLITCH_FILTER_EN);
                MODIFY_REG(BKP->PxGF[i], BKP_PIN_GLITCH_FILTER_WIDTH_Msk, TamperPinInitStruct->FilterWidth);
                MODIFY_REG(BKP->PxGF[i], BKP_PIN_SAMPLE_FREQ_Msk, TamperPinInitStruct->PinSampleFreq);
            }
            if (TamperPinInitStruct->PinPullEn) {
                SET_BIT(BKP->PxGF[i], BKP_PIN_PULL_ENABLE);
                MODIFY_REG(BKP->PxGF[i], BKP_PIN_PULL_SELECT_Msk, TamperPinInitStruct->PinPullSelect);
            }
            if (TamperPinInitStruct->PinExpectData)
                SET_BIT(BKP->PxGF[i], BKP_PIN_EXPECT_DATA);
        }
        pin >>= 1;
    }
    WRITE_REG(BKP->PLRTY, TamperPinInitStruct->PinPolarity);
    WRITE_REG(BKP->PDIR, TamperPinInitStruct->PinDir);
}


/**
 * @brief  DeInitializes Tamper
 * @note   This function is used to configure the global Tamper features of the BKP to default value.
 * @retval None
 */
void LL_BKP_Tamper_DeInit(void)
{
    LL_TamperPin_InitTypeDef TamperPinInitStruct;

    //all pin general configuration
    TamperPinInitStruct.PinHysterSel        = LL_BKP_PINHYSTER_LEVEL0;
    TamperPinInitStruct.PinPassfilterEn     = LL_BKP_PINPASSFILTER_DISABLE;
    TamperPinInitStruct.PinDriveStrength    = LL_BKP_PINDRISTRENGTH_LOW;
    TamperPinInitStruct.PinSlewRate         = LL_BKP_PINSLEW_SLOW;
    TamperPinInitStruct.Pin2SignalOut       = LL_BKP_PIN2SIGNALOUT_DISABLE;
    TamperPinInitStruct.Pin2SignalSel       = LL_BKP_PIN2SIGNALSEL_RFU;
    TamperPinInitStruct.Active0PinPoly      = LL_BKP_ACTIVE0REG_DISABLE;
    TamperPinInitStruct.Active1PinPoly      = LL_BKP_ACTIVE1REG_DISABLE;

    //each pin independent configuration
    TamperPinInitStruct.FilterClockSource   = LL_BKP_FILTERCLKSRC_32768Hz;
    TamperPinInitStruct.FilterEn            = LL_BKP_PINFILTER_DISABLE;
    TamperPinInitStruct.FilterWidth         = LL_BKP_FILTERWIDTH_0;
    TamperPinInitStruct.PinSampleFreq       = LL_BKP_PINSAMPLEFREQ_3;
    TamperPinInitStruct.PinSampleWidth      = LL_BKP_PINSAMPLEWIDTH_0;
    TamperPinInitStruct.PinPullEn           = LL_BKP_PINPULL_DISABLE;
    TamperPinInitStruct.PinPullSelect       = LL_BKP_PINPULLSEL_PULLDOWN;
    TamperPinInitStruct.PinExpectData       = LL_BKP_PINEXPECTDATA_LOW;

    TamperPinInitStruct.PinPolarity = LL_BKP_PIN0POLARITY_POSITIVE | LL_BKP_PIN1POLARITY_NEGATIVE;
    TamperPinInitStruct.PinDir      = LL_BKP_PIN0DIR_OUTPUT | LL_BKP_PIN1DIR_INPUT;

    TamperPinInitStruct.Pin = LL_BKP_ALLPIN;

    LL_BKP_Tamper_Init(&TamperPinInitStruct);
}


/**
 * @}
 */


/**
 * @}
 */
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
