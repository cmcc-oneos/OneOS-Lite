/**
 ******************************************************************************
 * @file    fm15f3xx_hal_i2c.c
 * @author  MCD Application Team
 * @brief   I2C HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Inter Integrated Circuit (I2C) peripheral:
 *           + Initialization and de-initialization functions
 *           + IO operation functions
 *           + Peripheral State, Mode and Error functions
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup I2C I2C
 * @brief I2C HAL module driver
 * @{
 */

#ifdef I2C_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup I2C_Private_Functions
 * @{
 */
/* Private functions */
static HAL_StatusTypeDef __I2C_WaitOnIntFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Flag, uint32_t Timeout, uint32_t Tickstart);


static void __I2C_ConfigSpeed(I2C_HandleTypeDef *hi2c, uint32_t Speed);


static void __I2C_Config(I2C_HandleTypeDef *hi2c);


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup I2C_Exported_Functions I2C Exported Functions
 * @{
 */


/** @defgroup I2C_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and de-initialization functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the I2C according to the specified parameters
 *         in the I2C_InitTypeDef and create the associated handle.
 * @param  hi2c pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for I2C module
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hi2c)
{
    /* Check the I2C handle allocation */
    if (hi2c == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_I2C_ALL_INSTANCE(hi2c->Instance));
    assert_param(IS_I2C_MODE(hi2c->Init.Mode));

    if (hi2c->Init.Mode == I2C_MODE_MASTER) {
        assert_param(IS_I2C_CLOCK_SPEED(hi2c->Init.ClockSpeed));
        assert_param(IS_I2C_ADDRESS(hi2c->Init.ExtAddress));
        assert_param(IS_I2C_ADDRESS_SIZE(hi2c->Init.ExtAddrSize));
    } else {
        assert_param(IS_I2C_ADDRESS(hi2c->Init.OwnAddress));
        assert_param(IS_I2C_ADDRESS_SIZE(hi2c->Init.OwnAddrSize));
        assert_param(IS_I2C_STRETCH(hi2c->Init.StretchEn));
    }

    if (hi2c->State == HAL_I2C_STATE_RESET) {
        /* Init the low level hardware : GPIO, CLOCK, NVIC */
        HAL_I2C_MspInit(hi2c);
    }

    hi2c->State = HAL_I2C_STATE_BUSY;

    /* Configure I2C */
    __I2C_Config(hi2c);

    hi2c->ErrorCode = HAL_I2C_ERROR_NONE;
    hi2c->State     = HAL_I2C_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  DeInitializes the I2C peripheral.
 * @param  hi2c pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for I2C module
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_DeInit(I2C_HandleTypeDef *hi2c)
{
    /* Check the I2C handle allocation */
    if (hi2c == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_I2C_ALL_INSTANCE(hi2c->Instance));

    hi2c->State = HAL_I2C_STATE_BUSY;

    /* Disable the I2C Peripheral Clock */
    __HAL_I2C_DISABLE(hi2c);

    /* DeInit the low level hardware: GPIO, CLOCK, NVIC */
    HAL_I2C_MspDeInit(hi2c);

    hi2c->ErrorCode = HAL_I2C_ERROR_NONE;
    hi2c->State     = HAL_I2C_STATE_RESET;
    return (HAL_OK);
}


/**
 * @brief I2C MSP Init.
 * @param  hi2c pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for I2C module
 * @retval None
 */
__weak void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hi2c);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_I2C_MspInit could be implemented in the user file
     */
}


/**
 * @brief I2C MSP DeInit
 * @param  hi2c pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for I2C module
 * @retval None
 */
__weak void HAL_I2C_MspDeInit(I2C_HandleTypeDef *hi2c)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hi2c);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_I2C_MspDeInit could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup I2C_Exported_Functions_Group2 IO operation functions
 *  @brief   Data transfers functions
 *
   @verbatim
   ===============================================================================
 ##### IO operation functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Start Timing.
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_START(I2C_HandleTypeDef *hi2c, uint32_t Timeout)
{
    uint32_t tickstart = 0x00U, MasterStatus = 0x00U;

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    if (hi2c->State == HAL_I2C_STATE_READY) {
        hi2c->State     = HAL_I2C_STATE_BUSY_TX;
        hi2c->ErrorCode = HAL_I2C_ERROR_NONE;

        /* clear status */
        __HAL_I2C_CLEAR_IT(hi2c, I2C_MASTER_INT_FLAG);
        __HAL_I2C_CLEAR_FLAG(hi2c, I2C_MASTER_FLAG_ALL);

        /* general start */
        hi2c->Instance->MSSPCON |= I2C_MASTER_START;

        /* Wait until INT flag is set */
        if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_MASTER_INT_FLAG, Timeout, tickstart) != HAL_OK) {
            return (HAL_TIMEOUT);
        }

        /* read statue */
        MasterStatus = hi2c->Instance->MSSPSTAT;

        /* Check Start */
        if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_START)) {
            hi2c->State = HAL_I2C_STATE_READY;
            return (HAL_OK);
        } else if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_WCOL)) {
            /*Write col */
            hi2c->State = HAL_I2C_STATE_ERROR;
            return (HAL_ERROR);
        }

        hi2c->State = HAL_I2C_STATE_READY;
        return (HAL_ERROR);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Stop Timing.
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_STOP(I2C_HandleTypeDef *hi2c, uint32_t Timeout)
{
    uint32_t tickstart = 0x00U, MasterStatus = 0x00U;

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    hi2c->State     = HAL_I2C_STATE_BUSY_TX;
    hi2c->ErrorCode = HAL_I2C_ERROR_NONE;

    /* clear status */
    __HAL_I2C_CLEAR_IT(hi2c, I2C_MASTER_INT_FLAG);
    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_MASTER_FLAG_ALL);

    /* general stop */
    hi2c->Instance->MSSPCON |= I2C_MASTER_STOP;

    /* Wait until INT flag is set */
    if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_MASTER_INT_FLAG, Timeout, tickstart) != HAL_OK) {
        return (HAL_TIMEOUT);
    }
    /* read statue */
    MasterStatus = hi2c->Instance->MSSPSTAT;

    /* Check Stop */
    if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_STOP)) {
        hi2c->State = HAL_I2C_STATE_READY;
        return (HAL_OK);
    } else if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_WCOL)) {
        /*Write col */
        hi2c->State = HAL_I2C_STATE_ERROR;
        return (HAL_ERROR);
    } else {
        hi2c->State = HAL_I2C_STATE_ERROR;
    }

    hi2c->State = HAL_I2C_STATE_READY;
    return (HAL_ERROR);
}


/**
 * @brief  Transmits in master mode an amount of data in blocking mode.
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *                the configuration information for the specified I2C.
 * @param  DevAddress Target device address The device 7 bits address value
 *         in datasheet must be shifted to the left before calling the interface
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be sent
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_Master_SendByte(I2C_HandleTypeDef *hi2c, uint8_t Data, uint32_t Timeout)
{
    uint32_t tickstart = 0x00U, MasterStatus = 0x00U;
    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    if (hi2c->State == HAL_I2C_STATE_READY) {
        hi2c->State     = HAL_I2C_STATE_BUSY_TX;
        hi2c->ErrorCode = HAL_I2C_ERROR_NONE;

        /* clear status */
        __HAL_I2C_CLEAR_IT(hi2c, I2C_MASTER_INT_FLAG);
        __HAL_I2C_CLEAR_FLAG(hi2c, I2C_MASTER_FLAG_ALL);

        /* Write data to DR */
        hi2c->Instance->MSSPBUF = Data;

        /* Wait until INT flag is set */
        if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_MASTER_INT_FLAG, Timeout, tickstart) != HAL_OK) {
            return (HAL_TIMEOUT);
        }
        /* read statue */
        MasterStatus = hi2c->Instance->MSSPSTAT;

        /* Check ACK */
        if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_NACK)) {
            /* No ACK, Generate Stop */
            hi2c->State             = HAL_I2C_STATE_READY;
            hi2c->Instance->MSSPCON |= I2C_MASTER_STOP;
            return (HAL_ERROR);
        } else if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_WCOL)) {
            /*Write col */
            hi2c->State = HAL_I2C_STATE_ERROR;
            return (HAL_ERROR);
        }

        hi2c->State = HAL_I2C_STATE_READY;
        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Receives in master mode an amount of data in blocking mode.
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *                the configuration information for the specified I2C.
 * @param  DevAddress Target device address The device 7 bits address value
 *         in datasheet must be shifted to the left before calling the interface
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be sent
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_Master_RecvByte(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint8_t IsSendACK, uint32_t Timeout)
{
    uint32_t tickstart = 0x00U, MasterStatus;
    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    if (hi2c->State == HAL_I2C_STATE_READY) {
        hi2c->State     = HAL_I2C_STATE_BUSY_RX;
        hi2c->ErrorCode = HAL_I2C_ERROR_NONE;

        /* Clear status */
        __HAL_I2C_CLEAR_IT(hi2c, I2C_MASTER_INT_FLAG);
        __HAL_I2C_CLEAR_FLAG(hi2c, I2C_MASTER_FLAG_ALL);

        /* Receive Enable */
        hi2c->Instance->MSSPCON |= I2C_MASTERS_RECEIVE_ENABLE;

        /* Wait until INT flag is set */
        if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_MASTER_INT_FLAG, Timeout, tickstart) != HAL_OK) {
            return (HAL_TIMEOUT);
        }
        /* Read statue */
        MasterStatus = hi2c->Instance->MSSPSTAT;
        /* Check Full Flag */
        if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_FULL)) {
            /* Read data form DR */
            *pData = hi2c->Instance->MSSPBUF;
        }
        if (__HAL_I2C_GET_FLAG(MasterStatus, I2C_MASTER_FLAG_WCOL)) {
            /*Write col */
            hi2c->State = HAL_I2C_STATE_ERROR;
            return (HAL_ERROR);
        }

        /* Is Send ACK */
        if (IsSendACK) {
            __HAL_I2C_MASTER_SEND_ACK(hi2c);
            __HAL_I2C_CLEAR_IT(hi2c, I2C_MASTER_INT_FLAG);
            if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_MASTER_INT_FLAG, Timeout, tickstart) != HAL_OK) {
                return (HAL_TIMEOUT);
            }
        } else {
            __HAL_I2C_MASTER_SEND_NACK(hi2c);
        }

        hi2c->State = HAL_I2C_STATE_READY;
        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Waiting Start timing and Address in slave mode an amount of data in blocking mode.
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *                the configuration information for the specified I2C.
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_Slave_WaitOnStartAndAddress(I2C_HandleTypeDef *hi2c, uint32_t *RW, uint32_t Timeout)
{
    uint32_t tickstart = 0x00U, SlaveStatue;
    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    hi2c->State = HAL_I2C_STATE_READY;

    /* Enable Send ACK */
    __HAL_I2C_SLAVE_SEND_ACK(hi2c);

    while (1) {
        /* Wait until INT flag is set */
        if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_SLAVE_INT_FLAG, Timeout, tickstart) != HAL_OK) {
            return (HAL_TIMEOUT);
        }
        /* Read statue */
        SlaveStatue = hi2c->Instance->SSSPSTAT;
        /* Clear INT flag */
        __HAL_I2C_CLEAR_IT(hi2c, I2C_SLAVE_INT_FLAG);
        /* Check Start Flag */
        if (__HAL_I2C_GET_FLAG(SlaveStatue, I2C_SLAVE_FLAG_START)) {
            __HAL_I2C_CLEAR_FLAG(hi2c, I2C_SLAVE_FLAG_START);
        }
        /* Check Receive Address */
        if (__HAL_I2C_GET_FLAG(SlaveStatue, I2C_SLAVE_FLAG_FULL)) {
            /* Clear invalid */
            READ_REG(hi2c->Instance->SSSPBUF);
            /* Read or Write */
            *RW = SlaveStatue;

            return (HAL_OK);
        }
    }
}


/**
 * @brief  Transmits in slave mode an amount of data in blocking mode.
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *                the configuration information for the specified I2C.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be sent
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_Slave_SendByte(I2C_HandleTypeDef *hi2c, uint8_t Data, uint32_t Timeout)
{
    uint32_t tickstart = 0x00U, SlaveStatus = 0x0U;

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    if (hi2c->State == HAL_I2C_STATE_READY) {
        hi2c->State     = HAL_I2C_STATE_BUSY_RX;
        hi2c->ErrorCode = HAL_I2C_ERROR_NONE;

        /* Clear status */
        __HAL_I2C_CLEAR_IT(hi2c, I2C_SLAVE_INT_FLAG);
//    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_SLAVE_FLAG_ALL);

        /* Write data to DR */
        hi2c->Instance->SSSPBUF = Data;

        /* Release SCL */
        hi2c->Instance->SSSPCON |= I2C_SLAVESSPC0N_CKP;

        /* Wait until INT flag is set */
        if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_SLAVE_INT_FLAG, Timeout, tickstart) != HAL_OK) {
            return (HAL_TIMEOUT);
        }

        SlaveStatus = hi2c->Instance->SSSPSTAT;

        /* Check COL */
        if (__HAL_I2C_GET_FLAG(SlaveStatus, I2C_SLAVE_FLAG_WCOL)) {
            /*Write col */
            hi2c->State = HAL_I2C_STATE_ERROR;
            return (HAL_ERROR);
        }

        hi2c->State = HAL_I2C_STATE_READY;
        return (HAL_OK);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Receive in slave mode an amount of data in blocking mode
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for the specified I2C.
 * @param  pData Pointer to data buffer
 * @param  Size Amount of data to be sent
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_I2C_Slave_RecvByte(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint8_t IsSendACK, uint32_t Timeout)
{
    uint32_t tickstart = 0x00U, SlaveStatus = 0x0U;

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    if (hi2c->State == HAL_I2C_STATE_READY) {
        if (pData == NULL) {
            return (HAL_ERROR);
        }
        hi2c->State     = HAL_I2C_STATE_BUSY_RX;
        hi2c->ErrorCode = HAL_I2C_ERROR_NONE;
        /* Clear status */
        __HAL_I2C_CLEAR_IT(hi2c, I2C_SLAVE_INT_FLAG);
        __HAL_I2C_CLEAR_FLAG(hi2c, I2C_SLAVE_FLAG_ALL);

        /* Is Send ACK */
        if (IsSendACK) {
            hi2c->Instance->SSSPCON |= I2C_SLAVESSPC0N_ACKEN;
        } else {
            hi2c->Instance->SSSPCON &= ~I2C_SLAVESSPC0N_ACKEN;
        }

        /* Wait until INT flag is set */
        if (__I2C_WaitOnIntFlagUntilTimeout(hi2c, I2C_SLAVE_INT_FLAG, Timeout, tickstart) != HAL_OK) {
            return (HAL_TIMEOUT);
        }
        /* read statue */
        SlaveStatus = hi2c->Instance->SSSPSTAT;

        /* Check Buf Full*/
        if (__HAL_I2C_GET_FLAG(SlaveStatus, I2C_SLAVE_FLAG_FULL)) {
            /* Read data form DR */
            *pData = hi2c->Instance->SSSPBUF;
        }
        /* Check Stop */
        if (__HAL_I2C_GET_FLAG(SlaveStatus, I2C_SLAVE_FLAG_STOP)) {
            hi2c->State = HAL_I2C_STATE_READY;
            return (HAL_OK);
        } else if (__HAL_I2C_GET_FLAG(SlaveStatus, I2C_SLAVE_FLAG_WCOL)) {
            /*Write col */
            hi2c->State = HAL_I2C_STATE_ERROR;
            return (HAL_ERROR);
        }

        hi2c->State = HAL_I2C_STATE_READY;

        return (HAL_BUSY);
    } else {
        return (HAL_BUSY);
    }
}


/**
 * @brief  Reset I2C in slave mode
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for the specified I2C.
 * @retval HAL status
 */
void HAL_I2C_Slave_Reset(I2C_HandleTypeDef *hi2c)
{
    /*Soft reset */
    hi2c->Instance->SSSPCON |= I2C_SLAVESSPC0N_RST;
    /*Clear int flag */
    hi2c->Instance->SSSPIR &= ~I2C_SLAVESSPIR_IF;
    /*Clear statue */
    hi2c->Instance->SSSPSTAT &= ~I2C_SLAVE_FLAG_ALL;
    /*Rlease SLC */
    hi2c->Instance->SSSPCON |= I2C_SLAVESSPC0N_CKP;
    /*Diable send ACK */
    hi2c->Instance->SSSPCON &= ~I2C_SLAVESSPC0N_ACKEN;
    /*flush buffer */
    READ_REG(hi2c->Instance->SSSPBUF);
}


/**
 * @brief  Reset I2C in slave mode
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for the specified I2C.
 * @param  it interrupt.This value can be one of the following values:
 *         @arg @ref I2C_MASTER_INT_ENABLE
 *         @arg @ref I2C_SLAVE_INT_ENABLE
 * @retval HAL status
 */
void HAL_I2C_Enable_IT(I2C_HandleTypeDef *hi2c, uint32_t it)
{
    __HAL_I2C_ENABLE_IT(hi2c, it);
}


/**
 * @brief  This function handles I2C Communication Timeout.
 * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
 *         the configuration information for I2C module
 * @param  Flag specifies the I2C flag to check.
 * @param  Status The new Flag status (SET or RESET).
 * @param  Timeout Timeout duration
 * @param  Tickstart Tick start value
 * @retval HAL status
 */
static HAL_StatusTypeDef __I2C_WaitOnIntFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Flag, uint32_t Timeout, uint32_t Tickstart)
{
    /* Wait until flag is set */
    while (__HAL_I2C_GET_IT_FLAG(hi2c, Flag) != SET) {
        /* Check for the Timeout */
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0U) || ((HAL_GetTick() - Tickstart) > Timeout)) {
                hi2c->State = HAL_I2C_STATE_READY;
                return (HAL_TIMEOUT);
            }
        }
    }

    return (HAL_OK);
}


/**
 * @brief  Config the I2C BAUD
 * @rmtoll MSSPBRG    I2C_MASTERSSPBRG
 * @param  I2Cx I2C Instance
 * @param  I2C_SPEED value
 */
static void __I2C_ConfigSpeed(I2C_HandleTypeDef *hi2c, uint32_t Speed)
{
    uint32_t SysClk = 0, BusDiv = 0, BrgCfg = 0;
    BusDiv  = LL_CMU_GetBusClkPrescaler() + 1;
    SysClk  = HAL_CMU_SystemCoreClockUpdate();

    BrgCfg = (SysClk / BusDiv / Speed / 4) - 1;

    LL_I2C_SetMasterBaud(hi2c->Instance, BrgCfg);
}


/**
 * @brief  Initialize the I2C registers according to the specified parameters in I2C_InitStruct.
 * @param  I2Cx I2C Instance.
 * @param  I2C_InitStruct pointer to a @ref LL_I2C_InitTypeDef structure.
 */
static void __I2C_Config(I2C_HandleTypeDef *hi2c)
{
    /* Disable the selected I2C peripheral */
    __HAL_I2C_DISABLE(hi2c);

    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_MASTER_FLAG_ALL);
    __HAL_I2C_CLEAR_FLAG(hi2c, I2C_SLAVE_FLAG_ALL);

    __HAL_I2C_CLEAR_IT(hi2c, I2C_MASTER_INT_ALL);
    __HAL_I2C_CLEAR_IT(hi2c, I2C_SLAVE_INT_ALL);

    if (hi2c->Init.Mode == I2C_MODE_MASTER) {
        /* Retrieve Clock frequencies */
        __I2C_ConfigSpeed(hi2c, hi2c->Init.ClockSpeed);
        //Enable Master
        hi2c->Instance->MEN |= I2C_MODE_MASTER;
    } else {
        /*Set slave AddressSize and address */
        MODIFY_REG(hi2c->Instance->SSSPADDR, I2C_SLAVESSPADDR, hi2c->Init.OwnAddress);
        MODIFY_REG(hi2c->Instance->SSSPCON, I2C_SLAVESSPC0N_A10EN | I2C_SLAVESSPC0N_CKSEN | I2C_SLAVESSPC0N_ACKEN, \
                   hi2c->Init.OwnAddrSize | hi2c->Init.StretchEn | I2C_SLAVESSPC0N_ACKEN);
        //Enable Slave
        hi2c->Instance->MEN &= ~(I2C_MODE | I2C_MASTER_EN);
    }
}


/**
 * @}
 */

#endif /* I2C_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
