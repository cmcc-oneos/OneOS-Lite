/**
 ******************************************************************************
 * @file    fm15f3xx_hal_dac.c
 * @author  MCD Application Team
 * @brief   DAC HAL module driver.
 *         This file provides firmware functions to manage the following
 *         functionalities of the Digital to Analog Converter (DAC) peripheral:
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup DAC DAC
 * @brief DAC driver modules
 * @{
 */

#ifdef DAC_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static __IO uint8_t DAC_FIFO_POINTER;


/** @addtogroup DAC_Private_Functions
 * @{
 */
/* Private function prototypes -----------------------------------------------*/
static void __DAC_Config(DAC_HandleTypeDef *hdac);


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup DAC_Exported_Functions DAC Exported Functions
 * @{
 */


/** @defgroup DAC_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ==============================================================================
 ##### Initialization and de-initialization functions #####
   ==============================================================================
    [..]  This section provides functions allowing to:
      (+) Initialize and configure the DAC.
      (+) De-initialize the DAC.

   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the DAC peripheral according to the specified parameters
 *         in the DAC_InitStruct.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_Init(DAC_HandleTypeDef* hdac)
{
    /* Check DAC handle */
    if (hdac == NULL) {
        return (HAL_ERROR);
    }
    /* Check the parameters */
    assert_param(IS_DAC_INSTANCE(hdac->Instance));

    if (hdac->State == HAL_DAC_STATE_RESET) {
        /* Init the low level hardware */
        HAL_DAC_MspInit(hdac);
    }

    /* Set DAC parameters */
    __DAC_Config(hdac);

    /* Get DAC fifo pointer init value */
    DAC_FIFO_POINTER = hdac->Instance->DPTR;

    /* Set DAC error code to none */
    hdac->ErrorCode = HAL_DAC_ERROR_NONE;

    /* Initialize the DAC state*/
    hdac->State = HAL_DAC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Deinitializes the DAC peripheral registers to their default reset values.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_DeInit(DAC_HandleTypeDef* hdac)
{
    /* Check DAC handle */
    if (hdac == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_DAC_INSTANCE(hdac->Instance));

    /* Change DAC state */
    hdac->State = HAL_DAC_STATE_BUSY;

    /* DeInit the low level hardware */
    HAL_DAC_MspDeInit(hdac);

    /* Set DAC error code to none */
    hdac->ErrorCode = HAL_DAC_ERROR_NONE;

    /* Change DAC state */
    hdac->State = HAL_DAC_STATE_RESET;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Initializes the DAC MSP.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval None
 */
__weak void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hdac);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DAC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes the DAC MSP.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval None
 */
__weak void HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac)
{
    __HAL_DAC_DISABLE(hdac);
}


/**
 * @}
 */


/** @defgroup DAC_Exported_Functions_Group2 IO operation functions
 *  @brief    IO operation functions
 *
   @verbatim
   ==============================================================================
 ##### IO operation functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Enables DAC.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_Start(DAC_HandleTypeDef* hdac)
{
    /* Enable the Peripheral */
    __HAL_DAC_ENABLE(hdac);

    /* Change DAC state */
    hdac->State = HAL_DAC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Disables DAC.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_Stop(DAC_HandleTypeDef* hdac)
{
    /* Disable the Peripheral */
    __HAL_DAC_DISABLE(hdac);

    /* Change DAC state */
    hdac->State = HAL_DAC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Enables DAC and INT.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @param  Interrupt flag can be combination of the following values:
 *         @arg @ref DAC_FLAG_WATERMARK_IT
 *         @arg @ref DAC_FLAG_TOP_IT
 *         @arg @ref DAC_FLAG_BOTTOM_IT
 * @param  WaterMark from 0 to 7.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_Start_IT(DAC_HandleTypeDef* hdac, uint32_t IT, uint8_t WaterMark)
{
    /* Set WaterMark */
    MODIFY_REG(hdac->Instance->DPCFG, DAC_DPCFG_WMK, WaterMark << DAC_DPCFG_WMK_Pos);

    /* Enable IT */
    __HAL_DAC_ENABLE_IT(hdac, IT);

    /* Enable the Peripheral */
    __HAL_DAC_ENABLE(hdac);

    /* Change DAC state */
    hdac->State = HAL_DAC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Disables DAC and INT.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_Stop_IT(DAC_HandleTypeDef* hdac)
{
    /* Disable the Peripheral */
    __HAL_DAC_DISABLE_IT(hdac, DAC_FLAG_WATERMARK_IT | DAC_FLAG_TOP_IT | DAC_FLAG_BOTTOM_IT);

    /* Disable the Peripheral */
    __HAL_DAC_DISABLE(hdac);

    /* Change DAC state */
    hdac->State = HAL_DAC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Handles DAC interrupt request
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval None
 */
void HAL_DAC_IRQHandler(DAC_HandleTypeDef* hdac)
{
    if (__HAL_DAC_GET_IT_FLAG(hdac, DAC_IT_BOTTOM)) {
        HAL_DAC_ConvBottomCallback(hdac);
    }
    if (__HAL_DAC_GET_IT_FLAG(hdac, DAC_IT_TOP)) {
        HAL_DAC_ConvTopCallback(hdac);
    }
    if (__HAL_DAC_GET_IT_FLAG(hdac, DAC_IT_WATERMARK)) {
        HAL_DAC_ConvWaterMarkCallback(hdac);
    }
    __HAL_DAC_CLEAR_IT_FLAG(hdac, DAC_IT_BOTTOM | DAC_IT_TOP | DAC_IT_WATERMARK);
}


/**
 * @brief  Conversion Bottom callback in non blocking mode.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval None
 */
__weak void HAL_DAC_ConvBottomCallback(DAC_HandleTypeDef* hdac)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hdac);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DAC_ConvBottomCallback could be implemented in the user file
     */
}


/**
 * @brief  Conversion Top callback in non blocking mode.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval None
 */
__weak void HAL_DAC_ConvTopCallback(DAC_HandleTypeDef* hdac)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hdac);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DAC_ConvTopCallback could be implemented in the user file
     */
}


/**
 * @brief  Conversion WaterMark callback in non blocking mode.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval None
 */
__weak void HAL_DAC_ConvWaterMarkCallback(DAC_HandleTypeDef* hdac)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hdac);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DAC_ConvWaterMarkCallback could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup DAC_Exported_Functions_Group3 Peripheral Control functions
 *  @brief      Peripheral Control functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral Control functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Set the specified data holding register value by single trigger.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @param  Data to be loaded in the selected data holding register.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_SetValueForSingleTrig(DAC_HandleTypeDef* hdac, uint16_t Data)
{
    __IO uint32_t tmp = 0U;
    /* Set the DAC selected data holding register */
    tmp = hdac->Instance->WORK & DAC_WORK_PTRM;

    if (DAC_WORKMODE_FIX == tmp) {
        DAC_FIFO_POINTER                        = hdac->Instance->DPTR;
        hdac->Instance->DATA[DAC_FIFO_POINTER]  = Data;
    } else if (DAC_WORKMODE_UP == tmp) {
        if (__HAL_DAC_GET_FLAG(hdac, DAC_FLAG_BOTTOM))
            DAC_FIFO_POINTER = 0;
        hdac->Instance->DATA[DAC_FIFO_POINTER++] = Data;
    } else {
        return (HAL_ERROR);
    }

    /* Check if software trigger enabled */
    tmp = hdac->Instance->TRIG & DAC_TRIG_TSC;
    if (tmp == DAC_TRIGSRC_SOFTWARE) {
        /* Enable the selected DAC software conversion*/
        hdac->Instance->SWTRIG = DAC_SWTRIG_CTRL;
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Set the specified data holding register value.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @param  Pointer the index of DAC data Register to load Data.
            @arg @ref Pointer:0U~7U
 * @param  Data to be loaded in DAC data Register.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DAC_SetValue(DAC_HandleTypeDef* hdac, uint8_t Pointer, uint16_t Data)
{
    __IO uint32_t tmp1 = 0U, tmp2 = 0U;

    /* Check if Pointer is Right in FIX Mode */
    tmp1    = hdac->Instance->WORK & DAC_WORK_PTRM;
    tmp2    = hdac->Instance->DPTR;
    if ((DAC_WORKMODE_FIX == tmp1) && (Pointer != tmp2)) {
        /* Disable the Peripheral */
        __HAL_DAC_DISABLE(hdac);

        hdac->Instance->DPTR = Pointer;

        /* Enable the Peripheral */
        __HAL_DAC_ENABLE(hdac);
    }

    /* Set the DAC selected data holding register */
    hdac->Instance->DATA[Pointer] = Data;

    /* Check if software trigger enabled */
    tmp2 = hdac->Instance->TRIG & DAC_TRIG_TSC;
    if ((DAC_WORKMODE_FIX == tmp1) && (DAC_TRIGSRC_SOFTWARE == tmp2)) {
        /* Enable the selected DAC software conversion*/
        hdac->Instance->SWTRIG = DAC_SWTRIG_CTRL;
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup DAC_Exported_Functions_Group4 Peripheral State and Errors functions
 *  @brief   Peripheral State and Errors functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral State and Errors functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  return the DAC state
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval HAL state
 */
HAL_DAC_StateTypeDef HAL_DAC_GetState(DAC_HandleTypeDef* hdac)
{
    /* Return DAC state */
    return (hdac->State);
}


/**
 * @brief  Return the DAC error code
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval DAC Error Code
 */
uint32_t HAL_DAC_GetError(DAC_HandleTypeDef *hdac)
{
    return (hdac->ErrorCode);
}


/* Private functions ---------------------------------------------------------*/


/**
 * @brief  Configures the DAC.
 * @param  hdac pointer to a DAC_HandleTypeDef structure that contains
 *         the configuration information for the specified DAC.
 * @retval HAL status
 */
static void __DAC_Config(DAC_HandleTypeDef *hdac)
{
    /* Disable the Peripheral */
    __HAL_DAC_DISABLE(hdac);

    /* Config CFG Reg */
    assert_param(IS_DAC_VERF(hdac->Init.VrefSel));
    assert_param(IS_DAC_POWERMODE(hdac->Init.PowerMode));
    MODIFY_REG(hdac->Instance->CFG, DAC_CFG_VSEL | DAC_CFG_HPEN, hdac->Init.VrefSel | hdac->Init.PowerMode);
    if (DAC_VREF_1P2V == hdac->Init.VrefSel) {
        __HAL_DAC_ENABLE_VREF12();
    }
    /* Config Work Reg */
    assert_param(IS_DAC_WORKMODE(hdac->Init.WorkMode));
    assert_param(IS_DAC_TRIGMODE(hdac->Init.TrigMode));
    MODIFY_REG(hdac->Instance->WORK, DAC_WORK_PTRM | DAC_WORK_SCANME, hdac->Init.WorkMode | hdac->Init.TrigMode);
    if (DAC_TRIGMODE_SCAN == hdac->Init.TrigMode) {
        assert_param(IS_DAC_SCANMODE(hdac->Init.ScanMode));
        assert_param(IS_DAC_SCANINTERVAL(hdac->Init.ScanIntervalCnt));
        MODIFY_REG(hdac->Instance->WORK, DAC_WORK_SCANOS | DAC_WORK_SCANINVL, hdac->Init.ScanMode | (hdac->Init.ScanIntervalCnt << DAC_WORK_SCANINVL_Pos));
    }
    /* Config Trig Reg */
    assert_param(IS_DAC_TRIGSRC(hdac->Init.TrigSource));
    assert_param(IS_DAC_TRIGFILTER(hdac->Init.TrigFilter));
    if (DAC_TRIGSRC_SOFTWARE != hdac->Init.TrigSource) {
        assert_param(IS_DAC_TRIGINV(hdac->Init.TrigInvEn));
        assert_param(IS_DAC_TRIGEDGE(hdac->Init.TrigEdge));
        MODIFY_REG(hdac->Instance->TRIG, DAC_TRIG_TINV | DAC_TRIG_TEDGE, hdac->Init.TrigInvEn | hdac->Init.TrigEdge);
    }
    MODIFY_REG(hdac->Instance->TRIG, DAC_TRIG_TSC | DAC_TRIG_TFILT, hdac->Init.TrigSource | hdac->Init.TrigFilter);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* DAC_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
