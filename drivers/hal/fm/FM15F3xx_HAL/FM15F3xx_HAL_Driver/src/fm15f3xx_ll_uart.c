/**
 ******************************************************************************
 * @file    fm15f3xx_ll_uart.c
 * @author  TYW
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/


#include "fm15f3xx_ll_uart.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

#if defined (UART0) || defined (UART1) || defined (UART2)


/** @defgroup UART
 * @brief UART driver modules
 * @{
 */


/** @defgroup UART_Private_TypesDefinitions
 * @{
 */


/**
 * @}
 */


/** @defgroup UART_Private_Defines
 * @{
 */


/**
 * @}
 */


/** @defgroup UART_Private_Macros
 * @{
 */


/**
 * @}
 */


/** @defgroup UART_Private_Variables
 * @{
 */


/**
 * @}
 */


/** @defgroup UART_Private_FunctionPrototypes
 * @{
 */


/**
 * @}
 */


/** @defgroup UART_Private_Functions
 * @{
 */


/**
 * @brief  De-initialize UART registers (Registers restored to their default values).
 * @param  UARTx UART Instance
 * @retval An ErrorStatus enumeration value:
 *          - SUCCESS: UART registers are de-initialized
 *          - ERROR: UART registers are not de-initialized
 */
ErrorStatus LL_UART_DeInit(UART_TypeDef *UARTx)
{
    ErrorStatus status = SUCCESS;

    if (UARTx == UART0) {
        LL_UART_SetTransferDir(UART0, LL_UART_DIRECTION_NONE);
        LL_UART_ClearFlag(UART0, LL_UART_FLAG_TXDONE);
    } else if (UARTx == UART1) {
        LL_UART_SetTransferDir(UART1, LL_UART_DIRECTION_NONE);
        LL_UART_ClearFlag(UART1, LL_UART_FLAG_TXDONE);
    } else if (UARTx == UART2) {
        LL_UART_SetTransferDir(UART2, LL_UART_DIRECTION_NONE);
        LL_UART_ClearFlag(UART2, LL_UART_FLAG_TXDONE);
    } else {
        status = ERROR;
    }

    return (status);
}


/**
 * @brief  Initialize UART registers according to the specified
 *         parameters in UART_InitStruct.
 * @param  UARTx UART Instance
 * @param  UART_InitStruct pointer to a LL_UART_InitTypeDef structure
 *         that contains the configuration information for the specified UART peripheral.
 * @retval An ErrorStatus enumeration value:
 *          - SUCCESS: UART registers are initialized according to UART_InitStruct content
 *          - ERROR: Problem occurred during UART Registers initialization
 */
ErrorStatus LL_UART_Init(UART_TypeDef *UARTx, LL_UART_InitTypeDef UART_InitStruct, uint32_t periphclk)
{
    ErrorStatus status = ERROR;

    /*---------------------------- UART CR Configuration -----------------------*/
    MODIFY_REG(UARTx->CR,
               UART_CR_RXEN | UART_CR_TXEN | UART_CR_PDSEL | UART_CR_STOPSEL,
               UART_InitStruct.TransferDirection | UART_InitStruct.Pdsel | UART_InitStruct.StopBits);

    /* Configure the UART Baud Rate */
    if (UART_InitStruct.BaudRate != 0U) {
        status = LL_UART_SetBaudRate(UARTx, periphclk, UART_InitStruct.SampleFactor, UART_InitStruct.BaudRate);
    }
    /* Endif (=> UART not in Disabled state => return ERROR) */

    return (status);
}


/**
 * @brief Set each @ref LL_UART_InitTypeDef field to default value.
 * @param  UART_InitStruct pointer to a @ref LL_UART_InitTypeDef structure
 *                          whose fields will be set to default values.
 * @retval None
 */

void LL_UART_StructInit(LL_UART_InitTypeDef *UART_InitStruct)
{
    /* Set UART_InitStruct fields to default values */
    UART_InitStruct->BaudRate           = 9600U;
    UART_InitStruct->Pdsel              = LL_UART_PDSEL_N_8;
    UART_InitStruct->StopBits           = LL_UART_STOPSEL_1BIT;
    UART_InitStruct->SampleFactor       = LL_UART_SAMPLEFACTOR_16;
    UART_InitStruct->TransferDirection  = LL_UART_DIRECTION_TX_RX;
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* UART0 || UART1 || UART2 */


/**
 * @}
 */

/************************ (C) COPYRIGHT Fudan Microelectronics *****END OF FILE****/

