/**
 ******************************************************************************
 * @file    fm15f3xx_ll_fsmc.c
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 *//* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_ll_fsmc.h"


/** @addtogroup FM15f3XX_LL_Driver
 * @{
 */

#if defined (FSMC)


/** @addtogroup FSMC_LL
 * @{
 */

/**
 * @brief  Initialize the FSMC_NORSRAM device according to the specified
 *         control parameters in the FSMC_NORSRAM_InitTypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Init Pointer to NORSRAM Initialization structure
 * @retval null
 */
void LL_FSMC_NORSRAMInit(FSMC_TypeDef *FSMCx, LL_FSMC_NORSRAMInitTypeDef *FSMC_NORSRAMInitStruct)
{
    LL_FSMC_Disable(FSMCx);

    MODIFY_REG(FSMCx->CTRL, 0x0701FF,
               LL_FSMC_NORSRAM_MODE |
               FSMC_NORSRAMInitStruct->MemoryWidth |
               FSMC_NORSRAMInitStruct->NWaitEn |
               FSMC_NORSRAMInitStruct->NWaitPol |
               FSMC_NORSRAMInitStruct->ClkEn);

    /* Config SYNCT Reg */
    if (LL_FSMC_CLK_ENABLE == FSMC_NORSRAMInitStruct->ClkEn) {
        LL_FSMC_SetClkDiv(FSMCx, FSMC_NORSRAMInitStruct->ClkDiv);
    }

    MODIFY_REG(FSMCx->EXTM, 0x07, FSMC_NORSRAMInitStruct->AccessMode);
}


/**
 * @brief  Initialize the FSMC_LCD device according to the specified
 *         control parameters in the FSMC_LCD_InitTypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Init Pointer to LCD Initialization structure
 * @retval null
 */
void LL_FSMC_LCDInit(FSMC_TypeDef *FSMCx, LL_FSMC_LCDInitTypeDef *FSMC_LCDInitStruct)
{
    LL_FSMC_Disable(FSMCx);

    MODIFY_REG(FSMCx->CTRL, 0x0701FF,
               LL_FSMC_LCD_MODE |
               FSMC_LCDInitStruct->LcdWidth |
               FSMC_LCDInitStruct->LcdType |
               FSMC_LCDInitStruct->EnOrRdPol |
               FSMC_LCDInitStruct->RwOrWrPol |
               LL_FSMC_CLK_DISABLE);

    MODIFY_REG(FSMCx->EXTM, 0x07, FSMC_LCDInitStruct->AccessMode);
}


/**
 * @brief  Initialize the Read Timing according to the specified
 *         parameters in the FSMC_Timing_TypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Timing Pointer to NORSRAM Timing structure
 * @retval HAL status
 */
void LL_FSMC_ReadTimingConfig(FSMC_TypeDef *FSMCx, LL_FSMC_TimingConfigTypeDef *FSMC_TimingtInitStruct)
{
    WRITE_REG(FSMCx->ARDT,
              (FSMC_TimingtInitStruct->AddressSetupTime << FSMC_ARDT_RDADDSET_Pos) |
              (FSMC_TimingtInitStruct->AddressHoldTime << FSMC_ARDT_RDADDHLD_Pos) |
              (FSMC_TimingtInitStruct->DataSetupTime << FSMC_ARDT_RDDATAST_Pos) |
              (FSMC_TimingtInitStruct->DataHoldTime << FSMC_ARDT_RDDATAHLD_Pos) |
              (FSMC_TimingtInitStruct->BusReadyTime << FSMC_ARDT_RDBUSRDY_Pos));
}


/**
 * @brief  Initialize the Write Timing according to the specified
 *         parameters in the FSMC_Timing_TypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Timing Pointer to NORSRAM Timing structure
 * @retval HAL status
 */
void LL_FSMC_WriteTimingConfig(FSMC_TypeDef *FSMCx, LL_FSMC_TimingConfigTypeDef *FSMC_TimingtInitStruct)
{
    WRITE_REG(FSMCx->AWRT,
              (FSMC_TimingtInitStruct->AddressSetupTime << FSMC_AWRT_WRADDSET_Pos) |
              (FSMC_TimingtInitStruct->AddressHoldTime << FSMC_AWRT_WRADDHLD_Pos) |
              (FSMC_TimingtInitStruct->DataSetupTime << FSMC_AWRT_WRDATAST_Pos) |
              (FSMC_TimingtInitStruct->DataHoldTime << FSMC_AWRT_WRDATAHLD_Pos) |
              (FSMC_TimingtInitStruct->BusReadyTime << FSMC_AWRT_WRBUSRDY_Pos));
}


/**
 * @brief  Enables FSMC.
 * @param  FSMC structure.
 */
void LL_FSMC_START(FSMC_TypeDef *FSMCx)
{
    /* Enable the Peripheral */
    LL_FSMC_Enable(FSMCx);
}


/**
 * @brief  Disables FSMC.
 * @param  FSMC structure.
 */
void LL_FSMC_Stop(FSMC_TypeDef *FSMCx)
{
    /* Disable the Peripheral */
    LL_FSMC_Disable(FSMCx);
}

/**
 * @brief  FSMC EXTM Mode Write Data
 * @param  FSMC structure.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number words to write
 */
void LL_FSMC_EXTM_Write(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num)
{
    uint32_t index = 0U;

    /* Clear flag */
    LL_FSMC_ClearFlag_Err(FSMCx);
    LL_FSMC_ClearFlag_Done(FSMCx);

    /* Set Wrtie Word Number */
    LL_FSMC_SetEXTMWordNum(FSMCx, 1);

    for (index = 0; index < Num; index++) {
        /* Set Address */
        LL_FSMC_SetEXTMStartAddr(FSMCx, Address++);

        /* Write Data */
        LL_FSMC_SetEXTMData(FSMCx, *Buffer++);

        /* Start Write */
        LL_FSMC_StartWrite(FSMCx);

        /* Check End of Access flag */
        while (!LL_FSMC_IsDone(FSMCx));

        /* Clear all flag */
        LL_FSMC_ClearFlag_Done(FSMCx);
    }
}

/**
 * @brief  FSMC EXTM Mode Read Data
 * @param  FSMC structure.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number words to read
 */
void LL_FSMC_EXTM_Read(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num)
{
    uint32_t index = 0U;

    /* Clear flag */
    LL_FSMC_ClearFlag_Err(FSMCx);
    LL_FSMC_ClearFlag_Done(FSMCx);

    /* Set Read Word Number */
    LL_FSMC_SetEXTMWordNum(FSMCx, 1);

    for (index = 0; index < Num; index++) {
        /* Set Address */
        LL_FSMC_SetEXTMStartAddr(FSMCx, Address++);

        /* Start Read Data */
        LL_FSMC_StartRead(FSMCx);

        /* Check End of Access flag */
        while (!LL_FSMC_IsDone(FSMCx));

        /* Read Data */
        *Buffer++ = LL_FSMC_GetEXTMData(FSMCx);

        /* Clear all flag */
        LL_FSMC_ClearFlag_Done(FSMCx);
    }
}

/**
 * @brief  FSMC EXTM FIFO Mode Write Data
 * @param  FSMC structure.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number word to write
 */
void LL_FSMC_EXTMFIFO_Write(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num)
{
    uint32_t index = 0U, count = 0U;

    /* Clear flag */
    LL_FSMC_ClearFlag_Err(FSMCx);
    LL_FSMC_ClearFlag_Done(FSMCx);
    LL_FSMC_ClearFifo(FSMCx);
    LL_FSMC_EnableRdFifo(FSMCx);

    while (Num) {
        /* Set Address */
        LL_FSMC_SetEXTMStartAddr(FSMCx, Address + count);

        if (Num >= 255) {
            count = 255;
            /* Set Wrtie Word Number */
            LL_FSMC_SetEXTMWordNum(FSMCx, 255);
        } else {
            count = Num;
            /* Set Wrtie Word Number */
            LL_FSMC_SetEXTMWordNum(FSMCx, Num);
        }
        /* Start Read Data */
        LL_FSMC_StartRead(FSMCx);
        for (index = 0; index < count; index++, Num--) {
            /* Write Data */
            LL_FSMC_SetEXTMData(FSMCx, *Buffer++);
        }

        /* Check End of Access flag */
        while (!LL_FSMC_IsDone(FSMCx));
        /* Clear all flag */
        LL_FSMC_ClearFlag_Done(FSMCx);
    }
}


/**
 * @brief  FSMC EXTM FIFO Mode Write Data
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *        the configuration information for the specified FSMC.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number word to write
 * @param  Timeout
 * @retval HAl Status
 */
void LL_FSMC_EXTMFIFO_Read(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num)
{
    uint32_t index = 0U, count = 0U;

    /* Clear flag */
    LL_FSMC_ClearFlag_Err(FSMCx);
    LL_FSMC_ClearFlag_Done(FSMCx);
    LL_FSMC_ClearFifo(FSMCx);
    LL_FSMC_EnableRdFifo(FSMCx);

    while (Num) {
        /* Set Address */
        LL_FSMC_SetEXTMStartAddr(FSMCx, Address + count);

        if (Num >= 255) {
            count = 255;
            /* Set Wrtie Word Number */
            LL_FSMC_SetEXTMWordNum(FSMCx, 255);
        } else {
            count = Num;
            /* Set Wrtie Word Number */
            LL_FSMC_SetEXTMWordNum(FSMCx, Num);
        }

        /* Start Read Data */
        LL_FSMC_StartRead(FSMCx);
        for (index = 0; index < count; index++, Num--) {
            /* Start Read Data */
            *Buffer++ = LL_FSMC_GetEXTMData(FSMCx);
        }
        /* Check End of Access flag */
        while (!LL_FSMC_IsDone(FSMCx));
        /* Clear all flag */
        LL_FSMC_ClearFlag_Done(FSMCx);
    }
}
/**
 * @}FSMC_LL
 */

#endif /* FSMC */


/**
 * @}FM15F3XX_LL_Driver
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
