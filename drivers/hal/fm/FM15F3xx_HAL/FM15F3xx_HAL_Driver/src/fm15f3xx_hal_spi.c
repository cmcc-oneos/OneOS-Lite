/**
 ******************************************************************************
 * @file    fm15f3xx_hal_spi.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-04-23
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


#include "fm15f3xx_hal.h"

/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/


/** @defgroup SPI_Private_Constants SPI Private Constants
 * @{
 */
#define SPI_DEFAULT_TIMEOUT 100U


/**
 * @}
 */
static uint32_t __SPI_SendData(SPI_HandleTypeDef *hspi, uint8_t* pData);


static uint32_t __SPI_ReceiveData(SPI_HandleTypeDef *hspi, uint8_t* pData);


static void __SPI_TxISR_AnyBIT(SPI_HandleTypeDef *hspi);


static void __SPI_RxISR_AnyBIT(SPI_HandleTypeDef *hspi);


static void __SPI_CloseTx_ISR(SPI_HandleTypeDef *hspi);


static void __SPI_CloseRx_ISR(SPI_HandleTypeDef *hspi);


static void __SPI_2LinesRxISR_AnyBIT(SPI_HandleTypeDef *hspi);


static void __SPI_2LinesTxISR_AnyBIT(SPI_HandleTypeDef *hspi);


static void __SPI_CloseRxTx_ISR(SPI_HandleTypeDef *hspi);


static HAL_StatusTypeDef __SPI_WaitFifoFlagStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Flag, uint32_t State, uint32_t Timeout, uint32_t Tickstart);


static HAL_StatusTypeDef __SPI_WaitFlagStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Flag, uint32_t State, uint32_t Timeout, uint32_t Tickstart);


static HAL_StatusTypeDef    __SPI_CheckFlag_BUSY(SPI_HandleTypeDef *hspi, uint32_t Timeout, uint32_t Tickstart);;
static void                 __SPI_Config(SPI_HandleTypeDef *hspi);


/**
 * @brief  Initialize the SPI according to the specified parameters
 *         in the SPI_InitTypeDef and initialize the associated handle.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_Init(SPI_HandleTypeDef *hspi)
{
    /* Check the SPI handle allocation */
    if (hspi == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_SPI_ALL_INSTANCE(hspi->Instance));
    assert_param(IS_SPI_MODE(hspi->Init.Mode));
    assert_param(IS_SPI_DIRECTION(hspi->Init.Direction));
    assert_param(IS_SPI_DATASIZE(hspi->Init.DataSize));
    assert_param(IS_SPI_SSN(hspi->Init.SSN));
    assert_param(IS_SPI_FIRST_BIT(hspi->Init.FirstBit));
    assert_param(IS_SPI_CPOL(hspi->Init.ClkPolarity));
    assert_param(IS_SPI_CPHA(hspi->Init.ClkPhase));
    if (hspi->Init.Mode == SPI_MODE_MASTER) {
        assert_param(IS_SPI_BAUDRATE_PRESCALER(hspi->Init.BaudRatePrescaler));
    }

    if (hspi->State == HAL_SPI_STATE_RESET) {
        /* Init the low level hardware : GPIO, CLOCK, NVIC... */
        HAL_SPI_MspInit(hspi);
    }

    hspi->State = HAL_SPI_STATE_BUSY;

    /* Disable the selected SPI peripheral */
    __HAL_SPI_DISABLE(hspi);

    /* Init SPI */
    __SPI_Config(hspi);

    hspi->ErrorCode = HAL_SPI_ERROR_NONE;
    hspi->State     = HAL_SPI_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  De Initialize the SPI peripheral.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_DeInit(SPI_HandleTypeDef *hspi)
{
    /* Check the SPI handle allocation */
    if (hspi == NULL) {
        return (HAL_ERROR);
    }

    /* Check SPI Instance parameter */
    assert_param(IS_SPI_ALL_INSTANCE(hspi->Instance));

    hspi->State = HAL_SPI_STATE_BUSY;

    /* Disable the SPI Peripheral Clock */
    __HAL_SPI_DISABLE(hspi);

    /* DeInit the low level hardware: GPIO, CLOCK, NVIC... */
    HAL_SPI_MspDeInit(hspi);

    hspi->ErrorCode = HAL_SPI_ERROR_NONE;
    hspi->State     = HAL_SPI_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief  Initialize the SPI MSP.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
__weak void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_SPI_MspInit should be implemented in the user file
     */
}


/**
 * @brief  De-Initialize the SPI MSP.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
__weak void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_SPI_MspDeInit should be implemented in the user file
     */
}


/**
 * @}
 */


/**
 * @brief  Transmit an amount of data in blocking mode.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @param  pData pointer to data buffer
 * @param  Size amount of data to be sent
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_Transmit(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    uint32_t            tickstart   = 0U;
    HAL_StatusTypeDef   errorcode   = HAL_OK;

    /* Check Direction parameter */
    assert_param(IS_SPI_DIRECTION_2LINES_OR_1LINE(hspi->Init.Direction));

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    if (hspi->State != HAL_SPI_STATE_READY) {
        errorcode = HAL_BUSY;
        goto error;
    }

    if ((pData == NULL) || (Size == 0)) {
        errorcode = HAL_ERROR;
        goto error;
    }

    /* Set the transaction information */
    hspi->State         = HAL_SPI_STATE_BUSY_TX;
    hspi->ErrorCode     = HAL_SPI_ERROR_NONE;
    hspi->pTxBuffPtr    = (uint8_t *) pData;
    hspi->TxXferSize    = Size;
    hspi->TxXferCount   = Size;

    /*Init field not used in handle to zero */
    hspi->pRxBuffPtr    = (uint8_t *) NULL;
    hspi->RxXferSize    = 0U;
    hspi->RxXferCount   = 0U;
    hspi->TxISR         = NULL;
    hspi->RxISR         = NULL;

    /* Configure communication direction : 1Line */
    if (hspi->Init.Direction == SPI_DIRECTION_1LINE) {
        SPI_1LINE_TX(hspi);
    }

    /* Check if the SPI is already enabled */
    if ((hspi->Instance->SPCR1 & SPI_SPCR1_SPIEN) != SPI_SPCR1_SPIEN) {
        /* Enable SPI peripheral */
        __HAL_SPI_ENABLE(hspi);
    }


    if ((hspi->Init.Mode == SPI_MODE_SLAVE) || (hspi->TxXferCount == 0x01)) {
        pData += __SPI_SendData(hspi, pData);
        hspi->TxXferCount--;
    }
    /* Transmit data */
    while (hspi->TxXferCount > 0U) {
        /* Wait until TXE flag is set to send data */
        if (__HAL_SPIFIFO_GET_FLAG(hspi, SPI_FLAG_FIFO_TXE)) {
            pData += __SPI_SendData(hspi, pData);
            hspi->TxXferCount--;
        } else {
            /* Timeout management */
            if ((Timeout == 0U) || ((Timeout != HAL_MAX_DELAY) && ((HAL_GetTick() - tickstart) >= Timeout))) {
                errorcode = HAL_TIMEOUT;
                goto error;
            }
        }
    }

    /* Wait until TXE flag */
    if (__SPI_WaitFifoFlagStateUntilTimeout(hspi, SPI_FLAG_FIFO_TXE, SET, Timeout, tickstart) != HAL_OK) {
        errorcode = HAL_TIMEOUT;
        goto error;
    }

    /* Check Busy flag */
    if (__SPI_CheckFlag_BUSY(hspi, Timeout, tickstart) != HAL_OK) {
        errorcode       = HAL_ERROR;
        hspi->ErrorCode = HAL_SPI_ERROR_FLAG;
        goto error;
    }

    /* Clear overrun flag in 2 Lines communication mode because received is not read */
    if (hspi->Init.Direction == SPI_DIRECTION_2LINES) {
        __HAL_SPI_CLEAR_OVRFLAG(hspi);
    }

    if (hspi->ErrorCode != HAL_SPI_ERROR_NONE) {
        errorcode = HAL_ERROR;
    }

error:
    hspi->State = HAL_SPI_STATE_READY;
    __HAL_SPI_CLEAR_FIFOFLAG(hspi);
    return (errorcode);
}


/**
 * @brief  Receive an amount of data in blocking mode.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @param  pData pointer to data buffer
 * @param  Size amount of data to be received
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    uint32_t            tickstart   = 0U;
    HAL_StatusTypeDef   errorcode   = HAL_OK;

    if ((hspi->Init.Mode == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_2LINES)) {
        hspi->State = HAL_SPI_STATE_BUSY_RX;
        /* Call transmit-receive function to send Dummy data on Tx line and generate clock on CLK line */
        return (HAL_SPI_TransmitReceive(hspi, pData, pData, Size, Timeout));
    }

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    if (hspi->State != HAL_SPI_STATE_READY) {
        errorcode = HAL_BUSY;
        goto error;
    }

    if ((pData == NULL) || (Size == 0)) {
        errorcode = HAL_ERROR;
        goto error;
    }

    /* Set the transaction information */
    hspi->State         = HAL_SPI_STATE_BUSY_RX;
    hspi->ErrorCode     = HAL_SPI_ERROR_NONE;
    hspi->pRxBuffPtr    = (uint8_t *) pData;
    hspi->RxXferSize    = Size;
    hspi->RxXferCount   = Size;

    /*Init field not used in handle to zero */
    hspi->pTxBuffPtr    = (uint8_t *) NULL;
    hspi->TxXferSize    = 0U;
    hspi->TxXferCount   = 0U;
    hspi->RxISR         = NULL;
    hspi->TxISR         = NULL;


    /* Configure communication direction: 1Line */
//  if(hspi->Init.Direction == SPI_DIRECTION_1LINE_TXONLY)
//  {
//    SPI_1LINE_RX(hspi);
//  }

    /* Check if the SPI is already enabled */
    if ((hspi->Instance->SPCR1 & SPI_SPCR1_SPIEN) != SPI_SPCR1_SPIEN) {
        /* Enable SPI peripheral */
        __HAL_SPI_ENABLE(hspi);
    }

    /* Transfer loop */
    while (hspi->RxXferCount > 0U) {
        /* Check the RXNE flag */
        if (__HAL_SPIFIFO_GET_FLAG(hspi, SPI_FLAG_FIFO_RXNE)) {
            /* read the received data */
            pData += __SPI_ReceiveData(hspi, pData);
            hspi->RxXferCount--;
        } else {
            /* Timeout management */
            if ((Timeout == 0U) || ((Timeout != HAL_MAX_DELAY) && ((HAL_GetTick() - tickstart) >= Timeout))) {
                errorcode = HAL_TIMEOUT;
                goto error;
            }
        }
    }

    /* Check the end of the transaction */
    if ((hspi->Init.Mode == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_1LINE)) {
        /* Disable SPI peripheral */
        __HAL_SPI_DISABLE(hspi);
    }

    if (hspi->ErrorCode != HAL_SPI_ERROR_NONE) {
        errorcode = HAL_ERROR;
    }

error:
    hspi->State = HAL_SPI_STATE_READY;
    __HAL_SPI_CLEAR_FIFOFLAG(hspi);
    return (errorcode);
}


/**
 * @brief  Transmit and Receive an amount of data in blocking mode.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @param  pTxData pointer to transmission data buffer
 * @param  pRxData pointer to reception data buffer
 * @param  Size amount of data to be sent and received
 * @param  Timeout Timeout duration
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout)
{
    uint32_t tmp = 0U, tmp1 = 0U;
#if (USE_SPI_CRC != 0U)
    __IO uint16_t tmpreg1 = 0U;
#endif /* USE_SPI_CRC */
    uint32_t tickstart = 0U;
    /* Variable used to alternate Rx and Tx during transfer */
    uint32_t            txallowed   = 1U;
    HAL_StatusTypeDef   errorcode   = HAL_OK;

    /* Check Direction parameter */
    assert_param(IS_SPI_DIRECTION_2LINES(hspi->Init.Direction));

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    tmp     = hspi->State;
    tmp1    = hspi->Init.Mode;

    if (!((tmp == HAL_SPI_STATE_READY) || \
          ((tmp1 == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_2LINES) && (tmp == HAL_SPI_STATE_BUSY_RX)))) {
        errorcode = HAL_BUSY;
        goto error;
    }

    if ((pTxData == NULL) || (pRxData == NULL) || (Size == 0)) {
        errorcode = HAL_ERROR;
        goto error;
    }

    /* Don't overwrite in case of HAL_SPI_STATE_BUSY_RX */
    if (hspi->State == HAL_SPI_STATE_READY) {
        hspi->State = HAL_SPI_STATE_BUSY_TX_RX;
    }

    /* Set the transaction information */
    hspi->ErrorCode     = HAL_SPI_ERROR_NONE;
    hspi->pRxBuffPtr    = (uint8_t *) pRxData;
    hspi->RxXferCount   = Size;
    hspi->RxXferSize    = Size;
    hspi->pTxBuffPtr    = (uint8_t *) pTxData;
    hspi->TxXferCount   = Size;
    hspi->TxXferSize    = Size;

    /*Init field not used in handle to zero */
    hspi->RxISR = NULL;
    hspi->TxISR = NULL;


    /* Check if the SPI is already enabled */
    if ((hspi->Instance->SPCR1 & SPI_SPCR1_SPIEN) != SPI_SPCR1_SPIEN) {
        /* Enable SPI peripheral */
        __HAL_SPI_ENABLE(hspi);
    }


    if ((hspi->Init.Mode == SPI_MODE_SLAVE) || (hspi->TxXferCount == 0x01U)) {
        pTxData += __SPI_SendData(hspi, pTxData);
        hspi->TxXferCount--;
    }
    while ((hspi->TxXferCount > 0U) || (hspi->RxXferCount > 0U)) {
        /* Check TXE flag */
        if (txallowed && (hspi->TxXferCount > 0U) && (__HAL_SPIFIFO_GET_FLAG(hspi, SPI_FLAG_FIFO_TXE))) {
            pTxData += __SPI_SendData(hspi, pTxData);
            hspi->TxXferCount--;
            /* Next Data is a reception (Rx). Tx not allowed */
            txallowed = 0U;
        }

        /* Check RXNE flag */
        if ((hspi->RxXferCount > 0U) && (__HAL_SPIFIFO_GET_FLAG(hspi, SPI_FLAG_FIFO_RXNE))) {
            pRxData += __SPI_ReceiveData(hspi, pRxData);
            hspi->RxXferCount--;
            /* Next Data is a Transmission (Tx). Tx is allowed */
            txallowed = 1U;
        }
        if ((Timeout != HAL_MAX_DELAY) && ((HAL_GetTick() - tickstart) >= Timeout)) {
            errorcode = HAL_TIMEOUT;
            goto error;
        }
    }

    /* Wait until TXE flag */
    if (__SPI_WaitFifoFlagStateUntilTimeout(hspi, SPI_FLAG_FIFO_TXE, SET, Timeout, tickstart) != HAL_OK) {
        errorcode = HAL_TIMEOUT;
        goto error;
    }

    /* Check Busy flag */
    if (__SPI_CheckFlag_BUSY(hspi, Timeout, tickstart) != HAL_OK) {
        errorcode       = HAL_ERROR;
        hspi->ErrorCode = HAL_SPI_ERROR_FLAG;
        goto error;
    }

    /* Clear overrun flag in 2 Lines communication mode because received is not read */
    if (hspi->Init.Direction == SPI_DIRECTION_2LINES) {
        __HAL_SPI_CLEAR_OVRFLAG(hspi);
    }

error:
    hspi->State = HAL_SPI_STATE_READY;
    __HAL_SPI_CLEAR_FIFOFLAG(hspi);
    return (errorcode);
}


/**
 * @brief  Transmit an amount of data in non-blocking mode with Interrupt.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @param  pData pointer to data buffer
 * @param  Size amount of data to be sent
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_Transmit_IT(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size)
{
    HAL_StatusTypeDef errorcode = HAL_OK;

    /* Check Direction parameter */
    assert_param(IS_SPI_DIRECTION_2LINES_OR_1LINE(hspi->Init.Direction));

    if ((pData == NULL) || (Size == 0)) {
        errorcode = HAL_ERROR;
        goto error;
    }

    if (hspi->State != HAL_SPI_STATE_READY) {
        errorcode = HAL_BUSY;
        goto error;
    }

    /* Set the transaction information */
    hspi->State         = HAL_SPI_STATE_BUSY_TX;
    hspi->ErrorCode     = HAL_SPI_ERROR_NONE;
    hspi->pTxBuffPtr    = (uint8_t *) pData;
    hspi->TxXferSize    = Size;
    hspi->TxXferCount   = Size;

    /* Init field not used in handle to zero */
    hspi->pRxBuffPtr    = (uint8_t *) NULL;
    hspi->RxXferSize    = 0U;
    hspi->RxXferCount   = 0U;
    hspi->RxISR         = NULL;

    /* Set the function for IT treatment */
    hspi->TxISR = __SPI_TxISR_AnyBIT;

    /* Configure communication direction : 1Line */
    if (hspi->Init.Direction == SPI_DIRECTION_1LINE) {
        SPI_1LINE_TX(hspi);
    }

    if (hspi->Init.Direction == SPI_DIRECTION_2LINES) {
        /* Enable TXE interrupt */
        __HAL_SPI_ENABLE_IT(hspi, (SPI_IT_TXE));
    } else {
        /* Enable TXE and ERR interrupt */
        __HAL_SPI_ENABLE_IT(hspi, (SPI_IT_TXE | SPI_IT_ERR));
    }

    /* Check if the SPI is already enabled */
    if ((hspi->Instance->SPCR1 & SPI_SPCR1_SPIEN) != SPI_SPCR1_SPIEN) {
        /* Enable SPI peripheral */
        __HAL_SPI_ENABLE(hspi);
    }

error:
    return (errorcode);
}


/**
 * @brief  Receive an amount of data in non-blocking mode with Interrupt.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @param  pData pointer to data buffer
 * @param  Size amount of data to be sent
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_Receive_IT(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size)
{
    HAL_StatusTypeDef errorcode = HAL_OK;

    if ((hspi->Init.Direction == SPI_DIRECTION_2LINES) && (hspi->Init.Mode == SPI_MODE_MASTER)) {
        hspi->State = HAL_SPI_STATE_BUSY_RX;
        /* Call transmit-receive function to send Dummy data on Tx line and generate clock on CLK line */
        return (HAL_SPI_TransmitReceive_IT(hspi, pData, pData, Size));
    }

    if (hspi->State != HAL_SPI_STATE_READY) {
        errorcode = HAL_BUSY;
        goto error;
    }

    if ((pData == NULL) || (Size == 0)) {
        errorcode = HAL_ERROR;
        goto error;
    }

    /* Set the transaction information */
    hspi->State         = HAL_SPI_STATE_BUSY_RX;
    hspi->ErrorCode     = HAL_SPI_ERROR_NONE;
    hspi->pRxBuffPtr    = (uint8_t *) pData;
    hspi->RxXferSize    = Size;
    hspi->RxXferCount   = Size;

    /* Init field not used in handle to zero */
    hspi->pTxBuffPtr    = (uint8_t *) NULL;
    hspi->TxXferSize    = 0U;
    hspi->TxXferCount   = 0U;
    hspi->TxISR         = NULL;

    /* Set the function for IT treatment */
    hspi->RxISR = __SPI_RxISR_AnyBIT;

    /* Configure communication direction : 1Line */
//  if(hspi->Init.Direction == SPI_DIRECTION_1LINE_TXONLY)
//  {
//    SPI_1LINE_RX(hspi);
//  }


    /* Enable TXE and ERR interrupt */
    __HAL_SPI_ENABLE_IT(hspi, (SPI_IT_RXNE | SPI_IT_ERR));


    /* Note : The SPI must be enabled after unlocking current process
              to avoid the risk of SPI interrupt handle execution before current
              process unlock */

    /* Check if the SPI is already enabled */
    if ((hspi->Instance->SPCR1 & SPI_SPCR1_SPIEN) != SPI_SPCR1_SPIEN) {
        /* Enable SPI peripheral */
        __HAL_SPI_ENABLE(hspi);
    }

error:
    return (errorcode);
}


/**
 * @brief  Transmit and Receive an amount of data in non-blocking mode with Interrupt.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @param  pTxData pointer to transmission data buffer
 * @param  pRxData pointer to reception data buffer
 * @param  Size amount of data to be sent and received
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_SPI_TransmitReceive_IT(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size)
{
    uint32_t            tmp = 0U, tmp1 = 0U;
    HAL_StatusTypeDef   errorcode = HAL_OK;

    /* Check Direction parameter */
    assert_param(IS_SPI_DIRECTION_2LINES(hspi->Init.Direction));

    tmp     = hspi->State;
    tmp1    = hspi->Init.Mode;

    if (!((tmp == HAL_SPI_STATE_READY) || \
          ((tmp1 == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_2LINES) && (tmp == HAL_SPI_STATE_BUSY_RX)))) {
        errorcode = HAL_BUSY;
        goto error;
    }

    if ((pTxData == NULL) || (pRxData == NULL) || (Size == 0)) {
        errorcode = HAL_ERROR;
        goto error;
    }

    /* Don't overwrite in case of HAL_SPI_STATE_BUSY_RX */
    if (hspi->State == HAL_SPI_STATE_READY) {
        hspi->State = HAL_SPI_STATE_BUSY_TX_RX;
    }

    /* Set the transaction information */
    hspi->ErrorCode     = HAL_SPI_ERROR_NONE;
    hspi->pTxBuffPtr    = (uint8_t *) pTxData;
    hspi->TxXferSize    = Size;
    hspi->TxXferCount   = Size;
    hspi->pRxBuffPtr    = (uint8_t *) pRxData;
    hspi->RxXferSize    = Size;
    hspi->RxXferCount   = Size;

    /* Set the function for IT treatment */
    hspi->RxISR = __SPI_2LinesRxISR_AnyBIT;
    hspi->TxISR = __SPI_2LinesTxISR_AnyBIT;

    /* Enable TXE, RXNE and ERR interrupt */
    __HAL_SPI_ENABLE_IT(hspi, (SPI_IT_TXE | SPI_IT_RXNE | SPI_IT_ERR));

    /* Check if the SPI is already enabled */
    if ((hspi->Instance->SPCR1 & SPI_SPCR1_SPIEN) != SPI_SPCR1_SPIEN) {
        /* Enable SPI peripheral */
        __HAL_SPI_ENABLE(hspi);
    }

error:
    return (errorcode);
}


/**
 * @brief  Handle SPI interrupt request.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for the specified SPI module.
 * @retval None
 */
void HAL_SPI_IRQHandler(SPI_HandleTypeDef *hspi)
{
    uint32_t    itsource    = hspi->Instance->SPIIE;
    uint32_t    fifoflag    = hspi->Instance->SPDF;
    uint32_t    statflag    = hspi->Instance->SPSR;

    /* SPI in mode Receiver ----------------------------------------------------*/
    if (((statflag & SPI_FLAG_RXWCOL) == RESET) &&
        ((fifoflag & SPI_FLAG_FIFO_RXNE) != RESET) && ((itsource & SPI_IT_RXNE) != RESET)) {
        hspi->RxISR(hspi);
        return;
    }

    /* SPI in mode Transmitter -------------------------------------------------*/
    if (((fifoflag & SPI_FLAG_FIFO_TXE) != RESET) && ((itsource & SPI_IT_TXE) != RESET)) {
        hspi->TxISR(hspi);
        return;
    }

    /* SPI in Error Treatment --------------------------------------------------*/
    if (((statflag & (SPI_FLAG_TXWCOL | SPI_FLAG_RXWCOL | SPI_FLAG_SERR | SPI_FLAG_MERR)) != RESET) && ((itsource & SPI_IT_ERR) != RESET)) {
        /* SPI Overrun error interrupt occurred ----------------------------------*/
        if ((statflag & (SPI_FLAG_RXWCOL | SPI_FLAG_TXWCOL)) != RESET) {
            if (hspi->State != HAL_SPI_STATE_BUSY_TX) {
                SET_BIT(hspi->ErrorCode, HAL_SPI_ERROR_OVR);
                __HAL_SPI_CLEAR_OVRFLAG(hspi);
            } else {
                __HAL_SPI_CLEAR_OVRFLAG(hspi);
                return;
            }
        }

        /* SPI Mode Fault error interrupt occurred -------------------------------*/
        if ((statflag & (SPI_FLAG_MERR | SPI_FLAG_SERR)) != RESET) {
            SET_BIT(hspi->ErrorCode, HAL_SPI_ERROR_ERR);
            __HAL_SPI_CLEAR_ERRFLAG(hspi);
        }

        if (hspi->ErrorCode != HAL_SPI_ERROR_NONE) {
            /* Disable all interrupts */
            __HAL_SPI_DISABLE_IT(hspi, SPI_IT_RXNE | SPI_IT_TXE | SPI_IT_ERR);

            hspi->State = HAL_SPI_STATE_READY;
            /* Disable the SPI DMA requests if enabled */
            if ((HAL_IS_BIT_SET(itsource, SPI_DMA_TXE)) || (HAL_IS_BIT_SET(itsource, SPI_DMA_RXNE))) {
                CLEAR_BIT(hspi->Instance->SPIIE, (SPI_DMA_TXE | SPI_DMA_RXNE));

//        /* Abort the SPI DMA Rx channel */
//        if(hspi->hdmarx != NULL)
//        {
//          /* Set the SPI DMA Abort callback :
//          will lead to call HAL_SPI_ErrorCallback() at end of DMA abort procedure */
//          hspi->hdmarx->XferAbortCallback = SPI_DMAAbortOnError;
//          HAL_DMA_Abort_IT(hspi->hdmarx);
//        }
//        /* Abort the SPI DMA Tx channel */
//        if(hspi->hdmatx != NULL)
//        {
//          /* Set the SPI DMA Abort callback :
//          will lead to call HAL_SPI_ErrorCallback() at end of DMA abort procedure */
//          hspi->hdmatx->XferAbortCallback = SPI_DMAAbortOnError;
//          HAL_DMA_Abort_IT(hspi->hdmatx);
//        }
            } else {
                /* Call user error callback */
                HAL_SPI_ErrorCallback(hspi);
            }
        }
        return;
    }
}


/**
 * @brief SPI error callback.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
__weak void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_SPI_ErrorCallback should be implemented in the user file
     */


    /* NOTE : The ErrorCode parameter in the hspi handle is updated by the SPI processes
              and user can use HAL_SPI_GetError() API to check the latest error occurred
     */
}


/**
 * @brief Rx Transfer completed callback.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
__weak void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_SPI_RxCpltCallback should be implemented in the user file
     */
}


/**
 * @brief Tx Transfer completed callback.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
__weak void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_SPI_TxCpltCallback should be implemented in the user file
     */
}


/**
 * @brief Tx and Rx Transfer completed callback.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
__weak void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_SPI_TxRxCpltCallback should be implemented in the user file
     */
}


/**
 * @brief Tx Half Transfer completed callback.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
__weak void HAL_SPI_TxHalfCpltCallback(SPI_HandleTypeDef *hspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_SPI_TxHalfCpltCallback should be implemented in the user file
     */
}


/* Private functions ---------------------------------------------------------*/


/**
 * @brief  Transmit a data.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *              the configuration information for SPI module.
 * @param  pData pointer to data buffer
 * @retval Offet of Data Buffer.
 */
static uint32_t __SPI_SendData(SPI_HandleTypeDef *hspi, uint8_t* pData)
{
    uint32_t tempdata = 0, offset = 0;
    switch (hspi->Init.DataSize) {
    case SPI_DATASIZE_16BIT:
        tempdata    = *((uint16_t *) pData);
        offset      = sizeof(uint16_t);
        break;
    case SPI_DATASIZE_8BIT:
        tempdata    = (*pData);
        offset      = sizeof(uint8_t);
        break;
    case SPI_DATASIZE_24BIT://TODO
        tempdata    = *((uint32_t *) pData);
        tempdata    &= 0xffffff;
        offset      = (3 * sizeof(uint8_t));
        break;
    case SPI_DATASIZE_32BIT:
        tempdata    = *((uint32_t *) pData);
        offset      = sizeof(uint32_t);
        break;
    }
    hspi->Instance->TXFIFO = tempdata;
    return (offset);
}


/**
 * @brief  Receive a data.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @param  pData pointer to data buffer
 * @retval Offet of Data Buffer.
 */
static uint32_t __SPI_ReceiveData(SPI_HandleTypeDef *hspi, uint8_t* pData)
{
    uint32_t offset = 0;
    switch (hspi->Init.DataSize) {
    case SPI_DATASIZE_16BIT:
        *((uint16_t *) pData)   = hspi->Instance->RXFIFO;
        offset                  = sizeof(uint16_t);
        break;
    case SPI_DATASIZE_8BIT:
        (*(uint8_t *) pData)    = hspi->Instance->RXFIFO;
        offset                  = sizeof(uint8_t);
        break;
    case SPI_DATASIZE_24BIT:
        *((uint32_t *) pData)   = hspi->Instance->RXFIFO;
        offset                  = (3 * sizeof(uint8_t));
        break;
    case SPI_DATASIZE_32BIT:
        *((uint32_t *) pData)   = hspi->Instance->RXFIFO;
        offset                  = sizeof(uint32_t);
        break;
    }
    return (offset);
}


/**
 * @brief Handle SPI Communication Timeout.
 * @param hspi pointer to a SPI_HandleTypeDef structure that contains
 *              the configuration information for SPI module.
 * @param Flag SPI FIFOflag to check
 * @param State flag state to check
 * @param Timeout Timeout duration
 * @param Tickstart tick start value
 * @retval HAL status
 */
static HAL_StatusTypeDef __SPI_WaitFifoFlagStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Flag, uint32_t State, uint32_t Timeout, uint32_t Tickstart)
{
    while ((((hspi->Instance->SPDF & Flag) == (Flag)) ? SET : RESET) != State) {
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0U) || ((HAL_GetTick() - Tickstart) >= Timeout)) {
                /* Disable the SPI and reset the CRC: the CRC value should be cleared
                   on both master and slave sides in order to resynchronize the master
                   and slave for their respective CRC calculation */

                /* Disable TXE, RXNE and ERR interrupts for the interrupt process */
                __HAL_SPI_DISABLE_IT(hspi, (SPI_IT_TXE | SPI_IT_RXNE | SPI_IT_ERR));

                if ((hspi->Init.Mode == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_1LINE)) {
                    /* Disable SPI peripheral */
                    __HAL_SPI_DISABLE(hspi);
                }

                hspi->State = HAL_SPI_STATE_READY;

                return (HAL_TIMEOUT);
            }
        }
    }

    return (HAL_OK);
}


/**
 * @brief  Handle the data any-bit transmit in Interrupt mode.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
static void __SPI_TxISR_AnyBIT(SPI_HandleTypeDef *hspi)
{
    /* Transmit data in any Bit mode */
    hspi->pTxBuffPtr += __SPI_SendData(hspi, hspi->pTxBuffPtr);
    hspi->TxXferCount--;

    if (hspi->TxXferCount == 0U) {
        __SPI_CloseTx_ISR(hspi);
    }
}


/**
 * @brief  Manage the any-bit receive in Interrupt context.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
static void __SPI_RxISR_AnyBIT(SPI_HandleTypeDef *hspi)
{
    hspi->pRxBuffPtr += __SPI_ReceiveData(hspi, hspi->pRxBuffPtr);
    hspi->RxXferCount--;

    if (hspi->RxXferCount == 0U) {
        __SPI_CloseRx_ISR(hspi);
    }
}


/**
 * @brief  Rx any-bit handler for Transmit and Receive in Interrupt mode.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
static void __SPI_2LinesRxISR_AnyBIT(SPI_HandleTypeDef *hspi)
{
    /* Receive data in any Bit mode */
    hspi->pRxBuffPtr += __SPI_ReceiveData(hspi, hspi->pRxBuffPtr);
    hspi->RxXferCount--;

    if (hspi->RxXferCount == 0U) {
        /* Disable RXNE interrupt */
        __HAL_SPI_DISABLE_IT(hspi, SPI_IT_RXNE);

        if (hspi->TxXferCount == 0U) {
            __SPI_CloseRxTx_ISR(hspi);
        }
    }
}


/**
 * @brief  Tx 16-bit handler for Transmit and Receive in Interrupt mode.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
static void __SPI_2LinesTxISR_AnyBIT(SPI_HandleTypeDef *hspi)
{
    /* Transmit data in any Bit mode */
    hspi->pTxBuffPtr += __SPI_SendData(hspi, hspi->pTxBuffPtr);
    hspi->TxXferCount--;

    /* Enable CRC Transmission */
    if (hspi->TxXferCount == 0U) {
        /* Disable TXE interrupt */
        __HAL_SPI_DISABLE_IT(hspi, SPI_IT_TXE);

        if (hspi->RxXferCount == 0U) {
            __SPI_CloseRxTx_ISR(hspi);
        }
    }
}


/**
 * @brief Handle SPI Communication Timeout.
 * @param hspi pointer to a SPI_HandleTypeDef structure that contains
 *              the configuration information for SPI module.
 * @param Flag SPI flag to check
 * @param State flag state to check
 * @param Timeout Timeout duration
 * @param Tickstart tick start value
 * @retval HAL status
 */
static HAL_StatusTypeDef __SPI_WaitFlagStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Flag, uint32_t State, uint32_t Timeout, uint32_t Tickstart)
{
    while ((((hspi->Instance->SPSR & Flag) == (Flag)) ? SET : RESET) != State) {
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0U) || ((HAL_GetTick() - Tickstart) >= Timeout)) {
                /* Disable the SPI and reset the CRC: the CRC value should be cleared
                   on both master and slave sides in order to resynchronize the master
                   and slave for their respective CRC calculation */

                /* Disable TXE, RXNE and ERR interrupts for the interrupt process */
                __HAL_SPI_DISABLE_IT(hspi, (SPI_IT_TXE | SPI_IT_RXNE | SPI_IT_ERR));

                if ((hspi->Init.Mode == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_1LINE)) {
                    /* Disable SPI peripheral */
                    __HAL_SPI_DISABLE(hspi);
                }

                hspi->State = HAL_SPI_STATE_READY;

                return (HAL_TIMEOUT);
            }
        }
    }

    return (HAL_OK);
}


/**
 * @brief Handle to check BSY flag before start a new transaction.
 * @param hspi pointer to a SPI_HandleTypeDef structure that contains
 *              the configuration information for SPI module.
 * @param Timeout Timeout duration
 * @param Tickstart tick start value
 * @retval HAL status
 */
static HAL_StatusTypeDef __SPI_CheckFlag_BUSY(SPI_HandleTypeDef *hspi, uint32_t Timeout, uint32_t Tickstart)
{
    /* Control the BSY flag */
    if (__SPI_WaitFlagStateUntilTimeout(hspi, SPI_FLAG_BUSY, RESET, Timeout, Tickstart) != HAL_OK) {
        SET_BIT(hspi->ErrorCode, HAL_SPI_ERROR_FLAG);
        return (HAL_TIMEOUT);
    }
    return (HAL_OK);
}


/**
 * @brief  Handle the end of the TX transaction.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
static void __SPI_CloseTx_ISR(SPI_HandleTypeDef *hspi)
{
    uint32_t        tickstart   = 0U;
    __IO uint32_t   count       = SPI_DEFAULT_TIMEOUT * (HAL_CMU_GetSysClock() / 24U / 1000U);

    /* Init tickstart for timeout management*/
    tickstart = HAL_GetTick();

    /* Wait until TXE flag is set */
    do {
        if (count-- == 0U) {
            SET_BIT(hspi->ErrorCode, HAL_SPI_ERROR_FLAG);
            break;
        }
    } while ((hspi->Instance->SPDF & SPI_FLAG_FIFO_TXE) == RESET);

    /* Disable TXE and ERR interrupt */
    __HAL_SPI_DISABLE_IT(hspi, (SPI_IT_TXE | SPI_IT_ERR));

    /* Check Busy flag */
    if (__SPI_CheckFlag_BUSY(hspi, SPI_DEFAULT_TIMEOUT, tickstart) != HAL_OK) {
        SET_BIT(hspi->ErrorCode, HAL_SPI_ERROR_FLAG);
    }

    /* Clear overrun flag in 2 Lines communication mode because received is not read */
    if (hspi->Init.Direction == SPI_DIRECTION_2LINES) {
        __HAL_SPI_CLEAR_OVRFLAG(hspi);
    }

    hspi->State = HAL_SPI_STATE_READY;
    if (hspi->ErrorCode != HAL_SPI_ERROR_NONE) {
        HAL_SPI_ErrorCallback(hspi);
    } else {
        HAL_SPI_TxCpltCallback(hspi);
    }
}


/**
 * @brief  Handle the end of the RX transaction.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
static void __SPI_CloseRx_ISR(SPI_HandleTypeDef *hspi)
{
    /* Disable RXNE and ERR interrupt */
    __HAL_SPI_DISABLE_IT(hspi, (SPI_IT_RXNE | SPI_IT_ERR));

    /* Check the end of the transaction */
    if ((hspi->Init.Mode == SPI_MODE_MASTER) && (hspi->Init.Direction == SPI_DIRECTION_1LINE)) {
        /* Disable SPI peripheral */
        __HAL_SPI_DISABLE(hspi);
    }

    /* Clear overrun flag in 2 Lines communication mode because received is not read */
    if (hspi->Init.Direction == SPI_DIRECTION_2LINES) {
        __HAL_SPI_CLEAR_OVRFLAG(hspi);
    }
    hspi->State = HAL_SPI_STATE_READY;

    if (hspi->ErrorCode == HAL_SPI_ERROR_NONE) {
        HAL_SPI_RxCpltCallback(hspi);
    } else {
        HAL_SPI_ErrorCallback(hspi);
    }
}


/**
 * @brief  Handle the end of the RXTX transaction.
 * @param  hspi pointer to a SPI_HandleTypeDef structure that contains
 *               the configuration information for SPI module.
 * @retval None
 */
static void __SPI_CloseRxTx_ISR(SPI_HandleTypeDef *hspi)
{
    uint32_t        tickstart   = 0U;
    __IO uint32_t   count       = SPI_DEFAULT_TIMEOUT * (HAL_CMU_GetSysClock() / 24U / 1000U);
    /* Init tickstart for timeout managment*/
    tickstart = HAL_GetTick();

    /* Disable ERR interrupt */
    __HAL_SPI_DISABLE_IT(hspi, SPI_IT_ERR);

    /* Wait until TXE flag is set */
    do {
        if (count-- == 0U) {
            SET_BIT(hspi->ErrorCode, HAL_SPI_ERROR_FLAG);
            break;
        }
    } while ((hspi->Instance->SPDF & SPI_FLAG_FIFO_TXE) == RESET);

    /* Check the end of the transaction */
    if (__SPI_CheckFlag_BUSY(hspi, SPI_DEFAULT_TIMEOUT, tickstart) != HAL_OK) {
        SET_BIT(hspi->ErrorCode, HAL_SPI_ERROR_FLAG);
    }

    /* Clear overrun flag in 2 Lines communication mode because received is not read */
    if (hspi->Init.Direction == SPI_DIRECTION_2LINES) {
        __HAL_SPI_CLEAR_OVRFLAG(hspi);
    }

    if (hspi->ErrorCode == HAL_SPI_ERROR_NONE) {
        if (hspi->State == HAL_SPI_STATE_BUSY_RX) {
            hspi->State = HAL_SPI_STATE_READY;
            HAL_SPI_RxCpltCallback(hspi);
        } else {
            hspi->State = HAL_SPI_STATE_READY;
            HAL_SPI_TxRxCpltCallback(hspi);
        }
    } else {
        hspi->State = HAL_SPI_STATE_READY;
        HAL_SPI_ErrorCallback(hspi);
    }
}


/**
 * @brief Initialize the SPI mode according to the specified parameters
 *         in the QSPI_InitTypeDef and initialize the associated handle.
 * @param SPI Instance
 * @retval None
 */
static void __SPI_Config(SPI_HandleTypeDef *hspi)
{
    /*----------------------- SPIx CR1 & CR2 Configuration ---------------------*/


    /* Configure : SPI Mode, Communication Mode, Data size, Clock polarity and phase, NSS management,
       Communication speed, First bit and CRC calculation state */
    MODIFY_REG(hspi->Instance->SPCR1, \
               SPI_SPCR1_MSTR | SPI_SPCR1_CPHA | SPI_SPCR1_CPOL | SPI_SPCR1_TBYTE | \
               SPI_SPCR1_LSBFIRST | SPI_SPCR1_SSNMCUEN | SPI_SPCR1_BR,
               hspi->Init.Mode |
               hspi->Init.ClkPhase |
               hspi->Init.ClkPolarity |
               hspi->Init.DataSize |
               hspi->Init.FirstBit |
               (hspi->Init.SSN & SPI_SPCR1_SSNMCUEN) |
               hspi->Init.BaudRatePrescaler);

    /* Configure : SSN management */
    MODIFY_REG(hspi->Instance->SPCR2, SPI_SPCR2_SSNMCU, (hspi->Init.SSN & SPI_SPCR2_SSNMCU));

    MODIFY_REG(hspi->Instance->SPCR3, SPI_SPCR3_TXONLYEN, hspi->Init.Direction);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
