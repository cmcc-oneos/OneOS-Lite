/**
 ******************************************************************************
 * @file    fm15f3xx_ll_mpu.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_hal.h"
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @addtogroup MPU_Private_Constants MPU Private Constants
 * @{
 */


/**
 * @}
 */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup MPU_Private_Functions MPU Private Functions
 * @{
 */


/**
 * @brief  Set each @ref MPU_SetRegionConfig field to default value.
 * @param  MPU_TypeDef pointer to a @ref regionConfig structure
 * whose fields will be set to default values.
 * @retval None
 */
void HAL_MPU_SetRegionConfig(MPU_TypeDef *base, HAL_MPU_RegionTypeDef *regionConfig)
{
    uint8_t regNumber = regionConfig->RegionNum;

    /* Check the parameters */
    assert_param(IS_MPU_ALL_INSTANCE(base));
    assert_param(IS_MPU_RIGHT(regionConfig->AccessRights1));
    assert_param(IS_MPU_RIGHT(regionConfig->AccessRights2));
    assert_param(IS_MPU_RIGHT(regionConfig->AccessRights3));

    base->CSR &= ~MPU_CSR_EN;
    /* The start and end address of the region descriptor. */
    base->REGION[regNumber].SADDR   = regionConfig->StartAddress;
    base->REGION[regNumber].EADDR   = regionConfig->EndAddress;
    base->REGION[regNumber].AUTHA   = regionConfig->AccessRights1;
    base->REGION[regNumber].AUTHB   = regionConfig->AccessRights2;
    base->REGION[regNumber].AUTHC   = regionConfig->AccessRights3;
    base->REGION[regNumber].CTRL    = regionConfig->SetCtrl;
    base->REGION[regNumber].SWCTL   = regionConfig->SetSwCtl;
    base->CSR                       |= MPU_CSR_EN;
}


/**
 * @brief  set MPU region address.
 * @param  None
 * @retval None
 */
void HAL_MPU_SetRegionAddr(MPU_TypeDef *base, uint32_t RegionNum, uint32_t StartAddr, uint32_t EndAddr)
{
    /* Check the parameters */
    assert_param(IS_MPU_ALL_INSTANCE(base));

    base->REGION[RegionNum].SADDR   = StartAddr;
    base->REGION[RegionNum].EADDR   = EndAddr;
}


/**
 * @brief  set MPU region access right.
 * @param  None
 * @retval None
 */
void HAL_MPU_SetRegionAccessRights(MPU_TypeDef *base, HAL_MPU_RightTypeDef *accessRights)
{
    uint8_t regionNum = accessRights->RegionNum;

    /* Check the parameters */
    assert_param(IS_MPU_ALL_INSTANCE(base));
    assert_param(IS_MPU_RIGHT(accessRights->AccessRights1));
    assert_param(IS_MPU_RIGHT(accessRights->AccessRights2));
    assert_param(IS_MPU_RIGHT(accessRights->AccessRights3));

    /* Set low master region access rights. */
    base->REGION[regionNum].AUTHA   = accessRights->AccessRights1;
    base->REGION[regionNum].AUTHB   = accessRights->AccessRights2;
    base->REGION[regionNum].AUTHC   = accessRights->AccessRights3;
    base->REGION[regionNum].CTRL    = accessRights->SetCtrl;
    base->REGION[regionNum].SWCTL   = accessRights->SetSwCtl;
}


/**
 * @brief  get MPU error access info.
 * @param  None
 * @retval None
 */
void HAL_MPU_GetErrorAccessInfo(MPU_TypeDef *base, uint32_t slaveNum, HAL_MPU_AccessErrTypeDef *errInform)
{
    /* Check the parameters */
    assert_param(IS_MPU_ALL_INSTANCE(base));

    errInform->Status = base->CSR;
    /* Error address. */
    errInform->Address = base->SP[slaveNum].EAR;
    /* Error detail information. */
    errInform->Details = base->SP[slaveNum].EDR;
}


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
