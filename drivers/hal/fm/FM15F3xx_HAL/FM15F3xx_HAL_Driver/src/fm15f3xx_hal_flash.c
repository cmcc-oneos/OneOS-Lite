/**
 ******************************************************************************
 * @file    fm15f3xx_hal_flash.c
 * @author  weifan
 * @version V1.0.0
 * @date    2020-09-04
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"
#include "fm15f3xx_hal_flash.h"


/** @addtogroup FM35F3xx_HAL_Driver
 * @{
 */


/** @defgroup FLASH FLASH
 * @brief FLASH HAL module driver
 * @{
 */
#ifdef FLASH_MODULE_ENABLED
/* Private define ------------------------------------------------------------*/


/** @addtogroup FLASH_Private_Constants
 * @{
 */
#if !defined (HAL_FLASH_OP_TIMEOUT)
#define HAL_FLASH_OP_TIMEOUT ( (uint32_t) 0xFFFFFFFF) /*!< Time out */
#endif /* HAL_FLASH_OP_TIMEOUT */


/**
 * @}
 */

/* Private function prototypes -----------------------------------------------*/


/** @addtogroup FLASH_Private_Functions
 * @{
 */
/* Program and erase operations */
static FLASH_Status __FLASH_Program_1Word(uint32_t Addr, uint32_t Data, uint8_t ReadCheckEn);


static FLASH_Status __FLASH_Program_4Word(uint32_t Addr, uint32_t Data_buf[4], uint8_t ReadCheckEn);


static FLASH_Status __FLASH_Program_32Word(uint32_t Addr, uint32_t Data_buf[32], uint8_t ReadCheckEn);


static FLASH_Status __FLASH_Erase_Sector(uint32_t Addr);


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup FLASH_Exported_Functions FLASH Exported Functions
 * @{
 */


/** @defgroup FLASH_Exported_Functions_Group1 Programming operation functions
 *  @brief   Programming operation functions
 *
   @verbatim
   ===============================================================================
 ##### Programming operation functions #####
   ===============================================================================
    [..]
    This subsection provides a set of functions allowing to manage the FLASH
    program operations.

   @endverbatim
 * @{
 */


/**
 * @brief  Modify data at a specified address
 * @param  Address specifies the address to be programmed.
 * @param  pData specifies the data to be programmed
 * @param  Len specifies the len of data
 * @param  SectorError pointer to variable that
 *                     contains the configuration information on faulty sector in case of error
 *                     (0xFFFFFFFFU means that all the data have been correctly modified)
 *
 * @retval FLASH_Status Flash Status
 */
FLASH_Status HAL_FLASH_Modify(uint32_t Address, uint8_t *pData, uint32_t Len, uint32_t *SectorError)
{
    FLASH_Status            status = FLASH_UNKNOWN;
    uint8_t                 sector[FLASH_SECTOR_SIZE];
    uint32_t                modify_start = Address;
    uint32_t                modify_end;
    uint32_t                modify_offset;
    uint32_t                modify_len;
    uint32_t                last_byte_address = Address + Len - 1;
    uint32_t                sector_begin_address;
    uint32_t                sector_tail_address;
    uint32_t                sector_number;
    uint32_t                i;
    uint32_t                j;
    FLASH_EraseParamTypeDef param;

    /* Initialization of SectorError variable */
    if (SectorError)
        *SectorError = 0xFFFFFFFFU;

    /* Check the parameters */
    assert_param(IS_FLASH_ADDRESS(Address));
    assert_param(IS_FLASH_ADDRESS(last_byte_address));

    sector_begin_address    = FLASH_ALIGN_ADDRESS(Address, FLASH_SECTOR_SIZE);
    sector_number           = FLASH_ADDRESS_TO_SECTOR(FLASH_ALIGN_ADDRESS(last_byte_address, FLASH_SECTOR_SIZE) - sector_begin_address) + 1;

    for (i = 0; i < sector_number; i++, modify_start = modify_end, sector_begin_address = sector_tail_address + 1) {
        sector_tail_address = sector_begin_address + FLASH_SECTOR_SIZE - 1;

        /* 计算扇区修改重叠区域 */
        modify_start    = FLASH_MAX(sector_begin_address, modify_start);
        modify_end      = FLASH_MIN(sector_tail_address, last_byte_address);
        modify_offset   = modify_start - sector_begin_address;
        modify_len      = modify_end - modify_start + 1;

        /* 缓存扇区数据，并更新用户数据 */
        for (j = 0; j < FLASH_SECTOR_SIZE; j++) {
            if ((j >= modify_offset) && (j < (modify_offset + modify_len)))
                sector[j] = pData[j - modify_offset];
            else
                sector[j] = ((uint8_t *) sector_begin_address)[j];
        }

        /* 擦除扇区 */
        param.InitSector    = FLASH_ADDRESS_TO_SECTOR(sector_begin_address);
        param.NumberSectors = 1;
        status              = HAL_FLASH_Erase(&param, SectorError);
        if (status != FLASH_COMPLETE)
            return (status);

        /* 写入数据 */
        status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_SECTOR, sector_begin_address, (uint32_t *) sector);
        if (status != FLASH_COMPLETE) {
            if (SectorError)
                *SectorError = FLASH_ADDRESS_TO_SECTOR(sector_begin_address);
            return (status);
        }
    }

    return (status);
}


/**
 * @brief  Program 1 word, 4 word or 32 word at a specified address
 * @param  TypeProgram Indicate the way to program at a specified address.
 *                     This parameter can be a value of @ref FLASH_ProgramTypeDef
 * @param  Address specifies the aligned address to be programmed, using FLASH_ALIGN_ADDRESS for alignment
 * @param  pData specifies the data to be programmed
 *
 * @retval FLASH_Status Flash Status
 */
FLASH_Status HAL_FLASH_Program(FLASH_ProgramTypeDef TypeProgram, uint32_t Address, uint32_t *pData)
{
    uint32_t        i;
    FLASH_Status    status = FLASH_UNKNOWN;

    /* Check the parameters */
    assert_param(IS_FLASH_TYPEPROGRAM(TypeProgram));
    assert_param(IS_FLASH_ADDRESS(Address));

    switch (TypeProgram) {
    case FLASH_TYPEPROGRAM_WORD:
        /* Program 1 word at a specified address. */
        status = __FLASH_Program_1Word(Address, *pData, LL_FLASH_READCHECK_ENABLE);
        break;
    case FLASH_TYPEPROGRAM_4WORD:
        /* Program 4 word at a specified address. */
        status = __FLASH_Program_4Word(Address, pData, LL_FLASH_READCHECK_ENABLE);
        break;
    case FLASH_TYPEPROGRAM_32WORD:
        /* Program 32 word at a specified address. */
        status = __FLASH_Program_32Word(Address, pData, LL_FLASH_READCHECK_ENABLE);
        break;
    case FLASH_TYPEPROGRAM_SECTOR:
        /* Program 1 sector at a specified address. */
        for (i = 0; i < FLASH_32WORDS_IN_SECTOR; i++) {
            status = __FLASH_Program_32Word(Address + i * FLASH_WORD_SIZE(32), pData + i * 32, LL_FLASH_READCHECK_ENABLE);
            if (status != FLASH_COMPLETE)
                break;
        }
        break;
    }

    return (status);
}


/**
 * @brief  Erase the specified FLASH memory sectors
 * @param  pEraseInit  pointer to an FLASH_EraseParamTypeDef structure that
 *                     contains the configuration information for the erasing.
 *
 * @param  SectorError pointer to variable that
 *                     contains the configuration information on faulty sector in case of error
 *                     (0xFFFFFFFFU means that all the sectors have been correctly erased)
 *
 * @retval FLASH_Status Flash Status
 */
FLASH_Status HAL_FLASH_Erase(FLASH_EraseParamTypeDef *pEraseInit, uint32_t *SectorError)
{
    FLASH_Status    status  = FLASH_UNKNOWN;
    uint32_t        index   = 0U;

    /* Initialization of SectorError variable */
    if (SectorError)
        *SectorError = 0xFFFFFFFFU;

    /* Check the parameters */
    assert_param(IS_FLASH_NUMBER_SECTORS(pEraseInit->NumberSectors));
    assert_param(IS_FLASH_SECTOR(pEraseInit->InitSector + pEraseInit->NumberSectors));

    /* Erase by sector by sector to be done */
    for (index = pEraseInit->InitSector; index < (pEraseInit->NumberSectors + pEraseInit->InitSector); index++) {
        status = __FLASH_Erase_Sector(FLASH_SECTOR_TO_ADDRESS(index));
        if (status != FLASH_COMPLETE) {
            /* In case of error, stop erase procedure and return the faulty sector */
            if (SectorError)
                *SectorError = index;
            break;
        }
    }

    return (status);
}


/**
 * @brief  Returns the FLASH Status.
 * @param  None
 * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_RDCOL, FLASH_ERROR_ACC,
 *                        FLASH_ERROR_AUTH, FLASH_ERROR_LVT, FLASH_ERROR_COMM, FLASH_UNKNOWN or FLASH_COMPLETE.
 */
static FLASH_Status __FLASH_GetStatus(void)
{
    FLASH_Status flashstatus = FLASH_COMPLETE;

    if ((FLASH->CMDSTAT & LL_FLASH_DONE_FINISH) == 0x00) {
        flashstatus = FLASH_BUSY;
    } else if ((FLASH->CMDSTAT & LL_FLASH_RESULT_FAIL) != (uint32_t) 0x00) {
        if ((FLASH->CMDSTAT & LL_FLASH_ERROR_ACC) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_ACC;
        } else if ((FLASH->CMDSTAT & LL_FLASH_ERROR_AUTH) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_AUTH;
        } else if ((FLASH->CMDSTAT & LL_FLASH_ERROR_LVT) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_LVT;
        } else if ((FLASH->CMDSTAT & LL_FLASH_ERROR_COMM) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_COMM;
        } else {
            flashstatus = FLASH_UNKNOWN;
        }
    } else {
        flashstatus = FLASH_COMPLETE;
    }

    /* Return the FLASH Status */
    return (flashstatus);
}


/**
 * @brief  Waits for a FLASH operation to complete.
 * @param  None
 * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_PROGRAM,
 *                       FLASH_ERROR_WRP, FLASH_ERROR_OPERATION or FLASH_COMPLETE.
 */
static FLASH_Status __FLASH_WaitForLastOperation(void)
{
    __IO FLASH_Status   status          = FLASH_COMPLETE;
    __IO uint32_t       TimeOutCounter  = 0;


    /* Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
       Even if the FLASH operation fails, the BUSY flag will be reset and an error
       flag will be set */
    do {
        status = __FLASH_GetStatus();
        TimeOutCounter++;
    } while ((status == FLASH_BUSY) && (TimeOutCounter != HAL_FLASH_OP_TIMEOUT));

    if (TimeOutCounter == HAL_FLASH_OP_TIMEOUT)
        status = FLASH_TIMEOUT;
    /* Return the operation status */
    return (status);
}


/**
 * @brief  Program word (32-bit) at a specified address.
 *
 * @param  Addr specifies the address to be programmed.
 * @param  Data specifies the data to be programmed.
 * @param  ReadCheckEn whether check value after programming.
 * @retval FLASH_Status
 */
static FLASH_Status __FLASH_Program_1Word(uint32_t Addr, uint32_t Data, uint8_t ReadCheckEn)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint32_t            * bp    = (uint32_t *) Addr;

    LL_FLASH_SetProgramSize(FLASH, LL_FLASH_PROGRAMSIZE_1WORD);
    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_PROGRAMCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    LL_FLASH_SetCMDPARAn(FLASH, 0, Data);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = __FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    if (status == FLASH_COMPLETE) {
        if (ReadCheckEn == LL_FLASH_READCHECK_ENABLE) {
            if ((*bp) != Data)
                status = FLASH_ERROR_NORAML_READ;
        }
    }

    return (status);
}


/**
 * @brief  Program 4 word at a specified address.
 *
 * @param  Addr specifies the address to be programmed.
 * @param  Data_buf specifies the data to be programmed.
 * @param  ReadCheckEn whether check value after programming.
 * @retval FLASH_Status
 */
static FLASH_Status __FLASH_Program_4Word(uint32_t Addr, uint32_t Data_buf[4], uint8_t ReadCheckEn)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint8_t             i       = 0;
    uint32_t            * bp    = (uint32_t *) Addr;

    LL_FLASH_SetProgramSize(FLASH, LL_FLASH_PROGRAMSIZE_4WORD);
    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_PROGRAMCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    LL_FLASH_SetCMDPARAn(FLASH, 0, Data_buf[0]);
    LL_FLASH_SetCMDPARAn(FLASH, 1, Data_buf[1]);
    LL_FLASH_SetCMDPARAn(FLASH, 2, Data_buf[2]);
    LL_FLASH_SetCMDPARAn(FLASH, 3, Data_buf[3]);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = __FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    if (status == FLASH_COMPLETE) {
        if (ReadCheckEn == LL_FLASH_READCHECK_ENABLE) {
            for (i = 0; i < 4; i++) {
                if ((*(bp + i)) != Data_buf[i]) {
                    status = FLASH_ERROR_NORAML_READ;
                    break;
                }
            }
        }
    }

    return (status);
}


/**
 * @brief  Program 32 word at a specified address.
 *
 * @param  Addr specifies the address to be programmed.
 * @param  Data_buf specifies the data to be programmed.
 * @param  ReadCheckEn whether check value after programming.
 * @retval FLASH_Status
 */
static FLASH_Status __FLASH_Program_32Word(uint32_t Addr, uint32_t Data_buf[32], uint8_t ReadCheckEn)
{
    __IO FLASH_Status   status = FLASH_COMPLETE;
    uint32_t            i;
    uint32_t            * bp = (uint32_t *) Addr;

    LL_FLASH_SetProgramSize(FLASH, LL_FLASH_PROGRAMSIZE_32WORD);
    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_PROGRAMCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    for (i = 0; i < 32; i++) {
        LL_FLASH_SetCMDPARAn(FLASH, i, Data_buf[i]);
    }
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = __FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    if (status == FLASH_COMPLETE) {
        if (ReadCheckEn == LL_FLASH_READCHECK_ENABLE) {
            for (i = 0; i < 32; i++) {
                if ((*(bp + i)) != Data_buf[i]) {
                    status = FLASH_ERROR_NORAML_READ;
                    break;
                }
            }
        }
    }

    return (status);
}


/**
 * @brief  Erase 1 sector in single mode.
 * @param  Addr address of the sector to be erased, address will be aligned in
            sector size by hardware.
 * @retval FLASH_Status
 */
static FLASH_Status __FLASH_Erase_Sector_Single(uint32_t Addr)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint32_t            * bp    = (uint32_t *) Addr;
    volatile uint32_t   tmp;

    LL_FLASH_SetEraseVReadl(FLASH, LL_FLASH_ERASEVREADL_VERIFY);   //Check empty after erase
    LL_FLASH_SetVRDVread(FLASH, LL_FLASH_VRDVREAD_DISABLE);
    LL_FLASH_SetEraseRetry(FLASH, LL_FLASH_ERASERETRY(0x3));
    LL_FLASH_SetEraseWay(FLASH, LL_FLASH_ERASEWAY_SINGLE);

    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_EREASCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);

    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = __FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    return (status);
}


/**
 * @brief  Erase 1 sector in retry mode.
 * @param  Addr address of the sector to be erased, address will be aligned in
            sector size by hardware.
 * @retval FLASH_Status
 */
static FLASH_Status __FLASH_Erase_Sector(uint32_t Addr)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint32_t            * bp    = (uint32_t *) Addr;
    uint32_t            i;
    volatile uint32_t   tmp;

    LL_FLASH_SetEraseVReadl(FLASH, LL_FLASH_ERASEVREADL_VERIFY);    //Check empty after erase
    LL_FLASH_SetVRDVread(FLASH, LL_FLASH_VRDVREAD_DISABLE);
    LL_FLASH_SetEraseWay(FLASH, LL_FLASH_ERASEWAY_RETRY);           //LL_FLASH_EraseWay_Retry;LL_FLASH_EraseWay_Single

    for (i = 0; i < 3; i++) {
        LL_FLASH_SetEraseRetry(FLASH, LL_FLASH_ERASERETRY(i));

        LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
        LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_EREASCMD);
        LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
        LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
        LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);

        *bp = 0x0;
        __DSB();

        /* Wait till Flash operation is completed and if Time out is reached exit */
        status = __FLASH_WaitForLastOperation();

        /* Flush the data cache */
        LL_Data_Cache_Flush();

        if (FLASH_COMPLETE == status)
            return (status);
    }

    status = __FLASH_Erase_Sector_Single(Addr);

    return (status);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* HAL_FLASH_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
