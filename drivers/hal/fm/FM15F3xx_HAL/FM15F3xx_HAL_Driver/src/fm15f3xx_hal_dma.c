/**
 ******************************************************************************
 * @file    fm15f3xx_hal_dma.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-04-24
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#include "fm15f3xx_hal.h"

#ifdef DMA_MODULE_ENABLED

/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/


/** @addtogroup DMA_Private_Constants
 * @{
 */
#define HAL_TIMEOUT_DMA_ABORT 5U /* 5 ms */


/**
 * @}
 */
/* Private macros ------------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/** @addtogroup DMA_Private_Functions
 * @{
 */
static void __DMA_Config(DMA_HandleTypeDef *hdma);


/**
 * @}
 */

/* Exported functions ---------------------------------------------------------*/


/** @addtogroup DMA_Exported_Functions
 * @{
 */


/** @addtogroup DMA_Exported_Functions_Group1
 *
   @verbatim
   ===============================================================================
 ##### Initialization and de-initialization functions  #####
   ===============================================================================
    [..]
    This section provides functions allowing to initialize the DMA Stream source
    and destination addresses, incrementation and data sizes, transfer direction,
    circular/normal mode selection, memory-to-memory mode selection and Stream priority value.
    [..]
    The HAL_DMA_Init() function follows the DMA configuration procedures as described in
    reference manual.

   @endverbatim
 * @{
 */


/**
 * @brief  Initialize the DMA according to the specified
 *         parameters in the DMA_InitTypeDef and create the associated handle.
 * @param  hdma Pointer to a DMA_HandleTypeDef structure that contains
 *         the configuration information for the specified DMA Stream.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DMA_Init(DMA_HandleTypeDef *hdma)
{
    /* Check the DMA peripheral state */
    if (hdma == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_DMA_ALL_INSTANCE(hdma->Instance));
    assert_param(IS_DMA_CHANNEL(hdma->Init.Channel));
    assert_param(IS_DMA_DATA_WIDTH(hdma->Init.Tcd.SrcDataWidth));
    assert_param(IS_DMA_DATA_WIDTH(hdma->Init.Tcd.DstDataWidth));
    assert_param(IS_DMA_BURST(hdma->Init.Tcd.SrcBurst));
    assert_param(IS_DMA_BURST(hdma->Init.Tcd.DstBurst));
    assert_param(IS_DMA_OFFSET(hdma->Init.Tcd.SrcOffset));
    assert_param(IS_DMA_OFFSET(hdma->Init.Tcd.DstOffset));
    assert_param(IS_DMA_PRIORITY(hdma->Init.Tcd.Priority));
    assert_param(IS_DMA_TRIGSRC(hdma->Init.TrigSrc));
    assert_param(IS_DMA_MINOR_OFFSET_MODE(hdma->Init.MinorLoop.OffsetMode));

    /* Change DMA peripheral state */
    hdma->State = HAL_DMA_STATE_BUSY;

    /* Disable the peripheral */
    __HAL_DMA_MUX_DISABLE(hdma);

    /* Config DMA */
    __DMA_Config(hdma);

    /* Initialize the error code */
    hdma->ErrorCode = HAL_DMA_ERROR_NONE;

    /* Initialize the DMA state */
    hdma->State = HAL_DMA_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  Starts the DMA Transfer.
 * @param  hdma   pointer to a DMA_HandleTypeDef structure that contains
 *                the configuration information for the specified DMA Stream.
 * @param  SrcAddress   The source memory Buffer address
 * @param  DstAddress   The destination memory Buffer address
 * @param  MajorLoopNum The Number of MajorLoop
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DMA_Start(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum)
{
    /* Check the parameters */
    assert_param(IS_DMA_ALL_INSTANCE(hdma->Instance));

    /* Check DMA is Free*/
    if (__HAL_DMA_GET_ACTIVE(hdma)) {
        /* Return error status */
        return (HAL_BUSY);
    }

    /* Clear the transfer complete flag */
    __HAL_DMA_CLEAR_DONE(hdma);

    /* Clear the transfer complete int */
    __HAL_DMA_CLEAR_FLAG_IT(hdma);

    /* Clear the transfer error flag*/
    __HAL_DMA_CLEAR_ERR(hdma);

    /* Initialize the error code */
    hdma->ErrorCode = HAL_DMA_ERROR_NONE;

    /* Configure Address */
    hdma->Instance->TCD[hdma->Init.Channel].SADDR   = SrcAddress;
    hdma->Instance->TCD[hdma->Init.Channel].DADDR   = DstAddress;

    /* MajorLoop Num*/
    hdma->Instance->TCD[hdma->Init.Channel].BITER_CITER_ELINKNO = (MajorLoopNum & 0x7fff) | ((MajorLoopNum & 0x7fff) << 16);

    if (hdma->Init.TrigSrc != DMA_TRIG_SOFTWARE) {
        /* Enable Reset Trig after Major Loop*/
        hdma->Instance->TCD[hdma->Init.Channel].CSR |= DMA_WORD6_DREQ;
        /* Enable Trig*/
        hdma->Instance->ERQ |= (0x1 << hdma->Init.Channel);

        /* Enable the Peripheral */
        __HAL_DMA_MUX_ENABLE(hdma);
    }

    return (HAL_OK);
}


/**
 * @brief  Start the DMA Transfer with interrupt enabled.
 * @param  hdma  pointer to a DMA_HandleTypeDef structure that contains
 *               the configuration information for the specified DMA Stream.
 * @param  SrcAddress   The source memory Buffer address
 * @param  DstAddress   The destination memory Buffer address
 * @param  MajorLoopNum The Number of MajorLoop
 * @param  IT    specifies the DMA interrupt sources to be enabled or disabled.
 *               This parameter can be any combination of the following values:
 *               @arg DMA_IT_MAJORLOOP_TC: Transfer complete interrupt.
 *               @arg DMA_IT_MAJORLOOP_HT: Half transfer complete interrupt.
 *               @arg DMA_IT_ERR: Transfer error interrupt.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DMA_Start_IT(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum, uint32_t IT)
{
    /* Check the parameters */
    assert_param(IS_DMA_ALL_INSTANCE(hdma->Instance));

    /* Check DMA is Free*/
    if (__HAL_DMA_GET_ACTIVE(hdma)) {
        /* Return error status */
        return (HAL_BUSY);
    }
    /* Clear the transfer complete flag */
    __HAL_DMA_CLEAR_DONE(hdma);

    /* Clear the transfer complete int */
    __HAL_DMA_CLEAR_FLAG_IT(hdma);

    /* Clear the transfer error flag*/
    __HAL_DMA_CLEAR_ERR(hdma);

    /* Initialize the error code */
    hdma->ErrorCode = HAL_DMA_ERROR_NONE;

    /* Configure Address */
    hdma->Instance->TCD[hdma->Init.Channel].SADDR   = SrcAddress;
    hdma->Instance->TCD[hdma->Init.Channel].DADDR   = DstAddress;

    /* MajorLoop Num*/
    hdma->Instance->TCD[hdma->Init.Channel].BITER_CITER_ELINKNO = (MajorLoopNum & 0x7fff) | ((MajorLoopNum & 0x7fff) << 16);

    /* Clear all interrupt flags at correct offset within the register */
    __HAL_DMA_DISABLE_IT(hdma, DMA_IT_ERR);
    __HAL_DMA_DISABLE_IT(hdma, DMA_IT_TC | DMA_IT_MAJORLOOP_HT);

    /* Enable Common interrupts*/
    if (IT & DMA_IT_MAJORLOOP_MASK) {
        __HAL_DMA_ENABLE_IT(hdma, IT & DMA_IT_MAJORLOOP_MASK);
    }
    /* Enable Error interrupts*/
    if (IT & DMA_IT_ERR_MASK) {
        __HAL_DMA_ENABLE_IT(hdma, IT & DMA_IT_ERR_MASK);
    }

    if (hdma->Init.TrigSrc != DMA_TRIG_SOFTWARE) {
        /* Enable Reset Trig after Major Loop*/
        hdma->Instance->TCD[hdma->Init.Channel].CSR |= DMA_WORD6_DREQ;
        /* Enable Trig*/
        hdma->Instance->ERQ |= (0x1 << hdma->Init.Channel);

        /* Enable the Peripheral */
        __HAL_DMA_MUX_ENABLE(hdma);
    }


    return (HAL_OK);
}


/**
 * @brief  Aborts the DMA Transfer.
 * @param  hdma   pointer to a DMA_HandleTypeDef structure that contains
 *                 the configuration information for the specified DMA Stream.
 *
 * @note  After disabling a DMA Stream,the current data will be transferred
 *        and the Stream will be effectively disabled only after the transfer of
 *        this minorloop is finished.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_DMA_Abort(DMA_HandleTypeDef *hdma)
{
    uint32_t tickstart = HAL_GetTick();

    if (hdma->State != HAL_DMA_STATE_BUSY) {
        return (HAL_ERROR);
    } else {
        /* Disable all the transfer interrupts */
        __HAL_DMA_DISABLE_IT(hdma, DMA_IT_ERR);
        __HAL_DMA_DISABLE_IT(hdma, DMA_IT_TC | DMA_IT_MAJORLOOP_HT);

        /* Abort the stream */
        __HAL_DMA_ABORT(hdma);

        /* Clear all interrupt flags at correct offset within the register */
        __HAL_DMA_CLEAR_FLAG_IT(hdma);
    }
    return (HAL_OK);
}


/**
 * @brief  Handles DMA interrupt request.
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *               the configuration information for the specified DMA Stream.
 * @retval None
 */
void HAL_DMA_IRQHandler(DMA_HandleTypeDef *hdma)
{
    if (__HAL_DMA_GET_FLAG_IT(hdma) != 0) {
        /* Clear the transfer half or complete int */
        __HAL_DMA_CLEAR_FLAG_IT(hdma);
        /* Half Transfer Complete Interrupt management ******************************/
        if (__HAL_DMA_GET_DONE(hdma) == 0) {
            /* Half transfer callback */
            HAL_DMA_XferHalfCpltCallback(hdma);
        }
        /* Transfer Complete Interrupt management ***********************************/
        else if (__HAL_DMA_GET_DONE(hdma) != 0) {
            /* Clear the transfer complete flag */
            __HAL_DMA_CLEAR_DONE(hdma);
            /* Transfer complete callback */
            HAL_DMA_XferCpltCallback(hdma);
        }
    }
}


/**
 * @brief  Handles DMA interrupt request.
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *               the configuration information for the specified DMA Stream.
 * @retval None
 */
void HAL_DMA_Error_IRQHandler(DMA_HandleTypeDef *hdma)
{
    /* Transfer Error Interrupt management ***************************************/
    if (__HAL_DMA_GET_ERR(hdma) != 0) {
        /*Error Code*/
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_BUS)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_BUS;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_SG)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_SG;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_MAJORLOOPNUM)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_MAJORLOOPNUM;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_DSTOFFSET)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_DSTOFFSET;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_DSTADDR)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_DSTADDR;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_SRCOFFSET)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_SRCOFFSET;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_SRCADDR)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_SRCADDR;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_PRIOR)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_PRIOR;
        }
        if (__HAL_DMA_GET_FLAG_ERR(hdma, DMA_FLAG_ERR_CANCELTX)) {
            hdma->ErrorCode |= HAL_DMA_ERROR_CANCELTX;
        }

        /* Clear the transfer error flag*/
        __HAL_DMA_CLEAR_ERR(hdma);

        /* Transfer error callback */
        HAL_DMA_ErrorCallback(hdma);
    }
}


/**
 * @brief  MajorLoop Xfer complete callback in non blocking mode
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval None
 */
__weak void HAL_DMA_XferCpltCallback(DMA_HandleTypeDef *hdma)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hdma);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DMA_MajorLoopXferCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  MajorLoop Xfer Half complete callback in non blocking mode
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval None
 */
__weak void HAL_DMA_XferHalfCpltCallback(DMA_HandleTypeDef *hdma)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hdma);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DMA_MajorLoopXferHalfCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Error callback in non blocking mode
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval None
 */
__weak void HAL_DMA_ErrorCallback(DMA_HandleTypeDef *hdma)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hdma);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DMA_ErrorCallback could be implemented in the user file
     */
}


/**
 * @brief  Returns the DMA state.
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *               the configuration information for the specified DMA Stream.
 * @retval HAL state
 */
HAL_DMA_StateTypeDef HAL_DMA_GetState(DMA_HandleTypeDef *hdma)
{
    return (hdma->State);
}


/**
 * @brief  Return the DMA error code
 * @param  hdma  pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval DMA Error Code
 */
uint32_t HAL_DMA_GetError(DMA_HandleTypeDef *hdma)
{
    return (hdma->ErrorCode);
}


/**
 * @brief  Get MajorLoop number of remaining.
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval remaining Number
 */
uint32_t HAL_DMA_GetMajorLoopCiter(DMA_HandleTypeDef *hdma)
{
    /* Check the parameters */
    assert_param(IS_DMA_ALL_INSTANCE(hdma->Instance));

    return ((hdma->Instance->TCD[hdma->Init.Channel].BITER_CITER_ELINKNO & 0x7FFF0000) >> 16);
}


/**
 * @brief  SoftWare Trig the DMA Transfer.
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval HAL  status
 */
HAL_StatusTypeDef HAL_DMA_Trig_SoftWare(DMA_HandleTypeDef *hdma)
{
    /* Check the parameters */
    assert_param(IS_DMA_ALL_INSTANCE(hdma->Instance));

    /* Check DMA is Free*/
    if (__HAL_DMA_GET_ACTIVE(hdma)) {
        /* Return error status */
        return (HAL_BUSY);
    }

    hdma->Instance->TCD[hdma->Init.Channel].CSR |= DMA_WORD6_START;

    return (HAL_OK);
}


/** @addtogroup DMA_Private_Functions
 * @{
 */


/**
 * @brief  Sets the DMA .
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval None
 */
static void __DMA_Config(DMA_HandleTypeDef *hdma)
{
    /* Clear Reg */
    hdma->Instance->CR      &= ~(DMA_CR_HOE | DMA_CR_HALT | DMA_CR_CLM);
    hdma->Instance->ERQ     &= ~(0x1 << hdma->Init.Channel);
    hdma->Instance->EEI     &= ~(0x1 << hdma->Init.Channel);
    hdma->Instance->INT     &= ~(0x1 << hdma->Init.Channel);
    hdma->Instance->ERR     &= ~(0x1 << hdma->Init.Channel);
    hdma->Instance->EARS    &= ~(0x1 << hdma->Init.Channel);
    hdma->Instance->DONE    &= ~(0x1 << hdma->Init.Channel);
    /* Configure Priority */
    if (hdma->Init.Tcd.Priority != DMA_PRIORITY_POLLING) {
        hdma->Instance->CR      |= DMA_CR_ERCA;
        hdma->Instance->CHPRI   |= hdma->Init.Tcd.Priority << (hdma->Init.Channel * 4);
    } else {
        hdma->Instance->CR &= (~DMA_CR_ERCA);
    }
    /* Configure TCD Word1 */
    hdma->Instance->TCD[hdma->Init.Channel].ATTR = DMA_ATTR_DBURST(hdma->Init.Tcd.DstBurst) | DMA_ATTR_DSIZE(hdma->Init.Tcd.DstDataWidth) | \
            DMA_ATTR_SBURST(hdma->Init.Tcd.SrcBurst) | DMA_ATTR_SSIZE(hdma->Init.Tcd.SrcDataWidth);
    hdma->Instance->TCD[hdma->Init.Channel].SOFF    = hdma->Init.Tcd.SrcOffset;
    hdma->Instance->TCD[hdma->Init.Channel].DOFF    = hdma->Init.Tcd.DstOffset;

    /* Configure Address */
    hdma->Instance->TCD[hdma->Init.Channel].SADDR   = hdma->Init.Tcd.SrcAddress;
    hdma->Instance->TCD[hdma->Init.Channel].DADDR   = hdma->Init.Tcd.DstAddress;

    /* MinorLoop */
    if (hdma->Init.MinorLoop.OffsetMode == DMA_MINORLOOP_OFFSET_DISABLE) {
        hdma->Instance->CR                                  &= ~DMA_CR_EMLM;
        hdma->Instance->TCD[hdma->Init.Channel].NBYTES_MLNO = hdma->Init.MinorLoop.BytesNum;
    } else {
        assert_param(IS_DMA_MINLOOPOFFSET(hdma->Init.MinorLoop.Offset));
        assert_param(IS_DMA_MINLOOPTRANSBYTES(hdma->Init.MinorLoop.BytesNum));
        hdma->Instance->CR                                      |= DMA_CR_EMLM;
        hdma->Instance->TCD[hdma->Init.Channel].NBYTES_MLOFFYES = ((hdma->Init.MinorLoop.BytesNum & 0x3ff) | ((hdma->Init.MinorLoop.Offset & 0xFFFFF) << 10) | \
                hdma->Init.MinorLoop.OffsetMode);
    }

    /* MajorLoop */
    assert_param(IS_DMA_MAJLOOPOFFSET(hdma->Init.MajorLoop.DstOffset));
    assert_param(IS_DMA_MAJLOOPOFFSET(hdma->Init.MajorLoop.SrcOffset));
    assert_param(IS_DMA_MAJLOOPTRANSBYTES(hdma->Init.MajorLoop.Num));
    hdma->Instance->TCD[hdma->Init.Channel].BITER_CITER_ELINKNO = (hdma->Init.MajorLoop.Num & 0x7fff) | ((hdma->Init.MajorLoop.Num & 0x7fff) << 16);
    hdma->Instance->TCD[hdma->Init.Channel].w3.DLAST                = hdma->Init.MajorLoop.DstOffset;
    hdma->Instance->TCD[hdma->Init.Channel].w3.SLAST                = hdma->Init.MajorLoop.SrcOffset;

    DMAMUX->CHCFG[hdma->Init.Channel] = hdma->Init.TrigSrc;
}


/**
 * @}
 */

#endif /* DMA_MODULE_ENABLED */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
