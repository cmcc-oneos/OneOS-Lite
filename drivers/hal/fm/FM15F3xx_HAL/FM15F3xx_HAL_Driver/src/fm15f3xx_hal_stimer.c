/**
 ******************************************************************************
 * @file    fm15f3xx_hal_stimer.c
 * @author  WYL
 * @version V1.0.0
 * @date    2020-06-29
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup TIM TIM
 * @brief TIM HAL module driver
 * @{
 */

#ifdef STIM_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/** @addtogroup TIM_Private_Functions
 * @{
 */
/* Private function prototypes -----------------------------------------------*/
static void __TIM_Config(ST_TypeDef *TIMx, TIM_Base_InitTypeDef *Structure);


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup TIM_Exported_Functions TIM Exported Functions
 * @{
 */


/** @defgroup TIM_Exported_Functions_Group1 Time Base functions
 *  @brief    Time Base functions
 *
   @verbatim
   ==============================================================================
 ##### Time Base functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the TIM Time base Unit according to the specified
 *         parameters in the TIM_HandleTypeDef and create the associated handle.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_Init(TIM_HandleTypeDef *htim)
{
    /* Check the TIM handle allocation */
    if (htim == NULL) {
        return (HAL_ERROR);
    }
    /* Check the TIMx Enable */
    if ((1 << (htim->Init.STx * 4)) & STIMER->CTRL) {
        return (HAL_BUSY);
    }

    /* Check the parameters */
    assert_param(IS_TIM_INSTANCE(htim->Init.STx));
    assert_param(IS_TIME_WORK_MODE(htim->Init.WorkMode));
    assert_param(IS_TIME_COUNTER_MODE(htim->Init.CountMode));
    assert_param(IS_TIM_WORKCLK_DIV(htim->Init.Prescaler));
    assert_param(IS_TIM_AUTORELOAD(htim->Init.AutoReload0));
    if (TIM_WORKMODE_PWM == htim->Init.WorkMode) {
        assert_param(IS_TIM_AUTORELOAD(htim->Init.AutoReload1));
    }

    if (htim->State == HAL_TIM_STATE_RESET) {
        /* Init the low level hardware : GPIO, CLOCK, NVIC */
        HAL_TIM_Base_MspInit(htim);
    }

    /* Set the TIM state */
    htim->State = HAL_TIM_STATE_BUSY;

    /* Set the Time Base configuration */
    __TIM_Config(STIMER, &htim->Init);

    /* Initialize the TIM state*/
    htim->State = HAL_TIM_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  DeInitializes the TIM Base peripheral
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_DeInit(TIM_HandleTypeDef *htim)
{
    htim->State = HAL_TIM_STATE_BUSY;

    /* Disable the TIM Peripheral Clock */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* DeInit the low level hardware: GPIO, CLOCK, NVIC */
    HAL_TIM_Base_MspDeInit(htim);

    /* Change TIM state */
    htim->State = HAL_TIM_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief  Initializes the TIM Base MSP.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_TIM_Base_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes TIM Base MSP.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_TIM_Base_MspDeInit could be implemented in the user file
     */
}


/**
 * @brief  Starts the TIM Base generation.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_Start(TIM_HandleTypeDef *htim)
{
    /* Set the TIM state */
    htim->State = HAL_TIM_STATE_BUSY;

    /* Enable the Peripheral */
    __HAL_TIM_ENABLE(htim->Init.STx);

    /* Change the TIM state*/
    htim->State = HAL_TIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the TIM Base generation.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_Stop(TIM_HandleTypeDef *htim)
{
    /* Set the TIM state */
    htim->State = HAL_TIM_STATE_BUSY;

    /* Disable the Peripheral */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* Change the TIM state*/
    htim->State = HAL_TIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the TIM Base generation in interrupt mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_Start_IT(TIM_HandleTypeDef *htim)
{
    /* Enable the TIM Update interrupt */
    __HAL_TIM_ENABLE_IT(htim->Init.STx);

    /* Enable the Peripheral */
    __HAL_TIM_ENABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the TIM Base generation in interrupt mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_Stop_IT(TIM_HandleTypeDef *htim)
{
    /* Disable the TIM Update interrupt */
    __HAL_TIM_DISABLE_IT(htim->Init.STx);

    /* Disable the Peripheral */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the TIM Base generation in DMA mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_Start_DMA(TIM_HandleTypeDef *htim)
{
    /* Enable the TIM Update interrupt */
    __HAL_TIM_ENABLE_DMA(htim->Init.STx);

    /* Enable the Peripheral */
    __HAL_TIM_ENABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the TIM Base generation in DMA mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Base_Stop_DMA(TIM_HandleTypeDef *htim)
{
    /* Disable the TIM Update interrupt */
    __HAL_TIM_DISABLE_DMA(htim->Init.STx);

    /* Disable the Peripheral */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup TIM_Exported_Functions_Group2 Time Trigger functions
 *  @brief    Time Input Capture functions
 *
   @verbatim
   ==============================================================================
 ##### Time Trigger functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the TIM TGS base according to the specified
 *         parameters in the TIM_HandleTypeDef and create the associated handle.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_TGS_Init(TIM_HandleTypeDef *htim)
{
    /* Check the TIM handle allocation */
    if (htim == NULL) {
        return (HAL_ERROR);
    }

    /* Check the TIMx Enable */
    if ((1 << (htim->Init.STx * 4)) & STIMER->CTRL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_TIM_INSTANCE(htim->Init.STx));
    assert_param(IS_TIME_WORK_MODE(htim->Init.WorkMode));
    assert_param(IS_TIME_COUNTER_MODE(htim->Init.CountMode));
    assert_param(IS_TIM_WORKCLK_DIV(htim->Init.Prescaler));
    assert_param(IS_TIM_AUTORELOAD(htim->Init.AutoReload0));

    if (htim->State == HAL_TIM_STATE_RESET) {
        /* Init the low level hardware : GPIO, CLOCK, NVIC and DMA */
        HAL_TIM_TGS_MspInit(htim);
    }

    /* Set the TIM state */
    htim->State = HAL_TIM_STATE_BUSY;

    /* Init the base time for the input capture */
    __TIM_Config(STIMER, &htim->Init);

    /* Initialize the TIM state*/
    htim->State = HAL_TIM_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  DeInitializes the TIM TGS peripheral
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_TGS_DeInit(TIM_HandleTypeDef *htim)
{
    htim->State = HAL_TIM_STATE_BUSY;

    /* Disable the TIM Peripheral Clock */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* DeInit the low level hardware: GPIO, CLOCK, NVIC and DMA */
    HAL_TIM_TGS_MspDeInit(htim);

    /* Change TIM state */
    htim->State = HAL_TIM_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief  Initializes the TIM TGS MSP.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_TGS_MspInit(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_TIM_IC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes TIM TGS MSP.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_TGS_MspDeInit(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_TIM_IC_MspDeInit could be implemented in the user file
     */
}


/**
 * @brief  Starts the TIM Trigger.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_TRIG_Start(TIM_HandleTypeDef *htim)
{
    /* Enable the Trigger */
    __HAL_TIM_ENABLE_TRIG(htim->Init.STx);

    /* Enable the Peripheral */
    __HAL_TIM_ENABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the TIM Trigger.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_TRIG_Stop(TIM_HandleTypeDef *htim)
{
    /* Disable the Input Capture channel */
    __HAL_TIM_DISABLE_TRIG(htim->Init.STx);

    /* Disable the Peripheral */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the TIM Trigger in interrupt mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_TRIG_Start_IT(TIM_HandleTypeDef *htim)
{
    /* Enable the TIM Update interrupt */
    __HAL_TIM_ENABLE_IT(htim->Init.STx);

    /* Enable the Trigger */
    __HAL_TIM_ENABLE_TRIG(htim->Init.STx);

    /* Enable the Peripheral */
    __HAL_TIM_ENABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the TIM Trigger in interrupt mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_TRIG_Stop_IT(TIM_HandleTypeDef *htim)
{
    /* Disable the TIM Update interrupt */
    __HAL_TIM_DISABLE_IT(htim->Init.STx);

    /* Disable the Trigger */
    __HAL_TIM_DISABLE_TRIG(htim->Init.STx);

    /* Disable the Peripheral */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the TIM FAULT.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Fault_Start(TIM_HandleTypeDef *htim)
{
    /* Enable the FAULT */
    __HAL_TIM_ENABLE_FAULT(htim->Init.STx);

    /* Enable the Peripheral */
    __HAL_TIM_ENABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the TIM FAULT.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Fault_Stop(TIM_HandleTypeDef *htim)
{
    /* Disable the FAULT */
    __HAL_TIM_DISABLE_FAULT(htim->Init.STx);

    /* Disable the Peripheral */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the TIM FAULT in interrupt mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Fault_Start_IT(TIM_HandleTypeDef *htim)
{
    /* Enable the TIM Update interrupt */
    __HAL_TIM_ENABLE_IT(htim->Init.STx);

    /* Enable the FAULT */
    __HAL_TIM_ENABLE_FAULT(htim->Init.STx);

    /* Enable the Peripheral */
    __HAL_TIM_ENABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the TIM FAULT in interrupt mode.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Fault_Stop_IT(TIM_HandleTypeDef *htim)
{
    /* Disable the TIM Update interrupt */
    __HAL_TIM_DISABLE_IT(htim->Init.STx);

    /* Disable the FAULT */
    __HAL_TIM_DISABLE_FAULT(htim->Init.STx);

    /* Disable the Peripheral */
    __HAL_TIM_DISABLE(htim->Init.STx);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup TIM_Exported_Functions_Group3 TIM IRQ handler management
 *  @brief    IRQ handler management
 *
   @verbatim
   ==============================================================================
 ##### IRQ handler management #####
   ==============================================================================
   [..]
    This section provides Timer IRQ handler function.

   @endverbatim
 * @{
 */


/**
 * @brief  This function handles TIM interrupts requests.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
void HAL_TIM_IRQHandler(TIM_HandleTypeDef *htim)
{
    /* ST0 */
    if (__HAL_TIM_GET_FLAG(TIM_FLAG_ST0) != RESET) {
        if (__HAL_TIM_GET_IT_SOURCE(ST0) != RESET) {
            __HAL_TIM_CLEAR_IT(TIM_FLAG_ST0);
            HAL_TIM_ST0Callback(htim);
        }
    }
    /* ST1 */
    if (__HAL_TIM_GET_FLAG(TIM_FLAG_ST1) != RESET) {
        if (__HAL_TIM_GET_IT_SOURCE(ST1) != RESET) {
            __HAL_TIM_CLEAR_IT(TIM_FLAG_ST1);
            HAL_TIM_ST1Callback(htim);
        }
    }
    /* ST2 */
    if (__HAL_TIM_GET_FLAG(TIM_FLAG_ST2) != RESET) {
        if (__HAL_TIM_GET_IT_SOURCE(ST2) != RESET) {
            __HAL_TIM_CLEAR_IT(TIM_FLAG_ST2);
            HAL_TIM_ST2Callback(htim);
        }
    }
    /* ST3 */
    if (__HAL_TIM_GET_FLAG(TIM_FLAG_ST3) != RESET) {
        if (__HAL_TIM_GET_IT_SOURCE(ST3) != RESET) {
            __HAL_TIM_CLEAR_IT(TIM_FLAG_ST3);
            HAL_TIM_ST3Callback(htim);
        }
    }
    /* ST4 */
    if (__HAL_TIM_GET_FLAG(TIM_FLAG_ST4) != RESET) {
        if (__HAL_TIM_GET_IT_SOURCE(ST4) != RESET) {
            __HAL_TIM_CLEAR_IT(TIM_FLAG_ST4);
            HAL_TIM_ST4Callback(htim);
        }
    }
    /* ST5 */
    if (__HAL_TIM_GET_FLAG(TIM_FLAG_ST5) != RESET) {
        if (__HAL_TIM_GET_IT_SOURCE(ST5) != RESET) {
            __HAL_TIM_CLEAR_IT(TIM_FLAG_ST5);
            HAL_TIM_ST5Callback(htim);
        }
    }
}


/**
 * @}
 */


/** @defgroup TIM_Exported_Functions_Group4 Peripheral Control functions
 *  @brief      Peripheral Control functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral Control functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the TIM TGS channels according to the specified
 *         parameters in the TIM_OC_InitTypeDef.
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @param  sConfig TIM TGS configuration structure
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_TGS_ConfigChannel(TIM_HandleTypeDef *htim, TIM_TGS_InitTypeDef* sConfig)
{
    /* Check the parameters */
    assert_param(IS_TIM_TGS_FUNCTION(sConfig->Function));
    assert_param(IS_TIM_TGS_CHANNELS(sConfig->Channel));
    assert_param(IS_TIM_TGS_CLK(sConfig->ClkSel));
    assert_param(IS_TIM_TGS_TRIGSRC(sConfig->TrigSrc));
    assert_param(IS_TIM_TGS_EDGE(sConfig->TrigEdge));
    assert_param(IS_TIM_TGS_FILTER(sConfig->InputFilter));
    assert_param(IS_TIM_TGS_FILTER(sConfig->InputInvert));

    htim->State = HAL_TIM_STATE_BUSY;
    if (TIM_TGS_FUNCTION_FAULT == sConfig->Function) {
        assert_param(IS_TIM_TGS_FAULT_ACTION(sConfig->Action));
        /* Set FAULTCFG */
        STIMER->FAULTCFG[htim->Init.STx]    = 0;
        STIMER->FAULTCFG[htim->Init.STx]    = sConfig->Action | sConfig->Channel;
    } else {
        assert_param(IS_TIM_TGS_TRIG_ACTION(sConfig->Action));
        /* Set TRINCFG */
        STIMER->TRINCFG[htim->Init.STx] = 0;
        STIMER->TRINCFG[htim->Init.STx] = sConfig->Action | sConfig->Channel;
    }
    /* Set TGSCH */
    STIMER->TGSCH[sConfig->Channel] = 0;
    STIMER->TGSCH[sConfig->Channel] = sConfig->TrigSrc | sConfig->ClkSel | sConfig->InputFilter | sConfig->InputInvert | sConfig->TrigEdge | ST_TGSCH_INEN;

    htim->State = HAL_TIM_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  Start the TIM Chain.
 * @param  Cascade This parameter can be one of the following values:
 *         @arg @ref TIM_CHAIN_0TO1
 *         @arg @ref TIM_CHAIN_0TO2
 *         @arg @ref TIM_CHAIN_0TO3
 *         @arg @ref TIM_CHAIN_0TO4
 *         @arg @ref TIM_CHAIN_0TO5
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_TIM_Chain_Start(uint32_t Chain)
{
    /* Check the parameters */
    assert_param(IS_TIM_CHAIN(Chain));

    for (int i = 1; i <= Chain; i++) {
        STIMER->CFG[i] |= ST_CFG_CHAIN;
    }

    //ENABLE TIM
    switch (Chain) {
    case TIM_CHAIN_0TO1:
        STIMER->CTRL = 0x11;
        break;
    case TIM_CHAIN_0TO2:
        STIMER->CTRL = 0x111;
        break;
    case TIM_CHAIN_0TO3:
        STIMER->CTRL = 0x1111;
        break;
    case TIM_CHAIN_0TO4:
        STIMER->CTRL = 0x11111;
        break;
    case TIM_CHAIN_0TO5:
        STIMER->CTRL = 0x111111;
        break;
    }

    return (HAL_OK);
}


/**
 * @brief  Set Chip in Stop Mode the value from TIM Count
 * @param  TIMx.
 * @param  Freeze Count or not Count .
 *          This parameter can be one of the following values:
 *            @arg TIM_STOPMODE_CNT_FREEZE
 *            @arg TIM_STOPMODE_CNT_NOFREEZE
 * @retval Count value
 */
void HAL_TIM_SetStopModeCountFreeze(STn_Type TIMx, uint8_t Freeze)
{
    if (Freeze)
        STIMER->CFG[TIMx] |= ST_CFG_STOPFZ;
    else
        STIMER->CFG[TIMx] &= ~ST_CFG_STOPFZ;
}


/**
 * @brief  Get the value from TIM Count
 * @param  TIMx.
 * @retval Count value
 */
uint32_t HAL_TIM_GetCountValue(STn_Type TIMx)
{
    STIMER->LOAD |= (1 << (TIMx * 4));
    __nop();
    return (STIMER->CVAL[TIMx]);
}


/**
 * @}
 */


/** @defgroup TIM_Exported_Functions_Group5 TIM Callbacks functions
 *  @brief    TIM Callbacks functions
 *
   @verbatim
   ==============================================================================
 ##### TIM Callbacks functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  ST0 callback in non blocking mode
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_ST0Callback(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the __HAL_TIM_PeriodElapsedCallback could be implemented in the user file
     */
}


/**
 * @brief  ST1 callback in non blocking mode
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_ST1Callback(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the __HAL_TIM_PeriodElapsedCallback could be implemented in the user file
     */
}


/**
 * @brief  ST2 callback in non blocking mode
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_ST2Callback(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the __HAL_TIM_PeriodElapsedCallback could be implemented in the user file
     */
}


/**
 * @brief  ST3 callback in non blocking mode
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_ST3Callback(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the __HAL_TIM_PeriodElapsedCallback could be implemented in the user file
     */
}


/**
 * @brief  ST4 callback in non blocking mode
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_ST4Callback(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the __HAL_TIM_PeriodElapsedCallback could be implemented in the user file
     */
}


/**
 * @brief  ST5 callback in non blocking mode
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval None
 */
__weak void HAL_TIM_ST5Callback(TIM_HandleTypeDef *htim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(htim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the __HAL_TIM_PeriodElapsedCallback could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup TIM_Exported_Functions_Group6 Peripheral State functions
 *  @brief   Peripheral State functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral State functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Return the TIM Base state
 * @param  htim pointer to a TIM_HandleTypeDef structure that contains
 *                the configuration information for TIM module.
 * @retval HAL state
 */
HAL_TIM_StateTypeDef HAL_TIM_GetState(TIM_HandleTypeDef *htim)
{
    return (htim->State);
}


/**
 * @brief  Time configuration
 * @param  TIMx TIM peripheral
 * @param  Structure pointer on TIM Time Base required parameters
 * @retval None
 */
static void __TIM_Config(ST_TypeDef *TIMx, TIM_Base_InitTypeDef *Structure)
{
    /* Reset TIMx */
    __HAL_TIM_RESET(Structure->STx);

    /* Set TIMx CFG */
    TIMx->CFG[Structure->STx]   = 0;
    TIMx->CFG[Structure->STx]   = Structure->CountPolarity | Structure->CountMode | Structure->WorkMode;

    /* Set the Auto-reload value */
    TIMx->MOD0[Structure->STx] = Structure->AutoReload0;
    if (Structure->WorkMode == TIM_WORKMODE_PWM) {
        TIMx->MOD1[Structure->STx] = Structure->AutoReload1;
    }

    /* Set the Prescaler value */
    TIMx->PSC[Structure->STx] = Structure->Prescaler;
}


/**
 * @}
 */

#endif /* HAL_TIM_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
