/**
 ******************************************************************************
 * @file    fm15f3xx_ll_qspi.c
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_ll_qspi.h"
#include "fm15f3xx_ll_dma.h"
#include "stdio.h"
#if defined (QSPI)
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/


/**
 * @brief  Set each @ref LL_QSPI_StructInit field to default value.
 * @param  QSPI_InitTypeDef pointer to a @ref QSPI_InitTypeDef structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_QSPI_StructInit(LL_QSPI_InitTypeDef* QSPIInitStruct)
{
    QSPIInitStruct->WorkMode        = LL_QSPI_WORK_INDIRECT;
    QSPIInitStruct->AccessFlash     = LL_QSPI_ACCESS_FLASHA;
    QSPIInitStruct->SpeedMode       = LL_QSPI_SPEED_SDR;
    QSPIInitStruct->EndianMode      = LL_QSPI_ENDIAN_LITTLE;
    QSPIInitStruct->SignificantBit  = LL_QSPI_RxFIFO_LSB;
    QSPIInitStruct->IoMode          = LL_QSPI_IO_HIGH;
    QSPIInitStruct->SampleShift     = LL_QSPI_SAMPLESHIFT_NONE;
    QSPIInitStruct->SamplePoint     = LL_QSPI_SAMPLEPOINT_DISABLE;
    QSPIInitStruct->ClkMode         = LL_QSPI_CLK_MODE0;
    QSPIInitStruct->ClkDiv          = LL_QSPI_CLK_DIV2;
    QSPIInitStruct->RxfifoWm        = 4;
    QSPIInitStruct->TxfifoWm        = 4;
    QSPIInitStruct->QuitStallCnt    = LL_QSPI_STALL_QUITCNT_DISABLE;
}


/**
 * @brief Initialize the QSPI mode according to the specified parameters
 *        in the QSPI_InitTypeDef and initialize the associated handle.
 * @param QSPI Instance
 * @param QSPI_InitStruct
 * @retval None
 */
void LL_QSPI_Init(QSPI_TypeDef *QSPIx, LL_QSPI_InitTypeDef* QSPI_InitStruct)
{
    /* Configure QSPI CGF */
    MODIFY_REG(QSPIx->CFG, 0x27, QSPI_InitStruct->AccessFlash | QSPI_InitStruct->SpeedMode);

    /* Configure QSPI Data Order */
    MODIFY_REG(QSPIx->DORDER, 0xC, QSPI_InitStruct->EndianMode | QSPI_InitStruct->SignificantBit);

    /* Configure QSPI mode */
    MODIFY_REG(QSPIx->MODE, 0xB, QSPI_InitStruct->WorkMode | QSPI_InitStruct->IoMode);

    /* Configure QSPI SAMPLE */
    MODIFY_REG(QSPIx->SAMPCFG, 0x3F, QSPI_InitStruct->SamplePoint | QSPI_InitStruct->SampleShift);

    /* Configure QSPI Clock Prescaler and Clock Mode */
    MODIFY_REG(QSPIx->CLKCFG, 0xF, QSPI_InitStruct->ClkMode | QSPI_InitStruct->ClkDiv);

    /* Configure QSPI Txfifo */
    MODIFY_REG(QSPIx->TXFIFOCFG, 0x7F, QSPI_TXFIFOCFG_EN | (QSPI_InitStruct->TxfifoWm << QSPI_TXFIFOCFG_WM_Pos));

    /* Configure QSPI Rxfifo */
    MODIFY_REG(QSPIx->RXFIFOCFG, 0x1FFF, QSPI_RXFIFOCFG_EN | (QSPI_InitStruct->RxfifoWm << QSPI_RXFIFOCFG_QUADWM_Pos) | \
               (QSPI_InitStruct->RxfifoWm << QSPI_RXFIFOCFG_NORMWM_Pos));

    /* Configure QSPI Stall */
    if (QSPI_InitStruct->QuitStallCnt == LL_QSPI_STALL_QUITCNT_DISABLE) {
        MODIFY_REG(QSPIx->ABORTCFG, QSPI_ABORTCFG_CSTALLEN, LL_QSPI_STALL_QUITCNT_DISABLE);
    } else {
        MODIFY_REG(QSPIx->ABORTCFG, QSPI_ABORTCFG_CSTALLEN, LL_QSPI_STALL_QUITCNT_ENABLE);
        WRITE_REG(QSPIx->SCNTTHR, QSPI_InitStruct->QuitStallCnt);
    }
}

/**
 * @brief Sets the command transmit configuration.
 * @param cmd  structure that contains the command configuration information
 * @retval None
 */
void LL_QSPI_Command(LL_QSPI_CommandTypeDef *cmd)
{
    /* Clear all Flag */
    LL_QSPI_ClearAllFlag(QSPI);
    /* Configure single mode transfer */
    if (cmd->InstructionLines == LL_QSPI_LINE_SINGLE || cmd->AddressLines == LL_QSPI_LINE_SINGLE || \
        cmd->AlternateLines == LL_QSPI_LINE_SINGLE || cmd->DataLines == LL_QSPI_LINE_SINGLE) {
        SET_BIT(QSPI->CFG, LL_QSPI_SINGLEMODE_ENABLE);
    } else {
        CLEAR_BIT(QSPI->CFG, LL_QSPI_SINGLEMODE_ENABLE);
    }

    if (cmd->DataLines != LL_QSPI_LINE_NONE) {
        /* Configure QSPI: the number of data to read or write */
        WRITE_REG(QSPI->DLEN, cmd->NumData);
    } else {
        WRITE_REG(QSPI->DLEN, 0);
    }
    if (cmd->InstructionLines != LL_QSPI_LINE_NONE) {
        if (cmd->AlternateLines != LL_QSPI_LINE_NONE) {
            /* Configure QSPI: alternate bytes  */
            WRITE_REG(QSPI->MODEALT, cmd->Alternate);
            if (cmd->AddressLines != LL_QSPI_LINE_NONE) {
                /*---- Command with instruction, address and alternate bytes ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(QSPI->IOMODE, (cmd->AlternateSize << QSPI_IOMODE_MODESIZE_Pos) | \
                          (cmd->AddressSize << QSPI_IOMODE_ADDRSIZE_Pos) | (cmd->DummyCycles << QSPI_IOMODE_DUMMYCYC_Pos) | \
                          (cmd->DataLines << QSPI_IOMODE_DATAIOM_Pos) | (cmd->AlternateLines << QSPI_IOMODE_MODEIOM_Pos) | \
                          (cmd->AddressLines << QSPI_IOMODE_ADDRIOM_Pos) | (cmd->InstructionLines));
                /* Configure QSPI: address */
                WRITE_REG(QSPI->FADDR, cmd->Address);

                /* Configure QSPI: instruction */
                WRITE_REG(QSPI->CMD, cmd->Instruction | cmd->TxOrRxData);
            } else {
                /*---- Command with instruction and alternate bytes ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(QSPI->IOMODE, (cmd->AlternateSize << QSPI_IOMODE_MODESIZE_Pos) | \
                          (cmd->DummyCycles << QSPI_IOMODE_DUMMYCYC_Pos) | (cmd->DataLines << QSPI_IOMODE_DATAIOM_Pos) | \
                          (cmd->AlternateLines << QSPI_IOMODE_MODEIOM_Pos) | (cmd->InstructionLines));
                /* Configure QSPI: instruction */
                WRITE_REG(QSPI->CMD, cmd->Instruction | cmd->TxOrRxData);
            }
        } else {
            if (cmd->AddressLines != LL_QSPI_LINE_NONE) {
                /*---- Command with instruction and address ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(QSPI->IOMODE, (cmd->AddressSize << QSPI_IOMODE_ADDRSIZE_Pos) | \
                          (cmd->DummyCycles << QSPI_IOMODE_DUMMYCYC_Pos) | (cmd->DataLines << QSPI_IOMODE_DATAIOM_Pos) | \
                          (cmd->AddressLines << QSPI_IOMODE_ADDRIOM_Pos) | (cmd->InstructionLines));
                /* Configure QSPI: address */
                WRITE_REG(QSPI->FADDR, cmd->Address);
                /* Configure QSPI: instruction */
                WRITE_REG(QSPI->CMD, cmd->Instruction | cmd->TxOrRxData);
            } else {
                /*---- Command with only instruction ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(QSPI->IOMODE, (cmd->DummyCycles << QSPI_IOMODE_DUMMYCYC_Pos) | \
                          (cmd->DataLines << QSPI_IOMODE_DATAIOM_Pos) | (cmd->InstructionLines));
                /* Configure QSPI: instruction */
                WRITE_REG(QSPI->CMD, cmd->Instruction | cmd->TxOrRxData);
            }
        }
    }
    if (cmd->DataLines == LL_QSPI_LINE_NONE) {
        LL_QSPI_StartTxIndirectMode(QSPI);
        while (LL_QSPI_GetStatus(QSPI, LL_QSPI_FLAG_TRANSOVER) != LL_QSPI_FLAG_TRANSOVER);
    }
}

/**
 * @brief Transmit an amount of data in blocking mode.
 * @param pData pointer to data buffer
 * @retval None
 */
void LL_QSPI_Transmit(uint8_t *pData)
{
    uint32_t TxXferCount;
    uint8_t    *pTxBuffPtr;
    __IO uint32_t  *data_reg = &QSPI->TXFIFOD;
    if (pData != NULL) {
        /* Update state */
        /* Configure counters and size of the handle */
        TxXferCount = QSPI->DLEN;
        pTxBuffPtr    = pData;
        LL_QSPI_StartTxIndirectMode(QSPI);
        while (TxXferCount > 0) {
            /* Wait until Txfifo Full flag is reset to send data */
            while (READ_BIT(QSPI->TXFIFOS, LL_QSPI_FLAG_TxFIFO_FULL) == LL_QSPI_FLAG_TxFIFO_FULL)
                ;
            *(__IO uint8_t *) data_reg = *pTxBuffPtr++;
            TxXferCount--;
        }
        while (1) {
            if (READ_BIT(QSPI->STATUS, LL_QSPI_FLAG_STALL) == LL_QSPI_FLAG_STALL) {
                *(__IO uint8_t *) data_reg = 0xFF;
            } else if (READ_BIT(QSPI->STATUS, LL_QSPI_FLAG_TRANSOVER) == LL_QSPI_FLAG_TRANSOVER) {
                break;
            }
        }
        while (LL_QSPI_GetStatus(QSPI, LL_QSPI_FLAG_TRANSOVER) != LL_QSPI_FLAG_TRANSOVER);
        LL_QSPI_ClearFlag(QSPI, LL_QSPI_FLAG_CLEAR_TxFIFO);
    }
}

/**
 * @brief Receive an amount of data in blocking mode
 * @param pData pointer to data buffer
 * @retval None
 */
void LL_QSPI_Receive(uint8_t *pData)
{
    uint32_t   RxXferCount  = 0;
    uint8_t       *pRxBuffPtr;
    __I uint32_t  *data_reg = &QSPI->RXFIFOD;
    if (pData != NULL) {
        /* Configure counters and size of the handle */
        RxXferCount = QSPI->DLEN;
        pRxBuffPtr    = pData;
        LL_QSPI_StartTxIndirectMode(QSPI);
        while (RxXferCount > 0) {
            /* Wait until Rxfifo don't empty to read received data */
            while ((QSPI->RXFIFOS & LL_QSPI_FLAG_RxFIFO_CNT) == 0)
                ;
            *pRxBuffPtr++ = *(__IO uint8_t *) data_reg;
            RxXferCount--;
        }
        while (LL_QSPI_GetStatus(QSPI, LL_QSPI_FLAG_TRANSOVER) != LL_QSPI_FLAG_TRANSOVER);
    }
}

/**
 * @brief Transmit an amount of data in blocking mode.
 * @param DmaChannel DMA Channel
 * @param pData pointer to data buffer
 * @retval None
 */
void LL_QSPI_Transmit_DMA(uint8_t DmaChannel, uint8_t* pData)
{
    uint32_t TxXferCount;
    if (pData != NULL) {
        TxXferCount = QSPI->DLEN;
        while ((LL_DMA_GetMajorLoopCiter(DmaChannel) > 0) || (!LL_QSPI_GetStatus(QSPI, LL_QSPI_FLAG_TRANSOVER)))
            ;
        LL_QSPI_ClearAllFlag(QSPI);
        LL_QSPI_EnableDMA(QSPI, LL_QSPI_INT_TxFIFOEMPTY);
        while (!LL_DMA_GET_DONE(DmaChannel) && LL_DMA_GET_ACTIVE(DmaChannel))
            ;
        LL_DMA_Start_IT(DmaChannel, (uint32_t)pData, (uint32_t) & (QSPI->TXFIFOD), TxXferCount, LL_DMA_IT_TC);
        LL_QSPI_StartTxIndirectMode(QSPI);
    }
}

/**
 * @brief Transmit an amount of data in blocking mode.
 * @param DmaChannel DMA Channel
 * @param pData pointer to data buffer
 * @param RxMode Recieve Align mode 1,2,4,8,16,32.
 * @retval None
 */
void LL_QSPI_Receive_DMA(uint8_t DmaChannel, uint8_t* pData, uint8_t RxAlign)
{
    uint32_t RxXferCount;
    if (pData != NULL) {
        RxXferCount = QSPI->DLEN;
        while (!LL_DMA_GET_DONE(DmaChannel) && LL_DMA_GET_ACTIVE(DmaChannel))
            ;
        LL_QSPI_DisableDMA(QSPI, LL_QSPI_DMA_RxFIFONFULL);
        LL_QSPI_ClearAllFlag(QSPI);
        LL_QSPI_EnableDMA(QSPI, LL_QSPI_DMA_RxFIFONFULL);
        LL_DMA_Start_IT(DmaChannel, (uint32_t) & (QSPI->RXFIFOD), (uint32_t)pData, RxXferCount / RxAlign, LL_DMA_IT_TC);
        LL_QSPI_StartTxIndirectMode(QSPI);
    }
}

/**
 * @}
 */
#endif /* defined (QSPI) */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
