/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lpmgr.c
 *
 * @brief       This file implements low power manager for fm33.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>

#define DBG_EXT_TAG "drv.lpmgr"
#define DBG_EXT_LVL DBG_EXT_INFO

//休眠时SWD的输入脚如果浮空，可能会感应电平导致IO输出寄存器翻转引起功耗变大
//如果SWD口无外部上拉的话，休眠前可开启上拉使能
void SWD_IO_PullUp(FunState NewState)
{
    if (DISABLE == NewState)    //关闭SWDTLK.SWDTDO上拉使能
    {
        AltFunIO(GPIOG, GPIO_Pin_8, ALTFUN_NORMAL);    // PG8;//SWDTCK
        AltFunIO(GPIOG, GPIO_Pin_9, ALTFUN_NORMAL);    // PG9;//SWDTDO
    }
    else    //打开SWDTCK,SWDTDO上拉使能
    {
        AltFunIO(GPIOG, GPIO_Pin_8, ALTFUN_PULLUP);    // PG8;//SWDTCK
        AltFunIO(GPIOG, GPIO_Pin_9, ALTFUN_PULLUP);    // PG9;//SWDTDO
    }
}

void fm_MF_PMU_Init(void)
{
    SWD_IO_PullUp(ENABLE);
    IWDT_IWDTCFG_IWDTSLP4096S_Setable(ENABLE);    //配置休眠时是否启用4096s长周期
    IWDT_Clr();                                   //清系统看门狗
}

void fm_Sleep(os_uint32_t mode)
{
    PMU_SleepCfg_InitTypeDef SleepCfg_InitStruct;

    /*下电复位配置*/
    // pdr和bor两个下电复位至少要打开一个
    //当电源电压低于下电复位时，芯片会被复位住
    // pdr电压档位不准但是功耗极低(几乎无测量）
    // bor电压档位准确但是需要增加2uA功耗
    ANAC_PDRCON_PDREN_Setable(ENABLE);                  //打开PDR
    ANAC_PDRCON_PDRCFG_Set(ANAC_PDRCON_PDRCFG_1P5V);    // pdr电压调整到1.5V
    ANAC_BORCON_OFF_BOR_Setable(ENABLE);                //关闭BOR 3uA
    RCC_SYSCLKSEL_LPM_RCLP_OFF_Setable(ENABLE);         //关闭rclp 0.2uA

    SleepCfg_InitStruct.PMOD  = PMU_LPMCFG_PMOD_SLEEP;    //功耗模式配置
    SleepCfg_InitStruct.SLPDP = mode;                     // Sleep配置
    SleepCfg_InitStruct.CVS   = DISABLE;                  //内核电压降低控制
    SleepCfg_InitStruct.XTOFF = PMU_LPMCFG_XTOFF_DIS;     //保持XTLF
    SleepCfg_InitStruct.SCR   = 0;                        // M0系统控制寄存器，一般配置为0即可

    PMU_SleepCfg_Init(&SleepCfg_InitStruct);    //休眠配置

    IWDT_Clr();
    __WFI();    //进入休眠
    IWDT_Clr();
}

/**
 ***********************************************************************************************************************
 * @brief           Put device into sleep mode.
 *
 * @param[in]       lpm             Low power manager structure.
 * @param[in]       mode            Low power mode.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
static os_err_t lpm_sleep(lpmgr_sleep_mode_e mode)
{
    switch (mode)
    {
    case SYS_SLEEP_MODE_NONE:
        break;

    case SYS_SLEEP_MODE_IDLE:
        // __WFI();
        break;

    case SYS_SLEEP_MODE_LIGHT:
        fm_MF_PMU_Init(); /* sleep mode */
        fm_Sleep(PMU_LPMCFG_SLPDP_SLEEP);
        break;

    case SYS_SLEEP_MODE_DEEP:

        fm_MF_PMU_Init(); /* DeepSleep mode */
        fm_Sleep(PMU_LPMCFG_SLPDP_DEEPSLEEP);
        break;

    default:
        OS_ASSERT(0);
    }

    return OS_EOK;
}

int drv_lpmgr_hw_init(void)
{
    os_lpmgr_init(lpm_sleep);
    return 0;
}

OS_PREV_INIT(drv_lpmgr_hw_init, OS_INIT_SUBLEVEL_MIDDLE);
