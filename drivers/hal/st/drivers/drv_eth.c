/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <os_memory.h>
#include <dma.h>
#include <string.h>
#include <bus/bus.h>
#include <net_dev.h>
#include "drv_eth.h"

/*
 * Emac driver uses CubeMX tool to generate emac and phy's configuration,
 * the configuration files can be found in CubeMX_Config folder.
 */

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.eth"
#include <dlog.h>

#ifdef SERIES_STM32H7
#define ETH_DMA_TRANSMIT_TIMEOUT (2000U)

extern ETH_TxPacketConfig TxConfig;
extern ETH_DMADescTypeDef DMARxDscrTab[ETH_RX_DESC_CNT];
extern ETH_DMADescTypeDef DMATxDscrTab[ETH_TX_DESC_CNT];
extern uint8_t            Rx_Buff[ETH_RX_DESC_CNT][ETH_MAX_PACKET_SIZE];

ETH_BufferTypeDef *rxbuff_dscrtab = OS_NULL;
ETH_BufferTypeDef *txbuff_dscrtab = OS_NULL;
#endif

struct os_stm32_eth
{
    struct os_net_device net_dev;

    ETH_HandleTypeDef *EthHandle;

    os_uint8_t dev_addr[OS_NET_MAC_LENGTH];
    uint32_t   eth_speed;
    uint32_t   eth_mode;

    ETH_DMADescTypeDef *DMARxDscrTab;
    ETH_DMADescTypeDef *DMATxDscrTab;

    os_uint8_t *rxbuff;
    os_uint8_t *txbuff;

    os_base_t reset_pin;
    os_base_t reset_value;

    os_task_t *phy_monitor;

    os_list_node_t list;
};

static os_list_node_t stm32_eth_list = OS_LIST_INIT(stm32_eth_list);

void HAL_ETH_RxCpltCallback(ETH_HandleTypeDef *heth)
{
    struct os_stm32_eth *eth_dev = OS_NULL;

    os_list_for_each_entry(eth_dev, &stm32_eth_list, struct os_stm32_eth, list)
    {
        if (eth_dev->EthHandle == heth)
        {
            os_net_rx_report_irq(&eth_dev->net_dev);
            break;
        }
    }
}

void HAL_ETH_ErrorCallback(ETH_HandleTypeDef *heth)
{
    LOG_E(DRV_EXT_TAG, "eth err");
}

static os_err_t phy_reset(struct os_stm32_eth *eth_dev)
{
    os_pin_write(eth_dev->reset_pin, eth_dev->reset_value);
    os_task_msleep(100);
    os_pin_write(eth_dev->reset_pin, !eth_dev->reset_value);
    os_task_msleep(100);

    return OS_EOK;
}

static os_err_t phy_gpio_init(struct os_stm32_eth *eth_dev)
{
    if (ETH_RESET_PIN < 0)
        return OS_EINVAL;

    eth_dev->reset_pin = ETH_RESET_PIN;

#ifdef ETH_RESET_PIN_ACTIVE_HIGH
    eth_dev->reset_value = PIN_HIGH;
#else
    eth_dev->reset_value         = PIN_LOW;
#endif

    os_pin_mode(eth_dev->reset_pin, PIN_MODE_OUTPUT);

    return OS_EOK;
}

static os_err_t os_stm32_eth_init(struct os_net_device *net_dev)
{
    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    HAL_ETH_DeInit(eth_dev->EthHandle);

    phy_reset(eth_dev);

    eth_dev->EthHandle->Init.MACAddr = (os_uint8_t *)&eth_dev->dev_addr[0];

#ifndef SERIES_STM32H7
    eth_dev->EthHandle->Init.RxMode = ETH_RXINTERRUPT_MODE;
#endif

    if (HAL_ETH_Init(eth_dev->EthHandle) != HAL_OK)
    {
        LOG_E(DRV_EXT_TAG, "eth hardware init failed");
        return OS_ERROR;
    }

#ifndef SERIES_STM32H7
#define MACFFR_FILTER_MODE_MASK                                                                                        \
    ETH_MULTICASTFRAMESFILTER_PERFECTHASHTABLE | ETH_MULTICASTFRAMESFILTER_HASHTABLE |                                 \
        ETH_MULTICASTFRAMESFILTER_PERFECT | ETH_MULTICASTFRAMESFILTER_NONE |                                           \
        ETH_UNICASTFRAMESFILTER_PERFECTHASHTABLE | ETH_UNICASTFRAMESFILTER_HASHTABLE | ETH_UNICASTFRAMESFILTER_PERFECT
    os_uint32_t tmpreg;
    /*config frame filter : group use hashtable; idividual use perfect*/
    tmpreg = eth_dev->EthHandle->Instance->MACFFR;
    tmpreg &= ~MACFFR_FILTER_MODE_MASK;
    tmpreg |= ETH_MULTICASTFRAMESFILTER_PERFECTHASHTABLE;

    eth_dev->EthHandle->Instance->MACFFR = tmpreg;
    tmpreg                               = eth_dev->EthHandle->Instance->MACFFR;
    HAL_Delay(ETH_REG_WRITE_DELAY);
    eth_dev->EthHandle->Instance->MACFFR = tmpreg;

    HAL_ETH_DMATxDescListInit(eth_dev->EthHandle, eth_dev->DMATxDscrTab, eth_dev->txbuff, ETH_TXBUFNB);
    HAL_ETH_DMARxDescListInit(eth_dev->EthHandle, eth_dev->DMARxDscrTab, eth_dev->rxbuff, ETH_RXBUFNB);

    if (HAL_ETH_Start(eth_dev->EthHandle) != HAL_OK)
    {
        LOG_E(DRV_EXT_TAG, "emac hardware start faild");
        return OS_ERROR;
    }
#else
    os_uint8_t                 i = 0;
    ETH_MACFilterConfigTypeDef FilterConfig;
    memset(&FilterConfig, 0, sizeof(ETH_MACFilterConfigTypeDef));

    for (i = 0; i < ETH_RX_DESC_CNT; i++)
    {
        HAL_ETH_DescAssignMemory(eth_dev->EthHandle, i, Rx_Buff[i], NULL);
    }

    FilterConfig.HashMulticast       = ENABLE;
    FilterConfig.HachOrPerfectFilter = ENABLE;
    FilterConfig.BroadcastFilter     = ENABLE;
    HAL_ETH_SetMACFilterConfig(eth_dev->EthHandle, &FilterConfig);

    if (HAL_ETH_Start_IT(eth_dev->EthHandle) != HAL_OK)
    {
        LOG_E(DRV_EXT_TAG, "emac hardware start faild");
        return OS_ERROR;
    }
#endif

    return OS_EOK;
}

static os_err_t os_stm32_eth_deinit(struct os_net_device *net_dev)
{
    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    HAL_ETH_DeInit(eth_dev->EthHandle);

    phy_reset(eth_dev);

    return OS_EOK;
}

#ifndef SERIES_STM32H7
#define ETH_MACADDR_LBASE ETH_MAC_ADDR_LBASE
#define ETH_MACADDR_HBASE ETH_MAC_ADDR_HBASE
#else
#define ETH_MACADDR_LBASE (uint32_t)(heth->Instance->MACA0HR)
#define ETH_MACADDR_HBASE (uint32_t)(heth->Instance->MACA1HR)
#endif
static os_err_t os_stm32_eth_set_perfect_filter(ETH_HandleTypeDef *heth, os_uint8_t *addr, os_bool_t enable)
{
    os_uint8_t  i        = 0;
    os_uint32_t tmpreg_h = 0;
    os_uint32_t tmpreg_l = 0;
    os_uint32_t addr_h   = 0;
    os_uint32_t addr_l   = 0;

    if (enable)
    {
        for (i = 1; i < 4; i++)
        {
            tmpreg_h = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8)));
            tmpreg_l = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8)));
            if (tmpreg_h || tmpreg_l)
            {
                break;
            }
        }

        if (i == 4)
        {
            LOG_E(DRV_EXT_TAG, "perfect filter is full!");
            return OS_EFULL;
        }

        addr_h = ((uint32_t)addr[5] << 8) | (uint32_t)addr[4] | 0x80000000;
        (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8))) = addr_h;

        addr_l = ((uint32_t)addr[3] << 24) | ((uint32_t)addr[2] << 16) | ((uint32_t)addr[1] << 8) | addr[0];
        (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8))) = addr_l;
    }
    else
    {
        addr_h = ((uint32_t)addr[5] << 8) | (uint32_t)addr[4] | 0x80000000;
        addr_l = ((uint32_t)addr[3] << 24) | ((uint32_t)addr[2] << 16) | ((uint32_t)addr[1] << 8) | addr[0];

        for (i = 1; i < 4; i++)
        {
            tmpreg_h = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8)));
            tmpreg_l = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8)));
            if ((tmpreg_h == addr_h) && (tmpreg_l == addr_l))
            {
                (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8))) = 0;
                (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8))) = 0;
            }
        }
    }

    return OS_EOK;
}

static os_uint32_t _calc_filter_hash(os_uint8_t *addr)
{
    os_uint32_t crc    = 0xFFFFFFFFU;
    os_uint32_t count1 = 0;
    os_uint32_t count2 = 0;

    for (count1 = 0; count1 < OS_NET_MAC_LENGTH; count1++)
    {
        crc = crc ^ addr[count1];
        for (count2 = 0; count2 < 0x08U; count2++)
        {
            if (0U != (crc & 1U))
            {
                crc >>= 1U;
                crc ^= 0xEDB88320U;
            }
            else
            {
                crc >>= 1U;
            }
        }
    }

    count1 = 0;
    count2 = ~crc;
    crc    = 0;

    for (count1 = 0; count1 < 32; count1++)
    {
        if (count2 & (1 << count1))
        {
            crc |= 1 << (31 - count1);
        }
    }

    crc = crc >> 26U;

    return crc;
}

#ifndef SERIES_STM32H7
#define ETH_HASHTABLE_L eth_dev->EthHandle->Instance->MACHTLR
#define ETH_HASHTABLE_H eth_dev->EthHandle->Instance->MACHTHR
#else
#define ETH_HASHTABLE_L eth_dev->EthHandle->Instance->MACHT0R
#define ETH_HASHTABLE_H eth_dev->EthHandle->Instance->MACHT1R
#endif
static os_err_t os_stm32_eth_set_filter(struct os_net_device *net_dev, os_uint8_t *addr, os_bool_t enable)
{
    os_uint32_t crc       = 0;
    os_uint32_t configVal = 0;

    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    if (*addr && 0x01)
    {
        crc = _calc_filter_hash(addr);

        configVal = ((os_uint32_t)1U << (crc & 0x1FU));

        if (enable)
        {
            if (0U != (crc & 0x20U))
            {
                ETH_HASHTABLE_H |= configVal;
            }
            else
            {
                ETH_HASHTABLE_L |= configVal;
            }
        }
        else
        {
            if (0U != (crc & 0x20U))
            {
                ETH_HASHTABLE_H &= ~configVal;
            }
            else
            {
                ETH_HASHTABLE_L &= ~configVal;
            }
        }

        return OS_EOK;
    }
    else
    {
        return os_stm32_eth_set_perfect_filter(eth_dev->EthHandle, addr, enable);
    }
}

static os_err_t os_stm32_eth_get_macaddr(struct os_net_device *net_dev, os_uint8_t *addr)
{
    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    memcpy(addr, eth_dev->dev_addr, OS_NET_MAC_LENGTH);

    return OS_EOK;
}

static os_err_t os_stm32_eth_send(struct os_net_device *net_dev, os_net_xfer_data_t *data)
{
    os_base_t   level;
    os_uint8_t *buffer_src;
    os_uint8_t *buffer_dst;
    os_uint16_t length;
    os_uint16_t cpy_length;

    HAL_StatusTypeDef state;

    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    length     = data->size;
    buffer_src = data->buff;

#ifndef SERIES_STM32H7
    __IO ETH_DMADescTypeDef *dmatxdesc;

    dmatxdesc = eth_dev->EthHandle->TxDesc;

    while ((dmatxdesc->Status & ETH_DMATXDESC_OWN) != (uint32_t)RESET)
    {
        if ((eth_dev->EthHandle->Instance->DMASR & ETH_DMASR_TUS) != (uint32_t)RESET)
        {
            eth_dev->EthHandle->Instance->DMASR   = ETH_DMASR_TUS;
            eth_dev->EthHandle->Instance->DMATPDR = 0;
        }
    }

    buffer_dst = (os_uint8_t *)dmatxdesc->Buffer1Addr;

    while (length)
    {
        cpy_length = min(ETH_MAX_PACKET_SIZE, length);
        memcpy(buffer_dst, buffer_src, cpy_length);
        length -= cpy_length;
        buffer_src += cpy_length;

        dmatxdesc  = (ETH_DMADescTypeDef *)(dmatxdesc->Buffer2NextDescAddr);
        buffer_dst = (uint8_t *)(dmatxdesc->Buffer1Addr);
    }
#else
    os_uint8_t i = 0;

    memset(txbuff_dscrtab, 0, ETH_TX_DESC_CNT * sizeof(ETH_BufferTypeDef));

    TxConfig.Length   = data->size;
    TxConfig.TxBuffer = txbuff_dscrtab;

    buffer_dst = eth_dev->txbuff;

    while (length)
    {
        if (i > 0)
        {
            txbuff_dscrtab[i].next = &txbuff_dscrtab[i - 1];
        }

        txbuff_dscrtab[i].buffer = buffer_dst;
        cpy_length               = min(ETH_MAX_PACKET_SIZE, length);
        memcpy(buffer_dst, buffer_src, cpy_length);
        txbuff_dscrtab[i].len = cpy_length;
        i++;

        length -= cpy_length;
        buffer_src += cpy_length;
        buffer_dst += cpy_length;
    }
#endif

    level = os_irq_lock();
    OS_ASSERT(eth_dev->EthHandle->Lock != HAL_LOCKED);
#ifndef SERIES_STM32H7
    state = HAL_ETH_TransmitFrame(eth_dev->EthHandle, data->size);
#else
    state                           = HAL_ETH_Transmit(eth_dev->EthHandle, &TxConfig, ETH_DMA_TRANSMIT_TIMEOUT);
#endif
    os_irq_unlock(level);

    if (state != HAL_OK)
    {
        LOG_E(DRV_EXT_TAG, "eth transmit frame faild: %d", state);
        return OS_ERROR;
    }

    return OS_EOK;
}

static os_net_xfer_data_t *os_stm32_eth_recv(struct os_net_device *net_dev)
{
    os_uint8_t        i = 0;
    os_base_t         level;
    os_uint8_t       *buffer_src;
    os_uint8_t       *buffer_dst;
    os_uint32_t       length;
    HAL_StatusTypeDef state;

    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;
    os_net_xfer_data_t  *data    = OS_NULL;

#ifndef SERIES_STM32H7
    os_uint32_t cpy_length = 0;

    __IO ETH_DMADescTypeDef *dmarxdesc;

    level = os_irq_lock();
    OS_ASSERT(eth_dev->EthHandle->Lock != HAL_LOCKED);
    state = HAL_ETH_GetReceivedFrame_IT(eth_dev->EthHandle);
    os_irq_unlock(level);

    length = eth_dev->EthHandle->RxFrameInfos.length;

    if ((state != HAL_OK) || (length == 0))
    {
        LOG_E(DRV_EXT_TAG, "receive frame faild");

        return OS_NULL;
    }

    data = os_net_get_buff(length);
    if (data == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "no memory");
        return OS_NULL;
    }

    buffer_dst = data->buff;
    buffer_src = (uint8_t *)eth_dev->EthHandle->RxFrameInfos.buffer;
    dmarxdesc  = eth_dev->EthHandle->RxFrameInfos.FSRxDesc;

    while (length)
    {
        cpy_length = min(ETH_RX_BUF_SIZE, length);
        memcpy(buffer_dst, buffer_src, cpy_length);
        length -= cpy_length;
        buffer_dst += cpy_length;

        dmarxdesc  = (ETH_DMADescTypeDef *)(dmarxdesc->Buffer2NextDescAddr);
        buffer_src = (uint8_t *)(dmarxdesc->Buffer1Addr);
    }

    dmarxdesc = eth_dev->EthHandle->RxFrameInfos.FSRxDesc;
    for (i = 0; i < eth_dev->EthHandle->RxFrameInfos.SegCount; i++)
    {
        dmarxdesc->Status |= ETH_DMARXDESC_OWN;
        dmarxdesc = (ETH_DMADescTypeDef *)(dmarxdesc->Buffer2NextDescAddr);
    }

    eth_dev->EthHandle->RxFrameInfos.SegCount = 0;

    if ((eth_dev->EthHandle->Instance->DMASR & ETH_DMASR_RBUS) != (uint32_t)RESET)
    {
        eth_dev->EthHandle->Instance->DMASR   = ETH_DMASR_RBUS;
        eth_dev->EthHandle->Instance->DMARPDR = 0;
    }
#else
    ETH_BufferTypeDef *src_rxbuffer = OS_NULL;

    memset(rxbuff_dscrtab, 0, ETH_RX_DESC_CNT * sizeof(ETH_BufferTypeDef));

    for (i = 0; i < ETH_RX_DESC_CNT - 1; i++)
    {
        rxbuff_dscrtab[i].next = &rxbuff_dscrtab[i + 1];
    }

    level = os_irq_lock();
    OS_ASSERT(eth_dev->EthHandle->Lock != HAL_LOCKED);
    state = HAL_ETH_GetRxDataBuffer(eth_dev->EthHandle, rxbuff_dscrtab);
    os_irq_unlock(level);

    HAL_ETH_GetRxDataLength(eth_dev->EthHandle, (uint32_t *)&length);
    if ((state != HAL_OK) || (length == 0))
    {
        LOG_D(DRV_EXT_TAG, "receive frame faild");
        return NULL;
    }

    HAL_ETH_BuildRxDescriptors(eth_dev->EthHandle);

    data = os_net_get_buff(length);
    if (data == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "no memory");
        return OS_NULL;
    }

    src_rxbuffer = rxbuff_dscrtab;
    buffer_dst   = data->buff;
    buffer_src   = src_rxbuffer->buffer;
    while (length)
    {
        memcpy(buffer_dst, buffer_src, src_rxbuffer->len);
        buffer_dst += src_rxbuffer->len;
        length -= src_rxbuffer->len;
        src_rxbuffer = src_rxbuffer->next;
    }

#endif
    return data;
}

/* clang-format off */
static HAL_StatusTypeDef os_eth_phyreg_write(ETH_HandleTypeDef *heth, uint32_t PHYAddr, uint32_t PHYReg, uint32_t pRegValue)
{
#ifndef SERIES_STM32H7
    return HAL_ETH_WritePHYRegister(heth, PHYReg, pRegValue);
#else
    return HAL_ETH_WritePHYRegister(heth, PHYAddr, PHYReg, pRegValue);
#endif
}

static HAL_StatusTypeDef os_eth_phyreg_read(ETH_HandleTypeDef *heth, uint32_t PHYAddr, uint32_t PHYReg, uint32_t *pRegValue)
{
#ifndef SERIES_STM32H7
    return HAL_ETH_ReadPHYRegister(heth, PHYReg, pRegValue);
#else
    return HAL_ETH_ReadPHYRegister(heth, PHYAddr, PHYReg, pRegValue);
#endif
}
/* clang-format on */

static void phy_monitor_task_entry(void *parameter)
{
    os_bool_t   link_status     = 0;
    os_bool_t   link_status_new = 0;
    uint8_t     phy_addr        = 0xFF;
    uint8_t     detected_count  = 0;
    uint8_t     i               = 0;
    os_uint32_t reg             = 0;

    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)parameter;

    while (phy_addr == 0xFF)
    {
        for (i = 0; i <= 0x1F; i++)
        {
#ifndef SERIES_STM32H7
            eth_dev->EthHandle->Init.PhyAddress = i;
#endif
            os_eth_phyreg_read(eth_dev->EthHandle, i, PHY_ID1_REG, (uint32_t *)&reg);

            if (reg != 0xFFFF && reg != 0x00)
            {
                phy_addr = i;
                break;
            }
        }

        detected_count++;
        os_task_msleep(1000);

        if (detected_count > 10)
        {
            LOG_E(DRV_EXT_TAG, "No PHY device was detected, please check hardware!");
            return;
        }
    }

    os_eth_phyreg_write(eth_dev->EthHandle, phy_addr, PHY_BASIC_CONTROL_REG, PHY_RESET_MASK);
    os_task_msleep(2000);
    os_eth_phyreg_write(eth_dev->EthHandle, phy_addr, PHY_BASIC_CONTROL_REG, PHY_AUTO_NEGOTIATION_MASK);

    while (1)
    {
        os_eth_phyreg_read(eth_dev->EthHandle, phy_addr, PHY_BASIC_STATUS_REG, (uint32_t *)&reg);
        if (reg & (PHY_AUTONEGO_COMPLETE_MASK | PHY_LINKED_STATUS_MASK))
        {
            link_status_new = OS_TRUE;
        }
        else
        {
            link_status_new = OS_FALSE;
        }

        if (link_status != link_status_new)
        {
            os_net_linkchange(&eth_dev->net_dev, link_status_new);
        }

        link_status = link_status_new;

        os_task_msleep(1000);
    }
}

const static struct os_net_device_ops net_dev_ops = {
    .init       = os_stm32_eth_init,
    .deinit     = os_stm32_eth_deinit,
    .recv       = os_stm32_eth_recv,
    .send       = os_stm32_eth_send,
    .get_mac    = os_stm32_eth_get_macaddr,
    .set_filter = os_stm32_eth_set_filter,
};

static int stm32_eth_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_uint8_t eth_align = 64;

    struct os_stm32_eth *eth_dev = os_calloc(1, sizeof(struct os_stm32_eth));
    if (eth_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "No memory");
        goto __exit;
    }

    eth_dev->EthHandle = (ETH_HandleTypeDef *)dev->info;

    if (phy_gpio_init(eth_dev))
    {
        LOG_E(DRV_EXT_TAG, "phy_gpio_init failed");
        goto __exit;
    }

#ifndef SERIES_STM32H7
    eth_dev->rxbuff = (os_uint8_t *)os_dma_malloc_align(ETH_RXBUFNB * ETH_MAX_PACKET_SIZE, eth_align);
    if (eth_dev->rxbuff == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "No memory");
        goto __exit;
    }

    eth_dev->txbuff = (os_uint8_t *)os_dma_malloc_align(ETH_TXBUFNB * ETH_MAX_PACKET_SIZE, eth_align);
    if (eth_dev->txbuff == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "No memory");
        goto __exit;
    }

    eth_dev->DMARxDscrTab =
        (ETH_DMADescTypeDef *)os_dma_malloc_align(ETH_RXBUFNB * sizeof(ETH_DMADescTypeDef), eth_align);
    if (eth_dev->DMARxDscrTab == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "No memory");
        goto __exit;
    }

    eth_dev->DMATxDscrTab =
        (ETH_DMADescTypeDef *)os_dma_malloc_align(ETH_TXBUFNB * sizeof(ETH_DMADescTypeDef), eth_align);
    if (eth_dev->DMATxDscrTab == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "No memory");
        goto __exit;
    }

    /* OUI 00-80-E1 STMICROELECTRONICS. */
    eth_dev->dev_addr[0] = 0x00;
    eth_dev->dev_addr[1] = 0x80;
    eth_dev->dev_addr[2] = 0xE1;
    /* generate MAC addr from 96bit unique ID (only for test). */
    eth_dev->dev_addr[3] = *(os_uint8_t *)(UID_BASE + 4);
    eth_dev->dev_addr[4] = *(os_uint8_t *)(UID_BASE + 2);
    eth_dev->dev_addr[5] = *(os_uint8_t *)(UID_BASE + 0);
#else
    eth_dev->txbuff = (os_uint8_t *)os_dma_malloc_align(ETH_TX_DESC_CNT * ETH_MAX_PACKET_SIZE, eth_align);
    if (eth_dev->txbuff == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "No memory");
        goto __exit;
    }

    txbuff_dscrtab = (ETH_BufferTypeDef *)os_dma_malloc_align(ETH_TX_DESC_CNT * sizeof(ETH_BufferTypeDef), eth_align);
    rxbuff_dscrtab = (ETH_BufferTypeDef *)os_dma_malloc_align(ETH_RX_DESC_CNT * sizeof(ETH_BufferTypeDef), eth_align);
    if ((txbuff_dscrtab == OS_NULL) || (rxbuff_dscrtab == OS_NULL))
    {
        LOG_E(DRV_EXT_TAG, "No memory");
        goto __exit;
    }

    memset(eth_dev->dev_addr, 0, OS_NET_MAC_LENGTH);

    memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 1);
    memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 2);
    memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 3);
    memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 4);

    memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, OS_NET_MAC_LENGTH);

    eth_dev->DMATxDscrTab = DMATxDscrTab;
    eth_dev->DMARxDscrTab = DMARxDscrTab;
    eth_dev->rxbuff       = (os_uint8_t *)Rx_Buff;
#endif

    eth_dev->net_dev.info.MTU       = ETH_MAX_PACKET_SIZE;
    eth_dev->net_dev.info.MRU       = ETH_MAX_PACKET_SIZE;
    eth_dev->net_dev.info.mode      = net_dev_mode_sta;
    eth_dev->net_dev.info.intf_type = net_dev_intf_ether;
    eth_dev->net_dev.info.xfer_flag = OS_NET_XFER_TX_TASK | OS_NET_XFER_RX_TASK;
    eth_dev->net_dev.ops            = &net_dev_ops;

    if (os_net_device_register(&eth_dev->net_dev, dev->name) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_net_device_register failed");
        goto __exit;
    }

    eth_dev->phy_monitor = os_task_create("phy_monitor",
                                          phy_monitor_task_entry,
                                          eth_dev,
                                          ETH_MONITOR_TASK_STACK_SIEZ,
                                          ETH_MONITOR_TASK_PRIORITY);
    if (eth_dev->phy_monitor == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "monitor_task create failed");
        os_net_device_unregister(&eth_dev->net_dev);
        goto __exit;
    }

    os_list_add(&stm32_eth_list, &eth_dev->list);

    os_task_startup(eth_dev->phy_monitor);

    return OS_EOK;
__exit:

    if (eth_dev->txbuff)
    {
        os_dma_free_align(eth_dev->txbuff);
    }

#ifndef SERIES_STM32H7
    if (eth_dev->rxbuff)
    {
        os_dma_free_align(eth_dev->rxbuff);
    }

    if (eth_dev->DMARxDscrTab)
    {
        os_dma_free_align(eth_dev->DMARxDscrTab);
    }

    if (eth_dev->DMATxDscrTab)
    {
        os_dma_free_align(eth_dev->DMATxDscrTab);
    }
#else
    if (txbuff_dscrtab)
    {
        os_dma_free_align(txbuff_dscrtab);
    }
    if (rxbuff_dscrtab)
    {
        os_dma_free_align(rxbuff_dscrtab);
    }
#endif
    if (eth_dev)
    {
        os_free(eth_dev);
    }

    return OS_EOK;
}

OS_DRIVER_INFO stm32_eth_driver = {
    .name  = "ETH_HandleTypeDef",
    .probe = stm32_eth_probe,
};

OS_DRIVER_DEFINE(stm32_eth_driver, DEVICE, OS_INIT_SUBLEVEL_LOW);
