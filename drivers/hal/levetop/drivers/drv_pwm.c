/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_pwm.c
 *
 * @brief       This file implements PWM driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <drv_pwm.h>
#include <pwm.h>
#include <sys.h>
#include <ioctrl_drv.h>

#define DBG_TAG "drv.pwm"
#include <dlog.h>

void __weak PWM_RisingCallback(void *hpwm)
{
}

void __weak PWM_FallingCallback(void *hpwm)
{
}

void __weak PWM_CaptureCallback(void *hpwm)
{
}

static os_err_t PWM_ConfigCallbackRegistry(PWM_HandleTypeDef *hpwm, pwm_callback_t callback, os_uint8_t index)
{
    switch (index)
    {
    case 0:
        hpwm->RisingCallback = callback;
        break;
    case 1:
        hpwm->FallingCallback = callback;
        break;
    case 2:
        hpwm->CaptureCallback = callback;
        break;
    default:
        return OS_ERROR;
    }

    return OS_EOK;
}

static os_err_t PWM_InputInit(PWM_HandleTypeDef *hpwm)
{
    DRV_PWM_CHxCmd(hpwm->instance, hpwm->init.channel, DISABLE);
    //配置死区
    if (hpwm->init.in.deadzone)
    {
        DRV_PWM_ConfigCHxDeadZone(hpwm->instance, hpwm->init.channel, hpwm->init.in.deadzone);
        DRV_PWM_CHxDeadZoneCmd(hpwm->instance, hpwm->init.channel, ENABLE);
    }
    else
    {
        DRV_PWM_CHxDeadZoneCmd(hpwm->instance, hpwm->init.channel, DISABLE);
    }

    //配置预分频系数
    DRV_PWM_ConfigCHxPerscaler(hpwm->instance, hpwm->init.channel, hpwm->init.in.prescaler);

    //配置分频系数
    DRV_PWM_ConfigCHxClkDiv(hpwm->instance, hpwm->init.channel, hpwm->init.in.clkdiv);

    //配置PWM载入值模式
    DRV_PWM_CHxAutoLoadMode(hpwm->instance, hpwm->init.channel, hpwm->init.in.autoload);

    //配置PWM捕获周期
    DRV_PWM_ConfigCHxPeriod(hpwm->instance, hpwm->init.channel, hpwm->init.in.period);

    //配置PWM方向
    DRV_PWM_SetCHxDirection(hpwm->instance, hpwm->init.channel, hpwm->init.direction);

    //配置PWM通道翻转
    DRV_PWM_CHxInverteCmd(hpwm->instance, hpwm->init.channel, hpwm->init.in.CHxInv);

    //配置边沿触发中断
    DRV_PWM_ConfigCHxEdgeinterrupt(hpwm->instance, hpwm->init.channel, hpwm->init.in.edgeinterrupt);

    //注册回调函数
    if (hpwm->init.in.edgeinterrupt)
    {
        PWM_ConfigCallbackRegistry(hpwm, PWM_RisingCallback, 0);
        PWM_ConfigCallbackRegistry(hpwm, PWM_FallingCallback, 1);
        PWM_ConfigCallbackRegistry(hpwm, PWM_CaptureCallback, 2);

        //配置中断优先级
        switch (hpwm->init.channel)
        {
        case 0:
            NVIC_Init(3, 3, PWM0_IRQn, 2);
            break;
        case 1:
            NVIC_Init(3, 3, PWM1_IRQn, 2);
            break;
        case 2:
            NVIC_Init(3, 3, PWM2_IRQn, 2);
            break;
        case 3:
            NVIC_Init(3, 3, PWM3_IRQn, 2);
            break;
        case 4:
            NVIC_Init(3, 3, PWM4_IRQn, 2);
            break;
        case 5:
            NVIC_Init(3, 3, PWM5_IRQn, 2);
            break;
        case 6:
            NVIC_Init(3, 3, PWM6_IRQn, 2);
            break;
        case 7:
            NVIC_Init(3, 3, PWM7_IRQn, 2);
            break;
        default:
            return OS_ERROR;
        }
    }

    DRV_PWM_CHxCmd(hpwm->instance, hpwm->init.channel, ENABLE);
    return OS_EOK;
}

static os_err_t PWM_OutputInit(PWM_HandleTypeDef *hpwm)
{
    //关闭PWM
    DRV_PWM_CHxCmd(hpwm->instance, hpwm->init.channel, DISABLE);

    //配置死区
    if (hpwm->init.out.deadzone)
    {
        DRV_PWM_ConfigCHxDeadZone(hpwm->instance, hpwm->init.channel, hpwm->init.out.deadzone);
        DRV_PWM_CHxDeadZoneCmd(hpwm->instance, hpwm->init.channel, ENABLE);
        //配置PWM1 PWM3 PWM5 PWM7
        switch (hpwm->init.channel + 1)
        {
        case 0:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP16);
            break;
        case 1:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP17);
            break;
        case 2:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP18);
            break;
        case 3:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP19);
            break;
        case 4:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP20);
            break;
        case 5:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP21);
            break;
        case 6:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP22);
            break;
        case 7:
            DRV_IOCTRL_SwapEn(IOCTRL_SWAP23);
            break;

        default:
            return OS_ERROR;
        }
        //配置PWM方向
        DRV_PWM_SetCHxDirection(hpwm->instance, hpwm->init.channel + 1, hpwm->init.direction);

        //配置PWM时钟翻转
        DRV_PWM_CHxTimerInverteCmd(hpwm->instance, hpwm->init.channel + 1, hpwm->init.out.timerInv);

        //配置PWM拉高使能
        DRV_PWM_CHxPullupCmd(hpwm->instance, hpwm->init.channel + 1, hpwm->init.out.pullup);

        //配置PWM初始电平
        DRV_PWM_SetCHxLevel(hpwm->instance, hpwm->init.channel + 1, hpwm->init.out.level);

        //配置timer中断
        DRV_PWM_CHxTimerInterruptCmd(hpwm->instance, hpwm->init.channel + 1, hpwm->init.out.timerInterrupt);

        //打开PWM
        DRV_PWM_CHxCmd(hpwm->instance, hpwm->init.channel + 1, ENABLE);
    }
    else
    {
        DRV_PWM_CHxDeadZoneCmd(hpwm->instance, hpwm->init.channel, DISABLE);
    }
    //配置分频系数
    DRV_PWM_ConfigCHxClkDiv(hpwm->instance, hpwm->init.channel, hpwm->init.out.clkdiv);

    //配置预分频值
    DRV_PWM_ConfigCHxPerscaler(hpwm->instance, hpwm->init.channel, hpwm->init.out.prescaler);

    //配置PWM载入值模式
    DRV_PWM_CHxAutoLoadMode(hpwm->instance, hpwm->init.channel, hpwm->init.out.autoload);

    //配置PWM波形脉冲宽度
    DRV_PWM_ConfigCHxWidth(hpwm->instance, hpwm->init.channel, hpwm->init.out.width);

    //配置PWM波形周期频率
    DRV_PWM_ConfigCHxPeriod(hpwm->instance, hpwm->init.channel, hpwm->init.out.period);

    //配置PWM方向
    DRV_PWM_SetCHxDirection(hpwm->instance, hpwm->init.channel, hpwm->init.direction);

    //配置PWM时钟翻转
    DRV_PWM_CHxTimerInverteCmd(hpwm->instance, hpwm->init.channel, hpwm->init.out.timerInv);

    //配置PWM拉高使能
    DRV_PWM_CHxPullupCmd(hpwm->instance, hpwm->init.channel, hpwm->init.out.pullup);

    //配置PWM初始电平
    DRV_PWM_SetCHxLevel(hpwm->instance, hpwm->init.channel, hpwm->init.out.level);

    //配置timer中断
    DRV_PWM_CHxTimerInterruptCmd(hpwm->instance, hpwm->init.channel, hpwm->init.out.timerInterrupt);

    //打开PWM
    DRV_PWM_CHxCmd(hpwm->instance, hpwm->init.channel, ENABLE);
    return OS_EOK;
}

static os_err_t InitPwm(PWM_HandleTypeDef *hpwm)
{
    // PWM管脚复用
    switch (hpwm->init.channel)
    {
    case 0:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP16);
        break;
    case 1:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP17);
        break;
    case 2:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP18);
        break;
    case 3:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP19);
        break;
    case 4:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP20);
        break;
    case 5:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP21);
        break;
    case 6:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP22);
        break;
    case 7:
        DRV_IOCTRL_SwapEn(IOCTRL_SWAP23);
        break;
    default:
        return OS_ERROR;
    }

    if (hpwm->init.direction == PWM_INPUT)    //输入
    {
        return PWM_InputInit(hpwm);
    }
    else    //输出
    {
        return PWM_OutputInit(hpwm);
    }
}

static os_err_t lt776_pwm_enabled(struct os_pwm_device *dev, os_uint32_t channel, os_bool_t enable)
{
    struct lt776_pwm *pwm;

    pwm = os_container_of(dev, struct lt776_pwm, pwm);

    pwm->info->hpwm.init.channel = channel;
    InitPwm(&pwm->info->hpwm);

    if (enable)
    {
        DRV_PWM_CHxCmd(pwm->info->hpwm.instance, channel, ENABLE);
    }
    else
    {
        DRV_PWM_CHxCmd(pwm->info->hpwm.instance, channel, DISABLE);
    }

    return OS_EOK;
}

static os_err_t lt776_pwm_set_period(struct os_pwm_device *dev, os_uint32_t channel, os_uint32_t period)
{
    struct lt776_pwm *pwm;
    pwm = os_container_of(dev, struct lt776_pwm, pwm);

    pwm->info->hpwm.init.out.period =
        (os_uint64_t)period * g_ips_clk / NSEC_PER_SEC / (pwm->info->hpwm.init.out.prescaler + 1) >> 4;
    DRV_PWM_ConfigCHxPeriod(pwm->info->hpwm.instance, channel, pwm->info->hpwm.init.out.period);
    return OS_EOK;
}

static os_err_t lt776_pwm_set_pulse(struct os_pwm_device *dev, os_uint32_t channel, os_uint32_t duty)
{
    struct lt776_pwm *pwm;
    pwm = os_container_of(dev, struct lt776_pwm, pwm);

    //配置PWM波形脉冲宽度
    pwm->info->hpwm.init.out.width =
        (os_uint64_t)duty * g_ips_clk / NSEC_PER_SEC / (pwm->info->hpwm.init.out.prescaler + 1) >> 4;
    DRV_PWM_ConfigCHxWidth(pwm->info->hpwm.instance, channel, pwm->info->hpwm.init.out.width);
    return OS_EOK;
}

const static struct os_pwm_ops lt776_pwm_ops = {
    .enabled    = lt776_pwm_enabled,
    .set_period = lt776_pwm_set_period,
    .set_pulse  = lt776_pwm_set_pulse,
};

static int lt776_pwm_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct lt776_pwm *pwm = os_calloc(1, sizeof(struct lt776_pwm));
    OS_ASSERT(pwm);

    pwm->info    = (struct lt776_pwm_info *)dev->info;
    pwm->pwm.ops = &lt776_pwm_ops;

    os_device_pwm_register(&pwm->pwm, dev->name);
    return 0;
}

OS_DRIVER_INFO lt776_pwm_driver = {
    .name  = "PWM_TypeDef",
    .probe = lt776_pwm_probe,
};

OS_DRIVER_DEFINE(lt776_pwm_driver, PREV, OS_INIT_SUBLEVEL_MIDDLE);
