/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_wdt.c
 *
 * @brief       This file implements watchdog driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_wdt.h>

#define DBG_TAG "drv.wdt"
#include <dlog.h>

struct lt776_wdt
{
    os_watchdog_t          wdg;
    struct lt776_wdt_info *info;
};

static void lt776_wdt_start(struct lt776_wdt *wdt)
{
    DRV_WDT_Open();
}

static void lt776_wdt_refresh(struct lt776_wdt *wdt)
{
    DRV_WDT_FeedDog();
}

static os_err_t lt776_wdt_init(os_watchdog_t *wdt)
{
    os_uint32_t cnt = IWDT_TIMEOUT * (DRV_CPM_GetIPSClHz() >> 12) / 1000;
    DRV_WDT_Init(cnt);

    return OS_EOK;
}

static os_err_t lt776_wdt_get_timeout(struct lt776_wdt *wdt, os_uint32_t *timeout_ms)
{
    *timeout_ms = 1000 * (wdt->info->inst->WMR) / (DRV_CPM_GetIPSClHz() >> 12);

    return OS_EOK;
}

static os_err_t lt776_wdt_control(os_watchdog_t *wdt, int cmd, void *arg)
{
    struct lt776_wdt *wdg = (struct lt776_wdt *)wdt;

    switch (cmd)
    {
    case OS_DEVICE_CTRL_WDT_KEEPALIVE:
        lt776_wdt_refresh(wdg);
        return OS_EOK;

    case OS_DEVICE_CTRL_WDT_SET_TIMEOUT:
        break;

    case OS_DEVICE_CTRL_WDT_GET_TIMEOUT:
        return lt776_wdt_get_timeout(wdg, (os_uint32_t *)arg);

    case OS_DEVICE_CTRL_WDT_START:
        lt776_wdt_start(wdg);
        return OS_EOK;
    }

    return OS_ENOSYS;
}

const static struct os_watchdog_ops ops = {
    .init    = &lt776_wdt_init,
    .control = &lt776_wdt_control,
};

static int lt776_wdt_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct lt776_wdt *wdt = os_calloc(1, sizeof(struct lt776_wdt));
    OS_ASSERT(wdt);

    wdt->info    = (struct lt776_wdt_info *)dev->info;
    wdt->wdg.ops = &ops;

    return os_hw_watchdog_register(&wdt->wdg, dev->name, OS_NULL);
}

OS_DRIVER_INFO lt776_wdt_driver = {
    .name  = "WDT_TypeDef",
    .probe = lt776_wdt_probe,
};

OS_DRIVER_DEFINE(lt776_wdt_driver, PREV, OS_INIT_SUBLEVEL_LOW);
