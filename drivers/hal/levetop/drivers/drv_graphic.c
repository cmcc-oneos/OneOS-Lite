/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_graphic.c
 *
 * @brief       This file provides graphic driver functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_graphic.h>

#define DBG_TAG "LCD"
#include <dlog.h>

os_uint32_t GetSdramAddr(os_uint8_t curAddr, os_uint8_t copySreen, CLCD_TypeDef *hclcd)
{
    os_uint32_t i;
    os_uint32_t fbAddr;

    if (curAddr)
        return _clcd_get_upbase(hclcd);

    if (_clcd_get_upbase(hclcd) == LCDC_ADDR0)
    {
        fbAddr = LCDC_ADDR1;
        if (copySreen)
            for (i = 0; i < (SCREEN_SIZE >> 1); i++)
            {
                *(__IO os_uint16_t *)(fbAddr + (i << 1)) = *(__IO os_uint16_t *)(LCDC_ADDR0 + (i << 1));
            }
    }
    else
    {
        fbAddr = LCDC_ADDR0;
        if (copySreen)
            for (i = 0; i < (SCREEN_SIZE >> 1); i++)
            {
                *(__IO os_uint16_t *)(fbAddr + (i << 1)) = *(__IO os_uint16_t *)(LCDC_ADDR1 + (i << 1));
            }
    }

    return fbAddr;
}

static void lt766_fill_color(os_uint16_t        x,
                             os_uint16_t        y,
                             os_uint16_t        w,
                             os_uint16_t        h,
                             os_uint16_t        color,
                             CLCD_HandleTypeDef hclcd)
{
    os_int32_t  i, j;
    os_uint32_t disAddr;
    disAddr = GetSdramAddr(1, 0, hclcd.instance);

    for (j = 0; j < h; j++)
    {
        for (i = 0; i < w; i++)
        {
            *(os_uint16_t *)(disAddr + OS_GRAPHIC_LCD_WIDTH * ((j + y) << 1) + ((i + x) << 1)) = color;
        }
    }

    DRV_CLCD_UpdateBaseAddr(disAddr);
}

static void CLCD_Init(CLCD_HandleTypeDef *hclcd)
{
    DRV_CLCD_SetMisc(hclcd->instance, hclcd->init.enableIE);
    DRV_CLCD_SetClkDiv(hclcd->instance, hclcd->init.clcd_clkdiv);

    DRV_CLCD_SetTiming0(hclcd->instance,
                        hclcd->init.clcd_timeing.hbp,
                        hclcd->init.clcd_timeing.hfp,
                        hclcd->init.clcd_timeing.hsync,
                        hclcd->init.clcd_timeing.width / OS_GRAPHIC_LCD_DEPTH);

    DRV_CLCD_SetTiming1(hclcd->instance,
                        hclcd->init.clcd_timeing.vbp,
                        hclcd->init.clcd_timeing.vfp,
                        hclcd->init.clcd_timeing.vsync,
                        hclcd->init.clcd_timeing.height);

    DRV_CLCD_SetTiming2(hclcd->instance,
                        hclcd->init.clcd_timeing.width,
                        hclcd->init.clcd_timeing.flags & (IEO | IPC | IHS | IVS));

    DRV_CLCD_SetTiming3(hclcd->instance, 0, FALSE);

    DRV_CLCD_SetControlRegister(hclcd->instance, hclcd->init.clcd_cfg.control);
}

static void lt766_display_on(struct os_device *dev, os_bool_t on_off)
{
    if (on_off)
    {
        os_pin_mode(BACKLIGHT_PWR_PIN, PIN_MODE_OUTPUT);
    }
    else
    {
        struct lt776_lcd *lt776 = os_container_of((os_graphic_t *)dev, struct lt776_lcd, graphic);
        lt766_fill_color(0,
                         0,
                         OS_GRAPHIC_LCD_WIDTH,
                         OS_GRAPHIC_LCD_HEIGHT,
                         OS_COLOR_TO16(OS_COLOR_BLACK),
                         lt776->hclcd);
        os_pin_mode(BACKLIGHT_PWR_PIN, PIN_MODE_OUTPUT_OD);
    }
}

static int lt776_lcd_init(struct lt776_lcd *lt776)
{
    os_uint32_t i;
    lt776->hclcd.instance                 = CLCD;
    lt776->hclcd.init.enableIE            = ENABLE;
    lt776->hclcd.init.clcd_timeing.width  = OS_GRAPHIC_LCD_WIDTH;
    lt776->hclcd.init.clcd_timeing.height = OS_GRAPHIC_LCD_HEIGHT;
    lt776->hclcd.init.clcd_timeing.hbp    = 26;
    lt776->hclcd.init.clcd_timeing.hfp    = 210;
    lt776->hclcd.init.clcd_timeing.vbp    = 13;
    lt776->hclcd.init.clcd_timeing.vfp    = 22;
    lt776->hclcd.init.clcd_timeing.hsync  = 20;
    lt776->hclcd.init.clcd_timeing.vsync  = 10;
    lt776->hclcd.init.clcd_timeing.flags  = (LCD_TIMING_ACTIVE_HIGH_OE | LCD_TIMING_ACTIVE_HIGH_PIXCLK |
                                            LCD_TIMING_ACTIVE_LOW_HSYNC | LCD_TIMING_ACTIVE_LOW_VSYNC);

#if (FREQUENCY_MHZ == 500)
    lt776->hclcd.init.clcd_clkdiv = 15;
#else
    lt776->hclcd.init.clcd_clkdiv = 12;
#endif

    lt776->hclcd.init.clcd_cfg.control = (LCD_FMT_GAMMA_CORR_DISABLE | LCD_FMT_INPUT_RGB565);

#if (defined BSP_USING_LCD_INPUT_LITTLE_ENDIAN)
    lt776->hclcd.init.clcd_cfg.control |= LCD_FMT_INPUT_LITTLE_ENDIAN;
#elif (defined BSP_USING_LCD_INPUT_BIG_ENDIAN)
    lt776->hclcd.init.clcd_cfg.control |= LCD_FMT_INPUT_BIG_ENDIAN;
#endif

#if (defined BSP_USING_LCD_OUTPUT_BGR)
    lt776->hclcd.init.clcd_cfg.control |= LCD_FMT_OUTPUT_BGR;
#elif (defined BSP_USING_LCD_OUTPUT_RGB)
    lt776->hclcd.init.clcd_cfg.control |= LCD_FMT_OUTPUT_RGB;
#endif

    lt776->hclcd.init.clcd_cfg.fb0_addr = LCDC_ADDR0;
    lt776->hclcd.init.clcd_cfg.fb1_addr = LCDC_ADDR1;
    CLCD_Init(&lt776->hclcd);

    _clcd_set_upbase(lt776->hclcd.instance, LCDC_ADDR0);
    DRV_CLCD_Cmd(lt776->hclcd.instance, ENABLE);

    for (i = 0; i < (SCREEN_SIZE >> 1); i++)
    {
        *(__IO os_uint16_t *)(LCDC_ADDR0 + (i << 1)) = OS_COLOR_TO16(OS_COLOR_BLACK);
    }
    return OS_EOK;
}

static void lt766_frame_flush(struct os_device *dev)
{
    os_graphic_t *graphic = (os_graphic_t *)dev;
    OS_ASSERT(graphic->info.framebuffer_curr);

    DRV_CLCD_UpdateBaseAddr((os_uint32_t)graphic->info.framebuffer_curr);
}

static void lt766_display_area(struct os_device *dev, os_graphic_area_t *area)
{
    struct lt776_lcd *lt776 = os_container_of((os_graphic_t *)dev, struct lt776_lcd, graphic);

    lt766_fill_color(area->x, area->y, area->w, area->h, OS_COLOR_TO16(area->color), lt776->hclcd);
}

const static struct os_graphic_ops ops = {
    .display_on   = lt766_display_on,
    .display_area = lt766_display_area,
    .frame_flush  = lt766_frame_flush,
};

static int os_hw_lcd_init(void)
{
    os_uint8_t       *fb;
    struct lt776_lcd *lt776 = OS_NULL;

    lt776 = os_calloc(1, sizeof(struct lt776_lcd));
    OS_ASSERT(lt776);

    lt776->graphic.ops = &ops;
    os_graphic_register("lcd", &lt776->graphic);

    InitSdram();
    fb = (os_uint8_t *)LCDC_ADDR0;
    os_graphic_add_framebuffer(&lt776->graphic.parent, fb, SCREEN_SIZE);

    fb = (os_uint8_t *)LCDC_ADDR1;
    os_graphic_add_framebuffer(&lt776->graphic.parent, fb, SCREEN_SIZE);

    lt776_lcd_init(lt776);

    // back light on
    lt766_display_on(&lt776->graphic.parent, OS_TRUE);

    return OS_EOK;
}

OS_DEVICE_INIT(os_hw_lcd_init, OS_INIT_SUBLEVEL_LOW);
