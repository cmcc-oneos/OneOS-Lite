/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash.c
 *
 * @brief       The file of flash drv for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <eflash_drv.h>
#include <drv_flash.h>
#include <cache_drv.h>

#include <fal_drv_flash.c>

/**
 * Read data from flash.
 * @note This operation's units is word.
 *
 * @param addr flash address
 * @param buf buffer to store read data
 * @param size read bytes size
 *
 * @return result
 */
int lt776_flash_read(os_uint32_t addr, os_uint8_t *buf, os_size_t size)
{
    if ((addr + size) > LT776_FLASH_END_ADDRESS)
    {
        os_kprintf("read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_EINVAL;
    }

    DRV_EFM_Read(addr, buf, size);

    return size;
}

static os_err_t EflashWrite(os_uint32_t flash_addr, const os_uint8_t *ram_addr, os_uint32_t len)
{
    os_uint16_t offset;
    os_uint16_t pageleft;
    os_uint32_t dest = flash_addr;
    os_uint8_t *src  = (os_uint8_t *)ram_addr;
    os_uint8_t  result;
    os_uint32_t eflash_buffer[LT776_FLASH_PAGE_SIZE / 4] = {0};
    os_uint16_t page_mask                                = LT776_FLASH_PAGE_SIZE - 1;

    if (flash_addr >= LT776_FLASH_END_ADDRESS)
        return FALSE;

    offset   = ((os_uint32_t)dest & page_mask);
    pageleft = LT776_FLASH_PAGE_SIZE - offset;

    if (offset != 0)
    {
        dest -= offset;
        memcpy((os_uint8_t *)eflash_buffer, (os_uint8_t *)dest, LT776_FLASH_PAGE_SIZE);
        if (len > pageleft)
        {
            memcpy((os_uint8_t *)eflash_buffer + offset, src, pageleft);
            len -= pageleft;
            src += pageleft;
        }
        else
        {
            memcpy((os_uint8_t *)eflash_buffer + offset, src, len);
            len = 0;
        }
        result = DRV_EFM_EreasePage(dest);
        result = DRV_EFM_ProgramBulk(dest, (os_uint8_t *)eflash_buffer, LT776_FLASH_PAGE_SIZE / 4);
        dest += LT776_FLASH_PAGE_SIZE;
    }
    while (len > 0)
    {
        if (len >= LT776_FLASH_PAGE_SIZE)
        {
            memcpy((os_uint8_t *)eflash_buffer, src, LT776_FLASH_PAGE_SIZE);
            result = DRV_EFM_EreasePage(dest);
            result = DRV_EFM_ProgramBulk(dest, (os_uint8_t *)eflash_buffer, LT776_FLASH_PAGE_SIZE / 4);

            dest += LT776_FLASH_PAGE_SIZE;
            src += LT776_FLASH_PAGE_SIZE;
            len -= LT776_FLASH_PAGE_SIZE;
        }
        else
        {
            memcpy((os_uint8_t *)eflash_buffer, (os_uint8_t *)dest, LT776_FLASH_PAGE_SIZE);
            memcpy((os_uint8_t *)eflash_buffer, src, len);
            result = DRV_EFM_EreasePage(dest);
            result = DRV_EFM_ProgramBulk(dest, (os_uint8_t *)eflash_buffer, LT776_FLASH_PAGE_SIZE / 4);
            len    = 0;
        }
    }

    return (result == 0 ? OS_EOK : OS_ERROR);
}

/**
 * Write data to flash.
 * @note This operation's units is word.
 * @note This operation must after erase. @see flash_erase.
 *
 * @param addr flash address
 * @param buf the write data buffer
 * @param size write bytes size
 *
 * @return result
 */
int lt776_flash_write(os_uint32_t addr, const os_uint8_t *buf, os_size_t size)
{
    if ((addr + size) > LT776_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: write outrange flash size! addr is (0x%p)\n", (void *)(addr + size));
        return OS_EINVAL;
    }

    if (addr % 4 != 0)
    {
        os_kprintf("write addr must be 4-byte alignment");
        return OS_EINVAL;
    }

    if (size < 1)
    {
        return OS_ERROR;
    }

    EflashWrite(addr, buf, size);

    return size;
}

/**
 * Erase data on flash.
 * @note This operation is irreversible.
 * @note This operation's units is different which on many chips.
 *
 * @param addr flash address
 * @param size erase bytes size
 *
 * @return result
 */
int lt776_flash_erase(os_uint32_t addr, os_size_t size)
{
    os_uint16_t page_start = 0;
    os_uint16_t page_end   = 0;
    os_uint16_t page_cnt   = 0;

    if ((addr + size) > LT776_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: erase outrange flash size! addr is (0x%p)\n", (void *)(addr + size));
        return OS_EINVAL;
    }

    page_start = addr / LT776_FLASH_PAGE_SIZE;
    page_end   = (addr + size) / LT776_FLASH_PAGE_SIZE + (((addr + size) % LT776_FLASH_PAGE_SIZE) ? 1 : 0);

    DeInitCache();

    for (page_cnt = 0; page_cnt < (page_end - page_start); page_cnt++)
    {
        DRV_EFM_EreasePage((page_start + page_cnt) * LT776_FLASH_PAGE_SIZE);
    }

    InitCache();

    return size;
}
