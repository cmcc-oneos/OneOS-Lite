/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        soft_i2c_bus.c
 *
 * @brief       this file implements i2c driver related functions
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-02-20    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_i2c.h>

#define DBG_TAG "drv.i2c"
#include <drv_log.h>

static I2C_InitTypeDef gI2cInit;
static os_list_node_t  lt776_i2c_list = OS_LIST_INIT(lt776_i2c_list);

static os_err_t lt776_i2c_init(struct lt776_i2c *dev)
{
    gI2cInit.Mode      = I2C_MODE_MASTER;
    gI2cInit.AddBits   = 0x00;
    gI2cInit.HighSpeed = ENABLE;
    gI2cInit.ClockMode = I2C_CLOCK_MODE_NORMAL;
    gI2cInit.Prescaler = 28;

    DRV_I2C_Init(I2C1, &gI2cInit);

    return OS_EOK;
}

static os_size_t lt776_i2c_transfer(struct os_i2c_bus_device *bus, struct os_i2c_msg msgs[], os_uint32_t num)
{
    if (msgs[0].flags & OS_I2C_RD)
    {
        DRV_I2C_Receive(I2C1, msgs[0].addr << 1, gI2cInit.HighSpeed, msgs[0].buf, msgs[0].len, 0);
    }
    else
    {
        DRV_I2C_Transmit(I2C1, msgs[0].addr << 1, gI2cInit.HighSpeed, msgs[0].buf, msgs[0].len, 0);
    }

    return num;
}

static const struct os_i2c_bus_device_ops i2c_bus_ops = {
    .i2c_transfer = lt776_i2c_transfer,
};

static int lt776_i2c_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_base_t level;

    struct lt776_i2c *lt_i2c = os_calloc(1, sizeof(struct lt776_i2c));
    OS_ASSERT(lt_i2c);

    lt776_i2c_init(lt_i2c);

    struct os_i2c_bus_device *dev_i2c = &lt_i2c->i2c;
    dev_i2c->ops                      = &i2c_bus_ops;

    level = os_irq_lock();
    os_list_add_tail(&lt776_i2c_list, &lt_i2c->list);
    os_irq_unlock(level);

    return os_i2c_bus_device_register(dev_i2c, dev->name, dev_i2c);
}

OS_DRIVER_INFO lt776_i2c_driver = {
    .name  = "I2C_TypeDef",
    .probe = lt776_i2c_probe,
};

OS_DRIVER_DEFINE(lt776_i2c_driver, PREV, OS_INIT_SUBLEVEL_HIGH);
