/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_sd.h
 *
 * @brief       This file implements sd driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_SD_H__
#define __DRV_SD_H__

#include <board.h>
#include <sdio_drv.h>

struct lt776_sd_info
{
    os_uint32_t block_size;
    os_uint64_t capacity;
};

struct lt776_sd_device
{
    os_blk_device_t       blk_dev;
    struct lt776_sd_info *info;
};

#endif
