/**********************************************************
* @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
* @File			cache_drv.h
* @Author		Jason Zhao
* @Date			2021/12/2
* @Time			10:46:2
* @Version		
* @Brief			
**********************************************************
* Modification History
*
*
**********************************************************/
#ifndef __CACHE_DRV_H__
#define __CACHE_DRV_H__

#include "lt776_reg.h"

 
#define PAGE_CACHE_CLEAN_GO		0x00000001
 
#define INVW1   ((1)<<26)
#define INVW0   ((1)<<24)
#define PUSHW0  ((1)<<25)
#define PUSHW1  ((1)<<27)
#define GO      (((uint32_t)1)<<31)
#define ENWRBUF ((1)<<1)
#define ENCACHE ((1)<<0)
 
#define LGO     (0x01)
 
#define CACHE_LINE_SIZE  0x10
#define CACHE_LINE_MASK  0x0f
 
#define R0_WT_WB				((1)<<0)
#define R0_ENCACHE				((1)<<1)
#define R2_WT_WB				((1)<<4)
#define R2_ENCACHE				((1)<<5)
#define RAM0_WT_WB				((1)<<12)
#define RAM0_ENCACHE			((1)<<13)
 
#define WRITE_BACK				(0xFF)
#define WRITE_THROUGH			(0xaa)
 
#define EFLASH_WRITE_BACK			(0xff)
#define EFLASH_WRITE_THROUGH	(0xaa)
 
#define BOOT_CACHEOFF			0xff00ffff
#define ROM_CACHEOFF			0xfffcffff
 
#define SPIM1_CACHEOFF		0xffff00ff
#define SPIM2_CACHEOFF		0xffffff00
#define SDRAM_CACHEOFF		0x00ffffff
 
#define EFLASH_CACHEOFF		0x00ffffff
 
#define BOOT_CACHE_SHIFT	(16)
#define ROM_CACHE_SHIFT		(16)
#define SPIM1_CACHE_SHIFT	(8)
#define SPIM2_CACHE_SHIFT	(0)
#define EFLASH_CACHE_SHIFT (24)
#define SDRAM_CACHE_SHIFT  (24)

 /**
* @brief  CACHE ģʽ����
*
*/
typedef enum
{
    CACHE_Off = 0,
    CACHE_Through,
    CACHE_Back,
}CACHE_ComTypeDef;

typedef struct
{
	CACHE_ComTypeDef SPIM1;
	CACHE_ComTypeDef SPIM2;
	CACHE_ComTypeDef SDRAMC;
	CACHE_ComTypeDef EFLASH;
	CACHE_ComTypeDef ROM;
	CACHE_ComTypeDef BOOT;

} CACHE_InitTypeDef;


extern void InitCache(void);
extern void SynCache(void);
extern void DeInitCache(void);



#endif  /* __CACHE_DRV_H__*/

