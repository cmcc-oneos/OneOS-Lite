/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    ccm_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of CCM module.
  *
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CCM_DRV_H
#define __CCM_DRV_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"
#ifdef FILE_OUT
/**@defgroup DRV 3 DRV driver
  *
  *@{
  */

/** @defgroup DRV_CCM CCM
  *
  *@{
  */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_CCM_Exported_Macros Exported Macros
  *
  * @{
  */
#endif

#define _ccm_set_fdcr(ccm, value) _reg_write(ccm->FDCR, value) /**< 设置FD寄存器      */
#define _ccm_get_fdcr_all(ccm) _reg_read(ccm->FDCR)            /**< 读取FD寄存器      */

#define _ccm_set_bmt(ccm, value) _reg_modify(ccm->CCR, 0xfffc, value)                 /**< 设置总线监视器超时值      */
#define _ccm_en_bmd(ccm) _bit_set(ccm->CCR, CCM_CCR_BMD)                              /**< 使能总线监视器调试模式      */
#define _ccm_dis_bmd(ccm) _bit_clr(ccm->CCR, CCM_CCR_BMD)                             /**< 禁用总线监视器调试模式      */
#define _ccm_en_bme(ccm) _bit_set(ccm->CCR, CCM_CCR_BME)                              /**< 使能总线监视器      */
#define _ccm_dis_bme(ccm) _bit_clr(ccm->CCR, CCM_CCR_BME)                             /**< 禁用总线监视器      */
#define _ccm_en_shint(ccm) _bit_set(ccm->CCR, CCM_CCR_SHINT)                          /**< 使能显示中断      */
#define _ccm_dis_shint(ccm) _bit_clr(ccm->CCR, CCM_CCR_SHINT)                         /**< 禁用显示中断      */
#define _ccm_en_periph_bridge_pae(ccm) _bit_set(ccm->CCR, CCM_CCR_PERIPH_BRIDGE_PAE)  /**< 使能periph_bridge_pae      */
#define _ccm_dis_periph_bridge_pae(ccm) _bit_clr(ccm->CCR, CCM_CCR_PERIPH_BRIDGE_PAE) /**< 禁用periph_bridge_pae      */
#define _ccm_en_periph_bridge_rae(ccm) _bit_set(ccm->CCR, CCM_CCR_PERIPH_BRIDGE_RAE)  /**< 使能periph_bridge_rae      */
#define _ccm_dis_periph_bridge_rae(ccm) _bit_clr(ccm->CCR, CCM_CCR_PERIPH_BRIDGE_RAE) /**< 禁用periph_bridge_rae      */
#define _ccm_set_sysclk_internal_osc(ccm) _bit_set(ccm->CCR, CCM_CCR_CLKMODE_PDE)     /**< 选取内部晶振为系统时钟源      */
#define _ccm_set_sysclk_external_osc(ccm) _bit_clr(ccm->CCR, CCM_CCR_CLKMODE_PDE)     /**< 选取外部晶振为系统时钟源      */
#define _ccm_dis_jtag(ccm) _bit_set(ccm->CCR, CCM_CCR_JTAGDIS)                        /**< 禁用Jtag功能      */
#define _ccm_en_jtag(ccm) _bit_clr(ccm->CCR, CCM_CCR_JTAGDIS)                         /**< 使能Jtag功能      */
#define _ccm_dis_btld(ccm) _bit_set(ccm->CCR, CCM_CCR_BTLDDIS)                        /**< 禁用bootloader功能      */
#define _ccm_en_btld(ccm) _bit_clr(ccm->CCR, CCM_CCR_BTLDDIS)                         /**< 使能bootloader功能      */
#define _ccm_dis_test(ccm) _bit_set(ccm->CCR, CCM_CCR_TESTDIS)                        /**< 禁用Test功能      */
#define _ccm_en_test(ccm) _bit_clr(ccm->CCR, CCM_CCR_TESTDIS)                         /**< 使能Test功能      */
#define _ccm_dis_swap(ccm) _bit_set(ccm->CCR, CCM_CCR_SWAPDIS)                        /**< 禁用Swap功能      */
#define _ccm_en_swap(ccm) _bit_clr(ccm->CCR, CCM_CCR_SWAPDIS)                         /**< 使能Swap功能      */
#define _ccm_get_ccr_all(ccm) _reg_read(ccm->CCR)                                     /**< 读取CCR寄存器      */

#define _ccm_en_phy_pll(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_PLL_EN)                                 /**< 使能PHY_PLL_EN功能      */
#define _ccm_dis_phy_pll(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_PLL_EN)                                /**< 禁用PHY_PLL_EN功能      */
#define _ccm_en_phy_reg(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_REG_EN)                                 /**< 使能PHY_REG_EN功能      */
#define _ccm_dis_phy_reg(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_REG_EN)                                /**< 禁用PHY_REG_EN功能      */
#define _ccm_en_suspendm(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_SUSPENDM)                              /**< 使能SUSPENDM功能      */
#define _ccm_dis_suspendm(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_SUSPENDM)                             /**< 禁用SUSPENDM功能      */
#define _ccm_select_term_reg(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_TERMSEL_DIR)                       /**< 通过控制寄存器控制      */
#define _ccm_select_term_USBC(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_TERMSEL_DIR)                      /**< 通过USB控制寄存器控制      */
#define _ccm_en_term(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_TERMSEL)                                   /**< 通过控制寄存器控制      */
#define _ccm_dis_term(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_TERMSEL)                                  /**< 通过USB控制寄存器控制      */
#define _ccm_select_phy_dir_reg(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_DIR)                            /**< 通过控制寄存器控制      */
#define _ccm_select_phy_dir_USBC(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_DIR)                           /**< 通过USB控制寄存器控制      */
#define _ccm_en_clk_12M(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_12M_EN_SEL)                             /**< 使能USBPHY 12M时钟      */
#define _ccm_dis_clk_12M(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_12M_EN_SEL)                            /**< 禁用USBPHY 12M时钟      */
#define _ccm_select_phyreg_en_phypa(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USBPHY_REG_EN_SEL)                 /**< 通过PHYPA[1]控制      */
#define _ccm_select_phyreg_en_USBC(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USBPHY_REG_EN_SEL)                  /**< 通过USB控制寄存器控制      */
#define _ccm_en_phy_sessendvalid(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_PHY_SESSENDVALID)                     /**< 使能USBPHY sessendvalid      */
#define _ccm_dis_phy_sessendvalid(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_PHY_SESSENDVALID)                    /**< 禁用USBPHY sessendvalid      */
#define _ccm_en_phy_avalid(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_PHY_AVALID)                                 /**< 使能USBPHY avalid      */
#define _ccm_dis_phy_avalid(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_PHY_AVALID)                                /**< 禁用USBPHY avalid      */
#define _ccm_en_phy_vbusvalid(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_PHY_VBUSVALID)                           /**< 使能USBPHY vbusvalid      */
#define _ccm_dis_phy_vbusvalid(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_PHY_VBUSVALID)                          /**< 禁用USBPHY vbusvalid      */
#define _ccm_select_valid_dir_reg(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_PHY_VALID_DIR)                       /**< 通过控制寄存器控制      */
#define _ccm_select_valid_dir_USBC(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_PHY_VALID_DIR)                      /**< 通过USB控制寄存器控制      */
#define _ccm_select_phy_id_reg(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_PHY_ID_DIR)                             /**< 通过控制寄存器控制      */
#define _ccm_select_phy_id_USBC(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_PHY_ID_DIR)                            /**< 通过USB控制寄存器控制      */
#define _ccm_en_phy_id_pullup(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_PHY_ID_PULLUP)                           /**< 使能USBPHY ID_PULLUP      */
#define _ccm_dir_phy_id_pullup(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_PHY_ID_PULLUP)                          /**< 禁用USBPHY ID_PULLUP     */
#define _ccm_select_resume_signal_LineState_powerState(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_PHY_RESUME_SEL) /**< USB恢复通过LineState和powerState产生     */
#define _ccm_select_resume_signal_LineState(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_PHY_RESUME_SEL)            /**< USB恢复通过LineState产生     */
#define _ccm_select_little_endian(ccm) _bit_set(ccm->PHYPA, CCM_PHYPA_USB_REG_ENDIAN)                      /**< 选取小端数据结构     */
#define _ccm_select_big_endian(ccm) _bit_clr(ccm->PHYPA, CCM_PHYPA_USB_REG_ENDIAN)                         /**< 选取大端数据结构     */
#define _ccm_get_phypa_all(ccm) _reg_read(ccm->PHYPA)                                                      /**< 读取PHYPA寄存器    */

#define _ccm_en_pwm1(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_PWM1_EN)                  /**< 使能PWM1输入输出      */
#define _ccm_dis_pwm1(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_PWM1_EN)                 /**< 禁用PWM1输入输出      */
#define _ccm_en_pwm2(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_PWM2_EN)                  /**< 使能PWM2输入输出      */
#define _ccm_dis_pwm2(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_PWM2_EN)                 /**< 禁用PWM2输入输出      */
#define _ccm_en_pwm3(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_PWM3_EN)                  /**< 使能PWM3/PWM4输入输出      */
#define _ccm_dis_pwm3(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_PWM3_EN)                 /**< 禁用PWM3/PWM4输入输出      */
#define _ccm_en_clock_pull(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_CLKOUT_PUE)         /**< 使能CLKOUT PULL      */
#define _ccm_dis_clock_pull(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_CLKOUT_PUE)        /**< 禁用CLKOUT PULL      */
#define _ccm_en_rstout_pull(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_RSTOUT_PUE)        /**< 使能RSTOUT PULL      */
#define _ccm_dis_rstout_pull(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_RSTOUT_PUE)       /**< 禁用RSTOUT PULL      */
#define _ccm_en_tdo_pull(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_TDO_PUE)              /**< 使能tdo PULL      */
#define _ccm_dis_tdo_pull(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_TDO_PUE)             /**< 禁用tdo PULL      */
#define _ccm_en_ss2_swap(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_SS2_SWAP)             /**< 使能SS2 swap功能（SPI）      */
#define _ccm_dis_ss2_swap(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_SS2_SWAP)            /**< 禁用SS2 swap功能（GINT）      */
#define _ccm_select_gint37_pullup(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT37_PS)   /**< 使能GINT37上拉     */
#define _ccm_select_gint37_pulldown(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT37_PS) /**< 使能GINT37下拉     */
#define _ccm_en_gint7_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT7_IE)          /**< 使能GINT7输入    */
#define _ccm_dis_gint7_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT7_IE)         /**< 禁用GINT7输入    */
#define _ccm_en_gint6_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT6_IE)          /**< 使能GINT6输入    */
#define _ccm_dis_gint6_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT6_IE)         /**< 禁用GINT6输入    */
#define _ccm_en_gint5_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT5_IE)          /**< 使能GINT5输入    */
#define _ccm_dis_gint5_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT5_IE)         /**< 禁用GINT5输入    */
#define _ccm_en_gint4_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT4_IE)          /**< 使能GINT4输入    */
#define _ccm_dis_gint4_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT4_IE)         /**< 禁用GINT4输入    */
#define _ccm_en_gint3_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT3_IE)          /**< 使能GINT3输入    */
#define _ccm_dis_gint3_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT3_IE)         /**< 禁用GINT3输入    */
#define _ccm_en_gint2_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT2_IE)          /**< 使能GINT2输入    */
#define _ccm_dis_gint2_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT2_IE)         /**< 禁用GINT2输入    */
#define _ccm_en_gint1_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT1_IE)          /**< 使能GINT1输入    */
#define _ccm_dis_gint1_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT1_IE)         /**< 禁用GINT1输入    */
#define _ccm_en_gint0_input(ccm) _bit_set(ccm->PCFG3, CCM_PCFG3_GINT0_IE)          /**< 使能GINT0输入    */
#define _ccm_dis_gint0_input(ccm) _bit_clr(ccm->PCFG3, CCM_PCFG3_GINT0_IE)         /**< 禁用GINT0输入    */
#define _ccm_get_pcfg3_all(ccm) _reg_read(ccm->PCFG3)                              /**< 读取PCFG3寄存器    */

#define _ccm_en_rtc_interface(ccm) _bit_set(ccm->RTCCFG12, CCM_RTCCFG12_RTC_EN_INTERFACE)  /**< 使能RTC接口    */
#define _ccm_dis_rtc_interface(ccm) _bit_clr(ccm->RTCCFG12, CCM_RTCCFG12_RTC_EN_INTERFACE) /**< 禁用RTC接口    */
#define _ccm_en_day_int(ccm) _bit_set(ccm->RTCCFG12, CCM_RTCCFG12_DAY_INT_EN)              /**< 使能DAY中断    */
#define _ccm_dis_day_int(ccm) _bit_clr(ccm->RTCCFG12, CCM_RTCCFG12_DAY_INT_EN)             /**< 禁用DAY中断    */
#define _ccm_en_hour_int(ccm) _bit_set(ccm->RTCCFG12, CCM_RTCCFG12_HOUR_INT_EN)            /**< 使能HOUR中断    */
#define _ccm_dis_hour_int(ccm) _bit_clr(ccm->RTCCFG12, CCM_RTCCFG12_HOUR_INT_EN)           /**< 禁用HOUR中断    */
#define _ccm_en_minute_int(ccm) _bit_set(ccm->RTCCFG12, CCM_RTCCFG12_MINUTE_INT_EN)        /**< 使能MINUTE中断    */
#define _ccm_dis_minute_int(ccm) _bit_clr(ccm->RTCCFG12, CCM_RTCCFG12_MINUTE_INT_EN)       /**< 禁用MINUTE中断    */
#define _ccm_en_second_int(ccm) _bit_set(ccm->RTCCFG12, CCM_RTCCFG12_SECOND_INT_EN)        /**< 使能SECOND中断    */
#define _ccm_dis_second_int(ccm) _bit_clr(ccm->RTCCFG12, CCM_RTCCFG12_SECOND_INT_EN)       /**< 禁用SECOND中断    */
#define _ccm_get_rtccfg12_all(ccm) _reg_read(ccm->RTCCFG12)                                /**< 读取RTCCFG12寄存器    */

/**
  * @}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_CCM_Exported_Types Exported Types
  *
  * @{
  */
#endif
/**
  * @}
  */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_CCM_Exported_Variables Exported Variables
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_CCM_Exported_Functions Exported Functions
  * @{
  */
#endif
  /**
  * @}
  */

  /**
  *@}
  */

  /**
  *@}
  */

#ifdef __cplusplus
}
#endif

#endif /* __CCM_DRV_H */

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE*************/
