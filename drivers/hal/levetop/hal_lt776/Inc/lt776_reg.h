/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			lt776_reg.h
 * @Author		Jason Zhao
 * @Date			2021/12/1
 * @Time			12:23:35
 * @Version
 * @Brief
 **********************************************************
 * Modification History
 *
 *
 **********************************************************/
#ifndef __LT776_REG_H__
#define __LT776_REG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "type.h"
#include "iomacros.h"

//#define FREQUENCY_MHZ  400
#define FREQUENCY_MHZ 500

#define __MACRO_DEF_ADC__
#define __MACRO_DEF_CACHE__
#define __MACRO_DEF_CCM__
#define __MACRO_DEF_CLCD__
#define __MACRO_DEF_CPM__
#define __MACRO_DEF_CRC__
#define __MACRO_DEF_DMAC__
#define __MACRO_DEF_EDMAC__
#define __MACRO_DEF_EFLASH__
#define __MACRO_DEF_EPORT__
#define __MACRO_DEF_I2C__
#define __MACRO_DEF_USB__
#define __MACRO_DEF_IO_CONTROL__
#define __MACRO_DEF_PCI__
#define __MACRO_DEF_PIT__
#define __MACRO_DEF_CAN__
#define __MACRO_DEF_PWM__
#define __MACRO_DEF_RESET__
#define __MACRO_DEF_RTC__
#define __MACRO_DEF_SPI__
#define __MACRO_DEF_SSI__
#define __MACRO_DEF_UART__

/**
 * @brief  寄存器基地址定义
 */
/* M4核模块寄存器基地址  */
#define ADC_BASE_ADDR (0x40020000)      /**< ADC寄存器基地址 */
#define CACHE_BASE_ADDR (0x40051000)    /**< CACHE寄存器基地址 */
#define CACHE2_BASE_ADDR (0x40055000)   /**< CACHE2寄存器基地址 */
#define CCM_BASE_ADDR (0x40001000)      /**< CCM寄存器基地址 */
#define CRC0_BASE_ADDR (0x40044000)     /**< CRC0寄存器基地址   */
#define CRC1_BASE_ADDR (0x40045000)     /**< CRC1寄存器基地址   */
#define CRYPTO_BASE_ADDR (0x40037000)   /**< CRYPTO(RSA)寄存器基地址   */
#define CPM_BASE_ADDR (0x40004000)      /**< CPM寄存器基地址 */
#define DAC_BASE_ADDR (0x40021000)      /**< DAC寄存器基地址   */
#define DMAC1_BASE_ADDR (0x40046000)    /**< DMA1寄存器基地址   */
#define DMAC2_BASE_ADDR (0x40047000)    /**< DMA2寄存器基地址  */
#define EDMAC0_0_BASE_ADDR (0X40039000) /**< EDMAC0_0寄存器基地址   */
#define EDMAC0_1_BASE_ADDR (0X40039040) /**< EDMAC0_1寄存器基地址   */
#define EDMAC1_0_BASE_ADDR (0X4000A000) /**< EDMAC1_0寄存器基地址   */
#define EDMAC1_1_BASE_ADDR (0X4000A040) /**< EDMAC1_0寄存器基地址   */
#define EFLASH_BASE_ADDR (0X40003000)   /**< EFLASH寄存器基地址 */
#define EPORT_BASE_ADDR (0x40019000)    /**< EPORT寄存器基地址 */
#define EPORT1_BASE_ADDR (0x4001a000)   /**< EPORT1寄存器基地址 */
#define EPORT2_BASE_ADDR (0x40024000)   /**< EPORT2寄存器基地址 */
#define EPORT3_BASE_ADDR (0x40025000)   /**< EPORT3寄存器基地址 */
#define EPORT4_BASE_ADDR (0x40026000)   /**< EPORT4寄存器基地址 */
#define EPORT5_BASE_ADDR (0x40027000)   /**< EPORT5寄存器基地址 */
#define EPORT6_BASE_ADDR (0x40028000)   /**< EPORT6寄存器基地址 */
#define EPORT7_BASE_ADDR (0x40029000)   /**< EPORT7寄存器基地址 */
#define EPORT8_BASE_ADDR (0x4002A000)   /**< EPORT8寄存器基地址 */
#define EPORT9_BASE_ADDR (0x4002B000)   /**< EPORT9寄存器基地址 */
#define EPORT10_BASE_ADDR (0x4002C000)  /**< EPORT10寄存器基地址 */
#define EPORT11_BASE_ADDR (0x4002D000)  /**< EPORT11寄存器基地址 */
#define EPORT12_BASE_ADDR (0x4002E000)  /**< EPORT12寄存器基地址 */
#define EPORT13_BASE_ADDR (0x4002F000)  /**< EPORT13寄存器基地址 */
#define EPORT14_BASE_ADDR (0x4000B000)  /**< EPORT14寄存器基地址 */
#define EPORT15_BASE_ADDR (0x4000C000)  /**< EPORT15寄存器基地址 */
#define EPORT16_BASE_ADDR (0x40009000)  /**< EPORT16寄存器基地址 */
#define LD_BASE_ADDR (0x40030000)       /**< LD(光照)寄存器基地址   */
#define MCC_BASE_ADDR (0x40022000)      /**< MCC寄存器基地址   */
#define I2C1_BASE_ADDR (0x40017000)     /**< I2C1寄存器基地址   */
#define I2C2_BASE_ADDR (0x4001B000)     /**< I2C2寄存器基地址   */
#define I2C3_BASE_ADDR (0x4001C000)     /**< I2C3寄存器基地址   */
#define I2S_BASE_ADDR (0x40057000)      /**< I2S寄存器基地址   */
#define IOCTRL_BASE_ADDR (0x40000000)   /**< IOCTRL寄存器基地址 */
#define LED_BASE_ADDR (0x63f70000)      /**< LED寄存器基地址 */
#define PCI_BASE_ADDR (0x40034000)      /**< PCI 防拆寄存器基地址 */
#define PGD_BASE_ADDR (0x40032000)      /**< PGD 毛刺检测寄存器基地址   */
#define PIT1_BASE_ADDR (0x40007000)     /**< PIT32_0 32bits 可编程中断计数器寄存器基地址 */
#define PIT2_BASE_ADDR (0x40008000)     /**< PIT32_1 32bits 可编程中断计数器寄存器基地址 */
#define PWM_BASE_ADDR (0x40018000)      /**< PWM 寄存器基地址 */
#define RESET_BASE_ADDR (0x40002000)    /**< RESET 寄存器基地址 */
#define RTC_BASE_ADDR (0x40035000)      /**< RTC 寄存器基地址 */
#define SD_BASE_ADDR (0xE0000000)       /**< SD 寄存器基地址   */
#define SDIO_BASE_ADDR (0xE0000000)     /**< SDIO 寄存器基地址   */
#define SECDET_BASE_ADDR (0X40033000)   /**< SECDET 安全检测寄存器基地址 */
#define SLA_BASE_ADDR (0x40038000)      /**< SLA 寄存器基地址   */
#define SPI1_BASE_ADDR (0x40010000)     /**< SPI1 寄存器基地址   */
#define SPI2_BASE_ADDR (0x40011000)     /**< SPI2 寄存器基地址   */
#define SPI3_BASE_ADDR (0x40012000)     /**< SPI3 寄存器基地址   */
#define SPIM1_BASE_ADDR (0x60000000)    /**< SSI1 高速SPI寄存器基地址   */
#define SPIM2_BASE_ADDR (0x70000000)    /**< SSI2 高速SPI寄存器基地址   */
#define TC_BASE_ADDR (0x40006000)       /**< TC 时间计数器寄存器基地址 */
#define TSI_BASE_ADDR (0x40023000)      /**< TSI 触控寄存器基地址   */
#define TRNG_BASE_ADDR (0x40031000)     /**< TRNG 随机数寄存器基地址 */
#define USBC_BASE_ADDR (0x4004C000)     /**< USB 寄存器基地址 */
#define UART1_BASE_ADDR (0x40013000)    /**< UART1 寄存器基地址   */
#define UART2_BASE_ADDR (0x40014000)    /**< UART2 寄存器基地址   */
#define UART3_BASE_ADDR (0x4001D000)    /**< UART3 寄存器基地址   */
#define UART4_BASE_ADDR (0x4001E000)    /**< UART4 寄存器基地址   */
#define USI3_BASE_ADDR (0x40016000)     /**< USI3 ISO7816 寄存器基地址 */
#define WDT_BASE_ADDR (0x40005000)      /**< WDT 看门狗寄存器基地址 */
#define PIT3_BASE_ADDR (0x4000d000)
#define PIT4_BASE_ADDR (0x4000e000)
#define CAN1_BASE_ADDR (0x4000f000)      /**< CAN1寄存器基地址 */
#define CLCD_BASE_ADDR (0x40048000)      /**< CLCD寄存器基地址 */
#define SDRAM_BASE_ADDR (0x9c000000)     /**< SDRAM 寄存器基地址 */
#define SDRAM2LCD_BASE_ADDR (0xa8000000) /**< SDRAM2LCD 寄存器基地址 */
#define M4SYS_BASE_ADDR (0xE0000000)     /**< M4 核寄存器基地址   */
#define DCMI_BASE_ADDR (0x40053000)      /**< SDRAM 寄存器基地址 */
#define DMA2D_BASE_ADDR (0x40052000)     /**< DMA2D 寄存器基地址 */

/* 网口模块寄存器基地址*/
#define ETH_MAC_BASE (0x40060000) /**< 网口寄存器基地址   */

/**
 * @brief  模块定义
 */
#ifdef __MACRO_DEF_ADC__
/*** ADC **********************************************/
/*ADC interrupt and status register:ADC_ISR*/
#define ADC_ANALOG_WD_FLAG ((u32)(1 << 7))
#define ADC_FIFO_EMPTY_STATUS ((u32)(1 << 6))
#define ADC_FIFO_FULL_STATUS ((u32)(1 << 5))
#define ADC_OVER_RUN_FLAG ((u32)(1 << 4))
#define ADC_END_SEQUENCE_FLAG ((u32)(1 << 3))
#define ADC_END_CONVERSION_FLAG ((u32)(1 << 2))
#define ADC_END_SAMPLING_FLAG ((u32)(1 << 1))
#define ADC_READY_FLAG ((u32)(1 << 0))

/*ADC interrupt enable register:ADC_IER*/
#define ADC_ANALOG_WD_INT_EN ((u32)(1 << 7))
#define ADC_OVER_RUN_INT_EN ((u32)(1 << 4))
#define ADC_END_SEQUENCE_INT_EN ((u32)(1 << 3))
#define ADC_END_CONVERSION_INT_EN ((u32)(1 << 2))
#define ADC_END_SAMPLING_INT_EN ((u32)(1 << 1))
#define ADC_READY_INT_EN ((u32)(1 << 0))

/*ADC control register:ADC_CR*/
#define ADC_STOP_CONVERSION_CMD_EN ((u32)(1 << 3))
#define ADC_START_CONVERSION_CMD_EN ((u32)(1 << 2))
#define ADC_DISABLE_CMD_EN ((u32)(1 << 1))
#define ADC_ENABLE_CMD_EN ((u32)(1 << 0))

/*ADC configuration register 1:ADC_CFGR1*/
#define ADC_ANALOG_INPUT_DIFF_MASK ((u32)(1 << 31))
#define ADC_OVERRUN_MODE_MASK ((u32)(1 << 30))
#define ADC_QADC_CLK_DISABLE_MASK ((u32)(1 << 29))
#define ADC_SAMPLE_OUTPUT_BUFFER_EN ((u32)(1 << 28))
#define ADC_DISCONTINUOUS_EN ((u32)(1 << 23))
#define ADC_AUTO_OFF_MODE_EN ((u32)(1 << 22))
#define ADC_WAIT_CONVERSION_MODE_MASK ((u32)(1 << 21))
#define ADC_CONTINUOUS_CONVERSION_MASK ((u32)(1 << 20))
#define ADC_EXTERNAL_VREF_MASK ((u32)(1 << 15))
#define ADC_CFGR1_ALIGN_LEFT_ALIGNMENT ((u32)(1 << 10))
#define ADC_CFGR1_ALIGN_RIGHT_ALIGNMENT ((u32)(0 << 10))
#define ADC_LEFT_ALIGNMENT_MASK ((u32)(1 << 10))
#define ADC_CFGR1_RES_10BIT ((u32)(1 << 8))
#define ADC_CFGR1_DMATH_2 ((u32)(2 << 4))
#define ADC_DMA_ACCESS_EN ((u32)(1 << 0))
#define ADC_CFGR1_SEQ_LEN_SHIFT ((u32)(24))

/*ADC configuration register 2:ADC_CFGR2*/
#define ADC_ANALOG_INPUT_BYPASS_EN ((u32)(1 << 15))
#define ADC_CFGR2_QPR_0 ((u32)(0 << 8))
#define ADC_CFGR2_QPR_1 ((u32)(1 << 8))
#define ADC_CFGR2_SCTNT_10H ((u32)(0x10))

/*ADC sampling time register:ADC_SMPR*/
#define ADC_SMPR_SMP_6 ((u32)(6))
#define ADC_SMPR_SMP_8 ((u32)(8))
/*ADC watch dog register:ADC_WDG*/
#define ADC_ANALOG_WATCHDOG_EN ((u32)(1 << 7))
#define ADC_WATCHDOG_CHANNEL_MASK ((u32)(1 << 6))

/*ADC watch dog threshold register:ADC_TR*/
#define ADC_NOTE_FOR_HT_MASK ((u32)(1 << 28))
#define ADC_NOTE_FOR_LT_MASK ((u32)(1 << 12))

/*ADC channel selection register 1:ADC_CHSELR1*/
/*ADC channel selection register 2:ADC_CHSELR2*/
#define ADC_CHSELR_MASK ((u8)(0x1F))
/*ADC FIFO access register:ADC_FIFO*/
/*ADC interrupt and status register 2:ADC_ISR2*/
#define ADC_FIFO_TIMEOUT_FLAG ((u32)(1 << 31))
#define ADC_DATA_BUFFER_TIMEOUT_FLAG ((u32)(1 << 15))
#define ADC_DATA_BUFFER_READY_FLAG ((u32)(1 << 12))

/*ADC data gather register:ADC_DGATR*/
#define ADC_DATA_BUFFER_TIMEOUT_INT_EN ((u32)(1 << 15))
#define ADC_DATA_BUFFER_TIMEOUT_EN ((u32)(1 << 14))
#define ADC_DATA_BUFFER_READY_INT_EN ((u32)(1 << 12))
#define ADC_DATA_GATHER_FUNCTION_EN ((u32)(1 << 0))

/*ADC data buffer register:ADC_DBUFR*/
/*ADC FIFO timeout register:ADC_FIFOTOR*/
#define ADC_FIFO_TIMEOUT_INT_EN ((u32)(1 << 15))
#define ADC_FIFO_TIMEOUT_EN ((u32)(1 << 14))

/*ADC data for test 3:ADC_DFT3*/
/*ADC data for test 2:ADC_DFT2*/
/*ADC data for test 1:ADC_DFT1*/
/*ADC data for test 0:ADC_DFT0*/
/*ADC data for test 7:ADC_DFT7*/
/*ADC data for test 6:ADC_DFT6*/
/*ADC data for test 5:ADC_DFT5*/
/*ADC data for test 4:ADC_DFT4*/
/*ADC data for test 8:ADC_DFT8*/
/*ADC channel selection register 3:ADC_CHSELR3*/
#endif
    typedef struct
    {
        volatile u32 ADC_ISR;   /**< 00 ADC interrupt and status register								*/
        volatile u32 ADC_IER;   /**< 04 ADC interrupt enable register										*/
        volatile u32 ADC_CR;    /**< 08 ADC control register 														*/
        volatile u32 ADC_CFGR1; /**< 0C ADC configuration register 1 										*/

        volatile u32 ADC_CFGR2; /**< 10 ADC configuration register 2 										*/
        volatile u32 ADC_SMPR;  /**< 14 ADC sampling time register 											*/
        volatile u32 ADC_WDG;   /**< 18 ADC watch dog register 													*/
        volatile u32 ADC_TR;    /**< 1C ADC watch dog threshold register 								*/

        volatile u32 RESERVED1[3]; /**< 20 24 28 */
        volatile u32 ADC_CHSELR1;  /**< 2C ADC channel selection register 1								 */

        volatile u32 ADC_CHSELR2;  /**< 30 ADC channel selection register 2								 */
        volatile u32 RESERVED2[6]; /**< 40 */

        volatile u32 ADC_FIFOR; /**< 4C ADC FIFO access register 												*/

        volatile u32 RESERVED3; /**< 50 */
        volatile u32 ADC_ISR2;  /**< 54 ADC interrupt and status register 2							*/
        volatile u32 ADC_DGATR; /**< 58 ADC data gather register 												*/
        volatile u32 ADC_DBUFR; /**< 5C ADC data buffer register 												*/

        volatile u32 ADC_FIFOTOR;  /**< 60 ADC FIFO timeout register 											 */
        volatile u32 RESERVED4[7]; /**< 64 */

        union
        {
            volatile u32 ADC_DET3; /**< 80 ADC data for test 3 														 */
            struct
            {
                volatile u16 CHANEL6;
                volatile u16 CHANEL7;
            } DET3;
        };
        union
        {
            volatile u32 ADC_DET2; /**< 84 ADC data for test 2 														 */
            struct
            {
                volatile u16 CHANEL4;
                volatile u16 CHANEL5;
            } DET2;
        };
        union
        {
            volatile u32 ADC_DET1; /**< 88 ADC data for test 1 														 */
            struct
            {
                volatile u16 CHANEL2;
                volatile u16 CHANEL3;
            } DET1;
        };
        union
        {
            volatile u32 ADC_DET0; /**< 8C ADC data for test 0 														 */
            struct
            {
                volatile u16 CHANEL0;
                volatile u16 CHANEL1;
            } DET0;
        };

        union
        {
            volatile u32 ADC_DET7; /**< 90 ADC data for test 7 														 */
            struct
            {
                volatile u16 CHANEL14;
                volatile u16 CHANEL15;
            } DET7;
        };
        union
        {
            volatile u32 ADC_DET6; /**< 94 ADC data for test 6 														 */
            struct
            {
                volatile u16 CHANEL12;
                volatile u16 CHANEL13;
            } DET6;
        };
        union
        {
            volatile u32 ADC_DET5; /**< 98 ADC data for test 5 														 */
            struct
            {
                volatile u16 CHANEL10;
                volatile u16 CHANEL11;
            } DET5;
        };
        union
        {
            volatile u32 ADC_DET4; /**< 9C ADC data for test 4 														 */
            struct
            {
                volatile u16 CHANEL8;
                volatile u16 CHANEL9;
            } DET4;
        };

        union
        {
            volatile u32 ADC_DET8; /**< A0 ADC data for test 8 														 */
            struct
            {
                volatile u16 CHANEL16;
                volatile u16 RESERVED5;
            } DET8;
        };
        volatile u32 ADC_CHSELR3; /**< A4 ADC channel selection register 3 								*/
    } ADC_TypeDef;
#define ADC ((ADC_TypeDef *)ADC_BASE_ADDR)

#ifdef __MACRO_DEF_CACHE__
/*** CCR ************/
#define CACHE_CCR_ENCACHE ((u32)1 << 0) /**< 启用缓存 */
#define CACHE_CCR_INVW0 ((u32)1 << 24)  /**< 使WAY 0无效 */
#define CACHE_CCR_PUSHW0 ((u32)1 << 25) /**< 推送WAY 0 */
#define CACHE_CCR_INVW1 ((u32)1 << 26)  /**< 使WAY 1无效 */
#define CACHE_CCR_PUSHW1 ((u32)1 << 27) /**< 推送WAY 1 */
#define CACHE_CCR_GO ((u32)1 << 31)     /**< 启动CACHE命令 */

#define IPCCCR *(volatile u32 *)(CACHE_BASE_ADDR + 0x0)
#define IPCCLCR *(volatile u32 *)(CACHE_BASE_ADDR + 0x4)
#define IPCCSAR *(volatile u32 *)(CACHE_BASE_ADDR + 0x8)
#define IPCCCVR *(volatile u32 *)(CACHE_BASE_ADDR + 0xc)
#define IPCCSRR *(volatile u32 *)(CACHE_BASE_ADDR + 0x10)

#define IPCCRGS *(volatile u32 *)(CACHE_BASE_ADDR + 0x20)
#define IPCCRGS_H *(volatile u32 *)(CACHE_BASE_ADDR + 0x24)

#define IR2HIGHADDR *(volatile u32 *)(CACHE_BASE_ADDR + 0x80)

#define IPCRINVPAGEADDR *(volatile u32 *)(CACHE_BASE_ADDR + 0x180)
#define IPCRINVPAGESIZE *(volatile u32 *)(CACHE_BASE_ADDR + 0x184)

#define IPSCCR *(volatile u32 *)(CACHE_BASE_ADDR + 0x800)
#define IPSCLCR *(volatile u32 *)(CACHE_BASE_ADDR + 0x804)
#define IPSCSAR *(volatile u32 *)(CACHE_BASE_ADDR + 0x808)
#define IPSCCVR *(volatile u32 *)(CACHE_BASE_ADDR + 0x80c)
#define IPSCSRR *(volatile u32 *)(CACHE_BASE_ADDR + 0x810)

#define IPSCRGS *(volatile u32 *)(CACHE_BASE_ADDR + 0x820)

#define DPCCCR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x0)
#define DPCCLCR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x4)
#define DPCCSAR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x8)
#define DPCCCVR *(volatile u32 *)(CACHE2_BASE_ADDR + 0xc)
#define DPCCSRR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x10)

#define DPCCRGS *(volatile u32 *)(CACHE2_BASE_ADDR + 0x20)
#define DPCCRGS_H *(volatile u32 *)(CACHE2_BASE_ADDR + 0x24)

#define DR2HIGHADDR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x80)

#define DPCRINVPAGEADDR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x180)
#define DPCRINVPAGESIZE *(volatile u32 *)(CACHE2_BASE_ADDR + 0x184)

#define DPSCCR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x800)
#define DPSCLCR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x804)
#define DPSCSA *(volatile u32 *)(CACHE2_BASE_ADDR + 0x808)
#define DPSCCVR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x80c)
#define DPSCSRR *(volatile u32 *)(CACHE2_BASE_ADDR + 0x810)

#define DPSCRGS *(volatile u32 *)(CACHE2_BASE_ADDR + 0x820)
#endif
    typedef struct
    {
        volatile u32 CACHE_R6_S0_HADDR; /**< 0x40 段6子区域0地址上限寄存器 */
        volatile u32 CACHE_R6_S1_HADDR; /**< 0x44 段6子区域1地址上限寄存器 */
        volatile u32 CACHE_R6_S2_HADDR; /**< 0x48 段6子区域2地址上限寄存器 */
        volatile u32 CACHE_R6_S3_HADDR; /**< 0x4C 段6子区域3地址上限寄存器 */
        volatile u32 CACHE_R6_S0_LADDR; /**< 0x50 段6子区域0地址下限寄存器 */
        volatile u32 CACHE_R6_S1_LADDR; /**< 0x54 段6子区域1地址下限寄存器 */
        volatile u32 CACHE_R6_S2_LADDR; /**< 0x58 段6子区域2地址下限寄存器 */
        volatile u32 CACHE_R6_S3_LADDR; /**< 0x5C 段6子区域3地址下限寄存器 */
        volatile u32 CACHE_R7_S0_HADDR; /**< 0x60 段7子区域0地址上限寄存器 */
        volatile u32 CACHE_R7_S1_HADDR; /**< 0x64 段7子区域1地址上限寄存器 */
        volatile u32 CACHE_R7_S2_HADDR; /**< 0x68 段7子区域2地址上限寄存器 */
        volatile u32 CACHE_R7_S3_HADDR; /**< 0x6C 段7子区域3地址上限寄存器 */
        volatile u32 CACHE_R7_S0_LADDR; /**< 0x70 段7子区域0地址下限寄存器 */
        volatile u32 CACHE_R7_S1_LADDR; /**< 0x74 段7子区域1地址下限寄存器 */
        volatile u32 CACHE_R7_S2_LADDR; /**< 0x78 段7子区域2地址下限寄存器 */
        volatile u32 CACHE_R7_S3_LADDR; /**< 0x7C 段7子区域3地址下限寄存器 */
        volatile u32 CACHE_R2_S0_HADDR; /**< 0x80 段2子区域0地址上限寄存器 */
        volatile u32 CACHE_R2_S1_HADDR; /**< 0x84 段2子区域1地址上限寄存器 */
        volatile u32 CACHE_R2_S2_HADDR; /**< 0x88 段2子区域2地址上限寄存器 */
        volatile u32 CACHE_R2_S3_HADDR; /**< 0x8C 段2子区域3地址上限寄存器 */
        volatile u32 CACHE_R2_S0_LADDR; /**< 0x90 段2子区域0地址下限寄存器 */
        volatile u32 CACHE_R2_S1_LADDR; /**< 0x94 段2子区域1地址下限寄存器 */
        volatile u32 CACHE_R2_S2_LADDR; /**< 0x98 段2子区域2地址下限寄存器 */
        volatile u32 CACHE_R2_S3_LADDR; /**< 0x9C 段2子区域3地址下限寄存器 */
    } CACHE_SegmentTypeDef;
    typedef struct
    {
        volatile u32 CACHE_CCR;    /**< 0x00 控制寄存器 */
        volatile u32 CACHE_CLCR;   /**< 0x04 行控制寄存器 */
        volatile u32 CACHE_CSAR;   /**< 0x08 查询地址寄存器 */
        volatile u32 CACHE_CCVR;   /**< 0x0C 读/写数值寄存器	*/
        volatile u32 RESERVED1[4]; /**< 0x10~0x1C */
        volatile u32 CACHE_ACRG;   /**< 0x20 主地址段存取权限寄存器 */
                                   // volatile u32 CACHE_R267_ACR; 	 /**< 0x24 子地址段存取权限寄存器 */
    } CACHE_TypeDef;
#define ICACHE ((CACHE_TypeDef *)(CACHE_BASE_ADDR))
#define DCACHE ((CACHE_TypeDef *)(CACHE2_BASE_ADDR))

#ifdef __MACRO_DEF_CCM__
///*** CCM **********************************************/
///*** FDCR 0x00************/
#define CCM_FDCR_LFDCR ((u16)0xff << 0)
#define CCM_FDCR_HFDCR ((u16)0xff << 8)

///*** CCR 0x02************/
#define CCM_CCR_BMT ((u16)3 << 0)
#define CCM_CCR_BMD ((u16)1 << 2)
#define CCM_CCR_BME ((u16)1 << 3)
#define CCM_CCR_SHINT ((u16)1 << 4)
#define CCM_CCR_PERIPH_BRIDGE_RAE ((u16)1 << 5)
#define CCM_CCR_PERIPH_BRIDGE_PAE ((u16)1 << 6)
#define CCM_CCR_MODE ((u16)7 << 8)
#define CCM_CCR_CLKMODE_PDE ((u16)1 << 11)
#define CCM_CCR_JTAGDIS ((u16)1 << 12)
#define CCM_CCR_BTLDDIS ((u16)1 << 13)
#define CCM_CCR_TESTDIS ((u16)1 << 14)
#define CCM_CCR_SWAPDIS ((u16)1 << 15)

///*** PHYPA 0x06************/
#define CCM_PHYPA_USBPHY_PLL_EN ((u16)1 << 0)
#define CCM_PHYPA_USBPHY_REG_EN ((u16)1 << 1)
#define CCM_PHYPA_USBPHY_SUSPENDM ((u16)1 << 2)
#define CCM_PHYPA_USBPHY_TERMSEL_DIR ((u16)1 << 3)
#define CCM_PHYPA_USBPHY_TERMSEL ((u16)1 << 4)
#define CCM_PHYPA_USBPHY_DIR ((u16)1 << 5)
#define CCM_PHYPA_USBPHY_12M_EN_SEL ((u16)1 << 6)
#define CCM_PHYPA_USBPHY_REG_EN_SEL ((u16)1 << 7)

#define CCM_PHYPA_PHY_SESSENDVALID ((u16)1 << 8)
#define CCM_PHYPA_PHY_AVALID ((u16)1 << 9)
#define CCM_PHYPA_PHY_VBUSVALID ((u16)1 << 10)
#define CCM_PHYPA_PHY_VALID_DIR ((u16)1 << 11)
#define CCM_PHYPA_PHY_ID_DIR ((u16)1 << 12)
#define CCM_PHYPA_PHY_ID_PULLUP ((u16)1 << 13)
#define CCM_PHYPA_PHY_RESUME_SEL ((u16)1 << 14)
#define CCM_PHYPA_USB_REG_ENDIAN ((u16)1 << 15)

///*** PDCR 0x0c************/
//#define CCM_PDCR_RSTOUTSWPEPORT2_EN                    ((u32)1<<25)
//#define CCM_PDCR_CLKOUTSWPEPORT2_EN                    ((u32)1<<29)

///*** PCFG3 0x14************/
#define CCM_PCFG3_PWM1_EN ((u32)1 << 0)
#define CCM_PCFG3_PWM2_EN ((u32)1 << 1)
#define CCM_PCFG3_PWM3_EN ((u32)1 << 2)
#define CCM_PCFG3_CLKOUT_PUE ((u32)1 << 8)
#define CCM_PCFG3_RSTOUT_PUE ((u32)1 << 9)
#define CCM_PCFG3_TDO_PUE ((u32)1 << 10)
#define CCM_PCFG3_SS2_SWAP ((u32)1 << 13)
#define CCM_PCFG3_GINT37_PS ((u32)1 << 14)
#define CCM_PCFG3_GINT7_IE ((u32)1 << 24)
#define CCM_PCFG3_GINT6_IE ((u32)1 << 25)
#define CCM_PCFG3_GINT5_IE ((u32)1 << 26)
#define CCM_PCFG3_GINT4_IE ((u32)1 << 27)
#define CCM_PCFG3_GINT3_IE ((u32)1 << 28)
#define CCM_PCFG3_GINT2_IE ((u32)1 << 29)
#define CCM_PCFG3_GINT1_IE ((u32)1 << 30)
#define CCM_PCFG3_GINT0_IE ((u32)1 << 31)

///*** RTCCFG12 0x18************/
#define CCM_RTCCFG12_RTC_EN_INTERFACE ((u32)1 << 15)
#define CCM_RTCCFG12_DAY_INT_EN ((u32)1 << 16)
#define CCM_RTCCFG12_HOUR_INT_EN ((u32)1 << 17)
#define CCM_RTCCFG12_MINUTE_INT_EN ((u32)1 << 18)
#define CCM_RTCCFG12_SECOND_INT_EN ((u32)1 << 19)
#define CCM_RTCCFG12_DAY_PULSE_INV ((u32)1 << 20)
#define CCM_RTCCFG12_HOUR_PULSE_INV ((u32)1 << 21)
#define CCM_RTCCFG12_MINUTE_PULSE_INV ((u32)1 << 22)
#define CCM_RTCCFG12_SECOND_PULSE_INV ((u32)1 << 23)

///*** RTCCFG3 0x1c************/
#define CCM_RTCCFG3_RTC_TESTMODE_EN ((u32)1 << 17)

///*** PMCSR 0x20************/
#define CCM_PMCSR_OSC_2K_CLK_SRC ((u32)1 << 24)
#define CCM_PMCSR_OSC_128K_CLK_SRC ((u32)1 << 25)
#define CCM_PMCSR_LVDT1P8V_SRC ((u32)1 << 26)
#define CCM_PMCSR_LVDT3P3V_SRC ((u32)1 << 27)

#endif
    typedef struct
    {
        volatile u16 RESERVED0; // 0x00  FD configuration register
        volatile u16 CCR;       // 0x02 	chip configuration register
        volatile u16 CIR;       // 0x04 	chip identification register
        volatile u16 PHYPA;     // 0x06 	PHY parameter configurationregister
        volatile u16 RESERVED1; // 0x08
        volatile u16 CTR;       // 0x0a chip test register
        volatile u32 RESERVED2; // 0x0C

        volatile u32 PCFG12;       // 0x10
        volatile u32 PCFG3;        // 0x14
        volatile u32 RTCCFG12;     // 0x18
        volatile u8 RTCCFG3;       // 0x1C
        volatile u8 RTC_SR;        // 0x20
        volatile u32 RESERVED3;    // 0x24
        volatile u32 OTG_PHY_CTRL; // 0x28
        volatile u32 RESERVED4[4]; // 0x2C~0x34
        volatile u32 FUSE_DATA;    // 0x38
        volatile u32 SSI_TRIM_CFG; // 0x3C
    } CCM_TypeDef;
#define CCM ((CCM_TypeDef *)CCM_BASE_ADDR)

#ifdef __MACRO_DEF_CRC__
#define CRC_MODE_CRC_32 (u32)4<<29)            //< CRC模式32
#define CRC_MODE_CRC_16 ((u32)2 << 29)         //< CRC模式16   */
#define CRC_MODE_CRC_08 ((u32)1 << 29)         /*< CRC模式08   */
#define CRC_MODE_MASK ((u32)0x1FFFFFFF)        /**< CRC模式掩码 */
#define CRC_MODE_UPDATA ((u32)(1 << 28))       /**< CRC更新初始数据 */
#define CRC_MODE_SOURCE_SEL_DMAC_WRITE ((u32)(1 << 27)) /**< CRC数据通过DMAC传输 */
#define CRC_MODE_BIG_ENDING ((u32)(1 << 26))            /**< CRC大端模式 */
#define CRC_MODE_BYTE_SWAP ((u32)(1 << 25))             /**< CRC字符切换*/
#define CRC_EDMAC_SEL ((u32)(1 << 24))                  /**< CRC数据采用EDMAC传输*/
#define CRC_EDMAC_UPDATA ((u32)(1 << 20))               /**< CRC EDMAC模式下数据更新*/
#define CRC_EDMAC_CRCLAT ((u32)(1 << 19))               /**< CRC EDMAC模式下锁存CRC_OK状态*/
#define CRC_DECODE ((u32)(1 << 18))                     /**< CRC 解码模式*/
#define CRC_SEND_CRC_CODE_EN ((u32)(1 << 17))           /**< CRC 发送CRC编码*/
#define CRC_WRITE_CRC_CODE_EN ((u32)(1 << 16))          /**< CRC 接收CRC编码*/
#define CRC_EDMAC_SEND_CODE_BS ((u32)(1 << 9))          /**< CRC 发送CRC字节交换编码*/
#define CRC_EDMAC_DEC_CODE_BS ((u32)(1 << 8))           /**< CRC 解码CRC字节交换编码*/
#define CRC_CRCLAT_ERR ((u32)(1 << 1))                  /**< CRC 锁存错误*/
#define CRC_OK ((u32)(1 << 0))                          /**< CRC 正确编码 */
#endif
    typedef struct
    {
        volatile u32 CR;                  /**<CRC control register */
        volatile u32 RES;                 /**<CRC result */
        volatile u32 INIT_DATA;           /**<CRC initial data */
        volatile u32 DATA_IN;             /**<CRC data in */
        volatile u32 DMAC_DATA_IN;        /**<DMAC data in */
        volatile u32 EMDAC_CRC_OK;        /**<EDMAC CRC OK */
        volatile u32 EDMAC_CRC_FIRST_ERR; /**<EDMAC first CRC error number */
        volatile u32 EDMAC_CRC_COUNT;     /**<EDMAC CRC counter */
    } CRC_TypeDef;
#define CRC1 ((CRC_TypeDef *)CRC1_BASE_ADDR)

#ifdef __MACRO_DEF_CPM__
/*sleep config register:CPM*/
#define CPM_SLPCFGR_LOW_POWER_MODE ((u32)(0 << 30))
#define CPM_SLPCFGR_RETENTION_MODE ((u32)(1 << 30))
#define CPM_SLPCFGR_DEEP_SLEEP_MODE ((u32)(1 << 30))
#define CPM_SLPCFGR_HIBERNATION_MODE ((u32)(1 << 31))
#define CPM_SLPCFGR_EPORT4_MODULE_CLOCK_SLEEP_EN ((u32)(1 << 28)) /**< eport4 module clock enable when stop*/
#define CPM_SLPCFGR_EPORT3_MODULE_CLOCK_SLEEP_EN ((u32)(1 << 27)) /**< eport3 module clock enable when stop*/
#define CPM_SLPCFGR_EPORT2_MODULE_CLOCK_SLEEP_EN ((u32)(1 << 26)) /**< eport2 module clock enable when stop*/
#define CPM_SLPCFGR_EPORT1_MODULE_CLOCK_SLEEP_EN ((u32)(1 << 22)) /**< eport1 module clock enable when stop*/
#define CPM_SLPCFGR_EPORT0_MODULE_CLOCK_SLEEP_EN ((u32)(1 << 21)) /**< eport0 module clock enable when stop*/
#define CPM_SLPCFGR_OSCEXT_SLEEP_EN ((u32)(1 << 20))              /**< OSCEXT enable when stop*/
#define CPM_SLPCFGR_PMU128K_SLEEP_EN ((u32)(1 << 19))             /**< PMU128K enable when stop*/
#define CPM_SLPCFGR_RTC32K_SLEEP_EN ((u32)(1 << 16))              /**< RTC32K enable when stop*/
#define CPM_SLPCFGR_VDD33_LDO_ENTER_LOWPOWER_EN ((u32)(1 << 18))  /**< VDD LDO enter lowpower when stop*/
#define CPM_SLPCFGR_FLASH_LDO_ENTER_LOWPOWER_EN ((u32)(1 << 17))  /**< FLASH LDO enter lowpower when stop*/
#define CPM_SLPCFGR_CARD1_LDO_POWEROFF ((u32)(1 << 11))           /**< Card1 LDO poweroff*/
#define CPM_SLPCFGR_CARD1_LDO_VOTAGE_OUT_1V8 ((u32)(0 << 8))
#define CPM_SLPCFGR_CARD1_LDO_VOTAGE_OUT_3V0 ((u32)(1 << 8))
#define CPM_SLPCFGR_CARD1_LDO_VOTAGE_OUT_3V3 ((u32)(2 << 8))
#define CPM_SLPCFGR_CARD1_LDO_VOTAGE_OUT_5V0 ((u32)(3 << 8))
#define CPM_SLPCFGR_FLASH_LDO_ENTER_LOWPOWER_POWEROFF ((u32)(1 << 7)) /**< FLASH LDO poweroff when stop*/
#define CPM_SLPCFGR_FLASH_IP_ENTER_LOWPOWER_EN ((u32)(1 << 4))        /**< FLASH IP enter lowpower when stop*/

/*sleep control register:SLPCR*/
#define CPM_SLPCR_SLEEP_CONF_MODE ((u32)(1 << 29))

/*SCDIVR:system clock divider register*/
#define CPM_SCDIVR_CORE_DIV_MASK ((u32)(0x0FFFFFFF))   /**< */
#define CPM_SCDIVR_CORE_DIV_SHIFT_MASK ((u32)(28))     /**< */
#define CPM_SCDIVR_CLKOUT_DIV_MASK ((u32)(0xFF00FFFF)) /**< */
#define CPM_SCDIVR_CLKOUT_DIV_SHIFT_MASK ((u32)(16))   /**<  */
#define CPM_SCDIVR_TRACE_DIV_MASK ((u32)(0xFFFF00FF))  /**< */
#define CPM_SCDIVR_TRACE_DIV_SHIFT_MASK ((u32)(8))     /**<  */
#define CPM_SCDIVR_SYS_DIV_MASK ((u32)(0xFFFFFF00))    /**< */
#define CPM_SCDIVR_SYS_DIV_SHIFT_MASK ((u32)(0))       /**< */

/*periphal clock divider register 1:PCDIVR1*/
#define CPM_PCDIVR_ARITH_DIV_MASK ((u32)(0xFFFF0FFF)) /**< */
#define CPM_PCDIVR_ARITH_DIV_SHIFT_MASK ((u32)(12))   /**< */
#define CPM_PCDIVR_AHB3_DIV_MASK ((u32)(0xFFFFF0FF))  /**< */
#define CPM_PCDIVR_AHB3_DIV_SHIFT_MASK ((u32)(8))     /**< */
#define CPM_PCDIVR_IPS_DIV_MASK ((u32)(0xFFFFFFF0))   /**< */
#define CPM_PCDIVR_SDRAM_DIV_MASK ((u32)(0xFFF0FFFF)) /**< */
#define CPM_PCDIVR_IPS_DIV_SHIFT_MASK ((u32)(0))      /**< */
#define CPM_PCDIVR_SDRAM_DIV_SHIFT_MASK ((u32)(16))   /**< */

#define CPM_PCDIVR_SDRAM2LCD_DIV_MASK ((u32)(0x0FFFFFFF)) /**< */
#define CPM_PCDIVR_SDRAM2LCD_DIV_SHIFT_MASK ((u32)(28))   /**< */
#define CPM_PCDIVR_SDRAM_SM_DIV_MASK ((u32)(0xF8FFFFFF))  /**< */
#define CPM_PCDIVR_SDRAM_SM_DIV_SHIFT_MASK ((u32)(24))    /**< */

/*periphal clock divider register 2:PCDIVR2*/
#define CPM_PCDIVR_TC_DIV_MASK ((u32)(0x0FFFFFFF))      /**< */
#define CPM_PCDIVR_TC_DIV_SHIFT_MASK ((u32)(28))        /**<  */
#define CPM_PCDIVR_MESH_DIV_MASK ((u32)(0xF0FFFFFF))    /**< */
#define CPM_PCDIVR_MESH_DIV_SHIFT_MASK ((u32)(24))      /**<  */
#define CPM_PCDIVR_ADC_DIV_MASK ((u32)(0xFFFF0FFF))     /**< */
#define CPM_PCDIVR_ADC_DIV_SHIFT_MASK ((u32)(12))       /**< */
#define CPM_PCDIVR_MCC_ADR_DIV_MASK ((u32)(0xFFFFF1FF)) /**< */
#define CPM_PCDIVR_MCC_ADR_DIV_SHIFT_MASK ((u32)(9))    /**< */
#define CPM_PCDIVR_MCC_DIV_MASK ((u32)(0xFFFFFE00))     /**< */
#define CPM_PCDIVR_MCC_DIV_SHIFT_MASK ((u32)(0))        /**< */

#define CPM_PCDIVR_CLCD_DIV_MASK ((u32)(0xFF00FFFF)) /**< */
#define CPM_PCDIVR_CLCD_DIV_SHIFT_MASK ((u32)(16))   /**< */

/*periphal clock divider register 3:PCDIVR3*/
#define CPM_PCDIVR_SDHOST_DIV_MASK ((u32)(0xF0FFFFFF)) /**< */
#define CPM_PCDIVR_SDHOST_DIV_SHIFT_MASK ((u32)(24))   /**< */

#define CPM_PCDIVR_DMA2D_SRAM_DIV_MASK ((u32)(0xFFFFFFF0))  /**< */
#define CPM_PCDIVR_DMA2D_SRAM_DIV_SHIFT_MASK ((u32)(0))     /**< */
#define CPM_PCDIVR_MIPI_SAMPLE_DIV_MASK ((u32)(0xFFFFFF0F)) /**< */
#define CPM_PCDIVR_MIPI_SAMPLE_DIV_SHIFT_MASK ((u32)(4))    /**< */
#define CPM_PCDIVR_DCMI_PIX_DIV_MASK ((u32)(0xFFFF80FF))    /**< */
#define CPM_PCDIVR_DCMI_PIX_DIV_SHIFT_MASK ((u32)(8))       /**< */
#define CPM_PCDIVR_DCMI_SENSOR_DIV_MASK ((u32)(0xFFC0FFFF)) /**< */
#define CPM_PCDIVR_DCMI_SENSOR_DIV_SHIFT_MASK ((u32)(16))   /**< */

/*clock divider update register*/
#define CPM_CDIVUPDR_SYS_DIV_UPDATE ((u32)(1 << 1))        /**< sys clk update*/
#define CPM_CDIVUPDR_PERIPHERAL_DIV_UPDATE ((u32)(1 << 0)) /**< peripheral clk update*/

/*clock divider enable register*/
#define CPM_CDIVENR_SDHOST_CLK_DIV_EN ((u32)(1 << 20))
#define CPM_CDIVENR_CLKOUT_CLK_DIV_EN ((u32)(1 << 15))
#define CPM_CDIVENR_TRACE_CLK_DIV_EN ((u32)(1 << 14))
#define CPM_CDIVENR_TC_CLK_DIV_EN ((u32)(1 << 13))
#define CPM_CDIVENR_MESH_CLK_DIV_EN ((u32)(1 << 12))
#define CPM_CDIVENR_ADC_CLK_DIV_EN ((u32)(1 << 10))
#define CPM_CDIVENR_MCC_CLK_DIV_EN ((u32)(1 << 8))
#define CPM_CDIVENR_ARITH_CLK_DIV_EN ((u32)(1 << 3))
#define CPM_CDIVENR_AHB3_CLK_DIV_EN ((u32)(1 << 2))
#define CPM_CDIVENR_IPS_CLK_DIV_EN ((u32)(1 << 0))
#define CPM_CDIVENR_SDRAM_CLK_DIV_EN ((u32)(1 << 4))

#define CPM_CDIVENR_SDRAM2LCD_CLK_DIV_EN ((u32)(1 << 7))
#define CPM_CDIVENR_SDRAMSM_CLK_DIV_EN ((u32)(1 << 6))
#define CPM_CDIVENR_CLCD_CLK_DIV_EN ((u32)(1 << 11))
#define CPM_CDIVENR_DCMISENSOR_CLK_DIV_EN ((u32)(1 << 19))
#define CPM_CDIVENR_DCMIPIX_CLK_DIV_EN ((u32)(1 << 18))
#define CPM_CDIVENR_MIPISAMPLE_CLK_DIV_EN ((u32)(1 << 17))
#define CPM_CDIVENR_DMA2DSRAM_CLK_DIV_EN ((u32)(1 << 16))

/*oscillator control and status register*/
#define CPM_OCSR_PMU2K_CLK_EN ((u32)(1 << 6))
#define CPM_OCSR_RTC32K_CLK_EN ((u32)(1 << 5))
#define CPM_OCSR_OSCEXT_CLK_EN ((u32)(1 << 4))
#define CPM_OCSR_OSC400M_CLK_EN ((u32)(1 << 3))
#define CPM_OCSR_USBPHY480M_CLK_EN ((u32)(1 << 2))
#define CPM_OCSR_PMU128K_CLK_EN ((u32)(1 << 1))
#define CPM_OCSR_OSC8M_CLK_EN ((u32)(1 << 0))
#define CPM_OCSR_PMU2K_STABLE ((u32)(1 << 14))      /**< */
#define CPM_OCSR_RTC32K_STABLE ((u32)(1 << 13))     /**< */
#define CPM_OCSR_OSCEXT_STABLE ((u32)(1 << 12))     /**< */
#define CPM_OCSR_OSC400M_STABLE ((u32)(1 << 11))    /**< */
#define CPM_OCSR_USBPHY480M_STABLE ((u32)(1 << 10)) /**< */
#define CPM_OCSR_PMU128K_STABLE ((u32)(1 << 9))     /**< */
#define CPM_OCSR_OSC8M_STABLE ((u32)(1 << 8))       /**< */

/*clock switch config register*/
#define CPM_CSWCFGR_OSCL_SELECT ((u32)(1 << 6))
#define CPM_CSWCFGR_OSC8M_SELECT ((u32)(1 << 8))            /**< */
#define CPM_CSWCFGR_OSC400M_SELECT ((u32)(1 << 9))          /**< */
#define CPM_CSWCFGR_USBPHY480M_SELECT ((u32)(1 << 10))      /**< */
#define CPM_CSWCFGR_OSCEXT_SELECT ((u32)(1 << 11))          /**< */
#define CPM_CSWCFGR_SOC_CLK_SOURCE_MASK ((u32)(0xFFFFFFFC)) /**< */
#define CPM_CSWCFGR_CLKOUT_SOURCE_SYS ((u32)(0 << 24))      /**< */
#define CPM_CSWCFGR_CLKOUT_SOURCE_ARITH ((u32)(1 << 24))    /**< */
#define CPM_CSWCFGR_CLKOUT_SOURCE_RTC32K ((u32)(3 << 24))   /**< */

/*core tick timer register*/
#define CPM_CTICKR_REFERENCE_CLK_SELECT_MASK ((u32)(1 << 25)) /**< */
#define CPM_CTICKR_SKEW_EN ((u32)(1 << 24))                   /**< */

/*chip config register:CHIPCFGR*/
#define CPM_CHIPCFGR_USBPHY_OSC_MODE_AUTO ((u32)(0 << 30))          /**< auto dection oscillator*/
#define CPM_CHIPCFGR_USBPHY_OSC_MODE_AUTO_FOR_SIMU ((u32)(1 << 30)) /**< auto dection oscillator for fast simulation*/
#define CPM_CHIPCFGR_USBPHY_OSC_MODE_INTER ((u32)(2 << 30))         /**< select internal oscillator*/
#define CPM_CHIPCFGR_USBPHY_OSC_MODE_EXTER ((u32)(3 << 30))         /**< select external oscillator*/
#define CPM_CHIPCFGR_USBPHY_CONF_SOFTWARE_MASK ((u32)(1 << 29))
#define CPM_CHIPCFGR_USBPHY_PLL_SOFTWARE_MASK ((u32)(1 << 28))
#define CPM_CHIPCFGR_USBPHY_IP_SOFTWARE_MASK ((u32)(1 << 23))
#define CPM_CHIPCFGR_USBPHY_INPUT_ISOLATION_EN ((u32)(1 << 27))
#define CPM_CHIPCFGR_USBPHY_OUTPUT_ISOLATION_EN ((u32)(1 << 26))
#define CPM_CHIPCFGR_USBPHY_RESET_SIGNAL_MASK ((u32)(1 << 25))
#define CPM_CHIPCFGR_USBPHY_POWER_SWITCH_EN ((u32)(1 << 24))
#define CPM_CHIPCFGR_MAIN_TO_SUB_SYSTEM_INT_EN ((u32)(1 << 22))
#define CPM_CHIPCFGR_SUB_TO_MAIN_SYSTEM_INT_FLAG ((u32)(1 << 21))
#define CPM_CHIPCFGR_SUB_TO_MAIN_SYSTEM_INT_FLAG_CLR ((u32)(1 << 20))
#define CPM_CHIPCFGR_SUB_TO_MAIN_SYSTEM_INT_EN ((u32)(1 << 10))
#define CPM_CHIPCFGR_SUB_TO_MAIN_SYSTEM_OUTPUT_ISOLATION_EN ((u32)(1 << 19))
#define CPM_CHIPCFGR_SUB_TO_MAIN_SYSTEM_POWER_STATUS_ISOLATION_EN ((u32)(1 << 18))
#define CPM_CHIPCFGR_PCI_H2L_ISOLATION_EN ((u32)(1 << 16))
#define CPM_CHIPCFGR_PCI_H2L_ISOLATION_SEL_MASK ((u32)(1 << 17))
#define CPM_CHIPCFGR_RTC1S_CLK_GATE_EN ((u32)(1 << 14))
#define CPM_CHIPCFGR_RTC1K_CLK_GATE_EN ((u32)(1 << 13))
#define CPM_CHIPCFGR_RTC32K_CLK_GATE_EN ((u32)(1 << 12))
#define CPM_CHIPCFGR_RTC32K_ISOLATION_EN ((u32)(1 << 11))

/*power control register:PWRCR*/
#define CPM_PWRCR_VCC_IO_LATCH_CLR_MASK ((u32)(1 << 31))
#define CPM_PWRCR_VCC_IO_LATCH_SET_MASK ((u32)(1 << 30))
#define CPM_PWRCR_VCC_5V_LV_DETECT_RESET_EN ((u32)(1 << 29))
#define CPM_PWRCR_VCC_1V8_LV_DETECT_RESET_EN ((u32)(1 << 28))
#define CPM_PWRCR_VCARD1_INTERFACE_ISOLATION_EN ((u32)(1 << 25))
#define CPM_PWRCR_VCC_5V_LVD_POWERDOWN_MASK ((u32)(1 << 23)) /**< When LVD5V detected by PMU, enter Power Off2 mode*/
#define CPM_PWRCR_VCC_5V_LV_DETECT_INT_EN ((u32)(1 << 15))
#define CPM_PWRCR_VCC_1V8_LV_DETECT_INT_EN ((u32)(1 << 14))
#define CPM_PWRCR_CARD1_IE_DETECT_INT_EN ((u32)(1 << 13))
#define CPM_PWRCR_CARD1_IE_EN_FAIL ((u32)(1 << 11))
#define CPM_PWRCR_CARD1_LV_DETECT_RESET_EN ((u32)(1 << 9))
#define CPM_PWRCR_VCC_5V_LV_DETECT_OUTPUT_EN ((u32)(1 << 7))
#define CPM_PWRCR_VCC_1V8_LV_DETECT_OUTPUT_EN ((u32)(1 << 6))
#define CPM_PWRCR_CARD1_LV_DETECT_OUTPUT_EN ((u32)(1 << 5))
#define CPM_PWRCR_VCC_5V_LV_DETECT_EN ((u32)(1 << 3))
#define CPM_PWRCR_VCC_1V8_LV_DETECT_EN ((u32)(1 << 2))
#define CPM_PWRCR_CARD1_LV_DETECT_EN ((u32)(1 << 1))

/*sleep counter register:SLPCNTR*/

/*wake up counter register:WKPCNTR*/

/*multiple clock gate control register:MULTICGTCR*/
/*system clock gate control register:SYSCGTCR*/
/*ahb3 clock gate control register:AHB3CGTCR*/
/*arith clock gate control register:ARITHCGTCR*/
/*ips clock gate control register:IPSCGTCR*/

/*vcc general trim register:VCCGTRIMR*/
#define CPM_VCCGTRIMR_V33_SW_ENB \
    ((u32)(1 << 31)) /**< when vdd5v<3.6V, set this bit to low will let vd33/vd33_flash=vdd5v*/
#define CPM_VCCGTRIMR_DISCHARGE_EN \
    ((u32)(1 << 30)) /**< when this bit is set,discharge vd33 when chip switch to poff2 mode*/
#define CPM_VCCGTRIMR_2KHZ_CLK_GATE_EN ((u32)(1 << 23))
#define CPM_VCCGTRIMR_CORE_VOLTAGE_MASK ((u32)(1 << 15)) /**< when this bit is set, the core voltage is 0.9V*/
#define CPM_VCCGTRIMR_VCC_LATCH_AUTO_SET_MASK ((u32)(1 << 13))
#define CPM_VCCGTRIMR_VCC_LATCH_AUTO_CLR_MASK ((u32)(1 << 12))
#define CPM_VCCGTRIMR_TEST_BIAS_CURRENT_EN ((u32)(1 << 7)) /**< test the bias current enable signal*/

/*vcc lv detect trim register:VCCLTRIMR*/
#define CPM_VCCLTRIMR_COARSE_LVD_MODULE_EN ((u32)(1 << 24))

/*vcc vref trim register:VCCVTRIMR*/
#define CPM_VCCVTRIMR_SLEEP_CONF_REG_PROTECT_EN ((u32)(1 << 31))
#define CPM_VCCVTRIMR_SUB_VDD_SRAM_POWER_MASK ((u32)(1 << 13))
#define CPM_VCCVTRIMR_SUB_VDD_POWER_MASK ((u32)(1 << 12))
#define CPM_VCCVTRIMR_VREF_STABLE_MASK ((u32)(1 << 11))
#define CPM_VCCVTRIMR_VREF_TRIM_EN ((u32)(1 << 10))
#define CPM_VCCVTRIMR_VREF_TRIM_VALUE_LOAD_BIT ((u32)(1 << 9))
#define CPM_VCCVTRIMR_STORE_VREF_VOLTAGE_VALUE_EN ((u32)(1 << 8))

/*vcc core test mode register:VCCCTMR*/
#define CPM_VCCCTMR_OVERWRITE_CSWCFGR_TRIM_EN ((u32)(1 << 29))
#define CPM_VCCCTMR_OVERWRITE_RTCTRIMR_TRIM_EN ((u32)(1 << 28))
#define CPM_VCCCTMR_OVERWRITE_RTCSTIMER_TRIM_EN ((u32)(1 << 26))
#define CPM_VCCCTMR_OVERWRITE_CARDTRIMR_TRIM_EN ((u32)(1 << 24))
#define CPM_VCCCTMR_OVERWRITE_VCCGTRIMR_TRIM_EN ((u32)(1 << 23))
#define CPM_VCCCTMR_OVERWRITE_VCCLTRIMR_TRIM_EN ((u32)(1 << 22))
#define CPM_VCCCTMR_OVERWRITE_VCCVTRIMR_TRIM_EN ((u32)(1 << 21))
#define CPM_VCCCTMR_OVERWRITE_O8MTRIMR_TRIM_EN ((u32)(1 << 20))
#define CPM_VCCCTMR_OVERWRITE_O120MTRIMR_TRIM_EN ((u32)(1 << 19))
#define CPM_VCCCTMR_OVERWRITE_OSCLSTIMER_TRIM_EN ((u32)(1 << 18))
#define CPM_VCCCTMR_OVERWRITE_OSCHSTIMER_TRIM_EN ((u32)(1 << 17))
#define CPM_VCCCTMR_OVERWRITE_OSCESTIMER_TRIM_EN ((u32)(1 << 16))
#define CPM_VCCCTMR_OVERWRITE_ARITHCGTCR_TRIM_EN ((u32)(1 << 13))
#define CPM_VCCCTMR_OVERWRITE_SCDIVR_TRIM_EN ((u32)(1 << 11))
#define CPM_VCCCTMR_OVERWRITE_PCDIVR_TRIM_EN ((u32)(1 << 10))
#define CPM_VCCCTMR_OVERWRITE_OCSR_TRIM_EN ((u32)(1 << 9))
#define CPM_VCCCTMR_CPU_CORE_TEST_MODE_EN ((u32)(1 << 7))
#define CPM_VCCCTMR_SOFT_POR ((u32)(1 << 3))

/*osc8mhz trim register:O8MTRIMR*/
/*osc120mhz trim register:O8MTRIMR*/

/*card ldo trim trgister:CARDTRIMR*/
#define CPM_CARDTRIMR_WAKEUP_FILTER_EN ((u32)(1 << 30))
#define CPM_CARDTRIMR_WAKEUP_FILTER_BYPASS_EN ((u32)(1 << 29))
#define CPM_CARDTRIMR_WAKEUP_FILTER_CLK_GATE_EN ((u32)(1 << 28))
#define CPM_CARDTRIMR_WAKEUP_ANALOG_FILTER_BYPASS_EN ((u32)(1 << 15))
#define CPM_CARDTRIMR_CARD1_SENSOR_RESISTANCE_REDUCE ((u32)(1 << 13))

/*oscl stable time register:OSCLSTIMER*/
/*osch stable time register:OSCHSTIMER*/
/*osce stable time register:OSCESTIMER*/

/*power status register:PWRSR*/
#define CPM_PWRSR_VCARD_ISOLATION_FLAG ((u32)(1 << 26))
#define CPM_PWRSR_VCC5V_LVD_FLAG ((u32)(1 << 23))
#define CPM_PWRSR_VCC1V8_LVD_FLAG ((u32)(1 << 22))
#define CPM_PWRSR_CARD1_LVD_FLAG ((u32)(1 << 21))
#define CPM_PWRSR_VCC5V_LVD_REAL_TIME_FLAG ((u32)(1 << 19))
#define CPM_PWRSR_VCC1V8_LVD_REAL_TIME_FLAG ((u32)(1 << 18))
#define CPM_PWRSR_CARD1_LVD_REAL_TIME_FLAG ((u32)(1 << 17))
#define CPM_PWRSR_CARD1_LVD_FAIL_FLAG ((u32)(1 << 15))
#define CPM_PWRSR_VCC_HIGH_POWER_READY_FLAG ((u32)(1 << 3))
#define CPM_PWRSR_CARD1_READY_FLAG ((u32)(1 << 2))
#define CPM_PWRSR_M2S_SYS_RST_RELEASE_FLAG ((u32)(1 << 13))
#define CPM_PWRSR_M2S_SYS_BUS_READY_FLAG ((u32)(1 << 8))
#define CPM_PWRSR_SUB_POWER_READY_FLAG ((u32)(1 << 4))

/*rtc trim register:RTCTRIMR*/
#define CPM_RTCTRIMR_RTC_TRIM_EN ((u32)(1 << 31))
#define CPM_RTCTRIMR_RTC_TRIM_LOAD_EN ((u32)(1 << 30))

/*pad wakeup interrupt control register:PADWKINTCR*/
#define CPM_PADWKINTCR_S2M_WAKEUP_SRC_STATUS ((u32)(1 << 31))
#define CPM_PADWKINTCR_S2M_WAKEUP_SRC_EN ((u32)(1 << 23))
#define CPM_PADWKINTCR_S2M_WAKEUP_SRC_INT_EN ((u32)(1 << 15))
#define CPM_PADWKINTCR_S2M_WAKEUP_SRC_INT_FLAG ((u32)(1 << 7))
#define CPM_PADWKINTCR_PAD_SS3_WAKEUP_SRC_STATUS ((u32)(1 << 28))
#define CPM_PADWKINTCR_PAD_SS3_WAKEUP_SRC_EN ((u32)(1 << 20))
#define CPM_PADWKINTCR_PAD_SS3_WAKEUP_SRC_INT_EN ((u32)(1 << 12))
#define CPM_PADWKINTCR_PAD_SS3_WAKEUP_SRC_INT_FLAG ((u32)(1 << 4))
#define CPM_PADWKINTCR_PCI_ATIMER_WAKEUP_SRC_STATUS ((u32)(1 << 27))
#define CPM_PADWKINTCR_PCI_ATIMER_WAKEUP_SRC_EN ((u32)(1 << 19))
#define CPM_PADWKINTCR_PCI_ATIMER_WAKEUP_SRC_INT_EN ((u32)(1 << 11))
#define CPM_PADWKINTCR_PCI_ATIMER_WAKEUP_SRC_INT_FLAG ((u32)(1 << 3))
#define CPM_PADWKINTCR_PCI_DET_WAKEUP_SRC_STATUS ((u32)(1 << 26))
#define CPM_PADWKINTCR_PCI_DET_WAKEUP_SRC_EN ((u32)(1 << 18))
#define CPM_PADWKINTCR_PCI_DET_WAKEUP_SRC_INT_EN ((u32)(1 << 10))
#define CPM_PADWKINTCR_PCI_DET_WAKEUP_SRC_INT_FLAG ((u32)(1 << 2))
#define CPM_PADWKINTCR_WAKE_WAKEUP_SRC_STATUS ((u32)(1 << 25))
#define CPM_PADWKINTCR_WAKE_WAKEUP_SRC_EN ((u32)(1 << 17))
#define CPM_PADWKINTCR_WAKE_WAKEUP_SRC_INT_EN ((u32)(1 << 9))
#define CPM_PADWKINTCR_WAKE_WAKEUP_SRC_INT_FLAG ((u32)(1 << 1))
#define CPM_PADWKINTCR_USB_DET_WAKEUP_SRC_STATUS ((u32)(1 << 24))
#define CPM_PADWKINTCR_USB_DET_WAKEUP_SRC_EN ((u32)(1 << 16))
#define CPM_PADWKINTCR_USB_DET_WAKEUP_SRC_INT_EN ((u32)(1 << 8))
#define CPM_PADWKINTCR_USB_DET_WAKEUP_SRC_INT_FLAG ((u32)(1 << 0))

/*wakeup filter counter register:FILTCNTR*/
/*card power on counter register:CARDPOCR*/
/*rtc 32k stable time register:RTCSTIMER*/
/*multiple reset control register:MULTIRSTCR*/
/*system reset control register:SYSRSTCR*/
/*ahb3 reset control register:AHB3RSTCR*/
/*arith reset control register:ARITHRSTTCR*/
/*ips reset control register:IPRSTCR*/

/*sleep config register 2:SLPCFGR2*/
#define CPM_SLPCFGR2_S2M_WAKEUP_SRC_SGL_INT_EN ((u32)(1 << 23))
#define CPM_SLPCFGR2_PAD_SS3_WAKEUP_SRC_SGL_INT_EN ((u32)(1 << 20))
#define CPM_SLPCFGR2_PCI_ATIMER_WAKEUP_SRC_SGL_INT_EN ((u32)(1 << 19))
#define CPM_SLPCFGR2_PCI_DET_WAKEUP_SRC_SGL_INT_EN ((u32)(1 << 18))
#define CPM_SLPCFGR2_WAKE_WAKEUP_SRC_SGL_INT_EN ((u32)(1 << 17))
#define CPM_SLPCFGR2_USB_DET_WAKEUP_SRC_SGL_INT_EN ((u32)(1 << 16))
#define CPM_SLPCFGR2_CORE_F_CLK_SLEEP_EN ((u32)(1 << 11))
#define CPM_SLPCFGR2_CLKOUT_CLK_SLEEP_EN ((u32)(1 << 10))
#define CPM_SLPCFGR2_CPM_IPS_CLK_SLEEP_EN ((u32)(1 << 9))
#define CPM_SLPCFGR2_TC_CLK_SLEEP_EN ((u32)(1 << 8))
#define CPM_SLPCFGR2_VDDWK_POWER_DOMAIN_SWITCH_MASK ((u32)(1 << 5))
#define CPM_SLPCFGR2_VDDPD_POWER_DOMAIN_RETENTION_MASK ((u32)(1 << 0))

/*power down counter register:PDNCNTR*/
/*power down counter register:PONCNTR*/

/*pad ss3 control register:PADSS3CR*/
#define CPM_PADSS3CR_PAD_SS3_CONTROL_EN ((u32)(1 << 7))
#define CPM_PADSS3CR_PAD_SS3_WAKEUP_SRC_MASK_EN ((u32)(1 << 3))
#define CPM_PADSS3CR_PAD_SS3_OUTPUT_HIGH_MASK ((u32)(1 << 2))
#define CPM_PADSS3CR_PAD_SS3_OUTPUT_EN ((u32)(1 << 1))
#define CPM_PADSS3CR_PAD_SS3_PULL_EN ((u32)(1 << 0))
/*wake up source control register:WKPSCR*/
#endif
    typedef struct
    {
        volatile u32 SLPCFGR; /**< 00 sleep configuration register 										*/
        volatile u32 SLPCR;   /**< 04 sleep control register 													*/
        volatile u32 SCDIVR;  /**< 08 system clock divider register										*/
        volatile u32 PCDIVR1; /**< 0C speripheral clock divider register 1 						*/

        volatile u32 PCDIVR2;  /**< 10 speripheral clock divider register 2						 */
        volatile u32 PCDIVR3;  /**< 14 speripheral clock divider register 3						 */
        volatile u32 CDIVUPDR; /**< 18 clock divider update register 									 */
        volatile u32 CDIVENR;  /**< 1C clock divider enable register 									 */

        volatile u32 OCSR;     /**< 20 oscillator control and status register					 */
        volatile u32 CSWCFGR;  /**< 24 clock switch config register										 */
        volatile u32 CTICKR;   /**< 28 core tick timer register												 */
        volatile u32 CHIPCFGR; /**< 2C chip config register														 */

        volatile u32 PWRCR;      /**< 30 power control register														*/
        volatile u32 SLPCNTR;    /**< 34 sleep counter register														*/
        volatile u32 WKPCNTR;    /**< 38 wake up counter register													*/
        volatile u32 MULTICGTCR; /**< 3C multiple clock gate control register							*/

        volatile u32 SYSCGTCR;   /**< 40 system clock gate control register								*/
        volatile u32 AHB3CGTCR;  /**< 44 ahb3 clock gate control registe 									*/
        volatile u32 ARITHCGTCR; /**< 48 arith clock gate control registe									*/
        volatile u32 IPSCGTCR;   /**< 4C ips clock gate control registe										*/

        volatile u32 VCCGTRIMR; /**< 50 vcc general trim register												 */
        volatile u32 VCCLTRIMR; /**< 54 vcc lv detect trim register											 */
        volatile u32 VCCVTRIMR; /**< 58 vcc vref trim register 													 */
        volatile u32 VCCCTMR;   /**< 5C vcc core test mode register											 */

        volatile u32 O8MTRIMR;   /**< 60 osc8mhz trim register 														*/
        volatile u32 RESERVED1;  /**< 64  */
        volatile u32 O500MTRIMR; /**< 68 osc500mhz trim register 													*/
        volatile u32 CARDTRIMR;  /**< 6C card ldo trim register														*/

        volatile u32 OSCLSTIMER; /**< 70 oscl stable time register 												*/
        volatile u32 OSCHSTIMER; /**< 74 osch stable time register 												*/
        volatile u32 OSCESTIMER; /**< 78 osce stable time register 												*/
        volatile u32 PWRSR;      /**< 7C power status register 														*/

        volatile u32 EPORTSLPCFGR; /**< 80 eport sleep config register 											*/
        volatile u32 EPORTCGTCR;   /**< 84 eport clock gate control register 								*/
        volatile u32 EPORTRSTCR;   /**< 88 eport reset control register											*/
        volatile u32 RTCTRIMR;     /**< 8C rtc trim register 																*/

        volatile u32 PADWKINTCR; /**< 90 pad wakeup interrupt control register 						*/
        volatile u32 FILTCNTR;   /**< 94 wakeup filter counter register										*/
        volatile u32 CARDPOCR;   /**< 98 card power on counter register										*/
        volatile u32 RTCSTIMER;  /**< 9C rtc 32k stable time register											*/

        volatile u32 MPDSLPCR;   /**< A0 memory power down sleep control register					*/
        volatile u32 MRMCR;      /**< A4      */
        volatile u32 WKPPADCR;   /**< A8   */
        volatile u32 MULTIRSTCR; /**< AC multiple reset control register 									*/

        volatile u32 SYSRSTCR;    /**< B0 system reset control register										 */
        volatile u32 AHB3RSTCR;   /**< B4 ahb3 reset control register											 */
        volatile u32 ARITHRSTTCR; /**< B8 arith reset control register 										 */
        volatile u32 IPRSTCR;     /**< BC ips reset control register 											 */

        volatile u32 SLPCFGR2;     /**< C0 sleep config register 2 													*/
        volatile u32 RESERVED4[3]; /**< C4 C8 CC */

        volatile u32 PDNCNTR;  /**< D0 power down counter register 											*/
        volatile u32 PONCNTR;  /**< D4 power on counter register 												*/
        volatile u32 PADSS3CR; /**< D8 pad ss3 control register													*/
        volatile u32 WKPSCR;   /**< DC wake up source control register 									*/
    } CPM_TypeDef;
#define CPM ((CPM_TypeDef *)CPM_BASE_ADDR)

#ifdef __MACRO_DEF_DMAC__
/*DMAC_CTRLn*/
#define DMAC_LLP_SRC_EN ((u32)(1 << 28))             /**<  */
#define DMAC_LLP_DST_EN ((u32)(1 << 27))             /**<  */
#define DMAC_TT_FC_MASK ((u32)(0x00700000))          /**< */
#define DMAC_TT_FC_SHIFT_MASK ((u32)(20))            /**<   */
#define DMAC_TT_FC_BITS_MASK ((u32)(0xFF8FFFFF))     /**< */
#define DMAC_SRC_MSIZE_MASK ((u32)(0x0001C000))      /**< */
#define DMAC_SRC_MSIZE_SHIFT_MASK ((u32)(14))        /**< */
#define DMAC_SRC_MSIZE_BITS_MASK ((u32)(0xFFFE3FFF)) /**< */

#define DMAC_DEST_MSIZE_MASK ((u32)(0x00003800))      /**< */
#define DMAC_DEST_MSIZE_SHIFT_MASK ((u32)(11))        /**<   */
#define DMAC_DEST_MSIZE_BITS_MASK ((u32)(0xFFFC7FFF)) /**< */

#define DMAC_SINC_INC ((u32)(1 << 0))
#define DMAC_SINC_DEC                                      ((u32)(1<<9)
#define DMAC_SINC_NO_CHANGE ((u32)(1 << 10))
#define DMAC_SINC_INC_MASK ((u32)(0x00000600))      /**< */
#define DMAC_SINC_INC_SHIFT_MASK ((u32)(9))         /**<   */
#define DMAC_SINC_INC_BITS_MASK ((u32)(0xFFFFF9FF)) /**< */

#define DMAC_DINC_INC ((u32)(1 << 0))
#define DMAC_DINC_DEC                                      ((u32)(1<<7)
#define DMAC_DINC_NO_CHANGE ((u32)(1 << 8))
#define DMAC_DINC_INC_MASK ((u32)(0x00000180))      /**< */
#define DMAC_DINC_INC_SHIFT_MASK ((u32)(7))         /**<  */
#define DMAC_DINC_INC_BITS_MASK ((u32)(0xFFFFFE7F)) /**< */

#define DMAC_SRC_TR_WIDTH_MASK ((u32)(0x00000070))      /**< */
#define DMAC_SRC_TR_WIDTH_SHIFT_MASK ((u32)(4))         /**<  */
#define DMAC_SRC_TR_WIDTH_BITS_MASK ((u32)(0xFFFFFF1F)) /**< */
#define DMAC_DST_TR_WIDTH_MASK ((u32)(0x00000007))      /**< */
#define DMAC_DST_TR_WIDTH_SHIFT_MASK ((u32)(1))         /**<  */
#define DMAC_DST_TR_WIDTH_BITS_MASK ((u32)(0xFFFFFFF1)) /**< */
#define DMAC_INT_EN ((u32)(1 << 0))
/*DMA_CFGn*/
#define DMAC_MAX_ABRS_MASK ((u32)(0x3FF00000))
#define DMAC_MAX_ABRS_SHIFT_MASK ((u32)(20)) /* */
#define DMAC_MAX_ABRS_BITS_MASK ((u32)(0xC00FFFFF))
#define DMAC_HS_SEL_SRC ((u32)(1 << 11))
#define DMAC_HS_SEL_DST ((u32)(1 << 10))
#define DMAC_FIFO_EMPTY ((u32)(1 << 9))
#define DMAC_CH_SUSP ((u32)(1 << 8))

#define DMAC_CH_PRIOR_MASK ((u32)(0x000000E0))
#define DMAC_CH_PRIOR_SHIFT_MASK ((u32)(5)) /* */
#define DMAC_CH_PRIOR_BITS_MASK ((u32)(0xFFFFFF1F))

/*DMA_CFG_HIGHn*/
#define DMAC_DST_PER_MASK ((u32)(0x00007800))
#define DMAC_DST_PER_SHIFT_MASK ((u32)(11)) /* */
#define DMAC_DST_PER_BITS_MASK ((u32)(0xFFFF87FF))

#define DMAC_SRC_PER_MASK ((u32)(0x00000780))
#define DMAC_SRC_PER_SHIFT_MASK ((u32)(7)) /* */
#define DMAC_SRC_PER_BITS_MASK ((u32)(0xFFFFF87F))

#define DMAC_FC_MODE ((u32)(1 << 0))

/**/
#define DMAC_CH_WE_EN_SHIFT_MASK ((u32)(8)) /* */
#define DMAC_CH_EN_MASK ((u32)(0x0000000F))
#define DMAC_EN ((u8)(1 << 0))
    /**/
    typedef struct _DMA_CHANNEL_REG
    {
        volatile u32 DMA_SADDR;     // 0x0
        volatile u32 DMA_SADDR_H;   // 0x04
        volatile u32 DMA_DADDR;     // 0x08
        volatile u32 DMA_DADDR_H;   // 0x0c
        volatile u32 DMA_LLP;       // 0x10
        volatile u32 DMA_LLP_H;     // 0x14
        volatile u32 DMA_CTRL;      // 0x18
        volatile u32 DMA_CTRL_HIGH; // 0x1c
        volatile u32 RESERVED[8];   // reserved[8];
        volatile u32 DMA_CFG;       // 0x40
        volatile u32 DMA_CFG_HIGH;  // 0x44

    } DMA_CHANNEL_REG;
    /****************DMA CONTROL Register define *************************/

    typedef struct _DMA_CONTROL_REG
    {
        volatile u32 DMA_RAWTFR;     // 0x00//0x2c0
        volatile u32 RESERVED1;      // 0x04//0x2c4
        volatile u32 DMA_RAWBLOCK;   // 0x08////0x2c8
        volatile u32 RESERVED2;      // 0x0c////0x2cc
        volatile u32 DMA_RAWSRCTRAN; // 0x10//0x2d0
        volatile u32 RESERVED3;      // 0x14//0x2d4
        volatile u32 DMA_RAWDSTTRAN; // 0x18//0x2d8
        volatile u32 RESERVED4;      // 0x1c//0x2dc
        volatile u32 DMA_RAWERR;     // 0x20//0x2e0
        volatile u32 RESERVED5;      // 0x24//0x2e4
        volatile u32 DMA_STATTFR;    // 0x28//0x2e8
        volatile u32 RESERVED6;      // 0x2c//0x2ec
        volatile u32 DMA_STATBLOCK;  // 0x30//0x2f0
        volatile u32 RESERVED7;      // 0x34//0x2f4
        volatile u32 DMA_STATSRC;    // 0x38//0x2f8
        volatile u32 RESERVED8;      // 0x3c //0x2fc

        volatile u32 DMA_STATDST; // 0x40//0x300
        volatile u32 RESERVED9;   // 0x44//0x304
        volatile u32 DMA_STATERR; // 0x48//0x308
        volatile u32 RESERVED10;  // 0x4c//0x30c

        volatile u32 DMA_MASKTFR;   // 0x50//0x310
        volatile u32 RESERVED11;    // 0x54//0x314
        volatile u32 DMA_MASKBLOCK; // 0x58//0x318
        volatile u32 RESERVED12;    // 0x5c//0x31c

        volatile u32 DMA_MASKSRC; // 0x60//0x320
        volatile u32 RESERVED13;  // 0x64//0x324
        volatile u32 DMA_MASKDST; // 0x68//0x328
        volatile u32 RESERVED14;  // 0x6c//0x32c

        volatile u32 DMA_MASKERR; // 0x70//0x330
        volatile u32 RESERVED15;  // 0x74//0x334
        volatile u32 DMA_CLRTFR;  // 0x78//0x338
        volatile u32 RESERVED16;  // 0x7c//0x33c

        volatile u32 DMA_CLRBLOCK; // 0x80//0x340
        volatile u32 RESERVED17;   // 0x84//0x344
        volatile u32 DMA_CLRSRC;   // 0x88//0x348
        volatile u32 RESERVED18;   // 0x8c//0x34c

        volatile u32 DMA_CLRDST; // 0x90//0x350
        volatile u32 RESERVED19; // 0x94//0x354
        volatile u32 DMA_CLRERR; // 0x98//0x358
        volatile u32 RESERVED20; // 0x9c//0x35c

        volatile u32 DMA_STATUSINT; // 0x100//0x360
        volatile u32 RESERVED21;    // 0x104//0x364
        volatile u32 DMA_SRCREQ;    // 0x108//0x368
        volatile u32 RESERVED22;    // 0x10c//0x36c

        volatile u32 DMA_DSTREQ;    // 0x110//0x370
        volatile u32 RESERVED23;    // 0x114//0x374
        volatile u32 DMA_SINGLESRC; // 0x118//0x378
        volatile u32 RESERVED24;    // 0x11c//0x37c

        volatile u32 DMA_SINGLEDST; // 0x120//0x380
        volatile u32 RESERVED25;    // 0x124//0x384
        volatile u32 DMA_LASTSRC;   // 0x128//0x388
        volatile u32 RESERVED26;    // 0x12c//0x38c

        volatile u32 DMA_LASTDST; // 0x130//0x390
        volatile u32 RESERVED27;  // 0x134//0x394
        volatile u32 DMA_CONFIG;  // 0x138//0x398
        volatile u32 RESERVED28;  // 0x13c//0x39c

        volatile u32 DMA_CHEN; // 0x140//0x3a0

    } DMA_CONTROL_REG;

    typedef struct _DMA_LLI
    {
        volatile u32 src_addr;
        volatile u32 dst_addr;
        volatile u32 next_lli;
        volatile u32 control0;
        volatile u32 len;
        volatile u32 hs_sel;
        volatile u32 per;
    } DMA_LLI;
#endif
    typedef struct
    {
        volatile u32 DAC_CR;    // 0x00
        volatile u32 DAC_DR;    // 0x04
        volatile u32 DAC_SWTR;  // 0x08
        volatile u32 DAC_DOR;   // 0x0c
        volatile u32 DAC_FSR;   // 0x10
        volatile u32 DAC_TRIMR; // 0x14
    } DAC_TypeDef;
#define DAC ((DAC_TypeDef *)DAC_BASE_ADDR)

    typedef struct _DMA_LLI_SSI
    {
        volatile u32 src_addr;
        volatile u32 dst_addr;
        volatile u32 next_lli;
        volatile u32 control0;
        volatile u32 len;
        volatile u32 hs_sel;
        volatile u32 per;
    } DMA_LLI_SSI;

    typedef struct
    {
        volatile u32 SRCADDR; /**< 0000 DMA Channel Source Address Register */
        volatile u32 RESERVED0;
        volatile u32 DSTADDR; /**< 0008 DMA Channel Destination Address Register */
        volatile u32 RESERVED1;
        volatile u32 LLI; /**< 0010 DMA Channel Linked List Item Register */
        volatile u32 RESERVED2;
        volatile u32 CTRL;        /**< 0018 DMA Channel Control Register */
        volatile u32 CTRL_HIGH;   /**< 001C DMA Channel Control Register */
        __I u32 RESERVED3[8];     /**	20 24 28 2C
                                                                                         30 34 38 3C*/
        volatile u32 CONFIG;      /**< 0040 DMA Channel Configuration Register */
        volatile u32 CONFIG_HIGH; /**< 0044 DMA Channel Configuration Register */
        __I u32 RESERVED4[4];     /**	48 4C  50 54 */
    } DMAC_ChannelTypeDef;

    typedef struct
    {
        DMAC_ChannelTypeDef CH[4]; /**< 0x0000---> 0x015C */

        volatile u32 RESERVED0[88]; /**< 0x0160---> 0x02BC */
        volatile u32 RAWTFR;        /**< 0x02C0 raw status for intTfr interrupt */
        volatile u32 RESERVED1;
        volatile u32 RAWBLOCK; /**< 0x02C8 raw status for intBlock interrupt */
        volatile u32 RESERVED2;
        volatile u32 RAWSRCTRAN; /**< 0x02D0 raw status for intScTran interrupt */
        volatile u32 RESERVED3;
        volatile u32 RAWDETTRAN; /**< 0x02D8 raw status for intDestTran interrupt */
        volatile u32 RESERVED4;
        volatile u32 RAWERR; /**< 0x02EO raw status for intErr interrupt */
        volatile u32 RESERVED5;
        volatile u32 STATTFR; /**< 0x02E8	status for intTfr interrupt */
        volatile u32 RESERVED6;
        volatile u32 STATBLOCK; /**< 0x02F0	status for intBlock interrupt */
        volatile u32 RESERVED7;
        volatile u32 STATSRC; /**< 0x02F8	status for intScTran interrupt */
        volatile u32 RESERVED8;
        volatile u32 STATDST; /**< 0x0300	status for intDestTran interrupt */
        volatile u32 RESERVED9;
        volatile u32 ERR; /**< 0x0308	status for intErr interrupt */
        volatile u32 RESERVED10;
        volatile u32 MASKTFR; /**< 0x0310 mask for intTfr interrupt */
        volatile u32 RESERVED11;
        volatile u32 MASKBLOCK; /**< 0x0318 mask for intBlock interrupt */
        volatile u32 RESERVED12;
        volatile u32 MASKSRC; /**< 0x0320 mask for intScTran interrupt */
        volatile u32 RESERVED13;
        volatile u32 MASKDST; /**< 0x0328 mask for intDestTran interrupt */
        volatile u32 RESERVED14;
        volatile u32 MASKERR; /**< 0x0330 mask for intErr interrupt */
        volatile u32 RESERVED15;
        volatile u32 CLRTFR; /**< 0x0338 clear for intTfr interrupt */
        volatile u32 RESERVED16;
        volatile u32 CLRBLOCK; /**< 0x0340 clear for intBlock interrupt */
        volatile u32 RESERVED17;
        volatile u32 CLRSRC; /**< 0x0348 clear for intScTran interrupt */
        volatile u32 RESERVED18;
        volatile u32 CLRDST; /**< 0x0350 clear for intDestTran interrupt */
        volatile u32 RESERVED19;
        volatile u32 CLRERR; /**< 0x0358 clear for intErr interrupt */
        volatile u32 RESERVED20;
        volatile u32 STATUSINT; /**< 0x0360 status for each interrupt type */
        volatile u32 RESERVED21;
        volatile u32 SRCREQ; /**< 0x0368 transacion request */
        volatile u32 RESERVED22;
        volatile u32 DESTREQ; /**< 0x0370 transacion request */
        volatile u32 RESERVED23;
        volatile u32 SINGLESRC; /**< 0x0378 transacion request */
        volatile u32 RESERVED24;
        volatile u32 SINGLEDST; /**< 0x0380 transacion request */
        volatile u32 RESERVED25;
        volatile u32 LASTSRC; /**< 0x0388 transacion request */
        volatile u32 RESERVED26;
        volatile u32 LASTDST; /**< 0x0390 transacion request */
        volatile u32 RESERVED27;
        volatile u32 CONFIG; /**< 0x0398 confuration  */
        volatile u32 RESERVED28;
        volatile u32 CHEN; /**< 0x03A0 cannel enable  */

    } DMAC_TypeDef;
#define DMAC1 ((DMAC_TypeDef *)DMAC1_BASE_ADDR)
#define DMAC2 ((DMAC_TypeDef *)DMAC2_BASE_ADDR)

#ifdef __MACRO_DEF_EDMAC__
/*EDMACCR*/
#define EDMAC_PIPELINE_EN ((u32)(1 << 31))  /**< */
#define EDMAC_SPI_HW ((u32)(1 << 30))       /**< */
#define EDMAC_PRIOR_CHG_EN ((u32)(1 << 29)) /**< */
#define EDMAC_PRIOR ((u32)(1 << 28))        /**< */
#define EDMAC_SPI_WROPT_EN ((u32)(1 << 27)) /**< */
#define EDMAC_INFINITY_EN ((u32)(1 << 26))  /**< */
#define EDMAC_SPI_FIFO_PRE_LOAD_3 ((u32)(1 << 25))

#define EDMAC_MAJ_DONE_IT_EN ((u32)(1 << 24))                      /**<                                            */
#define EDMAC_MAJ_DONE_IT_DIS ((u32) ~(1 << EDMAC_MAJ_DONE_IT_EN)) /**<                                            */

#define EDMAC_MAJ_CRC_SEL_MAJOR ((u32)(1 << 23)) /**< operation takes place when major loop ends */
#define EDMAC_MAJ_CRC_SEL_MINOR \
    ((u32) ~(1 << EDMAC_MAJ_CRC_SEL_MAJOR)) /**< operation takes place when minor loop ends */

#define EDMAC_CRC_CHANNEL_EN ((u32)(1 << 22))                      /**<                                            */
#define EDMAC_CRC_CHANNEL_DIS ((u32) ~(1 << EDMAC_CRC_CHANNEL_EN)) /**<                                            */
#define EDMAC_SEND_CRC_CHANNEL_EN ((u32)(1 << 21))                 /**<                                            */
#define EDMAC_SEND_CRC_CHANNEL_DIS \
    ((u32) ~(1 << EDMAC_SEND_CRC_CHANNEL_EN))       /**<                                            */
#define EDMAC_WRITE_CRC_CHANNEL_EN ((u32)(1 << 20)) /**<                                            */
#define EDMAC_WRITE_CRC_CHANNEL_DIS \
    ((u32) ~(1 << EDMAC_WRITE_CRC_CHANNEL_EN))      /**<                                            */
#define EDMAC_LINK_SRC_SEL_MASK ((u32)(0xFFF9FFFF)) /**<                                            */
#define EDMAC_LINK_SRC_SEL_SHIFT_MASK ((u8)(1 << 17))
/**<                                                         */
#define EDMAC_LINK_SRC_EDMAC0_CH0_DONE \
    ((u8)(0x00 << EDMAC_LINK_SRC_SEL_MASK)) /**<                                            */
#define EDMAC_LINK_SRC_EDMAC0_CH1_DONE \
    ((u8)(0x01 << EDMAC_LINK_SRC_SEL_MASK)) /**<                                            */
#define EDMAC_LINK_SRC_EDMAC1_CH0_DONE \
    ((u8)(0x02 << EDMAC_LINK_SRC_SEL_MASK)) /**<                                            */
#define EDMAC_LINK_SRC_EDMAC1_CH1_DONE \
    ((u8)(0x03 << EDMAC_LINK_SRC_SEL_MASK)) /**<                                            */

#define EDMAC_LINK_EN ((u32)(1 << 16))                           /**<                                            */
#define EDMAC_LINK_DIS ((u32)(EDMAC_LINK_EN))                    /**<                                            */
#define EDMAC_COMPARE_EN ((u32)(1 << 15))                        /**< 数据比较使能                               */
#define EDMAC_COMPARE_DIS ((u32)(EDMAC_COMPARE_EN))              /**< 数据比较禁止                               */
#define EDMAC_COMPARE_SKIP_EN ((u32)(1 << 14))                   /**< 跳过数据比较使能                           */
#define EDMAC_COMPARE_SKIP_DIS ((u32)(EDMAC_COMPARE_SKIP_EN))    /**< 跳过数据比较使能                           */
#define EDMAC_PRELOAD_DIS ((u32)(1 << 13))                       /**< 预加载数据禁止                             */
#define EDMAC_PRELOAD_EN ((u32)(~EDMAC_PRELOAD_DIS))             /**< 预加载数据使能                             */
#define EDMAC_MEM_TRANS_CRC_EN ((u32)(1 << 12))                  /**<                                            */
#define EDMAC_MEM_TRANS_CRC_DIS ((u32)(~EDMAC_MEM_TRANS_CRC_EN)) /**<                                            */
#define EDMAC_PERIPHERAL_NUMBER_MASK ((u32)(0xFFFFF0FF))         /**<                                            */
#define EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK ((u8)(1 << 8))        /**<                                            */
#define EDMAC_PERIPHERAL_TYPE_SPI1 \
    ((u8)(0x00 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:edmac1                           */
#define EDMAC_PERIPHERAL_TYPE_SPI2 \
    ((u8)(0x01 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:edmac2                           */
#define EDMAC_PERIPHERAL_TYPE_USI1 \
    ((u8)(0x02 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:USI1                             */
#define EDMAC_PERIPHERAL_TYPE_USI3 \
    ((u8)(0x04 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:USI3                             */
#define EDMAC_PERIPHERAL_TYPE_USI2 \
    ((u8)(0x03 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:USI2                             */
#define EDMAC_PERIPHERAL_TYPE_AES \
    ((u8)(0x05 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:AES                              */
#define EDMAC_PERIPHERAL_TYPE_DES \
    ((u8)(0x06 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:DES                              */
#define EDMAC_PERIPHERAL_TYPE_SM1 \
    ((u8)(0x07 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:SM1                              */
#define EDMAC_PERIPHERAL_TYPE_SCI1 \
    ((u8)(0x08 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:SCI1                             */
#define EDMAC_PERIPHERAL_TYPE_SMS4 \
    ((u8)(0x09 << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:SM4                              */
#define EDMAC_PERIPHERAL_TYPE_SHA \
    ((u8)(0x0A << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:SHA                              */
#define EDMAC_PERIPHERAL_TYPE_SSF33 \
    ((u8)(0x0B << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:SSF33                            */
#define EDMAC_PERIPHERAL_TYPE_CRYPTO \
    ((u8)(0x0C << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:CRYPTO                           */
#define EDMAC_PERIPHERAL_TYPE_SCI2 \
    ((u8)(0x0D << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:SCI2                             */
#define EDMAC_PERIPHERAL_TYPE_SPI3 \
    ((u8)(0x0E << EDMAC_PERIPHERAL_NUMBER_SHIFT_MASK)) /**< EDAMC设备:SPI3                             */

#define EDMAC_CHANNEL_PERIPHERAL_SPI1 ((u8)(0x00))                    /**< EDAMC设备:edmac1                       */
#define EDMAC_CHANNEL_PERIPHERAL_SPI2 ((u8)(0x01))                    /**< EDAMC设备:edmac2                       */
#define EDMAC_CHANNEL_PERIPHERAL_USI1 ((u8)(0x02))                    /**< EDAMC设备:USI1                         */
#define EDMAC_CHANNEL_PERIPHERAL_USI2 ((u8)(0x03))                    /**< EDAMC设备:USI2                         */
#define EDMAC_CHANNEL_PERIPHERAL_AES ((u8)(0x05))                     /**< EDAMC设备:AES                          */
#define EDMAC_CHANNEL_PERIPHERAL_DES ((u8)(0x06))                     /**< EDAMC设备:DES                          */
#define EDMAC_CHANNEL_PERIPHERAL_SM1 ((u8)(0x07))                     /**< EDAMC设备:SM1                          */
#define EDMAC_CHANNEL_PERIPHERAL_UART1 ((u8)(0x08))                   /**< EDAMC设备:SCI1                         */
#define EDMAC_CHANNEL_PERIPHERAL_SMS4 ((u8)(0x09))                    /**< EDAMC设备:SM4                          */
#define EDMAC_CHANNEL_PERIPHERAL_SHA ((u8)(0x0A))                     /**< EDAMC设备:SHA                          */
#define EDMAC_CHANNEL_PERIPHERAL_UART3 ((u8)(0x0B))                   /**< EDAMC设备:SSF33                        */
#define EDMAC_CHANNEL_PERIPHERAL_CRYPTO ((u8)(0x0C))                  /**< EDAMC设备:CRYPTO                       */
#define EDMAC_CHANNEL_PERIPHERAL_UART2 ((u8)(0x0D))                   /**< EDAMC设备:SCI2                         */
#define EDMAC_CHANNEL_PERIPHERAL_SPI3 ((u8)(0x0E))                    /**< EDAMC设备:SPI3                         */
#define EDMAC_CHANNEL_PERIPHERAL_ZUC ((u8)(0x0F))                     /**< EDAMC设备:SPI3                         */
#define EDMAC_START_IT_EN ((u32)(1 << 7))                             /**< EDMAC启动中断:使能                     */
#define EDMAC_START_IT_DIS ((u32)(~(1 << EDMAC_START_IT_EN)))         /**< EDMAC完成中断 使能                     */
#define EDMAC_DIR_MASK ((u32)(0xFFFFFF9F))                            /**< */
#define EDMAC_DIR_SHIFT_MASK ((u8)(1 << 5))                           /**< */
#define EDMAC_TTYPE_SRAM_TO_SRAM ((u8)(0x00 << EDMAC_DIR_SHIFT_MASK)) /**< EDMAC传输类型:从RAM到RAM               */
#define EDMAC_TTYPE_PERIPHERAL_TO_SRAM \
    ((u8)(0x01 << EDMAC_DIR_SHIFT_MASK)) /**< EDMAC传输类型:从设备到RAM              */
#define EDMAC_TTYPE_SRAM_TO_PERIPHERAL \
    ((u8)(0x02 << EDMAC_DIR_SHIFT_MASK))                      /**< EDMAC传输类型:从RAM到设备              */
#define EDMAC_TTYPE_BOTH ((u8)(0x03 << EDMAC_DIR_SHIFT_MASK)) /**< EDMAC传输类型:从RAM到设备及从设备到RAM */
#define EDMAC_DIR_SRAM_TO_SRAM ((u8)(1 << 0x00))              /**< EDMAC传输类型:从RAM到RAM               */
#define EDMAC_DIR_PERIPHERAL_TO_SRAM ((u8)(1 << 0x01))        /**< EDMAC传输类型:从设备到RAM              */
#define EDMAC_DIR_SRAM_TO_PERIPHERAL ((u8)(1 << 0x02))        /**< EDMAC传输类型:从RAM到设备              */
#define EDMAC_DIR_BOTH ((u8)(1 << 0x03))                      /**< EDMAC传输类型:从RAM到设备及从设备到RAM */
#define EDMAC_SPI_FIFO_PRELOAD_MASK ((u32)(0xFFFFFFF3))       /**<                                        */
#define EDMAC_SPI_FIFO_PRELOAD_SHIFT_MASK ((u8)(1 << 2))      /**<                                        */
#define EDMAC_SPI_FIFO_PRELOAD_NOT \
    ((u32)(0x00 << EDMAC_SPI_FIFO_PRELOAD_SHIFT_MASK)) /**<                                        */
#define EDMAC_SPI_FIFO_PRELOAD_2BYTES \
    ((u32)(0x01 << EDMAC_SPI_FIFO_PRELOAD_SHIFT_MASK)) /**<                                        */
#define EDMAC_SPI_FIFO_PRELOAD_4BYTES \
    ((u32)(0x02 << EDMAC_SPI_FIFO_PRELOAD_SHIFT_MASK)) /**<                                        */
#define EDMAC_SPI_FIFO_PRELOAD_8BYTES \
    ((u32)(0x03 << EDMAC_SPI_FIFO_PRELOAD_SHIFT_MASK)) /**<                                        */

#define EDMAC_VALID_FLAG ((u32)(1 << 1))                             /**< EDAMC VALID标志                        */
#define EDMAC_UNVALID_FLAG ((u32)(~(1 << EDMAC_VALID_FLAG)))         /**< EDMAC UNVALID标志                      */
#define EDMAC_MIN_DONE_IT_EN ((u32)(1 << 0))                         /**<                                        */
#define EDMAC_MIN_DONE_IT_DIS ((u32) ~((1 << EDMAC_MIN_DONE_IT_EN))) /**<                                        */

/*EDMACCSR*/
#define EDMAC_SSF_SPI1_FLAG ((u32)(1 << 31)) /**< EDAMC SPI SS 位                        */
#define EDMAC_SSF_SPI2_FLAG ((u32)(1 << 30)) /**< EDAMC SPI SS 位                        */
#define EDMAC_SSF_SPI3_FLAG ((u32)(1 << 29)) /**< EDAMC SPI SS 位                        */
#define EDMAC_SSF_SPI4_FLAG ((u32)(1 << 28)) /**< EDAMC SPI SS 位                        */
#define EDMAC_DUMMY_EN ((u32)(1 << 24))      /**< EDAMC SPI SS 位                        */

#define EDMAC_EN ((u32)(1 << 16))           /**< EDAMC 功能使能                         */
#define EDMAC_DIS ((u32)(~(1 << EDMAC_EN))) /**< EDAMC 功能禁止                         */
#define EDMAC_FLAG_FAIL ((u32)(1 << 15))    /**< EDAMC                                  */
#define EDMAC_FLAG_SCHNUM ((u32)(1 << 7))   /**< EDAMC 启动通道位                       */
#define EDMAC_FLAG_DCHNUM ((u32)(1 << 6))   /**< EDAMC 完成通道位                       */
#define EDMAC_FLAG_MAJ_DONE ((u32)(1 << 3)) /**< EDMAC                                  */
#define EDMAC_FLAG_START ((u32)(1 << 2))    /**< EDMAC                                  */
#define EDMAC_FLAG_MIN_DONE ((u32)(1 << 1)) /**< EDMAC                                  */
#define EDMAC_FLAG_BUSY ((u32)(1 << 0))     /**< EDMAC                                  */
/*EDMACRBAR*/
#define EDMAC_READ_BUFFER_INC_EN ((u32)(1 << 31)) /**< 读缓冲地址自增使能                     */
#define EDMAC_READ_BUFFER_INC_DIS \
    ((u32)(~(1 << EDMAC_READ_BUFFER_INC_EN)))     /**< 读缓冲地址自增禁止                     */
#define EDMAC_READ_BUFFER_DEC_EN ((u32)(1 << 30)) /**< 写缓冲地址自减使能                     */
#define EDMAC_READ_BUFFER_DEC_DIS \
    ((u32)(~(1 << EDMAC_READ_BUFFER_DEC_EN)))               /**< 写缓冲地址自减禁止                     */
#define EDMAC_READ_BUFFER_BASE_ADD_MASK ((u32)(0xFFF00000)) /**<                                        */
#define EDMAC_READ_BUFFER_BASE_ADD_SHIFTMASK ((u8)(1 << 0)) /**<                                        */
/*EDMACWBAR*/
#define EDMAC_WRITE_BUFFER_INC_EN ((u32)(1 << 31)) /**< 写缓冲地址自增使能                     */
#define EDMAC_WRITE_BUFFER_INC_DIS \
    ((u32)(~(1 << EDMAC_WRITE_BUFFER_INC_EN)))     /**< 写缓冲地址自增禁止                     */
#define EDMAC_WRITE_BUFFER_DEC_EN ((u32)(1 << 30)) /**< 写缓冲地址自减使能                     */
#define EDMAC_WRITE_BUFFER_DEC_DIS \
    ((u32)(~(1 << EDMAC_WRITE_BUFFER_DEC_EN)))               /**< 写缓冲地址自减禁止                     */
#define EDMAC_WRITE_BUFFER_BASE_ADD_MASK ((u32)(0xFFF00000)) /**<                                        */
#define EDMAC_WRITE_BUFFER_BASE_ADD_SHIFTMASK ((u8)(1 << 0))

#define EDMAC_MINOR_TRANSFER_DATA_SUM_MASK ((u32)(0xFFF00000))   /**<                                        */
#define EDMAC_MINOR_TRANSFER_DATA_SUM_SHIFT_MASK ((u32)(1 << 0)) /**<                                        */
#define EDMAC_MINOR_TRANSFER_DATA_CNT_MASK ((u32)(0xFFF00000))   /**<                                        */
#define EDMAC_MINOR_TRANSFER_DATA_CNT_SHIFT_MASK ((u32)(1 << 0)) /**<                                        */
#define EDMAC_MAJOR_TRANSFER_DATA_SUM_MASK ((u32)(0xFFF00000))   /**<                                        */
#define EDMAC_MAJOR_TRANSFER_DATA_SUM_SHIFT_MASK ((u32)(1 << 0)) /**<                                        */
#define EDMAC_MAJOR_TRANSFER_DATA_CNT_MASK ((u32)(0xFFF00000))   /**<                                        */
#define EDMAC_MAJOR_TRANSFER_DATA_CNT_SHIFT_MASK ((u32)(1 << 0)) /**<                                        */
#define EDMAC_PERIPHERAL_ADD_MASK ((u32)(0xFFFF0000))            /**<                                        */
#define EDMAC_PERIPHERAL_ADD_SHIFT_MASK ((u32)(1 << 0))          /**<                                        */

/*EDMACRBARSTEP*/
#define EDMAC_READ_BUFFER_ADD_STEP_EN ((u32)(1 << 31))
#define EDMAC_READ_BUFFER_ADD_DECREASE ((u32)(1 << 30))
#define EDMAC_READ_BUFFER_ADD_STEP_MASK ((u32)(0xFFFF0000))  /**<                                        */
#define EDMAC_READ_BUFFER_ADD_STEP_SHIFTMASK ((u32)(1 << 0)) /**<                                        */
/*EDMACWBARSTEP*/
#define EDMAC_WRITE_BUFFER_ADD_STEP_EN ((u32)(1 << 31))
#define EDMAC_WRITE_BUFFER_ADD_DECREASE ((u32)(1 << 30))
#define EDMAC_WRITE_BUFFER_ADD_STEP_MASK ((u32)(0xFFFF0000))  /**<                                        */
#define EDMAC_WRITE_BUFFER_ADD_STEP_SHIFTMASK ((u32)(1 << 0)) /**<                                        */
/*LASTMINSUMR*/
#define EDMAC_LAST_MINUM_SUM_EN ((u32)(1 << 31))
#define EDMAC_LAST_MINUM_SUM_MASK ((u32)(0xFFFF0000)) /**<                                        */
#define EDMAC_WRITE_BUFFER_ADD_STEP_SHIFTMASK ((u32)(1 << 0))
#endif
    typedef struct
    {
        volatile u32 CR;          /**< */
        volatile u32 CSR;         /**< */
        volatile u32 RBAR;        /**< */
        volatile u32 WBAR;        /**< */
        volatile u32 MINSUMR;     /**< */
        volatile u32 MINCNTR;     /**< */
        volatile u32 MAJSUMR;     /**< */
        volatile u32 MAJCNTR;     /**< */
        volatile u32 SPAR;        /**< */
        volatile u32 WBARSTEP;    /**< */
        volatile u32 LASTMINSUMR; /**< */
    } EDMAC_TypeDef;
#define EDMAC0_0 ((EDMAC_TypeDef *)EDMAC0_0_BASE_ADDR)
#define EDMAC0_1 ((EDMAC_TypeDef *)EDMAC0_1_BASE_ADDR)
#define EDMAC1_0 ((EDMAC_TypeDef *)EDMAC1_0_BASE_ADDR)
#define EDMAC1_1 ((EDMAC_TypeDef *)EDMAC1_1_BASE_ADDR)

#ifdef __MACRO_DEF_EFLASH__
#define EFLASH_EFCR_DISECC ((u32)0x01 << 31)
#define EFLASH_EFCR_INFOSWITCH ((u32)0x01 << 30)
#define EFLASH_EFCR_AHB_ERROR_RES ((u32)0x01 << 29)
#define EFLASH_EFCR_RWW_RES ((u32)0x01 << 28)

#define EFLASH_EFAPR_WPASSWD_SHIFT (8)
#define EFLASH_EFAPR_IOAP_SHIFT (4)
#define EFLASH_EFAPR_MOAP_SHIFT (0)

#define EFLASH_EFSTAT_PWRRDY ((u32)0x01 << 31)
#define EFLASH_EFSTAT_GRD_READ_VLD ((u32)0x01 << 30)
#define EFLASH_EFSTAT_EFM_IDLE ((u32)0x01 << 15)
#define EFLASH_EFSTAT_FLASH0_SLEEP ((u32)0x01 << 6)
#define EFLASH_EFSTAT_ECC_ERROR0 ((u32)0x01 << 5)
#define EFLASH_EFSTAT_RDWW0 ((u32)0x01 << 4)
#define EFLASH_EFSTAT_DONE0 ((u32)0x01 << 3)
#define EFLASH_EFSTAT_PEGOOD0 ((u32)0x01 << 2)
#define EFLASH_EFSTAT_SMWERR0 ((u32)0x01 << 1)
#define EFLASH_EFSTAT_FLASH_BUSY0 ((u32)0x01 << 0)

#define EFLASH_EFINTM_ECC_ERROR0_MASK ((u32)0x01 << 5)
#define EFLASH_EFINTM_ECC_RDWW0_MASK ((u32)0x01 << 4)
#define EFLASH_EFINTM_ECC_DONE0_MASK ((u32)0x01 << 3)

#define EFLASH_EFCMD_CMD0_VALID ((u32)0x01 << 15)
#define EFLASH_EFCMD_PRGW_NUM0_SHIFT (8)
#define EFLASH_EFCMD_CMD0_SHIFT (0)

#define EFLASH_EECCECNT_OVD ((u32)0x01 << 31)
#define EFLASH_EECCECNT_ECC_ERR_CNT_SHIFT (0)

#define EFLASH_ETIMBASE_20NS_BASE_OF_CLK_SHIFT (8)
#define EFLASH_ETIMBASE_1US_BASE_OF_CLK_SHIFT (0)

#define EFLASH_ETIMCFG_CFG_TNVSE_SHIFT (26)
#define EFLASH_ETIMCFG_CFG_TPWS_SHIFT (22)
#define EFLASH_ETIMCFG_CFG_TRE_SHIFT (17)
#define EFLASH_ETIMCFG_CFG_TRE_SHIFT (17)
#define EFLASH_ETIMCFG_CFG_TME_SHIFT (12)
#define EFLASH_ETIMCFG_CFG_TPROG_SHIFT (8)
#define EFLASH_ETIMCFG_CFG_TNVH_SHIFT (4)
#define EFLASH_ETIMCFG_CFG_TNVSP_SHIFT (0)

#define EFLASH_SMWOP0_WMV_SHIFT (22)
#define EFLASH_SMWOP0_WHV_SHIFT (18)
#define EFLASH_SMWOP0_WIPGM_SHIFT (16)
#define EFLASH_SMWOP0_SLOW_CLK_EN ((u32)0x01 << 13)
//#define EFLASH_SMWOP0_200NS_CLK_NUM _SHIFT(8)
#define EFLASH_SMWOP0_1US_CLK_NUM_SHIFT (0)

#define EFLASH_SMWINFO0_SMWR_LOOP0_SHIFT (16)
#define EFLASH_SMWINFO0_SMWR_LASTSET0_SHIFT (0)

#define EFLASH_EFDIRCTRLEN_DIRC0_EN ((u32)0x01 << 31)

#define EFLASH_DIRCADDR_DIRC_XADR_SHIFT (6)
#define EFLASH_DIRCADDR_DIRC_YADR_SHIFT (0)

#define EFLASH_EFUSEVDDQEN_EFUSE_VDDQ_EN ((u32)0x01 << 0)
#endif
    typedef struct
    {
        volatile u32 EFCR;           /**< 00 */
        volatile u32 EFAPR;          /**< 04 */
        volatile u32 EFSTAT;         /**< 08 */
        volatile u32 EFINTM;         /**< 0C */
        volatile u32 EFCMD;          /**< 10 */
        volatile u32 EECCECNT;       /**< 14 */
        volatile u32 ETIMBASE;       /**< 18 */
        volatile u32 ETIMCFG;        /**< 1C */
        volatile u32 EFPETIMER;      /**< 20 */
        volatile u32 SMWOP0;         /**< 24 */
        volatile u32 SMWOP1;         /**< 28 */
        volatile u32 SMWOP2;         /**< 2C */
        volatile u32 SMWOP3;         /**< 30 */
        volatile u32 SMPWHVOP0;      /**< 34 */
        volatile u32 SMPWHVOP1;      /**< 38 */
        volatile u32 SMEWHVOP0;      /**< 3C */
        volatile u32 SMEWHVOP1;      /**< 40 */
        volatile u32 SMWINFO0;       /**< 44 */
        volatile u32 RESERVED0;      /**< 48 */
        volatile u32 EFORDN0STAT;    /**< 4C */
        volatile u32 EFORDN1STAT;    /**< 50 */
        volatile u32 EFORDN2STAT;    /**< 54 */
        volatile u32 EFORDN3STAT;    /**< 58 */
        volatile u32 EFORDN4STAT;    /**< 5C */
        volatile u32 EFORDN5STAT;    /**< 60 */
        volatile u32 EFORDN6STAT;    /**< 64 */
        volatile u32 EFORDN7STAT;    /**< 68 */
        volatile u32 RESERVEDS[8];   /**< 6C 70 74 78 7C 80 84 88 */
        volatile u32 ERRADDR0;       /**< 8C */
        volatile u32 RESERVED1;      /**< 90 */
        volatile u32 EFDIRCTRLEN;    /**< 94 */
        volatile u32 DIRCARDDR;      /**< 98 */
        volatile u32 DIRCWDATAL;     /**< 9C */
        volatile u32 DIRCWDATAH;     /**< A0 */
        volatile u32 DIRCRDATAL;     /**< A4 */
        volatile u32 DIRCRDATAH;     /**< A8 */
        volatile u32 DIRCFPINCTL;    /**< AC */
        volatile u32 TRIMCODE0L;     /**< B0 */
        volatile u32 TRIMCODE0H;     /**< B4 */
        volatile u32 TRIMCODE1L;     /**< B8 */
        volatile u32 TRIMCODE1H;     /**< BC */
        volatile u32 RESERVEDS1[11]; /**< C0--->E8 */
        volatile u32 EFUSEEN;        /**< F0 */
        volatile u32 EFUSECTRL;      /**< F4 */
        volatile u32 EFUSEDATA;      /**< F8 */
        volatile u32 EFUSEVDDQEN;    /**< FC */
    } EFLASH_TypeDef;
#define EFM ((EFLASH_TypeDef *)EFLASH_BASE_ADDR)

#ifdef __MACRO_DEF_EPORT__
/*EPPAR*/
#define EPORT_EPPAR_EPPA0 (0)  /**<  */
#define EPORT_EPPAR_EPPA1 (2)  /**<  */
#define EPORT_EPPAR_EPPA2 (4)  /**<  */
#define EPORT_EPPAR_EPPA3 (6)  /**<  */
#define EPORT_EPPAR_EPPA4 (8)  /**<  */
#define EPORT_EPPAR_EPPA5 (10) /**<  */
#define EPORT_EPPAR_EPPA6 (12) /**<  */
#define EPORT_EPPAR_EPPA7 (14) /**<  */

/*EPDDR*/
#define EPORT_EPDDR_EPDD (0)          /**<  */
#define EPORT_EPDDR_EPDD0 (0x01 << 0) /**<  */
#define EPORT_EPDDR_EPDD1 (0x01 << 1) /**<  */
#define EPORT_EPDDR_EPDD2 (0x01 << 2) /**<  */
#define EPORT_EPDDR_EPDD3 (0x01 << 3) /**<  */
#define EPORT_EPDDR_EPDD4 (0x01 << 4) /**<  */
#define EPORT_EPDDR_EPDD5 (0x01 << 5) /**<  */
#define EPORT_EPDDR_EPDD6 (0x01 << 6) /**<  */
#define EPORT_EPDDR_EPDD7 (0x01 << 7) /**<  */

/*EPIER*/
#define EPORT_EPIER_EPIE (0)          /**<  */
#define EPORT_EPIER_EPIE0 (0x01 << 0) /**<  */
#define EPORT_EPIER_EPIE1 (0x01 << 1) /**<  */
#define EPORT_EPIER_EPIE2 (0x01 << 2) /**<  */
#define EPORT_EPIER_EPIE3 (0x01 << 3) /**<  */
#define EPORT_EPIER_EPIE4 (0x01 << 4) /**<  */
#define EPORT_EPIER_EPIE5 (0x01 << 5) /**<  */
#define EPORT_EPIER_EPIE6 (0x01 << 6) /**<  */
#define EPORT_EPIER_EPIE7 (0x01 << 7) /**<  */

/*EPDR*/
#define EPORT_EPDR_EPD (0)          /**<  */
#define EPORT_EPDR_EPD0 (0x01 << 0) /**<  */
#define EPORT_EPDR_EPD1 (0x01 << 1) /**<  */
#define EPORT_EPDR_EPD2 (0x01 << 2) /**<  */
#define EPORT_EPDR_EPD3 (0x01 << 3) /**<  */
#define EPORT_EPDR_EPD4 (0x01 << 4) /**<  */
#define EPORT_EPDR_EPD5 (0x01 << 5) /**<  */
#define EPORT_EPDR_EPD6 (0x01 << 6) /**<  */
#define EPORT_EPDR_EPD7 (0x01 << 7) /**<  */

/*EPPDR*/
#define EPORT_EPPDR_EPPD (0)          /**<  */
#define EPORT_EPPDR_EPPD0 (0x01 << 0) /**<  */
#define EPORT_EPPDR_EPPD1 (0x01 << 1) /**<  */
#define EPORT_EPPDR_EPPD2 (0x01 << 2) /**<  */
#define EPORT_EPPDR_EPPD3 (0x01 << 3) /**<  */
#define EPORT_EPPDR_EPPD4 (0x01 << 4) /**<  */
#define EPORT_EPPDR_EPPD5 (0x01 << 5) /**<  */
#define EPORT_EPPDR_EPPD6 (0x01 << 6) /**<  */
#define EPORT_EPPDR_EPPD7 (0x01 << 7) /**<  */

/*EPFR*/
#define EPORT_EPFR_EPF (0)          /**<  */
#define EPORT_EPFR_EPF0 (0x01 << 0) /**<  */
#define EPORT_EPFR_EPF1 (0x01 << 1) /**<  */
#define EPORT_EPFR_EPF2 (0x01 << 2) /**<  */
#define EPORT_EPFR_EPF3 (0x01 << 3) /**<  */
#define EPORT_EPFR_EPF4 (0x01 << 4) /**<  */
#define EPORT_EPFR_EPF5 (0x01 << 5) /**<  */
#define EPORT_EPFR_EPF6 (0x01 << 6) /**<  */
#define EPORT_EPFR_EPF7 (0x01 << 7) /**<  */

/*EPPUER*/
#define EPORT_EPPUER_EPPUE (0)          /**<  */
#define EPORT_EPPUER_EPPUE0 (0x01 << 0) /**<  */
#define EPORT_EPPUER_EPPUE1 (0x01 << 1) /**<  */
#define EPORT_EPPUER_EPPUE2 (0x01 << 2) /**<  */
#define EPORT_EPPUER_EPPUE3 (0x01 << 3) /**<  */
#define EPORT_EPPUER_EPPUE4 (0x01 << 4) /**<  */
#define EPORT_EPPUER_EPPUE5 (0x01 << 5) /**<  */
#define EPORT_EPPUER_EPPUE6 (0x01 << 6) /**<  */
#define EPORT_EPPUER_EPPUE7 (0x01 << 7) /**<  */

/*EPLPR*/
#define EPORT_EPLPR_EPLP (0)          /**<  */
#define EPORT_EPLPR_EPLP0 (0x01 << 0) /**<  */
#define EPORT_EPLPR_EPLP1 (0x01 << 1) /**<  */
#define EPORT_EPLPR_EPLP2 (0x01 << 2) /**<  */
#define EPORT_EPLPR_EPLP3 (0x01 << 3) /**<  */
#define EPORT_EPLPR_EPLP4 (0x01 << 4) /**<  */
#define EPORT_EPLPR_EPLP5 (0x01 << 5) /**<  */
#define EPORT_EPLPR_EPLP6 (0x01 << 6) /**<  */
#define EPORT_EPLPR_EPLP7 (0x01 << 7) /**<  */

/*EPODER*/
#define EPORT_EPODER_EPODE (0)          /**<  */
#define EPORT_EPODER_EPODE0 (0x01 << 0) /**<  */
#define EPORT_EPODER_EPODE1 (0x01 << 1) /**<  */
#define EPORT_EPODER_EPODE2 (0x01 << 2) /**<  */
#define EPORT_EPODER_EPODE3 (0x01 << 3) /**<  */
#define EPORT_EPODER_EPODE4 (0x01 << 4) /**<  */
#define EPORT_EPODER_EPODE5 (0x01 << 5) /**<  */
#define EPORT_EPODER_EPODE6 (0x01 << 6) /**<  */
#define EPORT_EPODER_EPODE7 (0x01 << 7) /**<  */

    typedef enum
    {
        /*EPORT*/
        EPORT_PIN0 = _bit(0),
        EPORT_PIN1 = _bit(1),
        EPORT_PIN2 = _bit(2),
        EPORT_PIN3 = _bit(3),
        EPORT_PIN4 = _bit(4),
        EPORT_PIN5 = _bit(5),
        EPORT_PIN6 = _bit(6),
        EPORT_PIN7 = _bit(7),

        /*EPORT1*/
        EPORT_PIN8 = _bit(0),
        EPORT_PIN9 = _bit(1),
        EPORT_PIN10 = _bit(2),
        EPORT_PIN11 = _bit(3),
        EPORT_PIN12 = _bit(4),
        EPORT_PIN13 = _bit(5),
        EPORT_PIN14 = _bit(6),
        EPORT_PIN15 = _bit(7),

        /*EPORT2*/
        EPORT_PIN16 = _bit(0),
        EPORT_PIN17 = _bit(1),
        EPORT_PIN18 = _bit(2),
        EPORT_PIN19 = _bit(3),
        EPORT_PIN20 = _bit(4),
        EPORT_PIN21 = _bit(5),
        EPORT_PIN22 = _bit(6),
        EPORT_PIN23 = _bit(7),

        /*EPORT3*/
        EPORT_PIN24 = _bit(0),
        EPORT_PIN25 = _bit(1),
        EPORT_PIN26 = _bit(2),
        EPORT_PIN27 = _bit(3),
        EPORT_PIN28 = _bit(4),
        EPORT_PIN29 = _bit(5),
        EPORT_PIN30 = _bit(6),
        EPORT_PIN31 = _bit(7),

        /*EPORT4*/
        EPORT_PIN32 = _bit(0),
        EPORT_PIN33 = _bit(1),
        EPORT_PIN34 = _bit(2),
        EPORT_PIN35 = _bit(3),
        EPORT_PIN36 = _bit(4),
        EPORT_PIN37 = _bit(5),
        EPORT_PIN38 = _bit(6),
        EPORT_PIN39 = _bit(7),

        /*EPORT5*/
        EPORT_PIN40 = _bit(0),
        EPORT_PIN41 = _bit(1),
        EPORT_PIN42 = _bit(2),
        EPORT_PIN43 = _bit(3),
        EPORT_PIN44 = _bit(4),
        EPORT_PIN45 = _bit(5),
        EPORT_PIN46 = _bit(6),
        EPORT_PIN47 = _bit(7),

        /*EPORT6*/
        EPORT_PIN48 = _bit(0),
        EPORT_PIN49 = _bit(1),
        EPORT_PIN50 = _bit(2),
        EPORT_PIN51 = _bit(3),
        EPORT_PIN52 = _bit(4),
        EPORT_PIN53 = _bit(5),
        EPORT_PIN54 = _bit(6),
        EPORT_PIN55 = _bit(7),

        /*EPORT7*/
        EPORT_PIN56 = _bit(0),
        EPORT_PIN57 = _bit(1),
        EPORT_PIN58 = _bit(2),
        EPORT_PIN59 = _bit(3),
        EPORT_PIN60 = _bit(4),
        EPORT_PIN61 = _bit(5),
        EPORT_PIN62 = _bit(6),
        EPORT_PIN63 = _bit(7),

        /*EPORT8*/
        EPORT_PIN64 = _bit(0),
        EPORT_PIN65 = _bit(1),
        EPORT_PIN66 = _bit(2),
        EPORT_PIN67 = _bit(3),
        EPORT_PIN68 = _bit(4),
        EPORT_PIN69 = _bit(5),
        EPORT_PIN70 = _bit(6),
        EPORT_PIN71 = _bit(7),

        /*EPORT9*/
        EPORT_PIN72 = _bit(0),
        EPORT_PIN73 = _bit(1),
        EPORT_PIN74 = _bit(2),
        EPORT_PIN75 = _bit(3),
        EPORT_PIN76 = _bit(4),
        EPORT_PIN77 = _bit(5),
        EPORT_PIN78 = _bit(6),
        EPORT_PIN79 = _bit(7),

        /*EPORT10*/
        EPORT_PIN80 = _bit(0),
        EPORT_PIN81 = _bit(1),
        EPORT_PIN82 = _bit(2),
        EPORT_PIN83 = _bit(3),
        EPORT_PIN84 = _bit(4),
        EPORT_PIN85 = _bit(5),
        EPORT_PIN86 = _bit(6),
        EPORT_PIN87 = _bit(7),

        /*EPORT11*/
        EPORT_PIN88 = _bit(0),
        EPORT_PIN89 = _bit(1),
        EPORT_PIN90 = _bit(2),
        EPORT_PIN91 = _bit(3),
        EPORT_PIN92 = _bit(4),
        EPORT_PIN93 = _bit(5),
        EPORT_PIN94 = _bit(6),
        EPORT_PIN95 = _bit(7),

        /*EPORT12*/
        EPORT_PIN96 = _bit(0),
        EPORT_PIN97 = _bit(1),
        EPORT_PIN98 = _bit(2),
        EPORT_PIN99 = _bit(3),
        EPORT_PIN100 = _bit(4),
        EPORT_PIN101 = _bit(5),
        EPORT_PIN102 = _bit(6),
        EPORT_PIN103 = _bit(7),

        /*EPORT13*/
        EPORT_PIN104 = _bit(0),
        EPORT_PIN105 = _bit(1),
        EPORT_PIN106 = _bit(2),
        EPORT_PIN107 = _bit(3),
        EPORT_PIN108 = _bit(4),
        EPORT_PIN109 = _bit(5),
        EPORT_PIN110 = _bit(6),
        EPORT_PIN111 = _bit(7),

        /*EPORT14*/
        EPORT_PIN112 = _bit(0),
        EPORT_PIN113 = _bit(1),
        EPORT_PIN114 = _bit(2),
        EPORT_PIN115 = _bit(3),
        EPORT_PIN116 = _bit(4),
        EPORT_PIN117 = _bit(5),
        EPORT_PIN118 = _bit(6),
        EPORT_PIN119 = _bit(7),

        /*EPORT15*/
        EPORT_PIN120 = _bit(0),
        EPORT_PIN121 = _bit(1),
        EPORT_PIN122 = _bit(2),
        EPORT_PIN123 = _bit(3),
        EPORT_PIN124 = _bit(4),
        EPORT_PIN125 = _bit(5),
        EPORT_PIN126 = _bit(6),
        EPORT_PIN127 = _bit(7),

        /*EPORT16*/
        EPORT_PIN128 = _bit(0),
        EPORT_PIN129 = _bit(1),
        EPORT_PIN130 = _bit(2),
        EPORT_PIN131 = _bit(3),
        EPORT_PIN132 = _bit(4),
        EPORT_PIN133 = _bit(5),
        EPORT_PIN134 = _bit(6),
        EPORT_PIN135 = _bit(7),

    } EPORT_PinxTypeDef;

#endif

    typedef struct
    {
        union
        {
            volatile u16 EPPAR; /**< 0x00 */
            struct
            {
                volatile u8 EPPAR_L;
                volatile u8 EPPAR_H;
            } EPPARHL;
        };

        volatile u8 EPIER; /**< 0x02 */
        volatile u8 EPDDR; /**< 0x03 */
        volatile u8 EPPDR; /**< 0x04 */
        // volatile u8 EPDR;	 /**< 0x05 */
        union
        {
            volatile UINT8 EPDR;
            struct
            {
                volatile UINT8 int0 : 1;
                volatile UINT8 int1 : 1;
                volatile UINT8 int2 : 1;
                volatile UINT8 int3 : 1;
                volatile UINT8 int4 : 1;
                volatile UINT8 int5 : 1;
                volatile UINT8 int6 : 1;
                volatile UINT8 int7 : 1;
            } U8EPDR;
        };
        volatile u8 EPPUER; /**< 0x06 */
        volatile u8 EPFR;   /**< 0x07 */
        volatile u8 EPODER; /**< 0x08 */
        volatile u8 EPLPR;  /**< 0x09 */
    } EPORT_TypeDef;
#define EPORT ((EPORT_TypeDef *)EPORT_BASE_ADDR)
#define EPORT1 ((EPORT_TypeDef *)EPORT1_BASE_ADDR)
#define EPORT2 ((EPORT_TypeDef *)EPORT2_BASE_ADDR)
#define EPORT3 ((EPORT_TypeDef *)EPORT3_BASE_ADDR)
#define EPORT4 ((EPORT_TypeDef *)EPORT4_BASE_ADDR)
#define EPORT5 ((EPORT_TypeDef *)EPORT5_BASE_ADDR)
#define EPORT6 ((EPORT_TypeDef *)EPORT6_BASE_ADDR)
#define EPORT7 ((EPORT_TypeDef *)EPORT7_BASE_ADDR)
#define EPORT8 ((EPORT_TypeDef *)EPORT8_BASE_ADDR)
#define EPORT9 ((EPORT_TypeDef *)EPORT9_BASE_ADDR)
#define EPORT10 ((EPORT_TypeDef *)EPORT10_BASE_ADDR)
#define EPORT11 ((EPORT_TypeDef *)EPORT11_BASE_ADDR)
#define EPORT12 ((EPORT_TypeDef *)EPORT12_BASE_ADDR)
#define EPORT13 ((EPORT_TypeDef *)EPORT13_BASE_ADDR)
#define EPORT14 ((EPORT_TypeDef *)EPORT14_BASE_ADDR)
#define EPORT15 ((EPORT_TypeDef *)EPORT15_BASE_ADDR)
#define EPORT16 ((EPORT_TypeDef *)EPORT16_BASE_ADDR)

#ifdef __MACRO_DEF_I2C__
/*I2CC*/
#define I2C_EN (1 << 0)                          /**< I2C模块使能                   */
#define I2C_DIS (0xFE)                           /**< I2C模块禁止                   */
#define I2C_EN_IT (1 << 1)                       /**< I2C 中断使能 */
#define I2C_DIS_IT (0xFD)                        /**< I2C 中断禁止*/
#define I2C_MASTER (1 << 2)                      /**< I2C 主模式*/
#define I2C_SLAVE (0xFB)                         /**< I2C 从模式*/
#define I2C_EN_ACK (1 << 3)                      /**< I2C 使能应答*/
#define I2C_DIS_ACK (0xF7)                       /**< I2C 禁止应答*/
#define I2C_EN_REPEAT_START (1 << 4)             /**< I2C 使能重复起始信号*/
#define I2C_DIS_REPEAT_START (0xEF)              /**< I2C 禁止重复起始信号*/
#define I2C_EN_ADDRESS_MATCH_IT (1 << 5)         /**< I2C 使能地址匹配中断*/
#define I2C_DIS_ADDRESS_MATCH_IT (0xDF)          /**< I2C 禁止地址匹配中断*/
#define I2C_EN_HIGH_SPEED_MODE (1 << 6)          /**< I2C 使能高速模式*/
#define I2C_DIS_HIGH_SPEED_MODE (0xBF)           /**< I2C 禁止高速模式 */
#define I2C_EN_SLAVE_HIGH_SPEED_MODE_IT (1 << 7) /**< I2C 使能高速模式中断 */
#define I2C_DIS_SLAVE_HIGH_SPEED_MODE_IT (0x7F)  /**< I2C 禁止高速模式中断 */
/*I2CP*/
#define I2C_CLOCK_MODE_TEST (1 << 6)                 /**< I2C  时钟测试模式*/
#define I2C_CLOCK_MODE_NORMAL (~I2C_CLOCK_MODE_TEST) /**< I2C  时钟正常模式 */
/*I2CS*/
#define I2C_FLAG_TF (1 << 0)    /**< I2C 标志TF */
#define I2C_FLAG_RC (1 << 1)    /**< I2C 标志RC*/
#define I2C_FLAG_AASLV (1 << 2) /**< I2C 标志AASLV */
#define I2C_FLAG_BUSY (1 << 3)  /**< I2C 标志BUSY*/
#define I2C_FLAG_ARBL (1 << 4)  /**< I2C 标志*ARBL*/
#define I2C_FLAG_RXTX (1 << 5)  /**< I2C 标志RXTX */
#define I2C_FLAG_DACK (1 << 6)  /**< I2C 标志DACK*/
#define I2C_FLAG_AACK (1 << 7)  /**< I2C 标志DACK*/
#define I2C_FLAG_RX_MASK (0)    /**< I2C 标志RX掩码*/
#define I2C_FLAG_TX_MASK (0x20) /**< I2C 标志TX掩码*/

/*I2CSHIR*/
#define I2C_FLAG_SLAVE_HIGH_SPEED (1 << 0) /**<           */
/*I2CSHT*/
#define I2C_EN_SDA_FILTER (1 << 6) /**<          */
#define I2C_EN_SCL_FILTER (1 << 7) /**<          */
/**/
#define I2C_SLAVE_HIGH_SPEED (1 << 0) /**<           */
/*I2PCR*/
#define I2C_PIN_SCL (0) /**< I2C管脚索引 SCL     */
#define I2C_PIN_SDA (1) /**< I2C管脚索引 SDA     */
#define I2C_PA_SHIFT_MASK (6)
#define I2C_WOM_SHIFT_MASK (4)
#define I2C_PD_SHIFT_MASK (2)
#define I2C_PU_SHIFT_MASK (0)
#define I2C_PIN_SDA_GPIO (0x80)                                /**< SDA管脚步用于GPIO   */
#define I2C_PIN_SDA_PRIMARY_FUN (~(I2C_PIN_SDA_GPIO))          /**< SDA管脚步用于主功能 */
#define I2C_PIN_SCL_GPIO (0x40)                                /**< SCL管脚步用于GPIO   */
#define I2C_PIN_SCL_PRIMARY_FUN (~(I2C_PIN_SCL_GPIO))          /**< SCL管脚步用于主功能 */
#define I2C_PIN_SDA_OPEN_DRAIN_MODE (0x20)                     /**< I2C管脚SDA 开漏输出 */
#define I2C_PIN_SDA_CMOS_MODE (~(I2C_PIN_SDA_OPEN_DRAIN_MODE)) /**< I2C管脚SDA CMOS输出 */
#define I2C_PIN_SCL_OPEN_DRAIN_MODE (0x10)                     /**< I2C管脚SCL 开漏输出 */
#define I2C_PIN_SCL_CMOS_MODE (~(I2C_PIN_SCL_OPEN_DRAIN_MODE)) /**< I2C管脚SDA CMOS输出 */
#define I2C_PIN_SDA_EN_PULLDOWN (0x04)                         /**< I2C管脚SDA 上拉使能 */
#define I2C_PIN_SDA_DIS_PULLDOWN (~(I2C_PIN_SDA_EN_PULLDOWN))  /**< I2C管脚SDA 上拉禁止 */
#define I2C_PIN_SCL_EN_PULLDOWN (0x02)                         /**< I2C管脚SCL 上拉使能 */
#define I2C_PIN_SCL_DIS_PULLDOWN (~(I2C_PIN_SCL_EN_PULLDOWN))  /**< I2C管脚SDA 上拉禁止 */
#define I2C_PIN_SDA_EN_PULLUP (0x02)                           /**< I2C管脚SDA 下拉使能 */
#define I2C_PIN_SDA_DIS_PULLUP (~(I2C_PIN_SDA_EN_PULLUP))      /**< I2C管脚SDA 下拉禁能 */
#define I2C_PIN_SCL_EN_PULLUP (0x01)                           /**< I2C管脚SCL 下拉使能 */
#define I2C_PIN_SCL_DIS_PULLUP (~(I2C_PIN_SCL_EN_PULLUP))      /**< I2C管脚SCL 下拉禁能 */
/*I2CPDR*/
#define I2C_PIN_SCL_DIR_GPIO_OUT (1 << 0)                     /**< I2C SCL 方向 输出    */
#define I2C_PIN_SCL_DIR_GPIO_IN (~(I2C_PIN_SCL_DIR_GPIO_OUT)) /**< I2C SCL 方向 输入    */
#define I2C_PIN_SDA_DIR_GPIO_OUT (1 << 1)                     /**< I2C SDA 方向 输出    */
#define I2C_PIN_SDA_DIR_GPIO_IN (~(I2C_PIN_SDA_DIR_GPIO_OUT)) /**< I2C SDA 方向 输入    */
#endif
    typedef struct
    {
        // volatile u8 SARH;			 /**< 0--1*/
        // volatile u8 SARL;			 /**< 0--1*/
        volatile u16 SAR;      /**< 0--1*/
        volatile u8 CCR;       /**< 2*/
        volatile u8 PR;        /**< 3*/
        volatile u8 SR;        /**< 4*/
        volatile u8 DR;        /**< 5*/
        volatile u8 SHTR;      /**< 6*/
        volatile u8 SHIR;      /**< 7*/
        volatile u8 PCR;       /**< 8*/
        volatile u8 PDR;       /**< 9*/
        volatile u8 DDR;       /**< a*/
        volatile u8 FCTR;      /**< b*/
        volatile u8 NSFTVR_10; /**< c*/
        volatile u8 NSFTVR_50; /**< d*/
    } I2C_TypeDef;
#define I2C1 ((I2C_TypeDef *)I2C1_BASE_ADDR)
#define I2C2 ((I2C_TypeDef *)I2C2_BASE_ADDR)
#define I2C3 ((I2C_TypeDef *)I2C3_BASE_ADDR)

    typedef struct
    {
        volatile u32 CR;     /**< 0*/
        volatile u32 SR;     /**< 4*/
        volatile u32 RIS;    /**< 8*/
        volatile u32 IER;    /**< C*/
        volatile u32 MIS;    /**< 10*/
        volatile u32 ICR;    /**< 14*/
        volatile u32 ESCR;   /**< 18*/
        volatile u32 ESUR;   /**< 1C*/
        volatile u32 CWSTRT; /**< 20*/
        volatile u32 CWSIZE; /**< 24*/
        volatile u32 DR;     /**< 28*/
        volatile u32 PROC;   /**< 2C*/
        volatile u32 DMAEN;  /**< 30*/

        volatile u32 IDMA_DST;                /**< 34*/
        volatile u32 IDMA_CFG;                /**< 38*/
        volatile u32 PIC_SIZE;                /**< 3C*/
        volatile u32 GRAY_SIZE;               /**< 40*/
        volatile u32 GRAY_COORD;              /**< 44*/
        volatile u32 SYNC_POSI;               /**< 48*/
        volatile u32 EMB_SYNC_ERR_STAT;       /**< 4C*/
        volatile u32 SOFT_CLR;                /**< 50*/
        volatile u32 EMB_SYNC_HEAD_HIGH;      /**< 54*/
        volatile u32 EMB_SYNC_WORD_HIGH;      /**< 58*/
        volatile u32 EMB_SYNC_WORD_HIGH_MASK; /**< 5C*/
        volatile u32 RGB2YUV_COEFF;           /**< 60*/
        volatile u32 SNSR_CTRL;               /**< 64*/
        volatile u32 EXTRA_DATA_FORM;         /**< 68*/
        volatile u32 HTIMING;                 /**< 6C*/
        volatile u32 THROTTLE;                /**< 70*/
        volatile u32 IDMA_CURR_ADDR;          /**< 74*/
        volatile u32 FIFOTH;                  /**< 78*/
    } DCMI_TypeDef;
#define DCMI ((DCMI_TypeDef *)DCMI_BASE_ADDR)

    typedef struct
    {
        volatile u32 CR;          /**< DMA2D Control Register, 												Address offset: 0x00 */
        volatile u32 ISR;         /**< DMA2D Interrupt Status Register,								Address offset: 0x04 */
        volatile u32 IFCR;        /**< DMA2D Interrupt Flag Clear Register,						Address offset: 0x08 */
        volatile u32 FGMAR;       /**< DMA2D Foreground Memory Address Register, 			Address offset: 0x0C */
        volatile u32 FGOR;        /**< DMA2D Foreground Offset Register, 							Address offset: 0x10 */
        volatile u32 BGMAR;       /**< DMA2D Background Memory Address Register, 			Address offset: 0x14 */
        volatile u32 BGOR;        /**< DMA2D Background Offset Register, 							Address offset: 0x18 */
        volatile u32 FGPFCCR;     /**< DMA2D Foreground PFC Control Register,					Address offset: 0x1C */
        volatile u32 FGCOLR;      /**< DMA2D Foreground Color Register,								Address offset: 0x20 */
        volatile u32 BGPFCCR;     /**< DMA2D Background PFC Control Register,					Address offset: 0x24 */
        volatile u32 BGCOLR;      /**< DMA2D Background Color Register,								Address offset: 0x28 */
        volatile u32 FGCMAR;      /**< DMA2D Foreground CLUT Memory Address Register,	Address offset: 0x2C */
        volatile u32 BGCMAR;      /**< DMA2D Background CLUT Memory Address Register,	Address offset: 0x30 */
        volatile u32 OPFCCR;      /**< DMA2D Output PFC Control Register,							Address offset: 0x34 */
        volatile u32 OCOLR;       /**< DMA2D Output Color Register,										Address offset: 0x38 */
        volatile u32 OMAR;        /**< DMA2D Output Memory Address Register, 					Address offset: 0x3C */
        volatile u32 OOR;         /**< DMA2D Output Offset Register, 									Address offset: 0x40 */
        volatile u32 NLR;         /**< DMA2D Number of Line Register,									Address offset: 0x44 */
        volatile u32 LWR;         /**< DMA2D Line Watermark Register,									Address offset: 0x48 */
        volatile u32 AMTCR;       /**< DMA2D AHB Master Timer Configuration Register,	Address offset: 0x4C */
        u32 RESERVED[236];        /**< Reserved, 0x50-0x3FF */
        volatile u32 FGCLUT[256]; /**< DMA2D Foreground CLUT,													Address
                                     offset:400-7FF */
        volatile u32 BGCLUT[256]; /**< DMA2D Background CLUT,													Address
                                     offset:800-BFF */
    } DMA2D_TypeDef;
#define DMA2D ((DMA2D_TypeDef *)DMA2D_BASE_ADDR)

    typedef struct
    {

        volatile u32 CR;    /**< 0--1*/
        volatile u32 SR;    /**< 2*/
        volatile u32 ESR;   /**< 3*/
        volatile u32 CDR;   /**< 4*/
        volatile u32 TXDR;  /**< 5*/
        volatile u32 RXDR;  /**< 6*/
        volatile u32 DMACR; /**< 7*/
        volatile u32 ITCR;  /**< 8*/
        volatile u32 ITIR;  /**< 9*/
        volatile u32 ITOR;  /**< a*/
    } I2S_TypeDef;
#define I2S ((I2S_TypeDef *)I2S_BASE_ADDR)

#ifdef __MACRO_DEF_IO_CONTROL__
/*SPICR*/
#define IOCTRL_SPICR_DS1_DS0 (0x03 << 0)   /**<  */
#define IOCTRL_SPICR_DS0 (0x01 << 0)       /**<  */
#define IOCTRL_SPICR_DS1 (0x01 << 1)       /**<  */
#define IOCTRL_SPICR_SR (0x01 << 2)        /**<  */
#define IOCTRL_SPICR_IS (0x01 << 3)        /**<  */
#define IOCTRL_SPICR_SPI1_PS (8)           /**<  */
#define IOCTRL_SPICR_SS1_PS (0x01 << 8)    /**<  */
#define IOCTRL_SPICR_SCK1_PS (0x01 << 9)   /**<  */
#define IOCTRL_SPICR_MISO1_PS (0x01 << 10) /**<  */
#define IOCTRL_SPICR_MOSI1_PS (0x01 << 11) /**<  */
#define IOCTRL_SPICR_SPI1_IE (12)          /**<  */
#define IOCTRL_SPICR_SS1_IE (0x01 << 12)   /**<  */
#define IOCTRL_SPICR_SCK1_IE (0x01 << 13)  /**<  */
#define IOCTRL_SPICR_MISO1_IE (0x01 << 14) /**<  */
#define IOCTRL_SPICR_MOSI1_IE (0x01 << 15) /**<  */
#define IOCTRL_SPICR_SPI2_PS (16)          /**<  */
#define IOCTRL_SPICR_SS2_PS (0x01 << 16)   /**<  */
#define IOCTRL_SPICR_SCK2_PS (0x01 << 17)  /**<  */
#define IOCTRL_SPICR_MISO2_PS (0x01 << 18) /**<  */
#define IOCTRL_SPICR_MOSI2_PS (0x01 << 19) /**<  */
#define IOCTRL_SPICR_SPI2_IE (20)          /**<  */
#define IOCTRL_SPICR_SS2_IE (0x01 << 20)   /**<  */
#define IOCTRL_SPICR_SCK2_IE (0x01 << 21)  /**<  */
#define IOCTRL_SPICR_MISO2_IE (0x01 << 22) /**<  */
#define IOCTRL_SPICR_MOSI2_IE (0x01 << 23) /**<  */
#define IOCTRL_SPICR_SPI3_PS (24)          /**<  */
#define IOCTRL_SPICR_SS3_PS (0x01 << 24)   /**<  */
#define IOCTRL_SPICR_SCK3_PS (0x01 << 25)  /**<  */
#define IOCTRL_SPICR_MISO3_PS (0x01 << 26) /**<  */
#define IOCTRL_SPICR_MOSI3_PS (0x01 << 27) /**<  */
#define IOCTRL_SPICR_SPI3_IE (28)          /**<  */
#define IOCTRL_SPICR_SS3_IE (0x01 << 28)   /**<  */
#define IOCTRL_SPICR_SCK3_IE (0x01 << 29)  /**<  */
#define IOCTRL_SPICR_MISO3_IE (0x01 << 30) /**<  */
#define IOCTRL_SPICR_MOSI3_IE (0x01 << 31) /**<  */

/*USICR*/
#define IOCTRL_USICR_DS1_DS0 (0x03 << 0)       /**<  */
#define IOCTRL_USICR_DS0 (0x01 << 0)           /**<  */
#define IOCTRL_USICR_DS1 (0x01 << 1)           /**<  */
#define IOCTRL_USICR_SR (0x01 << 2)            /**<  */
#define IOCTRL_USICR_IS (0x01 << 3)            /**<  */
#define IOCTRL_USICR_USI2_DREN (0x01 << 6)     /**<  */
#define IOCTRL_USICR_USI3_DREN (0x01 << 7)     /**<  */
#define IOCTRL_USICR_USI1_PS (8)               /**<  */
#define IOCTRL_USICR_ISORST1_PS (0x01 << 8)    /**<  */
#define IOCTRL_USICR_ISODAT1_PS (0x01 << 9)    /**<  */
#define IOCTRL_USICR_ISOCLK1_PS (0x01 << 10)   /**<  */
#define IOCTRL_USICR_USI2_SWAP (16)            /**<  */
#define IOCTRL_USICR_ISORST2_SWAP (0x01 << 16) /**<  */
#define IOCTRL_USICR_ISODAT2_SWAP (0x01 << 17) /**<  */
#define IOCTRL_USICR_ISOCLK2_SWAP (0x01 << 18) /**<  */
#define IOCTRL_USICR_USI2_DIEN (20)            /**<  */
#define IOCTRL_USICR_ISORST2_DIEN (0x01 << 20) /**<  */
#define IOCTRL_USICR_ISODAT2_DIEN (0x01 << 21) /**<  */
#define IOCTRL_USICR_ISOCLK2_DIEN (0x01 << 22) /**<  */
#define IOCTRL_USICR_USI3_PUEN (24)            /**<  */
#define IOCTRL_USICR_ISORST3_PUEN (0x01 << 24) /**<  */
#define IOCTRL_USICR_ISODAT3_PUEN (0x01 << 25) /**<  */
#define IOCTRL_USICR_ISOCLK3_PUEN (0x01 << 26) /**<  */
#define IOCTRL_USICR_USI3_DIEN (28)            /**<  */
#define IOCTRL_USICR_ISORST3_DIEN (0x01 << 28) /**<  */
#define IOCTRL_USICR_ISODAT3_DIEN (0x01 << 29) /**<  */
#define IOCTRL_USICR_ISOCLK3_DIEN (0x01 << 30) /**<  */

/*I2CCR*/
#define IOCTRL_I2CCR_DS1_DS0 (0x03 << 0)  /**<  */
#define IOCTRL_I2CCR_DS0 (0x01 << 0)      /**<  */
#define IOCTRL_I2CCR_DS1 (0x01 << 1)      /**<  */
#define IOCTRL_I2CCR_SR (0x01 << 2)       /**<  */
#define IOCTRL_I2CCR_IS (0x01 << 3)       /**<  */
#define IOCTRL_I2CCR_I2C1_PS (8)          /**<  */
#define IOCTRL_I2CCR_SCL1_PS (0x01 << 8)  /**<  */
#define IOCTRL_I2CCR_SDA1_PS (0x01 << 9)  /**<  */
#define IOCTRL_I2CCR_I2C1_IE (12)         /**<  */
#define IOCTRL_I2CCR_SCL1_IE (0x01 << 12) /**<  */
#define IOCTRL_I2CCR_SDA1_IE (0x01 << 13) /**<  */
#define IOCTRL_I2CCR_I2C2_PS (16)         /**<  */
#define IOCTRL_I2CCR_SCL2_PS (0x01 << 16) /**<  */
#define IOCTRL_I2CCR_SDA2_PS (0x01 << 17) /**<  */
#define IOCTRL_I2CCR_I2C2_IE (20)         /**<  */
#define IOCTRL_I2CCR_SCL2_IE (0x01 << 20) /**<  */
#define IOCTRL_I2CCR_SDA2_IE (0x01 << 21) /**<  */
#define IOCTRL_I2CCR_I2C3_PS (24)         /**<  */
#define IOCTRL_I2CCR_SCL3_PS (0x01 << 24) /**<  */
#define IOCTRL_I2CCR_SDA3_PS (0x01 << 25) /**<  */
#define IOCTRL_I2CCR_I2C3_IE (28)         /**<  */
#define IOCTRL_I2CCR_SCL3_IE (0x01 << 28) /**<  */
#define IOCTRL_I2CCR_SDA3_IE (0x01 << 29) /**<  */

/*UARTCR*/
#define IOCTRL_UARTCR_DS1_DS0 (0x03 << 0)   /**<  */
#define IOCTRL_UARTCR_DS0 (0x01 << 0)       /**<  */
#define IOCTRL_UARTCR_DS1 (0x01 << 1)       /**<  */
#define IOCTRL_UARTCR_SR (0x01 << 2)        /**<  */
#define IOCTRL_UARTCR_IS (0x01 << 3)        /**<  */
#define IOCTRL_UARTCR_UART1_PS (8)          /**<  */
#define IOCTRL_UARTCR_TXD1_PS (0x01 << 8)   /**<  */
#define IOCTRL_UARTCR_RXD1_PS (0x01 << 9)   /**<  */
#define IOCTRL_UARTCR_CTS1_PUE (0x01 << 11) /**<  */
//#define IOCTRL_UARTCR_UART3_PS                                (12)        /**<  */
//#define IOCTRL_UARTCR_TXD3_PS                                 (0x01<<12)  /**<  */
//#define IOCTRL_UARTCR_RXD3_PS                                 (0x01<<13)  /**<  */
//#define IOCTRL_UARTCR_CTS3_PUE                                (0x01<<15)  /**<  */
#define IOCTRL_UARTCR_UART2_PS (16)         /**<  */
#define IOCTRL_UARTCR_TXD2_PS (0x01 << 16)  /**<  */
#define IOCTRL_UARTCR_RXD2_PS (0x01 << 17)  /**<  */
#define IOCTRL_UARTCR_CTS2_PUE (0x01 << 19) /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_MASK                          (0x3F<<24)  /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP                               (24)        /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_BIT0                          (0x01<<24)  /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_BIT1                          (0x01<<25)  /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_BIT2                          (0x01<<26)  /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_BIT3                          (0x01<<27)  /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_BIT4                          (0x01<<28)  /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_BIT5                          (0x01<<29)  /**<  */
//#define IOCTRL_UARTCR_GINT_SWAP_LOAD_EN                       (0x01<<31)  /**<  */

/*GINTLCR*/
#define IOCTRL_GINTLCR_DS1_DS0 (0x03 << 0)   /**<  */
#define IOCTRL_GINTLCR_DS0 (0x01 << 0)       /**<  */
#define IOCTRL_GINTLCR_DS1 (0x01 << 1)       /**<  */
#define IOCTRL_GINTLCR_SR (0x01 << 2)        /**<  */
#define IOCTRL_GINTLCR_IS (0x01 << 3)        /**<  */
#define IOCTRL_GINTLCR_IE (16)               /**<  */
#define IOCTRL_GINTLCR_GINT0_IE (0x01 << 16) /**<  */
#define IOCTRL_GINTLCR_GINT1_IE (0x01 << 17) /**<  */
#define IOCTRL_GINTLCR_GINT2_IE (0x01 << 18) /**<  */
#define IOCTRL_GINTLCR_GINT3_IE (0x01 << 19) /**<  */
#define IOCTRL_GINTLCR_GINT4_IE (0x01 << 20) /**<  */
#define IOCTRL_GINTLCR_GINT5_IE (0x01 << 21) /**<  */
#define IOCTRL_GINTLCR_GINT6_IE (0x01 << 22) /**<  */
#define IOCTRL_GINTLCR_GINT7_IE (0x01 << 23) /**<  */
#define IOCTRL_GINTLCR_PS (24)               /**<  */
#define IOCTRL_GINTLCR_GINT0_PS (0x01 << 24) /**<  */
#define IOCTRL_GINTLCR_GINT1_PS (0x01 << 25) /**<  */
#define IOCTRL_GINTLCR_GINT2_PS (0x01 << 26) /**<  */
#define IOCTRL_GINTLCR_GINT3_PS (0x01 << 27) /**<  */
#define IOCTRL_GINTLCR_GINT4_PS (0x01 << 28) /**<  */
#define IOCTRL_GINTLCR_GINT5_PS (0x01 << 29) /**<  */
#define IOCTRL_GINTLCR_GINT6_PS (0x01 << 30) /**<  */
#define IOCTRL_GINTLCR_GINT7_PS (0x01 << 31) /**<  */

/*GINTHCR*/
#define IOCTRL_GINTHCR_DS1_DS0 (0x03 << 0)    /**<  */
#define IOCTRL_GINTHCR_DS0 (0x01 << 0)        /**<  */
#define IOCTRL_GINTHCR_DS1 (0x01 << 1)        /**<  */
#define IOCTRL_GINTHCR_SR (0x01 << 2)         /**<  */
#define IOCTRL_GINTHCR_IS (0x01 << 3)         /**<  */
#define IOCTRL_GINTHCR_IE (16)                /**<  */
#define IOCTRL_GINTHCR_GINT8_IE (0x01 << 16)  /**<  */
#define IOCTRL_GINTHCR_GINT9_IE (0x01 << 17)  /**<  */
#define IOCTRL_GINTHCR_GINT10_IE (0x01 << 18) /**<  */
#define IOCTRL_GINTHCR_GINT11_IE (0x01 << 19) /**<  */
#define IOCTRL_GINTHCR_GINT12_IE (0x01 << 20) /**<  */
#define IOCTRL_GINTHCR_GINT13_IE (0x01 << 21) /**<  */
#define IOCTRL_GINTHCR_GINT14_IE (0x01 << 22) /**<  */
#define IOCTRL_GINTHCR_GINT15_IE (0x01 << 23) /**<  */
#define IOCTRL_GINTHCR_PS (24)                /**<  */
#define IOCTRL_GINTHCR_GINT8_PS (0x01 << 24)  /**<  */
#define IOCTRL_GINTHCR_GINT9_PS (0x01 << 25)  /**<  */
#define IOCTRL_GINTHCR_GINT10_PS (0x01 << 26) /**<  */
#define IOCTRL_GINTHCR_GINT11_PS (0x01 << 27) /**<  */
#define IOCTRL_GINTHCR_GINT12_PS (0x01 << 28) /**<  */
#define IOCTRL_GINTHCR_GINT13_PS (0x01 << 29) /**<  */
#define IOCTRL_GINTHCR_GINT14_PS (0x01 << 30) /**<  */
#define IOCTRL_GINTHCR_GINT15_PS (0x01 << 31) /**<  */

/*SWAPCR*/
#define IOCTRL_SWAPCR_SWAP_BIT0 (0x01 << 0)
#define IOCTRL_SWAPCR_SWAP_BIT1 (0x01 << 1)
#define IOCTRL_SWAPCR_SWAP_BIT2 (0x01 << 2)
#define IOCTRL_SWAPCR_SWAP_BIT3 (0x01 << 3)
#define IOCTRL_SWAPCR_SWAP_BIT4 (0x01 << 4)
#define IOCTRL_SWAPCR_SWAP_BIT5 (0x01 << 5)
#define IOCTRL_SWAPCR_SWAP_BIT6 (0x01 << 6)
#define IOCTRL_SWAPCR_SWAP_BIT7 (0x01 << 7)
#define IOCTRL_SWAPCR_SWAP_BIT8 (0x01 << 8)
#define IOCTRL_SWAPCR_SWAP_BIT9 (0x01 << 9)
#define IOCTRL_SWAPCR_SWAP_BIT10 (0x01 << 10)
#define IOCTRL_SWAPCR_SWAP_BIT11 (0x01 << 11)
#define IOCTRL_SWAPCR_SWAP_BIT12 (0x01 << 12)
#define IOCTRL_SWAPCR_SWAP_BIT13 (0x01 << 13)
#define IOCTRL_SWAPCR_SWAP_BIT14 (0x01 << 14)
#define IOCTRL_SWAPCR_SWAP_BIT15 (0x01 << 15)
#define IOCTRL_SWAPCR_SWAP_BIT16 (0x01 << 16)
#define IOCTRL_SWAPCR_SWAP_BIT17 (0x01 << 17)
#define IOCTRL_SWAPCR_SWAP_BIT18 (0x01 << 18)
#define IOCTRL_SWAPCR_SWAP_BIT19 (0x01 << 19)
#define IOCTRL_SWAPCR_SWAP_BIT20 (0x01 << 20)
#define IOCTRL_SWAPCR_SWAP_BIT21 (0x01 << 21)
#define IOCTRL_SWAPCR_SWAP_BIT22 (0x01 << 22)
#define IOCTRL_SWAPCR_SWAP_BIT23 (0x01 << 23)

/*SSI1CR*/
#define IOCTRL_SSI1CR_DS1_DS0 (0x03 << 0) /**<  */
#define IOCTRL_SSI1CR_DS0 (0x01 << 0)     /**<  */
#define IOCTRL_SSI1CR_DS1 (0x01 << 1)     /**<  */
#define IOCTRL_SSI1CR_SR (0x01 << 2)      /**<  */
#define IOCTRL_SSI1CR_IS (0x01 << 3)      /**<  */
#define IOCTRL_SSI1CR_PUE (0x01 << 4)     /**<  */
#define IOCTRL_SSI1CR_ODE (0x01 << 5)     /**<  */
#define IOCTRL_SSI1CR_PS (16)             /**<  */
#define IOCTRL_SSI1CR_SS_PS (0x01 << 16)  /**<  */
#define IOCTRL_SSI1CR_SCK_PS (0x01 << 17) /**<  */
#define IOCTRL_SSI1CR_D0_PS (0x01 << 18)  /**<  */
#define IOCTRL_SSI1CR_D1_PS (0x01 << 19)  /**<  */
#define IOCTRL_SSI1CR_D2_PS (0x01 << 20)  /**<  */
#define IOCTRL_SSI1CR_D3_PS (0x01 << 21)  /**<  */
#define IOCTRL_SSI1CR_IE (24)             /**<  */
#define IOCTRL_SSI1CR_SS_IE (0x01 << 24)  /**<  */
#define IOCTRL_SSI1CR_SCK_IE (0x01 << 25) /**<  */
#define IOCTRL_SSI1CR_D0_IE (0x01 << 26)  /**<  */
#define IOCTRL_SSI1CR_D1_IE (0x01 << 27)  /**<  */
#define IOCTRL_SSI1CR_D2_IE (0x01 << 28)  /**<  */
#define IOCTRL_SSI1CR_D3_IE (0x01 << 29)  /**<  */

/*SSI2CR*/
#define IOCTRL_SSI2CR_DS1_DS0 (0x03 << 0) /**<  */
#define IOCTRL_SSI2CR_DS0 (0x01 << 0)     /**<  */
#define IOCTRL_SSI2CR_DS1 (0x01 << 1)     /**<  */
#define IOCTRL_SSI2CR_SR (0x01 << 2)      /**<  */
#define IOCTRL_SSI2CR_IS (0x01 << 3)      /**<  */
#define IOCTRL_SSI2CR_PUE (0x01 << 4)     /**<  */
#define IOCTRL_SSI2CR_ODE (0x01 << 5)     /**<  */
#define IOCTRL_SSI2CR_PS (16)             /**<  */
#define IOCTRL_SSI2CR_SS_PS (0x01 << 16)  /**<  */
#define IOCTRL_SSI2CR_SCK_PS (0x01 << 17) /**<  */
#define IOCTRL_SSI2CR_D0_PS (0x01 << 18)  /**<  */
#define IOCTRL_SSI2CR_D1_PS (0x01 << 19)  /**<  */
#define IOCTRL_SSI2CR_D2_PS (0x01 << 20)  /**<  */
#define IOCTRL_SSI2CR_D3_PS (0x01 << 21)  /**<  */
#define IOCTRL_SSI2CR_IE (24)             /**<  */
#define IOCTRL_SSI2CR_SS_IE (0x01 << 24)  /**<  */
#define IOCTRL_SSI2CR_SCK_IE (0x01 << 25) /**<  */
#define IOCTRL_SSI2CR_D0_IE (0x01 << 26)  /**<  */
#define IOCTRL_SSI2CR_D1_IE (0x01 << 27)  /**<  */
#define IOCTRL_SSI2CR_D2_IE (0x01 << 28)  /**<  */
#define IOCTRL_SSI2CR_D3_IE (0x01 << 29)  /**<  */

/*GINT31_30CR*/
#define IOCTRL_I2SCR_DS1_DS0 (0x03 << 0)     /**<  */
#define IOCTRL_I2SCR_DS0 (0x01 << 0)         /**<  */
#define IOCTRL_I2SCR_DS1 (0x01 << 1)         /**<  */
#define IOCTRL_I2SCR_SR (0x01 << 2)          /**<  */
#define IOCTRL_I2SCR_IS (0x01 << 3)          /**<  */
#define IOCTRL_I2SCR_PUE (4)                 /**<  */
#define IOCTRL_I2SCR_SD_PUE (0x01 << 4)      /**<  */
#define IOCTRL_I2SCR_LRCK_PUE (0x01 << 5)    /**<  */
#define IOCTRL_I2SCR_SCLK_PUE (0x01 << 6)    /**<  */
#define IOCTRL_I2SCR_AUDRSTN_PUE (0x01 << 7) /**<  */
#define IOCTRL_I2SCR_PS (8)                  /**<  */
#define IOCTRL_I2SCR_SD_PS (0x01 << 8)       /**<  */
#define IOCTRL_I2SCR_LRCK_PS (0x01 << 9)     /**<  */
#define IOCTRL_I2SCR_SCLK_PS (0x01 << 10)    /**<  */
#define IOCTRL_I2SCR_AUDRSTN_PS (0x01 << 11) /**<  */
#define IOCTRL_I2SCR_IE (12)                 /**<  */
#define IOCTRL_I2SCR_SD_IE (0x01 << 12)      /**<  */
#define IOCTRL_I2SCR_LRCK_IE (0x01 << 13)    /**<  */
#define IOCTRL_I2SCR_SCLK_IE (0x01 << 14)    /**<  */
#define IOCTRL_I2SCR_AUDRSTN_IE (0x01 << 15) /**<  */

/* CLCDCR  */
#define IOCTRL_CLCDCR_DS1_DS0 (0x03 << 0) /**<  */
#define IOCTRL_CLCDCR_DS0 (0x01 << 0)     /**<  */
#define IOCTRL_CLCDCR_DS1 (0x01 << 1)     /**<  */
#define IOCTRL_CLCDCR_SR (0x01 << 2)      /**<  */
#define IOCTRL_CLCDCR_IS (0x01 << 3)      /**<  */
#define IOCTRL_CLCDCR_PUE (0x01 << 4)     /**<  */
#define IOCTRL_CLCDCR_ODE (0x01 << 5)     /**<  */
#define IOCTRL_CLCDCR_PS (16)             /**<  */
#define IOCTRL_CLCDCR_IE (24)             /**<  */
/* CLCDDIECR  */
#define IOCTRL_CLCDDIECR_IE (0x01 << 0) /**<  */
/* CLCDDPSCR  */
#define IOCTRL_CLCDDPSCR_PS (0x01 << 0) /**<  */
/*DCMICR*/
#define IOCTRL_DCMICR_DS1_DS0 (0x03 << 0)    /**<  */
#define IOCTRL_DCMICR_DS0 (0x01 << 0)        /**<  */
#define IOCTRL_DCMICR_DS1 (0x01 << 1)        /**<  */
#define IOCTRL_DCMICR_SR (0x01 << 2)         /**<  */
#define IOCTRL_DCMICR_IS (0x01 << 3)         /**<  */
#define IOCTRL_DCMICR_PUE (0x01 << 4)        /**<  */
#define IOCTRL_DCMICR_ODE (0x01 << 5)        /**<  */
#define IOCTRL_DCMICR_PS (16)                /**<  */
#define IOCTRL_DCMICR_BLANK_PS (0x01 << 16)  /**<  */
#define IOCTRL_DCMICR_HSYNC_PS (0x01 << 17)  /**<  */
#define IOCTRL_DCMICR_VSYNC_PS (0x01 << 18)  /**<  */
#define IOCTRL_DCMICR_DATA_PS (0x01 << 19)   /**<  */
#define IOCTRL_DCMICR_ENB_PS (0x01 << 20)    /**<  */
#define IOCTRL_DCMICR_PWDN_PS (0x01 << 21)   /**<  */
#define IOCTRL_DCMICR_RESETN_PS (0x01 << 22) /**<  */
#define IOCTRL_DCMICR_CLKIN_PS (0x01 << 23)  /**<  */
#define IOCTRL_DCMICR_IE (24)                /**<  */
#define IOCTRL_DCMICR_BLANK_IE (0x01 << 24)  /**<  */
#define IOCTRL_DCMICR_HSYNC_IE (0x01 << 25)  /**<  */
#define IOCTRL_DCMICR_VSYNC_IE (0x01 << 26)  /**<  */
#define IOCTRL_DCMICR_DATA_IE (0x01 << 27)   /**<  */
#define IOCTRL_DCMICR_ENB_IE (0x01 << 28)    /**<  */
#define IOCTRL_DCMICR_PWDN_IE (0x01 << 29)   /**<  */
#define IOCTRL_DCMICR_RESETN_IE (0x01 << 30) /**<  */
#define IOCTRL_DCMICR_CLKIN_IE (0x01 << 31)  /**<  */
/*SDRCR*/
#define IOCTRL_SDRCR_DR1_DR0 (0x03 << 0)  /**<  */
#define IOCTRL_SDRCR_DR0 (0x01 << 0)      /**<  */
#define IOCTRL_SDRCR_DR1 (0x01 << 1)      /**<  */
#define IOCTRL_SDRCR_HX (0x01 << 2)       /**<  */
#define IOCTRL_SDRCR_ODE (0x01 << 3)      /**<  */
#define IOCTRL_SDRCR_PD (9)               /**<  */
#define IOCTRL_SDRCR_CK_PD (0x01 << 8)    /**<  */
#define IOCTRL_SDRCR_CKE_PD (0x01 << 9)   /**<  */
#define IOCTRL_SDRCR_SEL_PD (0x01 << 10)  /**<  */
#define IOCTRL_SDRCR_WE_PD (0x01 << 11)   /**<  */
#define IOCTRL_SDRCR_RAS_PD (0x01 << 12)  /**<  */
#define IOCTRL_SDRCR_CAS_PD (0x01 << 13)  /**<  */
#define IOCTRL_SDRCR_BANK_PD (0x01 << 14) /**<  */
#define IOCTRL_SDRCR_DQM_PD (0x01 << 15)  /**<  */
#define IOCTRL_SDRCR_PU (16)              /**<  */
#define IOCTRL_SDRCR_CK_PU (0x01 << 16)   /**<  */
#define IOCTRL_SDRCR_CKE_PU (0x01 << 17)  /**<  */
#define IOCTRL_SDRCR_SEL_PU (0x01 << 18)  /**<  */
#define IOCTRL_SDRCR_WE_PU (0x01 << 19)   /**<  */
#define IOCTRL_SDRCR_RAS_PU (0x01 << 20)  /**<  */
#define IOCTRL_SDRCR_CAS_PU (0x01 << 21)  /**<  */
#define IOCTRL_SDRCR_BANK_PU (0x01 << 22) /**<  */
#define IOCTRL_SDRCR_DQM_PU (0x01 << 23)  /**<  */
#define IOCTRL_SDRCR_IE (24)              /**<  */
#define IOCTRL_SDRCR_CK_IE (0x01 << 24)   /**<  */
#define IOCTRL_SDRCR_CKE_IE (0x01 << 25)  /**<  */
#define IOCTRL_SDRCR_SEL_IE (0x01 << 26)  /**<  */
#define IOCTRL_SDRCR_WE_IE (0x01 << 27)   /**<  */
#define IOCTRL_SDRCR_RAS_IE (0x01 << 28)  /**<  */
#define IOCTRL_SDRCR_CAS_IE (0x01 << 29)  /**<  */
#define IOCTRL_SDRCR_BANK_IE (0x01 << 30) /**<  */
#define IOCTRL_SDRCR_DQM_IE (0x01 << 31)  /**<  */
/*SMCR*/
#define IOCTRL_SMCR_DR1_DR0 (0x03 << 0)  /**<  */
#define IOCTRL_SMCR_DR0 (0x01 << 0)      /**<  */
#define IOCTRL_SMCR_DR1 (0x01 << 1)      /**<  */
#define IOCTRL_SMCR_HX (0x01 << 2)       /**<  */
#define IOCTRL_SMCR_ODE (0x01 << 3)      /**<  */
#define IOCTRL_SMCR_PD (9)               /**<  */
#define IOCTRL_SMCR_SMOE_PD (0x01 << 9)  /**<  */
#define IOCTRL_SMCR_SMWE_PD (0x01 << 10) /**<  */
#define IOCTRL_SMCR_SMBS_PD (0x01 << 11) /**<  */
#define IOCTRL_SMCR_PU (17)              /**<  */
#define IOCTRL_SMCR_SMOE_PU (0x01 << 17) /**<  */
#define IOCTRL_SMCR_SMWE_PU (0x01 << 18) /**<  */
#define IOCTRL_SMCR_SMBS_PU (0x01 << 19) /**<  */
#define IOCTRL_SMCR_IE (25)              /**<  */
#define IOCTRL_SMCR_SMOE_IE (0x01 << 25) /**<  */
#define IOCTRL_SMCR_SMWE_IE (0x01 << 26) /**<  */
#define IOCTRL_SMCR_SMBS_IE (0x01 << 27) /**<  */

/*SDRAMDCR*/
#define IOCTRL_SDRAMDCR_DR1_DR0 (0x03 << 0)       /**<  */
#define IOCTRL_SDRAMDCR_DR0 (0x01 << 0)           /**<  */
#define IOCTRL_SDRAMDCR_DR1 (0x01 << 1)           /**<  */
#define IOCTRL_SDRAMDCR_HX (0x01 << 2)            /**<  */
#define IOCTRL_SDRAMDCR_ODE (0x01 << 3)           /**<  */
#define IOCTRL_SDRAMDCR_PD (8)                    /**<  */
#define IOCTRL_SDRAMDCR_DATA0_7_PD (0x01 << 8)    /**<  */
#define IOCTRL_SDRAMDCR_DATA8_15_PD (0x01 << 9)   /**<  */
#define IOCTRL_SDRAMDCR_DATA16_23_PD (0x01 << 10) /**<  */
#define IOCTRL_SDRAMDCR_DATA24_31_PD (0x01 << 11) /**<  */
#define IOCTRL_SDRAMDCR_PU (16)                   /**<  */
#define IOCTRL_SDRAMDCR_DATA0_7_PU (0x01 << 16)   /**<  */
#define IOCTRL_SDRAMDCR_DATA8_15_PU (0x01 << 17)  /**<  */
#define IOCTRL_SDRAMDCR_DATA16_23_PU (0x01 << 18) /**<  */
#define IOCTRL_SDRAMDCR_DATA24_31_PU (0x01 << 19) /**<  */
#define IOCTRL_SDRAMDCR_IE (24)                   /**<  */
#define IOCTRL_SDRAMDCR_DATA0_7_IE (0x01 << 24)   /**<  */
#define IOCTRL_SDRAMDCR_DATA8_15_IE (0x01 << 25)  /**<  */
#define IOCTRL_SDRAMDCR_DATA16_23_IE (0x01 << 26) /**<  */
#define IOCTRL_SDRAMDCR_DATA24_31_IE (0x01 << 27) /**<  */

#endif
    typedef struct
    {
        volatile u32 SPICR;       /**< 0x00	*/
        volatile u32 USICR;       /**< 0x04	*/
        volatile u32 I2CCR;       /**< 0x08	*/
        volatile u32 UARTCR;      /**< 0x0C	*/
        volatile u32 GINTLCR;     /**< 0x10	*/
        volatile u32 GINTHCR;     /**< 0x14	*/
        volatile u32 RESERVED;    /**< 0x18	*/
        volatile u32 SWAPCR;      /**< 0x1C	*/
        volatile u32 SSI1CR;      /**< 0x20	*/
        volatile u32 SSI2CR;      /**< 0x24	*/
        volatile u32 SPIMSWAPCR;  /**< 0x28	*/
        volatile u32 CLCDCR;      /**< 0x2C	*/
        volatile u32 CLCD_DIECR;  /**< 0x30	*/
        volatile u32 CLCD_DPSCR;  /**< 0x34	*/
        volatile u32 DCMICR;      /**< 0x38	*/
        volatile u32 SDRCR;       /**< 0x3C	*/
        volatile u32 SMCR;        /**< 0x40	*/
        volatile u32 SDRAMDCR;    /**< 0x44	*/
        volatile u32 M_ADDRIECR;  /**< 0x48	*/
        volatile u32 M_ADDRPUCR;  /**< 0x4C	*/
        volatile u32 M_ADDRPDCR;  /**< 0x50	*/
        volatile u32 SWAPHCR;     /**< 0x54	*/
        volatile u32 I2SCR;       /**< 0x58	*/
        volatile u32 SWAPINTCR;   /**< 0x5C	*/
        volatile u32 SWAP2CR;     /**< 0x60	*/
        volatile u32 SWAP3CR;     /**< 0x64	*/
        volatile u32 SWAP4CR;     /**< 0x68	*/
        volatile u32 SWAP5CR;     /**< 0x6C	*/
        volatile u32 SDRAMSWAPCR; /**< 0x70	*/
    } IOCTRL_TypeDef;
#define IOCTRL ((IOCTRL_TypeDef *)IOCTRL_BASE_ADDR)

    typedef struct
    {
        volatile u8 PIER;
        volatile u8 WDR;
        volatile u8 FDR;
        volatile u8 PER;
    } LED_TypeDef;
#define LED ((LED_TypeDef *)LED_BASE_ADDR)

#ifdef __MACRO_DEF_PCI__
/*pci vd control register:PCIVDCR*/
#define PCI_VD_DET_EN ((u32)(1 << 31))
#define PCI_VD_HVD_EN ((u32)(1 << 1))
#define PCI_VD_LVD_EN ((u32)(1 << 0))

/*pci td control register:PCITDCR*/
#define PCI_TD_EN ((u32)(1 << 31))

/*pci sdio config register:SDIOCR*/
/*pci control register:PCICR*/
#define PCI_PCICR_FILTER_FAIL_CNT_FLAG ((u32)(1 << 19))
#define PCI_PCICR_DET_CLK_EN ((u32)(1 << 7))
#define PCI_PCICR_DET_CLK_DIV_UPDATE_MASK ((u32)(1 << 4))

/*async timer period register:ATIMPR*/
#define PCI_ATIMPR_ASYNC_TIMER_FLAG ((u32)(1 << 8))
#define PCI_ATIMPR_ASYNC_TIMER_EN ((u32)(1 << 8))
#define PCI_ATIMPR_ASYNC_TIMER_SLEEP_MODE_EN ((u32)(1 << 8))

/*pci nvsram control register:PCINVRAMCR*/
#define PCI_PCINVSRAMCR_NVSRAM_EN ((u32)(1 << 31))
#define PCI_PCINVSRAMCR_NVSRAM_UNLOCK_CLEAR_EN ((u32)(1 << 23))
#define PCI_PCINVSRAMCR_NVSRAM_LOCK_CONTROL_EN ((u32)(1 << 15))

/*vd/td enable period config register:DETPCR*/
#define PCI_DETPCR_RTC32K_CLK_GATE_EN ((u32)(1 << 31))
#define PCI_DETPCR_NVSRAM_CLK_GATE_EN ((u32)(1 << 30))

/*pci sdio pull control register:SDIOPCR*/
#define PCI_SDIOPCR_SDIO_GPIO_EN ((u32)(1 << 8))
#define PCI_SDIOPCR_SDIO_PULL_EN ((u32)(1 << 0))

/*pci detect status register:PCIDETSR*/
#define PCI_PCIDETSR_SDIO_DET_INT_EN ((u32)(1 << 31))
#define PCI_PCIDETSR_HTD_DET_INT_EN ((u32)(1 << 30))
#define PCI_PCIDETSR_LTD_DET_INT_EN ((u32)(1 << 29))
#define PCI_PCIDETSR_HVD_DET_INT_EN ((u32)(1 << 26))
#define PCI_PCIDETSR_LVD_DET_INT_EN ((u32)(1 << 25))
#define PCI_PCIDETSR_NVSRAM_CLR_INT_EN ((u32)(1 << 24))
#define PCI_PCIDETSR_ASYNC_TIMER_INT_EN ((u32)(1 << 1))
#define PCI_PCIDETSR_ASYNC_TIMER_INT_FLAG ((u32)(1 << 0))

/*pci soft attack enable register:PCISFTATENR*/
#define PCI_PCISFTATENR_SOFT_ATTACK_EN ((u32)(1 << 0))

/*pci soft attack register:PCISFTATCR*/
#define PCI_PCISFTATCR_SOFT_ATTACK_MASK ((u32)(1 << 0))

/*pci sdio gpio control register:PCISDIOGCR*/
/*pci register write enable register:PCIRWENR*/
#define PCI_PCIRWENR_PCICR_CONF_EN ((u32)(1 << 5))
#define PCI_PCIRWENR_TIMERCR_CONF_EN ((u32)(1 << 4))
#define PCI_PCIRWENR_DETPCR_CONF_EN ((u32)(1 << 3))
#define PCI_PCIRWENR_SDIOCR_CONF_EN ((u32)(1 << 2))
#define PCI_PCIRWENR_VDCR_CONF_EN ((u32)(1 << 1))
#define PCI_PCIRWENR_TDCR_CONF_EN ((u32)(1 << 0))

/*pci nvsram key register:PCINVSRAMKEYR*/
/*pci rtc interface enable register:PCIRTCENR*/
#define PCI_PCIRTCENR_RTC_INTERFACE_DELAY_MATCH_EN ((u32)(1 << 31))
#define PCI_PCIRTCENR_RTC_INTERFACE_ISOLATE_EN ((u32)(1 << 6))
#define PCI_PCIRTCENR_RTC_INTERFACE_EN ((u32)(1 << 0))

/*pci detect status latch register:PCIDETSLR*/
#define PCI_PCIDETSLR_NVSRAM_CLR_FLAG ((u32)(1 << 16))
#define PCI_PCIDETSLR_HTD_DET_FLAG ((u32)(1 << 5))
#define PCI_PCIDETSLR_LTD_DET_FLAG ((u32)(1 << 4))
#define PCI_PCIDETSLR_HVD_DET_FLAG ((u32)(1 << 1))
#define PCI_PCIDETSLR_LVD_DET_FLAG ((u32)(1 << 0))

    /*pci irc control register:PCIIRCCR*/

#endif
    typedef struct
    {
        volatile u32 PCIVDCR; /**< 00 pci vd control register													*/
        volatile u32 PCITDCR; /**< 04 pci td control register													*/
        volatile u32 SDIOCR;  /**< 08 pci sdio config register 												*/
        volatile u32 PCICR;   /**< 0C pci control register 														*/

        volatile u32 ATIMPR;     /**< 10 async timer period register 										 */
        volatile u32 PCINVRAMCR; /**< 14 pci nvsram control register 										 */
        volatile u32 DETPCR;     /**< 18 vd/td enable period config register 						 */
        volatile u32 SDIOPCR;    /**< 1C pci sdio pull control register									 */

        volatile u32 PCIDETSR;    /**< 20 pci detect status register 											*/
        volatile u32 PCISFTATENR; /**< 24 pci soft attack enable register									*/
        volatile u32 PCISFTATCR;  /**< 28 pci soft attack register 												*/
        volatile u32 PCISDIOGCR;  /**< 2C pci sdio gpio control register 									*/

        volatile u32 PCIRWENR;     /**< 30 pci register write enable register							 */
        volatile u32 RESERVED0;    /**< 34    */
        volatile u32 PCINVRAMKEYR; /**< 38 pci nvsram key register 												 */
        volatile u32 PCIRTCENR;    /**< 3C pci rtc interface enable register 							 */

        volatile u32 RESERVED1; /**< 40 */
        volatile u32 PCIDETSLR; /**< 44 pci detect status latch register 								*/
        volatile u32 PCIIRCCR;  /**< 48 pci irc control register 												*/
        volatile u32 PCICLKSR;  /**< 4C  */

        volatile u32 PCICLKCER; /**< 50 */
    } PCI_TypeDef;
#define PCI ((PCI_TypeDef *)PCI_BASE_ADDR)

#ifdef __MACRO_DEF_PIT__
#define PIT_PCSR_EN (1 << 0) /**< =1,则PIT使能 */
#define PIT_PCSR_RLD (1 << 1)
#define PIT_PCSR_PIF (1 << 2) /**< =1,则PIT计数达到了0,并且产生了中断信号 */
#define PIT_PCSR_PIE (1 << 3) /**< =1,则PIT中断请求使能 */
#define PIT_PCSR_OVW (1 << 4)
#define PIT_PCSR_PDBG (1 << 5)
#define PIT_PCSR_PDOZE (1 << 6)
#define PIT_PCSR_PRESCALER_1 (0x00 << 8)     /**< PIT模块分频数值:1分频    */
#define PIT_PCSR_PRESCALER_2 (0x01 << 8)     /**< PIT模块分频数值:2分频    */
#define PIT_PCSR_PRESCALER_4 (0x02 << 8)     /**< PIT模块分频数值:4分频    */
#define PIT_PCSR_PRESCALER_8 (0x03 << 8)     /**< PIT模块分频数值:8分频    */
#define PIT_PCSR_PRESCALER_16 (0x04 << 8)    /**< PIT模块分频数值:16分频   */
#define PIT_PCSR_PRESCALER_32 (0x05 << 8)    /**< PIT模块分频数值:32分频   */
#define PIT_PCSR_PRESCALER_64 (0x06 << 8)    /**< PIT模块分频数值:64分频   */
#define PIT_PCSR_PRESCALER_128 (0x07 << 8)   /**< PIT模块分频数值:128分频  */
#define PIT_PCSR_PRESCALER_256 (0x08 << 8)   /**< PIT模块分频数值:256分频  */
#define PIT_PCSR_PRESCALER_512 (0x09 << 8)   /**< PIT模块分频数值:512分频  */
#define PIT_PCSR_PRESCALER_1024 (0x0a << 8)  /**< PIT模块分频数值:1024分频 */
#define PIT_PCSR_PRESCALER_2048 (0x0b << 8)  /**< PIT模块分频数值:分频2048 */
#define PIT_PCSR_PRESCALER_4096 (0x0c << 8)  /**< PIT模块分频数值:分频4096 */
#define PIT_PCSR_PRESCALER_8192 (0x0d << 8)  /**< PIT模块分频数值:分频8192 */
#define PIT_PCSR_PRESCALER_16384 (0x0e << 8) /**< PIT模块分频数值:分频16384 */
#define PIT_PCSR_PRESCALER_32768 (0x0f << 8) /**< PIT模块分频数值:分频32768 */

#endif

    typedef struct
    {
        volatile u16 PCSR;  /**< PIT控制和状态寄存器, 偏移地址: 0x0000 */
        volatile u16 PMR;   /**< PIT计数load寄存器, 偏移地址: 0x0002 */
        volatile u16 PCNTR; /**< PIT当前计数寄存器, 偏移地址: 0x0004 */
    } PIT_TypeDef;

    typedef struct
    {
        union
        {
            volatile unsigned short PCSR; //     0x00
            struct
            {
                volatile unsigned char PCSR_H; //     0x00
                volatile unsigned char PCSR_L; //     0x01
            };
        };
        unsigned short RESERVED; // 0x02~0x03

        // volatile u32 PCSR;  /**< PIT32控制和状态寄存器, 偏移地址: 0x0000 */
        volatile u32 PMR;   /**< PIT32计数load寄存器, 偏移地址: 0x0004 */
        volatile u32 PCNTR; /**< PIT32当前计数寄存器, 偏移地址: 0x0008 */
    } PIT32_TypeDef;
#define PIT1 ((PIT32_TypeDef *)PIT1_BASE_ADDR)
#define PIT2 ((PIT32_TypeDef *)PIT2_BASE_ADDR)
#define PIT3 ((PIT32_TypeDef *)PIT3_BASE_ADDR)
#define PIT4 ((PIT32_TypeDef *)PIT4_BASE_ADDR)

#ifdef __MACRO_DEF_CAN__
/*** MCR ************/
#define CAN_MCR_MAXMB (0x0000003f)         /* CAN MCR MABMB area mask */
#define CAN_MCR_MDIS (((uint32_t)1 << 31)) /* CAN MCR MDIS bit mask */
#define CAN_MCR_FRZ (((uint32_t)1 << 30))  /* CAN MCR FRZ bit mask */
#define CAN_MCR_WRN_EN (0x00200000)        /* CAN MCR WRN_EN bit mask */
#define CAN_MCR_SOFT_RST (0x02000000)      /* CAN MCR SOFT_RST bit mask */
#define CAN_MCR_FEN (0x20000000)           /* CAN MCR FEN bit mask */
#define CAN_MCR_HALT (0x10000000)          /* CAN MCR HALT bit mask */
#define CAN_MCR_NOT_RDY (0x08000000)       /* CAN MCR NOT_RDY bit mask */
#define CAN_MCR_FRZ_ACK (0x01000000)       /* CAN MCR FRZ_ACK bit mask */
#define CAN_MCR_SLF_WAK (0x00400000)       /* CAN MCR SLF_WAK bit mask */
#define CAN_MCR_LPM_ACK (0x00100000)       /* CAN MCR LPM_ACK bit mask */
#define CAN_MCR_LPRIO_EN (0x00002000)      /* CAN MCR LPRIO_EN bit mask */
#define CAN_MCR_SRX_DIS (0X00020000)       /* CAN MCR SRX_DIS bit mask */
#define CAN_MCR_BCC (0x00010000)           /* CAN MCR BCC bit mask */
#define CAN_MCR_AEN (0x00001000)           /* CAN MCR AEN bit mask */
#define CAN_MCR_IDAM_A (0x00000000)        /* CAN MCR IDAM format A mask */
#define CAN_MCR_IDAM_B (0x00000100)        /* CAN MCR IDAM format B mask */
#define CAN_MCR_IDAM_C (0x00000200)        /* CAN MCR IDAM format C mask */

/*** CR ************/
#define CAN_TIMING_MASK (0x00C0FFF8) /* CAN CR PRESDIV&PSEG1&PSEG2&PROPSEG area mask */
#define CAN_CR_RJW_2 (0x00800000)    /* CAN CR RJW 2 mask */
#define CAN_CR_BOFF_MSK (0x00008000) /* CAN CR BOFF_MSK mask */
#define CAN_CR_ERR_MSK (0x00004000)  /* CAN CR ERR_MSK mask */
#define CAN_CR_CLK_SRC (0x00002000)  /* CAN CR This bit selects the clock source */
#define CAN_CR_LPB (0x00001000)      /* CAN CR LPB bit mask */
#define CAN_CR_TWRN_MSK (0x00000800) /* CAN CR TWRN_MSK mask */
#define CAN_CR_RWRN_MSK (0x00000400) /* CAN CR RWRN_MSK mask */
#define CAN_CR_BUFF_REC (0x00000040) /* CAN CR BUFF_REC bit mask */
#define CAN_CR_TSYN (0x00000020)     /* CAN CR TSYN bit mask */
#define CAN_CR_LBUF (0x00000010)     /* CAN CR LBUF bit mask */
#define CAN_CR_LOM (0x00000008)      /* CAN CR LOM bit mask */

/*** SR ************/
#define CAN_ESR_TWRN_INT (0x00020000)    // CAN ESR TWRN_INT bit mask
#define CAN_ESR_RWRN_INT (0x00010000)    // CAN ESR RWRN_INT bit mask
#define CAN_ESR_BIT0_ERROR (0x1 << 14)   // CAN ESR BIT0_ERROR bit mask
#define CAN_ESR_Error_Passive (0x1 << 4) // CAN ESR Error Passive
#define CAN_ESR_BOFF_INT (0x00000004)    // CAN ESR BOFF_INT bit mask
#define CAN_ESR_ERR_INT (0x00000002)     // CAN ESR ERR_INT bit mask

/*** IM1R ************/
#define CAN_IM1R_RX_FIFO_INT_MASK (0xffffff1f) // CAN IF1R RX_FIFO_INT bit mask
#define CAN_IM1R_RX_FIFO_INT (0xe0)            // CAN IF1R RX_FIFO_INT bit mask

/*** IF1R ************/
#define CAN_IF1R_RX_FIFO_OVERRUN_FLAG (0x1 << 7)        // CAN Rx fifo overrun interrupt flag
#define CAN_IF1R_RX_FIFO_ALMOST_OVERRUN_FLAG (0x1 << 6) // CAN Rx fifo almost overrun interrupt flag
#define CAN_IF1R_RX_FIFO_FLAG (0x1 << 5)                // CAN Rx fifo interrupt flag

/*** MB ************/
#define CAN_RXCODE_INACT (0x00000000)  // CAN rx code for INACTIVE
#define CAN_RXCODE_EMPTY (0x04000000)  // CAN rx code for EMPTY
#define CAN_TXCODE_INACT (0x08000000)  // CAN tx code for INACTIVE
#define CAN_TXCODE_ABORT (0x09000000)  // CAN rx code for ABORT
#define CAN_TXCODE_ONCE (0x0c000000)   // CAN rx code for TRANSMIT UNCONDITIONALLY ONCE
#define CAN_TXCODE_REMOTE (0x0a000000) // CAN rx code for TRANSMIT BY REMOTE REQUEST
#define CAN_MBS_RTR (0x00100000)       // CAN MB RTR bit mask
#define CAN_MBS_IDE (0x00200000)       // CAN MB IDE bit mask
#define CAN_MBS_SRR (0x00400000)       // CAN MB SRR bit mask
#define CAN_FIFO_EXT_MSK (0x40000000)  // CAN ID table EXT bit mask
#define CAN_DLC_8BYTE (0x00080000)

#endif

    /**
     * @brief  CAN消息缓存区
     */
    typedef struct
    {
        volatile u32 MB_CS;     // Message Buffer control & time stamp word
        volatile u32 MB_ID;     // Message Buffer priority & id word
        volatile u8 MB_DATA[8]; // Message Buffer data byte0~8

    } CAN_MessageBuffer_TypeDef;
    /**
     * @brief  CAN消息缓存区
     */

    /**
     * @brief  CAN rx individual mask registers
     */
    typedef struct
    {
        volatile u32 CAN_RXIMR[64]; // CAN rx individual mask register0~63

    } CAN_RxIndividualMask_TypeDef;
    /**
     * @brief  CAN rx individual mask registers
     */

    /**
     * @brief  CAN rx fifo registers
     */
    typedef struct
    {
        volatile u32 ID_TABLE[8]; // CAN rx individual mask register0~63

    } CAN_RxFifoIDTable_TypeDef;
    /**
     * @brief  CAN rx individual mask registers
     */

    /**
     * @brief  CAN模块寄存器定义
     */
    typedef struct
    {
        volatile u32 CAN_MCR;                 /**< 0x00 模块配置寄存器 */
        volatile u32 CAN_CR;                  /**< 0x04 控制寄存器 */
        volatile u32 CAN_FRT;                 /**< 0x08 自由运行定时器寄存器 */
        volatile u32 RESERVED0;               /**< 0x0C 保留 */
        volatile u32 CAN_RXGM;                /**< 0x10 RX全局掩码寄存器 */
        volatile u32 CAN_RX14M;               /**< 0x14 RX缓存掩码寄存器14 */
        volatile u32 CAN_RX15M;               /**< 0x18 RX缓存掩码寄存器15 */
        volatile u32 CAN_ECR;                 /**< 0x1C 错误计数寄存器 */
        volatile u32 CAN_ESR;                 /**< 0x20 错误状态寄存器 */
        volatile u32 CAN_IM2R;                /**< 0x24 中断掩码寄存器2 */
        volatile u32 CAN_IM1R;                /**< 0x28 中断掩码寄存器1 */
        volatile u32 CAN_IF2R;                /**< 0x2C 中断标志寄存器2 */
        volatile u32 CAN_IF1R;                /**< 0x30 中断标志寄存器1 */
        volatile u32 RESERVED1[19];           /**< 0x34~0x7f 保留 */
        CAN_MessageBuffer_TypeDef CAN_MB[64]; /**< 0x80~0x47F 消息缓存区*/
    } CAN_TypeDef;
#define CAN1 ((CAN_TypeDef *)CAN1_BASE_ADDR)

#ifdef __MACRO_DEF_RESET__
/*** RCR ************/
#define RESET_RCR_HFDE ((u32)(1U << 0))
#define RESET_RCR_HFDRE ((u32)(1U << 1))
#define RESET_RCR_HFDIE ((u32)(1U << 2))
#define RESET_RCR_HFDF ((u32)(1U << 3))
#define RESET_RCR_LFDE ((u32)(1U << 8))
#define RESET_RCR_LFDRE ((u32)(1U << 9))
#define RESET_RCR_LFDIE ((u32)(1U << 10))
#define RESET_RCR_LFDF ((u32)(1U << 11))
#define RESET_RCR_HVDE ((u32)(1U << 16))
#define RESET_RCR_HVDRE ((u32)(1U << 17))
#define RESET_RCR_HVDIE ((u32)(1U << 18))
#define RESET_RCR_HVDF ((u32)(1U << 19))
#define RESET_RCR_CRWE ((u32)(1U << 22))
#define RESET_RCR_CRE ((u32)(1U << 23))
#define RESET_RCR_LVDE ((u32)(1U << 24))
#define RESET_RCR_LVDRE ((u32)(1U << 25))
#define RESET_RCR_LVDIE ((u32)(1U << 26))
#define RESET_RCR_LVDF ((u32)(1U << 27))
#define RESET_RCR_FRCR_STOUT ((u32)(1U << 30))
#define RESET_RCR_SOFTRST ((u32)(1U << 31))
#endif
    typedef struct
    {
        volatile u32 RCR;  // 0x00
        volatile u8 LVDCR; // 0x04
        volatile u8 HVDCR; // 0x05
        volatile u8 RTR;   // 0x06
        volatile u8 RSR;   // 0x07
    } RESET_TypeDef;
#define RST ((RESET_TypeDef *)(RESET_BASE_ADDR))

#ifdef __MACRO_DEF_RTC__
/*PRT1R:PMU_RTC Timer1 Register */
#define RTC_PRT1R_DAYS_COUNTER (0)
#define RTC_PRT1R_DAYS_COUNTER_MASK ((u32)0x00007FFF) /**<  */

/*PRT2R:PMU_RTC Timer2 Register */
#define RTC_PRT2R_SECONDS_COUNTER (0)
#define RTC_PRT2R_SECONDS_COUNTER_MASK ((u32)0x0000003F) /**<  */
#define RTC_PRT2R_MINUTES_COUNTER (8)
#define RTC_PRT2R_MINUTES_COUNTER_MASK ((u32)0x00003F00) /**<  */
#define RTC_PRT2R_HOURS_COUNTER (16)
#define RTC_PRT2R_HOURS_COUNTER_MASK ((u32)0x001F0000) /**<  */

/*PRA1R:PMU_RTC Alarm1 Register */
#define RTC_PRA1R_ALARM_DAYS_EN (0x01 << 15) /**<  */
#define RTC_PRA1R_ALARM_DAYS (0)
#define RTC_PRA1R_ALARM_DAYS_MASK ((u32)0x00007FFF) /**<  */

/*PRA2R:PMU_RTC Alarm2 Register */
#define RTC_PRA2R_ALARM_HOURS_MASK ((u32)0x001F0000)   /**<  */
#define RTC_PRA2R_ALARM_MINUTES_MASK ((u32)0x00003F00) /**<  */
#define RTC_PRA2R_ALARM_SECONDS_MASK ((u32)0x0000003F) /**<  */
#define RTC_PRA2R_ALARM_HOURS (16)
#define RTC_PRA2R_ALARM_HOURS_EN (0x01 << 23) /**<  */
#define RTC_PRA2R_ALARM_MINUTES (8)
#define RTC_PRA2R_ALARM_MINUTES_EN (0x01 << 15) /**<  */
#define RTC_PRA2R_ALARM_SECONDS (0)
#define RTC_PRA2R_ALARM_SECONDS_EN (0x01 << 7) /**<  */

/*PRTCR:PMU_RTC Timer Counter Register */
#define RTC_PRTCR_DAYS_COUNTER_MASK ((u32)0xFFFE0000)
#define RTC_PRTCR_DAYS_COUNTER (17)
#define RTC_PRTCR_HOURS_COUNTER_MASK ((u32)0x0001F000)
#define RTC_PRTCR_HOURS_COUNTER (12)
#define RTC_PRTCR_MINUTES_COUNTER_MASK ((u32)0x00000FC0)
#define RTC_PRTCR_MINUTES_COUNTER (6)
#define RTC_PRTCR_SECONDS_COUNTER_MASK ((u32)0x0000003F)
#define RTC_PRTCR_SECONDS_COUNTER (0)

/*PRCSR:PMU_RTC Control and Status Register */
#define RTC_PRCSR_DAY_IEN (0x01 << 30)
#define RTC_PRCSR_HOUR_IEN (0x01 << 29)
#define RTC_PRCSR_MINUTE_IEN (0x01 << 28)
#define RTC_PRCSR_SECOND_IEN (0x01 << 27)
#define RTC_PRCSR_ALARM_IEN (0x01 << 26)
#define RTC_PRCSR_1KHZ_IEN (0x01 << 25)
#define RTC_PRCSR_32KHZ_IEN (0x01 << 24)
#define RTC_PRCSR_DAY_INTF (0x01 << 22)
#define RTC_PRCSR_HOUR_INTF (0x01 << 21)
#define RTC_PRCSR_MINUTE_INTF (0x01 << 20)
#define RTC_PRCSR_SECOND_INTF (0x01 << 19)
#define RTC_PRCSR_ALARM_INTF (0x01 << 18)
#define RTC_PRCSR_1KHZ_INTF (0x01 << 17)
#define RTC_PRCSR_32KHZ_INTF (0x01 << 16)
#define RTC_PRCSR_INT_TYPE_MASK ((u32)0x00000300)
#define RTC_PRCSR_LOW_LEVEL_INT (0x00 << 8)           /**< RTC中断产生类型为低电压    */
#define RTC_PRCSR_RISEING_EDGE_INT (0x01 << 8)        /**< RTC中断产生类型为上升沿    */
#define RTC_PRCSR_FALLING_EDGE_INT (0x02 << 8)        /**< RTC中断产生类型为下降沿    */
#define RTC_PRCSR_RISING_FALLING_EDGE_INT (0x03 << 8) /**< RTC中断产生类型为上升沿或下降沿    */
#define RTC_PRCSR_WCLK_DIV_MASK ((u32)0x000000F8)     /**<  */
#define RTC_PRCSR_CNTUPDATE_EN (0x01 << 2)
#define RTC_PRCSR_RCNT_WEN (0x01 << 1)
#define RTC_PRCSR_DIR_EN (0x01 << 0)

/*PRENR:PMU_RTC Enable Register */
#define RTC_PRENR_RTC_EN_CNTUPDATE_EN (0x01 << 10)
#define RTC_PRENR_RTC_EN_RCNT_WEN (0x01 << 9)
#define RTC_PRENR_RTC_EN_DIR (0x01 << 8)
#define RTC_PRENR_RTC_EN (0x01 << 0)

/*PRKEYR:PMU_RTC Key Register */
#define RTC_PRKEYR_KEY (0x5AA55AA5)
#endif
    typedef struct
    {
        volatile u32 PRT1R; // 0x00
        volatile u32 PRT2R; // 0x04
        volatile u32 PRA1R; // 0x08
        volatile u32 PRA2R; // 0x0c

        volatile u32 PRTCR;  // 0x10
        volatile u32 PRCSR;  // 0x14
        volatile u32 PRENR;  // 0x18
        volatile u32 PRKEYR; // 0x1c
    } RTC_TypeDef;
#define RTC ((RTC_TypeDef *)(RTC_BASE_ADDR))

    typedef struct
    {
        volatile u32 TS1CR;   // 0x00
        volatile u32 TS1SR;   // 0x04
        volatile u32 MSCR;    // 0x08
        volatile u32 OBTCR1;  // 0x0c
        volatile u32 OBTCR2;  // 0x10
        volatile u32 OBTCR;   // 0x14
        volatile u32 OBTSR;   // 0x18
        volatile u32 LFTR;    // 0x1c
        volatile u32 HFTR;    // 0x20
        volatile u32 OBTDR;   // 0x24
        volatile u32 OBTCNTR; // 0x28
    } SECDET_TypeDef;
#define SECDET ((SECDET_TypeDef *)(SECDET_BASE_ADDR))

#ifdef __MACRO_DEF_SPI__
/*SPIFR*/
#define SPI_FRAME_FORMAT_TI ((u8)(1 << 4))                      /**< TI frame format*/
#define SPI_FRAME_FORMAT_FREESCALE ((u8)(~SPI_FRAME_FORMAT_TI)) /**< freescale frame format*/
#define SPI_LOOP_BACK_MODE ((u8)(1 << 5))                       /**< loop back mode*/
#define SPI_LOOP_NORMAL_MODE ((u8)(~SPI_LOOP_BACK_MODE))        /**< normal mode*/
#define SPI_EN_GUARD_TIME ((u8)(1 << 6))                        /**< guard time is enable*/
#define SPI_DIS_GUARD_TIME ((u8)(~SPI_EN_GUARD_TIME))           /**< guard time is disable*/
#define SPI_EN_CONT_CS ((u8)(1 << 7))                           /**< keep peripheral chip select signal low between transfer until EOTF is set*/
#define SPI_DIS_CONT_CS ((u8)(~SPI_EN_CONT_CS))                 /**< return peripheral chip select singnal to high between transfers*/
#define SPI_FRAME_SIZE_MASK ((u8)(0xF0))                        /**< */
#define SPI_FRAME_SIZE_SHIFT_MASK ((u8)(0))                     /**< */
/*CR1*/
#define SPI_FIRSTBIT_LSB ((u8)(1 << 0))             /**< êy?Y′?ê?LSB                             */
#define SPI_FIRSTBIT_MSB ((u8)(0xFE))               /**< êy?Y′?ê?MSB                             */
#define SPI_EN_SSOE ((u8)(1 << 1))                  /**< SPI ????SS1ü??×÷ó????ü                  */
#define SPI_DIS_SSOE ((u8)(0xFD))                   /**< SPI ????SS1ü??×÷ó????ü                  */
#define SPI_CPHA_1EDGE ((u8)(0xFB))                 /**< SPI CPHAêy?Y2é?ˉ±?????è?:SPI_CPHA_2EDGE */
#define SPI_CPHA_2EDGE ((u8)(1 << 2))               /**< SPI CPHAêy?Y2é?ˉ±?????è?:1EDGE          */
#define SPI_CPOL_HIGH ((u8)(1 << 3))                /**< SPI CPOL???Dê±μ???éè??:??               */
#define SPI_CPOL_LOW ((u8)(0xF7))                   /**< SPI CPOL???Dê±μ???éè??:μí               */
#define SPI_MASTER ((u8)(1 << 4))                   /**< SPI1¤×÷?￡ê?:?÷?￡ê?                      */
#define SPI_SLAVE ((u8)(0xEF))                      /**< SPI1¤×÷?￡ê?:′ó?￡ê?                      */
#define SPI_WIRED_OR_MODE_OPEN_DRAIN ((u8)(1 << 5)) /**< SPIê?3??￡ê?:?a??ê?3?                    */
#define SPI_WIRED_OR_MODE_CMOS ((u8)(0xDF))         /**< SPIê?3??￡ê?:CMOSê?3?                    */
#define SPI_EN_SSOE ((u8)(1 << 1))                  /**< SPI ????SS1ü??×÷ó?ê1?ü                  */
#define SPI_EN ((u8)(1 << 6))                       /**< SPI?￡?éê1?ü                             */
#define SPI_DIS ((u8)(0xBF))                        /**< SPI?￡?é???1                             */
#define SPI_EN_IT ((u8)(1 << 7))                    /**< SPI?D??ê1?ü                             */
#define SPI_DIS_IT ((u8)(~SPI_EN_IT))               /**< SPI?D?????1                             */
/*SPICR2*/
#define SPI_SPC0_BIDIR_MODE ((u8)(1 << 0))    /**< ???????ò1ü???￡ê?:???ò                   */
#define SPI_SPC0_NORMAL_MODE ((u8)(0xFE))     /**< ????SPI?úDOZE?￡ê???:?y3￡?￡ê?            */
#define SPI_DOZE_INACTIVE_MODE ((u8)(1 << 1)) /**< ????SPI?úDOZE?￡ê???:í￡?1                */
#define SPI_DOZE_ACTIVE_MODE ((u8)(0xFD))     /**< ????SPI?úDOZE?￡ê???:?y3￡1¤×÷            */
#define SPI_GUARD_TIME_MASK ((u8)(0x03))      /**< */
#define SPI_GUARD_TIME_SHIFT_MASK ((u8)(2))   /**< */
/*SPITXFTOCTR TX FIFO timeout counter register*/
#define SPI_EN_TX_FIFO_TINEOUT ((u8)(1 << 6))                         /**< TX FIFO timeout funtion enable   */
#define SPI_DIS_TX_FIFO_TIMEOUT ((u8)(~SPI_EN_TX_FIFO_TINEOUT))       /**< TX FIFO timeout funtion disable  */
#define SPI_EN_TX_FIFO_TINEOUT_IT ((u8)(1 << 7))                      /**< TX FIFO timeout interrupt enable */
#define SPI_DIS_TX_FIFO_TIMEOUT_IT ((u8)(~SPI_EN_TX_FIFO_TINEOUT_IT)) /**< TX FIFO timeout interrupt disable*/
#define SPI_TX_FIFO_TIMEOUT_CNT_MASK ((u8)(0xC0))
/*SPITXFTOCTR RX FIFO timeout counter register*/
#define SPI_EN_RX_FIFO_TINEOUT ((u8)(1 << 6))                         /**< RX FIFO timeout funtion enable   */
#define SPI_DIS_RX_FIFO_TIMEOUT ((u8)(~SPI_EN_RX_FIFO_TINEOUT))       /**< RX FIFO timeout funtion disable  */
#define SPI_EN_RX_FIFO_TINEOUT_IT ((u8)(1 << 7))                      /**< RX FIFO timeout interrupt enable */
#define SPI_DIS_RX_FIFO_TIMEOUT_IT ((u8)(~SPI_EN_RX_FIFO_TINEOUT_IT)) /**< RX FIFO timeout interrupt disable*/
#define SPI_RX_FIFO_TIMEOUT_CNT_MASK ((u8)(0xC0))
/*SPI TX FIFO control register*/
#define SPI_EN_TX_FIFO_SERVICE_THRESHOLD_IT ((u8)(1 << 4)) /**< */
#define SPI_EN_TX_FIFO_UNDERFLOW_IT ((u8)(1 << 5))         /**< */
#define SPI_EN_TX_FIFO_OVERFLOW_IT ((u8)(1 << 6))          /**< */
#define SPI_TX_FIFO_CLEAR ((u8)(1 << 7))                   /**< */
#define SPI_TX_FIFO_SERVICE_TRHESHOLD_MASK ((u8)(0xF0))    /**< */
#define SPI_TX_FIFO_SERVICE_TRHESHOLD_SHIFT_MASK ((u8)(0)) /**< */
/*SPI RX FIFO control register*/
#define SPI_EN_RX_FIFO_SERVICE_THRESHOLD_IT ((u8)(1 << 4))
#define SPI_EN_RX_FIFO_UNDERFLOW_IT ((u8)(1 << 5))         /**< */
#define SPI_EN_RX_FIFO_OVERFLOW_IT ((u8)(1 << 6))          /**< */
#define SPI_RX_FIFO_CLEAR ((u8)(1 << 7)) /**< */           /**< */
#define SPI_RX_FIFO_SERVICE_TRHESHOLD_MASK ((u8)(0xF0))    /**< */
#define SPI_RX_FIFO_SERVICE_TRHESHOLD_SHIFT_MASK ((u8)(0)) /**< */
/*SPIPURD Pullup and reduced driver register*/
#define SPI_EN_DOUBLE_TX_DATA_MODE ((u8)(1 << 1))                           /**< */
#define SPI_DIS_DOUBLE_TX_DATA_MODE_DIS ((u8)(~SPI_EN_DOUBLE_TX_DATA_MODE)) /**< */
#define SPI_EN_MISO_MOSI_SWITCH ((u8)(1 << 2))                              /**< */
#define SPI_DIS_MISO_MOSI_SWITCH ((u8)(~SPI_EN_MISO_MOSI_SWITCH))           /**< */
#define SPI_EN_HIGH_SPEED ((u8)(1 << 7))                                    /**< SPI′ó?￡ê????ùê1?ü                       */
#define SPI_DIS_HIGH_SPEED ((u8)(0x7F))                                     /**< SPI′ó?￡ê????ù???1                       */
#define SPI_MASTER_SAMPLE_POINT_DELAY_MASK ((u8)(0xF3))                     /**< */
#define SPI_MASTER_SAMPLE_POINT_DELAY_SHIFT_MASK ((u8)(2))                  /**< */
#define SPI_REDUCE_CAP ((u8)(1 << 4))                                       /**< SPI?y?ˉ?üá|??D?                         */
#define SPI_FULL_DRIVER ((u8)(0xEF))                                        /**< SPI?y?ˉ?üá|?y3￡                         */
#define SPI_EN_PULLDOWN_DEVICE ((u8)(1 << 1))                               /**< SPIé?à-ê1?ü                             */
#define SPI_DIS_PULLDOWN_DEVICE ((u8)(0xFD))                                /**< SPIé?à-???1                             */
#define SPI_EN_PULLUP_DEVICE ((u8)(1 << 0))                                 /**< SPIé?à-ê1?ü                             */
#define SPI_DIS_DIS_PULLUP_DEVICE ((u8)(0xFE))                              /**< SPIé?à-???1                             */
#define SPI_PPS_SHIFT_MASK (4)
#define SPI_DDRSP_SHIFT_MASK (0)
/*IRSP interrupt register of SS*/
#define SPI_SS_PIN_ASSIGNMENT_SEL_MASK ((u8)(0x03))       /**< */
#define SPI_SS_PIN_ASSIGNMENT_SEL_SHIFT_MASK ((u8)(0x03)) /**< */
#define SPI_SS_PIN_DATA_BIT ((u8)(1 << 2))                /**< */
#define SPI_SS_LEVEL_HIGH ((u8)(1 << 3))                  /**< */
#define SPI_SS_LEVEL_LOW ((u8)(~SPI_SS_LEVEL_HIGH))       /**< */
#define SPI_SS_PIN_FLAG ((u8)(1 << 4))                    /**< */
#define SPI_EN_SS_IT ((u8)(1 << 7))                       /**< */
#define SPI_DIS_SS_IT ((u8)(~SPI_EN_SS_IT))               /**< */
/*TX FIFO status register*/
#define SPI_TX_FIFO_NEXT_POINTER_MASK ((u8)(0x0F))       /**< */
#define SPI_TX_FIFO_NEXT_POINTER_SHIFT_MASK ((u8)(0x0F)) /**< */
#define SPI_TX_FIFO_COUNTER_MASK ((u8)(0xF0))            /**< */
#define SPI_TX_FIFO_COUNTER_SHIFT_MASK ((u8)(0x00))      /**< */
/*RX FIFO status register*/
#define SPI_RX_FIFO_NEXT_POINTER_MASK ((u8)(0x0F))       /**< */
#define SPI_RX_FIFO_NEXT_POINTER_SHIFT_MASK ((u8)(0x0F)) /**< */
#define SPI_RX_FIFO_COUNTER_MASK ((u8)(0xF0))            /**< */
#define SPI_RX_FIFO_COUNTER_SHIFT_MASK ((u8)(0x00))      /**< */
/*SPI status register*/
#define SPI_FLAG_RX_FIFO_EMPTY ((u8)(1 << 0))    /**< SPI±ê??:                            */
#define SPI_FLAG_RX_FIFO_FULL ((u8)(1 << 1))     /**< SPI±ê??:                            */
#define SPI_FLAG_TX_FIFO_EMPTY ((u8)(1 << 2))    /**< SPI±ê??:                            */
#define SPI_FLAG_TX_FIFO_FULL ((u8)(1 << 3))     /**< SPI±ê??:                            */
#define SPI_FLAG_MODE_FAULT ((u8)(1 << 4))       /**< SPI±ê??:?￡ê?′í?ó                        */
#define SPI_FLAG_TRANSMISSION_END ((u8)(1 << 5)) /**< SPI±ê??:                            */
#define SPI_FLAG_FRAME_LOST ((u8)(1 << 6))       /**< SPI±ê??:3?í?                            */
#define SPI_FLAG_FINISH ((u8)(1 << 7))           /**< SPI±ê??:?D??                            */
#define SPI_FLAG_RX_FIFO_SERVICE ((u8)(1 << 0))  /**< SPI±ê??:                            */
#define SPI_FLAG_RX_FIFO_UNDEFLOW ((u8)(1 << 1)) /**< SPI±ê??:                            */
#define SPI_FLAG_RX_FIFO_OVERFLOW ((u8)(1 << 2)) /**< SPI±ê??:                            */
#define SPI_FLAG_RX_FIFO_TIMEOUT ((u8)(1 << 3))  /**< SPI±ê??:                            */
#define SPI_FLAG_TX_FIFO_SERVICE ((u8)(1 << 4))
#define SPI_FLAG_TX_FIFO_UNDERFLOW ((u8)(1 << 5)) /**< SPI±ê??:                            */
#define SPI_FLAG_TX_FIFO_OVERFLOW ((u8)(1 << 6))  /**< SPI±ê??:                            */
#define SPI_FLAG_TX_FIFO_TIMEOUT ((u8)(1 << 7))   /**< SPI±ê??:                            */
/*interrupt control register*/
#define SPI_EN_MODE_FAULT_IT ((u8)(1 << 4))                 /**< */
#define SPI_DIS_MODE_FAULT_IT ((u8)(~SPI_EN_MODE_FAULT_IT)) /**< */
#define SPI_EN_FRAME_LOST_IT ((u8)(1 << 5))                 /**< */
#define SPI_DIS_FRAME_LOST_IT ((u8)(~SPI_EN_FRAME_LOST_IT)) /**< */
/*SPI DMA threshold register*/
#define SPI_TX_DMA_THRESHOLD_MASK ((u8)(0x0F))       /**< */
#define SPI_TX_DMA_THRESHOLD_SHIFT_MASK ((u8)(0x04)) /**< */
#define SPI_RX_DMA_THRESHOLD_MASK ((u8)(0xF0))       /**< */
#define SPI_RX_DMA_THRESHOLD_SHIFT_MASK ((u8)(0x00)) /**< */
/*SPI DMA control register*/
#define SPI_EN_RX_DMA ((u8)(0x01))            /**< */
#define SPI_DIS_RX_DMA ((u8)(~SPI_EN_RX_DMA)) /**< */
#define SPI_EN_TX_DMA ((u8)(0x02))            /**< */
#define SPI_DIS_TX_DMA ((u8)(~SPI_EN_TX_DMA))
#endif
    typedef struct
    {
        volatile u8 BR;        /**< 0*/
        volatile u8 FR;        /**< 1*/
        volatile u8 CR1;       /**< 2*/
        volatile u8 CR2;       /**< 3*/
        volatile u8 RXFTOCTR;  /**< 4*/
        volatile u8 TXFTOCTR;  /**< 5*/
        volatile u8 RXFCR;     /**< 6*/
        volatile u8 TXFCR;     /**< 7*/
        volatile u8 ASCDR;     /**< 8*/
        volatile u8 BSCDR;     /**< 9*/
        volatile u8 DDR;       /**< a*/
        volatile u8 PURD;      /**< b*/
        volatile u8 TCNTM;     /**< c*/
        volatile u8 TCNTH;     /**< d*/
        volatile u8 PORT;      /**< e*/
        volatile u8 TCNTL;     /**< f*/
        volatile u8 IRSP;      /**< 10*/
        volatile u8 RESERVED0; /**< 11*/
        volatile u8 DR;        /**< 12*/
        volatile u8 DRH;       /**< 13*/
        volatile u8 RXFSR;     /**< 14*/
        volatile u8 TXFSR;     /**< 15*/
        volatile u8 SR;        /**< 16*/
        volatile u8 SRH;       /**< 17*/
        volatile u8 FDCR;      /**< 18*/
        volatile u8 ICR;       /**< 19*/
        volatile u8 DMACR;     /**< 1a*/
        volatile u8 DMATHR;    /**< 1b*/
        volatile u8 RXFDBGR;   /**< 1c*/
        volatile u8 RESERVED1; /**< 1d*/
        volatile u8 TXFDBGR;   /**< 1e*/
    } SPI_TypeDef;
#define SPI1 ((SPI_TypeDef *)(SPI1_BASE_ADDR))
#define SPI2 ((SPI_TypeDef *)(SPI2_BASE_ADDR))
#define SPI3 ((SPI_TypeDef *)(SPI3_BASE_ADDR))

#ifdef __MACRO_DEF_SSI__
/*SSI_CTRLR0*/
#define CTRLR0_DFS_VALUE(x) (x << 0)
#define CTRLR0_FRF_MOT (0x00 << 6)
#define CTRLR0_TMOD_VALUE(x) (x << 10)
#define CTRLR0_TMOD_TX_AND_RX (0x00 << 10)
#define CTRLR0_TMOD_TX_ONLY (0x01 << 10)
#define CTRLR0_TMOD_RX_ONLY (0x02 << 10)
#define CTRLR0_TMOD_EEPROM_READ (0x03 << 10)
#define CTRLR0_CFS_VALUE(x) (x << 16)
#define CTRLR0_SCPH_MASK (1 << 8)
#define CTRLR0_SCPOL_MASK (1 << 9)
#define CTRLR0_SRL_MASK (1 << 13)
#define CTRLR0_SSTE_MASK (1 << 14)
#define CTRLR0_SPI_FRF_STD (0x00 << 22)
#define CTRLR0_SPI_FRF_DUAL (0x01 << 22)
#define CTRLR0_SPI_FRF_QUAD (0x02 << 22)
/*SSIENR*/
#define SSI_EN ((u8)(1 << 0))
/*SPI_CTRLR0*/
#define CTRLR0_TRANS_TYPE_TT0 (0x00 << 0) // Instruction and Address STD mode
#define CTRLR0_TRANS_TYPE_TT1 (0x01 << 0) // Instruction STD mode and Address CTRLR0.SPI_FRF mode
#define CTRLR0_TRANS_TYPE_TT2 (0x02 << 0) // Instruction and Address CTRLR0.SPI_FRF mode
#define CTRLR0_ADDR_L_VALUE(x) (x << 2)
#define CTRLR0_INST_L_VALUE(x) (x << 8)
#define CTRLR0_WAIT_CYCLES_VALUE(x) (x << 11)
#define CTRLR0_CLK_STRETCH_EN_MASK (1 << 30)
#endif
    typedef struct
    {
        volatile u32 CTRLR0;         // 0x00
        volatile u32 CTRLR1;         // 0x04
        volatile u32 SSIENR;         // 0x08
        volatile u32 MWCR;           // 0x0c
        volatile u32 SER;            // 0x10
        volatile u32 BAUDR;          // 0x14
        volatile u32 TXFTLR;         // 0x18
        volatile u32 RXFTLR;         // 0x1c
        volatile u32 TXFLR;          // 0x20
        volatile u32 RXFLR;          // 0x24
        volatile u32 SR;             // 0x28
        volatile u32 IMR;            // 0x2c
        volatile u32 ISR;            // 0x30
        volatile u32 RISR;           // 0x34
        volatile u32 TXOICR;         // 0x38
        volatile u32 RXOICR;         // 0x3c
        volatile u32 RXUICR;         // 0x40
        volatile u32 MSTICR;         // 0x44
        volatile u32 ICR;            // 0x48
        volatile u32 DMACR;          // 0x4c
        volatile u32 DMATDLR;        // 0x50
        volatile u32 DMARDLR;        // 0x54
        volatile u32 IDR;            // 0x58
        volatile u32 VIDR;           // 0x5c
        volatile u32 DR;             // 0x60
        volatile u32 RESERVERED[35]; // 0x64~0xec
        volatile u32 RXSDR;          // 0xf0
        volatile u32 SPICTRLR0;      // 0xf4
        volatile u32 DDR_DE;         // 0xf8
        volatile u32 XIPMBR;         // 0xfc
        volatile u32 XIPIIR;         // 0x100
        volatile u32 XIPWIR;         // 0x104
        volatile u32 XIPCR;          // 0x108
        volatile u32 XIPSER;         // 0x10C
        volatile u32 XRXIOCR;        // 0x110
        volatile u32 XIPCTTOR;       // 0x114
    } SSI_TypeDef;
#define SSI1 ((SSI_TypeDef *)SPIM1_BASE_ADDR)
#define SSI2 ((SSI_TypeDef *)SPIM2_BASE_ADDR)

    typedef struct
    {
        volatile u16 CR;
        volatile u16 MR;
        volatile u16 CNTR;
        volatile u16 SR;
    } TC_TypeDef;
#define TC ((TC_TypeDef *)(TC_BASE_ADDR))

    typedef struct
    {
        volatile u32 CTRL;   /**< */
        volatile u32 DR;     /**< */
        volatile u32 TMCTRL; /**< */
        volatile u32 STSCR;  /**< */
        volatile u32 GI0SCR; /**< */
        volatile u32 GI1SCR; /**< */
        volatile u32 GI2SCR; /**< */
    } TRNG_TypeDef;
#define TRNG ((TRNG_TypeDef *)TRNG_BASE_ADDR)

#ifdef __MACRO_DEF_UART__
/*UARTCR1*/
#define UART_WOMS_SHIFT_MASK (6)
#define UART_PARITY_ODD ((u8)(1 << 0))             /**<*/
#define UART_PARITY_EVEN ((u8)(0xFE))              /**<*/
#define UART_PARITY_ENABLE ((u8)(1 << 1))          /**<*/
#define UART_PARITY_DISABLE ((u8)(0xFD))           /**<*/
#define UART_IDLE_LINE_AFTER_STOP ((u8)(1 << 2))   /**<*/
#define UART_IDLE_LINE_AFTER_START ((u8)(0xFB))    /**<*/
#define UART_WAKEUP_MODE_ADDRESS ((u8)(1 << 3))    /**<*/
#define UART_WAKEUP_IDLE_LINE ((u8)(0xF7))         /**<*/
#define UART_DATA_FORMAT_MODE_9BITS ((u8)(1 << 4)) /**<*/
#define UART_DATA_FORMAT_MODE_8BITS ((u8)(0xEF))   /**<*/
#define UART_RSCR_TO_TXD ((u8)(1 << 5))            /**<*/
#define UART_RSCR_TRANSMITTER_OUTPUT ((u8)(0xDF))  /**<*/
#define UART_OPEN_DRAIN_MODE ((u8)(1 << 6))        /**<*/
#define UART_CMOS_MODE ((u8)(0xBF))                /**<*/
#define UART_LOOPS_MODE ((u8)(1 << 7))             /**<*/
#define UART_LOOPS_NORMAL_MODE ((u8)(0x7F))        /**<*/
/*UARTCR2*/
#define UART_SBK ((u8)(1 << 0))           /**<*/
#define UART_IT_RWU_ASLEEP ((u8)(1 << 1)) /**<*/
#define UART_RE ((u8)(1 << 2))            /**<*/
#define UART_TE ((u8)(1 << 3))            /**<*/
#define UART_IT_IDLE ((u8)(1 << 4))       /**<*/
#define UART_IT_RIE ((u8)(1 << 5))        /**<*/
#define UART_IT_TCIE ((u8)(1 << 6))       /**<*/
#define UART_IT_TIE ((u8)(1 << 7))        /**<*/
/*UARTSR1*/
#define UART_IT_FLAG_PF ((u8)(1 << 0))   /**<*/
#define UART_IT_FLAG_FE ((u8)(1 << 1))   /**<*/
#define UART_IT_FLAG_NF ((u8)(1 << 2))   /**<*/
#define UART_IT_FLAG_OR ((u8)(1 << 3))   /**<*/
#define UART_IT_FLAG_IDLE ((u8)(1 << 4)) /**<*/
#define UART_IT_FLAG_RDRF ((u8)(1 << 5)) /**<*/
#define UART_IT_FLAG_TC ((u8)(1 << 6))   /**<*/
#define UART_IT_FLAG_TDRE ((u8)(1 << 7)) /**<*/
/*UARTSR2*/
#define UART_IT_FLAG_RAF ((u8)(1 << 0)) /**<*/
/*UARTPUD*/
#define UART_PU_SHIFT_MASK (0)            /**<*/
#define UART_PULLUP_EN ((u8)(1 << 0))     /**<*/
#define UART_PULLUP_DIS ((u8)(0xFE))      /**<*/
#define UART_REDUCE_DRIVE ((u8)(1 << 4))  /**<*/
#define UART_FULL_DRIVE ((u8)(0xEF))      /**<*/
#define UART_DOZE_MODE_DIS ((u8)(1 << 7)) /**<*/
#define UART_DOZE_MODE_EN ((u8)(0x7F))    /**<*/
/*UARTDDR:UART data direction register*/
#define UART_RXD_OUT ((u8)(1 << 0)) /**<*/
#define UART_TXD_OUT ((u8)(1 << 1)) /**<*/
/*UARTIRCR:UART infrared control register*/
#define UART_IR_EN ((u8)(1 << 0))            /**<*/
#define UART_IRSC_SYS_CLK ((u8)(1 << 1))     /**<*/
#define UART_RINV_ACTIVE_HIGH ((u8)(1 << 2)) /**<*/
#define UART_TINV_ACTIVE_HIGH ((u8)(1 << 3)) /**<*/
#define UART_RNUM_MASK ((u8)(0xCF))          /**<*/
#define UART_RNUM_SHIFT_MASK ((u8)(4))       /**<*/
#define UART_RNUM_BITS_MASK ((u8)(0x30))     /**<*/
#define UART_TNUM_MASK ((u8)(0x3F))          /**<*/
#define UART_TNUM_SHIFT_MASK ((u8)(6))       /**<*/
#define UART_TNUM_BITS_MASK ((u8)(0xC0))     /**<*/
/*UARTFCR:UART fifo control register*/
#define UART_TF_EN ((u8)(1 << 6))            /**<*/
#define UART_RF_EN ((u8)(1 << 7))            /**<*/
#define UART_RX_FLSEL_MASK ((u8)(0x38))      /**<*/
#define UART_RX_FLSEL_SHIFT_MASK ((u8)(3))   /**<*/
#define UART_RX_FLSEL_BITS_MASK ((u8)(0xC7)) /**<*/
#define UART_TX_FLSEL_MASK ((u8)(0x07))      /**<*/
#define UART_TX_FLSEL_SHIFT_MASK ((u8)(0))   /**<*/
#define UART_TX_FLSEL_BITS_MASK ((u8)(0xFC)) /**<*/
/*UARTFSR:UART FIFO status register*/
#define UART_FIFO_FLAG_R_EMPTY ((u8)(1 << 0)) /**<*/
#define UART_FIFO_FLAG_R_FULL ((u8)(1 << 1))  /**<*/
#define UART_FIFO_FLAG_T_EMPTY ((u8)(1 << 2)) /**<*/
#define UART_FIFO_FLAG_T_FULL ((u8)(1 << 3))  /**<*/
#define UART_FIFO_FLAG_RTOS ((u8)(1 << 4))    /**<*/
#define UART_FIFO_FLAG_RFTS ((u8)(1 << 5))    /**<*/
#define UART_FIFO_FLAG_FTC ((u8)(1 << 6))     /**<*/
#define UART_FIFO_FLAG_TFTS ((u8)(1 << 7))    /**< */
/*UARTDCR:UART DMA control register*/
#define UART_RX_DMA_EN ((u8)(1 << 0)) /**/
#define UART_TX_DMA_EN ((u8)(1 << 1)) /**/
/*UARTFCR2:UART fifo control register 2*/
#define UART_FIFO_RXF_CLR ((u8)(1 << 0))   /**<*/
#define UART_FIFO_TXF_CLR ((u8)(1 << 1))   /**<*/
#define UART_FIFO_RXF_TO_EN ((u8)(1 << 2)) /**<*/
#define UART_FIFO_RXFTO_IE ((u8)(1 << 3))  /**<*/
#define UART_FIFO_RXFOR_IE ((u8)(1 << 4))  /**<*/
#define UART_FIFO_RXF_IE ((u8)(1 << 5))    /**<*/
#define UART_FIFO_TXFC_IE ((u8)(1 << 6))   /**<*/
#define UART_FIFO_TXF_IE ((u8)(1 << 7))    /**<*/
/*UARTFSR2:UART FIFO status register 2*/
#define UART_FXPF ((u8)(1 << 0)) /**<*/
#define UART_FXFE ((u8)(1 << 1)) /**<*/
#define UART_FXNF ((u8)(1 << 2)) /**<*/
#define UART_FXOR ((u8)(1 << 3))
#endif
    typedef struct
    {
        volatile u8 BDL;       // 0x00
        volatile u8 BDH;       // 0x01
        volatile u8 CR2;       // 0x02
        volatile u8 CR1;       // 0x03
        volatile u8 SR2;       // 0x04
        volatile u8 SR1;       // 0x05
        volatile u8 DRL;       // 0x06
        volatile u8 DRH;       // 0x07
        volatile u8 PORT;      // 0x08
        volatile u8 PURD;      // 0x09
        volatile u8 BRDF;      // 0x0a
        volatile u8 DDR;       // 0x0b
        volatile u8 IRCR;      // 0x0c
        volatile u8 TR;        // 0x0d
        volatile u8 FCR;       // 0x0e
        volatile u8 IRDR;      // 0x0f
        volatile u8 DCR;       // 0x10
        volatile u8 FSR;       // 0x11
        volatile u8 RXTOCTR;   // 0x12
        volatile u8 FCR2;      // 0x13
        volatile u8 RESERVED1; // 0x14
        volatile u8 FSR2;      // 0x15
    } UART_TypeDef;
#define UART1 ((UART_TypeDef *)UART1_BASE_ADDR)
#define UART2 ((UART_TypeDef *)UART2_BASE_ADDR)
#define UART3 ((UART_TypeDef *)UART3_BASE_ADDR)
#define UART4 ((UART_TypeDef *)UART4_BASE_ADDR)

    typedef struct
    {
        volatile u8 BDR;        /**< 00*/
        volatile u8 CR1;        /**< 01*/
        volatile u8 CR2;        /**< 02*/
        volatile u8 SR;         /**< 03*/
        volatile u8 IER;        /**< 04*/
        volatile u8 DR;         /**< 05*/
        volatile u8 WTRH;       /**< 06*/
        volatile u8 WTRM;       /**< 07*/
        volatile u8 WTRL;       /**< 08*/
        volatile u8 GTRH;       /**< 09*/
        volatile u8 GTRL;       /**< 0A*/
        volatile u8 CSR;        /**< 0B*/
        volatile u8 PCR;        /**< 0C*/
        volatile u8 PDR;        /**< 0D*/
        volatile u8 DDR;        /**< 0E*/
        volatile u8 FIFOINTCON; /**< 0F*/
        volatile u8 CRCH;       /**< 10*/
        volatile u8 CRCL;       /**< 11*/
        volatile u8 CDCR;       /**< 12*/
        volatile u8 EBLCR;      /**< 13*/
    } USI_TypeDef;
#define USI3 ((USI_TypeDef *)USI3_BASE_ADDR)

#define WDT_EN (1 << 0)
#define WDT_DEBUG (1 << 1)
#define WDT_DOZE (1 << 2)
#define WDT_WAIT (1 << 3)
    typedef struct
    {
        volatile u16 WCR;
        volatile u16 WMR;
        volatile u16 WCNTR;
        volatile u16 WSR;
    } WDT_TypeDef;
#define WDT ((WDT_TypeDef *)WDT_BASE_ADDR)

#ifdef __MACRO_DEF_PWM__
#define PWM_PPR_CP67_SHIFT 24
#define PWM_PPR_CP45_SHIFT 16
#define PWM_PPR_CP23_SHIFT 8
#define PWM_PPR_CP01_SHIFT 0

#define PWM_DZ_DZ67_SHIFT 24
#define PWM_DZ_DZ45_SHIFT 16
#define PWM_DZ_DZ23_SHIFT 8
#define PWM_DZ_DZ01_SHIFT 0

#define PWM_PCSR_CSR7_SHIFT 28
#define PWM_PCSR_CSR6_SHIFT 24
#define PWM_PCSR_CSR5_SHIFT 20
#define PWM_PCSR_CSR4_SHIFT 16
#define PWM_PCSR_CSR3_SHIFT 12
#define PWM_PCSR_CSR2_SHIFT 8
#define PWM_PCSR_CSR1_SHIFT 4
#define PWM_PCSR_CSR0_SHIFT 0

#define PWM_PCR_CH0EN (1 << 0)
#define PWM_PCR_CH4EN (1 << 1)
#define PWM_PCR_CH0INV (1 << 2)
#define PWM_PCR_CH0MOD (1 << 3)
#define PWM_PCR_DZ0EN (1 << 4)
#define PWM_PCR_DZ1EN (1 << 5)
#define PWM_PCR_CH1EN (1 << 8)
#define PWM_PCR_CH5EN (1 << 9)
#define PWM_PCR_CH1INV (1 << 10)
#define PWM_PCR_CH1MOD (1 << 11)
#define PWM_PCR_CH2EN (1 << 16)
#define PWM_PCR_CH6EN (1 << 17)
#define PWM_PCR_CH2INV (1 << 18)
#define PWM_PCR_CH2MOD (1 << 19)
#define PWM_PCR_CH3EN (1 << 24)
#define PWM_PCR_CH7EN (1 << 25)
#define PWM_PCR_CH3INV (1 << 26)
#define PWM_PCR_CH3MOD (1 << 27)

#define PWM_PCR1_CH4INV (1 << 2)
#define PWM_PCR1_CH4MOD (1 << 3)
#define PWM_PCR1_DZ2EN (1 << 4)
#define PWM_PCR1_DZ3EN (1 << 5)
#define PWM_PCR1_CH5INV (1 << 10)
#define PWM_PCR1_CH5MOD (1 << 11)
#define PWM_PCR1_CH6INV (1 << 18)
#define PWM_PCR1_CH6MOD (1 << 19)
#define PWM_PCR1_CH7INV (1 << 26)
#define PWM_PCR1_CH7MOD (1 << 27)

#define PWM_PIER_CH0INT_EN (1 << 0)
#define PWM_PIER_CH1INT_EN (1 << 1)
#define PWM_PIER_CH2INT_EN (1 << 2)
#define PWM_PIER_CH3INT_EN (1 << 3)
#define PWM_PIER_CH4INT_EN (1 << 4)
#define PWM_PIER_CH5INT_EN (1 << 5)
#define PWM_PIER_CH6INT_EN (1 << 6)
#define PWM_PIER_CH7INT_EN (1 << 7)

#define PWM_PIFR_CH0INT (1 << 0)
#define PWM_PIFR_CH1INT (1 << 1)
#define PWM_PIFR_CH2INT (1 << 2)
#define PWM_PIFR_CH3INT (1 << 3)
#define PWM_PIFR_CH4INT (1 << 4)
#define PWM_PIFR_CH5INT (1 << 5)
#define PWM_PIFR_CH6INT (1 << 6)
#define PWM_PIFR_CH7INT (1 << 7)

#define PWM_PCCR0_CH0_INV (1 << 0)
#define PWM_PCCR0_CH0_RLINT (1 << 1)
#define PWM_PCCR0_CH0_FLINT (1 << 2)
#define PWM_PCCR0_CH0_CAPEN (1 << 3)
#define PWM_PCCR0_CH0_CAPIF (1 << 4)
#define PWM_PCCR0_CH0_CRLRD (1 << 6)
#define PWM_PCCR0_CH0_CFLRD (1 << 7)
#define PWM_PCCR0_CH1_INV (1 << 16)
#define PWM_PCCR0_CH1_RLINT (1 << 17)
#define PWM_PCCR0_CH1_FLINT (1 << 18)
#define PWM_PCCR0_CH1_CAPEN (1 << 19)
#define PWM_PCCR0_CH1_CAPIF (1 << 20)
#define PWM_PCCR0_CH1_CRLRD (1 << 22)
#define PWM_PCCR0_CH1_CFLRD (1 << 23)

#define PWM_PCCR1_CH2_INV (1 << 0)
#define PWM_PCCR1_CH2_RLINT (1 << 1)
#define PWM_PCCR1_CH2_FLINT (1 << 2)
#define PWM_PCCR1_CH2_CAPEN (1 << 3)
#define PWM_PCCR1_CH2_CAPIF (1 << 4)
#define PWM_PCCR1_CH2_CRLRD (1 << 6)
#define PWM_PCCR1_CH2_CFLRD (1 << 7)
#define PWM_PCCR1_CH3_INV (1 << 16)
#define PWM_PCCR1_CH3_RLINT (1 << 17)
#define PWM_PCCR1_CH3_FLINT (1 << 18)
#define PWM_PCCR1_CH3_CAPEN (1 << 19)
#define PWM_PCCR1_CH3_CAPIF (1 << 20)
#define PWM_PCCR1_CH3_CRLRD (1 << 22)
#define PWM_PCCR1_CH3_CFLRD (1 << 23)

#define PWM_PCCR2_CH4_INV (1 << 0)
#define PWM_PCCR2_CH4_RLINT (1 << 1)
#define PWM_PCCR2_CH4_FLINT (1 << 2)
#define PWM_PCCR2_CH4_CAPEN (1 << 3)
#define PWM_PCCR2_CH4_CAPIF (1 << 4)
#define PWM_PCCR2_CH4_CRLRD (1 << 6)
#define PWM_PCCR2_CH4_CFLRD (1 << 7)
#define PWM_PCCR2_CH5_INV (1 << 16)
#define PWM_PCCR2_CH5_RLINT (1 << 17)
#define PWM_PCCR2_CH5_FLINT (1 << 18)
#define PWM_PCCR2_CH5_CAPEN (1 << 19)
#define PWM_PCCR2_CH5_CAPIF (1 << 20)
#define PWM_PCCR2_CH5_CRLRD (1 << 22)
#define PWM_PCCR2_CH5_CFLRD (1 << 23)

#define PWM_PCCR3_CH6_INV (1 << 0)
#define PWM_PCCR3_CH6_RLINT (1 << 1)
#define PWM_PCCR3_CH6_FLINT (1 << 2)
#define PWM_PCCR3_CH6_CAPEN (1 << 3)
#define PWM_PCCR3_CH6_CAPIF (1 << 4)
#define PWM_PCCR3_CH6_CRLRD (1 << 6)
#define PWM_PCCR3_CH6_CFLRD (1 << 7)
#define PWM_PCCR3_CH7_INV (1 << 16)
#define PWM_PCCR3_CH7_RLINT (1 << 17)
#define PWM_PCCR3_CH7_FLINT (1 << 18)
#define PWM_PCCR3_CH7_CAPEN (1 << 19)
#define PWM_PCCR3_CH7_CAPIF (1 << 20)
#define PWM_PCCR3_CH7_CRLRD (1 << 22)
#define PWM_PCCR3_CH7_CFLRD (1 << 23)

#define PWM_PPCR_CH0_BITSET (1 << 0)
#define PWM_PPCR_CH1_BITSET (1 << 1)
#define PWM_PPCR_CH2_BITSET (1 << 2)
#define PWM_PPCR_CH3_BITSET (1 << 3)
#define PWM_PPCR_CH4_BITSET (1 << 4)
#define PWM_PPCR_CH5_BITSET (1 << 5)
#define PWM_PPCR_CH6_BITSET (1 << 6)
#define PWM_PPCR_CH7_BITSET (1 << 7)
#define PWM_PPCR_CH0_PULLUP (1 << 8)
#define PWM_PPCR_CH1_PULLUP (1 << 9)
#define PWM_PPCR_CH2_PULLUP (1 << 10)
#define PWM_PPCR_CH3_PULLUP (1 << 11)
#define PWM_PPCR_CH4_PULLUP (1 << 12)
#define PWM_PPCR_CH5_PULLUP (1 << 13)
#define PWM_PPCR_CH6_PULLUP (1 << 14)
#define PWM_PPCR_CH7_PULLUP (1 << 15)
#define PWM_PPCR_CH0_OUTPUT (1 << 16)
#define PWM_PPCR_CH1_OUTPUT (1 << 17)
#define PWM_PPCR_CH2_OUTPUT (1 << 18)
#define PWM_PPCR_CH3_OUTPUT (1 << 19)
#define PWM_PPCR_CH4_OUTPUT (1 << 20)
#define PWM_PPCR_CH5_OUTPUT (1 << 21)
#define PWM_PPCR_CH6_OUTPUT (1 << 22)
#define PWM_PPCR_CH7_OUTPUT (1 << 23)
#endif
    typedef struct
    {
        volatile u32 PPR;    /**< PWM Pre-scale Register 							*/
        volatile u32 DZ;     /**< PWM DeadZone Register								*/
        volatile u32 PCSR;   /**< PWM Clock Select Register						*/
        volatile u32 PCR;    /**< PWM Control Register 								*/
        volatile u32 PCR1;   /**< PWM Control Register 1 							*/
        volatile u32 PCNR0;  /**< PWM Counter Register0								*/
        volatile u32 PCMR0;  /**< PWM Comparator Register0 						*/
        volatile u32 PTR0;   /**< PWM Timer Register0									*/
        volatile u32 PCNR1;  /**< PWM Counter Register1								*/
        volatile u32 PCMR1;  /**< PWM Comparator Register1 						*/
        volatile u32 PTR1;   /**< PWM Timer Register1									*/
        volatile u32 PCNR2;  /**< PWM Counter Register2								*/
        volatile u32 PCMR2;  /**< PWM Comparator Register2 						*/
        volatile u32 PTR2;   /**< PWM Timer Register2									*/
        volatile u32 PCNR3;  /**< PWM Counter Register3								*/
        volatile u32 PCMR3;  /**< PWM Comparator Register3 						*/
        volatile u32 PTR3;   /**< PWM Timer Register3									*/
        volatile u32 PIER;   /**< PWM Interrupt Enable Register				*/
        volatile u32 PIFR;   /**< PWM Interrupt FlagRegister 					*/
        volatile u32 PCCR0;  /**< PWM Capture Control Register0				*/
        volatile u32 PCCR1;  /**< PWM Capture Control Register1				*/
        volatile u32 PCCR2;  /**< PWM Capture Control Register2				*/
        volatile u32 PCCR3;  /**< PWM Capture Control Register3				*/
        volatile u32 PCRLR0; /**< PWM Capture Rising Latch Register0 	*/
        volatile u32 PCFLR0; /**< PWM Capture Falling Latch Register0	*/
        volatile u32 PCRLR1; /**< PWM Capture Rising Latch Register1 	*/
        volatile u32 PCFLR1; /**< PWM Capture Falling Latch Register1	*/
        volatile u32 PCRLR2; /**< PWM Capture Rising Latch Register2 	*/
        volatile u32 PCFLR2; /**< PWM Capture Falling Latch Register2	*/
        volatile u32 PCRLR3; /**< PWM Capture Rising Latch Register3 	*/
        volatile u32 PCFLR3; /**< PWM Capture Falling Latch Register3	*/
        volatile u32 PPCR;   /**< PWM Port Control Register						*/
        volatile u32 PDCR0;  /**< PWM Delay Count Register 0 					*/
        volatile u32 PDCR1;  /**< PWM Delay Count Register 1 					*/
        volatile u32 PDCR2;  /**< PWM Delay Count Register 2 					*/
        volatile u32 PDCR3;  /**< PWM Delay Count Register 3 					*/
        volatile u32 PDCR4;  /**< PWM Delay Count Register 4 					*/
        volatile u32 PDCR5;  /**< PWM Delay Count Register 5 					*/
        volatile u32 PDCR6;  /**< PWM Delay Count Register 6 					*/
        volatile u32 PDCR7;  /**< PWM Delay Count Register 7 					*/
        volatile u32 PDER;   /**< PWM Delay Enable Register						*/
    } PWM_TypeDef;
#define PWM ((PWM_TypeDef *)(PWM_BASE_ADDR))

    typedef struct
    {
        volatile u32 SCONR;              // 0x00
        volatile u32 STMG0R;             // 0x04
        volatile u32 STMG1R;             // 0x08
        volatile u32 SCTLR;              // 0x0c
        volatile u32 SREFR;              // 0x10
        volatile u32 RESERVED_14[8 * 4]; // 0x14 - 0x90
        volatile u32 SMTMGR_SET0;        // 0x94
        volatile u32 SMTMGR_SET1;        // 0x98
        volatile u32 SMTMGR_SET2;        // 0x9C
        volatile u32 RESERVED_A0;        // 0xA0
        volatile u32 SMCTLR;             // 0xA4
        volatile u32 RESERVED_A8;        // 0xA8
        volatile u32 EXN_MODE_REG;       // 0xAC

    } SDRAM_TypeDef, SDRAM2LCD_TypeDef;
#define SDRAM ((SDRAM_TypeDef *)(SDRAM_BASE_ADDR))
#define SDRAM2LCD ((SDRAM2LCD_TypeDef *)(SDRAM2LCD_BASE_ADDR))

#ifdef __MACRO_DEF_CLCD__
// TIMING0, horizontal axis panel control register
#define HBP_OFF (24)
#define HFP_OFF (16)
#define HSW_OFF (8)
#define PPL_OFF (2)

#define HBP_MASK (0xff000000)
#define HFP_MASK (0x00ff0000)
#define HSW_MASK (0x0000ff00)
#define PPL_MASK (0x000000fc)

// TIMING1, vertical axis panel control register
#define VBP_OFF (24)
#define VFP_OFF (16)
#define VSW_OFF (10)
#define LPP_OFF (0)

#define VBP_MASK (0xff000000)
#define VFP_MASK (0x00ff0000)
#define VSW_MASK (0x0000fc00)
#define LPP_MASK (0x000003ff)

// TIMING2, clock and signal polarity control register
#define PCD_HI_OFF (27)
#define CPL_OFF (16)
#define ACB_OFF (6)
#define PCD_LO_OFF (0)

#define PCD_HI_MASK (0xf8000000)
#define BCD (1 << 26)
#define CPL_MASK (0x03ff0000)
#define IEO (1 << 14)
#define IPC (1 << 13)
#define IHS (1 << 12)
#define IVS (1 << 11)
#define ACB_MASK (0x000007c0)
#define PCD_LO_MASK (0x0000001f)

// TIMING3, line end control register
#define LED_OFF (0)

#define LEE (1 << 16)
#define LED_MASK (0x0000007f)

// UPBASE & LPBASE, upper & lower panel frame base addr register
//#define BASE_ADDR_MASK		(0xfffffffc)//word(32bit) aligned

// LCD_IMSC, interrupt mask set/clear register
// LCD_RIS, raw interrupt status register
// LCD_MIS, masked interrupt status register
// LCD_ICR, interrupt clear register
#define MBERR (1 << 4)
#define VCOMP (1 << 3)
#define LNBU (1 << 2)
#define FUF (1 << 1)

// control register
#define LCD_VCOMP_OFF (12)
#define LCDBPP_OFF (1)

#define WATERMARK (1 << 16)
#define RGB565 (1 << 15)
#define GAMMA_CORR (1 << 14)
#define LCD_VCOMP_MASK (0x00003000)
#define LCDPWR (1 << 11)
#define BEPO (1 << 10)
#define BEBO (1 << 9)
#define BGR (1 << 8)
#define LCDDUAL (1 << 7)
#define LCDMONO8 (1 << 6)
#define LCDTFT (1 << 5)
#define LCDBW (1 << 4)
#define LCDBPP_MASK (0x0000000e)
#define LCDEN (1 << 0)

// PALETTE, color palette register
#define B_H_OFF (26)
#define G_H_OFF (21)
#define R_H_OFF (16)
#define B_L_OFF (10)
#define G_L_OFF (5)
#define R_L_OFF (0)

#define I_H (1 << 31)
#define B_H_MASK (0x7c000000)
#define G_H_MASK (0x03e00000)
#define R_H_MASK (0x001f0000)
#define I_L (1 << 15)
#define B_L_MASK (0x00007c00)
#define G_L_MASK (0x000003e0)
#define R_L_MASK (0x0000001f)

// GAMMA, gamma correction register
#define GM_CORR_B_OFF (16)
#define GM_CORR_G_OFF (8)
#define GM_CORR_R_OFF (0)

#define GM_CORR_B_MASK (0x00ff0000)
#define GM_CORR_G_MASK (0x0000ff00)
#define GM_CORR_R_MASK (0x000000ff)

// Values which can be ORed together within the flags field of the
// LCDTiming_TypeDef structure.
#define LCD_TIMING_ACTIVE_HIGH_OE (0x00000000)
#define LCD_TIMING_ACTIVE_LOW_OE (IEO)
#define LCD_TIMING_ACTIVE_HIGH_PIXCLK (0x00000000)
#define LCD_TIMING_ACTIVE_LOW_PIXCLK (IPC)
#define LCD_TIMING_ACTIVE_HIGH_HSYNC (0x00000000)
#define LCD_TIMING_ACTIVE_LOW_HSYNC (IHS)
#define LCD_TIMING_ACTIVE_HIGH_VSYNC (0x00000000)
#define LCD_TIMING_ACTIVE_LOW_VSYNC (IVS)

// Values used to construct the config parameter to
// Valid parameters contain one of the LCD_FMT_xxx
// labels optionally ORed with the other flags.
#define LCD_FMT_GAMMA_CORR_ENABLE (GAMMA_CORR)
#define LCD_FMT_GAMMA_CORR_DISABLE (0x00000000)
#define LCD_FMT_OUTPUT_RGB (0x00000000)
#define LCD_FMT_OUTPUT_BGR (BGR)
#define LCD_FMT_INPUT_LITTLE_ENDIAN (0x00000000)
#define LCD_FMT_INPUT_BIG_ENDIAN (BEPO | BEBO)
#define LCD_FMT_INPUT_WINCE_MODE (BEPO)
#define LCD_FMT_INPUT_PALETTE_1BIT (0x00000000)
#define LCD_FMT_INPUT_PALETTE_2BIT (0x00000002)
#define LCD_FMT_INPUT_PALETTE_4BIT (0x00000004)
#define LCD_FMT_INPUT_PALETTE_8BIT (0x00000006)
#define LCD_FMT_PALETTE_FORMAT_RGB565 (RGB565)
#define LCD_FMT_PALETTE_FORMAT_RGB666 (0x00000000)
#define LCD_FMT_INPUT_RGB555 (0x00000008)
#define LCD_FMT_INPUT_RGB565 (0x00000008 | RGB565)
#define LCD_FMT_INPUT_RGB666 (0x00000008)
#define LCD_FMT_INPUT_RGB888 (0x0000000a)
#define LCD_FMT_MASK_ALL (0x0000c70e)
#endif
    typedef struct
    {
        volatile u32 LCD_TIMING0;         // 0x000, vertical axis panel control register
        volatile u32 LCD_TIMING1;         // 0x004, vertical axis panel control register
        volatile u32 LCD_TIMING2;         // 0x008, clock and signal polarity control register
        volatile u32 LCD_TIMING3;         // 0x00c, line end control register
        volatile u32 LCD_UPBASE;          // 0x010, upper panel frame base addr register
        volatile u32 LCD_LPBASE;          // 0x014, lower panel frame base addr register
        volatile u32 LCD_IMSC;            // 0x018, interrupt mask set/clear register
        volatile u32 LCD_CONTROL;         // 0x01c, control register
        volatile u32 LCD_RIS;             // 0x020, raw interrupt status register
        volatile u32 LCD_MIS;             // 0x024, masked interrupt status register
        volatile u32 LCD_ICR;             // 0x028, interrupt clear register
        volatile u32 LCD_UPCURR;          // 0x02c, upper panel current addr register
        volatile u32 LCD_LPCURR;          // 0x030, lower panel current addr register
        volatile u32 RSVD[115];           // 0x034 ~ 0x1ff
        volatile u32 LCD_PALETTE[128];    // 0x200 ~ 0x3ff, color palette register
        volatile u32 LCD_GAMMA_CORR[256]; // 0x400 ~ 0x7ff, gamma correction register
    } CLCD_TypeDef;
#define CLCD ((CLCD_TypeDef *)(CLCD_BASE_ADDR))

#ifdef __MACRO_DEF_USB__
#define USB_POWER_ENAB_SUSP (1 << 0)
#define USB_POWER_SUSP_MODE (1 << 1)
#define USB_POWER_RESUME (1 << 2)
#define USB_POWER_RESET (1 << 3)
#define USB_POWER_HS_MODE (1 << 4)
#define USB_POWER_HS_ENAB (1 << 5)
#define USB_POWER_SOFT_CONN (1 << 6)
#define USB_POWER_ISO_UPDATE (1 << 7)

// usb common interrupt number
#define USB_INTERRUPT_SUSPEND (1 << 0)
#define USB_INTERRUPT_RESUME (1 << 1)
#define USB_INTERRUPT_RESET (1 << 2)
#define USB_INTERRUPT_SOF (1 << 3)
#define USB_INTERRUPT_CONNECT (1 << 4)
#define USB_INTERRUPT_DISCON (1 << 5)
#define USB_INTERRUPT_SESSREQ (1 << 6)
#define USB_INTERRUPT_VBUSERR (1 << 7)

#define USB_TESTMODE_SE0NAK (1 << 0)
#define USB_TESTMODE_TESTJ (1 << 1)
#define USB_TESTMODE_TESTK (1 << 2)
#define USB_TESTMODE_TESTPACKET (1 << 3)

// usb tx interrupt number
#define USB_INTERRUPT_EP0 (1 << 0)
#define USB_TX_INTERRUPT_EP1 (1 << 1)
#define USB_TX_INTERRUPT_EP2 (1 << 2)
#define USB_TX_INTERRUPT_EP3 (1 << 3)
#define USB_TX_INTERRUPT_EP4 (1 << 4)
#define USB_TX_INTERRUPT_EP5 (1 << 5)
#define USB_TX_INTERRUPT_EP6 (1 << 6)
#define USB_TX_INTERRUPT_EP7 (1 << 7)

// Usb Rx Interrupt Number
#define USB_RX_INTERRUPT_EP0 (1 << 0)
#define USB_RX_INTERRUPT_EP1 (1 << 1)
#define USB_RX_INTERRUPT_EP2 (1 << 2)
#define USB_RX_INTERRUPT_EP3 (1 << 3)
#define USB_RX_INTERRUPT_EP4 (1 << 4)
#define USB_RX_INTERRUPT_EP5 (1 << 5)
#define USB_RX_INTERRUPT_EP6 (1 << 6)
#define USB_RX_INTERRUPT_EP7 (1 << 7)
// Device CSR0 Bit Define
#define DEV_CSR0_RXPKTRDY (1 << 0)
#define DEV_CSR0_TXPKTRDY (1 << 1)
#define DEV_CSR0_SENTSTALL (1 << 2)
#define DEV_CSR0_DATAEND (1 << 3)
#define DEV_CSR0_SETUPEND (1 << 4)
#define DEV_CSR0_SENDSTALL (1 << 5)
#define DEV_CSR0_SERVICE_RXPKTRDY (1 << 6)
#define DEV_CSR0_SERVICE_SETUPEND (1 << 7)

// TX Register Bit Low as Device
#define DEV_TXCSR_TXPKTRDY (1 << 0)
#define DEV_TXCSR_FIFO_NOT_EMPTY (1 << 1)
#define DEV_TXCSR_UNDER_RUN (1 << 2)
#define DEV_TXCSR_FLUSH_FIFO (1 << 3)
#define DEV_TXCSR_SEND_STALL (1 << 4)
#define DEV_TXCSR_SENT_SATLL (1 << 5)
#define DEV_TXCSR_CLR_DATA_TOG (1 << 6)
#define DEV_TXCSR_INCOMP_TX (1 << 7)

// TX Register Bit High as Device
#define DEV_TXCSR_DMAMODE (1 << 2)
#define DEV_TXCSR_FRC_DATA_TOG (1 << 3)
#define DEV_TXCSR_DMA_ENAB (1 << 4)
#define DEV_TXCSR_TXMODE (1 << 5)
#define DEV_TXCSR_ISO (1 << 6)
#define DEV_TXCSR_AUTO_SET (1 << 7)

// RX Register Bit Low as Device
#define DEV_RXCSR_RXPKTRDY (1 << 0)
#define DEV_RXCSR_FIFOFULL (1 << 1)
#define DEV_RXCSR_FLUSH_FIFO (1 << 4)
#define DEV_RXCSR_SEND_STALL (1 << 5)
#define DEV_RXCSR_SENT_STALL (1 << 6)
#define DEV_RXCSR_CLR_DATA_TOG (1 << 7)

// RX Register Bit High as Device
#define DEV_RXCSR_INCOMP_RX (1 << 0)
#define DEV_RXCSR_DMAMODE (1 << 3)
#define DEV_RXCSR_DISNYET (1 << 4)
#define DEV_RXCSR_DMA_ENAB (1 << 5)
#define DEV_RXCSR_ISO (1 << 6)
#define DEV_RXCSR_AUTOCLEAR (1 << 7)

#define DEV_INTR_CHANNEL(n) (1 << (n - 1))

// dma cntl
#define DEV_CNTL_DMAEN (1 << 0)
#define DEV_CNTL_DIRECTION_READ (1 << 1)
#define DEV_CNTL_DMAMODE (1 << 2)
#define DEV_CNTL_INTERE (1 << 3)
#define DEV_CNTL_EP(x) ((x & 0x07) << 4)
#define DEV_CNTL_BUSERROR (1 << 8)
#define DEV_CNTL_BURSTMODE(x) ((x & 0x03) << 9)
#endif
    typedef struct
    {
        volatile u8 FADDRR; /**<Function address register*/
        volatile u8 UCSR;   /**<USB control and status register*/

        volatile u8 INTRTX_L; /**<Low byte of Interrupt register for Endpoint0 and Tx Endpoint*/
        volatile u8 INTRTX_H; /**<High byte of Interrupt register for Endpoint0 and Tx Endpoint*/

        volatile u8 INTRRX_L; /**<Low byte of Interrupt register for Rx Endpoint*/
        volatile u8 INTRRX_H; /**<High byte of Interrupt register for Rx Endpoint*/

        volatile u8 INTRTXE_L; /**<Low byte of Interrupt enable register for IntrTx*/
        volatile u8 INTRTXE_H; /**<High byte of Interrupt enable register for IntrTx*/

        volatile u8 INTRRXE_L; /**<Low byte of Interrupt enable register for IntrRx*/
        volatile u8 INTRRXE_H; /**<High byte of Interrupt enable register for IntrRx*/

        volatile u8 INTRUSB;  /**<Interrupt register for common USB interrupts*/
        volatile u8 INTRUSBE; /**<Interrupt enable register for IntrUSB*/

        volatile u8 FNUMR_L; /**<Low byte of Frame number*/
        volatile u8 FNUMR_H; /**<High byte of Frame number*/

        volatile u8 EINDEX;  /**<Index register for selecting the endpoint status and control register*/
        volatile u8 TSTMODE; /**<Enables the USB test modes*/
    } USBCCOMMON_TypeDef;
    typedef struct
    {
        volatile u8 TXMAXP_L; /**<Low byte of Maximum packet size for peripheral Tx endpoint*/
        volatile u8 TXMAXP_H; /**<High byte of Maximum packet size for peripheral Tx endpoint*/
        union
        {
            volatile u8 E0CSR_L; /**<Low byte of Control Status register for Endpoint0*/
            volatile u8 TXCSR_L; /**<Low byte of Control Status register for peripheral Tx endpoint*/
        };
        union
        {
            volatile u8 E0CSR_H; /**<High byte of Control Status register for Endpoint0*/
            volatile u8 TXCSR_H; /**<High byte of Control Status register for peripheral Tx endpoint*/
        };
        volatile u8 RXMAXP_L; /**<Low byte of Maximum packet size for peripheral Rx endpoint*/
        volatile u8 RXMAXP_H; /**<High byte of Maximum packet size for peripheral Rx endpoint*/

        volatile u8 RXCSR_L; /**<Low byte of Control Status register for peripheral Rx endpoint*/
        volatile u8 RXCSR_H; /**<High byte of Control Status register for peripheral Rx endpoint*/
        union
        {
            volatile u8 E0COUNTR_L; /**<Low byte of Number of received bytes in Endpoint0 FIFO*/
            volatile u8 RXCOUNTR_L; /**<Low byte of Number of bytes in peripheral Rx endpoint FIFO*/
        };
        union
        {
            volatile u8 E0COUNTR_H; /**<High byte of Number of received bytes in Endpoint0 FIFO*/
            volatile u8 RXCOUNTR_H; /**<High byte of Number of bytes in peripheral Rx endpoint FIFO*/
        };
        volatile u8 TXTYPE;
        union
        {
            volatile u8 NAKLIMIT0;
            volatile u8 TXINTERVAL;
        };
    } USBCINDEXED_TypeDef;
    typedef struct
    {
        volatile u8 OTGCTRL;
        volatile u8 RESERVED;
        volatile u8 TXFIFOSZ;     /**<Tx Endpoint FIFO size,double buffer only set in one register(TX_fifosz/RX_fifosz)*/
        volatile u8 RXFIFOSZ;     /**<Rx Endpoint FIFO size,MAX FIFO size is 1024byte*/
        volatile u8 TX_fifoadd_L; /**<Tx Endpoint FIFO address(Low 8bit)*/
        volatile u8 TX_fifoadd_H; /**<Tx Endpoint FIFO address(High 8bit)*/
        volatile u8 RX_fifoadd_L; /**<Rx Endpoint FIFO address(Low 8bit)*/
        volatile u8 RX_fifoadd_H; /**<Rx Endpoint FIFO address(High 8bit)*/
    } USBCFIFOCFG_TypeDef;
    typedef struct
    {
        volatile u32 DMA_CNTL;
        volatile u32 DMA_ADDR;
        volatile u32 DMA_COUNT;
        volatile u32 RESERVED;
    } DMAReg_TypeDef;
    typedef struct
    {
        volatile u32 DMA_INTR;
        DMAReg_TypeDef USB_DMAReg[7];
    } USBCDMACFG_TypeDef;
    typedef struct
    {
        volatile u32 FIFO_ENDPOINTx[8];
    } USBCFIFOREG_TypeDef;

#define gUSBC_CommonReg ((USBCCOMMON_TypeDef *)(USBC_BASE_ADDR))
#define gUSBC_IndexReg ((USBCINDEXED_TypeDef *)USBC_INDEXED_ADDR)
#define gUSBC_ControlReg ((USBCFIFOCFG_TypeDef *)USBC_FIFOCFG_ADDR)
#define gUSBC_FIFOReg ((USBCFIFOREG_TypeDef *)USBC_FIFOREG_ADDR)
#define gUSBC_DMAReg ((USBCDMACFG_TypeDef *)USBC_DMACCFG_ADDR)

#define USBC_INDEXED_ADDR (USBC_BASE_ADDR + 0x10)
#define USBC_FIFOCFG_ADDR (USBC_BASE_ADDR + 0x60)
#define USBC_FIFOREG_ADDR (USBC_BASE_ADDR + 0x20)
#define USBC_DMACCFG_ADDR (USBC_BASE_ADDR + 0x200)

#ifdef __cplusplus
}
#endif

#endif /* __LT776_REG_H__*/
