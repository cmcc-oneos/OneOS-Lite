/**
 ******************************************************************************
            Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                    All Rights Reserved
******************************************************************************
* @file    ssi_drv.h
* @author  Product application department
* @version V1.0
* @date    2021.11.01
* @brief   Header file of SSI DRV module.
*
******************************************************************************
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SSI_DRV_H
#define __SSI_DRV_H
#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"
#include "type.h"
#include "cpm_drv.h"
//#include "delay.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
 *
 *@{
 */

/** @defgroup DRV_SPI SPI
 *
 *@{
 */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_SPI_Exported_Macros Exported Macros
 *
 * @{
 */
#endif

//--------------------------------------------
#define SPI4
//--------------------------------------------
#ifdef SPI4
#define SSI (SSI_TypeDef *)(0x60000000)
#endif

#ifdef SPI5
//#define SSI              (SSI_TypeDef *)(0x70000000)
//#define IOCTRL_SWAPCR 	 (*(volatile unsigned int *)(0x40000000+0x28))
//#define CS_SWAP_ON       (IOCTRL_SWAPCR |= (0x01<<24))
//#define CS_SWAP_OFF      (IOCTRL_SWAPCR &= ~(0x01<<24))
//#define EPORT12_EPDDR 	 (*(volatile unsigned char *)(0x4002e000+0x03))
//#define EPORT12_EPDR 		 (*(volatile unsigned char *)(0x4002e000+0x05))
//#define CS_OUT           (EPORT12_EPDDR|=  (1<<0))
//#define CS_ON            (EPORT12_EPDR |=  (1<<0))
//#define CS_OFF           (EPORT12_EPDR &= ~(1<<0))
//#define CS_INIT          { CS_SWAP_ON;CS_OUT; }
//#define CS_QUIT          { CS_SWAP_OFF;}
//#define CS_LOW           { CS_OFF; }
//#define CS_HIGH          { CS_ON; }
#endif

#define DMA_IE (0x01)

#define DMA_CH0_MASK 0x01
#define DMA_CH2_MASK 0x02
#define DMA_CH3_MASK 0x04
#define DMA_CH4_MASK 0x08
#define DMA_CH(n) (n)

#define DI (0x0) // increment
#define SI (0x0)

#define EPORT11_EPDDR (*(volatile unsigned char *)(0x4002d000 + 0x03))
#define EPORT11_EPDR (*(volatile unsigned char *)(0x4002d000 + 0x05))
#define IOCTRL_SWAPCR (*(volatile unsigned int *)(0x40000000 + 0x28))
#define CS_SWAP_ON (IOCTRL_SWAPCR |= 0x01)
#define CS_ON (EPORT11_EPDR |= (1 << 0))
#define CS_OFF (EPORT11_EPDR &= ~(1 << 0))
#define CS_OUT (EPORT11_EPDDR |= (1 << 0))
#define SSI_ENABLE (0x01)
#define SSI_DISABLE (0x00)
#define DFS(n) (n - 1) // Data Frame Size  n>=4 && n<=32   generally  n=8
#define SIZE_8_BIT DFS(8)
#define NDF_STD(n) (n) // Number of Data Frames
#define RFT(n) (n)
#define RSD(n) (n)
#define STA_BAUDR (0x04) /*必须为偶数，且最小值为2*/
#define TXFTHR(n) (n << 16)
#define TFT(n) (n)
#define DISABLE (0)
#define NONE (0)
#define CS_LOW  \
{           \
    CS_OFF; \
}
#define CS_HIGH \
{           \
    CS_ON;  \
}
#define CS_INIT     \
{               \
    CS_SWAP_ON; \
    CS_OUT;     \
}

#define SRC_HARD ~(0x1 << 11)
#define SRC_SOFT (0x1 << 11)
#define DST_HARD ~(0x1 << 10)
#define DST_SOFT (0x1 << 10)
#define DD (0x1 << 7)
#define SD (0x1 << 9)
#define DNC (0x2 << 7)
#define SNC (0x2 << 9)

#define CH0_MASK 0x0
#define CH0_UMASK (0x1 << 8 | 0x1)
#define CH0_MASK 0x0
#define CH1_UMASK (0x2 << 8 | 0x2)

#define DST_PER_DAC (0x08 << 11)

// config data for DMACCONFIG
#define DMACEN 0x1
// config data for CHENREG
#define CH0_EN 0x1 << 8 | 0x1
#define CH1_EN 0x2 << 8 | 0x2
#define CH2_EN 0x4 << 8 | 0x4
#define CH3_EN 0x8 << 8 | 0x8

// config data for DMACCxCTRL
#define INTEN 0x1

#define DWIDTH_B 0x0 << 1
#define DWIDTH_HW 0x1 << 1
#define DWIDTH_W 0x2 << 1
#define DWIDTH_DW 0x3 << 1
#define SWIDTH_B 0x0 << 4
#define SWIDTH_HW 0x1 << 4
#define SWIDTH_W 0x2 << 4
#define SWIDTH_DW 0x2 << 4

#define DIEC 0x0 << 7
#define DDEC 0x1 << 7
#define DNOCHG 0x2 << 7
#define SIEC 0x0 << 9
#define SDEC 0x1 << 9
#define SNOCHG 0x2 << 9

#define DBSIZE_4 0x1 << 11
#define DBSIZE_8 0x2 << 11
#define DBSIZE_16 0x3 << 11
#define DBSIZE_32 0x4 << 11
#define DBSIZE_64 0x5 << 11
#define DBSIZE_128 0x6 << 11
#define DBSIZE_256 0x7 << 11

#define SBSIZE_4 0x1 << 14
#define SBSIZE_8 0x2 << 14
#define SBSIZE_16 0x3 << 14
#define SBSIZE_32 0x4 << 14
#define SBSIZE_64 0x5 << 14
#define SBSIZE_128 0x6 << 14
#define SBSIZE_256 0x7 << 14

#define SRC_GATHER_EN 0x1 << 17
#define DST_SCATTER_EN 0x1 << 18

#define M2M_DMA 0x0 << 20
#define M2P_DMA 0x1 << 20
#define P2M_DMA 0x2 << 20
#define P2P_DMA 0x3 << 20
#define P2M_PER 0x4 << 20
#define P2P_SRC 0x5 << 20
#define M2P_PER 0x6 << 20
#define P2P_DST 0x7 << 20

#define LLP_SRC_EN 0x1 << 28
#define LLP_DST_EN 0x1 << 27

// config data for DMACCxCFG
#define CH_PRIOR0 0x0 << 5 // 0 is lowest, 3 is highest
#define CH_PRIOR1 0x1 << 5
#define CH_PRIOR2 0x2 << 5
#define CH_PRIOR3 0x3 << 5

#define CH_SUSP 0x1 << 8

#define FIFO_EMPTY 0x1 << 9

#define HS_SEL_SRC_HARD 0x0 << 11
#define HS_SEL_SRC_SOFT 0x1 << 11
#define HS_SEL_DST_HARD 0x0 << 10
#define HS_SEL_DST_SOFT 0x1 << 10

#define DST_HS_POL_HIGH 0x0 << 18
#define DST_HS_POL_LOW 0x1 << 18
#define SRC_HS_POL_HIGH 0x0 << 19
#define SRC_HS_POL_LOW 0x1 << 19

#define ABRST1 0x1 << 20
#define ABRST4 0x4 << 20
#define ABRST8 0x8 << 20
#define ABRST16 0x10 << 20
#define ABRST32 0x20 << 20

#define RELOAD_SRC 0x1 << 30
#define RELOAD_DST 0x1 << 31

// config data for DMACCxCFG_HIGH
#define FCMODE0 0x0 << 0
#define FCMODE1 0x1 << 0

#define FIFOMODE0 0x0 << 1
#define FIFOMODE1 0x1 << 1

#define PROTCTL1 0x1 << 3
#define PROTCTL2 0x2 << 3
#define PROTCTL3 0x4 << 3

#define SRC_PER0 0x0 << 7
#define SRC_PER1 0x1 << 7
#define SRC_PER2 0x2 << 7
#define SRC_PER3 0x3 << 7
#define SRC_PER4 0x4 << 7
#define SRC_PER5 0x5 << 7

#define DST_PER0 0x0 << 11
#define DST_PER1 0x1 << 11
#define DST_PER2 0x2 << 11
#define DST_PER3 0x3 << 11
#define DST_PER4 0x4 << 11
#define DST_PER5 0x5 << 11

#define SRC_PER_SPI_TX(n) ((n) << 7)
#define SRC_PER_SPI_RX(n) ((n) << 7)
#define DST_PER_SPI_TX(n) ((n) << 11)
#define DST_PER_SPI_RX(n) ((n) << 11)

#define SRC_PER_RAM (0x0 << 7)
#define SRC_PER_SPI1_TX (0x0 << 7)
#define SRC_PER_SPI2_TX (0x1 << 7)
#define SRC_PER_SPI3_TX (0x2 << 7)
#define SRC_PER_SPI1_RX (0x3 << 7)
#define SRC_PER_SPI2_RX (0x4 << 7)
#define SRC_PER_SPI3_RX (0x5 << 7)
#define SRC_PER_QADC (0x6 << 7)
#define SRC_PER_MCC1 (0x7 << 7)
#define SRC_PER_MCC2 (0x8 << 7)
#define SRC_PER_MCC3 (0x9 << 7)
#define SRC_PER_SCI1_TX (0xa << 7)
#define SRC_PER_SCI1_RX (0xb << 7)

#define DST_PER_RAM (0x0 << 11)
#define DST_PER_SPI1_TX (0x0 << 11)
#define DST_PER_SPI2_TX (0x1 << 11)
#define DST_PER_SPI3_TX (0x2 << 11)
#define DST_PER_SPI1_RX (0x3 << 11)
#define DST_PER_SPI2_RX (0x4 << 11)
#define DST_PER_SPI3_RX (0x5 << 11)
#define DST_PER_QADC (0x6 << 11)
#define DST_PER_MCC1 (0x7 << 11)
#define DST_PER_MCC2 (0x8 << 11)
#define DST_PER_MCC3 (0x9 << 11)
#define DST_PER_SCI1_TX (0xa << 11)
#define DST_PER_SCI1_RX (0xb << 11)
#define CHANNEL_UMASK(n) (((1 << n) << 8) | (1 << n))
#define CHANNEL_WRITE_ENABLE(n) ((1 << n) << 8)
#define CHANNEL_ENABLE(n) (1 << n)
#define CHANNEL_STAT(n) (1 << n)

#define QUAD_BAUDR (0x02) /*必须为偶数，且最小值为2*/
// w25q128 CMD
#define DUMMY_BYTE 0xa5
#define READ_ID_CMD 0x90
#define WRITE_EN_CMD 0x06
#define SECT_ERASE_CMD 0x20
#define GET_SAT1_CMD 0x05
#define READ_CMD 0x03
#define PAGE_PROG_CMD 0x02
#define GET_SAT2_CMD 0x35
#define PROG_STA12_CMD 0x01
#define PROG_STA2_CMD 0x31
#define DUAL_READ_CMD 0x3b
#define QUAD_PROG_CMD 0x32
#define QUAD_READ_CMD 0x6b
#define QPI_READ_CMD 0x0b
#define QPI_ENTER_CMD 0x38
#define QPI_EXIT_CMD 0xFF
#define SET_READ_PARA_CMD 0xc0
#define Reset_EN_CMD 0x66
#define Reset_CMD 0x99
/**
 * @}
 */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_SPI_Exported_Types Exported Types
 *
 * @{
 */
#endif
#define DMACR_TDMAE 0x02
#define DMACR_RDMAE 0x01

#define SR_BUSY 0x01
#define SR_TFNF 0x02
#define SR_TFE 0x04
#define SR_RFNE 0x08
#define SR_RFF 0x10
#define SR_TXE 0x20
/**
 *@brief 标志索引.
    */
typedef enum
{
    SSI_FLAG_INDEX_BUSY = 0x01,
    SSI_FLAG_INDEX_TFNF = 0x02,
    SSI_FLAG_INDEX_TFE = 0x04,
    SSI_FLAG_INDEX_RFNE = 0x08,
    SSI_FLAG_INDEX_RFF = 0x10,
} SSI_FlagIndexTypeDef;

#define _ssi_en(ssi) _bit_set(ssi->SSIENR, SSI_EN) /**< SPI 使能             */
#define _ssi_dis(ssi) _bit_clr(ssi->SSIENR, SSI_EN)
/**
 * @brief SPI状态
 */
typedef enum
{
    DRV_SSI_OK = 0x00,
    DRV_SSI_ERROR = 0x01,
    DRV_SSI_BUSY = 0x02,
    DRV_SSI_TIMEOUT = 0x03,
} DRV_SSI_StatusTypeDef;
/**
 * @brief SPI flash读写状态
 */
typedef enum
{
    DRV_SSI_READ = 0x00,
    DRV_SSI_WRITE = 0x01,
} DRV_SSI_ReadTypeDef;
/**
 * @brief DFS格式定义
 */
typedef enum
{
    DFS_01_BIT = 0x00,
    DFS_02_BIT = 0x01,
    DFS_03_BIT = 0x02,
    DFS_04_BIT = 0x03,
    DFS_05_BIT = 0x04,
    DFS_06_BIT = 0x05,
    DFS_07_BIT = 0x06,
    DFS_08_BIT = 0x07,
    DFS_09_BIT = 0x08,
    DFS_10_BIT = 0x09,
    DFS_11_BIT = 0x0a,
    DFS_12_BIT = 0x0b,
    DFS_13_BIT = 0x0c,
    DFS_14_BIT = 0x0d,
    DFS_15_BIT = 0x0e,
    DFS_16_BIT = 0x0f,
    DFS_17_BIT = 0x10,
    DFS_18_BIT = 0x11,
    DFS_19_BIT = 0x12,
    DFS_20_BIT = 0x13,
    DFS_21_BIT = 0x14,
    DFS_22_BIT = 0x15,
    DFS_23_BIT = 0x16,
    DFS_24_BIT = 0x17,
    DFS_25_BIT = 0x18,
    DFS_26_BIT = 0x19,
    DFS_27_BIT = 0x1a,
    DFS_28_BIT = 0x1b,
    DFS_29_BIT = 0x1c,
    DFS_30_BIT = 0x1d,
    DFS_31_BIT = 0x1e,
    DFS_32_BIT = 0x1f,
} SSI_DFSTypeDef;
/**
 * @brief FRF格式定义
 */
typedef enum
{
    FRF_SPI = 0x00,
    FRF_SSP = 0x01,
    FRF_MICROWIRE = 0x02,
} SSI_FRFTypeDef;
/**
 * @brief SCPH格式定义
 */
typedef enum
{
    SCPH_START_BIT = 0x01,
    SCPH_MIDDLE_BIT = 0x00,
} SSI_SCPHTypeDef;
/**
 * @brief SCPOL格式定义
 */
typedef enum
{
    INACTIVE_LOW = 0x01,
    INACTIVE_HIGH = 0x00,
} SSI_SCPOLTypeDef;
/**
 * @brief TMOD格式定义
 */
typedef enum
{
    TX_AND_RX = 0x00,
    TX_ONLY = 0x01,
    RX_ONLY = 0x02,
    EEPROM_READ = 0x03,
} SSI_TMODTypeDef;
/**
 * @brief SRL格式定义
 */
typedef enum
{
    NORMAL_MODE = 0x00,
    TESTING_MODE = 0x01,
} SSI_SRLTypeDef;
/**
 * @brief SSTE格式定义
 */
typedef enum
{
    TOGGLE_DISABLE = 0x00,
    TOGGLE_EN = 0x01,
} SSI_SSTETypeDef;

/**
 * @brief RXSDR格式定义
 */
typedef enum
{
    SAMPLE_DELAY_0_5 = 0x00010000,
    SAMPLE_DELAY_1_0 = 0x00000001,
    SAMPLE_DELAY_1_5 = 0x00010001,
    SAMPLE_DELAY_2_0 = 0x00000002,
    SAMPLE_DELAY_2_5 = 0x00010002,
} SSI_RXSDRTypeDef;
/**
 * @brief CFS格式定义
 */
typedef enum
{
    CFS_01_BIT = 0x00,
    CFS_02_BIT = 0x01,
    CFS_03_BIT = 0x02,
    CFS_04_BIT = 0x03,
    CFS_05_BIT = 0x04,
    CFS_06_BIT = 0x05,
    CFS_07_BIT = 0x06,
    CFS_08_BIT = 0x07,
    CFS_09_BIT = 0x08,
    CFS_10_BIT = 0x09,
    CFS_11_BIT = 0x0a,
    CFS_12_BIT = 0x0b,
    CFS_13_BIT = 0x0c,
    CFS_14_BIT = 0x0d,
    CFS_15_BIT = 0x0e,
    CFS_16_BIT = 0x0f,
} SSI_CFSTypeDef;
/**
 * @brief SSI格式定义
 */
typedef enum
{
    STD_mode = 0x00,
    DUAL_mode = 0x01,
    QUAD_mode = 0x02,
} SSI_FormatTypeDef;
/**
 * @brief SSI传输类型
 */
typedef enum
{
    TT0 = 0x00,
    TT1 = 0x01,
    TT2 = 0x02,
    TT3 = 0x03,
} SSI_TransTypeTypeDef;
/**
 * @brief SSI地址长度
 */
typedef enum
{
    ADDR_L0 = 0x00,
    ADDR_L4 = 0x01,
    ADDR_L8 = 0x02,
    ADDR_L12 = 0x03,
    ADDR_L16 = 0x04,
    ADDR_L20 = 0x05,
    ADDR_L24 = 0x06,
    ADDR_L28 = 0x07,
    ADDR_L32 = 0x08,
    ADDR_L36 = 0x09,
    ADDR_L40 = 0x0a,
    ADDR_L44 = 0x0b,
    ADDR_L48 = 0x0c,
    ADDR_L52 = 0x0d,
    ADDR_L56 = 0x0e,
    ADDR_L60 = 0x0f,
} SSI_AdressTypeDef;
/**
 * @brief SSI指令长度
 */
typedef enum
{
    INST_L0 = 0x00,
    INST_L4 = 0x01,
    INST_L8 = 0x02,
    INST_L16 = 0x03,
} SSI_InstructionTypeDef;
/**
 * @brief CLK_STRETCH_EN
 */
typedef enum
{
    CLK_STRETCH_DISABLE = 0x00,
    CLK_STRETCH_ENABLE = 0x01,
} SSI_CLK_STRETCH_ENTypeDef;

/**
 * @brief QPI传输模式
 */
typedef enum
{
    CMD_READ = 0,
    CMD_WRITE = 1,
    DATA_READ = 2,
    DATA_WRITE = 3,
} QPI_OPT_MODE;
/**
 *@brief spi初始化结构体定义.
    *
    */
typedef struct
{
    uint8_t DFS;    /**<SSI数据帧大小   0x00   CTRLR0
                        */
    uint8_t FRF;    /**<SSI数据帧格式   0x00   CTRLR0
                        */
    uint8_t SCPH;   /**<串行时钟相位    0x00
                        */
    uint8_t SCPOL;  /**<串行时钟极性    0x00
                        */
    uint8_t TMOD;   /**<传输模式        0x00
                        */
    uint8_t SRL;    /**<位移寄存器循环  0x00
                        */
    uint8_t SSTE;   /**<从设备选择切换使能  0x00
                        */
    uint8_t CFS;    /**<控制帧大小      0x00
                        */
    uint8_t Format; /**<SSI通讯格式     0x00
                        *- STD_mode    Standard SPI Format
                        *- DUAL_mode   Dual SPI Format
                        *- QUAD_mode   Quad SPI Format
                        */
    uint16_t NDF;   /**<数据帧数量     0x04     CTRLR1
                        */

    uint16_t SSI_STD_BaudRatePrescaler;  /**<SSI时钟分频值   0x14     BAUDR*/
    uint16_t SSI_DUAL_BaudRatePrescaler; /**<SSI时钟分频值   0x14     BAUDR*/
    uint16_t SSI_QUAD_BaudRatePrescaler; /**<SSI时钟分频值   0x14     BAUDR*/

    uint8_t Transfer_start_FIFO_level; // TXFTLR<<16
    uint8_t Transmit_FIFO_Threshold;   // TXFTLR
    uint8_t Receive_FIFO_Threshold;    // RXFTLR

    uint32_t Rx_Sample_Delay; /*接收延迟采样设置  0xf0   RXSDR*/

    uint8_t Trans_Type;         /**<SSI传输类型      0xf4    SPICTRLR0
                                    */
    uint8_t Address_Length;     /**<SSI地址长度       0xf4    SPICTRLR0
                                    */
    uint8_t Instruction_length; /**<SSI指令长度         0xf4    SPICTRLR0
                                    *- INST_L0   No Instruction
                                    *- INST_L4   4 bit Instruction length
                                    *- INST_L8   8 bit Instruction length
                                    *- INST_L16  6 bit Instruction length
                                    */
    uint8_t WAIT_CYCLES;        /**<等待cycle数         0xf4    SPICTRLR0
                                    */
    uint8_t CLK_STRETCH_EN;     /**<SPI传输扩展使能      0xf4    SPICTRLR0
                                    */

} SSI_InitTypeDef;

extern void DRV_SSI_StructInit(SSI_InitTypeDef *pinit);
extern DRV_SSI_StatusTypeDef DRV_SSI_DeInit(SSI_TypeDef *pssi);
extern DRV_SSI_StatusTypeDef DRV_SSI_STD_Init(SSI_TypeDef *pssi, uint32_t baund, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_Init(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t timeout);
extern void DRV_SSI_Cmd(SSI_TypeDef *pssi, FunctionalStateTypeDef NewState);
extern DRV_SSI_StatusTypeDef DRV_SSI_STD_Read_ID(SSI_TypeDef *pssi, uint16_t *ID, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_GetFlag(SSI_TypeDef *pssi, SSI_FlagIndexTypeDef FlagIndex, FlagStatusTypeDef *pflag);
extern DRV_SSI_StatusTypeDef DRV_SSI_WaitonFlagTimeout(SSI_TypeDef *pssi,
                                                        SSI_FlagIndexTypeDef index,
                                                        FlagStatusTypeDef status,
                                                        uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_STD_Read(SSI_TypeDef *pssi, uint32_t addr, uint32_t num, uint8_t *buf, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_STD_Sector_Erase(SSI_TypeDef *pssi, uint32_t addr, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_STD_Program(SSI_TypeDef *pssi, uint32_t addr, uint32_t num, uint8_t *buf, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_DUAL_Read(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t addr, uint32_t num, uint8_t *buf, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_QUAD_Init(SSI_TypeDef *pssi, uint16_t baund, DRV_SSI_ReadTypeDef read, uint32_t num, uint32_t waitCycles, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QUAD_Read(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t addr, uint32_t num, uint8_t *buf, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QUAD_Program(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t addr, uint32_t num, uint8_t *buf, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QPI_Enter(SSI_TypeDef *pssi, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QPI_EXIT(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QPI_Sector_Erase(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t addr, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QPI_Read(SSI_TypeDef *pssi, uint32_t addr, uint32_t num, uint8_t *buf, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QPI_Program(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t addr, uint32_t num, uint8_t *buf, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_XIP_Enter(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_XIP_EXIT(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t timeout);

extern DRV_SSI_StatusTypeDef DRV_SSI_STD_Reset_Enable(SSI_TypeDef *pssi, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_STD_Reset(SSI_TypeDef *pssi, uint32_t timeout);

extern void DRV_SSI_STD_DMA_Trig(SSI_TypeDef *pssi, unsigned char cmd, unsigned int addr, int dmaConf);
extern void DRV_SSI_DUAL_DMA_Trig(SSI_TypeDef *pssi, unsigned char cmd, unsigned int addr, int dmaConf);
extern void DRV_SSI_QUAD_DMA_Trig(SSI_TypeDef *pssi, unsigned char cmd, unsigned int addr, int dmaConf);
extern void DRV_SSI_QPI_DMA_Trig(SSI_TypeDef *pssi, unsigned char cmd, unsigned int addr, int dmaConf);

extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_QPI_Write_Enable(SSI_TypeDef *pssi, SSI_InitTypeDef *pinit, uint32_t timeout);
extern DRV_SSI_StatusTypeDef DRV_SSI_EFlash_Write_Enable(SSI_TypeDef *pssi, uint32_t timeout);

extern void DRV_SSI_DMA_Transmit_level(SSI_TypeDef *pssi, unsigned char level);
extern void DRV_SSI_DMA_Receive_level(SSI_TypeDef *pssi, unsigned char level);
extern DRV_SSI_StatusTypeDef DRV_SSI_WAIT_IDLE(SSI_TypeDef *pssi, uint32_t timeout);

extern DRV_SSI_StatusTypeDef DRV_PsramInit(SSI_TypeDef *pssi);

extern void SSI_Standard_Init(SSI_TypeDef *SSIx);
extern void SSI_STD_DMA_Program(SSI_TypeDef *SSIx, uint8_t *psend, uint32_t addr, uint32_t length, bool binten);
extern void SSI_STD_DMA_Read(SSI_TypeDef *SSIx, uint8_t *pread, uint32_t addr, uint32_t length, bool binten);

#endif /* __SSI_DRV_H */
