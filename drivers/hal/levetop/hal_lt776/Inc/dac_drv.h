/**
    ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    dac_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2020.03.18
  * @brief   Header file of Eport DRV module.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DAC_DRV_H__
#define __DAC_DRV_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
 *
 *@{
 */

/** @defgroup DRV_DAC DAC
 *
 *@{
 */
#endif

/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_DAC_Exported_Macros Exported Macros
 *
 * @{
 */
#endif
    /**
     * @brief  DAC 常用宏常量
     *
     */

#define DAC_EN (1 << 0)
#define DAC_BUFDIS (1 << 1)
#define DAC_ALIGN (1 << 2)
#define DAC_RES (1 << 3)
#define DAC_DMATH (1 << 16)
#define DAC_DMAEN (1 << 23)
#define DAC_STH (1 << 24)
#define DAC_STHIE (1 << 28)
#define DAC_UDFIE (1 << 29)
#define DAC_OVFIE (1 << 30)
#define DAC_FCLR ((uint32_t)1 << 31)

#define DAC_TSEL_MASK (0x00000600)
#define DAC_TSEL_SHIFT (9)

#define DAC_EXT_TMOD_MASK (0x00007000)
#define DAC_EXT_TMOD_SHIFT (12)

#define DAC_SW_TRIG (1 << 0)

#define DAC_SER (1 << 29)
#define DAC_OVF (1 << 30)
#define DAC_LD_DONE ((uint32_t)1 << 31)

#define _dac_en() (DAC->DAC_CR |= DAC_EN)
#define _dac_dis() (DAC->DAC_CR &= ~DAC_EN)
#define _dac_en_dma() (DAC->DAC_CR |= DAC_DMAEN)

#define _dac_set_right_align() (DAC->DAC_CR &= ~(DAC_ALIGN))
#define _dac_set_left_align() (DAC->DAC_CR |= DAC_ALIGN)
#define _dac_set_eight_res() (DAC->DAC_CR &= ~(DAC_RES))
#define _dac_set_twelve_res() (DAC->DAC_CR |= DAC_RES)

#define _dac_clr_trigger_mode() (DAC->DAC_CR &= ~(DAC_TSEL_MASK | DAC_EXT_TMOD_MASK))
#define _dac_set_trigger_mode(triggerMode, external_mode) (DAC->DAC_CR |= ((triggerMode << DAC_TSEL_SHIFT) | (external_mode << DAC_EXT_TMOD_SHIFT)))
#define _dac_diable_ext_verf() (DAC->DAC_TRIMR &= ~(0x1000000))

#define _dac_set_data(data) (DAC->DAC_DR = data)
#define _dac_get_data(data) (DAC->DAC_DOR << 4)
#define _dac_set_sw_trig() (DAC->DAC_SWTR |= DAC_SW_TRIG)
#define _dac_get_fsr_flag() (DAC->DAC_FSR & DAC_LD_DONE)
/**
 * @}
 */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_DAC_Exported_Types Exported Types
 *
 * @{
 */
#endif

    /**
     *@brief DAC 数据格式
     */
    typedef enum
    {
        LEFTALIGNED_8BITS = 0,
        LEFTALIGNED_12BITS,
        RIGHTALIGNED_8BITS,
        RIGHTALIGNED_12BITS
    } DATA_FORMAT_TypeDef;

    /**
     *@brief DAC 触发类型
     */
    typedef enum
    {
        TRIGGER_SOFTWARE = 0,
        TRIGGER_EXTERNAL,
        TRIGGER_PIT
    } TRIGGER_MODE_TypeDef;

    /**
     *@brief DAC 外部触发状态
     */
    typedef enum _DAC_EXT_TMOD_
    {
        MOD_RESERVED = 0,
        DET_ON_RISING = 1,
        DET_ON_FALLING,
        DET_ON_BOTH,
        DET_ON_HIGH,
        DET_ON_LOW
    } EXT_TMOD_TypeDef;

    /**
     * @brief DAC 初始化结构体定义
     */
    typedef struct
    {
        DATA_FORMAT_TypeDef DataFomat;    /*!< 数据格式 */
        TRIGGER_MODE_TypeDef TriggerMode; /*!< 触发模式*/
        EXT_TMOD_TypeDef ExtTriggerMode;  /*!< 外部触发的触发状态 */
        boolean boDmaEnable;              /*!< 是否支持DMA发送 */
    } DAC_InitTypeDef, *pDAC_InitTypeDef;

/**
 * @}
 */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_DAC_Exported_Variables Exported Variables
 *
 * @{
 */
#endif

/**
 * @}
 */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_DAC_Exported_Functions Exported Functions
 * @{
 */
#endif

    void DRV_DAC_Init(DAC_InitTypeDef Init);
    void DRV_DAC_Deinit(void);
    void DRV_DAC_SendData(uint8_t data);
    void DRV_DAC_SW_Trig(void);
    void DRV_DAC_GetData(uint16_t *data);
    // void DRV_DAC_SetAudioFrequency(uint32_t AudioFreq);

    /**
     * @}
     */

    /**
     *@}
     */

    /**
     *@}
     */

#ifdef __cplusplus
}

#endif
#endif /* __DAC_DRV_H */
/************************ (C) COPYRIGHT C*Core *****END OF FILE**********************/
