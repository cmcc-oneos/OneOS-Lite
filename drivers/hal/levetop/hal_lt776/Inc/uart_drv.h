/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    uart_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of UART DRV module..
  ******************************************************************************
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UART_DRV_H
#define __UART_DRV_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"
#include "eport_drv.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
  *
  *@{
  */

/** @defgroup DRV_UART UART
  *
  *@{
  */
#endif

/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Macros Exported Macros
  *
  * @{
  */
#endif
/**
  * @brief  UART 常用宏常量
  *
  */
#define UART_RECV_MAX_LEN 512
#define UART_RECEIVE_TRANSMIT_IT_TEST 0 //该模式能实现串口4中断收发功能

#define UART_PIN_INDEX_TXD ((uint8_t)(01))
#define UART_PIN_INDEX_RXD ((uint8_t)(00))

#define UART_WORDLENGTH_8B 0x00
#define UART_WORDLENGTH_9B ((uint8_t)UART_DATA_FORMAT_MODE_9BITS)

#define UART_PARITY_MODE_NONE 0x00
#define UART_PARITY_MODE_EVEN ((uint8_t)UART_PARITY_ENABLE)
#define UART_PARITY_MODE_ODD ((uint8_t)(UART_PARITY_ENABLE | UART_PARITY_ODD))

#define _uart_en_loops(uart) (uart->CR1 | UART_LOOPS_MODE)
#define _uart_dis_loops(uart) (uart->CR1 &= UART_LOOPS_NORMAL_MODE)
#define _uart_pin_opendrai_mode(uart) (uart->CR1 |= UART_OPEN_DRAIN_MODE)
#define _uart_pin_cmos_mode(uart) (uart->CR1 &= UART_CMOS_MODE)
#define _uart_data_format_9(uart) (uart->CR1 |= UART_DATA_FORMAT_MODE_9BITS)
#define _uart_data_format_8(uart) (uart->CR1 &= UART_DATA_FORMAT_MODE_8BITS)
#define _uart_wakeup_mode_add_match(uart) (uart->CR1 |= UART_WAKEUP_MODE_ADDRESS)
#define _uart_wakeup_mode_idle_lin(uart) (uart->CR1 &= UART_WAKEUP_IDLE_LINE)
#define _uart_idle_start_afer_stop(uart) (uart->CR1 |= UART_IDLE_LINE_AFTER_STOP)
#define _uart_idle_start_afer_start(uart) (uart->CR1 &= UART_IDLE_LINE_AFTER_START)
#define _uart_en_parity(uart) (uart->CR1 |= UART_PARITY_ENABLE)
#define _uart_dis_parity(uart) (uart->CR1 &= UART_PARITY_DISABLE)
#define _uart_parity_odd(uart) (uart->CR1 |= UART_PARITY_ODD)
#define _uart_parity_even(uart) (uart->CR1 &= ~UART_PARITY_ODD)
#define _uart_en_txd(uart) (uart->CR2 |= UART_TE)
#define _uart_dis_txd(uart) (uart->CR2 &= ~UART_TE)
#define _uart_en_rxd(uart) (uart->CR2 |= UART_RE)
#define _uart_dis_rxd(uart) (uart->CR2 &= ~UART_RE)
#define _uart_en_rx_it(uart) (uart->CR2 |= UART_IT_RIE)
#define _uart_dis_rx_it(uart) (uart->CR2 &= ~UART_IT_RIE)
#define _uart_en_tc_it(uart) (uart->CR2 |= UART_IT_TCIE)
#define _uart_en_tdre_it(uart) (uart->CR2 |= UART_IT_TIE)
#define _uart_dis_tc_it(uart) (uart->CR2 &= ~UART_IT_TCIE)
#define _uart_dis_tdre_it(uart) (uart->CR2 &= ~UART_IT_TIE)
#define _uart_en_idle_it(uart) _bit_set(uart->CR2, UART_IT_IDLE)
#define _uart_dis_idle_it(uart) _bit_clr(uart->CR2, UART_IT_IDLE)
#define _uart_get_it_source(uart, index) (uart->CR2 & index ? SET : RESET)
#define _uart_get_tx_it_flag(uart) (uart->SR1 & UART_IT_FLAG_TC)
#define _uart_get_tc_flag(uart) (uart->SR1 & UART_IT_FLAG_TC)
#define _uart_get_tdre_it_flag(uart) (uart->SR1 & UART_IT_FLAG_TDRE)
#define _uart_get_rx_it_flag(uart) (uart->SR1 & UART_IT_FLAG_RDRF)
#define _uart_get_pf_flag(uart) (uart->SR1 & UART_IT_FLAG_PF)
#define _uart_clear_tx_it_flag(uart) (uart->SR1)
#define _uart_clear_rx_it_flag(uart) (uart->SR1)
#define _uart_clear_raf_it_flag(uart) (uart->SR2)
#define _uart_get_data(uart) (uart->DRL) //xukai20200407
#define _uart_set_tx_bit(uart) (uart->PORT |= 0x02)
#define _uart_set_rx_bit(uart) (uart->PORT |= 0x01)
#define _uart_reset_tx_bit(uart) (uart->PORT &= ~(0x02))
#define _uart_reset_rx_bit(uart) (uart->PORT &= ~(0x01))
#define _uart_get_tx_bit(uart) (uart->PORT & UART_PIN_TXD)
#define _uart_get_rx_bit(uart) (uart->PORT & UART_PIN_RXD)
#define _uart_pin_dir_output(uart, pin) _bit_set(uart->DDR, (1 << (pin + UART_PIN_INDEX_RXD)))
#define _uart_pin_dir_input(uart, pin) _bit_clr(uart->DDR, (1 << (pin + UART_PIN_INDEX_RXD)))
#define _uart_set_pin(uart, pin) _bit_set(uart->PORT, 1 << (pin + UART_PIN_INDEX_RXD))
#define _uart_reset_pin(uart, pin) _bit_clr(uart->PORT, 1 << (pin + UART_PIN_INDEX_RXD))
#define _uart_set_pin_bit(uart, pin) _bit_set(uart->PORT, 1 << (pin + UART_PIN_INDEX_RXD))
#define _uart_reset_pin_bit(uart, pin) _bit_clr(uart->PORT, 1 << (pin + UART_PIN_INDEX_RXD))
#define _uart_get_pin_bit(uart, pin) _bit_get(uart->PORT, 1 << (pin + UART_PIN_INDEX_RXD))
#define _uart_pin_output_cmos(uart) _bit_clr(uart->CR1, (1 << UART_WOMS_SHIFT_MASK))
#define _uart_pin_output_opendrain(uart) _bit_set(uart->CR1, (1 << UART_WOMS_SHIFT_MASK))
#define _uart_pin_pullup_en(uart, pin) _bit_set(uart->PURD, (1 << (pin + UART_PU_SHIFT_MASK)))
#define _uart_pin_pullup_dis(uart, pin) _bit_clr(uart->PURD, (1 << (pin + UART_PU_SHIFT_MASK)))
#define _uart_txd_output(uart) (uart->DDR |= UART_TXD_OUT)
#define _uart_rxd_output(uart) (uart->DDR |= UART_RXD_OUT)
#define _uart_txd_input(uart) (uart->DDR &= ~UART_TXD_OUT)
#define _uart_rxd_input(uart) (uart->DDR &= ~UART_RXD_OUT)
#define _uart_en_pullup(uart) (uart->PURD |= UART_PULLUP_EN)
#define _uart_dis_pullup(uart) (uart->PURD &= UART_PULLUP_DIS)
#define _uart_en_full_drive(uart) (uart->PURD &= UART_FULL_DRIVE)
#define _uart_dis_full_drive(uart) (uart->PURD |= UART_REDUCE_DRIVE)
#define _uart_reset_control1_value(uart) (uart->CR1 = 0)
#define _uart_reset_control2_value(uart) (uart->CR2 = 0)
#define _uart_get_nine_bit(uart) ((uart->DRH & 0X80) == 0X80 ? 1 : 0)
/*UARTFCR:UART fifo control register*/
#define _uart_fifo_rx_en(uart) _bit_set(uart->FCR, UART_RF_EN)
#define _uart_fifo_rx_dis(uart) _bit_clr(uart->FCR, UART_RF_EN)
#define _uart_fifo_tx_en(uart) _bit_set(uart->FCR, UART_TF_EN)
#define _uart_fifo_tx_dis(uart) _bit_clr(uart->FCR, UART_TF_EN)
#define _uart_set_rx_fifo_level(uart, level) _reg_modify(uart->FCR, UART_RX_FLSEL_MASK, (level << UART_RX_FLSEL_SHIFT_MASK))
#define _uart_set_tx_fifo_level(uart, level) _reg_modify(uart->FCR, UART_TX_FLSEL_MASK, (level << UART_TX_FLSEL_SHIFT_MASK))
#define _uart_fifo_txrx_en(uart) _bit_set(uart->FCR, (UART_RF_EN | UART_TF_EN))
#define _uart_fifo_txrx_dis(uart) _bit_clr(uart->FCR, (UART_RF_EN | UART_TF_EN))
/*UARTFSR:UART FIFO status register*/
#define _uart_get_fifo_flag(uart, flag) _bit_get(uart->FSR, flag)
#define _uart_get_fifo_flags(uart) _reg_read(uart->FSR)
/*UARTDCR:UART DMA control register*/
#define _uart_tx_dma_en(uart) _bit_set(uart->DCR, UART_TX_DMA_EN)
#define _uart_tx_dma_dis(uart) _bit_clr(uart->DCR, UART_TX_DMA_EN)
#define _uart_rx_dma_en(uart) _bit_set(uart->DCR, UART_RX_DMA_EN)
#define _uart_rx_dma_dis(uart) _bit_clr(uart->DCR, UART_RX_DMA_EN)
#define _uart_txrx_dma_en(uart) _bit_set(uart->DCR, (UART_TX_DMA_EN | UART_RX_DMA_EN))
#define _uart_txrx_dma_dis(uart) _bit_clr(uart->DCR, (UART_TX_DMA_EN | UART_RX_DMA_EN))
/*UARTFCR2:UART fifo control register 2*/
#define _uart_fifo_tx_it_en(uart) _bit_set(uart->FCR2, UART_FIFO_TXF_IE)
#define _uart_fifo_txfc_it_en(uart) _bit_set(uart->FCR2, UART_FIFO_TXFC_IE)
#define _uart_fifo_rx_it_en(uart) _bit_set(uart->FCR2, UART_FIFO_RXF_IE)
#define _uart_fifo_rx_overrun_it_en(uart) _bit_set(uart->FCR2, UART_FIFO_RXFOR_IE)
#define _uart_fifo_rx_timeout_it_en(uart) _bit_set(uart->FCR2, UART_FIFO_RXFTO_IE)
#define _uart_fifo_rx_timeout_en(uart) _bit_set(uart->FCR2, UART_FIFO_RXF_TO_EN)
#define _uart_fifo_tx_it_dis(uart) _bit_clr(uart->FCR2, UART_FIFO_TXF_IE)
#define _uart_fifo_txfc_it_dis(uart) _bit_clr(uart->FCR2, UART_FIFO_TXFC_IE)
#define _uart_fifo_rx_it_dis(uart) _bit_clr(uart->FCR2, UART_FIFO_RXF_IE)
#define _uart_fifo_rx_overrun_it_dis(uart) _bit_clr(uart->FCR2, UART_FIFO_RXFOR_IE)
#define _uart_fifo_rx_timeout_it_dis(uart) _bit_clr(uart->FCR2, UART_FIFO_RXFTO_IE)
#define _uart_fifo_rx_timeout_dis(uart) _bit_clr(uart->FCR2, UART_FIFO_RXF_TO_EN)
#define _uart_fifo_tx_clr(uart) _bit_set(uart->FCR2, UART_FIFO_TXF_CLR)
#define _uart_fifo_rx_clr(uart) _bit_set(uart->FCR2, UART_FIFO_RXF_CLR)
/*UARTRXFTOCRT:UART rx FIFO timeout coutner register*/
#define _uart_set_rx_fifo_tiemout_cnt(uart, cnt) _reg_write(uart->RXTOCTR, cnt)
/*UARTFSR2:UART FIFO status register 2*/
#define _uart_get_fifo_mode_overrun_flag(uart) _bit_get(uart->FSR2, UART_FXOR)
#define _uart_get_fifo_mode_noise_flag(uart) _bit_get(uart->FSR2, UART_FXNF)
#define _uart_get_fifo_mode_frame_err_flag(uart) _bit_get(uart->FSR2, UART_FXFE)
#define _uart_get_fifo_mode_parity_err_flag(uart) _bit_get(uart->FSR2, UART_FXPF)
#define _uart_clear_fifo_mode_overrun_flag(uart) _bit_set(uart->FSR2, UART_FXOR)
#define _uart_clear_fifo_mode_noise_flag(uart) _bit_set(uart->FSR2, UART_FXNF)
#define _uart_clear_fifo_mode_frame_err_flag(uart) _bit_set(uart->FSR2, UART_FXFE)
#define _uart_clear_fifo_mode_parity_err_flag(uart) _bit_set(uart->FSR2, UART_FXPF)
/**
  * @}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Types Exported Types
  *
  * @{
  */
#endif

  /**
  * @brief UART状态定义
  */
  typedef enum
  {
    DRV_UART_OK = 0x00,
    DRV_UART_ERROR = 0x01,
    DRV_UART_BUSY = 0x02,
    DRV_UART_TIMEOUT = 0x03,
  } DRV_UART_StatusTypeDef;

  /**
  *@brief UART管脚定义
  */
  typedef enum
  {
    UART_PIN_RXD,
    UART_PIN_TXD
  } UART_PinTypeDef;

  /**
  * @brief UART 标志索引定义
  */
  typedef enum
  {
    UART_FLAG_INDEX_PF,
    UART_FLAG_INDEX_FE,
    UART_FLAG_INDEX_NF,
    UART_FLAG_INDEX_OR,
    UART_FLAG_INDEX_IDLE,
    UART_FLAG_INDEX_RDRF,
    UART_FLAG_INDEX_TC,
    UART_FLAG_INDEX_TDRE,
    UART_FLAG_INDEX_RAF,
  } UART_FlagIndexTypeDef;

  /**
  * @brief UART 中断索引定义
  */
  typedef enum
  {
    UART_IT_INDEX_TDRE = 0X80,
    UART_IT_INDEX_TCIE = 0X40,
    UART_IT_INDEX_RIE = 0X20,
    UART_IT_INDEX_ILIE = 0X10,
  } UART_ItIndexTypeDef;

  /**
  * @brief UART 初始化结构体定义
  */
  typedef struct
  {
    uint32_t BaudRate;   /*!< This member configures the UART communication baud rate.
                                           The baud rate is computed using the following formula:
                                           - IntegerDivider = ((PCLKx) / (8 * (OVR8+1) * (huart->Init.BaudRate)))
                                           - FractionalDivider = ((IntegerDivider - ((uint32_t) IntegerDivider)) * 8 * (OVR8+1)) + 0.5
                                           Where OVR8 is the "oversampling by 8 mode" configuration bit in the CR1 register. */
    uint32_t WordLength; /*!< Specifies the number of data bits transmitted or received in a frame.
                                           This parameter can be a value of @ref UART_Word_Length */
    uint32_t Parity;     /*!< Specifies the parity mode.
                                           This parameter can be a value of @ref UART_Parity
                                           @note When parity is enabled, the computed parity is inserted
                                                 at the MSB position of the transmitted data (9th bit when
                                                 the word length is set to 9 data bits; 8th bit when the
                                                 word length is set to 8 data bits). */
    uint32_t IPSFreq;

    UINT8 UART_Mode;
  } UART_InitTypeDef, *pUART_InitTypeDef;

  typedef enum
  {
    UART_NORMAL_MODE = 0,
    UART_INT_MODE,
    UART_EDMA_MODE
  } UART_MODE;

  typedef struct _uart_recv_buf
  {
    UINT16 rp;                    //read pointer
    UINT16 wp;                    //write pointer
    UINT8 dat[UART_RECV_MAX_LEN]; //buffer
  } UartRecvBufStruct;

/**
  * @}
  */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Variables Exported Variables
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Functions Exported Functions
  * @{
  */
#endif

  void DRV_UART_Init(UART_TypeDef *puart, UART_InitTypeDef Init);
  void DRV_UART_Deinit(UART_TypeDef *puart);
  void DRV_UART_SendByte(UART_TypeDef *puart, uint8_t outbyte);
  void DRV_UART_SendBytes(UART_TypeDef *puart, uint8_t *pbuf, uint16_t len);
  void DRV_UART_EnTx(UART_TypeDef *puart);
  void DRV_UART_DisTx(UART_TypeDef *puart);
  void DRV_UART_EnRx(UART_TypeDef *puart);
  void DRV_UART_DisRx(UART_TypeDef *puart);
  void DRV_UART_Dis(UART_TypeDef *puart);
  void DRV_UART_En(UART_TypeDef *puart);
  void DRV_UART_SendByteHex(UART_TypeDef *puart, uint8_t send);
  void DRV_UART_SendString(UART_TypeDef *puart, char *str);
  void DRV_UART_DisItTie(UART_TypeDef *puart);
  void DRV_UART_DisItTcie(UART_TypeDef *puart);
  void DRV_UART_DisItRe(UART_TypeDef *puart);
  void DRV_UART_EnItTie(UART_TypeDef *puart);
  void DRV_UART_EnItTcie(UART_TypeDef *puart);
  void DRV_UART_EnItRe(UART_TypeDef *puart);
  void DRV_UART_EnIt(UART_TypeDef *puart, UART_ItIndexTypeDef index);
  void DRV_UART_DisIt(UART_TypeDef *puart, UART_ItIndexTypeDef index);
  DRV_UART_StatusTypeDef DRV_UART_GetFlag(UART_TypeDef *puart,
                                          UART_FlagIndexTypeDef index,
                                          FlagStatusTypeDef *flag);
  DRV_UART_StatusTypeDef DRV_UART_ClearFlag(UART_TypeDef *puart,
                                            UART_FlagIndexTypeDef index);
  DRV_UART_StatusTypeDef DRV_UART_WaitonFlagTimeout(UART_TypeDef *puart,
                                                    UART_FlagIndexTypeDef index,
                                                    FlagStatusTypeDef status,
                                                    uint32_t timeout);
  DRV_UART_StatusTypeDef DRV_UART_SendData(UART_TypeDef *puart, uint16_t tosend, uint8_t bits);
  DRV_UART_StatusTypeDef DRV_UART_GetData(UART_TypeDef *puart, uint16_t *pres, uint8_t bits);
  DRV_UART_StatusTypeDef DRV_UART_GetStatus(UART_TypeDef *puart, uint8_t *pstatus1, uint8_t *pstatus2);
  FlagStatusTypeDef DRV_UART_GetItSource(UART_TypeDef *puart, UART_ItIndexTypeDef index);
  void DRV_UART_SetTxdDirOutput(UART_TypeDef *puart);
  void DRV_UART_SetTxdDirInput(UART_TypeDef *puart);
  void DRV_UART_SetRxdDirOutput(UART_TypeDef *puart);
  void DRV_UART_SetRxdDirInput(UART_TypeDef *puart);
  void DRV_UART_EnPullup(UART_TypeDef *puart);
  void DRV_UART_DisPullup(UART_TypeDef *puart);
  void DRV_UART_SetOutputOpendrain(UART_TypeDef *puart);
  void DRV_UART_SetOutputCmos(UART_TypeDef *puart);
  void DRV_UART_SetTxdPinBit(UART_TypeDef *puart);
  void DRV_UART_ResetTxdPinBit(UART_TypeDef *puart);
  void DRV_UART_SetRxdPinBit(UART_TypeDef *puart);
  void DRV_UART_ResetRxdPinBit(UART_TypeDef *puart);
  uint8_t DRV_UART_GetTxdPinBit(UART_TypeDef *puart);
  uint8_t DRV_UART_GetRxdPinBit(UART_TypeDef *puart);
  void DRV_UART_SetPinBit(UART_TypeDef *puart, uint8_t pin);
  void DRV_UART_ResetPinBit(UART_TypeDef *puart, uint8_t pin);
  uint8_t DRV_UART_GetPinBit(UART_TypeDef *puart, uint8_t pin);
  void DRV_UART_RxdPinToggle(UART_TypeDef *puart);
  void DRV_UART_TxdPinToggle(UART_TypeDef *puart);
  void DRV_UART_PutsDev(volatile const uint8_t *s);

  INT8 UART_RecvByte(UART_TypeDef *UARTx, UINT8 *dat);
  void UART_SendByte(UART_TypeDef *UARTx, UINT8 SendByte);
  //void UART_ISR(UART_TypeDef *UARTx, UartRecvBufStruct *UARTxRecvBufStruct);
  void UART_ISR(UART_TypeDef *UARTx);
  /**
  * @}
  */

  /**
  *@}
  */

  /**
  *@}
  */

#ifdef __cplusplus
}

#endif
#endif /* __UART_DRV_H */
/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
