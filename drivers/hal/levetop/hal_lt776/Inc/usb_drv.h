/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    usb_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of USB DRV module..
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_DRV_H__
#define __USB_DRV_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
 *
 *@{
 */

/** @defgroup DRV_UART UART
 *
 *@{
 */
#endif

/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Macros Exported Macros
 *
 * @{
 */
#endif
    /* Global USB Register */

#define USB_DMA /**< 关闭此宏，USB将采用CPU方式通信 */

#define USB_FIFO_SIZE (64)
#define USB_FIFO_EP0_ADDR (0)
#define USB_FIFO_EP1_ADDR (USB_FIFO_EP0_ADDR + USB_FIFO_SIZE)
#define USB_FIFO_EP2_ADDR (USB_FIFO_EP1_ADDR + USB_FIFO_SIZE)
#define USB_FIFO_EP3_ADDR (USB_FIFO_EP2_ADDR + USB_FIFO_SIZE)
#define USB_FIFO_EP4_ADDR (USB_FIFO_EP3_ADDR + USB_FIFO_SIZE)
#define USB_FIFO_EP5_ADDR (USB_FIFO_EP4_ADDR + USB_FIFO_SIZE)
#define USB_FIFO_EP6_ADDR (USB_FIFO_EP5_ADDR + USB_FIFO_SIZE)
#define USB_FIFO_EP7_ADDR (USB_FIFO_EP6_ADDR + USB_FIFO_SIZE)

// bulk transfer packet size
#define USB_MAX_PACKET_SIZE_V20 0x200
#define USB_MAX_PACKET_SIZE_V11 0x40
#define USB_MAX_PACKET_SIZE_EP0 0x40

#define CONTROL_EP 0
#define INDEX_EP1 1
#define INDEX_EP2 2
#define INDEX_EP3 3
#define INDEX_EP4 4
#define INDEX_EP5 5
#define INDEX_EP6 6
#define INDEX_EP7 7

#define BULKIN_EP INDEX_EP1
#define BULKOUT_EP INDEX_EP2
#define BULKIN_INT INDEX_EP3

/**
 * @}
 */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Types Exported Types
 *
 * @{
 */
#endif

    /**
     * @brief USB 包定义
     *
     */
    enum
    {
        PACKET_MID,
        PACKET_END = 1,
    };

    /**
     * @brief USB 版本协议定义
     *
     */
    typedef enum
    {
        USB_VERSION_11 = 0,
        USB_VERSION_20,
    } USBCVersion_Enum;
    /**
     * @brief USB 句柄结构体定义
     *
     */
    typedef struct
    {
        uint8_t osc;              /**< USB时钟选择0:内部时钟  1:外部时钟*/
        USBCVersion_Enum version; /**< USB协议版本号*/
        uint8_t Ep0DataStage;     /**< EP0从那一包开始发 0:从第一包发  1：发送从第二包开始的数据*/

    } USBC_HandleTypeDef;

/**
 * @}
 */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Variables Exported Variables
 *
 * @{
 */
#endif

    extern __IO uint16_t gUSBC_PacketSize;
    extern __IO uint32_t gUSBC_NewAddress;
    extern __IO uint8_t gUSBC_RxINT_Flag;
    extern __IO uint8_t gUSBC_TxINT_Flag;

/**
 * @}
 */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_UART_Exported_Functions Exported Functions
 * @{
 */
#endif

    extern void USBC_VarInit(void);

    extern void USBC_Init(USBC_HandleTypeDef *husb);

    extern void USBC_SetFIFOAddr(uint32_t FIFOAddr, uint8_t mode);

    extern void USBC_ClearRx(void);

    extern void USBC_SetTx(void);

    extern void USBC_ReadEPxData(uint8_t usbcEPx, uint8_t *src, uint16_t Length);

    extern void USBC_WriteEPxData(uint8_t usbcEPx, uint8_t *dest, uint16_t Length);

    extern void USBC_ReadEPxDataByDMA(uint8_t usbcEPx, uint8_t *src, uint16_t Length);

    extern void USBC_WriteEPxDataByDMA(uint8_t usbcEPx, uint8_t *dest, uint16_t Length);

    extern void USBC_EP0SendStall(void);

    extern void USBC_EPxSendStall(uint8_t EPx);

    extern void USBC_WriteEP0Data(uint8_t *src, uint16_t Length, uint8_t Status);

    extern uint16_t USBC_ReceiveData(uint8_t usbcEPx, uint8_t *buf);

    extern uint16_t USBC_EP0SendData(uint16_t requestLen, uint8_t *databuf, uint16_t dataLen);

    extern void USBC_SetTxSize(uint16_t Size);

    extern uint8_t USBC_SendData(uint8_t usbcEPx, uint8_t *buf, uint16_t len);

    extern uint8_t USBC_SendDataByDMA(uint8_t usbcEPx, uint8_t *buf, uint16_t len);

    extern uint8_t USBC_ReceiveDataByDMA(uint8_t usbcEPx, uint8_t *buf, uint16_t len);

    extern void USBC_Ep0Handler(void);

    extern void USB_IRQHandler(void);

    /**
     * @}
     */

    /**
     *@}
     */

    /**
     *@}
     */

#endif /* __USB_DRV_H__ */
