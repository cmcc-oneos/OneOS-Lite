/**********************************************************
* @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
* @File			sdram_drv.h
* @Author		Jason Zhao
* @Date			2021/12/1
* @Time			17:31:24
* @Version		
* @Brief			
**********************************************************
* Modification History
*
*
**********************************************************/
#ifndef __SDRAM_DRV_H__
#define __SDRAM_DRV_H__
 
 #include "lt776_reg.h"
 #include "cpm_drv.h"
 
#define ROW_WIDTH	12
#define COL_WIDTH	9
#define SDR_BL		2
#define T_RC			6
#define T_XSR			7
#define T_RCAR		7
#define T_WR			2
#define T_RP			2
#define T_RCD			2
#define T_RAS			5
#define SDR_BURST	3

#if(FREQUENCY_MHZ==500)
#define T_CL			3
#else
#define T_CL			2
#endif

#define SDRAM_START_ADDR	(0x80000000)
#define SDRAM_END_ADDR		(0x81000000)//16MB
#define SDRAM_LEN					(SDRAM_END_ADDR - SDRAM_START_ADDR)


extern void InitSdram(void);

#endif  /* __SDRAM_DRV_H__*/

