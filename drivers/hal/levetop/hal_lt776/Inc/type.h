/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    type.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of type.
  *
  ******************************************************************************
*/

#ifndef __TYPE_H__
#define __TYPE_H__

/*****************************************************************************/

#define __I  volatile const /*!< Defines 'read only' permissions                 */
#define __O  volatile       /*!< Defines 'write only' permissions                */
#define __IO volatile       /*!< Defines 'read / write' permissions              */

#define TRUE  1
#define FALSE 0
//#define NULL ((void *)0) /*!< Defines 'NULL' permissions */

/*****************************************************************************/
typedef unsigned char boolean;
typedef signed char   int8_t;
typedef signed short  int16_t;

typedef unsigned char  uint8_t;
typedef unsigned short uint16_t;

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;

typedef signed char        INT8;
typedef unsigned int       UINT32;
typedef unsigned short int UINT16;
typedef unsigned char      UINT8;

typedef volatile unsigned int vu32;
typedef volatile uint16_t     vu16;
typedef volatile uint8_t      vu8;

typedef float float32_t;

typedef unsigned char bool;

#ifndef __weak
#define __weak __attribute__((weak))
#endif /* __weak */

#if defined(__CC_ARM) || defined(__CLANG_ARM) /* ARM C Compiler */
typedef signed int   int32_t;
typedef unsigned int uint32_t;
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
typedef signed int   int32_t;
typedef unsigned int uint32_t;
#elif defined(__GNUC__)
#include "stdint.h"
#define ARMCM4_FP 1
#define __nop() __asm__ __volatile__ ("nop"::)
#ifndef WEAK
#define WEAK __attribute__((weak)) /**< Defines 'WEAK' permissions */
#endif                             /* WEAK */

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif /* __packed */
#endif

/*****************************************************************************/
typedef union
{
    uint16_t size;
    uint8_t  size_byte[2];
} uint16_8_union_t;

typedef union
{
    unsigned int size;
    uint16_t     size_short[2];
    uint8_t      size_byte[4];
} uint32_8_union_t;

/*****************************************************************************/
typedef enum
{
    BIT_RESET = 0,
    BIT_SET   = 1
} BitActionTypeDef;

typedef enum
{
    DISABLE = 0,
    ENABLE  = 1
} FunctionalStateTypeDef;

typedef enum
{
    RESET = 0,
    SET   = 1
} FlagStatusTypeDef;

/**
 * @brief	tatus structures definition
 */
typedef enum
{
    OK      = 0x00,
    ERROR   = 0x01,
    BUSY    = 0x02,
    TIMEOUT = 0x03,
} StatusTypeDef;

/*****************************************************************************/
#define GPIO_PinStateTypeDef FlagStatusTypeDef
#define FunctionalState      FunctionalStateTypeDef

#endif /* __TYPE_H__ */
