/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    eflash_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of eflash DRV module.
  * 
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EFLASH_DRV_H
#define __EFLASH_DRV_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"
#include "liblt776.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
  *
  *@{
  */

/** @defgroup DRV_EFLASH EFLASH
  *
  *@{
  */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EFLASH_Exported_Macros Exported Macros
  *
  * @{
  */
#endif

#define EFCRADD EFLASH_BASE_ADDR

#define writel(addr, value) (*(volatile unsigned long *)(addr)) = ((unsigned long)(value))
#define writew(addr, value) (*(volatile unsigned short *)(addr)) = ((unsigned short)(value))
#define writeb(addr, value) (*(volatile unsigned char *)(addr)) = ((unsigned char)(value))
#define readl(addr) (*(volatile unsigned long *)(addr))
#define readw(addr) (*(volatile unsigned short *)(addr))
#define readb(addr) (*(volatile unsigned char *)(addr))

#define EFLASH_WRITEABLE ENABLE
#define EFLASH_READABLE ENABLE
#define EFLASH_DISWRITEABLE DISABLE
#define EFLASH_DISREADABLE DISABLE

#define _eflash_set_rwsc(eflash, value) _reg_modify(eflash->EFCR, ~0x0000000F, value & 0x0F)
#define _eflash_set_tpws(eflash, value) _reg_modify(eflash->ETIMCFG, (~0x03C00000), ((value & 0x0F) << 22))
#define _eflash_set_verify_cnt(eflash, value) _reg_modify(eflash->SMWOP2, (~0x01F00000), ((value & 0x1F) << 20))
#define _eflash_clr_smw_err_flag(eflash) _bit_set(eflash->EFSTAT, 0x02020002)
#define _eflash_en_write_access(eflash) _reg_write(eflash->EFAPR, 0x9786ac02)
#define _eflash_dis_write_access(eflash) _bit_clr(eflash->EFAPR, 0x02)
#define _eflash_en_read_access(eflash) _reg_write(eflash->EFAPR, 0x9786ac01)
#define _eflash_dis_read_access(eflash) _bit_clr(eflash->EFAPR, 0x01)
#define _eflash_en_main_access(eflash) _reg_write(eflash->EFAPR, 0x9786ac03)
#define _eflash_dis_main_access(eflash) _reg_write(eflash->EFAPR, 0x9786ac01)
#define _eflash_get_smwop0(eflash) _reg_read(eflash->SMWOP0)

#define _eflash_set_ecc_err_cnt(cnt) _reg_modify(EFM->ETIMBASE, 0xFFFFFF00, (cnt))
#define _eflash_set_tnvh_cnt(cnt) _reg_modify(EFM->ETIMCFG, 0xFFFFFF0F, (cnt))
/**
  * @}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EFLASH_Exported_Types Exported Types
  *
  * @{
  */
#endif

  /**
* @brief  eflash函数接口定义
*
*/
  typedef struct
  {
    /* set eflash clock divider, configure the eflash timings. */
    void (*SetEFMClock)(uint32_t f_eflash_khz);
    /* eflash word program interface. */
    uint8_t (*ProgramWord)(uint32_t addr, uint32_t data, uint8_t intr_dis);
    /* eflash page erase interface. */
    uint8_t (*ErasePage)(uint32_t addr, uint8_t intr_dis);
    /* eflash line program interface. */
    uint8_t (*ProgramLine)(uint32_t addr, uint32_t num_words, uint32_t *data_buf, uint8_t intr_dis);
    /* eflash bulk program interface. */
    uint8_t (*ProgramBulk)(uint32_t addr, uint32_t num_words, uint32_t *data_buf, uint8_t intr_dis);
    /* the parameter is only used for program(2.7us per pulse) */
    uint32_t RegSMWOP0_2p7us;

  } EFLASH_OptFuncTypeDef;

/**
  * @}
  */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EFLASH_Exported_Variables Exported Variables
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EFLASH_Exported_Functions Exported Functions
  * @{
  */
#endif

  extern void DRV_EFM_WriteCmd(EFLASH_TypeDef *pefm, FunctionalStateTypeDef status);

  extern void DRV_EFM_ReadCmd(EFLASH_TypeDef *pefm, FunctionalStateTypeDef status);

  extern void DRV_EFM_MainCmd(EFLASH_TypeDef *pefm, FunctionalStateTypeDef status);

  extern void DRV_EFM_Init(uint32_t fclk);
  extern void DRV_EFLASH_SetWritePermission(void);

  //extern uint8_t EFlash_Recovery_to_Boot(void);

  extern uint8_t DRV_EFM_EreasePage(uint32_t address);

  extern uint8_t DRV_EFM_ProgramWord(uint32_t address, uint32_t data);

  extern uint8_t DRV_EFM_ProgramBulk(uint32_t address, uint8_t *buffer, uint32_t length);

  extern void DRV_EFM_Read(uint32_t address, uint8_t *buffer, uint32_t length);

  extern void EFLASH_Set_RWSC(uint8_t rwsc);
  extern uint8_t DRV_EFM_RecoveryToBoot(void);
  /**
  * @}
  */

  /**
  *@}
  */

  /**
  *@}
  */

#ifdef __cplusplus
}
#endif

#endif /* __EFLASH_HAL_H */

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE****/
