/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    edma_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of edma DRV module.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EDMAC_DRV_H
#define __EDMAC_DRV_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
  *
  *@{
  */

/** @defgroup DRV_EDMAC EDMAC
  *
  *@{
  */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EDMAC_Exported_Macros Exported Macros
  *
  * @{
  */
#endif

#define IS_EDAMC_DIR(dir)            (((dir == EDMAC_DIR_SRAM_TO_SRAM)) || \
                                      ((dir == EDMAC_DIR_SRAM_TO_PERIPHERAL)) || \
                                      ((dir == EDMAC_DIR_PERIPHERAL_TO_SRAM)) || \
                                      ((dir == EDMAC_DIR_BOTH)))
#define IS_EDMAC_PERIPH(periph)      ((((periph == EDMAC_CHANNEL_PERIPHERAL_SPI1)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_SPI2)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_USI1)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_USI2)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_AES)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_DES)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_SM1)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_SMS4)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_SHA)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_CRYPTO)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_UART1)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_UART2)) || \
                                       ((periph == EDMAC_CHANNEL_PERIPHERAL_SPI3)))

/**
  *@brief edmac寄存器级位操作宏
  *
  */
/*CR*/
#define _edmac_pipeline_en(edmac)                      _bit_set(edmac->CR,EDMAC_PIPELINE_EN)      /**< */
#define _edmac_pipeline_dis(edmac)                     _bit_clr(edmac->CR,EDMAC_PIPELINE_EN)      /**< */
#define _edmac_spi_hw_en(edmac)                        _bit_set(edmac->CR,EDMAC_SPI_HW)      /**< */
#define _edmac_spi_hw_dis(edmac)                       _bit_clr(edmac->CR,EDMAC_SPI_HW)      /**< */
#define _edmac_priority_change_en(edmac)               _bit_set(edmac->CR,EDMAC_PRIOR_CHG_EN)      /**< */
#define _edmac_priority_change_dis(edmac)              _bit_clr(edmac->CR,EDMAC_PRIOR_CHG_EN)      /**< */
#define _edmac_set_priroity_0(edmac)                   _bit_clr(edmac->CR,EDMAC_PRIOR)      /**< */
#define _edmac_set_priroity_1(edmac)                   _bit_set(edmac->CR,EDMAC_PRIOR)      /**< */
#define _edmac_spi_write_opt_en(edmac)                 _bit_set(edmac->CR,EDMAC_SPI_WROPT_EN)      /**< */
#define _edmac_spi_write_opt_dis(edmac)                _bit_clr(edmac->CR,EDMAC_SPI_WROPT_EN)      /**< */
#define _edmac_ininify_en(edmac)                       _bit_set(edmac->CR,EDMAC_INFINITY_EN)      /**< */
#define _edmac_ininify_dis(edmac)                      _bit_clr(edmac->CR,EDMAC_INFINITY_EN)      /**< */
#define _edmac_set_spi_fifo_pre_load3_1(edmac)         _bit_set(edmac->CR,EDMAC_SPI_FIFO_PRE_LOAD_3)      /**< */
#define _edmac_set_spi_fifo_pre_load3_0(edmac)         _bit_clr(edmac->CR,EDMAC_SPI_FIFO_PRE_LOAD_3)      /**< */
#define _edmac_maj_done_it_en(edmac)                   _bit_set(edmac->CR,EDMAC_MAJ_DONE_IT_EN)        /**< EMDAC MAJ DONE IT使能   */
#define _edmac_maj_done_it_dis(edmac)                  _bit_clr(edmac->CR,EDMAC_MAJ_DONE_IT_EN)        /**< EMDAC MAJ DONE IT禁止   */
#define _edmac_maj_crc_sel_maj(edmac)                  _bit_set(edmac->CR,EDMAC_MAJ_CRC_SEL_MAJOR)     /**< EMDAC                   */
#define _edmac_maj_crc_sel_min(edmac)                  _bit_clr(edmac->CR,EDMAC_MAJ_CRC_SEL_MINOR)     /**< EMDAC                   */
#define _edmac_crc_channel_en(edmac)                   _bit_set(edmac->CR,EDMAC_CRC_CHANNEL_EN)        /**< EMDAC                   */
#define _edmac_crc_channel_dis(edmac)                  _bit_clr(edmac->CR,EDMAC_CRC_CHANNEL_EN)       /**< EMDAC                   */
#define _edmac_send_crc_channel_en(edmac)              _bit_set(edmac->CR,EDMAC_SEND_CRC_CHANNEL_EN)   /**< EMDAC                   */
#define _edmac_send_crc_channel_dis(edmac)             _bit_clr(edmac->CR,EDMAC_SEND_CRC_CHANNEL_EN)  /**< EMDAC                   */
#define _edmac_write_crc_channel_en(edmac)             _bit_set(edmac->CR,EDMAC_WRITE_CRC_CHANNEL_EN)  /**< EMDAC                   */
#define _edmac_write_crc_channel_dis(edmac)            _bit_clr(edmac->CR,EDMAC_WRITE_CRC_CHANNEL_EN) /**< EMDAC                   */
#define _edmac_link_en(edmac)                          _bit_set(edmac->CR,EDMAC_LINK_EN)               /**< EMDAC                   */
#define _edmac_link_dis(edmac)                         _bit_clr(edmac->CR,EDMAC_LINK_EN)              /**< EMDAC                   */
#define _edmac_set_link_src(edmac,link_src)            _reg_modify(edmac->CR,EDMAC_LINK_SRC_SEL_MASK,link_src)      /**< */
#define _edmac_compare_en(edmac)                       _bit_set(edmac->CR,EDMAC_COMPARE_EN)            /**< EMDAC                   */
#define _edmac_compare_dis(edmac)                      _bit_clr(edmac->CR,EDMAC_COMPARE_EN)           /**< EMDAC                   */
#define _edmac_compare_skip_en(edmac)                  _bit_set(edmac->CR,EDMAC_COMPARE_SKIP_EN)      /**< */
#define _edmac_compare_skip_dis(edmac)                 _bit_clr(edmac->CR,EDMAC_COMPARE_SKIP_EN)      /**< */
#define _edmac_preload_en(edmac)                       _bit_set(edmac->CR,EDMAC_PRELOAD_EN)            /**< EMDAC                   */
#define _edmac_preload_dis(edmac)                      _bit_clr(edmac->CR,EDMAC_PRELOAD_EN)           /**< EMDAC                   */
#define _edmac_mem_trans_crc_en(edmac)                 _bit_set(edmac->CR,EDMAC_MEM_TRANS_CRC_EN)      /**< EMDAC                   */
#define _edmac_mem_trans_crc_dis(edmac)                _bit_clr(edmac->CR,EDMAC_MEM_TRANS_CRC_EN)     /**< EMDAC                   */
#define _edmac_start_it_en(edmac)                      _bit_set(edmac->CR,EDMAC_START_IT_EN)           /**< EMDAC 使能完成中断通道0 */
#define _edmac_start_it_dis(edmac)                     _bit_clr(edmac->CR,EDMAC_START_IT_DIS)          /**< EMDAC 禁止完成中断通道0 */
#define _edmac_set_ttype(edmac,ttype)                  _reg_modify(edmac->CR,EDMAC_DIR_MASK,ttype)      /**< */
#define _edmac_set_spi_fifo_preload02(edmac,preload)   _reg_modify(edmac->CR,EDMAC_SPI_FIFO_PRELOAD_MASK,preload)      /**< */
#define _edmac_valid(edmac)                            _bit_set(edmac->CR,EDMAC_VALID_FLAG)            /**< EMDAC 置位VALID0        */
#define _edmac_min_done_it_en(edmac)                   _bit_set(edmac->CR,EDMAC_MIN_DONE_IT_EN)        /**< EMDAC MIN DONE IT使能   */
#define _edmac_min_done_it_dis(edmac)                  _bit_clr(edmac->CR,EDMAC_MIN_DONE_IT_EN)       /**< EMDAC MIN DONE IT禁止   */
#define _edmac_get_min_done_it_ie(edmac)               _reg_chk(edmac->CR,EDMAC_MIN_DONE_IT_EN)   /**<获取 EMDAC MIN DONE IT ie   */
#define _edmac_maj_done_it_en(edmac)                   _bit_set(edmac->CR,EDMAC_MAJ_DONE_IT_EN)        /**< EMDAC MAJ DONE IT使能   */
#define _edmac_maj_done_it_dis(edmac)                  _bit_clr(edmac->CR,EDMAC_MAJ_DONE_IT_EN)       /**< EMDAC MAJ DONE IT禁止   */
#define _edmac_get_maj_done_it_ie(edmac)               _reg_chk(edmac->CR,EDMAC_MAJ_DONE_IT_EN)   /**<获取 EMDAC MIN DONE IT ie   */
/*CSR*/
#define _edmac_get_ssf_spi1(edmac)                      _reg_chk(edmac->CSR,EDMAC_SSF_SPI1_FLAG)      /**< */
#define _edmac_get_ssf_spi2(edmac)                      _reg_chk(edmac->CSR,EDMAC_SSF_SPI2_FLAG)      /**< */
#define _edmac_get_ssf_spi3(edmac)                      _reg_chk(edmac->CSR,EDMAC_SSF_SPI3_FLAG)      /**< */
#define _edmac_get_ssf_spi4(edmac)                      _reg_chk(edmac->CSR,EDMAC_SSF_SPI4_FLAG)      /**< */
#define _edmac_clear_ssf_spi1(edmac)                    _bit_set(edmac->CSR,EDMAC_SSF_SPI1_FLAG)      /**< */
#define _edmac_clear_ssf_spi2(edmac)                    _bit_set(edmac->CSR,EDMAC_SSF_SPI2_FLAG)      /**< */
#define _edmac_clear_ssf_spi3(edmac)                    _bit_set(edmac->CSR,EDMAC_SSF_SPI3_FLAG)      /**< */
#define _edmac_clear_ssf_spi4(edmac)                    _bit_set(edmac->CSR,EDMAC_SSF_SPI4_FLAG)      /**< */
#define _edmac_dummy_en(edmac)                          _bit_set(edmac->CR,EDMAC_DUMMY_EN)      /**< */
#define _edmac_dummy_dis(edmac)                         _bit_clr(edmac->CR,EDMAC_DUMMY_EN)      /**< */
#define _edmac_en(edmac)                                _bit_set(edmac->CSR,EDMAC_EN)                   /**< EMDAC 使能              */
#define _edmac_dis(edmac)                               _bit_clr(edmac->CSR,EDMAC_EN)                  /**< EMDAC 禁止              */
#define _edmac_get_fail_flag(edmac)                     _reg_chk(edmac->CSR,EDMAC_FLAG_FAIL)             /**< EMDAC 获取标志:FAILE    */
#define _edmac_get_start_ch_flag(edmac)                 _reg_chk(edmac->CSR,EDMAC_FLAG_SCHNUM)           /**< EMDAC 获取标志:SCHNUM   */
#define _edmac_get_done_ch_flag(edmac)                  _reg_chk(edmac->CSR,EDMAC_FLAG_DCHNUM)           /**< EMDAC 获取标志:DCHNUM   */
#define _edmac_get_maj_done_flag(edmac)                 _reg_chk(edmac->CSR,EDMAC_FLAG_MAJ_DONE)         /**< EMDAC 获取标志:MAJ_DONE */
#define _edmac_get_start_flag(edmac)                    _reg_chk(edmac->CSR,EDMAC_FLAG_START)            /**< EMDAC 获取标志:START    */
#define _edmac_get_min_done_flag(edmac)                 _reg_chk(edmac->CSR,EDMAC_FLAG_MIN_DONE)         /**< EMDAC 获取标志:MIN_DONE */
#define _edmac_get_busy_flag(edmac)                     _reg_chk(edmac->CSR,EDMAC_FLAG_BUSY)             /**< EMDAC 获取标志:BUSY     */
#define _edmac_clear_fail_flag(edmac)                   _bit_set(edmac->CSR,EDMAC_FLAG_FAIL)            /**< EMDAC  clear flag:FAIL  */
#define _edmac_clear_start_flag(edmac)                  _bit_set(edmac->CSR,EDMAC_FLAG_FAIL)            /**< EMDAC  clear flag:START */
#define _edmac_clear_busy_flag(edmac)                   _bit_set(edmac->CSR,EDMAC_FLAG_FAIL)            /**< EMDAC  clear flag:BUSY  */
#define _edmac_clear_maj_done_flag(edmac)               _bit_set(edmac->CSR,EDMAC_FLAG_MAJ_DONE)         /**< EMDAC 获取标志:MAJ_DONE */
#define _edmac_clear_min_done_flag(edmac)               _bit_set(edmac->CSR,EDMAC_FLAG_MIN_DONE)         /**< EMDAC 获取标志:MIN_DONE */
/*RBAR0*/
#define _edmac_read_dec_en(edmac)                      _bit_clr(edmac->RBAR,EDMAC_READ_BUFFER_DEC_EN) /**< 读地址增使能            */
#define _edmac_read_dec_dis(edmac)                     _bit_set(edmac->RBAR,EDMAC_READ_BUFFER_DEC_EN)  /**< 读地址增禁止            */
#define _edmac_read_inc_en(edmac)                      _bit_clr(edmac->RBAR,EDMAC_READ_BUFFER_INC_EN) /**< 读地址增使能            */
#define _edmac_read_inc_dis(edmac)                     _bit_set(edmac->RBAR,EDMAC_READ_BUFFER_INC_EN)  /**< 读地址增禁止            */
#define _edmac_set_read_buffer_addr(edmac,addr)        _reg_modify(edmac->RBAR,EDMAC_READ_BUFFER_BASE_ADD_MASK, addr)
/*WBAR0*/
#define _edmac_write_dec_en(edmac)                     _bit_clr(edmac->WBAR,EDMAC_WRITE_BUFFER_DEC_EN)/**< 写地址增使能            */
#define _edmac_write_dec_dis(edmac)                    _bit_set(edmac->WBAR,EDMAC_WRITE_BUFFER_DEC_EN) /**< 写地址增禁止            */
#define _edmac_write_inc_en(edmac)                     _bit_clr(edmac->WBAR,EDMAC_WRITE_BUFFER_INC_EN)/**< 写地址增使能            */
#define _edmac_write_inc_dis(edmac)                    _bit_set(edmac->WBAR,EDMAC_WRITE_BUFFER_INC_EN) /**< 写地址增禁止            */
#define _edmac_set_write_buffer_addr(edmac,addr)       _reg_modify(edmac->WBAR,EDMAC_WRITE_BUFFER_BASE_ADD_MASK, addr)
/*MINSUMR*/
#define _edmac_set_minor_sum(edmac, len)                _reg_write(edmac->MINSUMR, len)
#define _edmac_set_major_sum(edmac, len)                _reg_write(edmac->MAJSUMR, len)
#define _edmac_get_minor_cnt(edmac)                     _reg_read(edmac->MINCNTR)
#define _edmac_get_major_cnt(edmac)                     _reg_read(edmac->MAJCNTR)
/**
  * @}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EDMAC_Exported_Types Exported Types
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EDMAC_Exported_Variables Exported Variables
  *
  * @{
  */
#endif


/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EDMAC_Exported_Functions Exported Functions
  * @{
  */
#endif
extern void DRV_EDMAC_CtrlRegConfig(EDMAC_TypeDef *edmac,                   
                                         FunctionalStateTypeDef compare_en,  
                                         FunctionalStateTypeDef compare_skip,  
                                         FunctionalStateTypeDef preload_en,   
                                         uint8_t perip_num,                   
                                         uint8_t transfer_type);

extern void DRV_EDMAC_WriteAndReadBufferAdderAddConfig(EDMAC_TypeDef *edmac, uint8_t write_buf_add, uint8_t read_buf_add);


extern void DRV_EDMAC_ResetReg(EDMAC_TypeDef *edmacx);

/**
  * @}
  */

/**
  *@}
  */

/**
  *@}
  */

#ifdef __cplusplus
}
#endif

#endif /* __EDMA_HAL_H */

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE****/


