/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			misc.h
 * @Author		Jason Zhao
 * @Date			2021/12/2
 * @Time			14:41:34
 * @Version
 * @Brief
 **********************************************************
 * Modification History
 *
 *
 **********************************************************/
#ifndef __MISC_H__
#define __MISC_H__

#include "lt776.h"
#include "cpm_drv.h"

/**
 * @brief CPM 模块时钟索引定义
 *
 */
typedef enum
{
    /*multiple clock gate control register:MULTICGTCR*/
    MODULE_CLK_EFM_BUS  = 1,  /**< */
    MODULE_CLK_MCC      = 5,  /**< */
    MODULE_CLK_MCCADR   = 6,  /**< */
    MODULE_CLK_ADC      = 7,  /**< */
    MODULE_CLK_MESH     = 9,  /**< */
    MODULE_CLK_TC       = 10, /**< */
    MODULE_CLK_CLKOUT   = 15, /**< */
    MODULE_CLK_KEY_CTRL = 16, /**< */
    MODULE_CLK_EFM_IPS  = 18, /**< */
    MODULE_CLK_CPM_IPS  = 19, /**< */
    MODULE_CLK_EPORT    = 20, /**< */
    MODULE_CLK_EPORT1   = 21, /**< */
    MODULE_CLK_EPORT2   = 22, /**< */
    MODULE_CLK_EPORT3   = 23, /**< */
    MODULE_CLK_EPORT4   = 24, /**< */
    MODULE_CLK_TRACE    = 26, /**< */

    /*system clock gate control register:SYSCGTCR*/
    MODULE_CLK_DMAC1     = 1 + 32,  /**< */
    MODULE_CLK_DMAC2     = 2 + 32,  /**< */
    MODULE_CLK_CRC0      = 4 + 32,  /**< */
    MODULE_CLK_CRC1      = 5 + 32,  /**< */
    MODULE_CLK_AHB2_MUX  = 11 + 32, /**< */
    MODULE_CLK_SRAMD     = 12 + 32, /**< */
    MODULE_CLK_SRAM0     = 13 + 32, /**< */
    MODULE_CLK_SRAM1     = 14 + 32, /**< */
    MODULE_CLK_SRAM2     = 15 + 32, /**< */
    MODULE_CLK_SRAM3     = 16 + 32, /**< */
    MODULE_CLK_SSI4      = 17 + 32, /**< */
    MODULE_CLK_SSI5      = 18 + 32, /**< */
    MODULE_CLK_ROM       = 19 + 32, /**< */
    MODULE_CLK_M2S_BUS_M = 22 + 32, /**< */

    /*ahb3 clock gate enable control register:AHB3CGTCR*/
    MODULE_CLK_USBC     = 3 + 64, /**< */
    MODULE_CLK_AHB3_MUX = 5 + 64, /**< */

    /*arith clock gate control register:ARITHCGTCR*/
    MODULE_CLK_AES      = 1 + 96,  /**< */
    MODULE_CLK_SM4      = 2 + 64,  /**< */
    MODULE_CLK_RF       = 4 + 96,  /**< */
    MODULE_CLK_CRYPTO   = 5 + 64,  /**< */
    MODULE_CLK_SHA      = 6 + 96,  /**< */
    MODULE_CLK_EDMAC0   = 7 + 64,  /**< */
    MODULE_CLK_DES      = 8 + 96,  /**< */
    MODULE_CLK_ZUC      = 9 + 64,  /**< */
    MODULE_CLK_AHB2MLB  = 10 + 96, /**< */
    MODULE_CLK_AHB2IPS2 = 11 + 64, /**< */

    /*ips clock gate control register:IPSCGTCR*/
    MODULE_CLK_IO_CTRL   = 0 + 128,  /**< */
    MODULE_CLK_WDT       = 1 + 128,  /**< */
    MODULE_CLK_RTC       = 2 + 128,  /**< */
    MODULE_CLK_PIT1      = 3 + 128,  /**< */
    MODULE_CLK_PIT2      = 4 + 128,  /**< */
    MODULE_CLK_USI1      = 5 + 128,  /**< */
    MODULE_CLK_EDMAC1    = 6 + 128,  /**< */
    MODULE_CLK_SPI1      = 7 + 128,  /**< */
    MODULE_CLK_SPI2      = 8 + 128,  /**< */
    MODULE_CLK_SPI3      = 9 + 128,  /**< */
    MODULE_CLK_SCI1      = 10 + 128, /**< */
    MODULE_CLK_SCI2      = 11 + 128, /**< */
    MODULE_CLK_USI2      = 12 + 128, /**< */
    MODULE_CLK_I2C1      = 14 + 128, /**< */
    MODULE_CLK_PWM0      = 15 + 128, /**< */
    MODULE_CLK_I2C2      = 16 + 128, /**< */
    MODULE_CLK_I2C3      = 17 + 128, /**< */
    MODULE_CLK_SCI3      = 18 + 128, /**< */
    MODULE_CLK_QADC      = 20 + 128, /**< */
    MODULE_CLK_DAC       = 21 + 128, /**< */
    MODULE_CLK_MCC_2     = 22 + 128, /**< */
    MODULE_CLK_TSI       = 23 + 128, /**< */
    MODULE_CLK_LD        = 24 + 128, /**< */
    MODULE_CLK_TRNG      = 25 + 128, /**< */
    MODULE_CLK_PGD       = 26 + 128, /**< */
    MODULE_CLK_SEC_DET   = 27 + 128, /**< */
    MODULE_CLK_PCI       = 28 + 128, /**< */
    MODULE_CLK_PMURTC    = 29 + 128, /**< */
    MODULE_CLK_AHB2IPS   = 30 + 128, /**< */
    MODULE_CLK_CCM_RESET = 31 + 128, /**< */
} CPM_ModuleClkIndexTypeDef;
/**
 * @brief CPM时钟源索引定义
 *
 */
typedef enum
{
    HAL_CHIP_SYS_CLK_INDEX_CLKOUT = 0,
    HAL_CHIP_SYS_CLK_INDEX_TRACE,
    HAL_CHIP_SYS_CLK_INDEX_SYS,
    HAL_CHIP_CORE_CLK_INDEX_SYS,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_ARITH,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_AHB3,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_IPS,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_TC,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_MESH,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_ADC,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_MCC_ADR,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_MCC,

    HAL_CHIP_PERIPHERAL_CLK_INDEX_SDRAM2LCD,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_SDRAM_SM,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_SDRAM,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_CLCD,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_SD_HOST,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_DCMI_SENSOR,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_DCMI_PIX,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_MIPI_SAMPLE,
    HAL_CHIP_PERIPHERAL_CLK_INDEX_DMA2D_SRAM

} CPM_ClkSourceIndexTypeDef;

typedef enum
{
    CPM_SYSCLK_OSC8M = 0,
    CPM_SYSCLK_OSC400M,
    CPM_SYSCLK_USBPHY480M,
    CPM_SYSCLK_OSCEXT,
    CPM_SYSCLK_OSC500M
} CPM_SysClkSelTypeDef;
typedef enum
{
    OSC_108M_HZ = 0,
    OSC_120M_HZ,
    OSC_150M_HZ,
    OSC_160M_HZ,
} CPM_SysClkTrimTypeDef;
typedef enum
{
    CLK_DIV_1 = 0,
    CLK_DIV_2,
    CLK_DIV_3,
    CLK_DIV_4,
    CLK_DIV_5,
    CLK_DIV_6,
    CLK_DIV_7,
    CLK_DIV_8,
    CLK_DIV_9,
    CLK_DIV_10,
    CLK_DIV_11,
    CLK_DIV_12,
    CLK_DIV_13,
    CLK_DIV_14,
    CLK_DIV_15,
    CLK_DIV_16,
    CLK_DIV_17,
    CLK_DIV_18,
    CLK_DIV_19,
    CLK_DIV_20
} CPM_SysClkDivTypeDef,
    CPM_IpsClkDivTypeDef;

typedef struct
{
    CPM_SysClkSelTypeDef  SysClkSource;
    CPM_SysClkTrimTypeDef SysClkTrim;
    CPM_SysClkDivTypeDef  CoreClkDiv;
    CPM_SysClkDivTypeDef  SysClkDiv;
    CPM_IpsClkDivTypeDef  IpsClkDiv;
    CPM_IpsClkDivTypeDef  SdramClkDiv;
} CPM_ClkInitTypeDef;

extern void SetClkDiv(CPM_ClkSourceIndexTypeDef ClkSourceIndex, uint8_t Div);
extern void InitChip(void);
extern void delay(volatile uint32_t time);

#endif /* __MISC_H__*/
