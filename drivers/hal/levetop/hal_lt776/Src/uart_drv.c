/**
  **********************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  **********************************************************************************
  *@file    uart_drv.c
  *@author  Product application department
  *@version V1.0
  *@date    2021.11.01
  *@brief   UART模块DRV层驱动.
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
*/

#include "uart_drv.h"
#include "sys.h"

/*** 全局变量定义 ***************************************************************/
// extern UART_HandleTypeDef UART1_Handle;
/*** volatile **********/

/*** @ static **********/

/*** 常量定义 *******************************************************************/

/*** 函数定义 *******************************************************************/

static UartRecvBufStruct  g_Uart1RecvBufStruct = {0, 0};    // uart1 buffer
static UartRecvBufStruct  g_Uart2RecvBufStruct = {0, 0};    // uart2 buffer
static UartRecvBufStruct  g_Uart3RecvBufStruct = {0, 0};    // uart3 buffer
static UartRecvBufStruct  g_Uart4RecvBufStruct = {0, 0};    // uart3 buffer
static UartRecvBufStruct *UARTxRecvBufStruct;

/*******************************************************************************
 * Function Name  : UART_ISR
 * Description    : UART中断处理
 * Input          : None
 *
 * Output         : None
 * Return         : None
 ******************************************************************************/
void UART_ISR(UART_TypeDef *UARTx)
{
    if ((UARTx->SR1 & 0x20) == 0x20)
    {
        UARTxRecvBufStruct->dat[UARTxRecvBufStruct->wp] = UARTx->DRL;
        UARTxRecvBufStruct->wp += 1;
        if (UARTxRecvBufStruct->wp >= UART_RECV_MAX_LEN)
            UARTxRecvBufStruct->wp = 0;
    }
}

/*******************************************************************************
 * Function Name  : UART_RecvByte
 * Description    : UART接收一个字节
 * Input          : - UARTx:取值SCI2 or SCI1
 *
 *
 * Output         : - dat：接收数据缓冲
 *
 * Return         : - TRUE:接收到数据
 *                  - FALSE:没有接收到数据
 ******************************************************************************/
INT8 UART_RecvByte(UART_TypeDef *UARTx, UINT8 *dat)
{
    if (UARTx == UART1)
    {
        UARTxRecvBufStruct = &g_Uart1RecvBufStruct;
    }
    else
    {
        if (UARTx == UART2)
        {
            UARTxRecvBufStruct = &g_Uart2RecvBufStruct;
        }
        else if (UARTx == UART3)
        {
            UARTxRecvBufStruct = &g_Uart3RecvBufStruct;
        }
        else
        {
            UARTxRecvBufStruct = &g_Uart4RecvBufStruct;
        }
    }
    if (UARTxRecvBufStruct->wp != UARTxRecvBufStruct->rp)
    {
        *dat = UARTxRecvBufStruct->dat[UARTxRecvBufStruct->rp];
        UARTxRecvBufStruct->rp += 1;
        if (UARTxRecvBufStruct->rp >= UART_RECV_MAX_LEN)
            UARTxRecvBufStruct->rp = 0;
        return TRUE;
    }
    return FALSE;
}

/*******************************************************************************
 * Function Name  : UART_SendByte
 * Description    : UART发送一个字节
 * Input          : - UARTx: 取值SCI2 or SCI_BT
 *                  - SendByte：发送的字节
 *
 * Output         : None
 * Return         : None
 ******************************************************************************/
void UART_SendByte(UART_TypeDef *UARTx, UINT8 SendByte)
{
    UARTx->CR2 |= UART_TE;
    while ((UARTx->SR1 & UART_IT_FLAG_TDRE) == 0)
        ;
    UARTx->DRL = SendByte & 0xff;
    while ((UARTx->SR1 & UART_IT_FLAG_TC) == 0)
        ;
    UARTx->CR2 &= ~UART_TE;
}

/**
 *@brief 初始化UART模块的设置.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] Init  UART_InitTypeDef结构体，包含初始化信息;
 *@return 无
 *@note 波特率不可为0;
 */
void DRV_UART_Init(UART_TypeDef *puart, UART_InitTypeDef Init)
{
    uint8_t  reg = 0;
    uint16_t bd  = 0;

    /*9bits or 8bits*/
    if (Init.WordLength == UART_DATA_FORMAT_MODE_9BITS)
    {
        reg = UART_DATA_FORMAT_MODE_9BITS;
    }

    /*parity*/
    if (Init.Parity == UART_PARITY_MODE_EVEN)
    {
        reg |= UART_PARITY_MODE_EVEN;
    }
    else if (Init.Parity == UART_PARITY_MODE_ODD)
    {
        reg |= UART_PARITY_MODE_ODD;
    }

    /*CR1*/
    puart->CR1 = reg;

    /*baudrate*/
    puart->BRDF = (((Init.IPSFreq * 8 / Init.BaudRate) + 1) / 2) & 0x003f;

    bd         = (Init.IPSFreq * 4 / Init.BaudRate) >> 6;
    puart->BDH = (uint8_t)((bd >> 8) & 0x00ff);
    puart->BDL = (uint8_t)((bd)&0x00ff);

    puart->CR2 = 0;
    puart->CR2 |= UART_RE | UART_TE;
}
/**
 *@brief 恢复UART模块的设置.
 *
 *@param[in] piic 指向IIC_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_Deinit(UART_TypeDef *puart)
{
    puart->CR1 = 0;
    puart->CR2 = 0;
}
/**
 *@brief 查询方式发送单个字节数据.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SendByte(UART_TypeDef *puart, uint8_t outbyte)
{
    while (0 == (puart->SR1 & UART_IT_FLAG_TDRE))
    {
        ;
    }

    puart->DRL = outbyte;

    while (0 == (puart->SR1 & UART_IT_FLAG_TC))
    {
        ;
    }
}
/**
 *@brief 查询方式发送指定长度字节数据.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] pbuf  指向待发送数据指针;
 *@param[in] pbuf  待发送数据长度;
 *@return 无
 */
void DRV_UART_SendBytes(UART_TypeDef *puart, uint8_t *pbuf, uint16_t len)
{
    uint16_t loop = 0;

    for (loop = 0; loop < len; loop++)
    {
        DRV_UART_SendByte(puart, *(pbuf + loop));
    }
}
/**
 *@brief 使能发送.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_EnTx(UART_TypeDef *puart)
{
    _uart_en_txd(puart);
}
/**
 *@brief 使能接收.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_EnRx(UART_TypeDef *puart)
{
    _uart_en_rxd(puart);
}
/**
 *@brief 禁止发送.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_DisTx(UART_TypeDef *puart)
{
    _uart_dis_txd(puart);
}
/**
 *@brief 禁止接收.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */

void DRV_UART_DisRx(UART_TypeDef *puart)
{
    _uart_dis_rxd(puart);
}
/**
 *@brief 禁止发送和接收.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */

void DRV_UART_Dis(UART_TypeDef *puart)
{
    DRV_UART_DisTx(puart);
    DRV_UART_DisRx(puart);
}
/**
 *@brief 使能发送和接收.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_En(UART_TypeDef *puart)
{
    DRV_UART_EnTx(puart);
    DRV_UART_EnRx(puart);
}
/**
 *@brief 查询方式发送单个字节数据的高和低4位ASCII格式.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */

static void DRV_UART_SendOne(UART_TypeDef *puart, uint8_t send)
{
    if (send <= 0x09)
    {
        DRV_UART_SendByte(puart, send + 0x30);
    }

    else if ((send > 0x09) && (send <= 0x0f))
    {
        DRV_UART_SendByte(puart, send - 0x0a + 'A');
    }
}
/**
 *@brief 查询方式发送单个字节数据,ASCII 16进制格式.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SendByteHex(UART_TypeDef *puart, uint8_t send)
{
    uint8_t high;
    uint8_t low;

    high = send >> 4;
    low  = send & 0x0f;

    DRV_UART_SendOne(puart, high);
    DRV_UART_SendOne(puart, low);
}
/**
 *@brief 查询方式发送字符串.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] 字符串指针;
 *@return 无
 */
void DRV_UART_SendString(UART_TypeDef *puart, char *str)
{
    while (*str)
    {
        DRV_UART_SendByte(puart, *str);
        str++;
    }
}
/**
 *@brief 禁止TIE中断.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */

void DRV_UART_DisItTie(UART_TypeDef *puart)
{
    _uart_dis_tdre_it(puart);
}
/**
 *@brief 禁止TC中断.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */

void DRV_UART_DisItTcie(UART_TypeDef *puart)
{
    _uart_dis_tc_it(puart);
}

/**
 *@brief 禁止RE中断.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */

void DRV_UART_DisItRe(UART_TypeDef *puart)
{
    _uart_dis_rx_it(puart);
}
/**
 *@brief 使能指定中断.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] index 中断索引;
 *- UART_FLAG_INDEX_PF;
 *- UART_FLAG_INDEX_FE;
 *- UART_FLAG_INDEX_NF;
 *- UART_FLAG_INDEX_OR;
 *- UART_FLAG_INDEX_IDLE;
 *- UART_FLAG_INDEX_RDRF;
 *- UART_FLAG_INDEX_TC;
 *- UART_FLAG_INDEX_TDRE;
 *- UART_FLAG_INDEX_RAF;
 *@return 无
 */
void DRV_UART_EnIt(UART_TypeDef *puart, UART_ItIndexTypeDef index)
{
    puart->CR2 |= index;
}
/**
 *@brief 禁止指定中断.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] index 中断索引;
 *- UART_FLAG_INDEX_PF;
 *- UART_FLAG_INDEX_FE;
 *- UART_FLAG_INDEX_NF;
 *- UART_FLAG_INDEX_OR;
 *- UART_FLAG_INDEX_IDLE;
 *- UART_FLAG_INDEX_RDRF;
 *- UART_FLAG_INDEX_TC;
 *- UART_FLAG_INDEX_TDRE;
 *- UART_FLAG_INDEX_RAF;
 *@return 无
 */
void DRV_UART_DisIt(UART_TypeDef *puart, UART_ItIndexTypeDef index)
{
    puart->CR2 &= ~index;
}
/**
  *@brief 获取指令标志状态.
  *
  *@param[in] puart 指向UART_TypeDef结构体的指针;
  *@param[in] index 中断索引;
    *- UART_FLAG_INDEX_PF;
    *- UART_FLAG_INDEX_FE;
    *- UART_FLAG_INDEX_NF;
    *- UART_FLAG_INDEX_OR;
    *- UART_FLAG_INDEX_IDLE;
    *- UART_FLAG_INDEX_RDRF;
    *- UART_FLAG_INDEX_TC;
    *- UART_FLAG_INDEX_TDRE;
    *- UART_FLAG_INDEX_RAF;
  @param[out] flag 状态指值;
  *@return 获取结果 @ref DRV_UART_StatusTypeDef
  */
DRV_UART_StatusTypeDef DRV_UART_GetFlag(UART_TypeDef *puart, UART_FlagIndexTypeDef index, FlagStatusTypeDef *flag)
{
    uint8_t tmp = 0;

    *flag = RESET;

    if (index != UART_FLAG_INDEX_RAF)
    {
        tmp = puart->SR1 & (1 << index);
    }

    else
    {
        tmp = puart->SR2 & (1);
    }

    *flag = tmp ? SET : RESET;

    return DRV_UART_OK;
}
/**
 *@brief 清除指定标志.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] index 中断索引;
 *- UART_FLAG_INDEX_PF;
 *- UART_FLAG_INDEX_FE;
 *- UART_FLAG_INDEX_NF;
 *- UART_FLAG_INDEX_OR;
 *- UART_FLAG_INDEX_IDLE;
 *- UART_FLAG_INDEX_RDRF;
 *- UART_FLAG_INDEX_TC;
 *- UART_FLAG_INDEX_TDRE;
 *- UART_FLAG_INDEX_RAF;
 *@return 获取结果 @ref DRV_UART_StatusTypeDef
 */
DRV_UART_StatusTypeDef DRV_UART_ClearFlag(UART_TypeDef *puart, UART_FlagIndexTypeDef index)
{
    __attribute__((unused)) uint8_t reg;

    if (index != UART_FLAG_INDEX_RAF)
    {
        reg = puart->SR1;
        puart->SR1 &= ~(1 << index);
    }
    else
    {
        reg = puart->SR2;
        puart->SR2 &= ~(0X01);
    }

    return DRV_UART_OK;
}
/**
*@brief 超时方式获取指定标志SET或RESET状态.
*
*@param[in] puart 指向UART_TypeDef结构体的指针;
*@param[in] index 中断索引;
 *- UART_FLAG_INDEX_PF;
 *- UART_FLAG_INDEX_FE;
 *- UART_FLAG_INDEX_NF;
 *- UART_FLAG_INDEX_OR;
 *- UART_FLAG_INDEX_IDLE;
 *- UART_FLAG_INDEX_RDRF;
 *- UART_FLAG_INDEX_TC;
 *- UART_FLAG_INDEX_TDRE;
 *- UART_FLAG_INDEX_RAF;
@param[in] status 状态值;
*- SET;
*- RESE;
*@param[in] timeout 超时时间;
*@return 获取结果 @ref DRV_UART_StatusTypeDef
*/
DRV_UART_StatusTypeDef
DRV_UART_WaitonFlagTimeout(UART_TypeDef *puart, UART_FlagIndexTypeDef index, FlagStatusTypeDef status, uint32_t timeout)
{
    FlagStatusTypeDef tmp_flag = RESET;

    if (timeout == 0)
    {
        do
        {
            DRV_UART_GetFlag(puart, index, &tmp_flag);
        } while (tmp_flag != SET);
    }
    else
    {
        if (status == SET)
        {
            DRV_UART_GetFlag(puart, index, &tmp_flag);

            while (tmp_flag != SET)
            {
                timeout--;

                if (timeout == 0)
                {
                    return (DRV_UART_TIMEOUT);
                }

                DRV_UART_GetFlag(puart, index, &tmp_flag);
            }
        }
        else if (status == RESET)
        {
            DRV_UART_GetFlag(puart, index, &tmp_flag);

            while (tmp_flag != RESET)
            {
                timeout--;

                if (timeout == 0)
                {
                    return (DRV_UART_TIMEOUT);
                }

                DRV_UART_GetFlag(puart, index, &tmp_flag);
            }
        }
    }

    return DRV_UART_OK;
}
/**
  *@brief 查询方式发送数据.
  *
  *@param[in] puart  指向UART_TypeDef结构体的指针;
  *@param[in] tosend 待发送数据
  *@param[in] bits   数据长度单位bit;

  *@return 操作结果 @ref DRV_UART_StatusTypeDef
  */
DRV_UART_StatusTypeDef DRV_UART_SendData(UART_TypeDef *puart, uint16_t tosend, uint8_t bits)
{
    if (bits == UART_WORDLENGTH_8B)
    {
        puart->DRL = (uint8_t)tosend;
    }
    else if (bits == UART_WORDLENGTH_9B)
    {
        /*校验*/
    }

    return (DRV_UART_OK);
}
/**
 *@brief 查询方式发送数据.
 *
 *@param[in]  puart  指向UART_TypeDef结构体的指针;
 *@param[out] pres   读取数据指针;
 *@param[in]  bits   数据长度单位bit;
 *- UART_WORDLENGTH_8B;
 *- UART_WORDLENGTH_9B;
 *@return 操作结果 @ref DRV_UART_StatusTypeDef
 */
DRV_UART_StatusTypeDef DRV_UART_GetData(UART_TypeDef *puart, uint16_t *pres, uint8_t bits)
{
    uint16_t tmp_val = 0;

    if (bits == UART_WORDLENGTH_8B)
    {
        tmp_val = puart->DRL;
    }
    else if (bits == UART_WORDLENGTH_9B)
    {
        tmp_val = puart->DRH;
        tmp_val <<= 8;
        tmp_val |= puart->DRL;
    }
    *pres = tmp_val;

    return (DRV_UART_OK);
}
/**
 *@brief 获取模块状态.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[out] pstatus1 状态1指针;
 *@param[out] pstatus2 状态1指针;
 *@return 获取结果 @ref DRV_UART_StatusTypeDef
 */
DRV_UART_StatusTypeDef DRV_UART_GetStatus(UART_TypeDef *puart, uint8_t *pstatus1, uint8_t *pstatus2)
{
    *pstatus1 = puart->SR1;
    *pstatus2 = puart->SR2;

    return (DRV_UART_OK);
}
/**
 *@brief 指定中源断是否置位.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] index 中断源过引;
 *- UART_IT_INDEX_TDRE;
 *- UART_IT_INDEX_TCIE;
 *- UART_IT_INDEX_RIE ;
 *- UART_IT_INDEX_ILIE;
 *@return 获取结果 @ref FlagStatusTypeDef
 */
FlagStatusTypeDef DRV_UART_GetItSource(UART_TypeDef *puart, UART_ItIndexTypeDef index)
{
    uint8_t tmp_val = 0x00;

    tmp_val = puart->CR2;

    if (tmp_val & index)
    {
        return (SET);
    }
    else
    {
        return (RESET);
    }
}
/** GPIO *************************************************************************/
/**
 *@brief TXD管脚方向输出.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SetTxdDirOutput(UART_TypeDef *puart)
{
    _uart_txd_output(puart);
}
/**
 *@brief TXD管脚方向输入.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SetTxdDirInput(UART_TypeDef *puart)
{
    _uart_txd_input(puart);
}
/**
 *@brief RXD管脚方向输出.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SetRxdDirOutput(UART_TypeDef *puart)
{
    _uart_rxd_output(puart);
}
/**
 *@brief RXD管脚方向入.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SetRxdDirInput(UART_TypeDef *puart)
{
    _uart_rxd_input(puart);
}
/**
 *@brief 使能TXD/RXD管脚上拉.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_EnPullup(UART_TypeDef *puart)
{
    _uart_en_pullup(puart);
}
/**
 *@brief 禁止TXD/RXD管脚上拉.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_DisPullup(UART_TypeDef *puart)
{
    _uart_dis_pullup(puart);
}
/**
 *@brief TXD/RXD管脚OPENDRAIN输出.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SetOutputOpendrain(UART_TypeDef *puart)
{
    _uart_pin_output_opendrain(puart);
}
/**
 *@brief TXD/RXD管脚CMOS输出.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SetOutputCmos(UART_TypeDef *puart)
{
    _uart_pin_output_cmos(puart);
}
/**
 *@brief 置位TXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_SetTxdPinBit(UART_TypeDef *puart)
{
    _uart_set_tx_bit(puart);
}
/**
 *@brief 复位TXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_ResetTxdPinBit(UART_TypeDef *puart)
{
    _uart_reset_tx_bit(puart);
}
/**
 *@brief 置位RXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */

void DRV_UART_SetRxdPinBit(UART_TypeDef *puart)
{
    _uart_set_rx_bit(puart);
}
/**
 *@brief 复位RXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_ResetRxdPinBit(UART_TypeDef *puart)
{
    _uart_reset_rx_bit(puart);
}
/**
 *@brief 获取TXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 管脚电平
 */

uint8_t DRV_UART_GetTxdPinBit(UART_TypeDef *puart)
{
    return (_uart_get_tx_bit(puart));
}
/**
 *@brief 获取RXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 管脚电平
 */

uint8_t DRV_UART_GetRxdPinBit(UART_TypeDef *puart)
{
    return (_uart_get_rx_bit(puart));
}
/**
 *@brief 置位TXD或RXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] pin 指定管脚;
 *- UART_PIN_TXD;
 *- UART_PIN_RXD;
 *@return 无
 */
void DRV_UART_SetPinBit(UART_TypeDef *puart, uint8_t pin)
{
    _uart_set_pin_bit(puart, pin);
}
/**
 *@brief 复位TXD或RXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] pin 指定管脚;
 *- UART_PIN_TXD;
 *- UART_PIN_RXD;
 *@return 无
 */
void DRV_UART_ResetPinBit(UART_TypeDef *puart, uint8_t pin)
{
    _uart_reset_pin_bit(puart, pin);
}
/**
 *@brief 获取TXD或RXD管脚电平.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@param[in] pin 指定管脚;
 *- UART_PIN_TXD;
 *- UART_PIN_RXD;
 *@return 管脚电平状态;
 */
uint8_t DRV_UART_GetPinBit(UART_TypeDef *puart, uint8_t pin)
{
    // return(puart->PORT & (1<<pin));
    return (_uart_get_pin_bit(puart, pin));
}
/**
 *@brief RXD管脚电平翻转.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_RxdPinToggle(UART_TypeDef *puart)
{

    if (puart->PORT & UART_PIN_INDEX_RXD)
    {
        puart->PORT &= ~UART_PIN_INDEX_RXD;
    }
    else
    {
        puart->PORT |= UART_PIN_INDEX_RXD;
    }
}
/**
 *@brief TXD管脚电平翻转.
 *
 *@param[in] puart 指向UART_TypeDef结构体的指针;
 *@return 无
 */
void DRV_UART_TxdPinToggle(UART_TypeDef *puart)
{

    if (puart->PORT & UART_PIN_INDEX_TXD)
    {
        puart->PORT &= ~UART_PIN_INDEX_TXD;
    }
    else
    {
        puart->PORT |= UART_PIN_INDEX_TXD;
    }
}
/**
 *@brief 串口1查询方式发送单个字节数据用于DEBUG.
 *
 *@param[in] c 字符串指针;
 *@return 无
 */
static void DRV_UART_PutcDev(volatile const uint8_t c)
{
    if (c == '\n')
        DRV_UART_PutcDev('\r');

    DRV_UART_SendByte(UART1, c);
}
/**
 *@brief 查询方式发送数据用于DEBUG.
 *
 *@param[in] s 字符串指针;
 *@return 无
 */
void DRV_UART_PutsDev(volatile const uint8_t *s)
{
    while (*s)
        DRV_UART_PutcDev(*s++);
}

///**
// * @brief UART4 中断服务入口函数
// */
// void UART4_IRQHandler(void)
//{
//#ifdef UART_DEMO
//	#if UART_RECEIVE_TRANSMIT_IT_TEST
//		  UART_ISR(UART4, &g_Uart4RecvBufStruct);
//	#else
//			HAL_UART_IRQHandler(&UART1_Handle);
//	#endif
//#endif
//}

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
