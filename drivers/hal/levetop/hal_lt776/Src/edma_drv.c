/**
  **********************************************************************************
            Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                     All Rights Reserved
  **********************************************************************************
  * @file	  edma_drv.c
  * @author  Product application department
  * @version V1.0
  * @date	  2021.11.01
  * @brief   EDMA模块DRV层驱动.
  * @note 这个文件为应用层提供了操作EDMA的接口:
  *
  @verbatim
  ===================================================================================
                              ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
       1. nothing.
  @endverbatim
*/
#include "edma_drv.h"
//#include "common.h"
//#include "debug.h"
/*** 常量定义 *******************************************************************/
/* global variables */

/*** 全局变量定义 ***************************************************************/
/*** volatile **********/

/*** @ static **********/

/*** 函数定义 *******************************************************************/
/**
 * @brief 设置EDMA控制寄存器
 *
 * @param[in] edmacx EDMA寄存器结构体
 * @return @ref NONE
 */
void DRV_EDMAC_CtrlRegConfig(EDMAC_TypeDef         *edmacx,
                             FunctionalStateTypeDef compare_en,
                             FunctionalStateTypeDef compare_skip,
                             FunctionalStateTypeDef preload_en,
                             uint8_t                perip_num,
                             uint8_t                transfer_type)
{
    uint32_t reg = 0;
    uint32_t tmp = 0;

    /*CR*/
    /*比较使能*/
    if (compare_en == ENABLE)
    {
        reg |= EDMAC_COMPARE_EN;
    }

    if (compare_skip == ENABLE)
    {
        reg |= EDMAC_COMPARE_SKIP_EN;
    }

    if (preload_en == ENABLE)
    {
        reg |= EDMAC_PRELOAD_EN;
    }

    /*EDMAC 设备*/
    tmp = perip_num;
    tmp <<= 8;
    reg |= tmp;
    /*传输类型*/
    tmp = transfer_type;
    tmp <<= 5;
    reg |= tmp;

    /*The channel is availiable*/
    reg |= EDMAC_VALID_FLAG;

    /*写入寄存器CR*/
    edmacx->CR = reg;
}
/**
 * @brief 设置EDMA的读写buffer地址寄存器；
 *
 * @param[in] edmacx EDMA寄存器结构体
 * @param[in] write_buf_addr 写buffer地址
 * @param[in] read_buf_addr 读buffer地址
 *
 * @return @ref NONE
 */
void DRV_EDMAC_WriteAndReadBufferAdderAddConfig(EDMAC_TypeDef *edmacx, uint8_t write_buf_addr, uint8_t read_buf_addr)
{
    uint32_t reg = 0;
    // uint32_t tmp = 0;

    reg = write_buf_addr << 30;

    edmacx->WBAR &= 0x3fffff;
    edmacx->WBAR |= reg;

    reg = read_buf_addr << 30;

    edmacx->RBAR &= 0x3fffff;
    edmacx->RBAR |= reg;
}
/**
 * @brief 复位EDMA控制寄存器
 *
 * @param[in] edmacx EDMA寄存器结构体
 * @return @ref NONE
 */
void DRV_EDMAC_ResetReg(EDMAC_TypeDef *edmacx)
{
    /*复位寄存器*/
    edmacx->CR      = 0;
    edmacx->WBAR    = 0;
    edmacx->RBAR    = 0;
    edmacx->MINSUMR = 0;
    edmacx->MAJSUMR = 0;
}
/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
