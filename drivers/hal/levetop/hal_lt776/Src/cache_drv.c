/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			cache_drv.c
 * @Author		Jason Zhao
 * @Date			2021/12/2
 * @Time			10:45:33
 * @Version
 * @Brief
 **********************************************************/
#include "cache_drv.h"

/**
 * @brief ICACHE初始化
 * @param[in] rom: The attributes of rom memory regions；
 *                          取值cacheThrough, cacheBack or cacheOff
 *			 boot：The attributes of boot memory regions
 *                   	取值cacheThrough, cacheBack or cacheOff
 *			 spim1： The attributes of spim1 memory regions
 *				取值cacheThrough, cacheBack or cacheOff
 *			 eflash： The attributes of eflash memory regions
 *				取值cacheThrough, cacheBack or cacheOff
 * @note 上电后执行一次，请勿重复执行
 * @retval NONE
 */
static void DRV_ICACHE_Init(CACHE_ComTypeDef spim1,
                            CACHE_ComTypeDef spim2,
                            CACHE_ComTypeDef sdram,
                            CACHE_ComTypeDef eflash,
                            CACHE_ComTypeDef rom,
                            CACHE_ComTypeDef boot)
{
    /*    spim1 cache configuration    */
    if (CACHE_Off == spim1)
    {
        IPCCRGS_H &= SPIM1_CACHEOFF;
    }

    else if (CACHE_Through == spim1)
    {
        IPCCRGS_H &= SPIM1_CACHEOFF;
        IPCCRGS_H |= (WRITE_THROUGH << SPIM1_CACHE_SHIFT);
    }
    else if (CACHE_Back == spim1)
    {
        IPCCRGS_H &= SPIM1_CACHEOFF;
        IPCCRGS_H |= (WRITE_BACK << SPIM1_CACHE_SHIFT);
    }
    /*    spim2 cache configuration    */
    if (CACHE_Off == spim2)
    {
        IPCCRGS_H &= SPIM2_CACHEOFF;
    }
    else if (CACHE_Through == spim2)
    {
        IPCCRGS_H &= SPIM2_CACHEOFF;
        IPCCRGS_H |= ((uint32_t)WRITE_THROUGH << SPIM2_CACHE_SHIFT);
    }
    else if (CACHE_Back == spim2)
    {
        IPCCRGS_H &= SPIM2_CACHEOFF;
        IPCCRGS_H |= ((uint32_t)WRITE_BACK << SPIM2_CACHE_SHIFT);
    }
    /*sdram cache configuration*/

    if (CACHE_Off == sdram)
    {
        IPCCRGS_H &= SDRAM_CACHEOFF;
    }
    else if (CACHE_Through == sdram)
    {
        IPCCRGS_H &= SDRAM_CACHEOFF;
        IPCCRGS_H |= ((uint32_t)WRITE_THROUGH << SDRAM_CACHE_SHIFT);
    }
    else if (CACHE_Back == sdram)
    {
        IPCCRGS_H &= SDRAM_CACHEOFF;
        IPCCRGS_H |= ((uint32_t)WRITE_BACK << SDRAM_CACHE_SHIFT);
    }

    /*    rom cache configuration    */
    if (CACHE_Off == rom)
    {
        IPCCRGS &= ROM_CACHEOFF;
    }
    else if (CACHE_Through == rom)
    {
        IPCCRGS &= ROM_CACHEOFF;
        IPCCRGS |= (0x02 << ROM_CACHE_SHIFT);
    }
    else if (CACHE_Back == rom)
    {
        IPCCRGS &= ROM_CACHEOFF;
        IPCCRGS |= (0x03 << ROM_CACHE_SHIFT);
    }

    /*    boot cache configuration    */
    if (CACHE_Off == boot)
    {
        IPCCRGS_H &= BOOT_CACHEOFF;
    }
    else if (CACHE_Through == boot)
    {
        IPCCRGS_H &= BOOT_CACHEOFF;
        IPCCRGS_H |= ((uint32_t)WRITE_THROUGH << BOOT_CACHE_SHIFT);
    }
    else if (CACHE_Back == boot)
    {
        IPCCRGS_H &= BOOT_CACHEOFF;
        IPCCRGS_H |= ((uint32_t)WRITE_BACK << BOOT_CACHE_SHIFT);
    }

    /*    eflash cache configuration    */
    if (CACHE_Off == eflash)
    {
        IPCCRGS &= EFLASH_CACHEOFF;
    }
    else if (CACHE_Through == eflash)
    {
        // IR2HIGHADDR = 0x1fffff;
        IPCCRGS &= EFLASH_CACHEOFF;
        IPCCRGS |= ((uint32_t)EFLASH_WRITE_THROUGH << EFLASH_CACHE_SHIFT);
    }
    else if (CACHE_Back == eflash)
    {
        // IR2HIGHADDR = 0x1fffff;
        IPCCRGS &= EFLASH_CACHEOFF;
        IPCCRGS |= ((uint32_t)EFLASH_WRITE_BACK << EFLASH_CACHE_SHIFT);
    }

    IPCCCR |= (GO | INVW1 | INVW0);
    while (((IPCCCR) & (GO)) == GO)
        ;
    IPCCCR |= ENCACHE;
}

/**
 * @brief DCACHE初始化
 * @param[in] rom: The attributes of rom memory regions；
 *                          取值cacheThrough, cacheBack or cacheOff
 *                   boot：The attributes of boot memory regions
 *                          取值cacheThrough, cacheBack or cacheOff
 *                   spim1： The attributes of spim1 memory regions
 *                          取值cacheThrough, cacheBack or cacheOff
 *                   eflash： The attributes of eflash memory regions
 *                          取值cacheThrough, cacheBack or cacheOff
 * @note 上电后执行一次，请勿重复执行
 * @retval NONE
 */
static void DRV_DCACHE_Init(CACHE_ComTypeDef spim1,
                            CACHE_ComTypeDef spim2,
                            CACHE_ComTypeDef sdram,
                            CACHE_ComTypeDef eflash,
                            CACHE_ComTypeDef rom,
                            CACHE_ComTypeDef boot)
{
    /*    spim1 cache configuration    */
    if (CACHE_Off == spim1)
    {
        DPCCRGS_H &= SPIM1_CACHEOFF;
    }
    else if (CACHE_Through == spim1)
    {
        DPCCRGS_H &= SPIM1_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_THROUGH << SPIM1_CACHE_SHIFT);
    }
    else if (CACHE_Back == spim1)
    {
        DPCCRGS_H &= SPIM1_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_BACK << SPIM1_CACHE_SHIFT);
    }
    /*    spim2 cache configuration    */
    if (CACHE_Off == spim2)
    {
        DPCCRGS_H &= SPIM2_CACHEOFF;
    }
    else if (CACHE_Through == spim2)
    {
        DPCCRGS_H &= SPIM2_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_THROUGH << SPIM2_CACHE_SHIFT);
    }
    else if (CACHE_Back == spim2)
    {
        DPCCRGS_H &= SPIM2_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_BACK << SPIM2_CACHE_SHIFT);
    }
    /*    sdram cache configuration    */

    if (CACHE_Off == sdram)
    {
        DPCCRGS_H &= SDRAM_CACHEOFF;
    }
    else if (CACHE_Through == sdram)
    {
        DPCCRGS_H &= SDRAM_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_THROUGH << SDRAM_CACHE_SHIFT);
    }
    else if (CACHE_Back == sdram)
    {
        DPCCRGS_H &= SDRAM_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_BACK << SDRAM_CACHE_SHIFT);
    }

    /*    rom cache configuration    */
    if (CACHE_Off == rom)
    {
        DPCCRGS &= ROM_CACHEOFF;
    }
    else if (CACHE_Through == rom)
    {
        DPCCRGS &= ROM_CACHEOFF;
        DPCCRGS |= (0x02 << ROM_CACHE_SHIFT);
    }
    else if (CACHE_Back == rom)
    {
        DPCCRGS &= ROM_CACHEOFF;
        DPCCRGS |= (0x03 << ROM_CACHE_SHIFT);
    }

    /*    boot cache configuration    */
    if (CACHE_Off == boot)
    {
        DPCCRGS_H &= BOOT_CACHEOFF;
    }
    else if (CACHE_Through == boot)
    {
        DPCCRGS_H &= BOOT_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_THROUGH << BOOT_CACHE_SHIFT);
    }
    else if (CACHE_Back == boot)
    {
        DPCCRGS_H &= BOOT_CACHEOFF;
        DPCCRGS_H |= ((uint32_t)WRITE_BACK << BOOT_CACHE_SHIFT);
    }

    /*    eflash cache configuration   */
    if (CACHE_Off == eflash)
    {
        DPCCRGS &= EFLASH_CACHEOFF;
    }
    else if (CACHE_Through == eflash)
    {
        // DR2HIGHADDR = 0x1fffff;
        DPCCRGS &= EFLASH_CACHEOFF;
        DPCCRGS |= ((uint32_t)EFLASH_WRITE_THROUGH << EFLASH_CACHE_SHIFT);
    }
    else if (CACHE_Back == eflash)
    {
        // DR2HIGHADDR = 0x1fffff;
        DPCCRGS &= EFLASH_CACHEOFF;
        DPCCRGS |= ((uint32_t)EFLASH_WRITE_BACK << EFLASH_CACHE_SHIFT);
    }

    DPCCCR |= (GO | INVW1 | INVW0);
    while (((DPCCCR) & (GO)) == GO)
        ;
    DPCCCR |= ENCACHE;
}

static void DRV_ICACHE_Syn(void)
{
    DPCCCR |= (GO | PUSHW0 | PUSHW1);
    while (((DPCCCR) & (GO)) == GO)
        ;
}

static void DRV_DCACHE_Syn(void)
{
    IPCCCR |= (GO | INVW0 | INVW1);
    while (((IPCCCR) & (GO)) == GO)
        ;
}

void InitCache(void)
{
    CACHE_InitTypeDef cache_init;

    cache_init.SPIM1  = CACHE_Through;
    cache_init.SPIM2  = CACHE_Off;
    cache_init.SDRAMC = CACHE_Off; /* SDRAM的CACHE地址为0x18000000 */
    cache_init.EFLASH = CACHE_Through;
    cache_init.ROM    = CACHE_Off;
    cache_init.BOOT   = CACHE_Off;

    DRV_ICACHE_Init(cache_init.SPIM1,
                    cache_init.SPIM2,
                    cache_init.SDRAMC,
                    cache_init.EFLASH,
                    cache_init.ROM,
                    cache_init.BOOT);

    cache_init.SPIM1  = CACHE_Through;
    cache_init.SPIM2  = CACHE_Off;
    cache_init.SDRAMC = CACHE_Back; /* SDRAM的CACHE地址为0x18000000 */
    cache_init.EFLASH = CACHE_Through;
    cache_init.ROM    = CACHE_Off;
    cache_init.BOOT   = CACHE_Off;
    DRV_DCACHE_Init(cache_init.SPIM1,
                    cache_init.SPIM2,
                    cache_init.SDRAMC,
                    cache_init.EFLASH,
                    cache_init.ROM,
                    cache_init.BOOT);
}
void SynCache(void)
{
    DRV_ICACHE_Syn();
    DRV_DCACHE_Syn();
}
void DeInitCache(void)
{
    CACHE_InitTypeDef cache_init;

    cache_init.SPIM1  = CACHE_Off;
    cache_init.SPIM2  = CACHE_Off;
    cache_init.SDRAMC = CACHE_Off; /* SDRAM的CACHE地址为0x18000000 */
    cache_init.EFLASH = CACHE_Off;
    cache_init.ROM    = CACHE_Off;
    cache_init.BOOT   = CACHE_Off;

    DRV_ICACHE_Init(cache_init.SPIM1,
                    cache_init.SPIM2,
                    cache_init.SDRAMC,
                    cache_init.EFLASH,
                    cache_init.ROM,
                    cache_init.BOOT);
    DRV_DCACHE_Init(cache_init.SPIM1,
                    cache_init.SPIM2,
                    cache_init.SDRAMC,
                    cache_init.EFLASH,
                    cache_init.ROM,
                    cache_init.BOOT);
}
