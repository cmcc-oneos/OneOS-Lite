/**
  **********************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    ioctrl_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   IO control驱动.
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
 */

#include "ioctrl_drv.h"

/*** 全局变量定义 ***************************************************************/
/*** volatile **********/

/*** @ static **********/

/*** 常量定义 *******************************************************************/

/*** 函数定义 *******************************************************************/

/**
 * @brief IOCTRL 设置SPI 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @return NONE
 */
void DRV_IOCTRL_SetSPIDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_spi_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_spi_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_spi_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_spi_ds_12ma;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @return NONE
 */
void DRV_IOCTRL_SetSPISR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_spi_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_spi_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @return NONE
 */
void DRV_IOCTRL_SetSPIIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_spi_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_spi_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI1 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetSPI1PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_spi1_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_spi1_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI2 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetSPI2PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_spi2_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_spi2_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI3 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetSPI3PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_spi3_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_spi3_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI1 IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetSPI1IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_NULL:
        break;
    case IE_INPUT_DIS:
        _ioctrl_spi1_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_spi1_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI2 IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetSPI2IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_NULL:
        break;
    case IE_INPUT_DIS:
        _ioctrl_spi2_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_spi2_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SPI3 IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetSPI3IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_NULL:
        break;
    case IE_INPUT_DIS:
        _ioctrl_spi3_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_spi3_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置USI 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @return NONE
 */
void DRV_IOCTRL_SetUSIDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_usi_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_usi_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_usi_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_usi_ds_12ma;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置USI IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @return NONE
 */
void DRV_IOCTRL_SetUSISR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_usi_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_usi_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置USI IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @return NONE
 */
void DRV_IOCTRL_SetUSIIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_usi_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_usi_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置USI1 IO 上下拉
 *
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetUSI1PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_usi1_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_usi1_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL USI2
 *
 * @param[in] status
 *  - DISABLE
 *  - ENABLE
 * @return NONE
 */
void DRV_IOCTRL_SetUSI2DREN(FunctionalState status)
{

    switch (status)
    {
    case DISABLE:
        _ioctrl_usi2_dren_dis;
        break;
    case ENABLE:
        _ioctrl_usi2_dren_en;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL USI3
 *
 * @param[in] status
 *  - DISABLE
 *  - ENABLE
 * @return NONE
 */
void DRV_IOCTRL_SetUSI3DREN(FunctionalState status)
{

    switch (status)
    {
    case DISABLE:
        _ioctrl_usi3_dren_dis;
        break;
    case ENABLE:
        _ioctrl_usi3_dren_en;
        break;
    default:
        break;
    }
}
/**
  * @brief IOCTRL USI2 引脚复用失能
  *
  * @param[in] pins 引脚
  * @param[in]

  * @return NONE
  */
void DRV_IOCTRL_SetUSI2SWAPDis(uint8_t pins)
{
    _ioctrl_usi2_swap_dis(pins);
}
/**
  * @brief IOCTRL USI2 引脚复用使能
  *
  * @param[in] pins 引脚
  * @param[in]

  * @return NONE
  */
void DRV_IOCTRL_SetUSI2SWAPEn(uint8_t pins)
{
    _ioctrl_usi2_swap_pins_en(pins);
}
/**
 * @brief IOCTRL USI3 上拉使能
 *
 * @param[in] pins 引脚
 * @param[in] status
 *  - DISABLE
 *  - ENABLE
 * @return NONE
 */
void DRV_IOCTRL_SetUSI3PUEN(uint8_t pins, FunctionalState status)
{

    switch (status)
    {
    case DISABLE:
        _ioctrl_usi3_puen_dis(pins);
        break;
    case ENABLE:
        _ioctrl_usi3_puen_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL USI2 输入使能
 *
 * @param[in] pins 引脚
 * @param[in] status
 *  - DISABLE
 *  - ENABLE
 * @return NONE
 */
void DRV_IOCTRL_SetUSI2DIEN(uint8_t pins, FunctionalState status)
{
    switch (status)
    {
    case DISABLE:
        _ioctrl_usi2_dien_dis(pins);
        break;
    case ENABLE:
        _ioctrl_usi2_dien_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL USI3 输入使能
 *
 * @param[in] pins 引脚
 * @param[in] status
 *  - DISABLE
 *  - ENABLE
 * @return NONE
 */
void DRV_IOCTRL_SetUSI3DIEN(uint8_t pins, FunctionalState status)
{
    switch (status)
    {
    case DISABLE:
        _ioctrl_usi3_dien_dis(pins);
        break;
    case ENABLE:
        _ioctrl_usi3_dien_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置I2C 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @return NONE
 */
void DRV_IOCTRL_SetI2CDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_i2c_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_i2c_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_i2c_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_i2c_ds_12ma;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @return NONE
 */
void DRV_IOCTRL_SetI2CSR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_i2c_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_i2c_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @return NONE
 */
void DRV_IOCTRL_SetI2CIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_i2c_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_i2c_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C1 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetI2C1PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_i2c1_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_i2c1_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C2 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetI2C2PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_i2c1_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_i2c1_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C3 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetI2C3PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_i2c1_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_i2c1_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C1 IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetI2C1IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_NULL:
        break;
    case IE_INPUT_DIS:
        _ioctrl_i2c1_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_i2c1_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C2 IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetI2C2IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_NULL:
        break;
    case IE_INPUT_DIS:
        _ioctrl_i2c2_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_i2c2_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2C3 IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetI2C3IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_NULL:
        break;
    case IE_INPUT_DIS:
        _ioctrl_i2c2_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_i2c2_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置UART 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @return NONE
 */
void DRV_IOCTRL_SetUARTDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_uart_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_uart_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_uart_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_uart_ds_12ma;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置UART IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @return NONE
 */
void DRV_IOCTRL_SetUARTSR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_uart_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_uart_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置UART IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @return NONE
 */
void DRV_IOCTRL_SetUARTIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_uart_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_uart_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置UART1 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetUART1PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_uart1_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_uart1_pull_up(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置UART2 IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetUART2PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_NULL:
        break;
    case PS_PULL_DOWN:
        _ioctrl_uart2_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_uart2_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTL 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @return NONE
 */
void DRV_IOCTRL_SetGINTLDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_gintl_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_gintl_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_gintl_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_gintl_ds_12ma;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTL IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @return NONE
 */
void DRV_IOCTRL_SetGINTLSR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_gintl_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_gintl_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTL IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @return NONE
 */
void DRV_IOCTRL_SetGINTLIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_gintl_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_gintl_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTL IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetGINTLIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_gintl_gints_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_gintl_gints_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTL IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetGINTLPS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_DOWN:
        _ioctrl_gintl_gints_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_gintl_gints_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTH 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @return NONE
 */
void DRV_IOCTRL_SetGINTHDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_ginth_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_ginth_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_ginth_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_ginth_ds_12ma;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTH IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @return NONE
 */
void DRV_IOCTRL_SetGINTHSR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_ginth_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_ginth_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTH IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @return NONE
 */
void DRV_IOCTRL_SetGINTHIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_ginth_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_ginth_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTH IO 输入设置
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @return NONE
 */
void DRV_IOCTRL_SetGINTHIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_ginth_gints_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_ginth_gints_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置GINTH IO 上下拉
 *
 * @param[in] pins 引脚
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @return NONE
 */
void DRV_IOCTRL_SetGINTHPS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_DOWN:
        _ioctrl_ginth_gints_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_ginth_gints_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SwapEn(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swapcr_swap_en(pSwap);
}

/**
 * @brief IOCTRL 设置复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SwapDis(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swapcr_swap_dis(pSwap);
}
/**
 * @brief IOCTRL 设置SSI1 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetSSI1DS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_ssi1_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_ssi1_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_ssi1_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_ssi1_ds_12ma;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SSI1 IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetSSI1SR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_ssi1_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_ssi1_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI1 IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetSSI1IS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_ssi1_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_ssi1_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI1 IO 上下拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetSSI1PUE(IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_ssi1_pull_up_down_dis;
        break;
    case PUE_PULL_EN:
        _ioctrl_ssi1_pull_up_down_en;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI2 IO 设置输出属性
 *
 * @param[in] ode
 *  - ODE_OUTPUT_CMOS
 *  - ODE_OUTPUT_OPEN_DRAIN
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI1ODE(IOCTRL_ODETypeDef ode)
{
    switch (ode)
    {
    case ODE_OUTPUT_CMOS:
        _ioctrl_ssi1_output_cmos;
        break;
    case ODE_OUTPUT_OPEN_DRAIN:
        _ioctrl_ssi1_output_open_d;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI1 IO 输入设置
 *
 * @param[in] ps
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetSSI1IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_ssi1_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_ssi1_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SSI1 IO 上下拉
 *
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetSSI1PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_DOWN:
        _ioctrl_ssi1_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_ssi1_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI2 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI2DS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_ssi2_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_ssi2_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_ssi2_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_ssi2_ds_12ma;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SSI2 IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI2SR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_ssi2_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_ssi2_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI2 IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI2IS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_ssi2_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_ssi2_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI2 IO 上下拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI2PUE(IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_ssi2_pull_up_down_dis;
        break;
    case PUE_PULL_EN:
        _ioctrl_ssi2_pull_up_down_en;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI2 IO 设置输出属性
 *
 * @param[in] ode
 *  - ODE_OUTPUT_CMOS
 *  - ODE_OUTPUT_OPEN_DRAIN
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI2ODE(IOCTRL_ODETypeDef ode)
{
    switch (ode)
    {
    case ODE_OUTPUT_CMOS:
        _ioctrl_ssi2_output_cmos;
        break;
    case ODE_OUTPUT_OPEN_DRAIN:
        _ioctrl_ssi2_output_open_d;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SSI2 IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI2IE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_ssi2_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_ssi2_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SSI2 IO 上下拉
 *
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @note 即SPIM2
 * @return NONE
 */
void DRV_IOCTRL_SetSSI2PS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_DOWN:
        _ioctrl_ssi2_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_ssi2_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL SPIM设置复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SPIMSwapEn(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_spimswapcr_swap_en(pSwap);
}

/**
 * @brief IOCTRL SPIM设置复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SPIMSwapDisable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_spimswapcr_swap_dis(pSwap);
}

/**
 * @brief IOCTRL 设置CLCD 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_clcd_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_clcd_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_clcd_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_clcd_ds_12ma;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置CLCD IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDSR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_clcd_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_clcd_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置CLCD IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_clcd_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_clcd_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置CLCD IO 上下拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note 即SPIM1
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDPUE(IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_clcd_pull_up_down_dis;
        break;
    case PUE_PULL_EN:
        _ioctrl_clcd_pull_up_down_en;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置CLCD IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_clcd_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_clcd_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置CLCD IO 上下拉设置
 *
 * @param[in] ie
 *  - PS_PULL_DOWN
 *  - PS_PULL_UP
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDPS(uint8_t pins, IOCTRL_PSTypeDef ie)
{
    switch (ie)
    {
    case PS_PULL_DOWN:
        _ioctrl_clcd_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_clcd_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置DCLD DATA 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDDATAIE(uint32_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_clcd_data_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_clcd_data_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置CLCD DATA IO 上下拉设置
 *
 * @param[in] ps
 *  - PS_PULL_DOWN
 *  - PS_PULL_UP
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetCLCDDATAPS(uint32_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_DOWN:
        _ioctrl_clcd_data_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_clcd_data_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置DCMI 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetDCMIDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_dcmi_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_dcmi_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_dcmi_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_dcmi_ds_12ma;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置DCMI IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetDCMISR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_dcmi_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_dcmi_slew_slow;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置DCMI IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetDCMIIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_dcmi_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_dcmi_input_schmitt;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置DCMI IO 上下拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetDCMIPUE(IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_dcmi_pull_up_down_dis;
        break;
    case PUE_PULL_EN:
        _ioctrl_dcmi_pull_up_down_en;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置DCMI IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetDCMIIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_dcmi_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_dcmi_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置DCMI IO 上下拉设置
 *
 * @param[in] ie
 *  - PS_PULL_DOWN
 *  - PS_PULL_UP
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetDCMIPS(uint8_t pins, IOCTRL_PSTypeDef ie)
{
    switch (ie)
    {
    case PS_PULL_DOWN:
        _ioctrl_dcmi_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_dcmi_pull_up(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SDR IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_sdram_data_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_sdram_data_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDR IO 下拉使能
 *
 * @param[in] pde
 *  - PUDE_PULL_DOWN_DIS
 *  - PUDE_PULL_DOWN_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRPDE(uint8_t pins, IOCTRL_PDETypeDef pde)
{
    switch (pde)
    {
    case PUDE_PULL_DOWN_DIS:
        _ioctrl_sdrcr_pull_down_dis(pins);
        break;
    case PUDE_PULL_DOWN_EN:
        _ioctrl_sdrcr_pull_down_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDR IO 上拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRPUE(uint8_t pins, IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_sdrcr_pull_up_dis(pins);
        break;
    case PUE_PULL_EN:
        _ioctrl_sdrcr_pull_up_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SDR IO 设置输出属性
 *
 * @param[in] ode
 *  - ODE_OUTPUT_CMOS
 *  - ODE_OUTPUT_OPEN_DRAIN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRODE(IOCTRL_ODETypeDef ode)
{
    switch (ode)
    {
    case ODE_OUTPUT_CMOS:
        _ioctrl_sdrcr_output_cmos;
        break;
    case ODE_OUTPUT_OPEN_DRAIN:
        _ioctrl_sdrcr_output_open_d;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDR 速度
 *
 * @param[in] hx
 *  - HX_SPEED_UP_DIS
 *  - HX_SPEED_UP_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRHX(IOCTRL_HXTypeDef hx)
{
    switch (hx)
    {
    case HX_SPEED_UP_DIS:
        _ioctrl_sdrcr_speed_up_dis;
        break;
    case HX_SPEED_UP_EN:
        _ioctrl_sdrcr_speed_up_en;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SDR 驱动力
 *
 * @param[in] dc
 *  - DC_10PF
 *  - DC_20PF
 *  - DC_30PF
 *  - DC_50PF
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRDC(IOCTRL_DCTypeDef dc)
{
    switch (dc)
    {
    case DC_10PF:
        _ioctrl_sdrcr_dc_10pf;
        break;
    case DC_20PF:
        _ioctrl_sdrcr_dc_20pf;
        break;
    case DC_30PF:
        _ioctrl_sdrcr_dc_30pf;
        break;
    case DC_50PF:
        _ioctrl_sdrcr_dc_50pf;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SM IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSMIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_smcr_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_smcr_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SM IO 下拉使能
 *
 * @param[in] pde
 *  - PUDE_PULL_DOWN_DIS
 *  - PUDE_PULL_DOWN_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSMPDE(uint8_t pins, IOCTRL_PDETypeDef pde)
{
    switch (pde)
    {
    case PUDE_PULL_DOWN_DIS:
        _ioctrl_smcr_pull_down_dis(pins);
        break;
    case PUDE_PULL_DOWN_EN:
        _ioctrl_smcr_pull_down_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SM IO 上拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSMPUE(uint8_t pins, IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_smcr_pull_up_dis(pins);
        break;
    case PUE_PULL_EN:
        _ioctrl_smcr_pull_up_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SM IO 设置输出属性
 *
 * @param[in] ode
 *  - ODE_OUTPUT_CMOS
 *  - ODE_OUTPUT_OPEN_DRAIN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSMODE(IOCTRL_ODETypeDef ode)
{
    switch (ode)
    {
    case ODE_OUTPUT_CMOS:
        _ioctrl_smcr_output_cmos;
        break;
    case ODE_OUTPUT_OPEN_DRAIN:
        _ioctrl_smcr_output_open_d;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SM 速度
 *
 * @param[in] hx
 *  - HX_SPEED_UP_DIS
 *  - HX_SPEED_UP_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSMHX(IOCTRL_HXTypeDef hx)
{
    switch (hx)
    {
    case HX_SPEED_UP_DIS:
        _ioctrl_smcr_speed_up_dis;
        break;
    case HX_SPEED_UP_EN:
        _ioctrl_smcr_speed_up_en;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SM 驱动力
 *
 * @param[in] dc
 *  - DC_10PF
 *  - DC_20PF
 *  - DC_30PF
 *  - DC_50PF
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSMDC(IOCTRL_DCTypeDef dc)
{
    switch (dc)
    {
    case DC_10PF:
        _ioctrl_smcr_dc_10pf;
        break;
    case DC_20PF:
        _ioctrl_smcr_dc_20pf;
        break;
    case DC_30PF:
        _ioctrl_smcr_dc_30pf;
        break;
    case DC_50PF:
        _ioctrl_smcr_dc_50pf;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SDRAM DATA IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMDATAIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_sdram_data_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_sdram_data_input_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDRAM DATA IO 下拉使能
 *
 * @param[in] pde
 *  - PUDE_PULL_DOWN_DIS
 *  - PUDE_PULL_DOWN_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMDATAPDE(uint8_t pins, IOCTRL_PDETypeDef pde)
{
    switch (pde)
    {
    case PUDE_PULL_DOWN_DIS:
        _ioctrl_sdram_data_pull_down_dis(pins);
        break;
    case PUDE_PULL_DOWN_EN:
        _ioctrl_sdram_data_pull_down_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDRAM DATA IO 上拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMDATAPUE(uint8_t pins, IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_sdram_data_pull_up_dis(pins);
        break;
    case PUE_PULL_EN:
        _ioctrl_sdram_data_pull_up_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SDRAM DATA IO 设置输出属性
 *
 * @param[in] ode
 *  - ODE_OUTPUT_CMOS
 *  - ODE_OUTPUT_OPEN_DRAIN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMDATAODE(IOCTRL_ODETypeDef ode)
{
    switch (ode)
    {
    case ODE_OUTPUT_CMOS:
        _ioctrl_sdram_data_output_cmos;
        break;
    case ODE_OUTPUT_OPEN_DRAIN:
        _ioctrl_sdram_data_output_open_d;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDRAM DATA 速度
 *
 * @param[in] hx
 *  - HX_SPEED_UP_DIS
 *  - HX_SPEED_UP_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMDATAHX(IOCTRL_HXTypeDef hx)
{
    switch (hx)
    {
    case HX_SPEED_UP_DIS:
        _ioctrl_sdram_data_speed_up_dis;
        break;
    case HX_SPEED_UP_EN:
        _ioctrl_sdram_data_speed_up_en;
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SDRAM DATA 驱动力
 *
 * @param[in] dc
 *  - DC_10PF
 *  - DC_20PF
 *  - DC_30PF
 *  - DC_50PF
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMDATADC(IOCTRL_DCTypeDef dc)
{
    switch (dc)
    {
    case DC_10PF:
        _ioctrl_sdram_data_dc_10pf;
        break;
    case DC_20PF:
        _ioctrl_sdram_data_dc_20pf;
        break;
    case DC_30PF:
        _ioctrl_sdram_data_dc_30pf;
        break;
    case DC_50PF:
        _ioctrl_sdram_data_dc_50pf;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDRAM ADDR IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMADDRIE(uint32_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_sdram_addr_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_sdram_addr_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置SDRAM ADDR IO 下拉使能
 *
 * @param[in] pde
 *  - PUDE_PULL_DOWN_DIS
 *  - PUDE_PULL_DOWN_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMADDRPDE(uint32_t pins, IOCTRL_PDETypeDef pde)
{
    switch (pde)
    {
    case PUDE_PULL_DOWN_DIS:
        _ioctrl_sdram_addr_pull_down_dis(pins);
        break;
    case PUDE_PULL_DOWN_EN:
        _ioctrl_sdram_addr_pull_down_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SDRAM ADDR IO 上拉使能
 *
 * @param[in] pue
 *  - PUUE_PULL_DIS
 *  - PUUE_PULL_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetSDRAMADDRPUE(uint32_t pins, IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_sdram_addr_pull_up_dis(pins);
        break;
    case PUE_PULL_EN:
        _ioctrl_sdram_addr_pull_up_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SWAPH复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SwaphEn(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swaphcr_swap_en(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP2复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SwaphDisable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swaphcr_swap_dis(pSwap);
}

/**
 * @brief IOCTRL 设置I2S 驱动力
 *
 * @param[in] ds
 *  - DS_2MA
 *  - DS_4MA
 *  - DS_8MA
 *  - DS_12MA
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetI2SDS(IOCTRL_DSTypeDef ds)
{
    switch (ds)
    {
    case DS_2MA:
        _ioctrl_i2s_ds_2ma;
        break;
    case DS_4MA:
        _ioctrl_i2s_ds_4ma;
        break;
    case DS_8MA:
        _ioctrl_i2s_ds_8ma;
        break;
    case DS_12MA:
        _ioctrl_i2s_ds_12ma;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置I2S IO 反转速率
 *
 * @param[in] sr
 *  - SR_SLEW_FAST
 *  - SR_SLEW_SLOW
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetI2SSR(IOCTRL_SRTypeDef sr)
{
    switch (sr)
    {
    case SR_SLEW_FAST:
        _ioctrl_i2s_slew_fast;
        break;
    case SR_SLEW_SLOW:
        _ioctrl_i2s_slew_slow;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置I2S IO 输入属性
 *
 * @param[in] is
 *  - IS_INPUT_CMOS
 *  - IS_INPUT_SCHMITT
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetI2SIS(IOCTRL_ISTypeDef is)
{
    switch (is)
    {
    case IS_INPUT_CMOS:
        _ioctrl_i2s_input_cmos;
        break;
    case IS_INPUT_SCHMITT:
        _ioctrl_i2s_input_schmitt;
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置I2S IO 输入设置
 *
 * @param[in] ie
 *  - IE_INPUT_DIS
 *  - IE_INPUT_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetI2SIE(uint8_t pins, IOCTRL_IETypeDef ie)
{
    switch (ie)
    {
    case IE_INPUT_DIS:
        _ioctrl_i2s_input_dis(pins);
        break;
    case IE_INPUT_EN:
        _ioctrl_i2s_input_en(pins);
        break;
    default:
        break;
    }
}

/**
 * @brief IOCTRL 设置I2S IO 上下拉
 *
 * @param[in] ps
 *  - PS_PULL_NULL
 *  - PS_PULL_DOWN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetI2SPS(uint8_t pins, IOCTRL_PSTypeDef ps)
{
    switch (ps)
    {
    case PS_PULL_DOWN:
        _ioctrl_i2s_pull_down(pins);
        break;
    case PS_PULL_UP:
        _ioctrl_i2s_pull_up(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置I2S IO 上下拉使能
 *
 * @param[in] pue
 *  - PUE_PULL_DIS
 *  - PUE_PULL_EN
 * @note
 * @return NONE
 */
void DRV_IOCTRL_SetI2SPUE(uint8_t pins, IOCTRL_PUETypeDef pue)
{
    switch (pue)
    {
    case PUE_PULL_DIS:
        _ioctrl_i2s_pull_up_down_dis(pins);
        break;
    case PUE_PULL_EN:
        _ioctrl_i2s_pull_up_down_en(pins);
        break;
    default:
        break;
    }
}
/**
 * @brief IOCTRL 设置SWAPINT复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SwapintEn(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swapintcr_swap_en(pSwap);
}
/**
 * @brief IOCTRL 设置SWAPINT复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SwapintDisable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swapintcr_swap_dis(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP2复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap2En(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap2cr_swap_en(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP2复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap2Disable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap2cr_swap_dis(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP3复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap3En(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap3cr_swap_en(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP3复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap3Disable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap3cr_swap_dis(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP4复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap4En(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap4cr_swap_en(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP4复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap4Disable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap4cr_swap_dis(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP5复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap5En(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap5cr_swap_en(pSwap);
}
/**
 * @brief IOCTRL 设置SWAP5复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_Swap5Disable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_swap5cr_swap_dis(pSwap);
}
/**
 * @brief IOCTRL 设置SDRAM复用脚切换使能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SdramswapEn(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_sdramswap_swap_en(pSwap);
}
/**
 * @brief IOCTRL 设置SDRAM复用脚切换失能
 *
 * @param[in] pSwap bit位
 * @return NONE
 */
void DRV_IOCTRL_SdramswapDisable(IOCTRL_SwapTypeDef pSwap)
{
    _ioctrl_sdramswap_swap_dis(pSwap);
}

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
