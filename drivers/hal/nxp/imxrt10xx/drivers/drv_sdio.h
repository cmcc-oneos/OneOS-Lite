/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sdio_imx.h
 *
 * @brief       This file provides  definition and operation functions declaration for sdio.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef DRV_SDIO_H__
#define DRV_SDIO_H__

#include <os_stddef.h>
#include <os_task.h>
#include <device.h>
#include <fsl_sdio.h>
#include <drv_usdhc.h>

#ifdef __cplusplus
extern "C" {
#endif

#define IMXRT_SDIO_MAX_NUM IMXRT_USDHC_MAX_NUM

struct imxrt_sdio_device
{
    os_device_t            dev;
    struct os_imxrt_usdhc *imxrt_usdhc;
};

sdio_card_t *imxrt_sdio_get_card(char *name);

#ifdef __cplusplus
}
#endif

#endif
