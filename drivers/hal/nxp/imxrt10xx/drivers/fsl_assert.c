/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        fsl_assert.c
 *
 * @brief       This file implements assert function in sdk for imxrt.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-01   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#define LOG_TAG "fsl.assert"
#include <drv_log.h>
#include <os_assert.h>

#ifndef NDEBUG
#if (defined(__CC_ARM)) || (defined(__ARMCC_VERSION)) || (defined(__ICCARM__))
void __aeabi_assert(const char *failedExpr, const char *file, int line)
{
#if SDK_DEBUGCONSOLE == DEBUGCONSOLE_DISABLE
    os_kprintf("ASSERT ERROR \" %s \": file \"%s\" Line \"%d\" \r\n", failedExpr, file, line);
#else
    (void)os_kprintf("ASSERT ERROR \" %s \": file \"%s\" Line \"%d\" \r\n", failedExpr, file, line);
#endif
    os_safety_assert_process();
}
#elif (defined(__GNUC__))
#if defined(__REDLIB__)
void __assertion_failed(char *failedExpr)
{
    (void)os_kprintf("ASSERT ERROR \" %s \r\n", failedExpr);
    os_safety_assert_process();
}
#else
void __assert_func(const char *file, int line, const char *func, const char *failedExpr)
{
    (void)os_kprintf("ASSERT ERROR \" %s \": file \"%s\" Line \"%d\" function name \"%s\" \r\n",
                     failedExpr,
                     file,
                     line,
                     func);
    os_safety_assert_process();
}
#endif /* defined(__REDLIB__) */
#endif /* (defined(__CC_ARM) || (defined(__ICCARM__)) || (defined(__ARMCC_VERSION)) */
#endif /* NDEBUG */
