/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_tempmon.h
 *
 * @brief       This file implements tempmon driver for imxrt.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-01   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef DRV_TEMPMON_H__
#define DRV_TEMPMON_H__

#include "peripherals.h"

#define OS_IMXRT_TEMPMON_SET_LOW_ALARM   0
#define OS_IMXRT_TEMPMON_SET_HIGH_ALARM  1
#define OS_IMXRT_TEMPMON_SET_PANIC_ALARM 2

struct nxp_tempmon_info
{
    TEMPMON_Type           *base;
    const tempmon_config_t *config;
};

#endif /* DRV_TEMPMON_H__ */
