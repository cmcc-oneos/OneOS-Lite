/*!
 * @file       system_apm32f4xx.h
 *
 * @brief      CMSIS Cortex-M4 Device Peripheral Access Layer System Source File
 *
 * @version    V1.0.0
 *
 * @date       2021-09-08
 *
 */

#ifndef __SYSTEM_APM32F4XX_H
#define __SYSTEM_APM32F4XX_H

#ifdef __cplusplus
  extern "C" {
#endif

/** System Clock Frequency (Core Clock) */
extern unsigned int SystemCoreClock;

extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);

#ifdef __cplusplus
}
#endif

#endif /*__SYSTEM_APM32F4XX_H */
