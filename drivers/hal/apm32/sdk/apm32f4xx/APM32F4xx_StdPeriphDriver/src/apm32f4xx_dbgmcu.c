/*!
 * @file       apm32f4xx_dbgmcu.c
 *
 * @brief      This file provides all the DEBUG firmware functions
 *
 * @version    V1.0.0
 *
 * @date       2021-09-08
 *
 */

#include "apm32f4xx_dbgmcu.h"

/** @addtogroup Peripherals_Library Standard Peripheral Library
  @{
*/

/** @addtogroup DBGMCU_Driver DBGMCU Driver
  @{
*/

/** @addtogroup DBGMCU_Fuctions Fuctions
  @{
*/

/*!
 * @brief     Returns the device identifier.
 *
 * @param     None
 *
 * @retval    Device identifier
 */
uint32_t DBGMCU_ReadDEVID(void)
{
    return(DBGMCU->IDCODE_B.EQR);
}

/*!
 * @brief     Returns the device revision identifier.
 *
 * @param     None
 *
 * @retval    Device revision identifier
 */
uint32_t DBGMCU_ReadREVID(void)
{
    return(DBGMCU->IDCODE_B.WVR);
}

/*!
 * @brief     Enable the specified peripheral and low power mode behavior
 *            when the MCU under Debug mode
 *
 * @param     periph: Specifies the peripheral and low power mode
 *                    This parameter can be any combination of the following values:
 *                    @arg DBGMCU_SLEEP     : Keep debugger connection during SLEEP mode
 *                    @arg DBGMCU_STOP      : Keep debugger connection during STOP mode
 *                    @arg DBGMCU_STANDBY   : Keep debugger connection during STANDBY mode
 *
 * @retval    None
 */
void DBGMCU_Enable(uint32_t periph)
{
    DBGMCU->CFG |= periph;
}

/*!
 * @brief     DBGMCU_Disable the specified peripheral and low power mode behavior
 *            when the MCU under Debug mode
 *
 * @param     periph: Specifies the peripheral and low power mode
 *                    This parameter can be any combination of the following values:
 *                    @arg DBGMCU_SLEEP     : Keep debugger connection during SLEEP mode
 *                    @arg DBGMCU_STOP      : Keep debugger connection during STOP mode
 *                    @arg DBGMCU_STANDBY   : Keep debugger connection during STANDBY mode
 *
 * @retval    None
 */
void DBGMCU_Disable(uint32_t periph)
{
    DBGMCU->CFG &= ~periph;
}

/*!
 * @brief     Enable APB1 peripheral behavior when the MCU is in Debug mode.
 *
 * @param     periph: specifies the APB1 peripheral.
 *                    This parameter can be any combination of the following values:
 *                    @arg DBGMCU_TMR2_STOP          : TMR2 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR3_STOP          : TMR3 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR4_STOP          : TMR4 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR5_STOP          : TMR5 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR6_STOP          : TMR6 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR7_STOP          : TMR7 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR12_STOP         : TMR12 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR13_STOP         : TMR13 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR14_STOP         : TMR14 counter stopped when Core is halted
 *                    @arg DBGMCU_RTC_STOP           : RTC Calendar and Wakeup counter stopped when Core is halted.
 *                    @arg DBGMCU_WWDT_STOP          : Debug WWDG stopped when Core is halted
 *                    @arg DBGMCU_IWDT_STOP          : Debug IWDG stopped when Core is halted
 *                    @arg DBGMCU_I2C1_SMBUS_TIMEOUT : I2C1 SMBUS timeout mode stopped when Core is halted
 *                    @arg DBGMCU_I2C2_SMBUS_TIMEOUT : I2C2 SMBUS timeout mode stopped when Core is halted
 *                    @arg DBGMCU_I2C3_SMBUS_TIMEOUT : I2C3 SMBUS timeout mode stopped when Core is halted
 *                    @arg DBGMCU_CAN1_STOP          : Debug CAN1 stopped when Core is halted
 *                    @arg DBGMCU_CAN1_STOP          : Debug CAN2 stopped when Core is halted
 *
 * @retval    None
 */
void DBGMCU_EnableAPB1Periph(uint32_t periph)
{
    DBGMCU->APB1F |= periph;
}

/*!
 * @brief     Disable APB1 peripheral behavior when the MCU is in Debug mode.
 *
 * @param     periph: specifies the APB1 peripheral.
 *                    This parameter can be any combination of the following values:
 *                    @arg DBGMCU_TMR2_STOP          : TMR2 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR3_STOP          : TMR3 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR4_STOP          : TMR4 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR5_STOP          : TMR5 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR6_STOP          : TMR6 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR7_STOP          : TMR7 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR12_STOP         : TMR12 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR13_STOP         : TMR13 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR14_STOP         : TMR14 counter stopped when Core is halted
 *                    @arg DBGMCU_RTC_STOP           : RTC Calendar and Wakeup counter stopped when Core is halted.
 *                    @arg DBGMCU_WWDT_STOP          : Debug WWDG stopped when Core is halted
 *                    @arg DBGMCU_IWDT_STOP          : Debug IWDG stopped when Core is halted
 *                    @arg DBGMCU_I2C1_SMBUS_TIMEOUT : I2C1 SMBUS timeout mode stopped when Core is halted
 *                    @arg DBGMCU_I2C2_SMBUS_TIMEOUT : I2C2 SMBUS timeout mode stopped when Core is halted
 *                    @arg DBGMCU_I2C3_SMBUS_TIMEOUT : I2C3 SMBUS timeout mode stopped when Core is halted
 *                    @arg DBGMCU_CAN1_STOP          : Debug CAN1 stopped when Core is halted
 *                    @arg DBGMCU_CAN1_STOP          : Debug CAN2 stopped when Core is halted
 *
 * @retval    None
 */
void DBGMCU_DisableAPB1Periph(uint32_t periph)
{
    DBGMCU->APB1F &= ~periph;
}

/*!
 * @brief     Enable APB2 peripheral behavior when the MCU is in Debug mode.
 *
 * @param     periph: specifies the APB2 peripheral.
 *                    This parameter can be any combination of the following values:
 *                    @arg DBGMCU_TMR1_STOP  : TMR1 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR8_STOP  : TMR8 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR9_STOP  : TMR9 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR10_STOP : TMR10 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR11_STOP : TMR11 counter stopped when Core is halted
 *
 * @retval    None
 */
void DBGMCU_EnableAPB2Periph(uint32_t periph)
{
    DBGMCU->APB2F |= periph;
}

/*!
 * @brief     Disable APB2 peripheral behavior when the MCU is in Debug mode.
 *
 * @param     periph: specifies the APB2 peripheral.
 *                    This parameter can be any combination of the following values:
 *                    @arg DBGMCU_TMR1_STOP  : TMR1 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR8_STOP  : TMR8 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR9_STOP  : TMR9 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR10_STOP : TMR10 counter stopped when Core is halted
 *                    @arg DBGMCU_TMR11_STOP : TMR11 counter stopped when Core is halted
 *
 * @retval    None
 */
void DBGMCU_DisableAPB2Periph(uint32_t periph)
{
    DBGMCU->APB2F &= ~periph;
}

/**@} end of group DBGMCU_Fuctions*/
/**@} end of group DBGMCU_Driver*/
/**@} end of group Peripherals_Library*/
