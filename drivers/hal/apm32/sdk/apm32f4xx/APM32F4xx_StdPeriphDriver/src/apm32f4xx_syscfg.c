/*!
 * @file       apm32f4xx_syscfg.c
 *
 * @brief      This file provides all the SYSCFG firmware functions
 *
 * @version    V1.0.0
 *
 * @date       2021-09-08
 *
 */

#include "apm32f4xx_syscfg.h"
#include "apm32f4xx_rcm.h"

/** @addtogroup Peripherals_Library Standard Peripheral Library
  @{
*/

/** @addtogroup SYSCFG_Driver SYSCFG Driver
  @{
*/

/** @addtogroup SYSCFG_Fuctions Fuctions
  @{
*/

/*!
 * @brief     Reset the remap and EINT configuration registers to their default values.
 *
 * @param     None
 *
 * @retval    None
 */
void SYSCFG_Reset(void)
{
    RCM_EnableAPB2PeriphReset(RCM_APB2_PERIPH_SYSCFG);
    RCM_DisableAPB2PeriphReset(RCM_APB2_PERIPH_SYSCFG);
}

/*!
 * @brief     Changes the mapping of the specified pin.
 *
 * @param     memory: selects the memory remapping.
 *                   The parameter can be one of the following values:
 *                   @arg SYSCFG_REMAP_Flash:       Memory mapping to Flash
 *                   @arg SYSCFG_REMAP_SystemFlash: Memory mapping to SystemFlash
 *                   @arg SYSCFG_REMAP_SMC_BANK1:   Memory mapping to bank1 of SMC (NOR-flash/PSRAM)
 *                   @arg SYSCFG_REMAP_SRAM:        Memory mapping to SRAM
 *
 * @retval    None
 */
void SYSCFG_ConfigMemoryRemap(SYSCFG_REMAP_T memory)
{
    SYSCFG->MMSEL_B.MMSEL = memory;
}

/*!
 * @brief     Config the GPIO pin used as EINT Line.
 *
 * @param     port: select the port
 *                  The parameter can be one of the following values
 *                  @arg SYSCFG_PORT_GPIOA: Port Source GPIOA
 *                  @arg SYSCFG_PORT_GPIOB: Port Source GPIOB
 *                  @arg SYSCFG_PORT_GPIOC: Port Source GPIOC
 *                  @arg SYSCFG_PORT_GPIOD: Port Source GPIOD
 *                  @arg SYSCFG_PORT_GPIOE: Port Source GPIOE
 *                  @arg SYSCFG_PORT_GPIOF: Port Source GPIOF
 *                  @arg SYSCFG_PORT_GPIOG: Port Source GPIOG
 *                  @arg SYSCFG_PORT_GPIOH: Port Source GPIOH
 *                  @arg SYSCFG_PORT_GPIOI: Port Source GPIOI
 *
 * @param     pin:  select the pin
 *                  The parameter can be one of the following values
 *                  @arg SYSCFG_PIN_0 : Pin Source 0
 *                  @arg SYSCFG_PIN_1 : Pin Source 1
 *                  @arg SYSCFG_PIN_2 : Pin Source 2
 *                  @arg SYSCFG_PIN_3 : Pin Source 3
 *                  @arg SYSCFG_PIN_4 : Pin Source 4
 *                  @arg SYSCFG_PIN_5 : Pin Source 5
 *                  @arg SYSCFG_PIN_6 : Pin Source 6
 *                  @arg SYSCFG_PIN_7 : Pin Source 7
 *                  @arg SYSCFG_PIN_8 : Pin Source 8
 *                  @arg SYSCFG_PIN_9 : Pin Source 9
 *                  @arg SYSCFG_PIN_10: Pin Source 10
 *                  @arg SYSCFG_PIN_11: Pin Source 11
 *                  @arg SYSCFG_PIN_12: Pin Source 12
 *                  @arg SYSCFG_PIN_13: Pin Source 13
 *                  @arg SYSCFG_PIN_14: Pin Source 14
 *                  @arg SYSCFG_PIN_15: Pin Source 15
 *
 * @retval    None
 *
 * @note      pin from SYSCFG_PIN_12 to SYSCFG_PIN_15 is not fit for
 *            SYSCFG_PORT_GPIOI of the port.
 */
void SYSCFG_ConfigEINTLine(SYSCFG_PORT_T port, SYSCFG_PIN_T pin)
{
    __IO uint32_t status;
    __IO uint32_t tmp;
    uint32_t shift;

    shift = (0x04 * (pin & 0x03));
    status = ((uint32_t)(port & 0x0F)) << shift;
    tmp = (uint32_t) ~(0x0f << shift);

    if ((pin >> 2) == 0)
    {
        tmp &= SYSCFG->EINTCFG1;
        tmp |= status;
        SYSCFG->EINTCFG1 = tmp;
    }
    
    else if  ((pin >> 2) == 1)
    {
        tmp &= SYSCFG->EINTCFG2;
        tmp |= status;
        SYSCFG->EINTCFG2 = tmp;
    }
    else if  ((pin >> 2) == 2)
    {
        tmp &= SYSCFG->EINTCFG3;
        tmp |= status;
        SYSCFG->EINTCFG3 = tmp;
    }
    else if  ((pin >> 2) == 3)
    {
        tmp &= SYSCFG->EINTCFG4;
        tmp |= status;
        SYSCFG->EINTCFG4 = tmp;
    }
}

/*!
 * @brief     Selects the ETHERNET media interface
 *
 * @param     media: select the media
 *                   The parameter can be combination of folling values
 *                   @arg SYSCFG_INTERFACE_MII : MII mode selected
 *                   @arg SYSCFG_INTERFACE_RMII: RMII mode selected
 *
 * @retval    None
 */
void SYSCFG_ConfigMediaInterface(SYSCFG_INTERFACE_T media)
{
    SYSCFG->PMC_B.ENETSEL = media;
}

/*!
 * @brief     Enables the I/O Compensation Cell.
 *
 * @param     None
 *
 * @retval    None
 *
 * @note      The I/O compensation cell can be used only when the device supply
 *            voltage ranges from 2.4 to 3.6 V.
 */
void SYSCFG_EnableCompensationCell(void)
{
    SYSCFG->CCCTRL_B.CCPD = BIT_SET;
}

/*!
 * @brief     Disables the I/O Compensation Cell.
 *
 * @param     None
 *
 * @retval    None
 *
 * @note      The I/O compensation cell can be used only when the device supply
 *            voltage ranges from 2.4 to 3.6 V.
 */
void SYSCFG_DisableCompensationCell(void)
{
    SYSCFG->CCCTRL_B.CCPD = BIT_RESET;
}

/*!
 * @brief     Read the I/O Compensation Cell ready flag.
 *
 * @param     None
 *
 * @retval    SET or RESET
 */
uint8_t SYSCFG_ReadCompensationCellStatus(void)
{
    return (SYSCFG->CCCTRL & BIT8) ? SET : RESET;
}

/**@} end of group SYSCFG_Fuctions*/
/**@} end of group SYSCFG_Driver*/
/**@} end of group Peripherals_Library*/
