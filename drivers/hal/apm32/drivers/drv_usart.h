/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief        This file provides functions declaration for usart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_USART_H__
#define __DRV_USART_H__

#include <os_types.h>

#include "apm32_hal.h"

enum usart_irq_type
{
    USART_IRQ_TX = 0,
    USART_IRQ_RX,
    USART_IRQ_RX_IDLE,
};

typedef struct apm32_usart_info
{
    GPIO_T       *tx_pin_port;
    GPIO_Config_T tx_pin_info;
#ifdef SERIES_APM32F4XX
    GPIO_PIN_SOURCE_T tx_gpioPinSource;
    GPIO_AF_T         tx_gpioAf;
#endif

    GPIO_T       *rx_pin_port;
    GPIO_Config_T rx_pin_info;
#ifdef SERIES_APM32F4XX
    GPIO_PIN_SOURCE_T rx_gpioPinSource;
    GPIO_AF_T         rx_gpioAf;
#endif

    void (*gpio_PeriphClock)(uint32_t Periph);
    uint32_t gpio_Periph;
    void (*usart_PeriphClock)(uint32_t Periph);
    uint32_t usart_Periph;

    USART_T  *husart;
    IRQn_Type irq_type;
    uint8_t   irq_PrePri;
    uint8_t   irq_SubPri;

    USART_Config_T usart_def_cfg;
} apm32_usart_info_t;

#endif /* __DRV_USART_H__ */
