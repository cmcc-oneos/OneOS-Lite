/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        can.c
 *
 * @brief       This file provides functions for registering can device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __NET_DEV_H__
#define __NET_DEV_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <os_sem.h>
#include <os_errno.h>
#include <os_mb.h>
#include <os_task.h>
#include <device.h>

#define OS_NET_XFER_TX_TASK 1 << 0 /*if driver tx is in poll mode, need tx task*/
#define OS_NET_XFER_RX_TASK 1 << 1 /*if driver rx is report in irq, need rx task*/

struct os_net_device;

typedef enum
{
    net_dev_intf_ether = 0,
    net_dev_intf_bt    = 1,
    net_dev_intf_unknown
} os_net_intf_type_t;

typedef enum
{
    net_dev_mode_ap  = 0,
    net_dev_mode_sta = 1,
    net_dev_mode_unknown
} os_net_mode_t;

#ifndef OS_NET_DRV_TO_PROTOCOL
struct os_net_xfer_data
{
    os_uint8_t *buff;
    os_uint32_t size;
};
typedef struct os_net_xfer_data os_net_xfer_data_t;

#define os_net_data_t os_net_xfer_data_t
#else
#define os_net_data_t void
#endif

struct os_net_device_ops
{
    os_err_t (*init)(struct os_net_device *net_dev);
    os_err_t (*deinit)(struct os_net_device *net_dev);

    os_err_t (*send)(struct os_net_device *net_dev, os_net_data_t *data);
    os_net_data_t *(*recv)(struct os_net_device *net_dev);

    os_err_t (*get_mac)(struct os_net_device *net_dev, os_uint8_t *addr);
    os_err_t (*set_filter)(struct os_net_device *net_dev, os_uint8_t *buff, os_bool_t enable);
};

struct os_net_protocol_ops
{
    os_err_t (*init)(struct os_net_device *net_dev);
    os_err_t (*deinit)(struct os_net_device *net_dev);

    os_err_t (*recv)(struct os_net_device *net_dev, os_net_data_t *data);

    os_err_t (*linkchange)(struct os_net_device *net_dev, os_bool_t status);
};

struct os_net_device_info
{
    os_uint32_t MTU;
    os_uint32_t MRU;

    os_net_mode_t      mode;
    os_bool_t          linkstatus;
    os_net_intf_type_t intf_type;
    os_uint8_t         xfer_flag;

    char      *ip_addr;
    char      *gw_addr;
    char      *ip_mask;
    os_uint8_t dhcp_start_id;
    os_uint8_t dhcp_end_id;
};

struct os_net_device
{
    os_device_t dev;

    struct os_net_device_info info;

    const struct os_net_device_ops   *ops;
    const struct os_net_protocol_ops *protocol_ops;

    os_task_t *tx_task;
    os_mb_t   *tx_mb;

    os_task_t *rx_task;
    os_sem_t  *rx_sem;

    void *userdata;
};

#ifndef OS_NET_DRV_TO_PROTOCOL
os_net_xfer_data_t *os_net_get_buff(os_uint32_t size);
os_net_xfer_data_t *os_net_free_buff(os_net_xfer_data_t *buff);
#endif

os_err_t os_net_get_macaddr(struct os_net_device *net_dev, os_uint8_t *addr);
os_err_t os_net_tx_data(struct os_net_device *net_dev, os_net_data_t *data);
os_err_t os_net_set_filter(struct os_net_device *net_dev, os_uint8_t *addr, os_bool_t enable);

os_err_t os_net_linkchange(struct os_net_device *net_dev, os_bool_t status);
os_err_t os_net_rx_report_data(struct os_net_device *net_dev, os_net_data_t *data);
os_err_t os_net_rx_report_irq(struct os_net_device *net_dev);
os_err_t os_net_get_mac_string2addr(char *string, os_uint8_t *addr);

os_err_t os_net_device_unregister(struct os_net_device *net_dev);
os_err_t os_net_device_register(struct os_net_device *net_dev, const char *name);

#ifdef __cplusplus
}
#endif

#endif
