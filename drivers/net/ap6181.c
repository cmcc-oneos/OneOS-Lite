/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_ap6181.c
 *
 * @brief       This file implements ap6181 driver.
 *
 * @revision
 * Date         Author          Notes
 * 2021-08-12   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <os_types.h>
#include <os_errno.h>
#include <os_assert.h>
#include <dlog.h>

#include "oneos_config.h"
#include "drv_cfg.h"

#include "wlan_dev.h"
#include "wwd_dev.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "ap6181"
#include <drv_log.h>

#ifndef AP6181_INIT_SECURITY
#define AP6181_INIT_SECURITY OS_WLAN_SECURITY_WPA2_MIXED_PSK
#endif

#ifndef AP6181_INIT_COUNTRY
#define AP6181_INIT_COUNTRY OS_WLAN_COUNTRY_CHINA
#endif

struct os_ap6181_info
{
    struct os_wwd_device wwd_dev;

    struct os_wlan_device wlan_dev;

    os_task_t *task;
};

static struct os_ap6181_info *ap6181 = OS_NULL;

static int ap6181_early_init(void)
{
    if (BSP_AP6181_REG_ON_PIN >= 0)
    {
        os_pin_mode(BSP_AP6181_REG_ON_PIN, PIN_MODE_OUTPUT);
        os_pin_write(BSP_AP6181_REG_ON_PIN, PIN_LOW);
        os_hw_us_delay(1000);
        os_pin_write(BSP_AP6181_REG_ON_PIN, PIN_HIGH);
        os_hw_us_delay(1000);
    }
    return 0;
}
OS_PREV_INIT(ap6181_early_init, OS_INIT_SUBLEVEL_HIGH);

#if defined(MANUFACTOR_STM32)
static os_int32_t os_wwd_sdio_probe(struct os_mmcsd_card *card)
{
    ap6181->wwd_dev.card = (void *)card;
    return 0;
}

static os_int32_t os_wwd_sdio_remove(struct os_mmcsd_card *card)
{
    return 0;
}

static struct os_sdio_device_id wwd_sdio_id = {
    .func_code    = 0,
    .manufacturer = 0x02D0,
    .product      = 0xA962,
};

struct os_sdio_driver wwd_sdio_driver = {
    .name   = "wwd",
    .probe  = os_wwd_sdio_probe,
    .remove = os_wwd_sdio_remove,
    .id     = &wwd_sdio_id,
};

os_err_t os_wwd_wait_bus(void)
{
    while (ap6181->wwd_dev.card == OS_NULL)
    {
        os_task_msleep(50);
    }

    return OS_EOK;
}

os_err_t os_wwd_bus_init()
{
    ap6181->wwd_dev.card   = OS_NULL;
    ap6181->wwd_dev.driver = (void *)&wwd_sdio_driver;

    sdio_register_driver((struct os_sdio_driver *)ap6181->wwd_dev.driver);

    return OS_EOK;
}

#elif defined(SERIES_NXP_IMXRT10XX)
#include "drv_sdio.h"

os_err_t os_wwd_wait_bus(void)
{
    return OS_EOK;
}

os_err_t os_wwd_bus_init(void)
{
    ap6181->wwd_dev.card = (void *)imxrt_sdio_get_card(BSP_AP6181_BUS_NAME);
    if (ap6181->wwd_dev.card == OS_NULL)
    {
        return OS_ERROR;
    }

    ap6181->wwd_dev.driver = OS_NULL;

    return OS_EOK;
}
#endif

#ifdef BSP_AP6181_TASK_STACK_SIZE
static void ap6181_task_entry(void *parameter)
{
    while (1)
    {
        os_wlan_irq_handler(&ap6181->wlan_dev);
        os_task_msleep(BSP_AP6181_TASK_PERIOD);
    }
}
#endif

static int ap6181_init(void)
{
    ap6181 = (struct os_ap6181_info *)os_calloc(1, sizeof(struct os_ap6181_info));
    if (ap6181 == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "ap6181 call failed!");
        goto __exit;
    }

    if (os_wwd_bus_init() != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_wwd_bus_init failed!");
        goto __exit;
    }

    os_wwd_wait_bus();

#ifdef BSP_AP6181_STA
    ap6181->wlan_dev.net_dev.info.mode = net_dev_mode_sta;
#else
    ap6181->wlan_dev.net_dev.info.mode = net_dev_mode_ap;
#endif
    ap6181->wlan_dev.net_dev.info.intf_type = net_dev_intf_ether;
    ap6181->wlan_dev.net_dev.info.xfer_flag = OS_NET_XFER_TX_TASK;

    if (os_wwd_wlan_register(&ap6181->wlan_dev, &ap6181->wwd_dev) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_wwd_wlan_register failed!");
        goto __exit;
    }

    if (BSP_AP6181_IRQ_PIN >= 0)
    {
        os_pin_mode(BSP_AP6181_IRQ_PIN, PIN_MODE_INPUT_PULLUP);
        os_pin_attach_irq(BSP_AP6181_IRQ_PIN, PIN_IRQ_MODE_RISING, os_wlan_irq_handler, &ap6181->wlan_dev);
        os_pin_irq_enable(BSP_AP6181_IRQ_PIN, PIN_IRQ_ENABLE);
    }

#ifdef BSP_AP6181_TASK_STACK_SIZE
    ap6181->task =
        os_task_create("ap6181_task", ap6181_task_entry, OS_NULL, BSP_AP6181_TASK_STACK_SIZE, BSP_AP6181_TASK_PRIORITY);
    if (ap6181->task == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "ap6181->task create failed");
        goto __exit;
    }

    if (os_task_startup(ap6181->task) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "ap6181->task start failed");
        goto __exit;
    }
#endif

#ifdef BSP_AP6181_AP
    ap6181->wlan_dev.info.ssid                  = BSP_AP6181_AP_SSID;
    ap6181->wlan_dev.info.password              = BSP_AP6181_AP_PASSWORD;
    ap6181->wlan_dev.info.channel               = BSP_AP6181_AP_CHANNEL;
    ap6181->wlan_dev.net_dev.info.ip_addr       = BSP_AP6181_AP_IP_ADDR;
    ap6181->wlan_dev.net_dev.info.gw_addr       = BSP_AP6181_AP_GW_ADDR;
    ap6181->wlan_dev.net_dev.info.ip_mask       = BSP_AP6181_AP_IP_MASK;
    ap6181->wlan_dev.net_dev.info.dhcp_start_id = BSP_AP6181_AP_DHCP_SID;
    ap6181->wlan_dev.net_dev.info.dhcp_end_id   = BSP_AP6181_AP_DHCP_EID;
#endif
    ap6181->wlan_dev.info.security = AP6181_INIT_SECURITY;
    ap6181->wlan_dev.info.country  = AP6181_INIT_COUNTRY;
    if (os_wlan_start(&ap6181->wlan_dev) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_wlan_start failed!");

        os_pin_detach_irq(BSP_AP6181_IRQ_PIN);

        goto __exit;
    }

    if (os_wlan_net_register(&ap6181->wlan_dev, "ap6181") != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_wlan_net_register failed!");
        goto __exit;
    }

    return OS_EOK;

__exit:
    if (ap6181->task)
    {
        os_task_destroy(ap6181->task);
    }

    if (ap6181)
    {
        os_free(ap6181);
    }

    return OS_EOK;
}

OS_DEVICE_INIT(ap6181_init, OS_INIT_SUBLEVEL_LOW);
