/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_hook.h
 *
 * @brief       Provide kernel hook
 *
 * @revision
 * Date         Author          Notes
 * 2022-03-18   OneOS Team      First version
 ***********************************************************************************************************************
 */
#ifndef __OS_HOOK_H__
#define __OS_HOOK_H__

#include <oneos_config.h>
#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(OS_USING_IPC_HOOK)

#define OS_IPC_HOOK_DEFINE(domain, hp, sta, ...)
#define OS_IPC_HOOK_DECLARE(domain, hp, sta, ...)
#define OS_IPC_HOOK_CALL(domain, hp, sta, ...)
#define OS_IPC_HOOK_ADD(domain, hp, sta, func)
#define OS_IPC_HOOK_DEL(domain, hp, sta, func)

#else

/**
 * @brief hook define
 *
 * @param domain, hook domain
 * @param hp, hook point
 * @param sta, hook point state
 * @param ..., hook para
 *
 * @return no return
 *
 * eg. OS_IPC_HOOK_DEFINE(sem, init, start, os_sem_t*);
 *
 */
#define OS_IPC_HOOK_DEFINE(domain, hp, sta, ...) void (*gs_os_hook_##domain##_##hp##_##sta)(__VA_ARGS__) = OS_NULL

#define OS_IPC_HOOK_DECLARE(domain, hp, sta, ...) extern void (*gs_os_hook_##domain##_##hp##_##sta)(__VA_ARGS__)

/**
 * @brief trace call
 *
 * @param domain, hook domain
 * @param hp, hook point
 * @param sta, hook point state
 * @param ..., hook para
 *
 * @return no return
 *
 * eg. OS_IPC_HOOK_CALL(sem, init, start, sem);
 *
 */
#define OS_IPC_HOOK_CALL(domain, hp, sta, ...)                                                                         \
    do                                                                                                                 \
    {                                                                                                                  \
        if (gs_os_hook_##domain##_##hp##_##sta)                                                                        \
        {                                                                                                              \
            gs_os_hook_##domain##_##hp##_##sta(__VA_ARGS__);                                                           \
        }                                                                                                              \
    } while (0);

/**
 * @brief register hook
 *
 * @param domain, hook domain
 * @param hp, hook point
 * @param sta, hook point state
 * @param func, callback func
 *
 * @return OS_TRUE or OS_FALSE
 *
 * eg. OS_IPC_HOOK_ADD(sem, init, start, func);
 *
 */
#define OS_IPC_HOOK_ADD(domain, hp, sta, func)                                                                         \
    (!(gs_os_hook_##domain##_##hp##_##sta)) ? ((gs_os_hook_##domain##_##hp##_##sta = func), OS_TRUE) : (OS_FALSE)

/**
 * @brief unregister hook
 *
 * @param domain, hook domain
 * @param hp, hook point
 * @param sta, hook point state
 * @param func, not care
 *
 * @return OS_TRUE
 */
#define OS_IPC_HOOK_DEL(domain, hp, sta, func) (gs_os_hook_##domain##_##hp##_##sta = OS_NULL), OS_TRUE

#endif

#ifdef __cplusplus
}
#endif

#endif
