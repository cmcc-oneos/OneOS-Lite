## 1.简介
之前文章使用`Renode`简单支持了`OneOS-Lite`的运行，但shell的使用却并没支持起来。

此次，我们将基于`Renode`仿真开发`OneOS-Lite`的`shell`使用，通过虚拟环境，也能通过`shell`，体验更好的互动！

再次，我们给出了相关源码地址和工具，方便大家体验！

`OneOS-Lite`源码： [https://gitee.com/cmcc-oneos/OneOS-Lite](https://gitee.com/cmcc-oneos/OneOS-Lite)

`Renode`工具下载：[https://dl.antmicro.com/projects/renode/builds/renode-latest.msi](https://dl.antmicro.com/projects/renode/builds/renode-latest.msi)

`OneOS-Cube`编译工具下载：[https://gitee.com/cmcc-oneos/one-os-cube](https://gitee.com/cmcc-oneos/one-os-cube)

## 2.编译project
首先通过git下载好`OneOS-Lite`源码，然后找到`project`目录下的`STM32F407VG-renode`项目。

在该`project`目录下打开`OneOS-Cube`工具，执行`menuconfig`进行配置，也可以直接执行`scons`进行编译。
![](./编译.gif)

生成的`elf`可执行文件放在源码根目录下的`out`文件夹里面。

## 3.使用Renode运行
首先查看`STM32F407VG-renode`项目下是否有`stm32f4_discovery.resc`描述文件。
![](./脚本.png)

修改该描述文件中的文件路径为你自己电脑所在的路径。然后，打开`Renode`工具，执行该描述文件。
```
include @D:\gitee\OneOS-Lite\projects\STM32F407VG-renode\stm32f4_discovery.resc

s
```

![](./启动.gif)

## 4.体验虚拟环境下的shell
如上`gif`所示，我们可以在`shell`命令行中，使用`help`命令查看常用命令。可以使用`show_task`，查看系统中所存在的任务；可以使用`show_mem`查看系统内存使用情况；可以使用`show_mem`，查看信号量的使用情况，等等。
